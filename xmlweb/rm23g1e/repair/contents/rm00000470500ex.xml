<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A9_T00JX" variety="T00JX">
<name>PARKING ASSIST ECU (for RHD)</name>
<para id="RM00000470500EX" category="A" type-id="80001" name-id="PM589-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM00000470500EX_01" type-id="01" category="01">
<s-1 id="RM00000470500EX_01_0012" proc-id="RM23G0E___0000D2R00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="2">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000470500EX_01_0013" proc-id="RM23G0E___0000B9W00000">
<ptxt>REMOVE DOOR SCUFF PLATE ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E200370E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the door scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 4 clips, 10 claws and 2 guides and remove the door scuff plate.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0014" proc-id="RM23G0E___0000B9X00000">
<ptxt>REMOVE COWL SIDE TRIM BOARD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E200371" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Detach the clip and claw and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0015" proc-id="RM23G0E___0000CLB00000">
<ptxt>REMOVE INSTRUMENT SIDE PANEL RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E200372E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the instrument side panel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 5 clips, claw and 3 guides and remove the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0016" proc-id="RM23G0E___0000D2G00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL FINISH PANEL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="E201134E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a screwdriver, detach the 2 claws and open the cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="E200374" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 14 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and each cable and remove the lower instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0018" proc-id="RM23G0E___00008CS00000">
<ptxt>REMOVE INSTRUMENT PANEL FINISH PLATE GARNISH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238239" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and remove the instrument panel finish plate garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0010" proc-id="RM23G0E___00007BI00000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 clips and 2 guides to install the No. 2 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
<figure>
<graphic graphicname="B298555E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0017" proc-id="RM23G0E___0000D2G00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL FINISH PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="E201134E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a screwdriver, detach the 2 claws and open the cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="E200374" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 14 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and each cable and remove the lower instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0011" proc-id="RM23G0E___0000D2I00000">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL AIRBAG ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E204224" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts and Lower No. 1 instrument panel airbag.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0019" proc-id="RM23G0E___0000D2K00000">
<ptxt>REMOVE DRIVER SIDE JUNCTION BLOCK ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 3 connectors.</ptxt>
<figure>
<graphic graphicname="E197200" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and disconnect the 2 connectors labeled (1) as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E197201E06" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector labeled (2).</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and 2 nuts and disconnect the driver side junction block assembly.</ptxt>
<figure>
<graphic graphicname="E197202" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and disconnect the connector as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E178377" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and release the connector's lock as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E178379" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and disconnect the connector as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E178381" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the driver side junction block assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000470500EX_01_0009" proc-id="RM23G0E___0000D2Q00000">
<ptxt>REMOVE PARKING ASSIST ECU</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E197678" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the 3 connectors.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and parking assist ECU.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>