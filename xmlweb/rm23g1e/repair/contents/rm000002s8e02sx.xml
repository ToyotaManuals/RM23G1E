<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AL_T00K9" variety="T00K9">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002S8E02SX" category="U" type-id="303FJ" name-id="MP0GP-25" from="201207">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000002S8E02SX_z0" proc-id="RM23G0E___0000D9E00000">
<content5 releasenbr="1">
<step1>
<ptxt>READ DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Communication D-Door Motor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connection status between front power window regulator motor assembly LH*1 or RH*2 and main body ECU (multiplex network body ECU) / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>When there is a malfunction, a DTC is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication P-Door Motor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connection status between front power window regulator motor assembly RH*1 or LH*2 and main body ECU (multiplex network body ECU) / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>When there is a malfunction, a DTC is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication RR-Door Motor*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connection status between rear power window regulator motor assembly RH and main body ECU (multiplex network body ECU) / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>When there is a malfunction, a DTC is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication RL-Door Motor*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connection status between rear power window regulator motor assembly LH and main body ECU (multiplex network body ECU) / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>When there is a malfunction, a DTC is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication Slide Roof*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connection status between sliding roof drive gear sub-assembly and main body ECU (multiplex network body ECU) / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>When there is a malfunction, a DTC is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication Master SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connection status between multiplex network master switch and main body ECU (multiplex network body ECU) / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>When there is a malfunction, a DTC is stored.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Number of Trouble Codes</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number of trouble codes / Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle">
<ptxt>Number of stored DTCs displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Communication Double Locking*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connection status between double lock door control relay assembly and main body ECU (multiplex network body ECU) / OK or STOP</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: Connection</ptxt>
<ptxt>STOP: No connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>When there is a malfunction, a DTC is stored.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
<item>
<ptxt>*3: for 5 Door</ptxt>
</item>
<item>
<ptxt>*4: w/ Sliding Roof System</ptxt>
</item>
<item>
<ptxt>*5: w/ Double Locking System</ptxt>
</item>
</list1>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>