<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AL_T00K9" variety="T00K9">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002S8I02RX" category="C" type-id="801I2" name-id="NW1MT-04" from="201207" to="201210">
<dtccode>B2325</dtccode>
<dtcname>LIN Communication Bus Malfunction</dtcname>
<subpara id="RM000002S8I02RX_01" type-id="60" category="03" proc-id="RM23G0E___0000DAQ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU (multiplex network body ECU) intermittently monitors the LIN communication bus between the components related to the doors, double lock door control relay assembly*1 and sliding roof drive gear sub-assembly*2. DTC B2325 is stored when a malfunction in the LIN communication bus between the components related to the doors, double lock door control relay assembly*1 and sliding roof drive gear sub-assembly*2 is detected consecutively 3 times.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Double Locking System</ptxt>
</item>
<item>
<ptxt>*2: w/ Sliding Roof System</ptxt>
</item>
</list1>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.56in"/>
<colspec colname="COL2" colwidth="2.93in"/>
<colspec colname="COL3" colwidth="2.59in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2325</ptxt>
</entry>
<entry valign="middle">
<ptxt>The main body ECU (multiplex network body ECU) detects a malfunction in the LIN communication bus between components related to the doors, double lock door control relay assembly*2 and sliding roof drive gear sub-assembly*3 consecutively 3 times.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Multiplex network master switch assembly</ptxt>
</item>
<item>
<ptxt>Front power window regulator motor assembly LH</ptxt>
</item>
<item>
<ptxt>Front power window regulator motor assembly RH</ptxt>
</item>
<item>
<ptxt>Rear power window regulator motor assembly LH*1</ptxt>
</item>
<item>
<ptxt>Rear power window regulator motor assembly RH*1</ptxt>
</item>
<item>
<ptxt>Double lock door control relay*2</ptxt>
</item>
<item>
<ptxt>Sliding roof drive gear sub-assembly*3</ptxt>
</item>
<item>
<ptxt>Main body ECU (multiplex network body ECU)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for 5 Door</ptxt>
</item>
<item>
<ptxt>*2: w/ Double Locking System</ptxt>
</item>
<item>
<ptxt>*3: w/ Sliding Roof System</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002S8I02RX_02" type-id="32" category="03" proc-id="RM23G0E___0000DAR00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C212388E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002S8I02RX_03" type-id="51" category="05" proc-id="RM23G0E___0000DAS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle and turn a courtesy light switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</atten3>
<atten4>
<ptxt>When DTC B2325 and a LIN communication stop DTC are output simultaneously, first perform troubleshooting for the LIN communication stop DTC. Then perform troubleshooting for DTC B2325.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002S8I02RX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002S8I02RX_05_0036" proc-id="RM23G0E___0000DB700000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0002" proc-id="RM23G0E___0000DAT00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2325 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0016" fin="true">OK</down>
<right ref="RM000002S8I02RX_05_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0003" proc-id="RM23G0E___0000DAU00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - MULTIPLEX NETWORK MASTER SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the main body ECU (multiplex network body ECU) from the driver side junction block assembly (See page <xref label="Seep01" href="RM000003WBO02VX"/>).</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K4*1 or J9*2 multiplex network master switch assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A-16 (LIN2) - K4-17 (LIN1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-16 (LIN2) or K4-17 (LIN1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A-16 (LIN2) - J9-17 (LIN1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-16 (LIN2) or J9-17 (LIN1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0004" fin="false">OK</down>
<right ref="RM000002S8I02RX_05_0037" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0004" proc-id="RM23G0E___0000DAV00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<test1>
<ptxt>Install the main body ECU (multiplex network body ECU) to the driver side junction block assembly (See page <xref label="Seep02" href="RM000003WBM02UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the K4*1 or J9*2 multiplex network master switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K7*1 or J7*2 front power window regulator motor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0005" proc-id="RM23G0E___0000DAW00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0006" fin="false">A</down>
<right ref="RM000002S8I02RX_05_0018" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0006" proc-id="RM23G0E___0000DAX00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<test1>
<ptxt>Connect the K7*1 or J7*2 front power window regulator motor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the K4*1 or J9*2 multiplex network master switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0007" proc-id="RM23G0E___0000DAY00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0008" fin="false">A</down>
<right ref="RM000002S8I02RX_05_0017" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0008" proc-id="RM23G0E___0000DAZ00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<test1>
<ptxt>Connect the K4*1 or J9*2 multiplex network master switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the J7*1 or K7*2 front power window regulator motor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0009" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0009" proc-id="RM23G0E___0000DB000000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (for 5 Door)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (for 3 Door, w/ Double Locking System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (for 3 Door, w/o Double Locking System, w/ Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (for 3 Door, w/o Double Locking System, w/o Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0010" fin="false">A</down>
<right ref="RM000002S8I02RX_05_0047" fin="false">B</right>
<right ref="RM000002S8I02RX_05_0049" fin="false">C</right>
<right ref="RM000002S8I02RX_05_0052" fin="true">D</right>
<right ref="RM000002S8I02RX_05_0019" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0010" proc-id="RM23G0E___0000DB100000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the J7*1 or K7*2 front power window regulator motor assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Disconnect the L4 rear power window regulator motor assembly RH connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0011" proc-id="RM23G0E___0000DB200000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0012" fin="false">A</down>
<right ref="RM000002S8I02RX_05_0020" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0012" proc-id="RM23G0E___0000DB300000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the L4 rear power window regulator motor assembly RH connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the M4 rear power window regulator motor assembly LH connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0013" proc-id="RM23G0E___0000DB400000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/ Double Locking System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/o Double Locking System, w/ Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/o Double Locking System, w/o Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0014" fin="false">A</down>
<right ref="RM000002S8I02RX_05_0039" fin="false">B</right>
<right ref="RM000002S8I02RX_05_0053" fin="true">C</right>
<right ref="RM000002S8I02RX_05_0021" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0014" proc-id="RM23G0E___0000DB500000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the M4 rear power window regulator motor assembly LH connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G49 double lock door control relay assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0038" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0038" proc-id="RM23G0E___0000DB800000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/ Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/o Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0039" fin="false">A</down>
<right ref="RM000002S8I02RX_05_0054" fin="true">B</right>
<right ref="RM000002S8I02RX_05_0041" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0039" proc-id="RM23G0E___0000DB900000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>w/ Double Locking System:</ptxt>
<test2>
<ptxt>Connect the G49 double lock door control relay assembly connector.</ptxt>
</test2>
</test1>
<test1>
<ptxt>w/o Double Locking System:</ptxt>
<test2>
<ptxt>Connect the M4 rear power window regulator motor assembly LH connector.</ptxt>
</test2>
</test1>
<test1>
<ptxt>Disconnect the W2 sliding roof drive gear sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0015" proc-id="RM23G0E___0000DB600000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0024" fin="true">A</down>
<right ref="RM000002S8I02RX_05_0023" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0047" proc-id="RM23G0E___0000DBA00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the J7*1 or K7*2 front power window regulator motor assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Disconnect the G49 double lock door control relay assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0048" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0048" proc-id="RM23G0E___0000DBB00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/ Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output (w/o Sliding Roof System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0049" fin="false">A</down>
<right ref="RM000002S8I02RX_05_0055" fin="true">B</right>
<right ref="RM000002S8I02RX_05_0056" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0049" proc-id="RM23G0E___0000DBC00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>w/ Double Locking System:</ptxt>
<test2>
<ptxt>Connect the G49 double lock door control relay assembly connector.</ptxt>
</test2>
</test1>
<test1>
<ptxt>w/o Double Locking System:</ptxt>
<test2>
<ptxt>Connect the J7*1 or K7*2 front power window regulator motor assembly connector. </ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test2>
</test1>
<test1>
<ptxt>Disconnect the W2 sliding roof drive gear sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0050" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0050" proc-id="RM23G0E___0000DBD00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2325 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002S8I02RX_05_0051" fin="true">A</down>
<right ref="RM000002S8I02RX_05_0057" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0051">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0016">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0037">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0018">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR ASSEMBLY<xref label="Seep01" href="RM0000011QF0B8X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0017">
<testtitle>REPLACE MULTIPLEX NETWORK MASTER SWITCH ASSEMBLY<xref label="Seep01" href="RM000002STU06UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0052">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0019">
<testtitle>REPLACE FRONT POWER WINDOW REGULATOR MOTOR ASSEMBLY<xref label="Seep01" href="RM0000011QF0B8X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0020">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR MOTOR ASSEMBLY RH<xref label="Seep01" href="RM0000011RX077X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0053">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0021">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR MOTOR ASSEMBLY LH<xref label="Seep01" href="RM0000011RX077X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0054">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0041">
<testtitle>REPLACE DOUBLE LOCK DOOR CONTROL RELAY ASSEMBLY<xref label="Seep01" href="RM000003CM8009X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0023">
<testtitle>REPLACE SLIDING ROOF DRIVE GEAR SUB-ASSEMBLY<xref label="Seep01" href="RM000002LPC01YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0024">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0055">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0056">
<testtitle>REPLACE DOUBLE LOCK DOOR CONTROL RELAY ASSEMBLY<xref label="Seep01" href="RM000003CM8009X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8I02RX_05_0057">
<testtitle>REPLACE SLIDING ROOF DRIVE GEAR SUB-ASSEMBLY<xref label="Seep01" href="RM000002LPC01YX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>