<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001B" variety="S001B">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001B_7B96L_T00G9" variety="T00G9">
<name>AIR SUSPENSION SYSTEM</name>
<para id="RM000001N9400PX" category="C" type-id="804BS" name-id="SC1HV-07" from="201207" to="201210">
<dtccode>C1713/13</dtccode>
<dtcname>Rear Height Control Sensor RH Circuit Malfunction</dtcname>
<dtccode>C1714/14</dtccode>
<dtcname>Rear Height Control Sensor LH Circuit Malfunction</dtcname>
<subpara id="RM000001N9400PX_01" type-id="60" category="03" proc-id="RM23G0E___00009NO00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The height control sensor changes its resistance value based on changes in vehicle height. The suspension control ECU outputs a fixed voltage of 5 V to the SHB terminal of the height control sensor. If the resistance value of the sensor changes, the voltage value of the sensor changes accordingly. The suspension control ECU then inputs the voltage value from the sensor to detect the vehicle height change.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1713/13</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the ignition switch ON, a voltage of 5.5 V or higher or 4.3 V or less at the power source of the rear height control sensor RH is detected for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>With the ignition switch ON, a voltage of 4.7 V or higher or 0.3 V or less at the rear height control sensor RH is detected for 1 second.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Rear height control sensor RH</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1714/14</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the ignition switch ON, a voltage of 5.5 V or higher or 4.3 V or less at the power source of the rear height control sensor LH is detected for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>With the ignition switch ON, a voltage of 4.7 V or higher or 0.3 V or less at the rear height control sensor LH is detected for 1 second.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Rear height control sensor LH</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001N9400PX_02" type-id="32" category="03" proc-id="RM23G0E___00009NP00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C216922E02" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001N9400PX_03" type-id="51" category="05" proc-id="RM23G0E___00009NQ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the suspension control ECU, perform registration (See page <xref label="Seep01" href="RM000003X4500LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001N9400PX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001N9400PX_04_0001" proc-id="RM23G0E___00009NR00000">
<testtitle>INSPECT REAR HEIGHT CONTROL SENSOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C214173E05" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration </title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.57in"/>
<colspec colname="COL2" colwidth="2.97in"/>
<colspec colname="COL3" colwidth="0.57in"/>
<colspec colname="COL4" colwidth="2.97in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Height Control Sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Rear Height Control Sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Min. Low</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Max. High</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>45°</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>45°</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<test1>
<ptxt>Check the rear height control sensor RH (when DTC C1713/13 is output).</ptxt>
<test2>
<ptxt>Remove the rear height control sensor RH (See page <xref label="Seep01" href="RM000002BGU00HX"/>).</ptxt>
</test2>
<test2>
<ptxt>Connect 3 dry cell batteries of 1.5 V in series.</ptxt>
</test2>
<test2>
<ptxt>Connect the positive (+) end of the batteries to terminal 1 (SHB) of the height control sensor and the negative (-) end of the batteries to terminal 3 (SHG). While slowly moving the sensor link up and down, measure the voltage between terminal 2 (SHRR) and terminal 3 (SHG).</ptxt>
</test2>
<test2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>3 (SHG) - 2 (SHRR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Max. High (45°)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.4 to 4.6 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Normal (0°)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.3 to 2.6 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Min. Low (-45°)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.4 to 0.6 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not apply a voltage of 5 V or higher.</ptxt>
</atten3>
</test2>
</test1>
<test1>
<ptxt>Check the rear height control sensor LH (when DTC C1714/14 is output).</ptxt>
<test2>
<ptxt>Remove the rear height control sensor LH (See page <xref label="Seep02" href="RM000002BGU00HX"/>).</ptxt>
</test2>
<test2>
<ptxt>Connect 3 dry cell batteries of 1.5 V in series.</ptxt>
</test2>
<test2>
<ptxt>Connect the positive (+) end of the batteries to terminal 1 (SHB) of the height control sensor and the negative (-) end of the batteries to terminal 3 (SHG). While slowly moving the sensor link up and down, measure the voltage between terminal 2 (SHRL) and terminal 3 (SHG).</ptxt>
</test2>
<test2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>3 (SHG) - 2 (SHRL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Max. High (45°)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.4 to 4.6 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Normal (0°)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.3 to 2.6 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Min. Low (-45°)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.4 to 0.6 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not apply a voltage of 5 V or higher.</ptxt>
</atten3>
</test2>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Rear height control sensor RH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Rear height control sensor LH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001N9400PX_04_0002" fin="false">A</down>
<right ref="RM000001N9400PX_04_0006" fin="true">B</right>
<right ref="RM000001N9400PX_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001N9400PX_04_0002" proc-id="RM23G0E___00009NS00000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR HEIGHT CONTROL SENSOR - SUSPENSION CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the rear height control sensor RH (when DTC C1713/13 is output).</ptxt>
<test2>
<ptxt>Disconnect the S3 rear height control sensor RH connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the R19 suspension control ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>S3-3 (SHG) - R19-16 (SGR2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S3-2 (SHRR) - R19-26 (SHRR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S3-1 (SHB) - R19-13 (SBR2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S3-3 (SHG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S3-2 (SHRR) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S3-1 (SHB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>Check the rear height control sensor LH (when DTC C1714/14 is output).</ptxt>
<test2>
<ptxt>Disconnect the S6 rear height control sensor LH connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the R19 suspension control ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>S6-3 (SHG) - R19-15 (SGL2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S6-2 (SHRL) - R19-24 (SHRL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S6-1 (SHB) - R19-14 (SBL2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S6-3 (SHG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S6-2 (SHRL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>S6-1 (SHB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001N9400PX_04_0009" fin="true">OK</down>
<right ref="RM000001N9400PX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N9400PX_04_0006">
<testtitle>REPLACE REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY RH<xref label="Seep01" href="RM000002BGU00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001N9400PX_04_0007">
<testtitle>REPLACE REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH<xref label="Seep01" href="RM000002BGU00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001N9400PX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001N9400PX_04_0009">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000002BGR013X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>