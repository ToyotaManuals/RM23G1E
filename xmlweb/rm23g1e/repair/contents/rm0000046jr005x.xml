<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S0028" variety="S0028">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S0028_7B9FD_T00P1" variety="T00P1">
<name>ROOF HEADLINING (for 3 Door)</name>
<para id="RM0000046JR005X" category="A" type-id="80002" name-id="IT2BH-02" from="201207">
<name>DISASSEMBLY</name>
<subpara id="RM0000046JR005X_01" type-id="11" category="10" proc-id="RM23G0E___0000I1X00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles. </ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000046JR005X_02" type-id="01" category="01">
<s-1 id="RM0000046JR005X_02_0001" proc-id="RM23G0E___0000I1Y00000">
<ptxt>REMOVE NO. 1 ROOF SILENCER PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240557" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<s3>
<ptxt>Remove the 3 No. 1 roof silencer pads.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B240561" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Sliding Roof:</ptxt>
<s3>
<ptxt>Remove the No. 1 roof silencer pad.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JR005X_02_0002" proc-id="RM23G0E___0000I1Z00000">
<ptxt>REMOVE NO. 2 ROOF SILENCER PAD (w/o Sliding Roof)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240559" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the No. 2 roof silencer pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JR005X_02_0004" proc-id="RM23G0E___0000I2000000">
<ptxt>REMOVE NO. 4 ROOF SILENCER PAD (w/o Sliding Roof)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240555" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the No. 4 roof silencer pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JR005X_02_0005" proc-id="RM23G0E___0000I2100000">
<ptxt>REMOVE VANITY LIGHT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B175184E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws labeled A and separate the bulb holder from the vanity light as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw labeled B and remove the vanity light.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Claw A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Claw B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bulb Holder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000046JR005X_02_0006" proc-id="RM23G0E___0000I2200000">
<ptxt>REMOVE VANITY LIGHT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JR005X_02_0007" proc-id="RM23G0E___0000I2300000">
<ptxt>REMOVE NO. 1 ROOF WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the visor connectors approximately 90° clockwise and remove them from the roof headlining.</ptxt>
</s2>
<s2>
<ptxt>Detach each clamp and remove the No. 1 roof wire.</ptxt>
</s2>
<s2>
<ptxt>Remove any butyl tape remaining on the roof headlining.</ptxt>
<figure>
<graphic graphicname="B240563E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Sliding Roof</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Sliding Roof</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Rain Sensor, for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*D</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ EC Mirror</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*E</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Rain Sensor, for RHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Visor Connector</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JR005X_02_0009" proc-id="RM23G0E___0000C1F00000">
<ptxt>REMOVE NO. 2 ANTENNA CORD SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Sliding Roof:</ptxt>
<s3>
<ptxt>for Diversity:</ptxt>
<ptxt>Including the clamps and notch in the part of the illustration labeled A, detach the antenna cord from the 6 clamps and the 2 notches on the rear of the roof headlining.</ptxt>
</s3>
<s3>
<ptxt>for Non Diversity:</ptxt>
<ptxt>Not including the clamps and notch in the part of the illustration labeled A, detach the antenna cord from the clamp and the notch on the rear of the roof headlining.</ptxt>
<figure>
<graphic graphicname="B239115E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the antenna cord from the roof headlining.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the roof headlining.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Sliding Roof:</ptxt>
<s3>
<ptxt>for Diversity:</ptxt>
<ptxt>Including the clamps and notch in the part of the illustration labeled A, detach the antenna cord from the 9 clamps and the 2 notches on the rear of the roof headlining.</ptxt>
</s3>
<s3>
<ptxt>for Non Diversity:</ptxt>
<ptxt>Not including the clamps and notch in the part of the illustration labeled A, detach the antenna cord from the 4 clamps and the notch on the rear of the roof headlining.</ptxt>
<figure>
<graphic graphicname="B239116E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the antenna cord from the roof headlining.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the roof headlining.</ptxt>
</s3>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>