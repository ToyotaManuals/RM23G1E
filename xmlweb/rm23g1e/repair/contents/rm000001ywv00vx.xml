<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9CW_T00MK" variety="T00MK">
<name>REAR POWER SEAT CONTROL SYSTEM</name>
<para id="RM000001YWV00VX" category="J" type-id="801ON" name-id="SE5BD-07" from="201210">
<dtccode/>
<dtcname>Reclining Motor Circuit</dtcname>
<subpara id="RM000001YWV00VX_01" type-id="60" category="03" proc-id="RM23G0E___0000G9B00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The fold seat control ECU receives switch operation signals from the No. 1 and No. 2 fold seat switches and rear power seat switch and activates the power seat motors. At this time, the Hall IC detects the actuation of the seatback and sends a seatback actuation signal to the fold seat control ECU. The fold seat control ECU uses signals from the Hall IC to detect if an object is caught or if any other abnormal condition has occurred.</ptxt>
</content5>
</subpara>
<subpara id="RM000001YWV00VX_02" type-id="32" category="03" proc-id="RM23G0E___0000G9C00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B239515E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001YWV00VX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001YWV00VX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001YWV00VX_04_0002" proc-id="RM23G0E___0000G9E00001">
<testtitle>INSPECT NO. 3 SEATBACK FRAME SUB-ASSEMBLY (RECLINING MOTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LH Side:</ptxt>
<figure>
<graphic graphicname="B239766E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test2>
<ptxt>Remove the No. 3 seatback frame sub-assembly LH (See page <xref label="Seep01" href="RM0000045DH009X"/>).</ptxt>
</test2>
<test2>
<ptxt>Check if the No. 3 seatback frame moves smoothly when the battery is connected to the reclining motor connector terminals.</ptxt>
</test2>
</test1>
<test1>
<ptxt>for RH Side:</ptxt>
<test2>
<ptxt>Remove the No. 3 seatback frame sub-assembly RH (See page <xref label="Seep02" href="RM0000045DH009X"/>).</ptxt>
</test2>
<test2>
<ptxt>Check if the No. 3 seatback frame moves smoothly when the battery is connected to the reclining motor connector terminals.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → 3 (FWD)</ptxt>
<ptxt>Battery negative (-) → 4 (BACK)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Forward</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) → 4 (BACK)</ptxt>
<ptxt>Battery negative (-) → 3 (FWD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Backward</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK (for LH Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK (for RH Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for LH Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for RH Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001YWV00VX_04_0005" fin="false">A</down>
<right ref="RM000001YWV00VX_04_0008" fin="false">B</right>
<right ref="RM000001YWV00VX_04_0009" fin="true">C</right>
<right ref="RM000001YWV00VX_04_0010" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000001YWV00VX_04_0005" proc-id="RM23G0E___0000G9F00001">
<testtitle>CHECK HARNESS AND CONNECTOR (FOLD SEAT CONTROL ECU - NO. 3 SEATBACK FRAME LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the d2 and d1 ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the d3 No. 3 seatback frame connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>d1-37 (SV2) - d3-5 (HLV)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d1-37 (SV2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d1-19 (SRCL) - d3-2 (HLS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d1-19 (SRCL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d1-24 (SG2) - d3-6 (HLG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d1-24 (SG2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d2-1 (RCL+) - d3-3 (FWD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d2-1 (RCL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d2-5 (RCL-) - d3-4 (BACK)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d2-5 (RCL-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001YWV00VX_04_0001" fin="false">OK</down>
<right ref="RM000001YWV00VX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001YWV00VX_04_0001" proc-id="RM23G0E___0000G9D00001">
<testtitle>CHECK FOLD SEAT CONTROL ECU LH</testtitle>

<content6 releasenbr="1">
<test1>
<ptxt>Remove the fold seat control ECU LH with its connectors still connected (See page <xref label="Seep01" href="RM0000045DH009X"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
</test1>
<figure>
<graphic graphicname="B193667E18" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>d1-37 (SV2) - d1-24 (SG2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.5 to 8 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d1-19 (SRCL) - d1-24 (SG2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Motor is operating</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d2-1 (RCL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear power seat switch LH forward</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d2-5 (RCL-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear power seat switch LH backward</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Fold Seat Control ECU LH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001YWV00VX_04_0004" fin="true">OK</down>
<right ref="RM000001YWV00VX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001YWV00VX_04_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000001YWK00WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001YWV00VX_04_0008" proc-id="RM23G0E___0000G9H00001">
<testtitle>CHECK HARNESS AND CONNECTOR (FOLD SEAT CONTROL ECU - NO. 3 SEATBACK FRAME RH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the c2 and c1 ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the c3 No. 3 seatback frame connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>c1-37 (SV2) - c3-5 (HLV)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c1-37 (SV2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c1-19 (SRCL) - c3-2 (HLS)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c1-19 (SRCL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c1-24 (SG2) - c3-6 (HLG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c1-24 (SG2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c2-1 (RCL+) - c3-3 (FWD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c2-1 (RCL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c2-5 (RCL-) - c3-4 (BACK)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c2-5 (RCL-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001YWV00VX_04_0007" fin="true">OK</down>
<right ref="RM000001YWV00VX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001YWV00VX_04_0007" proc-id="RM23G0E___0000G9G00001">
<testtitle>CHECK FOLD SEAT CONTROL ECU RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the fold seat control ECU RH with its connectors still connected (See page <xref label="Seep01" href="RM0000045DP009X"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
</test1>
<figure>
<graphic graphicname="B193667E21" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>c1-37 (SV2) - c1-24 (SG2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.5 to 8 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c1-19 (SRCL) - c1-24 (SG2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Motor is operating</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c2-1 (RCL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear power seat switch RH forward</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c2-5 (RCL-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear power seat switch RH backward</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Fold Seat Control ECU RH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001YWV00VX_04_0004" fin="true">OK</down>
<right ref="RM000001YWV00VX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001YWV00VX_04_0009">
<testtitle>REPLACE NO. 3 SEATBACK FRAME SUB-ASSEMBLY LH<xref label="Seep01" href="RM0000045DH009X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001YWV00VX_04_0010">
<testtitle>REPLACE NO. 3 SEATBACK FRAME SUB-ASSEMBLY RH<xref label="Seep01" href="RM0000045DH009X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001YWV00VX_04_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>