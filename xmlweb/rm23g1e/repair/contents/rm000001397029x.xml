<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12018_S0014" variety="S0014">
<name>CLUTCH</name>
<ttl id="12018_S0014_7B94W_T00EK" variety="T00EK">
<name>CLUTCH UNIT (for 1KD-FTV)</name>
<para id="RM000001397029X" category="A" type-id="30014" name-id="CL1SK-03" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000001397029X_01" type-id="01" category="01">
<s-1 id="RM000001397029X_01_0002" proc-id="RM23G0E___00008GY00001">
<ptxt>INSTALL CLUTCH DISC ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert SST into the clutch disc. Then insert SST (together with the clutch disc) into the flywheel to install the clutch disc.</ptxt>
<figure>
<graphic graphicname="C212716E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<sst>
<sstitem>
<s-number>09301-00110</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Flywheel Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be sure to install the clutch disc so that it is facing in the correct direction.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001397029X_01_0003" proc-id="RM23G0E___00008GZ00001">
<ptxt>INSTALL CLUTCH COVER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215516E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Align the matchmarks on the clutch cover and flywheel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the 6 bolts uniformly in the order shown in the illustration, starting with the bolt located near the knock pin on the top.</ptxt>
<sst>
<sstitem>
<s-number>09301-00110</s-number>
</sstitem>
</sst>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Move SST up and down, and right and left lightly after checking that the clutch disc assembly is in the center, and then tighten the bolts.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000001397029X_01_0004" proc-id="RM23G0E___00008H000001">
<ptxt>INSPECT AND ADJUST CLUTCH COVER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C217883E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a dial indicator with a roller instrument, measure the diaphragm spring tip alignment.</ptxt>
<sst>
<sstitem>
<s-number>09333-00013</s-number>
</sstitem>
</sst>
<spec>
<title>Maximum non-alignment</title>
<specitem>
<ptxt>0.5 mm (0.0196 in.)</ptxt>
</specitem>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the is alignment is more than the maximum, use SST to adjust the diaphragm spring tip alignment.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM000001397029X_01_0005" proc-id="RM23G0E___00008H100001">
<ptxt>INSTALL RELEASE FORK SUPPORT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the release fork support to the manual transmission unit.</ptxt>
<torque>
<torqueitem>
<t-value1>39</t-value1>
<t-value2>400</t-value2>
<t-value4>29</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000001397029X_01_0008" proc-id="RM23G0E___00008H300001">
<ptxt>INSTALL CLUTCH RELEASE BEARING ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C203417E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Apply release hub grease to the clutch release bearing, and then install it to the clutch release fork with the clip.</ptxt>
<spec>
<title>Grease</title>
<specitem>
<ptxt>Toyota Genuine Release Hub Grease or equivalent</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Release hub grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000001397029X_01_0007" proc-id="RM23G0E___00008H200001">
<ptxt>INSTALL CLUTCH RELEASE FORK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the clutch release fork.</ptxt>
<atten3>
<ptxt>After the installation, move the fork forward and backward to check that the release bearing slides smoothly.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Apply clutch spline grease to the spline of the input shaft.</ptxt>
<figure>
<graphic graphicname="C215512E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Grease</title>
<specitem>
<ptxt>Toyota Genuine Clutch Spline Grease or equivalent</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clutch spline grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000001397029X_01_0009">
<ptxt>INSTALL CLUTCH RELEASE FORK BOOT</ptxt>
</s-1>
<s-1 id="RM000001397029X_01_0010" proc-id="RM23G0E___00008H400001">
<ptxt>INSTALL MANUAL TRANSMISSION ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the manual transmission unit assembly (See page <xref label="Seep01" href="RM00000178Q00SX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>