<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001M" variety="S001M">
<name>STEERING COLUMN</name>
<ttl id="12038_S001M_7B98P_T00ID" variety="T00ID">
<name>STEERING LOCK SYSTEM</name>
<para id="RM000001T1903HX" category="D" type-id="303F2" name-id="SR0EG-33" from="201207" to="201210">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000001T1903HX_z0" proc-id="RM23G0E___0000BAB00000">
<content5 releasenbr="1">
<step1>
<ptxt>DESCRIPTION</ptxt>
<step2>
<ptxt>The steering lock system locks or unlocks the steering by activating the steering lock bar with a motor. The steering lock ECU activates the motor based on signals from the certification ECU and power management control ECU.</ptxt>
</step2>
<step2>
<ptxt>A Local Interconnect Network (LIN) is used for communication between different ECUs in this system.</ptxt>
</step2>
</step1>
<step1>
<ptxt>FUNCTIONS OF COMPONENTS</ptxt>
<step2>
<ptxt>The steering lock ECU controls the system based on information from the following components:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.12in"/>
<colspec colname="COL2" colwidth="4.96in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Steering lock actuator assembly</ptxt>
</entry>
<entry>
<ptxt>Consists of a motor, lock bar, lock and unlock position sensors, etc. activated by the steering lock ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering lock ECU</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Included in the steering lock actuator assembly. This ECU activates the steering lock motor based on permission signals from the certification ECU.</ptxt>
</item>
<item>
<ptxt>Records ECU verification codes.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Power management control ECU</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Permits the steering lock ECU to supply power to activate the motor.</ptxt>
</item>
<item>
<ptxt>Performs power supply switching control.</ptxt>
</item>
<item>
<ptxt>Controls the engine switch indicator.</ptxt>
</item>
<item>
<ptxt>Operates the IG2 relay.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Certification ECU</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Performs key verification (checks whether the correct key is present).</ptxt>
</item>
<item>
<ptxt>Controls the security indicator and the illumination of the lettering on the engine switch.</ptxt>
</item>
<item>
<ptxt>Records ECU verification codes.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>IG2 relay</ptxt>
</entry>
<entry>
<ptxt>Controlled by the power management control ECU. Signals indicating the on/off status of this relay are sent to the power management control ECU and steering lock ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ID code box</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Transmits a command to set or cancel the immobiliser to the ECM based on permission signals from the certification ECU.</ptxt>
</item>
<item>
<ptxt>Records ECU verification codes.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>WARNING FUNCTION OF ENGINE SWITCH INDICATOR</ptxt>
<figure>
<graphic graphicname="A109251E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Indicator Light</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step2>
<ptxt>The power management control ECU blinks the indicator light LED of the engine switch when any of the following problems occur in the system.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indicator Blink Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indication Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Countermeasure</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Steering lock release malfunction</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Blinks in green at 1-second intervals</ptxt>
</item>
<item>
<ptxt>Goes off 30 seconds after blinking starts</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Motor operates to release steering lock, but steering lock cannot be released (lock bar stuck in steering column).</ptxt>
</entry>
<entry valign="middle">
<ptxt>Push engine switch while turning steering wheel to right and left</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Malfunction in push button start function</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Blinks in amber at 2-second intervals</ptxt>
</item>
<item>
<ptxt>Goes off 15 seconds after blinking starts</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in devices for activating motor</ptxt>
</item>
<item>
<ptxt>Problem in steering lock ECU or power management control ECU</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Troubleshoot by following "How to Proceed with Troubleshooting" (See page <xref label="Seep01" href="RM000000YEF0FUX"/>)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>