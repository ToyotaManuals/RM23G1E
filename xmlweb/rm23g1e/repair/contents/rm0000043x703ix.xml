<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001P" variety="S001P">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001P_7B99L_T00J9" variety="T00J9">
<name>NAVIGATION SYSTEM (for DVD)</name>
<para id="RM0000043X703IX" category="D" type-id="3001B" name-id="IN0KV-80" from="201210">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM0000043X703IX_z0" proc-id="RM23G0E___0000CD700001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use these procedures to troubleshoot the navigation system (for DVD).</ptxt>
</item>
<item>
<ptxt>*: Use the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>INSPECT BATTERY VOLTAGE</testtitle>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding.</ptxt>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>BASIC INSPECTION</testtitle>
<test1>
<ptxt>Turn the ignition switch to ACC.</ptxt>
</test1>
<test1>
<ptxt>Check whether or not the display appears on the display and navigation module display.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Display appears</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Display does not appear</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to step 6</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK CAN COMMUNICATION SYSTEM*</testtitle>
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for LHD with Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for LHD without Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for RHD with Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for RHD without Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to CAN Communication System (See page <xref label="Seep23" href="RM000000WI80BXX"/>)</action-ci-right>
<result>C</result>
<action-ci-right>Go to CAN Communication System (See page <xref label="Seep24" href="RM0000047VV006X"/>)</action-ci-right>
<result>D</result>
<action-ci-right>Go to CAN Communication System (See page <xref label="Seep25" href="RM0000047VX006X"/>)</action-ci-right>
<result>E</result>
<action-ci-right>Go to CAN Communication System (See page <xref label="Seep26" href="RM00000482H006X"/>)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK FOR DTC*</testtitle>
<test1>
<ptxt>Check for DTCs and note any codes that are output. </ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs. </ptxt>
</test1>
<test1>
<ptxt>Check for DTCs. Based on the DTCs output in the first step, try to force output of the navigation system DTCs by simulating the conditions indicated by the DTCs.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Even if the malfunction symptom is not confirmed, check for DTCs as the system stores past DTCs.</ptxt>
</item>
<item>
<ptxt>Refer to the detailed description on the diagnostic screen as necessary (See page <xref label="Seep02" href="RM0000043X8019X"/>).</ptxt>
</item>
<item>
<ptxt>Check and clear past diagnostic trouble codes. Check the diagnostic trouble code and inspect the area the code indicates (See page <xref label="Seep13" href="RM0000044WO01MX"/>).</ptxt>
</item>
</list1>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>Go to step 8</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>DIAGNOSTIC TROUBLE CODE CHART</testtitle>
<test1>
<ptxt>Find the output code in Diagnostic Trouble Code Chart (See page <xref label="Seep16" href="RM0000044WL00TX"/>).</ptxt>
<atten3>
<ptxt>The navigation system outputs DTCs for the following systems. When DTCs other than those in Diagnostic Trouble Code Chart for the navigation system are output, refer to Diagnostic Trouble Code Chart for the appropriate system.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Audio and visual system (w/ Navigation System) (See page <xref label="Seep17" href="RM000003YVG02KX"/>)</ptxt>
</item>
<item>
<ptxt>Rear seat entertainment system (w/ Rear Seat Entertainment System) (See page <xref label="Seep18" href="RM000003WTP02GX"/>)</ptxt>
</item>
<item>
<ptxt>TOYOTA parking assist-sensor system (for 8 Sensor Type) (See page <xref label="Seep19" href="RM000003AKC02OX"/>)</ptxt>
</item>
<item>
<ptxt>Parking assist monitor system (See page <xref label="Seep20" href="RM0000035D503TX"/>)</ptxt>
</item>
<item>
<ptxt>Wide view front monitor system (See page <xref label="Seep21" href="RM000003EAY00JX"/>)</ptxt>
</item>
<item>
<ptxt>Side monitor system (w/ Parking Assist Monitor System) (See page <xref label="Seep22" href="RM000003XLW01TX"/>)</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTCs for navigation system are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for audio and visual system (w/ Navigation System) are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for rear seat entertainment system (w/ Rear Seat Entertainment System) are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for TOYOTA parking assist-sensor system (for 8 Sensor Type) are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for parking assist monitor system are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for wide view front monitor system are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>F</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTCs for side monitor system (w/ Parking Assist Monitor System) are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<results>
<result>B</result>
<action-ci-right>GO TO AUDIO AND VISUAL SYSTEM (w/ Navigation System)</action-ci-right>
<result>C</result>
<action-ci-right>GO TO REAR SEAT ENTERTAINMENT SYSTEM</action-ci-right>
<result>D</result>
<action-ci-right>GO TO TOYOTA PARKING ASSIST-SENSOR SYSTEM (for 8 Sensor Type)</action-ci-right>
<result>E</result>
<action-ci-right>GO TO PARKING ASSIST MONITOR SYSTEM</action-ci-right>
<result>F</result>
<action-ci-right>GO TO WIDE VIEW FRONT MONITOR SYSTEM</action-ci-right>
<result>G</result>
<action-ci-right>GO TO SIDE MONITOR SYSTEM (w/ Parking Assist Monitor System)</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PROBLEM SYMPTOMS TABLE</testtitle>
<test1>
<ptxt>Refer to Problem Symptoms Table (See page <xref label="Seep14" href="RM0000011BR0KWX"/>).</ptxt>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fault is not listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fault is listed in problem symptoms table</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the symptom does not recur and no DTC is output, perform the symptom simulation method (See page <xref label="Seep15" href="RM000003YMJ00HX"/>).</ptxt>
</atten4>
<results>
<result>B</result>
<action-ci-right>Go to step 9</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>OVERALL ANALYSIS AND TROUBLESHOOTING</testtitle>
<test1>
<ptxt>Terminals of ECU (See page <xref label="Seep01" href="RM0000044WM025X"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the speakers.</ptxt>
<test2>
<ptxt>Inspect the front No. 1 speaker assembly (See page <xref label="Seep03" href="RM000002MK501FX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the front No. 2 speaker assembly (See page <xref label="Seep04" href="RM000003AW600NX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the front No. 3 speaker assembly (See page <xref label="Seep05" href="RM000000VEM023X"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the front No. 4 speaker assembly (See page <xref label="Seep06" href="RM000000VEM023X"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the rear speaker set (See page <xref label="Seep07" href="RM000002MK901SX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the rear No. 2 speaker assembly (for 5 Doors: See page <xref label="Seep08" href="RM000002MK901SX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the rear header speaker assembly (See page <xref label="Seep09" href="RM000002ZX401EX"/>).</ptxt>
</test2>
<test2>
<ptxt>Inspect the No. 1 speaker with box assembly.</ptxt>
<list1 type="unordered">
<item>
<ptxt>for 5 Doors: (See page <xref label="Seep10" href="RM000002ZX401EX"/>).</ptxt>
</item>
<item>
<ptxt>for 3 Doors: (See page <xref label="Seep11" href="RM000002ZX401EX"/>).</ptxt>
</item>
</list1>
</test2>
</test1>
<test1>
<ptxt>Inspect the steering pad switch assembly (See page <xref label="Seep12" href="RM000003C4V012X"/>).</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK CIRCUIT</testtitle>
<test1>
<ptxt>Adjust, repair or replace as necessary.</ptxt>
</test1>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>RECHECK FOR DIAGNOSTIC TROUBLE CODE</testtitle>
<atten4>
<ptxt>After clearing DTCs, recheck for DTCs.</ptxt>
</atten4>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PERFORM CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>