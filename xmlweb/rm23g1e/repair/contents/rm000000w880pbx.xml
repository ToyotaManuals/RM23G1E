<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94H_T00E5" variety="T00E5">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1KD-FTV)</name>
<para id="RM000000W880PBX" category="C" type-id="302GG" name-id="AT61R-03" from="201207" to="201210">
<dtccode>P0724</dtccode>
<dtcname>Brake Switch "B" Circuit High</dtcname>
<subpara id="RM000000W880PBX_01" type-id="60" category="03" proc-id="RM23G0E___000084R00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The purpose of this circuit is to prevent the engine from stalling while driving in lock-up when the brakes are suddenly applied.</ptxt>
<ptxt>When the brake pedal is depressed, this switch sends a signal to the TCM. Then the TCM cancels the operation of the lock-up clutch while braking is in progress.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0724</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch remains on even when the vehicle is driven in a stop (less than 3 km/h (2 mph)) and go (30 km/h (19 mph) or more) pattern 5 times (2-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in stop light switch signal circuit</ptxt>
</item>
<item>
<ptxt>Stop light switch</ptxt>
</item>
<item>
<ptxt>TCM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W880PBX_02" type-id="64" category="03" proc-id="RM23G0E___000084S00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates that the stop light switch remains on. When the stop light switch remains on during stop and go driving, the TCM interprets this as a fault in the stop light switch. Then the MIL illuminates and the TCM stores the DTC. The vehicle must stop (less than 3 km/h (2 mph)) and go (30 km/h (19 mph) or more) 5 times for 2 driving cycles in order for the DTC to be stored.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W880PBX_06" type-id="32" category="03" proc-id="RM23G0E___000084T00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C146704E14" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W880PBX_07" type-id="51" category="05" proc-id="RM23G0E___000084U00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
</step2>
</step1>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Brake pedal depressed</ptxt>
</item>
<item>
<ptxt>OFF: Brake pedal released</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W880PBX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W880PBX_08_0003" proc-id="RM23G0E___000084V00000">
<testtitle>INSPECT STOP LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the A5 stop light switch.</ptxt>
<figure>
<graphic graphicname="C171632E08" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL1" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.39in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
<ptxt>(pedal released)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 - 4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
<ptxt>(pedal depressed)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
<ptxt>(pedal depressed)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 - 4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
<ptxt>(pedal released)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Stop Light Switch)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not pushed</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pushed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W880PBX_08_0004" fin="false">OK</down>
<right ref="RM000000W880PBX_08_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W880PBX_08_0004" proc-id="RM23G0E___000084W00000">
<testtitle>CHECK HARNESS AND CONNECTOR (TCM - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G70 TCM connector.</ptxt>
<figure>
<graphic graphicname="C212049E03" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL1" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.39in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>G70-14 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>G70-14 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to TCM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W880PBX_08_0005" fin="true">OK</down>
<right ref="RM000000W880PBX_08_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W880PBX_08_0005">
<testtitle>REPLACE TCM<xref label="Seep01" href="RM0000048F3006X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W880PBX_08_0007">
<testtitle>REPLACE STOP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM000003QJT00TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W880PBX_08_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>