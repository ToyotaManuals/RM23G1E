<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9ER_T00OF" variety="T00OF">
<name>AIR CONDITIONING AMPLIFIER</name>
<para id="RM0000039R5016X" category="A" type-id="80001" name-id="ACC61-02" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM0000039R5016X_02" type-id="11" category="10" proc-id="RM23G0E___0000HU100000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039R5016X_01" type-id="01" category="01">
<s-1 id="RM0000039R5016X_01_0001" proc-id="RM23G0E___0000HTZ00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039R5016X_01_0021" proc-id="RM23G0E___0000B9W00000">
<ptxt>REMOVE DOOR SCUFF PLATE ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E200370E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the door scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 4 clips, 10 claws and 2 guides and remove the door scuff plate.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039R5016X_01_0022" proc-id="RM23G0E___0000B9X00000">
<ptxt>REMOVE COWL SIDE TRIM BOARD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E200371" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Detach the clip and claw and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039R5016X_01_0023" proc-id="RM23G0E___00007C000000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 clips and 2 guides and remove the No. 2 instrument panel under cover.</ptxt>
<figure>
<graphic graphicname="B298555E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039R5016X_01_0024" proc-id="RM23G0E___00008CQ00000">
<ptxt>REMOVE INSTRUMENT SIDE PANEL RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238212E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the instrument side panel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 5 clips, claw, and 3 guides.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039R5016X_01_0025" proc-id="RM23G0E___00007BW00000">
<ptxt>REMOVE INSTRUMENT PANEL ORNAMENT
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238217E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the instrument panel ornament.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 5 clips and remove the instrument panel ornament.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039R5016X_01_0026" proc-id="RM23G0E___00007BX00000">
<ptxt>REMOVE GLOVE COMPARTMENT DOOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238219E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts &lt;C&gt; and 2 screws &lt;A&gt; or &lt;B&gt;.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Screw</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 5 clips and claw.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and remove the glove compartment door.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039R5016X_01_0027" proc-id="RM23G0E___00007BY00000">
<ptxt>REMOVE NO. 2 AIR DUCT SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="E204225" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the No. 2 air duct sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039R5016X_01_0017" proc-id="RM23G0E___00007BZ00000">
<ptxt>REMOVE ECU INTEGRATION BOX RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240503" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Disconnect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 nuts, bolt and ECU integration box RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039R5016X_01_0010" proc-id="RM23G0E___0000HU000000">
<ptxt>REMOVE AIR CONDITIONER AMPLIFIER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240505" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and remove the air conditioner amplifier assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>