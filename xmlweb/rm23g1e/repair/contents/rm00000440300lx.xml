<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B98Z_T00IN" variety="T00IN">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM00000440300LX" category="J" type-id="803DS" name-id="AV9PE-02" from="201207" to="201210">
<dtccode/>
<dtcname>Speaker Circuit (Independent Amplifier)</dtcname>
<subpara id="RM00000440300LX_01" type-id="60" category="03" proc-id="RM23G0E___0000BO300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The stereo component amplifier assembly sends sound signals to the speaker.</ptxt>
</content5>
</subpara>
<subpara id="RM00000440300LX_02" type-id="32" category="03" proc-id="RM23G0E___0000BO400000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E199615E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000440300LX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000440300LX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000440300LX_04_0001" proc-id="RM23G0E___0000BO500000">
<testtitle>CHECK SPEAKER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check which speakers are malfunctioning.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Malfunction in front speaker area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Malfunction in rear speaker area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>All speakers do not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0002" fin="false">A</down>
<right ref="RM00000440300LX_04_0009" fin="false">B</right>
<right ref="RM00000440300LX_04_0017" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0002" proc-id="RM23G0E___0000BO600000">
<testtitle>CHECK FRONT SIDE SPEAKER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check which speakers are malfunctioning.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Front No. 1 speaker assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front No. 3 speaker assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front No. 2 speaker assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front No. 4 speaker assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0003" fin="false">A</down>
<right ref="RM00000440300LX_04_0023" fin="false">B</right>
<right ref="RM00000440300LX_04_0025" fin="false">C</right>
<right ref="RM00000440300LX_04_0027" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0003" proc-id="RM23G0E___0000BO700000">
<testtitle>INSPECT FRONT NO. 1 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H10*1 and/or H13*2 front No. 1 speaker assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for RH</ptxt>
</item>
<item>
<ptxt>*2: for LH</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="E144966" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.0 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0007" fin="false">OK</down>
<right ref="RM00000440300LX_04_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0007" proc-id="RM23G0E___0000BO800000">
<testtitle>CHECK HARNESS AND CONNECTOR (FRONT NO. 1 SPEAKER - FRONT NO. 3 SPEAKER)</testtitle>
<content6 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>*1: for RH</ptxt>
</item>
<item>
<ptxt>*2: for LH</ptxt>
</item>
</list1>
<test1>
<ptxt>Disconnect the H10*1 and/or H13*2 front No. 1 speaker assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the H21*1 and/or H22*2 front No. 3 speaker assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for RH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H10-2 - H21-3 (+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H10-1 - H21-4 (-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H10-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H10-2  - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H13-2 - H22-3 (+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H13-1 - H22-4 (-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H13-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H13-2  - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0032" fin="false">OK</down>
<right ref="RM00000440300LX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0032" proc-id="RM23G0E___0000BOH00000">
<testtitle>INSPECT FRONT NO. 3 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H21*1 and/or H22*2 front No. 3 speaker assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for RH</ptxt>
</item>
<item>
<ptxt>*2: for LH</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="E180660E17" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for RH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (TWR+) - 3 (+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (TWR-) - 4 (-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (TWL+) - 3 (+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (TWL-) - 4 (-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0017" fin="true">OK</down>
<right ref="RM00000440300LX_04_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0023" proc-id="RM23G0E___0000BOA00000">
<testtitle>INSPECT FRONT NO. 3 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the front No. 3 speaker assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM000003AW800VX"/>)</ptxt>
</test1>
<test1>
<ptxt>Check that the speaker sounds normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0019" fin="true">OK</down>
<right ref="RM00000440300LX_04_0024" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0024" proc-id="RM23G0E___0000BOB00000">
<testtitle>CHECK HARNESS AND CONNECTOR (STEREO COMPONENT AMPLIFIER - FRONT NO. 3 SPEAKER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H7 stereo component amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the H21*1 and/or H22*2 front No. 3 speaker assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for RH</ptxt>
</item>
<item>
<ptxt>*2: for LH</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for RH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H7-2 (FR+) - H21-1 (TWR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-6 (FR-) - H21-2 (TWR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-2 (FR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-6 (FR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H7-1 (FL+) - H22-1 (TWL+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-5 (FL-) - H22-2 (TWL-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-1 (FL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-5 (FL-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0017" fin="true">OK</down>
<right ref="RM00000440300LX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0025" proc-id="RM23G0E___0000BOC00000">
<testtitle>INSPECT FRONT NO. 2 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the J4*1 and/or K3*2 front No. 2 speaker assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for RH</ptxt>
</item>
<item>
<ptxt>*2: for LH</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="E199617" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.0 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0026" fin="false">OK</down>
<right ref="RM00000440300LX_04_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0026" proc-id="RM23G0E___0000BOD00000">
<testtitle>CHECK HARNESS AND CONNECTOR (STEREO COMPONENT AMPLIFIER - FRONT NO. 2 SPEAKER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H12 stereo component amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the J4*1 and/or K3*2 front No. 2 speaker assembly connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for RH</ptxt>
</item>
<item>
<ptxt>*2: for LH</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for RH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H12-10 (WFR+) - J4-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-9 (WFR-) - J4-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-10 (WFR+)  - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-9 (WFR-)  - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H12-3 (WFL+) - K3-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-8 (WFL-) - K3-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-3 (WFL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-8 (WFL-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0017" fin="true">OK</down>
<right ref="RM00000440300LX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0027" proc-id="RM23G0E___0000BOE00000">
<testtitle>INSPECT FRONT NO. 4 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H14 front No. 4 speaker assembly connector.</ptxt>
<figure>
<graphic graphicname="E199617" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6.0 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0028" fin="false">OK</down>
<right ref="RM00000440300LX_04_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0028" proc-id="RM23G0E___0000BOF00000">
<testtitle>CHECK HARNESS AND CONNECTOR (STEREO COMPONENT AMPLIFIER - FRONT NO. 4 SPEAKER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H7 stereo component amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the H14 front No. 4 speaker assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H7-8 (CTR+) - H14-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-7 (CTR-) - H14-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-8 (CTR+)  - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H7-7 (CTR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0017" fin="true">OK</down>
<right ref="RM00000440300LX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0009" proc-id="RM23G0E___0000BO900000">
<testtitle>INSPECT REAR NO. 1 SPEAKER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for 5 Door:</ptxt>
<test2>
<ptxt>Disconnect the L1*1 and/or M1*2 rear No. 1 speaker assembly connector.</ptxt>
<figure>
<graphic graphicname="E144966" width="2.775699831in" height="1.771723296in"/>
</figure>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.0 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>for 3 Door:</ptxt>
<test2>
<ptxt>Disconnect the Q32*1 and/or R37*2 rear No. 1 speaker assembly connector.</ptxt>
<figure>
<graphic graphicname="E144966" width="2.775699831in" height="1.771723296in"/>
</figure>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.0 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0029" fin="false">OK</down>
<right ref="RM00000440300LX_04_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0029" proc-id="RM23G0E___0000BOG00000">
<testtitle>CHECK HARNESS AND CONNECTOR (STEREO COMPONENT AMPLIFIER - REAR NO. 1 SPEAKER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H12 stereo component amplifier assembly connector.</ptxt>
</test1>
<test1>
<ptxt>for 5 Door:</ptxt>
<ptxt>Disconnect the L1*1 and/or M1*2 rear No. 1 speaker assembly connector.</ptxt>
</test1>
<test1>
<ptxt>for 3 Door:</ptxt>
<ptxt>Disconnect the Q32*1 and/or R37*2 rear No. 1 speaker assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for RH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H12-5 (RR+) - L1-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-12 (RR-) - L1-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-5 (RR+) - Q32-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-12 (RR-) - Q32-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-5 (RR+)  - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-12 (RR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H12-4 (RL+) - M1-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-11 (RL-) - M1-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-4 (RL+) - R37-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-11 (RL-) - R37-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-4 (RL+)  - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H12-11 (RL-)  - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000440300LX_04_0017" fin="true">OK</down>
<right ref="RM00000440300LX_04_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000440300LX_04_0017">
<testtitle>REPLACE STEREO COMPONENT AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM000003AI901AX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000440300LX_04_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000440300LX_04_0015">
<testtitle>REPLACE FRONT NO. 1 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000002MK701TX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000440300LX_04_0019">
<testtitle>REPLACE FRONT NO. 3 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000003AW800VX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000440300LX_04_0030">
<testtitle>REPLACE FRONT NO. 2 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000000VEO02MX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000440300LX_04_0031">
<testtitle>REPLACE FRONT NO. 4 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000000VEO02MX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000440300LX_04_0020">
<testtitle>REPLACE REAR NO. 1 SPEAKER ASSEMBLY<xref label="Seep01" href="RM000002MKB029X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>