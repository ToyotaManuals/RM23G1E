<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B99P_T00JD" variety="T00JD">
<name>CLEARANCE WARNING ECU (for LHD)</name>
<para id="RM00000466V01DX" category="A" type-id="30014" name-id="PM4CS-03" from="201210">
<name>INSTALLATION</name>
<subpara id="RM00000466V01DX_01" type-id="01" category="01">
<s-1 id="RM00000466V01DX_01_0001" proc-id="RM23G0E___0000CKP00001">
<ptxt>INSTALL CLEARANCE WARNING ECU ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the clearance warning ECU with the bolt.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466V01DX_01_0010" proc-id="RM23G0E___00007BJ00001">
<ptxt>INSTALL GLOVE COMPARTMENT DOOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238219E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect each connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 5 clips and claw to install the glove compartment door.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts &lt;C&gt; and 2 screws &lt;A&gt; or &lt;B&gt;.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Screw</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000466V01DX_01_0011" proc-id="RM23G0E___00007BI00000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 clips and 2 guides to install the No. 2 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
<figure>
<graphic graphicname="B298555E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000466V01DX_01_0012" proc-id="RM23G0E___00007BK00001">
<ptxt>INSTALL INSTRUMENT PANEL ORNAMENT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 clips to install the instrument panel ornament.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466V01DX_01_0013" proc-id="RM23G0E___00008CA00001">
<ptxt>INSTALL INSTRUMENT SIDE PANEL RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 5 clips, claw and 3 guides to install the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466V01DX_01_0014" proc-id="RM23G0E___0000B9Q00001">
<ptxt>INSTALL COWL SIDE TRIM BOARD RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and claw to install the cowl side trim board.</ptxt>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466V01DX_01_0015" proc-id="RM23G0E___0000B9R00001">
<ptxt>INSTALL DOOR SCUFF PLATE ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 clips, 10 claws and 2 guides to install the door scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466V01DX_01_0008" proc-id="RM23G0E___0000CKQ00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="2">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000466V01DX_01_0009" proc-id="RM23G0E___0000CKR00001">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0INX"/>).  </ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>