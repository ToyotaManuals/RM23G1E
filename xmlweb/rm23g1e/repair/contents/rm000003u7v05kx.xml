<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000003U7V05KX" category="C" type-id="800K3" name-id="ESZK3-01" from="201207" to="201210">
<dtccode>P1604</dtccode>
<dtcname>Startability Malfunction</dtcname>
<subpara id="RM000003U7V05KX_01" type-id="60" category="03" proc-id="RM23G0E___00002PX00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored if the engine does not start or continues to crank without starting for certain period of time. This DTC is also stored if the vehicle has run out of fuel. It is necessary to check whether there was enough fuel in the fuel tank before inspection.</ptxt>
<table pgwide="1">
<title>P1604</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>After engine is started</ptxt>
</entry>
<entry valign="middle">
<list1 type="nonmark">
<item>
<ptxt>Both conditions are met for 2 seconds or more*1 (1 trip detection logic):</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>STA signal is input to the ECM.</ptxt>
</item>
<item>
<ptxt>Engine speed is 500 rpm or less.</ptxt>
</item>
</list2>
</list1>
<ptxt>*1: The time required for DTC detection may be as much as 30 seconds depending on the engine coolant temperature and atmospheric pressure.</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery</ptxt>
</item>
<item>
<ptxt>Entry and start system</ptxt>
</item>
<item>
<ptxt>Engine immobiliser system</ptxt>
</item>
<item>
<ptxt>Irregular fuel</ptxt>
</item>
<item>
<ptxt>Lack of fuel</ptxt>
</item>
<item>
<ptxt>Suction control valve</ptxt>
</item>
<item>
<ptxt>Injector assembly</ptxt>
</item>
<item>
<ptxt>Fuel filter element sub-assembly</ptxt>
</item>
<item>
<ptxt>Glow system</ptxt>
</item>
<item>
<ptxt>Injector driver</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor</ptxt>
</item>
<item>
<ptxt>Intake system</ptxt>
</item>
<item>
<ptxt>Exhaust system</ptxt>
</item>
<item>
<ptxt>Engine damaged, seized up</ptxt>
</item>
<item>
<ptxt>Loss of compression</ptxt>
</item>
<item>
<ptxt>Pressure discharge valve</ptxt>
</item>
<item>
<ptxt>Fuel pressure sensor</ptxt>
</item>
<item>
<ptxt>EGR system</ptxt>
</item>
<item>
<ptxt>Diesel throttle system</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>After starting the engine (engine speed is 500 rpm or more), the engine speed drops to 200 rpm or less within 2 seconds*2 (1 trip detection logic).</ptxt>
<ptxt>*2: This DTC may be stored when the engine stalls due to clutch misoperation.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P1604</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>MAP</ptxt>
</item>
<item>
<ptxt>MAF</ptxt>
</item>
<item>
<ptxt>Intake Air</ptxt>
</item>
<item>
<ptxt>Coolant Temp</ptxt>
</item>
<item>
<ptxt>Battery Voltage</ptxt>
</item>
<item>
<ptxt>Starter Signal</ptxt>
</item>
<item>
<ptxt>Engine Speed of Cyl #1 (to #4)</ptxt>
</item>
<item>
<ptxt>Immobiliser Communication</ptxt>
</item>
<item>
<ptxt>Target Common Rail Pressure</ptxt>
</item>
<item>
<ptxt>Fuel Press</ptxt>
</item>
<item>
<ptxt>Common Rail Pres Sens 2</ptxt>
</item>
<item>
<ptxt>Target Pump SCV Current</ptxt>
</item>
<item>
<ptxt>Injection Feedback Val #1 (to #4)</ptxt>
</item>
<item>
<ptxt>Injection Volume</ptxt>
</item>
<item>
<ptxt>Actual Throttle Position</ptxt>
</item>
<item>
<ptxt>Throttle Motor Duty #1</ptxt>
</item>
<item>
<ptxt>Target EGR Position</ptxt>
</item>
<item>
<ptxt>Actual EGR Valve Pos.</ptxt>
</item>
<item>
<ptxt>EGR Close Lrn. Status</ptxt>
</item>
<item>
<ptxt>EGR Close Lrn. Val.</ptxt>
</item>
<item>
<ptxt>Glow Control Unit Duty</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003U7V05KX_02" type-id="51" category="05" proc-id="RM23G0E___00002PY00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>In contrast to normal malfunction diagnosis for components, circuits and systems, DTC P1604 is used to determine the malfunctioning area from the problem symptoms and freeze frame data when the user mentions problems such as starting difficulty.</ptxt>
<ptxt>As these DTCs can be stored as a result of certain user actions, even if these DTCs are output, if the customer makes no mention of problems, clear these DTCs without performing any troubleshooting and return the vehicle to the customer.</ptxt>
</item>
<item>
<ptxt>When confirming the freeze frame data, be sure to check all 5 data sets of freeze frame data.</ptxt>
</item>
<item>
<ptxt>The fourth set of freeze frame data is the data recorded when the DTC is stored.</ptxt>
<figure>
<graphic graphicname="A103809E51" width="2.775699831in" height="2.775699831in"/>
</figure>
</item>
</list1>
</atten4>
<step1>
<ptxt>In order to start the engine, the starting system, glow system, fuel system and the components related to compression must be functioning properly.</ptxt>
</step1>
<step1>
<ptxt>The cause of the problem can be narrowed down by checking if the engine cranks normally.</ptxt>
</step1>
<step1>
<ptxt>Starting system (when cranking is abnormal)</ptxt>
<step2>
<ptxt>The engine cannot be started if a sufficient cranking speed cannot be obtained. The following are possible causes of an insufficient cranking speed.</ptxt>
<step3>
<ptxt>Problem with the entry and start system (Engine does not crank).</ptxt>
</step3>
<step3>
<ptxt>Problem with the immobiliser system (Engine does not crank).</ptxt>
</step3>
<step3>
<ptxt>Problem with the battery (Cranking speed is low).</ptxt>
</step3>
<step3>
<ptxt>Problem with the starter (Cranking speed is low).</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>Glow system (when cranking is normal)</ptxt>
<step2>
<ptxt>When there is a problem with the glow system, the intake air temperature does not rise adequately and the fuel combustion temperature is not reached. Therefore, there is no initial combustion or it takes time for the engine to start. The following are possible causes.</ptxt>
<step3>
<ptxt>Engine coolant temperature sensor malfunction.</ptxt>
</step3>
<step3>
<ptxt>Glow plug malfunction.</ptxt>
<atten4>
<ptxt>If the glow plug has deteriorated, the engine may be less likely to start when the outside temperature is lower than 0°C (32°F).</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Glow control unit malfunction.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>Fuel system (when cranking is normal)</ptxt>
<step2>
<ptxt>The engine cannot be started if fuel is not supplied. A minimum fuel pressure of 25000 kPa or higher must be supplied to start the engine. The following are possible causes of insufficient fuel pressure.</ptxt>
<step3>
<ptxt>Fuel line clog.</ptxt>
</step3>
<step3>
<ptxt>Lack of fuel.</ptxt>
</step3>
<step3>
<ptxt>Fuel frozen.</ptxt>
</step3>
<step3>
<ptxt>Low quality fuel.</ptxt>
</step3>
<step3>
<ptxt>Air in fuel pipe.</ptxt>
</step3>
<step3>
<ptxt>Fuel filter element sub-assembly clog.</ptxt>
</step3>
<step3>
<ptxt>Problem with fuel supply pump assembly.</ptxt>
</step3>
<step3>
<ptxt>Problem with common rail assembly.</ptxt>
</step3>
<step3>
<ptxt>Problem with injector assemblies.</ptxt>
</step3>
</step2>
</step1>
<figure>
<graphic graphicname="A244277E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<step1>
<ptxt>Engine assembly</ptxt>
<step2>
<ptxt>There may be a problem with the engine unit itself if there is no problem with cranking, the glow system or fuel system.</ptxt>
<step3>
<ptxt>Engine friction too high.</ptxt>
</step3>
<step3>
<ptxt>Insufficient compression.</ptxt>
</step3>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000003U7V05KX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003U7V05KX_03_0004" proc-id="RM23G0E___00002Q000000">
<testtitle>READ OUTPUT DTC (RECORD STORED DTC AND FREEZE FRAME DATA) (PROCEDURE 1)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC. </ptxt>
</test1>
<test1>
<ptxt>Record the stored DTCs and freeze frame data. </ptxt>
<atten4>
<ptxt>Freeze frame data shows the actual engine conditions when engine starting trouble occurred.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003U7V05KX_03_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003U7V05KX_03_0001" proc-id="RM23G0E___00002PZ00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P1604)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P1604 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P1604 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than DTC P1604 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003U7V05KX_03_0005" fin="false">A</down>
<right ref="RM000003U7V05KX_03_0003" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003U7V05KX_03_0005" proc-id="RM23G0E___00002Q100000">
<testtitle>TAKE SNAPSHOT DURING STARTING AND IDLING (PROCEDURE 3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
<figure>
<graphic graphicname="A176220E18" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data.</ptxt>
</test1>
<test1>
<ptxt>Take a snapshot of the following Data List items with the intelligent tester during "engine switch on (IG) (5 seconds) → Starting → Idling (10 seconds)".</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Snapshot Button</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003U7V05KX_03_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003U7V05KX_03_0006" proc-id="RM23G0E___00002Q200000">
<testtitle>DETERMINE CAUSE OF PROBLEM (CHECK FREEZE FRAME DATA AND SNAPSHOT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Determine the cause of the problem based on the freeze frame data recorded in procedure 1 and the Data List values recorded when starting the engine in procedure 3.</ptxt>
<table pgwide="1">
<title>Coolant Temp</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Coolant Temp" in freeze frame data is below 35°C (95°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine starting trouble which occurred with cold engine</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>The engine coolant temperature is 60 to 90°C (140 to 194°F) after warming up the engine.</ptxt>
</item>
<item>
<ptxt>Normally, "Coolant Temp" is the same as the outside air 	temperature 	after an overnight engine soak.</ptxt>
</item>
<item>
<ptxt>Engine starting trouble with a cold engine occurs when the engine coolant temperature sensor is malfunctioning.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Coolant Temp" in freeze frame data is below 35°C (95°F).</ptxt>
<ptxt>Currently, no engine starting trouble (cranking time less than 4 seconds) after warming up engine ("Coolant Temp" is 60°C (140°F) or higher).</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine starting trouble which only occurs with cold engine</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Engine Speed</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Engine Speed" in freeze frame data is less than 120 rpm</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Engine starting trouble may have occurred because engine speed was too low</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>During cranking: 120 to 400 rpm</ptxt>
</item>
<item>
<ptxt>Idling with warm engine: 610 to 710 rpm*1</ptxt>
</item>
<item>
<ptxt>Idling with warm engine: 650 to 750 rpm*2</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>The battery may be fully depleted or the battery terminals may be loose.</ptxt>
</item>
<item>
<ptxt>The viscosity of the engine oil may be inappropriate (low viscosity oil is not used).</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Engine Speed" in Data List is less than 120 rpm when cranking engine</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*1: for Manual Transaxle</ptxt>
<ptxt>*2: for Automatic Transaxle</ptxt>
<table pgwide="1">
<title>Battery Voltage</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Battery Voltage" in freeze frame data is below 6 V</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Engine starting trouble may have occurred because battery is fully depleted</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>During cranking: 6 V or higher</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>The battery may be fully depleted or the battery terminals may be loose.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Battery Voltage" in Data List is below 6 V when cranking engine</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Fuel Press</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC4" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Fuel Press" in freeze frame data is below 10000 kPa</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Problem supplying fuel to fuel supply pump assembly (low pressure side)</ptxt>
<list1 type="unordered">
<item>
<ptxt>Ran out of fuel, air in fuel, fuel frozen (in this case, values of "Fuel Temperature" and "Coolant Temp" in freeze frame data are low)</ptxt>
</item>
<item>
<ptxt>Fuel filter clog, fuel line clog (low pressure side) or fuel leak</ptxt>
</item>
<item>
<ptxt>Feed pump (in fuel supply pump assembly) malfunctioning</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle">
<ptxt>When in a stable condition such as when the engine is idling after being warmed up, the fuel pressure is within +/-5000 kPa of the target fuel pressure</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Disconnect the inlet hose from the fuel supply pump assembly (low pressure side), operate the hand pump and check that fuel is being supplied.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel Press is less than 10000 kPa from the value of Fuel Press when cranking started</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Fuel Press" in freeze frame data is below 25000 kPa</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Problem supplying fuel to fuel supply pump assembly (low pressure side) or problem on high pressure side</ptxt>
<list1 type="unordered">
<item>
<ptxt>Fuel supply pump assembly (SCV operation malfunction)</ptxt>
</item>
<item>
<ptxt>common rail Assembly</ptxt>
</item>
<item>
<ptxt>Fuel injector (if glow plug is covered with fuel when removed, injector of corresponding cylinder may be stuck open)</ptxt>
</item>
<item>
<ptxt>Fuel line clog (high pressure side), fuel leak</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle">
<ptxt>When in a stable condition such as when the engine is idling after being warmed up, the fuel pressure is within +/-5000 kPa of the target fuel pressure</ptxt>
<atten4>
<ptxt>In order to maintain the injection amount before an engine stall, the target common rail pressure is increased drastically. At this time, Fuel Press may not follow correctly, but this is normal behavior and not a malfunction.</ptxt>
</atten4>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>For startup at least 25000 kPa of fuel pressure is needed (Take care there is a response lag when the pressure rise)</ptxt>
</item>
<item>
<ptxt>If the fuel pressure is 10000 kPa or lower (when starting with an engine coolant temperature of 0°C (0°F) or lower, 15000 kPa or lower), fuel injection is stopped.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel Press is below Target Common Rail Pressure by 15000 kPa or more</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel Press increases to a value that is higher than Target Common Rail Pressure immediately after cranking, and, remains higher than Target Common Rail Pressure</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Injector (fuel injection problem caused by air in injector)</ptxt>
</item>
<item>
<ptxt>Problem with injection system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When in a stable condition such as when the engine is idling after being warmed up, the fuel pressure is within +/-5000 kPa of the target fuel pressure</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>If there is air in the injectors for all cylinders, fuel cannot be injected.</ptxt>
</item>
<item>
<ptxt>If there is an injection system problem related to 2 or more cylinders, fuel delivery and injection control are stopped.</ptxt>
</item>
<item>
<ptxt>When the EDU circuit has a malfunction (e.g. an EDU relay or fuse malfunction), the ECM cannot perform diagnosis of malfunctions and DTCs are not stored as voltage is unstable during cranking. Also, an excess amount of fuel is supplied by the fuel supply pump assembly and "Fuel Press" becomes larger than "Target Common Rail Pressure" as fuel injection cannot be performed even though a fuel injection signal is output due to a failure to detect malfunctions at engine start.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Injection Feedback Val #1(to #4)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC6" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Injection Feedback Val #1 (to #4)" in freeze frame data is more than 3.0 mm<sup>3</sup>/st</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Injector assembly malfunction or compression problem</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>-3.0 to 3.0 mm<sup>3</sup>/st</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>If an injector assembly is malfunctioning, even though the engine starts, idling is rough.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Injection Feedback Val #1 (to #4)" in Data List is more than 3.0 mm<sup>3</sup>/ st when idling</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Injection Volume</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC6" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Injection Volume" in Data List is more than 10 mm<sup>3</sup>/st and "Injection Feedback Val #1 (to #4)" is within range of 3.0 mm<sup>3</sup>/st when idling after warming up engine</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector assemblies of all cylinders malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Main Injection Period</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COLSPEC8" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Main Injection Period" in freeze frame data or Data List is 0 μs</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Fuel supply pump assembly (when SCV has open or short circuit, fuel supply from pump stops)</ptxt>
</item>
<item>
<ptxt>Problem with injection system (injector circuit is open (2 cylinders or more) or injector driver is malfunctioning)</ptxt>
</item>
<item>
<ptxt>Problem with engine immobiliser system</ptxt>
</item>
<item>
<ptxt>Engine speed 60 rpm or less</ptxt>
</item>
<item>
<ptxt>ECM malfunction</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Indicates that fuel injection control has stopped.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Immobiliser Communication</title>
<tgroup cols="4">
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC9" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Immobiliser Communication" in freeze frame data or Data List is OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine immobiliser system</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel injection has stopped due to engine immobiliser system.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Immobiliser Fuel Cut History</title>
<tgroup cols="4">
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC9" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Immobiliser Fuel Cut History" in freeze frame data is OFF to ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel injection has stopped due to engine immobiliser system.</ptxt>
<atten4>
<ptxt>"Main Injection Period" in freeze frame data is 0 μs</ptxt>
</atten4>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Inspect engine immobiliser system.</ptxt>
</item>
<item>
<ptxt>Inspection looseness in battery terminal.</ptxt>
</item>
<item>
<ptxt>Inspect connection condition of ECM power supply system wiring.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Stop Light Switch</title>
<tgroup cols="4">
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC9" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Judgment of Data List Values</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Problem Cause</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnosis Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"Stop Light Switch" in freeze frame data is OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starting problems due to the brake pedal being released during cranking causing the power to the starter to be cut off and cranking to be insufficient.</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>If there are no contact problems with the stop light switch signal circuit, the problem was probably due to operation by the user.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003U7V05KX_03_0002" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003U7V05KX_03_0002">
<testtitle>ENGINE DIFFICULT TO START OR STALLING<xref label="Seep01" href="RM000000TJ509SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003U7V05KX_03_0003">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW03MX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>