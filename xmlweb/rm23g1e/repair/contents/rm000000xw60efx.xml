<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000000XW60EFX" category="C" type-id="803KU" name-id="BCB3C-02" from="201207" to="201210">
<dtccode>C1241</dtccode>
<dtcname>Low Power Supply Voltage Malfunction</dtcname>
<subpara id="RM000000XW60EFX_01" type-id="60" category="03" proc-id="RM23G0E___0000A3000000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If a malfunction is detected in the power supply circuit, the skid control ECU (housed in the brake actuator assembly) stores this DTC and the fail-safe function prohibits ABS operation.</ptxt>
<ptxt>This DTC is stored when the IG1 terminal voltage deviates from the DTC detection condition due to a malfunction in the power supply or charging circuit, such as the battery or generator circuit, etc.</ptxt>
<ptxt>The DTC is cleared when the IG1 terminal voltage returns to normal.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.92in"/>
<colspec colname="COL2" colwidth="3.09in"/>
<colspec colname="COL3" colwidth="3.07in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1241</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>At a vehicle speed of 3 km/h (2 mph) or more, the IG1 terminal voltage is below 9.5 V for 10 seconds or more.</ptxt>
</item>
<item>
<ptxt>When the solenoid relay remains on and the IG1 terminal voltage is below 9.5 V, the relay contact is open for 0.22 seconds or more.</ptxt>
</item>
<item>
<ptxt>With the IG1 terminal voltage at 9.5 V or less, the vehicle speed sensor power supply decreases for 60 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ECU-IG NO. 1 fuse</ptxt>
</item>
<item>
<ptxt>Battery</ptxt>
</item>
<item>
<ptxt>Charging system</ptxt>
</item>
<item>
<ptxt>Power source circuit</ptxt>
</item>
<item>
<ptxt>Internal power supply circuit of the skid control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XW60EFX_02" type-id="32" category="03" proc-id="RM23G0E___0000A3100000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C194309E05" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XW60EFX_03" type-id="51" category="05" proc-id="RM23G0E___0000A3200000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08IX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XW60EFX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XW60EFX_04_0003" proc-id="RM23G0E___0000A1500000">
<testtitle>CHECK TERMINAL VOLTAGE (IG1)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C199357E08" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A6-34 (IG1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XW60EFX_04_0014" fin="false">OK</down>
<right ref="RM000000XW60EFX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XW60EFX_04_0014" proc-id="RM23G0E___0000A1600000">
<testtitle>CHECK HARNESS AND CONNECTOR (GND1 TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A6-1 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000000XW60EFX_04_0005" fin="false">OK</down>
<right ref="RM000000XW60EFX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XW60EFX_04_0005" proc-id="RM23G0E___0000A3300000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0EZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 20 km/h (12 mph) or more for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM000000XHV0EZX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XW60EFX_04_0006" fin="true">A</down>
<right ref="RM000000XW60EFX_04_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XW60EFX_04_0006">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XW60EFX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XW60EFX_04_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XW60EFX_04_0011">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01JX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>