<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002G" variety="S002G">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002G_7B9HZ_T00RN" variety="T00RN">
<name>ROOF DRIP SIDE FINISH MOULDING</name>
<para id="RM0000038LD00TX" category="A" type-id="30014" name-id="ET4A1-01" from="201207">
<name>INSTALLATION</name>
<subpara id="RM0000038LD00TX_01" type-id="11" category="10" proc-id="RM23G0E___0000JFR00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>When installing the roof drip side finish moulding clip, heat the vehicle body and clip using a heat light.</ptxt>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Roof Drip Side Finish Moulding Clip</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</item>
</list1>
</atten4>
<atten3>
<ptxt>Do not heat the vehicle body or clip excessively.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM0000038LD00TX_02" type-id="01" category="01">
<s-1 id="RM0000038LD00TX_02_0010" proc-id="RM23G0E___0000JFS00000">
<ptxt>INSTALL ROOF DRIP SIDE FINISH MOULDING CLIP</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>If reusing the clips, do not remove the double-sided tape remaining on the clips and where the clips will be installed on the body.</ptxt>
</item>
<item>
<ptxt>If installing new clips, remove the double-sided tape remaining where the clips will be installed on the body and clean the body with a non-residue solvent.</ptxt>
</item>
</list1>
</atten3>
<s2>
<figure>
<graphic graphicname="B187426E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Apply a 2 to 3 mm (0.0787 to 0.118 in.) bead of adhesive (3M DP-105 or equivalent) to new roof drip side finish moulding clips.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>2 to 3 mm Bead of Adhesive</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Adhesive strength (tensile strength): 13.7 MPa (140.0 kgf/cm<sup>2</sup>, 1991 psi) or more (when the temperature is 23°C (73°F))</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Apply primer to the body where the roof drip side finish moulding clip will be installed.</ptxt>
<atten3>
<ptxt>Do not let the primer dry.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>While using the position shown in the illustration as a reference, place the roof drip side finish moulding clip onto the roof panel. Then position and install the clip after lightly pressing it so that the adhesive adheres to the primer.</ptxt>
<figure>
<graphic graphicname="B236284E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Front Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Rear Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Reference Value</title>
<table pgwide="1">
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement</ptxt>
</entry>
<entry valign="middle">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>a</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 mm</ptxt>
<ptxt>0.157 in.</ptxt>
</entry>
<entry valign="middle">
<ptxt>b</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 mm</ptxt>
<ptxt>0.157 in.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 mm</ptxt>
<ptxt>0.433 in.</ptxt>
</entry>
<entry valign="middle">
<ptxt>d</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 mm</ptxt>
<ptxt>0.157 in.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>e</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 mm</ptxt>
<ptxt>0.157 in.</ptxt>
</entry>
<entry valign="middle">
<ptxt>f</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 mm</ptxt>
<ptxt>0.433 in.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Install the rear roof drip side finish moulding when 40 minutes or more have elapsed after pressing and installing the roof drip side finish moulding clips.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Initial hardening time: 40 minutes</ptxt>
</item>
<item>
<ptxt>Complete hardening time: 48 hours</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LD00TX_02_0011" proc-id="RM23G0E___0000JFT00000">
<ptxt>INSTALL ROOF DRIP SIDE FINISH MOULDING LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip to install the roof drip side finish moulding.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LD00TX_02_0012" proc-id="RM23G0E___0000JFU00000">
<ptxt>INSTALL CENTER NO. 2 ROOF DRIP SIDE FINISH MOULDING LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the center No. 2 roof drip side finish moulding.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LD00TX_02_0013" proc-id="RM23G0E___0000JFV00000">
<ptxt>INSTALL CENTER ROOF DRIP SIDE FINISH MOULDING LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the center roof drip side finish moulding.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LD00TX_02_0014" proc-id="RM23G0E___0000JFW00000">
<ptxt>INSTALL REAR ROOF DRIP SIDE FINISH MOULDING LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip to install the rear roof drip side finish moulding.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LD00TX_02_0016" proc-id="RM23G0E___0000JFX00000">
<ptxt>INSTALL ROOF DRIP SIDE FINISH MOULDING JOINT COVER LH (w/o Roof Rack)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure to install the roof drip side finish moulding joint cover on the other side.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 clamps to install the roof drip side finish moulding joint cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LD00TX_02_0017" proc-id="RM23G0E___0000JFY00000">
<ptxt>INSTALL ROOF RACK ASSEMBLY LH (w/ Roof Rack)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the roof rack (See page <xref label="Seep01" href="RM000003CIC00BX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>