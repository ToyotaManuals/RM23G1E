<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12034_S001K" variety="S001K">
<name>PARKING BRAKE</name>
<ttl id="12034_S001K_7B98D_T00I1" variety="T00I1">
<name>PARKING BRAKE CABLE</name>
<para id="RM0000011C801GX" category="A" type-id="80001" name-id="PB1N9-03" from="201207">
<name>REMOVAL</name>
<subpara id="RM0000011C801GX_03" type-id="01" category="01">
<s-1 id="RM0000011C801GX_03_0073" proc-id="RM23G0E___0000B2R00000">
<ptxt>DISCONNECT NO. 2 PARKING BRAKE SHOE ASSEMBLY WITH PARKING BRAKE SHOE LEVER</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Refer to the procedures up to "Remove No. 2 Parking Brake Shoe Assembly with Parking Brake Shoe Lever" (See page <xref label="Seep01" href="RM000001EDH010X"/>).</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000011C801GX_03_0091" proc-id="RM23G0E___0000B2S00000">
<ptxt>REMOVE FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>Remove the fuel tank sub-assembly (See page <xref label="Seep01" href="RM0000045EZ00PX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1KD-FTV, for 3 Door:</ptxt>
<ptxt>Remove the fuel tank sub-assembly (See page <xref label="Seep02" href="RM0000045BP004X"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1KD-FTV, for 5 Door:</ptxt>
<ptxt>Remove the fuel tank sub-assembly (See page <xref label="Seep03" href="RM0000045BM008X"/>).</ptxt>
</s2>
<s2>
<ptxt>for 2TR-FE, for 3 Door:</ptxt>
<ptxt>Remove the fuel tank sub-assembly (See page <xref label="Seep04" href="RM0000045E6004X"/>).</ptxt>
</s2>
<s2>
<ptxt>for 2TR-FE, for 5 Door:</ptxt>
<ptxt>Remove the fuel tank sub-assembly (See page <xref label="Seep05" href="RM0000045EZ00QX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 5L-E:</ptxt>
<ptxt>Remove the fuel tank sub-assembly (See page <xref label="Seep06" href="RM0000045BM009X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011C801GX_03_0092" proc-id="RM23G0E___0000B2T00000">
<ptxt>REMOVE CENTER EXHAUST PIPE ASSEMBLY (for 3 Door)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1KD-FTV:</ptxt>
<ptxt>Remove the center exhaust pipe assembly (See page <xref label="Seep01" href="RM00000456A00EX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 2TR-FE:</ptxt>
<ptxt>Remove the center exhaust pipe assembly (See page <xref label="Seep02" href="RM0000046ET005X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011C801GX_03_0072" proc-id="RM23G0E___0000B2Q00000">
<ptxt>REMOVE REAR CONSOLE BOX ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Automatic Transmission:</ptxt>
<ptxt>Remove the rear console box assembly (See page <xref label="Seep01" href="RM0000046JD004X"/>).</ptxt>
</s2>
<s2>
<ptxt>for Manual Transmission:</ptxt>
<ptxt>Remove the rear console box assembly (See page <xref label="Seep02" href="RM0000046N6003X"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Refrigerated Cool Box:</ptxt>
<ptxt>Remove the rear console box assembly (See page <xref label="Seep03" href="RM0000046MR009X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011C801GX_03_0056" proc-id="RM23G0E___0000B2P00000">
<ptxt>REMOVE NO. 2 PARKING BRAKE CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the adjusting nut.</ptxt>
<figure>
<graphic graphicname="C217137" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 2 parking brake cable from the No. 1 parking brake pull rod.</ptxt>
</s2>
<s2>
<ptxt>Pinch the claws of the No. 2 parking brake cable and push the cable outside of the vehicle slightly.</ptxt>
<figure>
<graphic graphicname="C215920" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the No. 2 parking brake cable assembly from the backing plate.</ptxt>
<figure>
<graphic graphicname="C217119" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for 3 Door:</ptxt>
<s3>
<ptxt>Remove the 2 bolts, 3 nuts and No. 2 parking brake cable.</ptxt>
<figure>
<graphic graphicname="C214614E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Parking Brake Cable Clamp</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3 Parking Brake Cable Clamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Detach the No. 1 parking brake cable clamp from the No. 2 parking brake cable.</ptxt>
</s3>
<s3>
<ptxt>Remove the No. 3 parking brake cable clamp from the body.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 5 Door:</ptxt>
<s3>
<ptxt>Remove the No. 1 parking brake cable heat insulator.</ptxt>
<figure>
<graphic graphicname="C217116" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Remove the 5 bolts and No. 2 parking brake cable.</ptxt>
<figure>
<graphic graphicname="C214612E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Parking Brake Cable Clamp</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Detach the No. 2 parking brake cable clamp from the No. 2 parking brake cable.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011C801GX_03_0043" proc-id="RM23G0E___0000B2O00000">
<ptxt>REMOVE NO. 3 PARKING BRAKE CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the adjusting nut.</ptxt>
<figure>
<graphic graphicname="C217137" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 3 parking brake cable from the No. 1 parking brake pull rod.</ptxt>
</s2>
<s2>
<ptxt>Pinch the claws of the No. 3 parking brake cable and push the cable outside of the vehicle slightly.</ptxt>
<figure>
<graphic graphicname="C215921" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the No. 3 parking brake cable assembly from the backing plate.</ptxt>
<figure>
<graphic graphicname="C217120" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for 3 Door:</ptxt>
<figure>
<graphic graphicname="C214615E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Parking Brake Cable Clamp</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 4 Parking Brake Cable Clamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s3>
<ptxt>Remove the 2 bolts, 3 nuts and No. 3 parking brake cable.</ptxt>
</s3>
<s3>
<ptxt>Detach the No. 2 parking brake cable clamp from the No. 3 parking brake cable.</ptxt>
</s3>
<s3>
<ptxt>Remove the No. 4 parking brake cable clamp from the body.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 5 Door:</ptxt>
<figure>
<graphic graphicname="C214604E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3 Parking Brake Cable Clamp</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s3>
<ptxt>Remove the 4 bolts and No. 3 parking brake cable.</ptxt>
</s3>
<s3>
<ptxt>Detach the No. 3 parking brake cable clamp from the No. 3 parking brake cable.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>