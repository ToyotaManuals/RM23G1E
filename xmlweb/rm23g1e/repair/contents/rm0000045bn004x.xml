<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8YV_T008J" variety="T008J">
<name>FUEL TANK (for 3 Door)</name>
<para id="RM0000045BN004X" category="A" type-id="30014" name-id="FU8CQ-01" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM0000045BN004X_01" type-id="01" category="01">
<s-1 id="RM0000045BN004X_01_0001" proc-id="RM23G0E___00005MK00000">
<ptxt>INSTALL NO. 1 FUEL TANK CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new No. 1 fuel tank cushions to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0002" proc-id="RM23G0E___00005ML00000">
<ptxt>INSTALL FUEL TUBE GROMMET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clamp to install the fuel tube grommet.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0003" proc-id="RM23G0E___00005MM00000">
<ptxt>INSTALL FUEL TANK FILLER PIPE LOWER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the fuel tank filler pipe lower with the 8 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>3.0</t-value1>
<t-value2>31</t-value2>
<t-value3>27</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0004" proc-id="RM23G0E___00005MN00000">
<ptxt>INSTALL FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel tank breather hose to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0005" proc-id="RM23G0E___00005MO00000">
<ptxt>INSTALL FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel tank to filler pipe hose to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0006" proc-id="RM23G0E___00005MP00000">
<ptxt>INSTALL FUEL SENDER GAUGE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the fuel sender gauge with the 5 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>1.5</t-value1>
<t-value2>15</t-value2>
<t-value3>13</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0020" proc-id="RM23G0E___00005N100000">
<ptxt>INSTALL FUEL CUT OFF VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel cut off valve to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0007" proc-id="RM23G0E___00005MQ00000">
<ptxt>INSTALL NO. 1 FUEL EVAPORATION TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the No. 1 fuel evaporation tube with the 4 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>1.5</t-value1>
<t-value2>15</t-value2>
<t-value3>13</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0008" proc-id="RM23G0E___00005MR00000">
<ptxt>INSTALL FUEL TANK VENT TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the fuel tank vent tube with the 7 screws and attach the 2 clamps.</ptxt>
<torque>
<torqueitem>
<t-value1>3.5</t-value1>
<t-value2>36</t-value2>
<t-value3>31</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0009" proc-id="RM23G0E___00005MS00000">
<ptxt>INSTALL FUEL TANK CHECK VALVE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the hose and install the fuel tank check valve.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0010" proc-id="RM23G0E___00005MT00000">
<ptxt>INSTALL NO. 4 FUEL TUBE CLAMP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 4 fuel tube clamp to the fuel tank check valve and attach the clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0011" proc-id="RM23G0E___00005MU00000">
<ptxt>INSTALL FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the fuel tank on a transmission jack and lift up the transmission jack.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 fuel tank bands with the 2 pins and 2 clips.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 fuel tank bands with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0012" proc-id="RM23G0E___00005MV00000">
<ptxt>CONNECT FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank breather hose to the filler pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0013" proc-id="RM23G0E___00005MW00000">
<ptxt>CONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank to filler pipe hose to the filler pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0014" proc-id="RM23G0E___00005MX00000">
<ptxt>CONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 fuel hoses.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0015" proc-id="RM23G0E___00005MY00000">
<ptxt>INSTALL NO. 1 FUEL TANK PROTECTOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 fuel tank protector with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0016" proc-id="RM23G0E___00005MZ00000">
<ptxt>INSTALL REAR FLOOR SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel sender gauge connector and fuel tank ground connector.</ptxt>
</s2>
<s2>
<ptxt>Install the rear floor service hole cover with the 3 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0024" proc-id="RM23G0E___00005N400000">
<ptxt>INSTALL REAR FLOOR MAT SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 6 claws to install the rear floor mat rear support plate.</ptxt>
</s2>
<s2>
<ptxt>Install the 5 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000045BN004X_01_0017" proc-id="RM23G0E___00005N000000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0021" proc-id="RM23G0E___00005N200000">
<ptxt>BLEED AIR FROM FUEL SYSTEM</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM000002SY802NX_01_0002"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000045BN004X_01_0022" proc-id="RM23G0E___00005N300000">
<ptxt>INSPECT FOR FUEL LEAK</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM000002SY802OX_01_0001"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM000002SY802NX_01_0001"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>