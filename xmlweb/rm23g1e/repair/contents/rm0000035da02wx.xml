<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A2_T00JQ" variety="T00JQ">
<name>PARKING ASSIST MONITOR SYSTEM</name>
<para id="RM0000035DA02WX" category="D" type-id="303F2" name-id="PM37Y-05" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM0000035DA02WX_z0" proc-id="RM23G0E___0000CUA00000">
<content5 releasenbr="1">
<step1>
<ptxt>GENERAL</ptxt>
<step2>
<ptxt>This system has a rear television camera assembly mounted on the back door to display the rear view of the vehicle on the display and navigation module display. The display panel also shows a composite view consisting of the area behind the vehicle and parking guidelines to assist the driver in parking the vehicle by monitoring the area behind the vehicle.</ptxt>
</step2>
<step2>
<ptxt>This system consists of the following components:</ptxt>
<step3>
<ptxt>Parking assist ECU</ptxt>
</step3>
<step3>
<ptxt>Rear television camera assembly</ptxt>
</step3>
<step3>
<ptxt>Display and navigation module display</ptxt>
</step3>
<step3>
<ptxt>Skid control ECU</ptxt>
</step3>
<step3>
<ptxt>Spiral cable sub-assembly</ptxt>
</step3>
</step2>
<step2>
<ptxt>This system is equipped with a self-diagnosis system, which is operated on a designated window that appears on the display panel, just as in the navigation system.</ptxt>
</step2>
</step1>
<step1>
<ptxt>FUNCTION OF COMPONENTS</ptxt>
<step2>
<ptxt>The parking assist ECU controls the system by using information from the following components.</ptxt>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>Rear Television Camera Assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Mounted on the back door to transmit the rear view of the vehicle to the parking assist ECU.</ptxt>
</item>
<item>
<ptxt>Has a color video camera that uses a Charge-coupled Device (CCD) and a wide-angle lens.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Parking Assist ECU</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Transmits video signals which contain a composite consisting of the rear view of the vehicle taken with the television camera and parking assist guidelines to the multi-display.</ptxt>
</item>
<item>
<ptxt>Performs overall control of the system by receiving signals from the sensors.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Display and Navigation Module Display</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Receives the video signals containing a composite of the rear view of the vehicle and parking assist guideline signals from the parking assist ECU and displays them on the display panel.</ptxt>
</item>
<item>
<ptxt>Uses the yaw rate detected by the gyro sensor that is built into the display and navigation module display to transmit the movement of the vehicle to the parking assist ECU.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Steering Pad Switch Assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>When multi-terrain select turns on, the shift lever is moved to R and camera selection is selected, pressing the up/down switch of the multi-function switch changes the camera display screen.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Spiral Cable Sub-assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Detects the angle of the steering wheel and sends the resulting signals to the parking assist ECU using CAN communication.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Park/Neutral Position Switch Assembly*1</ptxt>
<ptxt>Back-up Light Switch Assembly*2</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Sends the shift position signal to the parking assist ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Skid Control ECU</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Transmits a vehicle speed signal to the parking assist ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Back Door Courtesy Light Switch Assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Transmits a courtesy light switch signal to main body ECU (multiplex network body ECU).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for Automatic Transmission</ptxt>
</item>
<item>
<ptxt>*2: for Manual Transmission</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>OPERATION EXPLANATION</ptxt>
<step2>
<ptxt>The reverse position signal is sent from the shift lever position switch to the parking assist ECU when the shift lever is moved to R.</ptxt>
<ptxt>After receiving the reverse position signal, the parking assist ECU switches the display signal for the display and navigation module display from the navigation system to the parking assist monitor system.</ptxt>
</step2>
<step2>
<ptxt>In parallel parking assist mode, an appropriate steering angle and timing information can be provided for the driver. This is based on the information from the steering angle sensor signal and the vehicle angle data signal that are sent to the parking assist ECU.</ptxt>
<atten4>
<ptxt>The steering angle sensor signal is used to control the parking assist for estimated guide line mode only.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>DISPLAY MODE SETTING</ptxt>
<figure>
<graphic graphicname="E199673E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Line Mode Button</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Estimated Guide Line Mode</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking Guide Line Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Guide Line Deletion Mode</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Line Mode Button on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step2>
<ptxt>While the parking assist monitor is displayed, pressing the line mode button switches the parking assist monitor display mode.</ptxt>
<table pgwide="1">
<title>Parking Assist Monitor Display Mode</title>
<tgroup cols="5">
<colspec colname="COL3" colwidth="2.41in"/>
<colspec colname="COLSPEC4" colwidth="1.17in"/>
<colspec colname="COLSPEC5" colwidth="1.17in"/>
<colspec colname="COLSPEC6" colwidth="1.17in"/>
<colspec colname="COLSPEC0" colwidth="1.16in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Parking Assist Monitor Display Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Distance Guide Line </ptxt>
<ptxt>(Red with Black)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Width Extension Line</ptxt>
<ptxt>(Green)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Predicted Path Line</ptxt>
<ptxt>(Yellow)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Parking Guide Line</ptxt>
<ptxt>(Green)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Estimated Guide Line Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not displayed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Parking Guide Line Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Displayed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Guide Line Deletion Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not displayed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>COMMUNICATION SYSTEM OUTLINE</ptxt>
<step2>
<ptxt>The components of the parking assist monitor system communicate with each other through the AVC-LAN. Also, parallel parking assist mode judges the vehicle angle data transmitted via the AVC-LAN from the display and navigation module display (the data is calculated by the display and navigation module display by integrating the yaw rate of the gyro sensor built into the display and navigation module display).</ptxt>
</step2>
<step2>
<ptxt>If a short circuit or open circuit occurs in the AVC-LAN, communication is interrupted and the parking assist monitor system will stop functioning.</ptxt>
</step2>
</step1>
<step1>
<ptxt>DIAGNOSTIC FUNCTION OUTLINE</ptxt>
<step2>
<ptxt>This parking assist monitor system has a diagnostic function displayed in the display and navigation module display. This function enables the calibration (adjustment and verify) of the parking assist monitor system (See page <xref label="Seep01" href="RM000003WVZ02WX"/>).</ptxt>
</step2>
<step2>
<ptxt>The following items can be checked by using the intelligent tester.</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL3" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep02" href="RM0000035DB02WX"/>
</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Data List / Active Test</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>
<xref label="Seep03" href="RM000003WW002FX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>