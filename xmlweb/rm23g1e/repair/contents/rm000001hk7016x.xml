<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001H" variety="S001H">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001H_7B982_T00HQ" variety="T00HQ">
<name>BRAKE MASTER CYLINDER</name>
<para id="RM000001HK7016X" category="G" type-id="3001K" name-id="BR0RQ-09" from="201207">
<name>INSPECTION</name>
<subpara id="RM000001HK7016X_03" type-id="11" category="10" proc-id="RM23G0E___0000AUL00000">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>The master cylinder and piston are designed so that the piston can easily fall out. Prevent this by making sure the tip of the master cylinder faces downward when handling the master cylinder.</ptxt>
</item>
<item>
<ptxt>Make sure foreign matter does not attach to the piston of the master cylinder. If foreign matter attaches, clean it off with a cloth. Then apply lithium soap base glycol grease to the entire outer circumference of the contact surface of the piston.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM000001HK7016X_01" type-id="01" category="01">
<s-1 id="RM000001HK7016X_01_0001" proc-id="RM23G0E___0000AUK00000">
<ptxt>INSPECT AND ADJUST BRAKE BOOSTER PUSH ROD</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Adjust the booster push rod when the master cylinder is replaced with a new one. Adjustment is not necessary when the master cylinder is reinstalled and the booster is replaced with a new one.</ptxt>
</atten4>
<s2>
<ptxt>Set SST on the master cylinder and lower the rod of SST until it touches the piston.</ptxt>
<figure>
<graphic graphicname="G028332E06" width="2.775699831in" height="3.779676365in"/>
</figure>
<sst>
<sstitem>
<s-number>09737-00013</s-number>
</sstitem>
</sst>
<atten4>
<ptxt>SST 09737-00012 can also be used.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Apply chalk to the flat tip of the rod of SST . Turn SST upside down and measure the clearance between the brake booster push rod and SST.</ptxt>
<spec>
<title>Standard clearance</title>
<specitem>
<ptxt>0 mm (0 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Apply Chalk</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If there is clearance between the main body of SST and the shell of the brake booster, the push rod is protruding too far. If the chalk does not stick to the tip of the brake booster push rod, the push rod protrusion is insufficient.</ptxt>
</s2>
<s2>
<ptxt>If the clearance is not within the standard, adjust the length by holding the rod using SST and turning the tip of the rod using a 7 mm socket driver.</ptxt>
<figure>
<graphic graphicname="C128539E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09737-00020</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>Check the push rod clearance again after adjusting.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>