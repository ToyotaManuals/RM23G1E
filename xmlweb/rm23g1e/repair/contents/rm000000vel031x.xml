<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B997_T00IV" variety="T00IV">
<name>INSTRUMENT PANEL SPEAKER</name>
<para id="RM000000VEL031X" category="A" type-id="30014" name-id="AV9NX-03" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000000VEL031X_02" type-id="11" category="10" proc-id="RM23G0E___0000C0E00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0CLX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000000VEL031X_01" type-id="01" category="01">
<s-1 id="RM000000VEL031X_01_0001" proc-id="RM23G0E___0000C0800001">
<ptxt>INSTALL FRONT NO. 4 SPEAKER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the speaker connector.</ptxt>
</s2>
<s2>
<ptxt>Align the speaker with the positioning pins of the instrument panel and temporarily install the speaker.</ptxt>
</s2>
<s2>
<ptxt>Install the front No. 4 speaker with the 2 bolts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</item>
<item>
<ptxt>When installing the speaker to the instrument panel, be careful that the wires do not get caught between parts.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000VEL031X_01_0034" proc-id="RM23G0E___0000C0D00001">
<ptxt>INSTALL UPPER INSTRUMENT CLUSTER FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 clips and 2 guides to install the upper instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL031X_01_0026" proc-id="RM23G0E___0000C0B00001">
<ptxt>INSTALL DISPLAY AND NAVIGATION MODULE DISPLAY WITH BRACKET (w/ Navigation System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Insert the display and navigation module display and attach the 8 clips on the backside of the display and navigation module display.</ptxt>
<figure>
<graphic graphicname="B241764" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>When inserting the display, do not press the knobs on it.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the display and navigation module display with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12.5</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL031X_01_0027" proc-id="RM23G0E___00006Y400000">
<ptxt>INSTALL INTEGRATION CONTROL AND PANEL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E204228" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 clips to install the integration control and panel assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL031X_01_0005" proc-id="RM23G0E___0000C0900001">
<ptxt>INSTALL FRONT NO. 2 SPEAKER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Align the speaker with the positioning pins of the instrument panel and temporarily install the speaker.</ptxt>
</s2>
<s2>
<ptxt>Install the front No. 2 speaker with the 2 bolts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</item>
<item>
<ptxt>When installing the speaker to the instrument panel, be careful that the wires do not get caught between parts.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000VEL031X_01_0032" proc-id="RM23G0E___0000C0C00001">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL SPEAKER PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 clips, claw and 2 guides to install the No. 1 instrument panel speaker panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL031X_01_0014" proc-id="RM23G0E___0000BZV00001">
<ptxt>INSTALL FRONT PILLAR GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 guides to install the front pillar garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL031X_01_0018" proc-id="RM23G0E___0000BZW00001">
<ptxt>INSTALL NO. 1 ASSIST GRIP
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for the other No. 1 assist grip.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the No. 1 assist grip.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL031X_01_0033" proc-id="RM23G0E___0000BZY00001">
<ptxt>REMOVE FRONT NO. 1 ASSIST GRIP PLUG LH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for the other front No. 1 assist grip plug.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the front No. 1 assist grip plug.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEL031X_01_0019" proc-id="RM23G0E___0000C0A00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>