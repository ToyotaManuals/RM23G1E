<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001H" variety="S001H">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001H_7B984_T00HS" variety="T00HS">
<name>HYDRAULIC BRAKE BOOSTER (for RHD)</name>
<para id="RM00000171V01QX" category="A" type-id="8000E" name-id="BR3UU-01" from="201207">
<name>REASSEMBLY</name>
<subpara id="RM00000171V01QX_01" type-id="01" category="01">
<s-1 id="RM00000171V01QX_01_0016" proc-id="RM23G0E___0000AXD00000">
<ptxt>INSTALL BRAKE BOOSTER PISTON SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of lithium soap base glycol grease to a new piston.</ptxt>
<figure>
<graphic graphicname="F051194" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the piston.</ptxt>
<atten3>
<ptxt>When installing, be careful not to damage the seals, etc.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the plug.</ptxt>
</s2>
<s2>
<ptxt>Using 2 screwdrivers, install a new C-ring while pressing in the piston.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0004">
<ptxt>INSTALL MASTER CYLINDER BOOT</ptxt>
</s-1>
<s-1 id="RM00000171V01QX_01_0017" proc-id="RM23G0E___0000AXE00000">
<ptxt>INSTALL MASTER CYLINDER PUSH ROD CLEVIS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the brake master cylinder side lock nut and rod operating adapter to the brake master cylinder.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>260</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the lock nut and master cylinder push rod clevis to the rod operating adapter.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>260</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0015" proc-id="RM23G0E___0000AXC00000">
<ptxt>INSTALL MASTER CYLINDER SOLENOID</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket.</ptxt>
<atten3>
<ptxt>Keep all surfaces of the master cylinder solenoid, master cylinder and gasket, especially contact surfaces, away from water and dust.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the master cylinder solenoid with 6 new bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>32</t-value1>
<t-value2>325</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0018" proc-id="RM23G0E___0000AXF00000">
<ptxt>INSTALL BRAKE BOOSTER ACCUMULATOR PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the brake booster accumulator pipe and compression spring.</ptxt>
<atten3>
<ptxt>Make sure that no foreign matter enters the pump.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install a new O-ring to the brake booster accumulator.</ptxt>
</s2>
<s2>
<ptxt>Install the brake booster accumulator.</ptxt>
<torque>
<torqueitem>
<t-value1>57</t-value1>
<t-value2>585</t-value2>
<t-value4>42</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0024" proc-id="RM23G0E___0000AXK00000">
<ptxt>INSTALL NO. 3 BRAKE ACTUATOR BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 brake actuator bracket.</ptxt>
<torque>
<torqueitem>
<t-value1>7.8</t-value1>
<t-value2>80</t-value2>
<t-value3>69</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0013" proc-id="RM23G0E___0000AXA00000">
<ptxt>INSTALL NO. 1 BRAKE BOOSTER PUMP BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the brake booster pump bush to the No. 1 brake booster pump bracket.</ptxt>
</s2>
<s2>
<ptxt>Using a 5 mm hexagon wrench, install the No. 1 brake booster pump bracket with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>7.8</t-value1>
<t-value2>80</t-value2>
<t-value3>69</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0020" proc-id="RM23G0E___0000AXG00000">
<ptxt>INSTALL BRAKE BOOSTER WITH ACCUMULATOR PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 4 mm hexagon wrench, install the 2 pins to the brake master cylinder.</ptxt>
<torque>
<torqueitem>
<t-value1>7.8</t-value1>
<t-value2>80</t-value2>
<t-value3>69</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install 2 new brake booster pump collars and 2 brake booster pump bushes to the brake booster with accumulator pump assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the brake booster with accumulator pump assembly to the brake master cylinder.</ptxt>
</s2>
<s2>
<ptxt>Install a new clip.</ptxt>
<figure>
<graphic graphicname="F051199E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect the wire harnesses with the 2 screws.</ptxt>
<figure>
<graphic graphicname="F051186E08" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>2.9</t-value1>
<t-value2>30</t-value2>
<t-value3>26</t-value3>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Red Cable</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Black Cable</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install 2 new plugs.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0009" proc-id="RM23G0E___0000AX900000">
<ptxt>INSTALL NO. 1 BRAKE ACTUATOR TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a union nut wrench, install the No. 1 brake actuator tube.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0021" proc-id="RM23G0E___0000AXH00000">
<ptxt>INSTALL NO. 1 BRAKE ACTUATOR HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using needle nose pliers, install the brake actuator hose with the 2 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0014" proc-id="RM23G0E___0000AXB00000">
<ptxt>INSTALL MASTER CYLINDER RESERVOIR GROMMET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of lithium soap base glycol grease to 3 new reservoir grommets.</ptxt>
<figure>
<graphic graphicname="F051416" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 reservoir grommets to the brake master cylinder.</ptxt>
<atten3>
<ptxt>Be careful of the size of each grommet.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0022" proc-id="RM23G0E___0000AXI00000">
<ptxt>INSTALL BRAKE MASTER CYLINDER RESERVOIR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the brake master cylinder reservoir sub-assembly with the screw.</ptxt>
<torque>
<torqueitem>
<t-value1>1.7</t-value1>
<t-value2>17</t-value2>
<t-value3>15</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the master cylinder reservoir filler cap.</ptxt>
</s2>
<s2>
<ptxt>Using a pin punch and hammer, install a new pin to the brake master cylinder reservoir.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171V01QX_01_0023" proc-id="RM23G0E___0000AXJ00000">
<ptxt>INSTALL NO. 1 BRAKE ACTUATOR BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the brake fluid level warning switch connector to the No. 1 brake actuator bracket.</ptxt>
</s2>
<s2>
<ptxt>Using a 5 mm hexagon wrench, install the No. 1 brake actuator bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>7.8</t-value1>
<t-value2>80</t-value2>
<t-value3>69</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>