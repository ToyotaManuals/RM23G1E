<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12053_S001Z" variety="S001Z">
<name>LIGHTING (INT)</name>
<ttl id="12053_S001Z_7B9BM_T00LA" variety="T00LA">
<name>LIGHTING SYSTEM</name>
<para id="RM000001Y8B02XX" category="U" type-id="3001G" name-id="LI73T-04" from="201210">
<name>TERMINALS OF ECU</name>
<subpara id="RM000001Y8B02XX_z0" proc-id="RM23G0E___0000F6500001">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK DRIVER SIDE JUNCTION BLOCK ASSEMBLY, MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)</ptxt>
<figure>
<graphic graphicname="B235899E08" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step2>
<ptxt>Remove the main body ECU (See page <xref label="Seep01" href="RM000003WBO03DX"/>).</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A-29 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-30 (BECU) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-32 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-11 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G63-3 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Install the main body ECU (See page <xref label="Seep02" href="RM000003WBM03CX"/>).</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<colspec colname="COL4" colwidth="1.43in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G63-1 (RCYL) - 2D-4 (GND1)*7</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear door courtesy light RH signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear door courtesy light RH on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rear door courtesy light RH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G63-4 (OILE) - 2D-4 (GND1)*3</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Side step light signal output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Side step light on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Side step light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G63-5 (ILUP) - 2D-4 (GND1)*5</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Room light switch up signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Room light switch up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Room light switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G63-8 (ILDN) - 2D-4 (GND1)*5</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Room light switch down signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Room light switch down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Room light switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G64-1 (GCTY) - 2D-4 (GND1)*6</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>V - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Glass hatch courtesy light switch signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Glass hatch open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Glass hatch closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G64-6 (RCTY) - 2D-4 (GND1)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear door courtesy light switch RH signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear door RH open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rear door RH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G64-19 (BCTY) - 2D-4 (GND1)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Back door courtesy light switch signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Back door open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G65-1 (FLCL) - 2D-4 (GND1)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Front door courtesy light LH signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front door courtesy light LH on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Front door courtesy light LH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G65-2 (OSPT) - 2D-4 (GND1)*5</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>SB - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Front door illumination signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front door illumination on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Front door illumination off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G65-3 (LCTY) - 2D-4 (GND1)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>V - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear door courtesy light switch LH signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear door LH open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rear door LH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G65-4 (LCYL) - 2D-4 (GND1)*7</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear door courtesy light LH signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear door courtesy light LH on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rear door courtesy light LH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G65-5 (CSPT) - 2D-4 (GND1)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>GR - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Center console spot light signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON or ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G65-6 (TSPT) - 2D-4 (GND1)*5</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>SB - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Rear door illumination signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear door illumination on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rear door illumination off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2D-15 (FRCY) - 2D-4 (GND1)*1</ptxt>
<ptxt>2H-26 (FRCY) - 2D-4 (GND1)*2</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Front door courtesy light switch RH signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front door RH open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Front door RH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2D-18 (ILE) - 2D-4 (GND1)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Interior light signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Interior light on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Interior light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2D-26 (FRCL) - 2D-4 (GND1)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Front door courtesy light RH signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front door courtesy light RH on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Front door courtesy light RH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2D-36 (FSPT) - 2D-4 (GND1)*4</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>SB - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Interior foot light signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Interior foot light on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Interior foot light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>2I-27 (FLCY) - 2D-4 (GND1)*1</ptxt>
<ptxt>2D-31 (FLCY) - 2D-4 (GND1)*2</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Front door courtesy light switch LH signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front door LH open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Front door LH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
<item>
<ptxt>*3: w/ Side Step Light</ptxt>
</item>
<item>
<ptxt>*4: w/ Interior Foot Light</ptxt>
</item>
<item>
<ptxt>*5: w/ Door Illumination</ptxt>
</item>
<item>
<ptxt>*6: w/ Glass Hatch Opener System</ptxt>
</item>
<item>
<ptxt>*7: for 5 Door</ptxt>
</item>
</list1>
<ptxt>If the result is not as specified, the main body ECU may have a malfunction.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>