<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000000PF30QZX" category="C" type-id="3022O" name-id="ESME9-07" from="201210">
<dtccode>P0112</dtccode>
<dtcname>Intake Air Temperature Circuit Low Input</dtcname>
<dtccode>P0113</dtccode>
<dtcname>Intake Air Temperature Circuit High Input</dtcname>
<subpara id="RM000000PF30QZX_01" type-id="60" category="03" proc-id="RM23G0E___00000MJ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A067628E12" width="2.775699831in" height="3.779676365in"/>
</figure>
<list1 type="nonmark">
<item>
<ptxt>The intake air temperature sensor, mounted on the mass air flow meter, monitors the intake air temperature. The intake air temperature sensor has a built-in thermistor with a resistance that varies according to the temperature of the intake air. When the intake air temperature is low, the resistance of the thermistor increases. When the temperature is high, the resistance drops. These variations in resistance are transmitted to the ECM as voltage changes (see Fig. 1). </ptxt>
</item>
<item>
<ptxt>The intake air temperature sensor is powered by a 5 V supply from the THA terminal of the ECM, via resistor R.</ptxt>
</item>
<item>
<ptxt>Resistor R and the intake air temperature sensor are connected in series. When the resistance value of the intake air temperature sensor changes according to changes in the intake air temperature, the voltage at terminal THA also varies. Based on this signal, the ECM increases the fuel injection volume when the engine is cold to improve driveability.</ptxt>
<atten4>
<ptxt>When DTC P0112 or P0113 is stored, the ECM enters fail-safe mode. During fail-safe mode, the intake air temperature is estimated to be 20°C (68°F) by the ECM. The ECM continues operating fail-safe mode until a pass condition is detected.</ptxt>
</atten4>
</item>
</list1>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL3" colwidth="3.12in"/>
<colspec colname="COL4" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0112</ptxt>
</entry>
<entry valign="middle">
<ptxt>A short in the intake air temperature sensor circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in intake air temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Intake air temperature sensor (built into mass air flow meter)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0113</ptxt>
</entry>
<entry valign="middle">
<ptxt>An open in the intake air temperature sensor circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in intake air temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Intake air temperature sensor (built into mass air flow meter)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When either any of these DTCs are output, check the intake air temperature by entering the following menus: Powertrain / Engine and ECT / Data List / Intake Air.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Malfunction</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Open circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Short circuit</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PF30QZX_02" type-id="64" category="03" proc-id="RM23G0E___00000MK00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the sensor voltage and uses this value to calculate the intake air temperature. When the sensor output voltage deviates from the normal operating range, the ECM interprets this as a malfunction in the intake air temperature sensor and stores a DTC.</ptxt>
<ptxt>Example:</ptxt>
<ptxt>If the sensor output voltage is higher than 4.91 V for 0.5 seconds or more, the ECM determines that there is an open circuit in the intake air temperature sensor circuit and stores DTC P0113. Conversely, if the output voltage is below 0.18 V for 0.5 seconds or more, the ECM determines that there is a short in the sensor circuit and stores DTC P0112.</ptxt>
<ptxt>If the malfunction is not repaired successfully, a DTC is stored 0.5 seconds after the engine is next started.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PF30QZX_07" type-id="32" category="03" proc-id="RM23G0E___00000ML00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A147856E17" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000PF30QZX_08" type-id="51" category="05" proc-id="RM23G0E___00000MM00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PF30QZX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PF30QZX_09_0001" proc-id="RM23G0E___00000MN00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INTAKE AIR TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Intake Air.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Same as actual intake air temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If there is an open circuit, the intelligent tester indicates -40°C (-40°F).</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>If there is a short circuit, the intelligent tester indicates 140°C (284°F) or higher.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PF30QZX_09_0003" fin="false">A</down>
<right ref="RM000000PF30QZX_09_0008" fin="false">B</right>
<right ref="RM000000PF30QZX_09_0004" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0002" proc-id="RM23G0E___00000MO00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR OPEN IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A208642E08" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Connect terminals 1 (THA) and 2 (E2) of the mass air flow meter wire harness side connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Intake Air.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester.</ptxt>
<spec>
<title>Standard value</title>
<specitem>
<ptxt>140°C (284°F) or higher</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mass Air Flow Meter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Mass Air Flow Meter)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PF30QZX_09_0005" fin="true">OK</down>
<right ref="RM000000PF30QZX_09_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0003" proc-id="RM23G0E___00000MP00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C33-1 (THA) - C35-15 (THA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C33-2 (E2) - C35-7 (ETHA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PF30QZX_09_0007" fin="true">OK</down>
<right ref="RM000000PF30QZX_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0008" proc-id="RM23G0E___00000MQ00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A198733E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Intake Air.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester.</ptxt>
<spec>
<title>Standard value</title>
<specitem>
<ptxt>-40°C (-40°F)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mass Air Flow Meter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PF30QZX_09_0010" fin="true">OK</down>
<right ref="RM000000PF30QZX_09_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0009" proc-id="RM23G0E___00000MR00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C33-1 (THA) or C35-15 (THA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PF30QZX_09_0012" fin="true">OK</down>
<right ref="RM000000PF30QZX_09_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0004">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0VHX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0005">
<testtitle>CONFIRM GOOD CONNECTION TO SENSOR. IF OK, REPLACE MASS AIR FLOW METER<xref label="Seep01" href="RM000000VW602UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0007">
<testtitle>CONFIRM GOOD CONNECTION TO ECM. IF OK, REPLACE ECM<xref label="Seep01" href="RM00000329202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0010">
<testtitle>REPLACE MASS AIR FLOW METER<xref label="Seep01" href="RM000000VW602UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PF30QZX_09_0012">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>