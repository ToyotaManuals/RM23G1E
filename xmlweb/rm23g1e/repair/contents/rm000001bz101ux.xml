<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000L" variety="S000L">
<name>1KD-FTV INTAKE / EXHAUST</name>
<ttl id="12010_S000L_7B90O_T00AC" variety="T00AC">
<name>EXHAUST MANIFOLD W/  TURBOCHARGER (w/o DPF)</name>
<para id="RM000001BZ101UX" category="L" type-id="3001A" name-id="IT01R-21" from="201207">
<name>PRECAUTION</name>
<subpara id="RM000001BZ101UX_z0" proc-id="RM23G0E___00006A300000">
<content5 releasenbr="1">
<step1>
<ptxt>MAINTENANCE PRECAUTION</ptxt>
<step2>
<ptxt>Do not stop the engine immediately after pulling a trailer, driving at a high speed, or driving uphill. Let the engine idle for 20 to 120 seconds before turning the ignition switch off. According to the driving conditions, vary the idling time.</ptxt>
</step2>
<step2>
<ptxt>Avoid quick acceleration or quickly increasing the engine speed immediately after starting a cold engine.</ptxt>
</step2>
<step2>
<ptxt>If the turbocharger is found to be defective, it must be replaced. Also, inspect the source of the trouble including the conditions under which the turbocharger was used. Repair or replace the following as necessary:</ptxt>
<step3>
<ptxt>Engine oil (level and quality)</ptxt>
</step3>
<step3>
<ptxt>Oil lines leading to the turbocharger</ptxt>
</step3>
</step2>
<step2>
<ptxt>Be careful when removing and reinstalling the turbocharger assembly. Do not drop, strike or grasp easily-deformed assembly parts, such as the actuator or push rod, during removal and reinstallation.</ptxt>
<figure>
<graphic graphicname="A228291" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Before removal, cover both the intake and exhaust ports and the oil inlet to prevent dirt or foreign objects from entering.</ptxt>
</step2>
<step2>
<ptxt>If replacing the turbocharger, check for deposits in the oil pipe. If necessary, replace the oil pipe.</ptxt>
<figure>
<graphic graphicname="A098799E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sludge</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Thoroughly remove any old gasket material sticking to the lubrication oil pipe flange or turbocharger oil flange.</ptxt>
</step2>
<step2>
<ptxt>If replacing the bolts or nuts, use TOYOTA genuine parts to prevent breakage or deformation.</ptxt>
</step2>
<step2>
<ptxt>If replacing the turbocharger, add 20 cc (1.2 cu. in.) of fresh oil into the turbocharger oil inlet hole and turn the turbine wheel by hand to spread oil over the bearing.</ptxt>
<figure>
<graphic graphicname="A110320" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>If overhauling or replacing the engine, cut the fuel supply after reassembly and crank the engine for 30 seconds to distribute oil throughout the engine.</ptxt>
<ptxt>Then reconnect the fuel supply and allow the engine to idle for 60 seconds.</ptxt>
</step2>
<step2>
<ptxt>Since the turbine wheel runs at an extremely high speed, if the engine is running without the air cleaner, air cleaner cap and hose, the turbine wheel will be damaged due to the entry of foreign particles.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>