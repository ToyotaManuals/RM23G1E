<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A2_T00JQ" variety="T00JQ">
<name>PARKING ASSIST MONITOR SYSTEM</name>
<para id="RM0000035D702UX" category="T" type-id="3001H" name-id="ED1S1-55" from="201207">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM0000035D702UX_z0" proc-id="RM23G0E___0000CU000000">
<content5 releasenbr="38">
<atten4>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms the listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</atten4>
<atten3>
<ptxt>The following inspection procedure of the parking assist monitor system is based on the assumption that the navigation system is normal. If the navigation system is malfunctioning, first proceed with troubleshooting of the navigation system.</ptxt>
</atten3>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>General</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry morerows="5" colsep="1" valign="middle" align="left">
<ptxt>When the shift lever is in R, the parking assist monitor image is not displayed (screen is not black)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "ECU power source circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003EB200CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Image from all cameras are abnormal"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003WW801IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Proceed to "Image from parking assist monitor is abnormal"</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001YQ808OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Proceed to "Reverse signal circuit"</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001W8V07KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="4" colsep="1" valign="middle" align="left">
<ptxt>When the shift lever is in R, the parking assist monitor image is not displayed (screen is black)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Image from camera for parking assist monitor is abnormal"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001YQ808OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Reverse signal circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001W8V07KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace display and navigation module display</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003B6H01LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="3" colsep="1" valign="middle" align="left">
<ptxt>When the shift lever is not in R, the parking assist monitor image is displayed</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Reverse signal circuit"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001W8V07KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace display and navigation module display</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003B6H01LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Display Malfunction (w/ Side Monitor System)</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Image from camera for the parking assist monitor is abnormal (side monitor system is also abnormal)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Images from all cameras are abnormal"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003WW801IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Image from camera for the parking assist monitor is abnormal (side monitor system is normal)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Image from camera for parking assist monitor is abnormal"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001YQ808OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Display Malfunction (w/o Side Monitor System)</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Image from camera for the parking assist monitor is abnormal</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Image from camera for parking assist monitor is abnormal"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001YQ808OX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Images from all cameras are abnormal</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003WW801IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Parking Assist Monitor System Malfunction</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle" align="left">
<ptxt>Projected course guidelines are not displayed</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"System initializing" is displayed</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Correct the steering angle neutral point</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000035DE02IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="3" colsep="1" valign="middle" align="left">
<ptxt>"System initializing" is displayed even after correcting the steering angle neutral point</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace spiral cable sub-assembly</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002O8X01FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Steering angle setting (diagnosis system)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003WVZ02WX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"System not ready" is displayed</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Parking assist ECU initialization (calibration)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000035DD02YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle" align="left">
<ptxt>"System not ready" is displayed even after parking assist ECU initialization</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Replace parking assist ECU (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000466X015X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Replace parking assist ECU (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000470500EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>During parallel parking assist mode, the screen does not change even though the vehicle has moved</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>If vehicle is moving in reverse at very low speed, screen may not change</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>During parallel parking assist mode, the screen changes even though the vehicle has not moved</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>If vehicle is stopped for extended period of time, screen may change</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"CHK" message(s) are displayed on the SIGNAL CHECK screen</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to ""CHK" message(s) are displayed on the SIGNAL CHECK screen"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003XLF01LX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>A back door open warning message is displayed even after back door is closed</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Proceed to "Message indicating back door is open is displayed even after back door is closed"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003XLE032X" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>