<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000187Z06JX" category="C" type-id="3035F" name-id="ESTG0-65" from="201210">
<dtccode>P0168</dtccode>
<dtcname>Fuel Temperature Too High</dtcname>
<subpara id="RM00000187Z06JX_01" type-id="60" category="03" proc-id="RM23G0E___00002DM00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0180 (See page <xref label="Seep01" href="RM00000188B06CX_01"/>).</ptxt>
<table pgwide="1">
<title>P0168</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Drive vehicle at high speed or with heavy load (Use freeze frame data from time malfunction occurred as reference while driving)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The fuel temperature is 110°C (230°F) or higher for the specified time (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Fuel cooler (Problem cooling fuel)</ptxt>
</item>
<item>
<ptxt>Fuel lines or fuel filter clogged</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000187Z06JX_02" type-id="51" category="05" proc-id="RM23G0E___00002DN00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07MX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See Page <xref label="Seep04" href="RM0000012XK07MX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187Z06JX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187Z06JX_03_0001" proc-id="RM23G0E___00002DO00001">
<testtitle>CHECK OTHER DTC OUTPUT (IN ADDITION TO DTC P0168)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0168 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0168 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTCs other than P0168 are output, perform troubleshooting for those codes first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000187Z06JX_03_0008" fin="false">A</down>
<right ref="RM00000187Z06JX_03_0003" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000187Z06JX_03_0008" proc-id="RM23G0E___00002DT00001">
<testtitle>CHECK FUEL SUPPLY PUMP ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform a visual inspection of the exterior of the fuel supply pump assembly and check that there are no fuel leaks.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There are no fuel leaks or stains.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187Z06JX_03_0009" fin="false">OK</down>
<right ref="RM00000187Z06JX_03_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187Z06JX_03_0009" proc-id="RM23G0E___00002DU00001">
<testtitle>CHECK FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if dirt is clogging the fuel cooler cooling fins. Clean the fins if they are clogged with dirt.</ptxt>
<atten4>
<ptxt>If the fuel cooler cooling fins are not clogged with dirt, replace the fuel filter.</ptxt>
</atten4>
</test1>
</content6>
<res>
<right ref="RM00000187Z06JX_03_0004" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187Z06JX_03_0002" proc-id="RM23G0E___00002DP00001">
<testtitle>REPLACE FUEL SUPPLY PUMP ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the fuel supply pump assembly (See page <xref label="Seep01" href="RM0000045CH009X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187Z06JX_03_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187Z06JX_03_0006" proc-id="RM23G0E___00002DR00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187Z06JX_03_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187Z06JX_03_0007" proc-id="RM23G0E___00002DS00001">
<testtitle>PERFORM FUEL SUPPLY PUMP INITIALIZATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform fuel supply pump initialization (See page <xref label="Seep01" href="RM000000TIN05LX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187Z06JX_03_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187Z06JX_03_0004" proc-id="RM23G0E___00002DQ00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK13SX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Fuel Temperature.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle in a city area and check that the value of Fuel Temperature does not exceed 110°C (230°F).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187Z06JX_03_0005" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187Z06JX_03_0003">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW050X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000187Z06JX_03_0005">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>