<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B991_T00IP" variety="T00IP">
<name>AUDIO AND VISUAL SYSTEM (w/ Navigation System)</name>
<para id="RM000001ZYU0KAX" category="J" type-id="801Q7" name-id="AV6V1-17" from="201210">
<dtccode/>
<dtcname>Sound Signal Circuit between Radio Receiver and Stereo Jack Adapter</dtcname>
<subpara id="RM000001ZYU0KAX_01" type-id="60" category="03" proc-id="RM23G0E___0000BV000001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 1 stereo jack adapter assembly sends an external device sound signal to the radio receiver assembly through this circuit.</ptxt>
<ptxt>The sound signal that has been sent is amplified by the stereo component amplifier assembly or radio receiver assembly, and then is sent to the speakers.</ptxt>
<ptxt>If there is an open or short in the circuit, sound cannot be heard from the speakers even if there is no malfunction in the stereo component amplifier assembly, radio receiver assembly or speakers.</ptxt>
</content5>
</subpara>
<subpara id="RM000001ZYU0KAX_02" type-id="32" category="03" proc-id="RM23G0E___0000BV100001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E199619E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001ZYU0KAX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001ZYU0KAX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001ZYU0KAX_04_0001" proc-id="RM23G0E___0000BV200001">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 STEREO JACK ADAPTER - RADIO RECEIVER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H39 No. 1 stereo jack adapter assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the H1 radio receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H39-8 (AUXO) - H1-5 (AUXI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-7 (ASGN) - H1-21 (ASGN)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-5 (ARO) - H1-19 (ARI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-6 (ALO) - H1-20 (ALI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-9 (AGND) - H1-6 (AGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-8 (AUXO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-7 (ASGN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-5 (ARO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-6 (ALO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H39-9 (AGND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001ZYU0KAX_04_0002" fin="true">OK</down>
<right ref="RM000001ZYU0KAX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001ZYU0KAX_04_0002">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000003YVJ02JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001ZYU0KAX_04_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>