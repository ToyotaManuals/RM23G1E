<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9BA_T00KY" variety="T00KY">
<name>ENGINE IMMOBILISER SYSTEM (w/o Entry and Start System)</name>
<para id="RM000001TCH01HX" category="C" type-id="3010J" name-id="TD6D4-01" from="201207" to="201210">
<dtccode>B2797</dtccode>
<dtcname>Communication Malfunction No. 1</dtcname>
<subpara id="RM000001TCH01HX_01" type-id="60" category="03" proc-id="RM23G0E___0000F1400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when a communication error occurs between the transponder key amplifier and transponder key ECU assembly. Some possible reasons for the communication error are: 1) 2 or more ignition keys are positioned too close together, or 2) noise is occurring in the communication line.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2797</ptxt>
</entry>
<entry valign="middle">
<ptxt>2 or more ignition keys are positioned too close to each other or noise occurs in the communication line.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Key</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Transponder key amplifier</ptxt>
</item>
<item>
<ptxt>Transponder key ECU assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001TCH01HX_02" type-id="32" category="03" proc-id="RM23G0E___0000F1500000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B224123E15" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001TCH01HX_03" type-id="51" category="05" proc-id="RM23G0E___0000F1600000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the transponder key ECU assembly, refer to the Service Bulletin.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001TCH01HX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001TCH01HX_04_0001" proc-id="RM23G0E___0000F1700000">
<testtitle>CHECK KEY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the ignition key being used is near other ignition keys, as shown in the illustration. Also, check whether a key ring is in contact with the key grip. </ptxt>
<figure>
<graphic graphicname="B213624" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Key is near other keys and/or key ring is in contact with key grip</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Key is not near other keys and key ring is not in contact with key grip</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001TCH01HX_04_0002" fin="false">A</down>
<right ref="RM000001TCH01HX_04_0004" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0002" proc-id="RM23G0E___0000F1800000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Separate the keys from each other or remove the key ring.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001TCQ024X"/>).</ptxt>
</test1>
<test1>
<ptxt>Insert a key into the ignition key cylinder, and then remove it. Repeat this for all the other keys.</ptxt>
</test1>
<test1>
<ptxt>Check that no DTC is output (See page <xref label="Seep02" href="RM000001TCQ024X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2797 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCH01HX_04_0011" fin="true">OK</down>
<right ref="RM000001TCH01HX_04_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0004" proc-id="RM23G0E___0000F1A00000">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY ECU - TRANSPONDER KEY AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G138*1 or G74*2 ECU connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 5L-E</ptxt>
</item>
<item>
<ptxt>*2: except 5L-E</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Disconnect the G75 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for 5L-E</title>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G138-15 (CODE) - G75-4 (CODE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G138-15 (CODE) or G75-4 (CODE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>except 5L-E</title>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G74-15 (CODE) - G75-4 (CODE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G74-15 (CODE) or G75-4 (CODE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCH01HX_04_0003" fin="false">OK</down>
<right ref="RM000001TCH01HX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0003" proc-id="RM23G0E___0000F1900000">
<testtitle>CHECK TRANSPONDER KEY ECU ASSEMBLY (NOISE)</testtitle>
<content6 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>*1: for 5L-E</ptxt>
</item>
<item>
<ptxt>*2: except 5L-E</ptxt>
</item>
</list1>
<test1>
<ptxt>Using an oscilloscope, check for noise in the waveform between the terminals of the G75 amplifier connector and G138*1 or G74*2 ECU connector.</ptxt>
<figure>
<graphic graphicname="B237349E03" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>G75-4 (CODE) - G138-15 (CODE)*1</ptxt>
<ptxt>G75-4 (CODE) - G74-15 (CODE)*2</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Key inserted in ignition key cylinder</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>No noise is present.</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>for 5L-E</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>except 5L-E</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Component with harness connected</ptxt>
<ptxt>(Transponder Key Amplifier)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Component with harness connected</ptxt>
<ptxt>(Transponder Key ECU Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry>
<ptxt>Noise should not appear</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001TCH01HX_04_0005" fin="false">OK</down>
<right ref="RM000001TCH01HX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0005" proc-id="RM23G0E___0000F1B00000">
<testtitle>REPLACE TRANSPONDER KEY AMPLIFIER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the transponder key amplifier with a new or normally functioning one (See page <xref label="Seep01" href="RM000003YO1014X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCH01HX_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0006" proc-id="RM23G0E___0000F1C00000">
<testtitle>CHECK WHETHER ENGINE STARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the engine starts normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Engine starts.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCH01HX_04_0007" fin="true">OK</down>
<right ref="RM000001TCH01HX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0007">
<testtitle>END (TRANSPONDER KEY AMPLIFIER IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0008">
<testtitle>FIND CAUSE OF NOISE AND REMOVE IT</testtitle>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0010">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000001TCH01HX_04_0011">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>