<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A1_T00JP" variety="T00JP">
<name>TOYOTA PARKING ASSIST-SENSOR SYSTEM (for 8 Sensor Type)</name>
<para id="RM000000VVD05MX" category="D" type-id="303FF" name-id="PM2VT-07" from="201207">
<name>OPERATION CHECK</name>
<subpara id="RM000000VVD05MX_z0" proc-id="RM23G0E___0000CQD00000">
<content5 releasenbr="1">
<step1>
<ptxt>SELF-CHECK FUNCTION INSPECTION (INITIAL CHECK)</ptxt>
<figure>
<graphic graphicname="E197515E02" width="2.775699831in" height="4.7836529in"/>
</figure>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the TOYOTA parking assist-sensor system off, or check that the TOYOTA parking assist-sensor system is off.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the TOYOTA parking assist-sensor system on and check that the following occurs:</ptxt>
<ptxt>1) After approximately 0.1 seconds, the buzzer sounds for approximately 1 second.</ptxt>
<ptxt>2) The system starts detection operation mode.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>During the initial check, malfunction detection operates and obstacle detection does not operate.</ptxt>
</item>
<item>
<ptxt>In the detection operation mode, obstacle detection and malfunction detection operate.</ptxt>
</item>
<item>
<ptxt>If a sensor has an open circuit or a sensor malfunctions because it is wet or frozen, a malfunction display is shown in the multi-information display and the buzzer sounds.</ptxt>
</item>
<item>
<ptxt>If the ignition switch is turned to ON while the TOYOTA parking assist-sensor system is on, the buzzer does not sound even though the malfunction detection operates.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
<step1>
<ptxt>MALFUNCTION DISPLAY (MULTI-INFORMATION DISPLAY)</ptxt>
<step2>
<ptxt>Open circuit indication</ptxt>
<step3>
<ptxt>If there is an open circuit between the ultrasonic sensor and the clearance warning ECU*1 or parking assist ECU*2, or a sensor is malfunctioning, the malfunction is displayed as shown in the illustration.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/o Parking Assist Monitor System and/or Side Monitor System</ptxt>
</item>
<item>
<ptxt>*2: w/ Parking Assist Monitor System and/or Side Monitor System</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="E204290E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The example shows an open circuit in the ultrasonic sensor (front left sensor).</ptxt>
</item>
<item>
<ptxt>If a sensor has an open circuit, check for DTCs and troubleshoot according to each inspection procedure (See page <xref label="Seep01" href="RM000000VKU048X"/>).</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
<step2>
<ptxt>Frozen indication</ptxt>
<step3>
<ptxt>If a sensor is covered with foreign matter, such as mud or snow, the affected sensor is displayed as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E180127E03" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The example shows that the ultrasonic sensor (front left sensor) is covered with foreign matter.</ptxt>
</item>
<item>
<ptxt>If "CLEAN SONAR" is displayed, refer to "Frozen indication is displayed in self-check function" in the Problem Symptoms Table to inspect the problem (See page <xref label="Seep02" href="X394000005X08RD"/>).</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>DETECTION RANGE MEASUREMENT AND DISPLAY INSPECTION</ptxt>
<step2>
<ptxt>Detection range measurement:</ptxt>
<atten3>
<ptxt>Make sure that the vehicle does not move by applying the parking brake firmly.</ptxt>
</atten3>
<step3>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Move the shift lever to R to check the ultrasonic sensors.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Turn the TOYOTA parking assist-sensor system on.</ptxt>
</step2>
<step2>
<ptxt>Move a φ 60 mm (2.36 in.) pole near each sensor to measure its detection range.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>These detection ranges are applicable when positioning a pole with a diameter of 60 mm (2.36 in.) parallel or perpendicular to the ground.</ptxt>
</item>
<item>
<ptxt>The ranges vary depending on the measuring method and type of obstacle (such as walls).</ptxt>
</item>
<item>
<ptxt>For close-range and medium-range detection, the values shown are for when using a pole with a diameter of 60 mm (2.36 in.). For long-range detection, the values shown are for when using walls.</ptxt>
</item>
<item>
<ptxt>The No. 1 ultrasonic sensor side view detection range hatched area*1 represents the cross section of the top view of the detection range along the lines labeled A. The hatched area*1 does not represent the entire side view detection range.</ptxt>
</item>
</list1>
</atten3>
<step3>
<ptxt>Detection range of front corner sensors</ptxt>
<figure>
<graphic graphicname="E194324E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
<step3>
<ptxt>Detection range of front corner sensors</ptxt>
<figure>
<graphic graphicname="E194325E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
<step3>
<ptxt>Detection range of rear corner sensors</ptxt>
<figure>
<graphic graphicname="E194323E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
<step3>
<ptxt>Detection range of rear corner sensors</ptxt>
<figure>
<graphic graphicname="E194322E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Check the displays and buzzer sounds when the ultrasonic sensors detect an obstacle.</ptxt>
<step3>
<ptxt>Front corner sensors</ptxt>
<table pgwide="1">
<title>Operation Condition</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>TOYOTA Parking Assist-sensor System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>In any position except P</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Less than 15 km/h (9.3 mph) if speed is increasing</ptxt>
</item>
<item>
<ptxt>Less than 10 km/h (6.2 mph) if speed is decreasing</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E180123E05" width="7.106578999in" height="8.799559038in"/>
</figure>
<spec>
<title>Standard</title>
<table pgwide="1">
<title>Multi-information Display and Buzzer</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC1" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>During Judgment</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Obstacle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
<ptxt>Close-range detection</ptxt>
<ptxt>Within approximately 350 +/-40 mm (13.78 +/-1.57 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds continuously</ptxt>
<ptxt>Number of bars displayed: 1 (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
<ptxt>Medium-range detection</ptxt>
<ptxt>From approximately 350 +/-40 to 475 +/-50 mm (13.78 +/-1.57 to 18.70 +/-1.97 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 75 ms / Off: 75 ms)</ptxt>
<ptxt>Number of bars displayed: 2 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 475 +/-50 to 600 +/-60 mm (18.70 +/-1.97 to 23.62 +/-2.36 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>Number of bars displayed: 3 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Display and Navigation Module Display or Accessory Meter and Buzzer</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC1" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>During Judgment</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Obstacle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
<ptxt>Close-range detection</ptxt>
<ptxt>Within approximately 350 +/-40 mm (13.78 +/-1.57 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds continuously</ptxt>
<ptxt>*4: Red (illuminated)</ptxt>
<ptxt>*5: Number of bars displayed: 1 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
<ptxt>Medium-range detection</ptxt>
<ptxt>From approximately 350 +/-40 to 475 +/-50 mm (13.78 +/-1.57 to 18.70 +/-1.97 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On 75 ms / Off: 75 ms)</ptxt>
<ptxt>*4: Yellow (blinking)</ptxt>
<ptxt>*5: Number of bars displayed: 2 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 475 +/-50 to 600 +/-60 mm (18.70 +/-1.97 to 23.62 +/-2.36 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>*4: Yellow (blinking)</ptxt>
<ptxt>*5: Number of bars displayed: 3 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>*5: When shift lever in D</ptxt>
<atten4>
<ptxt>Ultrasonic waves are used to measure the detection range; however, the detection range may vary depending on the ambient temperature.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Front center sensors</ptxt>
<table pgwide="1">
<title>Operation Condition</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>TOYOTA Parking Assist-sensor System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>except P, R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E194328E03" width="7.106578999in" height="7.795582503in"/>
</figure>
<spec>
<title>Standard</title>
<table pgwide="1">
<title>Multi-information Display and Buzzer</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>During Detection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Obstacle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
<ptxt>Close-range detection</ptxt>
<ptxt>Within approximately 300 +/-30 mm (11.8 +/-1.18 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds continuously</ptxt>
<ptxt>Number of bars displayed: 1 (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
<ptxt>Medium-range detection</ptxt>
<ptxt>From approximately 300 +/-30 to 375 +/-40 mm (11.8 +/-1.18 to 14.8 +/-1.57 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 75 ms / Off: 75 ms)</ptxt>
<ptxt>Number of bars displayed: 2 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 375 +/-40 to 500 +/-50 mm (11.8 +/-1.18 to 19.7 +/-1.97 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>Number of bars displayed: 3 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*4</ptxt>
<ptxt>Longest-range detection</ptxt>
<ptxt>From approximately 500 +/-50 to 1000 +/-100 mm (19.7 +/-1.97 to 39.4 +/-3.94 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 650 ms)</ptxt>
<ptxt>Number of bars displayed: 4 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wall</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Display and Navigation Module Display or Accessory Meter and Buzzer</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>During Detection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Obstacle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
<ptxt>Close-range detection</ptxt>
<ptxt>Within approximately 300 +/-30 mm (11.8 +/-1.18 in.)</ptxt>

</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds continuously</ptxt>
<ptxt>*4: Red (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
<ptxt>Medium-range detection</ptxt>
<ptxt>From approximately 300 +/-30 to 375 +/-40 mm (11.8 +/-1.18 to 14.8 +/-1.57 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 75 ms / Off: 75 ms)</ptxt>
<ptxt>*4: Yellow (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 375 +/-40 to 550 +/-50 mm (14.8 +/-1.57 to 21.7 +/-1.97 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>*4: Yellow (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*4</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 500 +/-50 to 1000 +/-100 mm (19.7 +/-1.97 to 39.4 +/-3.94 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>*4: Yellow (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wall</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Ultrasonic waves are used to measure the detection range; however, the detection range may vary depending on the ambient temperature.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Rear corner sensors</ptxt>
<table pgwide="1">
<title>Operation Condition</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>TOYOTA Parking Assist-sensor System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E180124E04" width="7.106578999in" height="6.791605969in"/>
</figure>
<spec>
<title>Standard</title>
<table pgwide="1">
<title>Multi-information Display and Buzzer</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC1" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>During Judgment</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Obstacle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
<ptxt>Close-range detection</ptxt>
<ptxt>Within approximately 250 +/-30 mm (9.84 +/-1.18 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds continuously</ptxt>
<ptxt>Number of bars displayed: 1 (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
<ptxt>Medium-range detection</ptxt>
<ptxt>From approximately 250 +/-30 to 375 +/-40 mm (9.84 +/-1.18 to 14.76 +/-1.57 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 75 ms / Off: 75 ms)</ptxt>
<ptxt>Number of bars displayed: 2 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 375 +/-40 to 550 +/-50 mm (14.76 +/-1.57 to 21.65 +/-1.97 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>Number of bars displayed: 3 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Display and Navigation Module Display or Accessory Meter and Buzzer</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC1" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>During Judgment</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Obstacle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
<ptxt>Close-range detection</ptxt>
<ptxt>Within approximately 250 +/-30 mm (9.84 +/-1.18 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds continuously</ptxt>
<ptxt>*4: Red (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
<ptxt>Medium-range detection</ptxt>
<ptxt>From approximately 250 +/-30 to 375 +/-40 mm (9.84 +/-1.18 to 14.76 +/-1.57 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 75 ms / Off: 75 ms)</ptxt>
<ptxt>*4: Yellow (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 375 +/-40 to 550 +/-50 mm (14.76 +/-1.57 to 21.65 +/-1.97 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>*4: Yellow (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Ultrasonic waves are used to measure the detection range; however, the detection range may vary depending on the ambient temperature.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Rear center sensors</ptxt>
<table pgwide="1">
<title>Operation Condition</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>TOYOTA Parking Assist-sensor System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E180125E04" width="7.106578999in" height="7.795582503in"/>
</figure>
<spec>
<title>Standard</title>
<table pgwide="1">
<title>Multi-information Display and Buzzer</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>During Detection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Obstacle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
<ptxt>Close-range detection</ptxt>
<ptxt>Within approximately 400 +/-40 mm (15.7 +/-1.57 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds continuously</ptxt>
<ptxt>Number of bars displayed: 1 (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
<ptxt>Medium-range detection</ptxt>
<ptxt>From approximately 400 +/-40 to 500 +/-50 mm (15.7 +/-1.57 to 19.7 +/-1.97 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 75 ms / Off: 75 ms)</ptxt>
<ptxt>Number of bars displayed: 2 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 500 +/-50 to 650 +/-70 mm (19.7 +/-1.97 to 25.6 +/-2.76 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>Number of bars displayed: 3 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*4</ptxt>
<ptxt>Longest-range detection</ptxt>
<ptxt>From approximately 650 +/-70 to 1500 +/-150 mm (25.6 +/-2.76 to 59.1 +/-5.91 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 650 ms)</ptxt>
<ptxt>Number of bars displayed: 4 (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wall</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Display and Navigation Module Display or Accessory Meter and Buzzer</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COLSPEC2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>During Detection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Obstacle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
<ptxt>Close-range detection</ptxt>
<ptxt>Within approximately 400 +/-40 mm (15.7 +/-1.57 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds continuously</ptxt>
<ptxt>*5: Red (illuminated)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
<ptxt>Medium-range detection</ptxt>
<ptxt>From approximately 400 +/-40 to 500 +/-50 mm (15.7 +/-1.57 to 19.7 +/-1.97 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 75 ms / Off: 75 ms)</ptxt>
<ptxt>*5: Yellow (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3</ptxt>
<ptxt>Long-range detection</ptxt>
<ptxt>From approximately 500 +/-50 to 650 +/-70 mm (19.7 +/-1.97 to 25.6 +/-2.76 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 150 ms)</ptxt>
<ptxt>*5: Yellow (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 mm (2.36 in.) diameter pole</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*4</ptxt>
<ptxt>Longest-range detection</ptxt>
<ptxt>From approximately 650 +/-70 to 1500 +/-150 mm (25.6 +/-2.76 to 59.1 +/-5.91 in.)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Buzzer: Sounds intermittently (On: 150 ms / Off: 650 ms)</ptxt>
<ptxt>*5: Yellow (blinking)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wall</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>Ultrasonic waves are used to measure the detection range; however, the detection range may vary depending on the ambient temperature.</ptxt>
</atten4>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>