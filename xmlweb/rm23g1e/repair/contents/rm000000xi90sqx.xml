<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97M_T00HA" variety="T00HA">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Vacuum Brake Booster)</name>
<para id="RM000000XI90SQX" category="C" type-id="803L0" name-id="BCDPR-02" from="201210">
<dtccode>C1405</dtccode>
<dtcname>Open or Short in Front Speed Sensor RH Circuit</dtcname>
<dtccode>C1406</dtccode>
<dtcname>Open or Short in Front Speed Sensor LH Circuit</dtcname>
<subpara id="RM000000XI90SQX_01" type-id="60" category="03" proc-id="RM23G0E___0000AL400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1401 and C1402 (See page <xref label="Seep01" href="RM000000XI90SPX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.93in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1405</ptxt>
<ptxt>C1406</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>An open in the speed sensor signal circuit continues for 0.5 seconds or more.</ptxt>
</item>
<item>
<ptxt>With the IG1 terminal voltage at 9.5 V or higher, the sensor power supply voltage decreases for 0.5 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front speed sensor RH/LH</ptxt>
</item>
<item>
<ptxt>Skid control sensor wire</ptxt>
</item>
<item>
<ptxt>Speed sensor circuit</ptxt>
</item>
<item>
<ptxt>Brake actuator assembly (Skid control ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTC C1405 is for the front speed sensor RH.</ptxt>
</item>
<item>
<ptxt>DTC C1406 is for the front speed sensor LH.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XI90SQX_02" type-id="32" category="03" proc-id="RM23G0E___0000AL500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C217214E10" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XI90SQX_03" type-id="51" category="05" proc-id="RM23G0E___0000AL600001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08JX"/>).</ptxt>
</item>
<item>
<ptxt>Check the speed sensor signal after replacement (See page <xref label="Seep02" href="RM000000XHT0BZX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XI90SQX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XI90SQX_04_0001" proc-id="RM23G0E___0000AL700001">
<testtitle>CHECK HARNESS AND CONNECTOR (MOMENTARY INTERRUPTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, check for any momentary interruption in the wire harness and connector corresponding to the DTC (See page <xref label="Seep01" href="RM000000XHS0CIX"/>).</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.17in"/>
<colspec colname="COL2" colwidth="3.21in"/>
<colspec colname="COL3" colwidth="1.52in"/>
<colspec colname="COLSPEC1" colwidth="1.19in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Speed Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Normal (there are no momentary interruptions).</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Perform the above inspection before removing the sensor and disconnecting the connector.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000XI90SQX_04_0034" fin="false">OK</down>
<right ref="RM000000XI90SQX_04_0031" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0034" proc-id="RM23G0E___0000AKQ00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (FR/FL WHEEL SPEED)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.16in"/>
<colspec colname="COL2" colwidth="2.41in"/>
<colspec colname="COL3" colwidth="1.55in"/>
<colspec colname="COLSPEC2" colwidth="1.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor RH value/ min.: 0 km/h (0 mph), max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Wheel Speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front speed sensor LH value/ min.: 0 km/h (0 mph), max.: 326 km/h (202 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual wheel speed</ptxt>
</entry>
<entry valign="middle">
<ptxt>No large fluctuations when driving at a constant speed.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that there is no difference between the speed value output from the speed sensor displayed on the intelligent tester and the speed value displayed on the speedometer when driving the vehicle.</ptxt>
<atten4>
<ptxt>Factors that affect the indicated vehicle speed include tire size, tire inflation and tire wear. The speed indicated on the speedometer has an allowable margin of error. This can be tested using a speedometer tester (calibrated chassis dynamometer). For details about testing and the margin of error, refer to the reference chart (See page <xref label="Seep02" href="RM000002Z4Q02LX"/>).</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The speed value output from the speed sensor displayed on the intelligent tester is the same as the actual vehicle speed measured using a speedometer tester (calibrated chassis dynamometer).</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000XI90SQX_04_0004" fin="false">OK</down>
<right ref="RM000000XI90SQX_04_0031" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0004" proc-id="RM23G0E___0000AL800001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0G5X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM000000XHV0G5X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.10in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XI90SQX_04_0023" fin="true">A</down>
<right ref="RM000000XI90SQX_04_0018" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0031" proc-id="RM23G0E___0000ALB00001">
<testtitle>INSPECT SKID CONTROL SENSOR WIRE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Make sure that there is no looseness at the locking parts and connecting parts of the connectors.</ptxt>
</test1>
<test1>
<ptxt>Remove the skid control sensor wire (See page <xref label="Seep01" href="RM000001B2J01GX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214500E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.74in"/>
<colspec colname="COL2" colwidth="1.02in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Ap1-1 - p1-1 (FR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ap1-2 - p1-2 (FR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ap1-1 - Ap1-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.71in"/>
<colspec colname="COL2" colwidth="1.05in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Ap2-1 - p2-1 (FL-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ap2-2 - p2-2 (FL+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ap2-1 - Ap2-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Skid Control Sensor Wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XI90SQX_04_0019" fin="false">OK</down>
<right ref="RM000000XI90SQX_04_0032" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0019" proc-id="RM23G0E___0000ALA00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - FRONT SPEED SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Install the skid control sensor wire.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the p1 and/or p2 speed sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COLSPEC0" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="1.65in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A6-7 (FR+) - p1-2 (FR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A6-7 (FR+) - Body ground	</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A6-6 (FR-) - p1-1 (FR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A6-6 (FR-) - Body ground	</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.64in"/>
<colspec colname="COLSPEC1" colwidth="0.86in"/>
<colspec colname="COL2" colwidth="1.63in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A6-19 (FL+) - p2-2 (FL+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A6-19 (FL+) - Body ground	</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A6-18 (FL-) - p2-1 (FL-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A6-18 (FL-) - Body ground	</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XI90SQX_04_0008" fin="false">OK</down>
<right ref="RM000000XI90SQX_04_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0008" proc-id="RM23G0E___0000AL900001">
<testtitle>CHECK TERMINAL VOLTAGE (FR+, FL+)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the p1 and/or p2 speed sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C207217E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<title>for RH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>p1-2 (FR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for LH</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>p2-2 (FL+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Front Speed Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XI90SQX_04_0010" fin="true">OK</down>
<right ref="RM000000XI90SQX_04_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0023">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0010">
<testtitle>REPLACE FRONT SPEED SENSOR<xref label="Seep01" href="RM000001B2J01GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0018">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0032">
<testtitle>REPLACE SKID CONTROL SENSOR WIRE<xref label="Seep01" href="RM000001B2J01GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XI90SQX_04_0017">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>