<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12018_S0014" variety="S0014">
<name>CLUTCH</name>
<ttl id="12018_S0014_7B94V_T00EJ" variety="T00EJ">
<name>CLUTCH UNIT (for 1GR-FE)</name>
<para id="RM0000017E100LX" category="G" type-id="3001K" name-id="CL0N6-05" from="201207">
<name>INSPECTION</name>
<subpara id="RM0000017E100LX_01" type-id="01" category="01">
<s-1 id="RM0000017E100LX_01_0001" proc-id="RM23G0E___00008GN00000">
<ptxt>INSPECT CLUTCH DISC ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C173305" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the rivet depth.</ptxt>
<spec>
<title>Minimum rivet depth</title>
<specitem>
<ptxt>0.3 mm (0.0118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the depth is less than the minimum, replace the clutch disc assembly.</ptxt>
</s2>
<s2>
<ptxt>Using a dial gauge, measure the clutch disc assembly runout.</ptxt>
<figure>
<graphic graphicname="CL00373E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum runout</title>
<specitem>
<ptxt>0.7 mm (0.0276 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the runout is more than the maximum, replace the clutch disc assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017E100LX_01_0002" proc-id="RM23G0E___00008GO00000">
<ptxt>INSPECT CLUTCH COVER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C173310E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the depth and width of the diaphragm spring wear.</ptxt>
<spec>
<title>Maximum Wear</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Depth</ptxt>
</entry>
<entry>
<ptxt>Width</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>0.35 mm (0.0138 in.)</ptxt>
</entry>
<entry>
<ptxt>5.0 mm (0.197 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Width</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Depth</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the depth or width is more than the maximum, replace the clutch cover assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017E100LX_01_0003" proc-id="RM23G0E___00008GP00000">
<ptxt>INSPECT FLYWHEEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215503" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a dial gauge, check the flywheel sub-assembly runout.</ptxt>
<spec>
<title>Maximum runout</title>
<specitem>
<ptxt>0.1 mm (0.00393 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the runout is more than the maximum, replace the flywheel sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017E100LX_01_0004" proc-id="RM23G0E___00008GQ00000">
<ptxt>INSPECT CLUTCH RELEASE BEARING ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="F050790E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Check that the clutch release bearing moves smoothly without abnormal resistance by turning the sliding parts of the clutch release bearing (contact surfaces with the clutch cover) while applying force in the axial direction.</ptxt>
</s2>
<s2>
<ptxt>Inspect the clutch release bearing for damage or wear.</ptxt>
<ptxt>If necessary, replace the release bearing assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017E100LX_01_0005" proc-id="RM23G0E___00008GR00000">
<ptxt>INSPECT INPUT SHAFT BEARING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="Q002481E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Check that the bearing turns smoothly by hand by applying rotational force.</ptxt>
<ptxt>If the bearing is stuck or difficult to turn, replace the input shaft bearing.</ptxt>
<atten4>
<ptxt>The bearing is permanently lubricated and requires no cleaning or lubrication.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000017E100LX_01_0006" proc-id="RM23G0E___00008GS00000">
<ptxt>REPLACE INPUT SHAFT BEARING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215504E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using SST, remove the bearing.</ptxt>
<sst>
<sstitem>
<s-number>09303-35011</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST and a hammer, install a new bearing.</ptxt>
<figure>
<graphic graphicname="C215506E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09304-12012</s-number>
</sstitem>
</sst>
<atten4>
<ptxt>After installing the input shaft bearing to the engine side, make sure that it rotates smoothly.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>