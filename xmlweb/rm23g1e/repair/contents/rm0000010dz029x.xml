<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S001X" variety="S001X">
<name>DOOR LOCK</name>
<ttl id="12051_S001X_7B9B0_T00KO" variety="T00KO">
<name>DOOR CONTROL TRANSMITTER (w/o Entry and Start System)</name>
<para id="RM0000010DZ029X" category="G" type-id="3001K" name-id="DL33L-07" from="201207">
<name>INSPECTION</name>
<subpara id="RM0000010DZ029X_01" type-id="01" category="01">
<s-1 id="RM0000010DZ029X_01_0001" proc-id="RM23G0E___0000EMJ00000">
<ptxt>INSPECT DOOR CONTROL TRANSMITTER MODULE (except Separate Type)</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When replacing the transmitter battery, before starting work, remove static electricity that has built up in the body by touching, for example, the vehicle to prevent the door control transmitter module set from being damaged.</ptxt>
</atten3>
<s2>
<ptxt>Inspect the operation of the transmitter.</ptxt>
<s3>
<ptxt>Remove the battery (lithium battery) from the transmitter (See page <xref label="Seep01" href="RM0000010W9029X"/>).</ptxt>
</s3>
<s3>
<ptxt>Install a new or normal battery (lithium battery).</ptxt>
<figure>
<graphic graphicname="B185421" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>When a new or normal battery is not available, first connect 2 new 1.5 V batteries in series. Then connect leads to the batteries and apply 3 V to the transmitter shown in the illustration.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>From outside the vehicle, approximately 1 m (3.28 ft) from the driver side outside door handle, test the transmitter by pointing its key plate at the vehicle and pressing a transmitter switch.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The door lock can be operated via the transmitter.</ptxt>
</specitem>
<specitem>
<ptxt>The LED comes on more than once.</ptxt>
</specitem>
</spec>
<list1 type="unordered">
<item>
<ptxt>The operational area differs depending on the user, the way the transmitter is held and the location.</ptxt>
</item>
<item>
<ptxt>The weak electric waves from the transmitter may be affected if the area has strong electric waves or noise. The transmitter operational area may be reduced or the transmitter may not function.</ptxt>
</item>
</list1>
</s3>
</s2>
<s2>
<ptxt>Inspect the battery capacity.</ptxt>
<s3>
<ptxt>Remove the battery from the electrical key transmitter that does not operate (See page <xref label="Seep02" href="RM0000010W9029X"/>).</ptxt>
</s3>
<s3>
<ptxt>Attach a lead wire (0.6 mm (0.0236 in.) in diameter or less including wire sheath) with tape or equivalent to the negative terminal.</ptxt>
<atten3>
<ptxt>Do not wrap the lead wire around a terminal, wedge it between the terminals or solder it. A terminal may be deformed or damaged, and the battery will not be able to be installed correctly.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Carefully extend the lead wire out from the position shown in the illustration and install the previously removed transmitter battery.</ptxt>
<figure>
<graphic graphicname="B178438" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Using an oscilloscope, check the transmitter battery voltage waveform.</ptxt>
<figure>
<graphic graphicname="B243279E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>Measure the transmitter battery voltage while pressing the lock or unlock switch on the transmitter.</ptxt>
</atten4>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.04in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COLSPEC1" colwidth="1.03in"/>
<colspec colname="COLSPEC0" colwidth="1.03in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) - Battery negative (-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.5 V/DIV., 100 ms/DIV.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off, all doors closed and lock or unlock switch pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.2 to 3.2 V (Refer to waveform)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the transmitter battery.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010DZ029X_01_0002" proc-id="RM23G0E___0000EMK00000">
<ptxt>INSPECT TRANSMITTER BATTERY (for Separate Type)</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When replacing the transmitter battery, before starting work, remove static electricity that has built up in the body by touching, for example, the vehicle to prevent the door control transmitter module set from being damaged.</ptxt>
</atten3>
<s2>
<ptxt>Inspect the operation of the transmitter.</ptxt>
<s3>
<ptxt>Remove the battery (lithium battery) from the transmitter (See page <xref label="Seep01" href="RM0000010W9029X"/>).</ptxt>
</s3>
<s3>
<ptxt>Install a new or normal battery (lithium battery).</ptxt>
<figure>
<graphic graphicname="B239793" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>If a new or normal battery is not available, first connect 2 new 1.5 V batteries in series. Then connect leads to the batteries and apply 3 V to the transmitter shown in the illustration.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>From outside the vehicle, approximately 1 m (3.28 ft) away from the driver side outside door handle, test the transmitter by pointing its key plate at the vehicle and pressing a transmitter switch, or while carrying the transmitter, touch the inside of the handle and pull it.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The door lock can be operated via the transmitter.</ptxt>
</specitem>
<specitem>
<ptxt>The LED comes on more than once.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The operational area differs depending on the user, the way the transmitter is held and the location.</ptxt>
</item>
<item>
<ptxt>The weak electric waves from the transmitter may be affected if the area has strong electric waves or noise. The transmitter operational area may be reduced or the transmitter may not function.</ptxt>
</item>
</list1>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Inspect the battery capacity.</ptxt>
<figure>
<graphic graphicname="B239794" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the battery from the electrical key transmitter that does not operate (See page <xref label="Seep02" href="RM0000010W9029X"/>).</ptxt>
</s3>
<s3>
<ptxt>Attach a lead wire (0.6 mm (0.0236 in.) in diameter or less including wire sheath) with tape or equivalent to the negative terminal.</ptxt>
<atten3>
<ptxt>Do not wrap the lead wire around a terminal, wedge it between terminals or solder it. A terminal may be deformed or damaged, and the battery will not be able to be installed correctly.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Carefully extend the lead wire from the position shown in the illustration and install the previously removed transmitter battery.</ptxt>
</s3>
<s3>
<ptxt>Using an oscilloscope, check the transmitter battery voltage waveform.</ptxt>
<figure>
<graphic graphicname="B239795E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Measure the transmitter battery voltage while pressing the lock or unlock switch on the transmitter.</ptxt>
</atten4>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.04in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COLSPEC1" colwidth="1.03in"/>
<colspec colname="COLSPEC0" colwidth="1.03in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Battery positive (+) - Battery negative (-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.5 V/DIV., 100 ms/DIV.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off, all doors closed and lock or unlock switch pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.2 to 3.2 V (Refer to waveform)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the transmitter battery.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>