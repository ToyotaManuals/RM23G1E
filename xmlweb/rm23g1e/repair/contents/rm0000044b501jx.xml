<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0011" variety="S0011">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0011_7B93I_T00D6" variety="T00D6">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM0000044B501JX" category="C" type-id="305G5" name-id="CC3FM-03" from="201210">
<dtccode>P0571</dtccode>
<dtcname>Stop Light Switch Circuit Malfunction</dtcname>
<subpara id="RM0000044B501JX_01" type-id="60" category="03" proc-id="RM23G0E___000079E00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the brake pedal is depressed, the stop light switch sends a signal to the ECM. Upon receiving the signal, the ECM cancels cruise control. Even if there is a malfunction in the stop light signal circuit while cruise control is in operation, normal driving is maintained due to a fail-safe function.</ptxt>
<ptxt>When the brake pedal is depressed, positive voltage is applied to terminal STP of the ECM through the STOP fuse and stop light switch, and the ECM cancels cruise control.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0571</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The voltages of terminals ST1- and STP of the ECM are both below 1 V for 0.5 seconds or more.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Stop light switch circuit</ptxt>
</item>
<item>
<ptxt>Stop light switch assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000044B501JX_02" type-id="32" category="03" proc-id="RM23G0E___000079F00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E194929E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000044B501JX_03" type-id="51" category="05" proc-id="RM23G0E___000079G00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When the ECM is replaced with a new one, initialization must be performed (See page <xref label="Seep01" href="RM0000044H500AX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM0000044B501JX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000044B501JX_04_0001" proc-id="RM23G0E___000079H00001">
<testtitle>CHECK HARNESS AND CONNECTOR (STOP LIGHT SWITCH - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A5 switch connector.</ptxt>
<figure>
<graphic graphicname="E182722E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A5-2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>A5-4 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Stop Light Switch Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044B501JX_04_0002" fin="false">OK</down>
<right ref="RM0000044B501JX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044B501JX_04_0002" proc-id="RM23G0E___000079I00001">
<testtitle>INSPECT STOP LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the stop light switch (See page <xref label="Seep01" href="RM000003QJT00TX"/>).</ptxt>
<figure>
<graphic graphicname="E200528E04" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 8.3 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>3 - 4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Not pushed</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pushed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044B501JX_04_0003" fin="false">OK</down>
<right ref="RM0000044B501JX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044B501JX_04_0003" proc-id="RM23G0E___000079J00001">
<testtitle>CHECK HARNESS AND CONNECTOR (STOP LIGHT SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A5 switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G55*1 and G56*1 or G57*2 ECM connectors.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 1GR-FE</ptxt>
</item>
<item>
<ptxt>*2: for 1KD-FTV</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for 1GR-FE</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A5-3 - G55-8 (ST1-)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A5-1 - G56-18 (STP)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A5-3 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A5-1 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1KD-FTV</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A5-3 - G57-9 (ST1-)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A5-1 - G57-8 (STP)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A5-3 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A5-1 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.06in"/>
<colspec colname="COL2" colwidth="2.07in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for 1KD-FTV)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044B501JX_04_0006" fin="true">A</down>
<right ref="RM0000044B501JX_04_0007" fin="true">B</right>
<right ref="RM0000044B501JX_04_0004" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000044B501JX_04_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000044B501JX_04_0005">
<testtitle>REPLACE STOP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM000003QJT00TX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044B501JX_04_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044B501JX_04_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001YX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>