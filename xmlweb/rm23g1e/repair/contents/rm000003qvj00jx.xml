<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002E" variety="S002E">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002E_7B9H0_T00QO" variety="T00QO">
<name>HEADLIGHT ASSEMBLY (for Halogen Headlight)</name>
<para id="RM000003QVJ00JX" category="A" type-id="30014" name-id="LE48Q-02" from="201207">
<name>INSTALLATION</name>
<subpara id="RM000003QVJ00JX_02" type-id="11" category="10" proc-id="RM23G0E___0000J2H00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003QVJ00JX_01" type-id="01" category="01">
<s-1 id="RM000003QVJ00JX_01_0001" proc-id="RM23G0E___0000J2D00000">
<ptxt>INSTALL HEADLIGHT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the clamp to install the headlight.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt and 2 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003QVJ00JX_01_0010" proc-id="RM23G0E___0000J2E00000">
<ptxt>INSTALL FRONT BUMPER BAR REINFORCEMENT LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front bumper reinforcement with the 6 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>306</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003QVJ00JX_01_0012" proc-id="RM23G0E___0000J2G00000">
<ptxt>INSTALL FRONT BUMPER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the lower front bumper cover with the 5 bolts and clip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003QVJ00JX_01_0011" proc-id="RM23G0E___0000J2F00000">
<ptxt>INSTALL RADIATOR GRILLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator grille (See page <xref label="Seep01" href="RM0000038K500TX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003QVJ00JX_01_0003" proc-id="RM23G0E___0000J2900000">
<ptxt>PREPARATION FOR HEADLIGHT AIMING ADJUSTMENT (Using a screen)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Prepare the vehicle (except China and Taiwan):</ptxt>
<figure>
<graphic graphicname="E198096E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>25 or 3 m</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Place the vehicle in a location that is dark enough to clearly observe the cutoff line. The cutoff line is a distinct line, below which light from the headlights can be observed and above which it cannot.</ptxt>
</item>
<item>
<ptxt>Place the vehicle at a 90° angle to the wall.</ptxt>
</item>
<item>
<ptxt>Create a 25 m (82.0 ft.) distance between the vehicle (headlight bulb center) and wall.</ptxt>
</item>
<item>
<ptxt>Make sure that the vehicle is on a level surface.</ptxt>
</item>
<item>
<ptxt>Bounce the vehicle up and down to settle the suspension.</ptxt>
</item>
</list1>
<atten3>
<ptxt>A distance of 25 m (82.0 ft.) between the vehicle (headlight bulb center) and wall is necessary for proper aim adjustment. If unavailable, secure a distance of exactly 3 m (9.84 ft.) for the check and adjustment (the target zone will change with the distance, so follow the instructions in the illustration).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Prepare the vehicle (for China and Taiwan):</ptxt>
<figure>
<graphic graphicname="E198096E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 or 3 m</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Place the vehicle in a location that is dark enough to clearly observe the cutoff line. The cutoff line is a distinct line, below which light from the headlights can be observed and above which it cannot.</ptxt>
</item>
<item>
<ptxt>Place the vehicle at a 90° angle to the wall.</ptxt>
</item>
<item>
<ptxt>Create a 10 m (32.8 ft) distance between the vehicle (headlight bulb center) and the wall.</ptxt>
</item>
<item>
<ptxt>Make sure that the vehicle is on a level surface.</ptxt>
</item>
<item>
<ptxt>Bounce the vehicle up and down to settle the suspension.</ptxt>
</item>
</list1>
<atten3>
<ptxt>A distance of 10 m (32.8 ft) between the vehicle (headlight bulb center) and the wall is necessary for proper aim adjustment. If unavailable, secure a distance of exactly 3 m (9.84 ft) for check and adjustment. (The target zone will change with the distance so follow the instructions in the illustration.)</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Prepare a piece of thick white paper approximately 2 m (6.56 ft.) (height) x 4 m (13.1 ft.) (width) to use as a screen.</ptxt>
</s2>
<s2>
<ptxt>Draw a vertical line down the center of the screen (V line).</ptxt>
</s2>
<s2>
<ptxt>except China and Taiwan:</ptxt>
<ptxt>Set the screen as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E198099E04" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Stand the screen perpendicular to the ground.</ptxt>
</item>
<item>
<ptxt>Align the V line on the screen with the center of the vehicle.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>for China and Taiwan:</ptxt>
<ptxt>Set the screen as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E198099E03" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Stand the screen perpendicular to the ground.</ptxt>
</item>
<item>
<ptxt>Align the V line on the screen with the center of the vehicle.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Prepare a piece of thick white paper approximately 2 m (6.56 ft.) (height) x 4 m (13.1 ft.) (width) to use as a screen.</ptxt>
</s2>
<s2>
<ptxt>Draw a vertical line down the center of the screen (V line).</ptxt>
</s2>
<s2>
<ptxt>Draw base lines (H, V LH, and V RH Lines) on the screen as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E257183E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>V LH Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>V Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>V RH Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>H Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The base lines differ for "low beam inspection" and "high beam inspection".</ptxt>
</item>
<item>
<ptxt>Mark the headlight bulb center marks on the screen. If the center mark cannot be observed on the headlight, use the center of the headlight bulb or the name of the manufacturer marked on the headlight as the center mark. </ptxt>
</item>
</list1>
</atten4>
<s3>
<ptxt>H Line (Headlight):</ptxt>
<ptxt>Draw a horizontal line across the screen so that it passes through the center marks. The H line should be at the same height as the headlight bulb center marks of the low beam headlights.</ptxt>
</s3>
<s3>
<ptxt>V LH Line, V RH Line (Center mark position of the left-hand (LH) and right-hand (RH) headlights):</ptxt>
<ptxt>Draw two vertical lines so that they intersect the H line at each center mark (aligned with the center of the low beam headlight bulbs).</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003QVJ00JX_01_0004" proc-id="RM23G0E___0000J2A00000">
<ptxt>INSPECT HEADLIGHT AIMING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Cover the headlight or disconnect the connector of the headlight on the opposite side to prevent light from the headlight that is not being inspected from affecting the headlight aiming.</ptxt>
<atten3>
<ptxt>Do not keep the headlight covered for more than 3 minutes. The headlight lens is made of synthetic resin, which may melt or be damaged due to excessive heat.</ptxt>
</atten3>
<atten4>
<ptxt>When checking the aim of the high beam headlight, cover the low beam headlight or disconnect the connector.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>except China and Taiwan:</ptxt>
<ptxt>Turn on the headlight and check if the cutoff line matches the preferred cutoff line in the following illustration.</ptxt>
<figure>
<graphic graphicname="E234067E10" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Since the low beam light and the high beam light are a unit, if the aim on the low beam is correct, the high beam should also be correct. However, check both beams just to make sure.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 25 m (82.0 ft.):</ptxt>
<ptxt>The low beam cutoff line should be within 48 mm (1.89 in.) and 698 mm (2.29 ft.) below the H line as well as 249 mm (9.80 in.) left or right of the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The low beam cutoff line should be within 6 mm (0.24 in.) and 84 mm (3.30 in.) below the H line as well as 30 mm (1.18 in.) left or right of the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 25 m (82.0 ft.):</ptxt>
<ptxt>The horizontal line of the preferred low beam cutoff line is 249 mm (9.80 in.) below the H line and point A of the preferred low beam cutoff line is on the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The horizontal line of the preferred low beam cutoff line is 30 mm (1.18 in.) below the H line and point A of the preferred low beam cutoff line is on the V LH or V RH line.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>for China:</ptxt>
<ptxt>Turn on the headlight and check if the cutoff line matches the preferred cutoff line in the following illustration.</ptxt>
<figure>
<graphic graphicname="E234067E07" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Since the low beam light and the high beam light are a unit, if the aim on the low beam is correct, the high beam should also be correct. However, check both beams just to make sure.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The low beam cutoff line should be within 94 mm (3.70 in.) and 285 mm (11.2 in.) below the H line as well as 99 mm (3.89 in.) left or right of the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The low beam cutoff line should be within 28 mm (1.10 in.) and 85 mm (3.34 in.) below the H line as well as 30 mm (1.18 in.) left or right of the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The horizontal line of the preferred low beam cutoff line is 189 mm (7.44 in.) below the H line and point A of the preferred low beam cutoff line is on the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The horizontal line of the preferred low beam cutoff line is 57 mm (2.24 in.) below the H line and point A of the preferred low beam cutoff line is on the V LH or V RH line.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>for Taiwan:</ptxt>
<ptxt>Turn on the headlight and check if the cutoff line matches the preferred cutoff line in the following illustration.</ptxt>
<figure>
<graphic graphicname="E234067E11" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Since the low beam light and the high beam light are a unit, if the aim on the low beam is correct, the high beam should also be correct. However, check both beams just to make sure.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The low beam cutoff line should be within 19 mm (0.74 in.) and 150 mm (5.90 in.) below the H line as well as 66 mm (2.59 in.) left or right of the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The low beam cutoff line should be within 6 mm (0.23 in.) and 45 mm (1.77 in.) below the H line as well as 20 mm (0.78 in.) left or right of the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The horizontal line of the preferred low beam cutoff line is 99 mm (3.89 in.) below the H line and point A of the preferred low beam cutoff line is on the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The horizontal line of the preferred low beam cutoff line is 30 mm (1.18 in.) below the H line and point A of the preferred low beam cutoff line is on the V LH or V RH line.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>except China and Taiwan:</ptxt>
<ptxt>Turn on the high beams and check if the center of intensity for each high beam matches the center of intensity in the illustration.</ptxt>
<figure>
<graphic graphicname="E235330E11" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Since the low beam light and the high beam light are a unit, if the aim on the low beam is correct, the high beam should also be correct. However, check both beams just to make sure.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 25 m (82.0 ft.):</ptxt>
<ptxt>The high beam center of intensity should be within 175 mm (6.88 in.) above and 249 mm (9.80 in.) below the H line as well as 497 mm (1.63 ft.) left or right of the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The high beam center of intensity should be within 21 mm (0.82 in.) above and 30 mm (1.18 in.) below the H line as well as 60 mm (2.36 in.) left or right of the V LH or V RH line.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>for China:</ptxt>
<ptxt>Turn on the high beams and check if the center of intensity for each high beam matches the center of intensity in the illustration.</ptxt>
<figure>
<graphic graphicname="E256074E02" width="7.106578999in" height="4.7836529in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Since the low beam light and the high beam light are a unit, if the aim on the low beam is correct, the high beam should also be correct. However, check both beams just to make sure.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The high beam center of intensity should be within 47 mm (1.85 in.) below the H line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The high beam center of intensity should be within 14 mm (0.55 in.) below the H line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The high beam center of intensity for the headlight assembly LH should be within 169 mm (6.65 in.) left and 349 mm (1.14 ft.) right of the V LH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The high beam center of intensity for the headlight assembly LH should be within 51 mm (2.0 in.) left and 105 mm (4.13 in.) right of the V LH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The high beam center of intensity for the headlight assembly RH should be within 349 mm (1.14 ft.) left or right of the V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The high beam center of intensity for the headlight assembly RH should be within 105 mm (4.13 in.) left or right of the V RH line.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>for Taiwan:</ptxt>
<ptxt>Turn on the high beams and check if the center of intensity for each high beam matches the center of intensity in the illustration.</ptxt>
<figure>
<graphic graphicname="E235330E13" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Since the low beam light and the high beam light are a unit, if the aim on the low beam is correct, the high beam should also be correct. However, check both beams just to make sure.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The high beam center of intensity should be within 70 mm (2.75 in.) above and 99 mm (3.89 in.) below the H line as well as 198 mm (7.79 in.) left or right of the V LH or V RH line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The high beam center of intensity should be within 21 mm (0.82 in.) above and 30 mm (1.18 in.) below the H line as well as 59 mm (2.32 in.) left or right of the V LH or V RH line.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003QVJ00JX_01_0005" proc-id="RM23G0E___0000J2B00000">
<ptxt>ADJUST HEADLIGHT AIMING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Adjust the aim vertically:</ptxt>
<ptxt>Adjust the aim of each headlight to the specified range by turning each aiming screw with a screwdriver.</ptxt>
<figure>
<graphic graphicname="E201129E01" width="2.775699831in" height="5.787629434in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Headlight Leveling</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Headlight Leveling</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vertical Aiming</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Horizontal Aiming</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Screwdriver</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The final turn of the aiming screw should be made in the clockwise direction. If the screw is tightened excessively, loosen it, and then retighten it so that the final turn of the screw is in the clockwise direction.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Since the low beam light and the high beam light are a unit, if the aim on the low beam is correct, the high beam should also be correct. However, check both beams just to make sure.</ptxt>
</item>
<item>
<ptxt>If it is not possible to correctly adjust headlight aim, check the bulb, headlight unit and headlight unit reflector installation.</ptxt>
</item>
<item>
<ptxt>The headlight aim moves up when turning the vertical aiming screw clockwise, and moves down when turning the vertical aiming screw counterclockwise. The headlight aim moves right when turning the horizontal aiming screw clockwise, and moves left when turning the horizontal aiming screw counterclockwise.</ptxt>
</item>
<item>
<ptxt>Confirm the direction of rotation of the aiming screw by observing it while it is being adjusted. Due to the position of the screwdriver, the direction of rotation of the adjusting screw can be different than the direction of rotation of the screwdriver being used to adjust it.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Adjust the aim horizontally:</ptxt>
<ptxt>Adjust the aim of each headlight to the specified range by turning each aiming screw with a screwdriver.</ptxt>
<atten3>
<ptxt>The final turn of the aiming screw should be made in the clockwise direction. If the screw is tightened excessively, loosen it, and then retighten it so that the final turn of the screw is in the clockwise direction.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Since the low beam light and the high beam light are a unit, if the aim on the low beam is correct, the high beam should also be correct. However, check both beams just to make sure.</ptxt>
</item>
<item>
<ptxt>If it is not possible to correctly adjust headlight aim, check the bulb, headlight unit and headlight unit reflector installation.</ptxt>
</item>
<item>
<ptxt>Confirm the direction of rotation of the aiming screw by observing it while it is being adjusted. Due to the position of the screwdriver, the direction of rotation of the adjusting screw can be different than the direction of rotation of the screwdriver being used to adjust it.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000003QVJ00JX_01_0006" proc-id="RM23G0E___0000IW700000">
<ptxt>PREPARATION FOR FOG LIGHT AIMING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Prepare the vehicle (except China and Taiwan):</ptxt>
<figure>
<graphic graphicname="E198126E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>25 or 3 m</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Place the vehicle in a location that is dark enough to clearly observe the cutoff line. The cutoff line is a distinct line, below which light from the headlights can be observed and above which it cannot.</ptxt>
</item>
<item>
<ptxt>Place the vehicle at a 90° angle to the wall.</ptxt>
</item>
<item>
<ptxt>Create a 25 m (82.0 ft.) distance between the vehicle (headlight bulb center) and wall.</ptxt>
</item>
<item>
<ptxt>Make sure that the vehicle is on a level surface.</ptxt>
</item>
<item>
<ptxt>Bounce the vehicle up and down to settle the suspension.</ptxt>
</item>
</list1>
<atten3>
<ptxt>A distance of 25 m (82.0 ft.) between the vehicle (headlight bulb center) and wall is necessary for proper aim adjustment. If unavailable, secure a distance of exactly 3 m (9.84 ft.) for the check and adjustment (the target zone will change with the distance, so follow the instructions in the illustration).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Prepare the vehicle (for China and Taiwan):</ptxt>
<figure>
<graphic graphicname="E198126E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 or 3 m</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Place the vehicle in a location that is dark enough to clearly observe the cutoff line. The cutoff line is a distinct line, below which light from the headlights can be observed and above which it cannot.</ptxt>
</item>
<item>
<ptxt>Place the vehicle at a 90° angle to the wall.</ptxt>
</item>
<item>
<ptxt>Create a 10 m (32.8 ft) distance between the vehicle (headlight bulb center) and the wall.</ptxt>
</item>
<item>
<ptxt>Make sure that the vehicle is on a level surface.</ptxt>
</item>
<item>
<ptxt>Bounce the vehicle up and down to settle the suspension.</ptxt>
</item>
</list1>
<atten3>
<ptxt>A distance of 10 m (32.8 ft) between the vehicle (headlight bulb center) and the wall is necessary for proper aim adjustment. If unavailable, secure a distance of exactly 3 m (9.84 ft) for check and adjustment. (The target zone will change with the distance so follow the instructions in the illustration.)</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Prepare a piece of thick white paper approximately 2 m (6.56 ft.) (height) x 4 m (13.1 ft.) (width) to use as a screen.</ptxt>
</s2>
<s2>
<ptxt>Draw a vertical line down the center of the screen (V line).</ptxt>
</s2>
<s2>
<ptxt>except China and Taiwan:</ptxt>
<ptxt>Set the screen as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E198127E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Stand the screen perpendicular to the ground.</ptxt>
</item>
<item>
<ptxt>Align the V line on the screen with the center of the vehicle.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>for China and Taiwan:</ptxt>
<ptxt>Set the screen as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E198127E03" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Stand the screen perpendicular to the ground.</ptxt>
</item>
<item>
<ptxt>Align the V line on the screen with the center of the vehicle.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Prepare a piece of thick white paper approximately 2 m (6.56 ft.) (height) x 4 m (13.1 ft.) (width) to use as a screen.</ptxt>
</s2>
<s2>
<ptxt>Draw a vertical line down the center of the screen (V line).</ptxt>
</s2>
<s2>
<ptxt>Draw base lines (H, V LH, and V RH Lines) on the screen as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E257183E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>V LH Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>V Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>V RH Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>H Line</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The base lines differ for "low beam inspection" and "high beam inspection".</ptxt>
</item>
<item>
<ptxt>Mark the headlight bulb center marks on the screen. If the center mark cannot be observed on the headlight, use the center of the headlight bulb or the name of the manufacturer marked on the headlight as the center mark. </ptxt>
</item>
</list1>
</atten4>
<s3>
<ptxt>H Line (Headlight):</ptxt>
<ptxt>Draw a horizontal line across the screen so that it passes through the center marks. The H line should be at the same height as the headlight bulb center marks of the low beam headlights.</ptxt>
</s3>
<s3>
<ptxt>V LH Line, V RH Line (Center mark position of the left-hand (LH) and right-hand (RH) headlights):</ptxt>
<ptxt>Draw two vertical lines so that they intersect the H line at each center mark (aligned with the center of the low beam headlight bulbs).</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003QVJ00JX_01_0007" proc-id="RM23G0E___0000IW800000">
<ptxt>INSPECT FOG LIGHT AIMING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Cover the fog light or disconnect the connector of the fog light on the opposite side to prevent light from the fog light that is not being inspected from affecting the fog light aiming inspection.</ptxt>
<atten3>
<ptxt>Do not keep the fog light covered for more than 3 minutes. The fog light lens is made of synthetic resin, which may melt or be damaged due to excessive heat.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>except China and Taiwan:</ptxt>
<ptxt>Turn on the fog light and check if the cutoff line falls within the specified area in the following illustration.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the alignment distance is 25 m (82 ft.):</ptxt>
<ptxt>The upper edge of the hot zone for the fog light should be 248 mm (9.76 in.) below the H line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The upper edge of the hot zone for the fog light should be 29 mm (1.14 in.) below the H line.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="I041221E59" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>for China and Taiwan:</ptxt>
<ptxt>Turn on the fog light and check if the cutoff line falls within the specified area in the following illustration.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the alignment distance is 10 m (32.8 ft.):</ptxt>
<ptxt>The upper edge of the hot zone for the fog light should be 99 mm (3.89 in.) below the H line.</ptxt>
</item>
<item>
<ptxt>If the alignment distance is 3 m (9.84 ft.):</ptxt>
<ptxt>The upper edge of the hot zone for the fog light should be 29 mm (1.14 in.) below the H line.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="I041221E60" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003QVJ00JX_01_0008" proc-id="RM23G0E___0000IW900000">
<ptxt>ADJUST FOG LIGHT AIMING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Adjust the aim vertically.</ptxt>
<figure>
<graphic graphicname="E198128E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Aiming Screw</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Adjust the aim of each fog light to the specified range by turning the aiming screw with a screwdriver.</ptxt>
<atten3>
<ptxt>The final turn of the aiming screw should be made in the clockwise direction. If the screw is tightened excessively, loosen it, and then retighten it so that the final turn of the screw is in the clockwise direction.</ptxt>
</atten3>
<atten4>
<ptxt>If it is not possible to correctly adjust fog light aim, check the bulb, fog light unit and fog light unit reflector installation.</ptxt>
</atten4>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>