<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B8_T00KW" variety="T00KW">
<name>THEFT DETERRENT SYSTEM</name>
<para id="RM00000215L01SX" category="J" type-id="3026K" name-id="TD4Z1-03" from="201210">
<dtccode/>
<dtcname>Glass Breakage Sensor Circuit</dtcname>
<subpara id="RM00000215L01SX_01" type-id="60" category="03" proc-id="RM23G0E___0000EXB00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the glass breakage sensor detects that the back door glass, quarter window assembly LH* or quarter window assembly RH* is broken, the sensor sounds the alarm for 30 seconds.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*: for 5 Door</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM00000215L01SX_02" type-id="32" category="03" proc-id="RM23G0E___0000EXC00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B240859E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="B240860E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000215L01SX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000215L01SX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000215L01SX_05_0001" proc-id="RM23G0E___0000EXD00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (GLASS BREAKAGE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000000W0S02DX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Sealed Glas Brak Sen</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Glass breakage sensor detection / With or Without</ptxt>
</entry>
<entry valign="middle">
<ptxt>With: Glass breakage sensor operating</ptxt>
<ptxt>Without: Glass breakage sensor not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>"With" (glass breakage sensor operating) appears on screen.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 5 Door)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for 3 Door)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000215L01SX_05_0018" fin="true">A</down>
<right ref="RM00000215L01SX_05_0012" fin="false">B</right>
<right ref="RM00000215L01SX_05_0016" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM00000215L01SX_05_0012" proc-id="RM23G0E___0000EXG00001">
<testtitle>INSPECT QUARTER WINDOW ASSEMBLY LH (GLASS BREAKAGE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the R25 sensor connector.</ptxt>
<figure>
<graphic graphicname="B240861E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (SSR+) - 2 (SSR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01SX_05_0005" fin="false">OK</down>
<right ref="RM00000215L01SX_05_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01SX_05_0005" proc-id="RM23G0E___0000EXF00001">
<testtitle>INSPECT QUARTER WINDOW ASSEMBLY RH (GLASS BREAKAGE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the Q20 sensor connector.</ptxt>
<figure>
<graphic graphicname="B240861E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (SSR+) - 2 (SSR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01SX_05_0013" fin="false">OK</down>
<right ref="RM00000215L01SX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01SX_05_0013" proc-id="RM23G0E___0000EXH00001">
<testtitle>INSPECT BACK DOOR GLASS (GLASS BREAKAGE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the Y2 sensor connector.</ptxt>
<figure>
<graphic graphicname="B240863E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3 (SSR+) - 1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01SX_05_0003" fin="false">OK</down>
<right ref="RM00000215L01SX_05_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01SX_05_0003" proc-id="RM23G0E___0000EXE00001">
<testtitle>CHECK HARNESS AND CONNECTOR (GLASS BREAKAGE SENSOR - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the R25, Q20 and Y2 sensor connectors.</ptxt>
</test1>
<test1>
<ptxt>Remove the main body ECU (See page <xref label="Seep01" href="RM000003WBO03DX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R25-1 (SSR+) - A-19 (GPBS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Q20-1 (SSR+) - R25-2 (SSR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Y2-3 (SSR+) - Q20-2 (SSR-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R25-1 (SSR+) or A-19 (GPBS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Q20-1 (SSR+) or R25-2 (SSR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Y2-3 (SSR+) or Q20-2 (SSR-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Y2-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000215L01SX_05_0007" fin="true">OK</down>
<right ref="RM00000215L01SX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01SX_05_0016" proc-id="RM23G0E___0000EXJ00001">
<testtitle>INSPECT BACK DOOR GLASS (GLASS BREAKAGE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>w/ Glass Hatch Opener System:</ptxt>
<figure>
<graphic graphicname="B240863E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test2>
<ptxt>Disconnect the Y2 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3 (SSR+) - 1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>w/o Glass Hatch Opener System:</ptxt>
<figure>
<graphic graphicname="B240862E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test2>
<ptxt>Disconnect the X8 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2 (SSR+) - 1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (w/ Glass Hatch Opener System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (w/o Glass Hatch Opener System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000215L01SX_05_0015" fin="false">A</down>
<right ref="RM00000215L01SX_05_0011" fin="true">B</right>
<right ref="RM00000215L01SX_05_0017" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000215L01SX_05_0015" proc-id="RM23G0E___0000EXI00001">
<testtitle>CHECK HARNESS AND CONNECTOR (GLASS BREAKAGE SENSOR - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>w/ Glass Hatch Opener System:</ptxt>
<test2>
<ptxt>Disconnect the Y2 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Remove the main body ECU (See page <xref label="Seep01" href="RM000003WBO03DX"/>).</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Y2-3 (SSR+) - A-19 (GPBS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Y2-3 (SSR+) or A-19 (GPBS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Y2-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>w/o Glass Hatch Opener System:</ptxt>
<test2>
<ptxt>Disconnect the X8 sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Remove the main body ECU (See page <xref label="Seep02" href="RM000003WBO03DX"/>).</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>X8-2 (SSR+) - A-19 (GPBS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>X8-2 (SSR+) or A-19 (GPBS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>X8-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000215L01SX_05_0007" fin="true">OK</down>
<right ref="RM00000215L01SX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000215L01SX_05_0007">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO03DX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000215L01SX_05_0018">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002TGC01FX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000215L01SX_05_0010">
<testtitle>REPLACE QUARTER WINDOW ASSEMBLY LH (GLASS BREAKAGE SENSOR)<xref label="Seep01" href="RM00000192A04JX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000215L01SX_05_0009">
<testtitle>REPLACE QUARTER WINDOW ASSEMBLY RH (GLASS BREAKAGE SENSOR)<xref label="Seep01" href="RM00000192A04JX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000215L01SX_05_0011">
<testtitle>REPLACE BACK DOOR GLASS (GLASS BREAKAGE SENSOR)<xref label="Seep01" href="RM0000024NS06AX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000215L01SX_05_0017">
<testtitle>REPLACE BACK DOOR GLASS (GLASS BREAKAGE SENSOR)<xref label="Seep01" href="RM0000024NS06CX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000215L01SX_05_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>