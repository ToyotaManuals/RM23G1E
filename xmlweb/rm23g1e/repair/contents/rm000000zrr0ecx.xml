<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM000000ZRR0ECX" category="C" type-id="803LT" name-id="BC86K-13" from="201210">
<dtccode>C1290</dtccode>
<dtcname>Steering Angle Sensor Zero Point Malfunction</dtcname>
<subpara id="RM000000ZRR0ECX_01" type-id="60" category="03" proc-id="RM23G0E___0000ACJ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU acquires steering angle sensor zero point every time the ignition switch is turned to ON and the vehicle is driven at 40 km/h (25 mph) or more for approximately 10 seconds. The ECU also stores the previous zero point.</ptxt>
<ptxt>If the front wheel alignment or steering wheel position is adjusted without disconnecting the cable from the negative (-) battery terminal, or if the yaw rate and acceleration sensor zero point is not acquired after the adjustments have been completed, the skid control ECU detects the difference between the previous zero point and newly acquired zero point and stores this DTC to indicate poor adjustment.</ptxt>
<ptxt>The steering angle sensor zero point malfunction warning is canceled by turning the ignition switch off.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1290</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The steering angle sensor zero point calibration position vastly differs from the recorded value.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Yaw rate and acceleration sensor zero point calibration incomplete</ptxt>
</item>
<item>
<ptxt>Poor adjustment of the centered position of the steering wheel</ptxt>
</item>
<item>
<ptxt>Poor adjustment of front wheel alignment</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000ZRR0ECX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000ZRR0ECX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000ZRR0ECX_03_0001" proc-id="RM23G0E___0000ACK00001">
<testtitle>PERFORM ZERO POINT CALIBRATION OF YAW RATE AND ACCELERATION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform zero point calibration of the yaw rate and acceleration sensor (See page <xref label="Seep01" href="RM00000452J00KX"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the stored zero point of the yaw rate and acceleration sensor is cleared, steering angle sensor zero point is also cleared.</ptxt>
</item>
<item>
<ptxt>If the zero point and output value of the yaw rate and acceleration sensor and the output value of the speed sensors are not normal, steering angle sensor zero point cannot be acquired normally even if the vehicle is driven straight ahead at 40 km/h (25 mph) or more.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000ZRR0ECX_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000ZRR0ECX_03_0002" proc-id="RM23G0E___0000ACL00001">
<testtitle>CHECK STEERING ANGLE SENSOR ZERO POINT CALIBRATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Drive the vehicle straight ahead at 40 km/h (25 mph) or more for at least 10 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the centered position of the steering wheel is correctly set while driving straight ahead.</ptxt>
<atten4>
<ptxt>If the front wheel alignment and steering position are adjusted as a result of an abnormal centered position of the steering wheel, acquire the yaw rate and acceleration sensor zero point again after the adjustments are completed.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The centered position of the steering wheel is correctly set.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000ZRR0ECX_03_0003" fin="true">OK</down>
<right ref="RM000000ZRR0ECX_03_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000ZRR0ECX_03_0003">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000ZRR0ECX_03_0004">
<testtitle>ADJUST FRONT WHEEL ALIGNMENT OR STEERING POSITION</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>