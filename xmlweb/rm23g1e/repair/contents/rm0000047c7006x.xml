<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9DI_T00N6" variety="T00N6">
<name>FRONT SEAT CUSHION HEATER (for Walk in Seat Type)</name>
<para id="RM0000047C7006X" category="A" type-id="80001" name-id="SE8M4-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000047C7006X_02" type-id="11" category="10" proc-id="RM23G0E___0000H0F00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the RH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000047C7006X_01" type-id="01" category="01">
<s-1 id="RM0000047C7006X_01_0008" proc-id="RM23G0E___0000H0E00001">
<ptxt>REMOVE FRONT SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front seat assembly (See page <xref label="Seep01" href="RM0000046B8006X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000047C7006X_01_0009" proc-id="RM23G0E___0000GYT00001">
<ptxt>REMOVE RECLINING ADJUSTER RELEASE HANDLE RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241192E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Raise the reclining adjuster release handle to reveal the claw. Using a screwdriver, detach the claw and remove the handle.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000047C7006X_01_0010" proc-id="RM23G0E___0000GYU00001">
<ptxt>REMOVE FRONT SEAT CUSHION SHIELD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241194" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 5 claws and 2 clips.</ptxt>
</s2>
<s2>
<ptxt>Detach the cable clamp and remove the cushion shield.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000047C7006X_01_0011" proc-id="RM23G0E___0000GYV00001">
<ptxt>REMOVE FRONT SEAT INNER CUSHION SHIELD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241195" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 5 claws and remove the cushion shield.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000047C7006X_01_0013" proc-id="RM23G0E___0000GZB00001">
<ptxt>REMOVE FRONT SEAT INNER BELT ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 connectors and detach the 3 clamps.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and front seat inner belt assembly RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000047C7006X_01_0005" proc-id="RM23G0E___0000H0D00001">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B241196" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the rubber band from the seat cushion spring.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B242236" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the seat heater connector and detach the wire harness clamp.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B242244E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<s3>
<ptxt>Detach the fastening tape and open the cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Disconnect the airbag wire harness.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B241202" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a moulding remover, remove the clip.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241200" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Detach the hook.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B239243" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the seat heater connector and detach the 3 wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>Detach the hooks.</ptxt>
<figure>
<graphic graphicname="B239180" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<figure>
<graphic graphicname="B239181" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the claw and disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the seat cushion cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000047C7006X_01_0012" proc-id="RM23G0E___0000GYZ00001">
<ptxt>REMOVE SEPARATE TYPE FRONT SEAT CUSHION COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239182" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the hog rings and seat cushion cover from the seat cushion pad.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000047C7006X_01_0007" proc-id="RM23G0E___0000GZ700001">
<ptxt>REMOVE FRONT SEAT CUSHION HEATER ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239184E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins and remove the front seat cushion heater from the front seat cushion cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>