<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AL_T00K9" variety="T00K9">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002S8G02LX" category="C" type-id="304N6" name-id="NW1MO-08" from="201207" to="201210">
<dtccode>B2287</dtccode>
<dtcname>LIN Communication Master Malfunction</dtcname>
<dtccode>B278C</dtccode>
<dtcname>Lost Communication with Power Source Control</dtcname>
<subpara id="RM000002S8G02LX_01" type-id="60" category="03" proc-id="RM23G0E___0000D9R00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>DTC B2287 is stored when there is an open or short circuit or an ECU communication malfunction between the power management control ECU and certification ECU.</ptxt>
</item>
<item>
<ptxt>DTC B278C is stored when LIN communication between the certification ECU and power management control ECU stops for 10 seconds or more.</ptxt>
</item>
</list1>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.74in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.22in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2287</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is an open or short circuit or an ECU communication malfunction between the power management control ECU and certification ECU.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Certification ECU</ptxt>
</item>
<item>
<ptxt>Power management control ECU</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B278C</ptxt>
</entry>
<entry valign="middle">
<ptxt>No communication between the certification ECU and power management control ECU for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Certification ECU</ptxt>
</item>
<item>
<ptxt>Power management control ECU</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002S8G02LX_02" type-id="32" category="03" proc-id="RM23G0E___0000D9S00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C212392E05" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002S8G02LX_03" type-id="51" category="05" proc-id="RM23G0E___0000D9T00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle and turn a courtesy light switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and bulbs for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When DTC B2287 and B2785 are output simultaneously, perform troubleshooting for DTC B2785 first (See page <xref label="Seep01" href="RM000002XHV079X"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002S8G02LX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002S8G02LX_04_0001" proc-id="RM23G0E___0000D9U00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8G02LX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0002" proc-id="RM23G0E___0000D9V00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2287 or B278C is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8G02LX_04_0006" fin="true">OK</down>
<right ref="RM000002S8G02LX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0003" proc-id="RM23G0E___0000D9W00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU - POWER MANAGEMENT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G38 certification ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G51 power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition </ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-29 (LIN) - G51-24 (LIN2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-29 (LIN) or G51-24 (LIN2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8G02LX_04_0010" fin="false">OK</down>
<right ref="RM000002S8G02LX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0010" proc-id="RM23G0E___0000D9Z00000">
<testtitle>CHECK HARNESS AND CONNECTOR (POWER MANAGEMENT CONTROL ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G51 power management control ECU connector.</ptxt>
<figure>
<graphic graphicname="C198951E30" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition </ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G51-5 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G51-6 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G51-1 (AM22) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G51-2 (AM21) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Power Management Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000002S8G02LX_04_0004" fin="false">OK</down>
<right ref="RM000002S8G02LX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0004" proc-id="RM23G0E___0000D9X00000">
<testtitle>REPLACE POWER MANAGEMENT CONTROL ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the power management control ECU with a new or normally functioning one (See page <xref label="Seep01" href="RM0000039R5015X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8G02LX_04_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0012" proc-id="RM23G0E___0000DA000000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8G02LX_04_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0005" proc-id="RM23G0E___0000D9Y00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2287 or B278C is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8G02LX_04_0009" fin="true">OK</down>
<right ref="RM000002S8G02LX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0009">
<testtitle>END (POWER MANAGEMENT CONTROL ECU IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0006">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0013">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8G02LX_04_0008">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>