<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A0_T00JO" variety="T00JO">
<name>TOYOTA PARKING ASSIST-SENSOR SYSTEM (for 4 Sensor Type)</name>
<para id="RM000002BIZ024X" category="J" type-id="300WI" name-id="PM2VH-05" from="201207" to="201210">
<dtccode/>
<dtcname>Park / Neutral Position Switch Circuit</dtcname>
<subpara id="RM000002BIZ024X_01" type-id="60" category="03" proc-id="RM23G0E___0000CO800000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit sends the park/neutral position switch assembly signals to the clearance warning ECU.</ptxt>
</content5>
</subpara>
<subpara id="RM000002BIZ024X_02" type-id="32" category="03" proc-id="RM23G0E___0000CO900000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E202337E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002BIZ024X_04" type-id="51" category="05" proc-id="RM23G0E___0000COA00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002BIZ024X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002BIZ024X_05_0001" proc-id="RM23G0E___0000COB00000">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
<figure>
<graphic graphicname="E179071E09" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 (RL) - 2 (RB)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1 (RL) - 2 (RB)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for A750F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for A343F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002BIZ024X_05_0008" fin="false">A</down>
<right ref="RM000002BIZ024X_05_0003" fin="true">B</right>
<right ref="RM000002BIZ024X_05_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002BIZ024X_05_0008" proc-id="RM23G0E___0000COD00000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
<figure>
<graphic graphicname="E146355E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-2 (RB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Park/Neutral Position Switch Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002BIZ024X_05_0002" fin="false">OK</down>
<right ref="RM000002BIZ024X_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002BIZ024X_05_0002" proc-id="RM23G0E___0000COC00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CLEARANCE WARNING ECU - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I1 clearance warning ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I1-13 (RL) - C40-1 (RL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I1-13 (RL) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002BIZ024X_05_0005" fin="true">OK</down>
<right ref="RM000002BIZ024X_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002BIZ024X_05_0005">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000003QOU00OX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002BIZ024X_05_0003">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002BIZ024X_05_0007">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002BIZ024X_05_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002BIZ024X_05_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>