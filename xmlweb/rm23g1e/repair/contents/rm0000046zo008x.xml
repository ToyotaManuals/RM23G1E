<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9DD_T00N1" variety="T00N1">
<name>REAR NO. 1 SEAT ASSEMBLY (for 60/40 Split Tumble Seat Type RH Side)</name>
<para id="RM0000046ZO008X" category="A" type-id="80002" name-id="SE529-01" from="201207">
<name>DISASSEMBLY</name>
<subpara id="RM0000046ZO008X_02" type-id="11" category="10" proc-id="RM23G0E___0000GWR00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
</content3>
</subpara>
<subpara id="RM0000046ZO008X_01" type-id="01" category="01">
<s-1 id="RM0000046ZO008X_01_0001" proc-id="RM23G0E___0000GVW00000">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241029" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws.</ptxt>
</s2>
<s2>
<ptxt>Move the cover in the direction of the arrow to detach the guide.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241031" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 2 hooks and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0002" proc-id="RM23G0E___0000GVX00000">
<ptxt>REMOVE REAR NO. 1 SEAT RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B241032" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 2 claws.</ptxt>
</s2>
<s2>
<ptxt>Move the cover in the direction of the arrow to detach the guide and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0003" proc-id="RM23G0E___0000GVY00000">
<ptxt>REMOVE SEAT TRACK OPEN LEVER PIN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241034" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, remove the stopper.</ptxt>
</s2>
<s2>
<ptxt>Remove the pin as shown in the illustration.</ptxt>
<atten4>
<ptxt>Use the same procedure for both pins.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0004" proc-id="RM23G0E___0000GVZ00000">
<ptxt>REMOVE REAR SEAT HINGE PAWL GUIDE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241036" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the pawl guide.</ptxt>
<atten4>
<ptxt>Use the same procedure for both pawl guides.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0005" proc-id="RM23G0E___0000GW000000">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B241037" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a screwdriver, remove the E-ring.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241038" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the pin and hinge.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0006" proc-id="RM23G0E___0000GW100000">
<ptxt>REMOVE REAR NO. 1 SEAT HINGE SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B241042" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a screwdriver, remove the E-ring.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241043" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the pin.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241044" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a screwdriver, detach the 6 claws.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="B241045" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 4 screws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the reclining release handle from the under cover.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241047" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Raise the under cover.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the spring of the seat stay ball-joint.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the seat stay.</ptxt>
</s2>
<s2>
<ptxt>Remove the seat hinge and seat stay as a unit.</ptxt>
<atten3>
<ptxt>The seat stay cannot be disconnected from the seat hinge. If the stay is disconnected from the hinge, replacement of both parts is necessary.</ptxt>
</atten3>
<atten4>
<ptxt>If it is necessary to replace the seat hinge, replace it together with the seat stay as the stay cannot be disconnected from the hinge.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0032" proc-id="RM23G0E___0000GWP00000">
<ptxt>REMOVE REAR SEAT STAY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>As the seat stay cannot be disconnected from the seat hinge, it is removed in the previous step.</ptxt>
</item>
<item>
<ptxt>If it is necessary to replace the seat stay, replace it together with the seat hinge as the stay cannot be disconnected from the hinge.</ptxt>
</item>
</list1>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0030" proc-id="RM23G0E___0000GWN00000">
<ptxt>REMOVE REAR SEAT CUSHION EDGE PROTECTOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241039" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the protector from the cushion frame.</ptxt>
<atten4>
<ptxt>Use the same procedure for both protectors.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0008" proc-id="RM23G0E___0000GW200000">
<ptxt>REMOVE FOLD SEAT LEG LOCK CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241040" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the cushion from the protector.</ptxt>
<atten4>
<ptxt>Use the same procedure for both cushions.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0031" proc-id="RM23G0E___0000GWO00000">
<ptxt>REMOVE REAR SEAT CUSHION SUPPORT SPRING</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B242238" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the spring from the seat cushion frame.</ptxt>
<atten4>
<ptxt>Use the same procedure for both springs.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0009" proc-id="RM23G0E___0000GW300000">
<ptxt>REMOVE REAR SEAT CUSHION UNDER COVER SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the under cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0010" proc-id="RM23G0E___0000GW400000">
<ptxt>REMOVE REAR NO. 2 SEAT HINGE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241049" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 3 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0011" proc-id="RM23G0E___0000GW500000">
<ptxt>REMOVE REAR NO. 2 SEAT HINGE COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0033" proc-id="RM23G0E___0000GWQ00000">
<ptxt>REMOVE REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B242886E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the nut and inner belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046ZO008X_01_0013" proc-id="RM23G0E___0000GW600000">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B241050E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Cut off the tack pins and remove the carpet from the cushion pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="B241051" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 hog rings and seat cushion cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0014" proc-id="RM23G0E___0000GW700000">
<ptxt>REMOVE REAR SEAT INNER CUSHION EDGE PROTECTOR RH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B241053E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Cut off the tack pins and remove the carpet from the cushion pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0015" proc-id="RM23G0E___0000GW800000">
<ptxt>REMOVE SEPARATE TYPE REAR SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241054" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the hog rings and seat cushion cover from the seat cushion pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0016" proc-id="RM23G0E___0000GW900000">
<ptxt>REMOVE REAR SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237641" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0017" proc-id="RM23G0E___0000GWA00000">
<ptxt>REMOVE NO. 1 SEATBACK PAD RH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B241056E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Detach the fastening tape and open the cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="B241057" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 hog rings.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241059" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Open the 2 fasteners, and then open the seatback cover.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241061" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the seatback pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0018" proc-id="RM23G0E___0000GWB00000">
<ptxt>REMOVE REAR SEATBACK LOCK STRIKER COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237754E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the claw and guide, and then remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0019" proc-id="RM23G0E___0000GWC00000">
<ptxt>REMOVE REAR SEAT LOCK CONTROL LEVER SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B237756" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B165052E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Disconnect the cable in the order shown in the illustration and remove the lever.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0020" proc-id="RM23G0E___0000GWD00000">
<ptxt>REMOVE REAR NO. 1 SEAT HEADREST SUPPORT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241062" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the 2 supports.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0021" proc-id="RM23G0E___0000GWE00000">
<ptxt>REMOVE SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241063" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 2 hooks and remove the seatback cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0022" proc-id="RM23G0E___0000GWF00000">
<ptxt>REMOVE SEPARATE TYPE REAR SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241064E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 fastening tapes.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the hog rings and seatback cover from the seatback pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0023" proc-id="RM23G0E___0000GWG00000">
<ptxt>REMOVE REAR NO. 1 SEAT LOCK CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241066" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 2 cable clamps, and then disconnect the cable and remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0025" proc-id="RM23G0E___0000GWI00000">
<ptxt>REMOVE REAR SEAT INNER RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241068" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and 2 guides, and then remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0024" proc-id="RM23G0E___0000GWH00000">
<ptxt>REMOVE REAR SEAT INNER RECLINING COVER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and 2 guides, and then remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0026" proc-id="RM23G0E___0000GWJ00000">
<ptxt>REMOVE REAR SEAT RECLINING COVER INNER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241069" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 guides and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0027" proc-id="RM23G0E___0000GWK00000">
<ptxt>REMOVE REAR SEAT RECLINING COVER INNER RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 guides and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0028" proc-id="RM23G0E___0000GWL00000">
<ptxt>REMOVE REAR SEATBACK FRAME SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241067" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and seatback frame.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046ZO008X_01_0029" proc-id="RM23G0E___0000GWM00000">
<ptxt>REMOVE NO. 1 FOLD SEAT LOCK CONTROL CABLE ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B241070" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Cut off the 2 cable ties.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B241071" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Detach the 3 cable clamps, and then disconnect the cable and remove it.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>