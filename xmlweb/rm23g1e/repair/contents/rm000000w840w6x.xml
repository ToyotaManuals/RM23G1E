<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0012" variety="S0012">
<name>A343F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0012_7B941_T00DP" variety="T00DP">
<name>AUTOMATIC TRANSMISSION SYSTEM</name>
<para id="RM000000W840W6X" category="C" type-id="302G7" name-id="AT633-03" from="201207" to="201210">
<dtccode>P0705</dtccode>
<dtcname>Transmission Range Sensor Circuit Malfunction (PRNDL Input)</dtcname>
<subpara id="RM000000W840W6X_01" type-id="60" category="03" proc-id="RM23G0E___00007NM00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The Park/Neutral Position (PNP) switch detects the shift lever position and sends signals to the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0705</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<ptxt>(A) Any 2 or more of the following signals are ON simultaneously (2-trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>P input signal</ptxt>
</item>
<item>
<ptxt>N input signal</ptxt>
</item>
<item>
<ptxt>NSW input signal</ptxt>
</item>
<item>
<ptxt>R input signal</ptxt>
</item>
<item>
<ptxt>D input signal</ptxt>
</item>
<item>
<ptxt>2 input signal</ptxt>
</item>
<item>
<ptxt>L input signal</ptxt>
</item>
</list1>
<ptxt>(B) All signals are OFF simultaneously for P, N, NSW, R, D, 2 and L positions (2-trip detection logic).</ptxt>
<ptxt>(C) Any of the following signals is ON for 2.0 seconds or more with the shift lever in 3 (2-trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>P input signal</ptxt>
</item>
<item>
<ptxt>N input signal</ptxt>
</item>
<item>
<ptxt>NSW input signal</ptxt>
</item>
<item>
<ptxt>R input signal</ptxt>
</item>
<item>
<ptxt>2 input signal</ptxt>
</item>
<item>
<ptxt>L input signal</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in park/neutral position switch circuit</ptxt>
</item>
<item>
<ptxt>Park/neutral position switch</ptxt>
</item>
<item>
<ptxt>Shift lock control ECU (transmission floor shift assembly)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W840W6X_02" type-id="64" category="03" proc-id="RM23G0E___00007NN00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates a problem with the park/neutral position switch or the wire harness in the park/neutral position switch circuit.</ptxt>
<ptxt>The park/neutral position switch detects the shift lever position and sends a signal to the ECM.</ptxt>
<ptxt>For security, the park/neutral position switch detects the shift lever position so that the engine can be started only when the shift lever is in P or N.</ptxt>
<ptxt>The park/neutral position switch sends a signal to the ECM according to the shift lever position (P, R, N, D, 3, 2 or L).</ptxt>
<ptxt>The ECM determines that there is a problem with the switch or related parts if it receives more than 1 position signal simultaneously. The ECM will illuminate the MIL and store the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W840W6X_07" type-id="32" category="03" proc-id="RM23G0E___00007NO00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C214976E01" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W840W6X_08" type-id="51" category="05" proc-id="RM23G0E___00007NP00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Neutral Position SW Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in P or N: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in P or N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (P Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in P: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in P: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (R Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in R: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in R: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (2 Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in 2: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in 2: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (L Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in L: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in L: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in N: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (D Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in D: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in D: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (3 Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in 3: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in 3: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W840W6X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W840W6X_09_0001" proc-id="RM23G0E___00007NQ00000">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C40 park/neutral position switch connector.</ptxt>
<figure>
<graphic graphicname="C197712E11" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>2 (RB) - 6 (PL)</ptxt>
</item>
<item>
<ptxt>4 (B) - 5 (L)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Shift lever in P</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 1 (RL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>2 (RB) - 9 (NL)</ptxt>
</item>
<item>
<ptxt>4 (B) - 5 (L)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Shift lever in N</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 7 (DL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in D or 3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 3 (2L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 8 (LL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in L</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>2 (RB) - 6 (PL)</ptxt>
</item>
<item>
<ptxt>4 (B) - 5 (L)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in P</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 1 (RL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>2 (RB) - 9 (NL)</ptxt>
</item>
<item>
<ptxt>4 (B) - 5 (L)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in N</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 7 (DL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in D or 3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 3 (2L)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (RB) - 8 (LL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever not in L</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Park/Neutral Position Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W840W6X_09_0006" fin="false">OK</down>
<right ref="RM000000W840W6X_09_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840W6X_09_0006" proc-id="RM23G0E___00007NS00000">
<testtitle>CHECK PARK/NEUTRAL POSITION SWITCH ASSEMBLY (POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C40 park/neutral position switch connector.</ptxt>
<figure>
<graphic graphicname="C197711E28" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-2 (RB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C40-2 (RB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Park/Neutral Position Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W840W6X_09_0004" fin="false">OK</down>
<right ref="RM000000W840W6X_09_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840W6X_09_0004" proc-id="RM23G0E___00007NR00000">
<testtitle>INSPECT SHIFT LOCK CONTROL ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G45 shift lock control ECU connector.</ptxt>
<figure>
<graphic graphicname="C214966E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 (AT3) - 9 (NSSD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in 3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 (AT3) - 9 (NSSD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in D</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Lock Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W840W6X_09_0008" fin="false">OK</down>
<right ref="RM000000W840W6X_09_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840W6X_09_0008" proc-id="RM23G0E___00007NT00000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C63 and G60 ECM connectors.</ptxt>
<figure>
<graphic graphicname="C215341E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C63-16 (P) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in P</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-17 (R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in R</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-11 (N) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G60-9 (D) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in D</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G60-23 (3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in 3</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-15 (2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in 2</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-25 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in L</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G60-24 (NSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in P or N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-16 (P) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in P</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-17 (R) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in R</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-11 (N) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G60-9 (D) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in D</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G60-23 (3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in 3</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-15 (2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in 2</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C63-25 (L) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in L</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G60-24 (NSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: The voltage will drop slightly due to the illumination of the back-up light.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W840W6X_09_0010" fin="true">OK</down>
<right ref="RM000000W840W6X_09_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W840W6X_09_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W840W6X_09_0011">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W840W6X_09_0013">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (PARK/NEUTRAL POSITION SWITCH - IGNITION SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000W840W6X_09_0017">
<testtitle>REPLACE SHIFT LOCK CONTROL ECU (TRANSMISSION FLOOR SHIFT ASSEMBLY)<xref label="Seep01" href="RM000002YBF036X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W840W6X_09_0015">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>