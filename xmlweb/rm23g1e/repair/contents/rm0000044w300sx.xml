<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8Z4_T008S" variety="T008S">
<name>COMMON RAIL (w/ DPF)</name>
<para id="RM0000044W300SX" category="A" type-id="80001" name-id="FU8CF-03" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000044W300SX_02" type-id="11" category="10" proc-id="RM23G0E___00005RW00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipes and fuel inlet pipe, clean them with a brush and compressed air.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000044W300SX_01" type-id="01" category="01">
<s-1 id="RM0000044W300SX_01_0019" proc-id="RM23G0E___00005RT00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000044W300SX_01_0044" proc-id="RM23G0E___00005RV00001">
<ptxt>REMOVE COWL TOP VENTILATOR LOUVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000001OV303UX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000044W300SX_01_0035" proc-id="RM23G0E___00005RU00001">
<ptxt>REMOVE ELECTRIC EGR CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000004KSK005X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000044W300SX_01_0045" proc-id="RM23G0E___00006CV00001">
<ptxt>DISCONNECT WIRE HARNESS
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="A245686" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Remove the bolt and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="A246260" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 5 clamps and disconnect the wire harness from the cowl top panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0046" proc-id="RM23G0E___000032S00000">
<ptxt>REMOVE MANIFOLD ABSOLUTE PRESSURE SENSOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the manifold absolute pressure sensor connector and vacuum hose.</ptxt>
<figure>
<graphic graphicname="A239598" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and manifold absolute pressure sensor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0047" proc-id="RM23G0E___00006CW00001">
<ptxt>REMOVE EMISSION CONTROL VALVE BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and emission control valve bracket.</ptxt>
<figure>
<graphic graphicname="A239639" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0048" proc-id="RM23G0E___00006CZ00001">
<ptxt>REMOVE THROTTLE BODY BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and throttle body bracket.</ptxt>
<figure>
<graphic graphicname="A244614" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0049" proc-id="RM23G0E___00006CX00001">
<ptxt>REMOVE NO. 1 GAS FILTER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the vacuum hose and remove the No. 1 gas filter from the gas filter bracket.</ptxt>
<figure>
<graphic graphicname="A244608" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0050" proc-id="RM23G0E___00006CY00001">
<ptxt>REMOVE GAS FILTER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="A246247" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and gas filter bracket.</ptxt>
<figure>
<graphic graphicname="A244609" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0051" proc-id="RM23G0E___00006D000001">
<ptxt>REMOVE INTAKE AIR CONNECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts, intake air connector and gasket.</ptxt>
<figure>
<graphic graphicname="A245684" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0052" proc-id="RM23G0E___00005QU00001">
<ptxt>REMOVE NO. 1 FUEL PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 fuel pipe (See page <xref label="Seep01" href="RM0000028RU03BX"/>).</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts, union bolt, gasket and No. 1 fuel pipe.</ptxt>
<figure>
<graphic graphicname="A239663E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" align="center" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Fuel Pipe</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0036" proc-id="RM23G0E___00005QO00001">
<ptxt>REMOVE NO. 4 INJECTION PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, nut and 2 No. 2 injection pipe clamps.</ptxt>
<figure>
<graphic graphicname="A239621" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, loosen the union nuts and remove the No. 4 injection pipe.</ptxt>
<figure>
<graphic graphicname="A239619E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0037" proc-id="RM23G0E___00005QP00001">
<ptxt>REMOVE NO. 2 FUEL PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 6 mm hexagon wrench, remove the union bolt and gasket.</ptxt>
<figure>
<graphic graphicname="A239622" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Fuel Check Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the fuel check valve, gasket and No. 2 fuel pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0038" proc-id="RM23G0E___00005QQ00001">
<ptxt>REMOVE NO. 3 NOZZLE LEAKAGE PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 fuel hoses.</ptxt>
<figure>
<graphic graphicname="A239624" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Fuel Check Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Remove the fuel check valve, gasket and No. 3 nozzle leakage pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0039" proc-id="RM23G0E___00005QR00001">
<ptxt>REMOVE NO. 2 NOZZLE LEAKAGE PIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A239626" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the union bolt, gasket and No. 2 nozzle leakage pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0042" proc-id="RM23G0E___00006CQ00001">
<ptxt>REMOVE VACUUM CONTROL VALVE SET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 vacuum switching valve connectors.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 3 vacuum hoses, and then remove the 2 bolts and vacuum control valve set.</ptxt>
<figure>
<graphic graphicname="A244983" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0043" proc-id="RM23G0E___000048X00000">
<ptxt>REMOVE OIL FILTER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the oil filter.</ptxt>
<figure>
<graphic graphicname="A227248E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09228-07501</s-number>
</sstitem>
</sst>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300SX_01_0016" proc-id="RM23G0E___00005RS00001">
<ptxt>REMOVE COMMON RAIL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the pressure discharge valve connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and common rail.</ptxt>
<figure>
<graphic graphicname="A239630" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not remove the pressure discharge valve or fuel pressure sensor.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>