<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM000002NOW068X" category="C" type-id="3038R" name-id="ESMGI-07" from="201210">
<dtccode>P0299</dtccode>
<dtcname>Turbocharger / Supercharger Underboost</dtcname>
<subpara id="RM000002NOW068X_01" type-id="60" category="03" proc-id="RM23G0E___00001O300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Refer to DTC P0046 (See page <xref label="Seep01" href="RM000002PPL05RX_01"/>).</ptxt>
</atten4>
<table pgwide="1">
<title>P0299</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Accelerate vehicle with accelerator depressed halfway or more starting from a speed of 120 km/h (75 mph) or more for 2 minutes (make sure gear is fixed and does not change)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The pressure is 55 kPa below the Target Booster Pressure for 40 seconds at an engine speed of 2800 to 4400 rpm (2 trip detection logic)*1 (1 trip detection logic)*2.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Turbocharger sub-assembly</ptxt>
</item>
<item>
<ptxt>Extremely low battery voltage (P0560 output simultaneously)</ptxt>
</item>
<item>
<ptxt>Disconnected pipe between turbo and intake manifold</ptxt>
</item>
<item>
<ptxt>Disconnected manifold absolute pressure sensor hose</ptxt>
</item>
<item>
<ptxt>Throttle valve stuck closed</ptxt>
</item>
<item>
<ptxt>Electric EGR control valve assembly does not close</ptxt>
</item>
<item>
<ptxt>Exhaust system or intake system modified (leading to increased pressure loss, etc.)</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor</ptxt>
</item>
<item>
<ptxt>Injection system (reduction in fuel injection volume)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: Only for vehicle Euro-OBD.</ptxt>
</item>
<item>
<ptxt>*2: Only for vehicle except Euro-OBD.</ptxt>
</item>
</list1>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0299</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Target Booster Pressure</ptxt>
</item>
<item>
<ptxt>MAP</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC P0299 is stored due to the variable nozzle being stuck open, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Lack of power</ptxt>
</item>
<item>
<ptxt>Vehicle surge or hesitation under light or medium load</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000002NOW068X_03" type-id="51" category="05" proc-id="RM23G0E___00001O400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07OX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Comparison of the data list items manifold absolute pressure and target booster pressure during acceleration under medium to high engine speeds can be used to determine the state of the problem.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002NOW068X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002NOW068X_04_0001" proc-id="RM23G0E___00001O500001">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0299)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0299 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0299 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0036" fin="false">A</down>
<right ref="RM000002NOW068X_04_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0036" proc-id="RM23G0E___00001O700001">
<testtitle>CHECK INTAKE AND EXHAUST SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for intake and exhaust system modifications.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No modifications.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0037" fin="false">OK</down>
<right ref="RM000002NOW068X_04_0043" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0037" proc-id="RM23G0E___00001O800001">
<testtitle>INSPECT TURBOCHARGER SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the turbocharger sub-assembly (See page <xref label="Seep01" href="RM000003TBT015X"/>).</ptxt>
<atten4>
<ptxt>Check if the VN is stuck, if there are abnormal noises from the turbo (impeller is damaged), if there is turbo shaft rattle, and if the turbo shaft does not rotate smoothly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0038" fin="false">OK</down>
<right ref="RM000002NOW068X_04_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0038" proc-id="RM23G0E___00001O900001">
<testtitle>CHECK CONNECTION OF VACUUM HOSE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the vacuum hoses connected to the manifold absolute pressure sensor.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0039" fin="false">OK</down>
<right ref="RM000002NOW068X_04_0044" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0039" proc-id="RM23G0E___00001OA00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (MAP)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / MAP.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>Standard Value</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard Value</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle">
<ptxt>90 to 102 kPa</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3000 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>100 to 150 kPa</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0040" fin="false">OK</down>
<right ref="RM000002NOW068X_04_0045" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0040" proc-id="RM23G0E___00001OB00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ACTUAL THROTTLE POSITION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Actual Throttle Position.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>Standard Value</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>-5 to 5%</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle">
<ptxt>55 to 90%</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0041" fin="false">OK</down>
<right ref="RM000002NOW068X_04_0046" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0041" proc-id="RM23G0E___00001OC00001">
<testtitle>INSPECT ELECTRIC EGR CONTROL VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the electric EGR control valve assembly (See page <xref label="Seep01" href="RM000002RK701EX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0042" fin="false">OK</down>
<right ref="RM000002NOW068X_04_0047" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0042" proc-id="RM23G0E___00001OD00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INJECTION FEEDBACK VAL #1 TO #4)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / Data List / Injection Feedback Val #1 to #4.</ptxt>
</test1>
<test1>
<ptxt>Read the values.</ptxt>
<spec>
<title>Standard Value</title>
<specitem>
<ptxt>The compensatory injection volume is less than 3.0 mm<sup>3</sup>/st</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the injector assembly is malfunctioning, the compensatory injection volume remains at 5.0 mm<sup>3</sup>/st.</ptxt>
</item>
<item>
<ptxt>If there is a disconnection, the feedback value will increase and +5.0 mm<sup>3</sup>/st will be indicated, because it will become impossible for the injector assembly to inject.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0035" fin="false">OK</down>
<right ref="RM000002NOW068X_04_0048" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0035" proc-id="RM23G0E___00001O600001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK13TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Accelerate the vehicle with the accelerator depressed halfway or more starting from a speed of 120 km/h (75 mph) or more for 2 minutes (make sure the gear is fixed and does not change).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC / Pending.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Result</ptxt>
</entry>
<entry>
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>No output</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>P0299</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002NOW068X_04_0003" fin="true">A</down>
<right ref="RM000002NOW068X_04_0028" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002NOW068X_04_0012">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW051X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0043">
<testtitle>REPAIR OR REPLACE INTAKE AND EXHAUST SYSTEM</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0030">
<testtitle>REPLACE TURBOCHARGER SUB-ASSEMBLY<xref label="Seep01" href="RM000002S6F00QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0044">
<testtitle>REPAIR OR REPLACE VACUUM HOSE</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0045">
<testtitle>REPLACE MANIFOLD ABSOLUTE PRESSURE SENSOR</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0046">
<testtitle>REPLACE DIESEL THROTTLE BODY ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0047">
<testtitle>REPLACE ELECTRIC EGR CONTROL VALVE ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0048">
<testtitle>REPLACE INJECTOR ASSEMBLY OF MALFUNCTIONING CYLINDER<xref label="Seep01" href="RM0000044TN00TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0028">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002NOW068X_04_0003">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0ZPX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>