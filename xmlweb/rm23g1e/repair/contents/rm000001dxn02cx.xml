<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM000001DXN02CX" category="C" type-id="804QX" name-id="BCB7K-03" from="201210">
<dtccode>C1253</dtccode>
<dtcname>Pump Motor Relay</dtcname>
<subpara id="RM000001DXN02CX_01" type-id="60" category="03" proc-id="RM23G0E___0000AG700001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The motor relay (semiconductor relay) is built into the master cylinder solenoid and drives the pump motor based on a signal from the skid control ECU.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1253</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is a motor system circuit (motor input circuit) malfunction.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ABS NO. 1 fuse</ptxt>
</item>
<item>
<ptxt>Motor relay circuit</ptxt>
</item>
<item>
<ptxt>Master cylinder solenoid (Skid control ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001DXN02CX_02" type-id="32" category="03" proc-id="RM23G0E___0000AG800001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C173280E03" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001DXN02CX_03" type-id="51" category="05" proc-id="RM23G0E___0000AG900001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the master cylinder solenoid, perform calibration (See page <xref label="Seep01" href="RM00000452J00KX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001DXN02CX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DXN02CX_04_0001" proc-id="RM23G0E___0000AGA00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (MOTOR RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Active Test.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" align="center" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Motor Relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Motor relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Relay ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>An operating sound of the motor can be heard.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check the operating sound of the motor individually when operating it with the intelligent tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>An operating sound of the motor can be heard.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001DXN02CX_04_0012" fin="false">OK</down>
<right ref="RM000001DXN02CX_04_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0012" proc-id="RM23G0E___0000AGD00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00LX"/>). </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off. </ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal more than 40 times.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Wait until the pump motor stops.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal several times until the pump motor turns on. (Procedure A)</ptxt>
</test1>
<test1>
<ptxt>Wait until the pump stops. (Procedure B)</ptxt>
</test1>
<test1>
<ptxt>Repeat the above steps (procedure A and B) 3 more times.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00LX"/>).</ptxt>
<atten4>
<ptxt>Reinstall the sensors, connectors, etc. and restore the previous vehicle conditions before rechecking for DTCs.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.56in"/>
<colspec colname="COL2" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXN02CX_04_0008" fin="true">A</down>
<right ref="RM000001DXN02CX_04_0007" fin="true">B</right>
<right ref="RM000001DXN02CX_04_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0004" proc-id="RM23G0E___0000AGB00001">
<testtitle>CHECK TERMINAL VOLTAGE (+BM1, +BM2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A7 and A8 skid control ECU connectors. </ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C124799E49" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A7-2 (+BM1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A8-2 (+BM2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXN02CX_04_0013" fin="false">OK</down>
<right ref="RM000001DXN02CX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0013" proc-id="RM23G0E___0000AAB00001">
<testtitle>CHECK HARNESS AND CONNECTOR (GND1, GND2 AND GND3 TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A7 and A8 skid control ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A7-1 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A7-32 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A8-4 (GND3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000001DXN02CX_04_0006" fin="false">OK</down>
<right ref="RM000001DXN02CX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0006" proc-id="RM23G0E___0000AGC00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00LX"/>). </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off. </ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal more than 40 times.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Wait until the pump motor stops. </ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal several times until the pump motor turns on. (Procedure A)</ptxt>
</test1>
<test1>
<ptxt>Wait until the pump stops. (Procedure B)</ptxt>
</test1>
<test1>
<ptxt>Repeat the above steps (procedure A and B) 3 more times.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00LX"/>).</ptxt>
<atten4>
<ptxt>Reinstall the sensors, connectors, etc. and restore the previous vehicle conditions before rechecking for DTCs.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.56in"/>
<colspec colname="COL2" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DXN02CX_04_0008" fin="true">A</down>
<right ref="RM000001DXN02CX_04_0007" fin="true">B</right>
<right ref="RM000001DXN02CX_04_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0007">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0008">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DXN02CX_04_0014">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>