<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0022" variety="S0022">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0022_7B9C6_T00LU" variety="T00LU">
<name>AIRBAG SYSTEM</name>
<para id="RM000000XFJ0JWX" category="C" type-id="303MI" name-id="RS8NV-13" from="201207" to="201210">
<dtccode>B1805/52</dtccode>
<dtcname>Short in Front Passenger Side Squib Circuit</dtcname>
<dtccode>B1806/52</dtccode>
<dtcname>Open in Front Passenger Side Squib Circuit</dtcname>
<dtccode>B1807/52</dtccode>
<dtcname>Short to GND in Front Passenger Side Squib Circuit</dtcname>
<dtccode>B1808/52</dtccode>
<dtcname>Short to B+ in Front Passenger Side Squib Circuit</dtcname>
<subpara id="RM000000XFJ0JWX_01" type-id="60" category="03" proc-id="RM23G0E___0000FGW00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The front passenger side squib circuit consists of the center airbag sensor and instrument panel passenger airbag. </ptxt>
<ptxt>The circuit instructs the SRS to deploy when deployment conditions are met.</ptxt>
<ptxt>These DTCs are stored when a malfunction is detected in the front passenger side squib circuit.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1805/52</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor detects a line short circuit signal 5 times in the front passenger side squib circuit during the primary check.</ptxt>
</item>
<item>
<ptxt>A front passenger side squib malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>No. 2 instrument panel wire</ptxt>
</item>
<item>
<ptxt>Instrument panel passenger airbag assembly (Front passenger side squib)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B1806/52</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor detects open circuit signal in the front passenger side squib circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A front passenger side squib malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>No. 2 instrument panel wire</ptxt>
</item>
<item>
<ptxt>Instrument panel passenger airbag assembly (Front passenger side squib)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B1807/52</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor detects a short circuit to ground signal in the front passenger side squib circuit for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>A front passenger side squib malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>No. 2 instrument panel wire</ptxt>
</item>
<item>
<ptxt>Instrument panel passenger airbag assembly (Front passenger side squib)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B1808/52</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor detects a short circuit to B+ signal in the front passenger side squib circuit for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>A front passenger side squib malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>No. 2 instrument panel wire</ptxt>
</item>
<item>
<ptxt>Instrument panel passenger airbag assembly (Front passenger side squib)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XFJ0JWX_02" type-id="32" category="03" proc-id="RM23G0E___0000FGX00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C146249E65" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XFJ0JWX_04" type-id="51" category="05" proc-id="RM23G0E___0000FH300000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable from the negative (-) battery terminal while performing repairs, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>To perform the simulation method, enter check mode (signal check) with the intelligent tester (See page <xref label="Seep01" href="RM000000XFF0CNX"/>), and then wiggle each connector of the airbag system or drive the vehicle on various types of road (See page <xref label="Seep02" href="RM000000XFD0GRX"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XFJ0JWX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XFJ0JWX_03_0001" proc-id="RM23G0E___0000FGY00000">
<testtitle>CHECK INSTRUMENT PANEL PASSENGER AIRBAG ASSEMBLY (FRONT PASSENGER SIDE SQUIB)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C217392E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the connectors from the instrument panel passenger airbag.</ptxt>
</test1>
<test1>
<ptxt>Connect the white wire side of SST (resistance: 2.1 Ω) to connector E (orange connector).</ptxt>
<atten2>
<ptxt>Never connect the tester to the instrument panel passenger airbag (front passenger side squib) for measurement, as this may lead to a serious injury due to airbag deployment.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not forcibly insert SST into the terminals of the connector when connecting SST.</ptxt>
</item>
<item>
<ptxt>Insert SST straight into the terminals of the connector.</ptxt>
</item>
</list1>
</atten3>
</test1>
<sst>
<sstitem>
<s-number>09843-18061</s-number>
</sstitem>
</sst>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0H0X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0H0X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1805, B1806, B1807 or B1808 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1805, B1806, B1807 and B1808 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Passenger Side Squib</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector E</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>SST</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Front Passenger Side Squib)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Color: Orange</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XFJ0JWX_03_0007" fin="true">OK</down>
<right ref="RM000000XFJ0JWX_03_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0002" proc-id="RM23G0E___0000FGZ00000">
<testtitle>CHECK CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect SST from connector E.</ptxt>
</test1>
<test1>
<ptxt>Check that the No. 2 instrument panel wire connectors (on the instrument panel passenger airbag side) are not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The lock button is not disengaged, and the claw of the lock is not deformed or damaged.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="C213964E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Passenger Side Squib</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XFJ0JWX_03_0003" fin="false">OK</down>
<right ref="RM000000XFJ0JWX_03_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0003" proc-id="RM23G0E___0000FH000000">
<testtitle>CHECK FRONT PASSENGER SIDE SQUIB CIRCUIT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C217393E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F1-2 (P+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F1-1 (P-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F1-2 (P+) - F1-1 (P-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Release the activation prevention mechanism built into connector B (See page <xref label="Seep01" href="RM000000XFD0GRX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>F1-2 (P+) - F1-1 (P-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F1-2 (P+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>F1-1 (P-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Passenger Side Squib</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector E</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Front Passenger Side Squib)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Color: Orange</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000XFJ0JWX_03_0004" fin="false">OK</down>
<right ref="RM000000XFJ0JWX_03_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0004" proc-id="RM23G0E___0000FH100000">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connectors to the instrument panel passenger airbag and center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C215839E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0H0X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0H0X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1805, B1806, B1807 or B1808 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1805, B1806, B1807 and B1808 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Passenger Side Squib</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XFJ0JWX_03_0010" fin="true">OK</down>
<right ref="RM000000XFJ0JWX_03_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0005" proc-id="RM23G0E___0000FH200000">
<testtitle>CHECK INSTRUMENT PANEL WIRE (CENTER AIRBAG SENSOR - NO. 2 INSTRUMENT PANEL WIRE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Restore the released activation prevention mechanism of connector B to its original condition.</ptxt>
<figure>
<graphic graphicname="C217391E02" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the instrument panel wire connector from the No. 2 instrument panel wire.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>GF1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>GF1-2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>GF1-1 - GF1-2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Release the activation prevention mechanism built into connector B (See page <xref label="Seep01" href="RM000000XFD0GRX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC3" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>GF1-1 - GF1-2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>GF1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>GF1-2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Passenger Side Squib</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Instrument Panel Wire</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Instrument Panel Wire</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 2 Instrument Panel Wire)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000XFJ0JWX_03_0008" fin="true">OK</down>
<right ref="RM000000XFJ0JWX_03_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0007">
<testtitle>REPLACE INSTRUMENT PANEL PASSENGER AIRBAG ASSEMBLY<xref label="Seep01" href="RM000003YQ000PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0008">
<testtitle>REPLACE NO. 2 INSTRUMENT PANEL WIRE</testtitle>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0009">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000003YQB00PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0010">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YQB00PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XFJ0JWX_03_0011">
<testtitle>REPLACE INSTRUMENT PANEL WIRE</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>