<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM00000451R012X" category="J" type-id="305RU" name-id="BCB6X-04" from="201210">
<dtccode/>
<dtcname>Brake Warning Light does not Come ON</dtcname>
<subpara id="RM00000451R012X_01" type-id="60" category="03" proc-id="RM23G0E___0000A9300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to Brake Warning Light Remains ON (See page <xref label="Seep01" href="RM00000451Q012X_01"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000451R012X_02" type-id="32" category="03" proc-id="RM23G0E___0000A9400001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to Brake Warning Light Remains ON (See page <xref label="Seep01" href="RM00000451Q012X_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000451R012X_03" type-id="51" category="05" proc-id="RM23G0E___0000A9500001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the master cylinder solenoid, perform calibration (See page <xref label="Seep01" href="RM00000452J00KX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000451R012X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000451R012X_04_0009" proc-id="RM23G0E___0000A7E00001">
<testtitle>CHECK CAN COMMUNICATION LINE
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Select CAN Bus Check from the System Selection Menu screen and follow the prompts on the screen to inspect the CAN bus (for LHD: See page <xref label="Seep01" href="RM000000WIB0CMX"/>, for RHD: See page <xref label="Seep02" href="RM000000WIB0COX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>CAN Bus Check indicates no malfunctions in CAN communication.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.93in"/>
<colspec colname="COL2" colwidth="1.20in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM00000451R012X_04_0006" fin="false">A</down>
<right ref="RM00000451R012X_04_0003" fin="true">B</right>
<right ref="RM00000451R012X_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451R012X_04_0006" proc-id="RM23G0E___0000A8A00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (BRAKE WARNING LIGHT)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="2.26in"/>
<colspec colname="COL3" colwidth="1.60in"/>
<colspec colname="COL4" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Brake Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake warning light/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Warning light on</ptxt>
<ptxt>OFF: Warning light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>When performing the Brake Warning Light Active Test, check Brake Warning Light in the Data List (See page <xref label="Seep02" href="RM000001DWY027X"/>).</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Brake Warning Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake warning light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Warning light ON/OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Observe the combination meter.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.69in"/>
<colspec colname="COL2" colwidth="3.91in"/>
<colspec colname="COL3" colwidth="1.48in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Data List Display</ptxt>
</entry>
<entry>
<ptxt>Data List Display when Performing Active Test ON/OFF Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM00000451R012X_04_0004" fin="true">A</down>
<right ref="RM00000451R012X_04_0005" fin="true">B</right>
<right ref="RM00000451R012X_04_0008" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451R012X_04_0003">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO086X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451R012X_04_0004">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L03KX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451R012X_04_0005">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01NX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451R012X_04_0007">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO088X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451R012X_04_0008">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>