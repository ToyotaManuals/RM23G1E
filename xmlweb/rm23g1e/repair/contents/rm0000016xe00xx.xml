<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001A" variety="S001A">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001A_7B96A_T00FY" variety="T00FY">
<name>REAR AXLE SHAFT</name>
<para id="RM0000016XE00XX" category="A" type-id="80001" name-id="AD1A9-02" from="201207">
<name>REMOVAL</name>
<subpara id="RM0000016XE00XX_02" type-id="11" category="10" proc-id="RM23G0E___00009DQ00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000016XE00XX_01" type-id="01" category="01">
<s-1 id="RM0000016XE00XX_01_0030" proc-id="RM23G0E___00009DP00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>w/ Navigation System (for HDD):</ptxt>
<ptxt>After the ignition switch is turned off, the HDD navigation system requires approximately a minute to record various types of memory and settings. As a result, after turning the ignition switch off, wait a minute or more before disconnecting the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000016XE00XX_01_0002">
<ptxt>REMOVE REAR WHEEL</ptxt>
</s-1>
<s-1 id="RM0000016XE00XX_01_0026">
<ptxt>DRAIN BRAKE FLUID</ptxt>
</s-1>
<s-1 id="RM0000016XE00XX_01_0029" proc-id="RM23G0E___00009DO00000">
<ptxt>DISCONNECT REAR FLEXIBLE HOSE LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the union bolt and gasket from the rear disc brake cylinder, and then disconnect the rear flexible hose from the rear disc brake cylinder.</ptxt>
<figure>
<graphic graphicname="F043125" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use a container to catch brake fluid as it drains out.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000016XE00XX_01_0024" proc-id="RM23G0E___00009DM00000">
<ptxt>REMOVE REAR SPEED SENSOR LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the speed sensor connector.</ptxt>
<figure>
<graphic graphicname="C212844" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and speed sensor.</ptxt>
<atten3>
<ptxt>Pull out the sensor while trying as much as possible not to rotate it.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000016XE00XX_01_0028" proc-id="RM23G0E___00009DN00000">
<ptxt>REMOVE PARKING BRAKE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the parking brake assembly (See page <xref label="Seep01" href="RM000001EDH010X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016XE00XX_01_0010" proc-id="RM23G0E___00009DK00000">
<ptxt>REMOVE REAR AXLE SHAFT WITH PARKING BRAKE PLATE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 nuts and rear axle shaft with parking brake plate.</ptxt>
<figure>
<graphic graphicname="F042741" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016XE00XX_01_0011" proc-id="RM23G0E___00009DL00000">
<ptxt>REMOVE REAR AXLE SHAFT OIL SEAL LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="F042742E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using SST, remove the rear axle shaft oil seal.</ptxt>
<sst>
<sstitem>
<s-number>09308-00010</s-number>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>