<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM00000451T00SX" category="J" type-id="305SV" name-id="BCB6U-02" from="201207" to="201210">
<dtccode/>
<dtcname>Slip Indicator Light does not Come ON</dtcname>
<subpara id="RM00000451T00SX_01" type-id="60" category="03" proc-id="RM23G0E___0000A9B00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to Slip Indicator Light Remains ON (See page <xref label="Seep01" href="RM00000451S00SX_01"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000451T00SX_02" type-id="32" category="03" proc-id="RM23G0E___0000A9C00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to Slip Indicator Light Remains ON (See page <xref label="Seep01" href="RM00000451S00SX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000451T00SX_03" type-id="51" category="05" proc-id="RM23G0E___0000A9D00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the master cylinder solenoid, perform calibration (See page <xref label="Seep01" href="RM00000452J00IX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000451T00SX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000451T00SX_04_0004" proc-id="RM23G0E___0000A9A00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (SLIP INDICATOR LIGHT)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.88in"/>
<colspec colname="COL3" colwidth="1.98in"/>
<colspec colname="COL4" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Slip Indicator Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Slip indicator light/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Indicator light on</ptxt>
<ptxt>OFF: Indicator light off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>When performing the Slip Indicator Light Active Test, check Slip Indicator Light in the Data List (See page <xref label="Seep02" href="RM000001DWY022X"/>).</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Slip Indicator Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Slip indicator light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Indicator light ON/OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Observe the combination meter.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.69in"/>
<colspec colname="COL2" colwidth="3.91in"/>
<colspec colname="COL3" colwidth="1.48in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Data List Display</ptxt>
</entry>
<entry>
<ptxt>Data List Display when Performing Active Test ON/OFF Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Does not change between ON and OFF (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM00000451T00SX_04_0003" fin="true">A</down>
<right ref="RM00000451T00SX_04_0002" fin="true">B</right>
<right ref="RM00000451T00SX_04_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000451T00SX_04_0002">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01NX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451T00SX_04_0003">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L031X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000451T00SX_04_0005">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>