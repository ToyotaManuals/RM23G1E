<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B944_T00DS" variety="T00DS">
<name>SPEED SENSOR</name>
<para id="RM000002BL1032X" category="A" type-id="30014" name-id="ATA8C-01" from="201207">
<name>INSTALLATION</name>
<subpara id="RM000002BL1032X_01" type-id="01" category="01">
<s-1 id="RM000002BL1032X_01_0001" proc-id="RM23G0E___00007Q400000">
<ptxt>INSTALL SPEED SENSOR SP2</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with ATF and install it to the sensor.</ptxt>
</s2>
<s2>
<ptxt>Install the sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>5.4</t-value1>
<t-value2>55</t-value2>
<t-value3>48</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL1032X_01_0002" proc-id="RM23G0E___00007Q500000">
<ptxt>INSTALL SPEED SENSOR NT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with ATF and install it to the sensor.</ptxt>
</s2>
<s2>
<ptxt>Install the sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>5.4</t-value1>
<t-value2>55</t-value2>
<t-value3>48</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL1032X_01_0003" proc-id="RM23G0E___00006FC00000">
<ptxt>INSTALL FRONT EXHAUST PIPE ASSEMBLY (w/ DPF)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the front exhaust pipe to the No. 2 turbine outlet elbow with 3 new nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>54</t-value1>
<t-value2>554</t-value2>
<t-value4>40</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the front exhaust pipe to the 2 exhaust pipe supports.</ptxt>
<atten4>
<ptxt>Install the exhaust pipe supports after installing the No. 3 frame crossmember.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Connect the No. 7 exhaust pipe air hose to the front exhaust pipe with a new clip.</ptxt>
<figure>
<graphic graphicname="A246253E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stopper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 to 10 mm (0.157 to 0.394 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Align the paint marks of the front exhaust pipe and No. 7 exhaust pipe air hose and insert the No. 7 exhaust pipe air hose until it contacts the stopper.</ptxt>
</item>
<item>
<ptxt>Make sure the clip is 4 to 10 mm (0.157 to 0.394 in.) from the end of the No. 7 exhaust pipe air hose when installing the clip.</ptxt>
</item>
<item>
<ptxt>Make sure that there is no slack in the No. 7 exhaust pipe air hose, and that it is not twisted or bent.</ptxt>
</item>
<item>
<ptxt>Take care not to damage the inner or outer surface of the No. 7 exhaust pipe air hose when installing it. If the No. 7 exhaust pipe air hose is damaged, replace it with a new one.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the No. 6 exhaust pipe air hose to the front exhaust pipe with a new clip.</ptxt>
<figure>
<graphic graphicname="A246254E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stopper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 to 10 mm (0.157 to 0.394 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Align the paint marks of the front exhaust pipe and No. 6 exhaust pipe air hose and insert the No. 6 exhaust pipe air hose until it contacts the stopper.</ptxt>
</item>
<item>
<ptxt>Make sure the clip is 4 to 10 mm (0.157 to 0.394 in.) from the end of the exhaust pipe air hose when installing the clip.</ptxt>
</item>
<item>
<ptxt>Make sure that there is no slack in the No. 6 exhaust pipe air hose, and that it is not twisted or bent.</ptxt>
</item>
<item>
<ptxt>Take care not to damage the inner or outer surface of the No. 6 exhaust pipe air hose when installing it. If the No. 6 exhaust pipe air hose is damaged, replace it with a new one.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the No. 3 exhaust gas temperature sensor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 2 exhaust gas temperature sensor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the exhaust gas temperature sensor connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002BL1032X_01_0004" proc-id="RM23G0E___0000JMC00000">
<ptxt>CONNECT CENTER EXHAUST PIPE ASSEMBLY (w/ DPF)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the free length of the compression spring.</ptxt>
<spec>
<title>Minimum free length</title>
<specitem>
<ptxt>43 mm (1.693 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the free length is less than the minimum, replace the compression spring.</ptxt>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer and wooden block, tap in a new gasket until its surface is flush with the front exhaust pipe.</ptxt>
<figure>
<graphic graphicname="A076194E11" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wooden Block</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure to install the gasket so that it faces the correct direction.</ptxt>
</item>
<item>
<ptxt>Do not reuse the gasket.</ptxt>
</item>
<item>
<ptxt>Do not damage the gasket.</ptxt>
</item>
<item>
<ptxt>When connecting the exhaust pipe, do not push in the gasket with the exhaust pipe.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Connect the center exhaust pipe assembly and install the 2 compression springs with the 2 bolts.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL1032X_01_0005" proc-id="RM23G0E___0000JMD00000">
<ptxt>CONNECT AIR FUEL RATIO SENSOR (w/ DPF)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the air fuel ratio sensor connector and attach the clamp.</ptxt>
<figure>
<graphic graphicname="A239606E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wire Harness</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure that the part of the wire harness labeled A is as shown in the illustration.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BL1032X_01_0006" proc-id="RM23G0E___00007QT00000">
<ptxt>INSTALL NO. 3 FRAME CROSSMEMBER SUB-ASSEMBLY (w/ DPF)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the frame crossmember to the rear engine mounting insulator with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>306</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the frame crossmember with the 4 bolts and 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>72</t-value1>
<t-value2>734</t-value2>
<t-value4>53</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002BL1032X_01_0007" proc-id="RM23G0E___00007QG00000">
<ptxt>INSTALL FRONT SUSPENSION MEMBER BRACKET LH AND RH (w/ DPF)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front suspension member bracket LH and front suspension member bracket RH with the 8 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>33</t-value1>
<t-value2>337</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002BL1032X_01_0008" proc-id="RM23G0E___00006DJ00000">
<ptxt>INSPECT FOR EXHAUST GAS LEAK (w/ DPF)
</ptxt>
<content1 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>If gas is leaking, tighten the areas necessary to stop the leak. Replace damaged parts as necessary.</ptxt>
</item>
</list1>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>