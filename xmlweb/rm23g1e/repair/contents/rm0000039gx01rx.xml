<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S001X" variety="S001X">
<name>DOOR LOCK</name>
<ttl id="12051_S001X_7B9AU_T00KI" variety="T00KI">
<name>FRONT DOOR LOCK</name>
<para id="RM0000039GX01RX" category="A" type-id="30014" name-id="DL94V-01" from="201210">
<name>INSTALLATION</name>
<subpara id="RM0000039GX01RX_02" type-id="11" category="10" proc-id="RM23G0E___0000EKI00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039GX01RX_01" type-id="01" category="01">
<s-1 id="RM0000039GX01RX_01_0012" proc-id="RM23G0E___0000EK900001">
<ptxt>INSTALL FRONT DOOR INSIDE LOCKING CABLE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door inside locking cable assembly.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 claws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0013" proc-id="RM23G0E___0000EKA00001">
<ptxt>INSTALL FRONT DOOR LOCK REMOTE CONTROL CABLE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door lock remote control cable assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0003" proc-id="RM23G0E___0000EK800001">
<ptxt>INSTALL FRONT DOOR LOCK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When reusing the removed front door lock assembly, replace the door lock wiring harness seal on the connector with a new one.</ptxt>
</item>
<item>
<ptxt>Do not allow grease or dust to adhere to the surface of the connector which contacts the door lock wiring harness seal.</ptxt>
</item>
<item>
<ptxt>Reusing the door lock wiring harness seal or using a damaged door lock wiring harness seal may allow water into the connection. This may result in a malfunction of the front door lock assembly.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Apply MP grease to the sliding parts of the front door lock assembly.</ptxt>
</s2>
<s2>
<ptxt>Install a new door lock wiring harness seal to the front door lock assembly.</ptxt>
</s2>
<s2>
<ptxt>Insert the front door lock open rod to the front door lock assembly.</ptxt>
<figure>
<graphic graphicname="B237873E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Slide</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check that the front door lock open rod is securely connected to the front door lock assembly.</ptxt>
</s2>
<s2>
<ptxt>Using a T30 "TORX" wrench, install the front door lock assembly with the 3 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039GX01RX_01_0038" proc-id="RM23G0E___0000EKD00001">
<ptxt>INSTALL FRONT DOOR OUTSIDE HANDLE COVER WITH LOCK CYLINDER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door outside handle cover with lock cylinder assembly.</ptxt>
<atten4>
<ptxt>Make sure that the front door lock cylinder rod is inserted into the front door lock assembly.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using a T30 "TORX" socket wrench, install the front door lock cylinder with the screw.</ptxt>
<figure>
<graphic graphicname="B238538" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>4.0</t-value1>
<t-value2>41</t-value2>
<t-value3>35</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the hole plug.</ptxt>
<figure>
<graphic graphicname="B238537" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0039" proc-id="RM23G0E___0000EKE00001">
<ptxt>INSTALL FRONT DOOR REAR LOWER FRAME SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door rear lower frame sub-assembly with the bolt as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B241226" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0040" proc-id="RM23G0E___0000EKF00001">
<ptxt>INSTALL FRONT DOOR GLASS RUN LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door glass run.</ptxt>
<figure>
<graphic graphicname="B238535" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0041" proc-id="RM23G0E___0000EKG00001">
<ptxt>INSTALL FRONT DOOR GLASS SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</s2>
<s2>
<ptxt>Connect the power window regulator master switch assembly and move the front door glass sub-assembly so that the door glass bolt installation locations can be seen.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal and power window regulator master switch assembly.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Insert the front door glass sub-assembly into the front door panel along the front door glass run as indicated by the arrows in the order shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B241243E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the front door glass sub-assembly with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B238531E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>Tighten the bolts in the order shown in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0042" proc-id="RM23G0E___0000EKH00001">
<ptxt>INSTALL FRONT DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply new butyl tape to the front door panel.</ptxt>
</s2>
<s2>
<ptxt>Pass the front door lock remote control cable assembly and front door inside locking cable assembly through a new front door service hole cover.</ptxt>
<figure>
<graphic graphicname="B238607E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reference Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Attach the front door service hole cover using the reference points on the front door panel.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>There should be no wrinkles or folds after attaching the service hole cover.</ptxt>
</item>
<item>
<ptxt>After attaching the service hole cover, check the seal quality.</ptxt>
</item>
</list1>
</atten3>
<atten3>
<ptxt>Securely install the front door service hole cover preventing wrinkles and air bubbles.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Attach the 2 clamps.</ptxt>
<figure>
<graphic graphicname="B238530" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the bolt to the front door wire.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0043" proc-id="RM23G0E___0000BZ000001">
<ptxt>INSTALL FRONT DOOR INNER GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door inner glass weatherstrip.</ptxt>
<figure>
<graphic graphicname="B238605" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0044" proc-id="RM23G0E___0000BYV00001">
<ptxt>INSTALL FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the front door lock remote control cable assembly and front door inside locking cable assembly.</ptxt>
<figure>
<graphic graphicname="B241244" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Position Memory System:</ptxt>
<s3>
<ptxt>Connect the connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Attach the front door trim board sub-assembly by attaching the 4 claws of the front door inner glass weatherstrip as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B241251" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 12 clips and front door trim board retainer to install the front door trim board sub-assembly.</ptxt>
<figure>
<graphic graphicname="B238522" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0045" proc-id="RM23G0E___0000BYW00001">
<ptxt>INSTALL ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the assist grip cover.</ptxt>
<figure>
<graphic graphicname="B241252" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0046" proc-id="RM23G0E___0000BYX00001">
<ptxt>INSTALL NO. 2 DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws to install the inside handle bezel.</ptxt>
<figure>
<graphic graphicname="B241259" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0047" proc-id="RM23G0E___0000BYY00001">
<ptxt>INSTALL FRONT DOOR LOWER FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the front door lower frame bracket garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039GX01RX_01_0025" proc-id="RM23G0E___0000EKB00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039GX01RX_01_0027" proc-id="RM23G0E___0000EKC00001">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0INX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>