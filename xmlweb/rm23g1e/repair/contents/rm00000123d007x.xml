<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000B" variety="S000B">
<name>5L-E ENGINE MECHANICAL</name>
<ttl id="12007_S000B_7B8XU_T007I" variety="T007I">
<name>CAMSHAFT</name>
<para id="RM00000123D007X" category="A" type-id="80001" name-id="EMCHA-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM00000123D007X_01" type-id="01" category="01">
<s-1 id="RM00000123D007X_01_0028" proc-id="RM23G0E___00004WH00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000123D007X_01_0029" proc-id="RM23G0E___000014X00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 13 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0030" proc-id="RM23G0E___00004WI00001">
<ptxt>REMOVE FRONT BUMPER COVER LOWER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper cover lower.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0031" proc-id="RM23G0E___00004WJ00001">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Unhook the engine under cover from the vehicle body as shown in the illustration.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0032" proc-id="RM23G0E___000014W00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<figure>
<graphic graphicname="A224962E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Drain the coolant by removing the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug and drain the coolant from the engine.</ptxt>
<atten3>
<ptxt>If coolant is not drained from the radiator drain cock plug, cylinder block drain cock plugs and radiator reservoir, clogging in the radiator or coolant leakage from the seal of the water pump may result.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0043" proc-id="RM23G0E___00004WU00001">
<ptxt>REMOVE FRONT FENDER APRON SEAL RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 clips and fender apron seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0044" proc-id="RM23G0E___000014100000">
<ptxt>REMOVE RESONATOR WITH AIR CLEANER CAP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224304" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the sensor connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Loosen the hose clamp and remove the resonator with air cleaner cap.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A226728" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 4 hook clamps, and then remove the air cleaner cap and resonator with air cleaner cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0045">
<ptxt>REMOVE AIR CLEANER FILTER ELEMENT SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM00000123D007X_01_0046" proc-id="RM23G0E___00004WV00001">
<ptxt>REMOVE AIR CLEANER CASE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and air cleaner case.</ptxt>
<figure>
<graphic graphicname="A224627" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0047" proc-id="RM23G0E___00004WW00001">
<ptxt>DISCONNECT WIRE HARNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and generator wire.</ptxt>
<figure>
<graphic graphicname="A225290" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the generator connector and cooler compressor connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 wire harness clamps.</ptxt>
</s2>
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Detach the 4 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="A225291" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000123D007X_01_0048" proc-id="RM23G0E___00004WX00001">
<ptxt>REMOVE WIRING HARNESS CLAMP BRACKET (for LHD)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and wiring harness clamp bracket.</ptxt>
<figure>
<graphic graphicname="A225292" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0033" proc-id="RM23G0E___00004WK00001">
<ptxt>REMOVE NO. 1 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp and remove the No. 1 radiator hose.</ptxt>
<figure>
<graphic graphicname="A218411" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts and hose clamp.</ptxt>
<figure>
<graphic graphicname="A218417" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0034" proc-id="RM23G0E___00004WL00001">
<ptxt>REMOVE RADIATOR RESERVE TANK ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the reservoir hose from the upper side of the radiator tank.</ptxt>
<figure>
<graphic graphicname="A224937" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and radiator reservoir.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0035" proc-id="RM23G0E___00004WM00001">
<ptxt>REMOVE FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 4 nuts holding the fluid coupling fan.</ptxt>
<figure>
<graphic graphicname="A218410" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the vane pump V belt and the fan and generator V belt (See page <xref label="Seep01" href="RM00000122Y00CX"/>).</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts holding the fan shroud.</ptxt>
<figure>
<graphic graphicname="A224938" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 nuts of the fluid coupling fan, and then remove the shroud together with the coupling fan.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the fan pulley from the water pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0036" proc-id="RM23G0E___00004VA00001">
<ptxt>REMOVE VANE PUMP DRIVE PULLEY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A223614" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts, vane pump drive pulley and cooler compressor drive pulley.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0037" proc-id="RM23G0E___00004VB00001">
<ptxt>REMOVE CRANKSHAFT PULLEY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A058720E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using SST, remove the pulley bolt.</ptxt>
<sst>
<sstitem>
<s-number>09213-54015</s-number>
<s-subnumber>91651-60855</s-subnumber>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using SST, remove the pulley.</ptxt>
<figure>
<graphic graphicname="A058719E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05020</s-subnumber>
<s-subnumber>09954-05021</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00490</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-40011</s-number>
<s-subnumber>09957-04010</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0038" proc-id="RM23G0E___00004VI00001">
<ptxt>REMOVE IDLE PULLEY ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and Idle pulley bracket.</ptxt>
<figure>
<graphic graphicname="A224977" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0039" proc-id="RM23G0E___00004VC00001">
<ptxt>REMOVE TIMING BELT COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 11 bolts, washers, timing belt cover, and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A112017" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0040" proc-id="RM23G0E___00004VD00001">
<ptxt>REMOVE TIMING BELT GUIDE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the timing belt guide.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0041" proc-id="RM23G0E___00004VF00001">
<ptxt>SET NO. 1 CYLINDER TO TDC/COMPRESSION
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>If reusing the timing belt, draw a direction arrow on the timing belt (in the direction the belt move when the engine is running), and place matchmarks on the pulleys and timing belt.</ptxt>
</atten4>
<s2>
<ptxt>Turn the crankshaft 90° counterclockwise, and align the timing mark of the crankshaft timing pulley with the protrusion of the timing belt case.</ptxt>
<figure>
<graphic graphicname="A058730" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>

<ptxt>If the timing belt is disengaged, having the crankshaft timing pulley at the wrong angle can cause the piston head and valve head to come into contact with each other when removing the camshaft timing pulley, and camshaft causing damage. Therefore, always set the crankshaft pulley is removed at the correct angle.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Loosen the No. 1 timing belt idler bolt (A), and shift the idler to the left as far as possible.</ptxt>
<figure>
<graphic graphicname="A058731E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pry</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Move</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the No. 1 timing belt idler bolt (A), and then relieve the timing belt tension.</ptxt>
</s2>
<s2>
<ptxt>Remove the timing belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0042" proc-id="RM23G0E___00004VF00001">
<ptxt>REMOVE TIMING BELT
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>If reusing the timing belt, draw a direction arrow on the timing belt (in the direction the belt move when the engine is running), and place matchmarks on the pulleys and timing belt.</ptxt>
</atten4>
<s2>
<ptxt>Turn the crankshaft 90° counterclockwise, and align the timing mark of the crankshaft timing pulley with the protrusion of the timing belt case.</ptxt>
<figure>
<graphic graphicname="A058730" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>

<ptxt>If the timing belt is disengaged, having the crankshaft timing pulley at the wrong angle can cause the piston head and valve head to come into contact with each other when removing the camshaft timing pulley, and camshaft causing damage. Therefore, always set the crankshaft pulley is removed at the correct angle.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Loosen the No. 1 timing belt idler bolt (A), and shift the idler to the left as far as possible.</ptxt>
<figure>
<graphic graphicname="A058731E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pry</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Move</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the No. 1 timing belt idler bolt (A), and then relieve the timing belt tension.</ptxt>
</s2>
<s2>
<ptxt>Remove the timing belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0027" proc-id="RM23G0E___00004UR00000">
<ptxt>REMOVE INTAKE PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224305E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Loosen the hose clamp and remove the 2 bolts and intake pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0011" proc-id="RM23G0E___00004UT00000">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 9 bolts, nut, cylinder head cover and gasket.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000123D007X_01_0004" proc-id="RM23G0E___00004WC00001">
<ptxt>REMOVE CAMSHAFT TIMING PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using the crankshaft pulley bolt, turn the crankshaft 90° counterclockwise and align the timing mark of the crankshaft timing pulley with the protrusion of the timing belt case.</ptxt>
<figure>
<graphic graphicname="A057443E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Timing Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Set the No. 1 cylinder to 90° BTDC/compression to avoid interference with the piston top and valve head.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using SST, loosen the pulley bolt.</ptxt>
<figure>
<graphic graphicname="A224023E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09960-10010</s-number>
<s-subnumber>09962-01000</s-subnumber>
<s-subnumber>09963-01000</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST, separate the timing pulley from the camshaft.</ptxt>
<figure>
<graphic graphicname="A224024E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05010</s-subnumber>
<s-subnumber>09954-05021</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the pulley bolt and timing pulley.</ptxt>
</s2>
<s2>
<ptxt>Remove the timing pulley woodruff key.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000123D007X_01_0005" proc-id="RM23G0E___00004WD00001">
<ptxt>REMOVE NO. 2 TIMING BELT COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and timing belt cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000123D007X_01_0006" proc-id="RM23G0E___00004WE00001">
<ptxt>REMOVE CAMSHAFT OIL SEAL RETAINER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts, retainer and gasket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000123D007X_01_0007" proc-id="RM23G0E___00004WF00001">
<ptxt>REMOVE CAMSHAFT OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver and hammer, tap out the oil seal.</ptxt>
<figure>
<graphic graphicname="A057634E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000123D007X_01_0008" proc-id="RM23G0E___00004WG00001">
<ptxt>REMOVE CAMSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the camshaft with a wrench so that the key groove faces upward.</ptxt>
<figure>
<graphic graphicname="A057786E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upward</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Key Groove</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Uniformly loosen and remove the 10 bearing cap bolts in several steps in the sequence shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A057787E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 5 bearing caps and camshaft.</ptxt>
<atten4>
<ptxt>Arrange the bearing caps in the correct order.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>