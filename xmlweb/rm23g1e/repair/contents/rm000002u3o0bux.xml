<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B98Z_T00IN" variety="T00IN">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM000002U3O0BUX" category="J" type-id="804HB" name-id="AV6TA-09" from="201210">
<dtccode/>
<dtcname>Sound Signal Circuit between Radio Receiver and Multi-media Interface ECU</dtcname>
<subpara id="RM000002U3O0BUX_01" type-id="60" category="03" proc-id="RM23G0E___0000BQ400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The multi-media interface ECU sends sound signals to the radio receiver assembly through this circuit.</ptxt>
<ptxt>If there is an open or short in the circuit, sound cannot be heard from the speakers even if there is no malfunction in the stereo component amplifier assembly, radio receiver assembly or speakers.</ptxt>
</content5>
</subpara>
<subpara id="RM000002U3O0BUX_02" type-id="32" category="03" proc-id="RM23G0E___0000BQ500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E187093E05" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002U3O0BUX_04" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002U3O0BUX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002U3O0BUX_03_0001" proc-id="RM23G0E___0000BQ600001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA INTERFACE ECU - RADIO RECEIVER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H33 multi-media interface ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the H36 radio receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>H33-2 (URO+) - H36-2 (CDR+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H33-3 (URO-) - H36-3 (CDR-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H33-4 (ULO+) - H36-4 (CDL+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H33-5 (ULO-) - H36-5 (CDL-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H33-1 (USD1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H33-2 (URO+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H33-3 (URO-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H33-4 (ULO+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H33-5 (ULO-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002U3O0BUX_03_0005" fin="true">OK</down>
<right ref="RM000002U3O0BUX_03_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U3O0BUX_03_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002U3O0BUX_03_0005">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000012A80F4X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>