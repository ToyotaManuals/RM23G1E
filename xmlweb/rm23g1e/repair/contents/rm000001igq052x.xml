<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM000001IGQ052X" category="C" type-id="801AR" name-id="ESMGY-08" from="201207" to="201210">
<dtccode>P0069</dtccode>
<dtcname>MAP Sensor Too High or Too Low</dtcname>
<subpara id="RM000001IGQ052X_01" type-id="60" category="03" proc-id="RM23G0E___00001ZY00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>To detect atmospheric pressure, the atmospheric pressure sensor built into the ECM is used in the common rail system. Following the changes in the atmospheric pressure, the ECM corrects the fuel injection volume, timing and duration, and adjusts the common rail internal fuel pressure to optimize combustion.</ptxt>
<table pgwide="1">
<title>P0069</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1 second elapses after ignition switch turned from off to ON (starter remains off)</ptxt>
</entry>
<entry valign="middle">
<ptxt>After the ignition switch is turned to ON, the difference between the atmospheric pressure sensor output and manifold absolute pressure sensor output is more than 8 kPa for 0.5 seconds (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0069</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Atmosphere Pressure</ptxt>
</item>
<item>
<ptxt>MAP</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Turn the ignition switch to ON and check the Atmosphere Pressure and MAP items in the Data List. If the atmospheric pressure displayed on the tester differs greatly from the actual atmospheric pressure (standard: 101 kPa), the sensor (atmospheric pressure sensor or manifold absolute pressure sensor) is malfunctioning.</ptxt>
</item>
<item>
<ptxt>If DTC P0069 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
<item>
<ptxt>Lack of power</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000001IGQ052X_02" type-id="51" category="05" proc-id="RM23G0E___00001ZZ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK061X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Although the DTC titles say manifold absolute pressure Sensor Too High or Too Low, these DTCs relate to the atmospheric pressure sensor.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000001IGQ052X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001IGQ052X_03_0001" proc-id="RM23G0E___000020000000">
<testtitle>CHECK OTHER DTC OUTPUT (IN ADDITION TO DTC P0069)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0069 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0069 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If codes other than P0069 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000001IGQ052X_03_0003" fin="false">B</down>
<right ref="RM000001IGQ052X_03_0002" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000001IGQ052X_03_0003" proc-id="RM23G0E___000020100000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001IGQ052X_03_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001IGQ052X_03_0004" proc-id="RM23G0E___000020200000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000000PDK0Z6X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001IGQ052X_03_0005" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001IGQ052X_03_0002">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW03LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001IGQ052X_03_0005">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>