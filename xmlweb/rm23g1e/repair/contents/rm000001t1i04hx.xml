<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001M" variety="S001M">
<name>STEERING COLUMN</name>
<ttl id="12038_S001M_7B98P_T00ID" variety="T00ID">
<name>STEERING LOCK SYSTEM</name>
<para id="RM000001T1I04HX" category="J" type-id="3053G" name-id="SR314-03" from="201210">
<dtccode/>
<dtcname>Unable to Lock Steering Wheel</dtcname>
<subpara id="RM000001T1I04HX_01" type-id="60" category="03" proc-id="RM23G0E___0000BB500001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The steering lock ECU sends an unlock position signal to the power management control ECU. Upon receiving the signal, the power management control ECU permits an engine start. This prevents the engine from being started with the steering locked.</ptxt>
<ptxt>The steering lock actuator activates the steering lock motor and moves the lock bar into the steering column to lock the steering wheel.</ptxt>
<ptxt>When the steering lock is operating, the steering lock may not lock when the lock bar is not aligned with the lock hole of the steering column. In this case, the steering lock can be locked by turning the steering wheel a little in the same manner as is done for a vehicle with a mechanical key to change the position of the lock hole.</ptxt>
<ptxt>The diagnosis information of the steering lock ECU is transmitted to the intelligent tester via the certification ECU as the steering lock ECU is not connected to the CAN communication system.</ptxt>
</content5>
</subpara>
<subpara id="RM000001T1I04HX_02" type-id="32" category="03" proc-id="RM23G0E___0000BB600001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C199368E11" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001T1I04HX_03" type-id="51" category="05" proc-id="RM23G0E___0000BB700001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>When the engine switch is off, the main body ECU may occasionally go into a non-active state called sleep mode. Therefore, before proceeding with the inspection, it is necessary to perform the following steps to wake up the ECU:</ptxt>
<ptxt>With the engine switch off, open the driver door. Then (with the engine switch still off) open and close any door several times at 1.5 second intervals.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If the steering lock actuator assembly (steering lock ECU) is replaced, with the engine switch off and the shift lever in P (for Automatic Transmission), open and close the driver side door to record the current lock position into the steering lock ECU. If this is not performed, the engine may not start.</ptxt>
</item>
<item>
<ptxt>When replacing the steering lock ECU, ID code box or certification ECU, registration must be performed. Refer to the Service Bulletin for the registration procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001T1I04HX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001T1I04HX_06_0011" proc-id="RM23G0E___0000BBA00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (LOCK REQUEST RECEIVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the steering lock command is functioning properly.</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.47in"/>
<colspec colname="COL2" colwidth="2.27in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Lock Request Receive</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering lock lock command reception status/NG or OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>NG: Lock request not received</ptxt>
<ptxt>OK: Lock request received</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The certification ECU receives a lock request signal within 10 seconds of turning the engine switch from on (IG) to off and opening a door, and OK is displayed on the intelligent tester screen.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0001" fin="false">OK</down>
<right ref="RM000001T1I04HX_06_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0001" proc-id="RM23G0E___0000BB800001">
<testtitle>INSPECT STEERING LOCK ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Move the shift lever to P.*</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*: for Automatic Transmission</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C197722E33" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G35-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G35-3 (IGE) - G35-1 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Steering lock motor operating immediately after turning engine switch from off to on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G35-3 (IGE) - G35-1 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Steering lock motor not operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Component with harness connected</ptxt>
<ptxt>(Steering Lock ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0004" fin="false">OK</down>
<right ref="RM000001T1I04HX_06_0024" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0004" proc-id="RM23G0E___0000BB900001">
<testtitle>CHECK HARNESS AND CONNECTOR (STEERING LOCK ECU - POWER MANAGEMENT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G51 power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G35 steering lock ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G35-3 (IGE) - G51-8 (SLR+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G35-3 (IGE) or G51-8 (SLR+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.84in"/>
<colspec colname="COL1" colwidth="2.84in"/>
<colspec colname="COL2" colwidth="1.4in"/>
<thead>
<row>
<entry namest="COLSPEC0" nameend="COL1" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COLSPEC0" nameend="COL1" valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Manual Tilt and Manual Telescopic Steering Column</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>for Power Tilt and Power Telescopic Steering Column</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0007" fin="true">A</down>
<right ref="RM000001T1I04HX_06_0008" fin="true">B</right>
<right ref="RM000001T1I04HX_06_0025" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0012" proc-id="RM23G0E___0000BBB00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (FL AND FR DOOR COURTESY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the driver side door courtesy switch is functioning properly.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.49in"/>
<colspec colname="COL2" colwidth="2.25in"/>
<colspec colname="COL3" colwidth="1.79in"/>
<colspec colname="COLSPEC0" colwidth="1.55in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FL Door Courtesy</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door courtesy switch LH/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front door courtesy switch LH on</ptxt>
<ptxt>OFF: Front door courtesy switch LH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FR Door Courtesy</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door courtesy switch RH/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front door courtesy switch RH on</ptxt>
<ptxt>OFF: Front door courtesy switch RH off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Driver side door courtesy switch is functioning properly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0013" fin="false">OK</down>
<right ref="RM000001T1I04HX_06_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0013" proc-id="RM23G0E___0000BBC00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (S CODE CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the S code certification is functioning properly.</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.47in"/>
<colspec colname="COL2" colwidth="2.27in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>S Code Check</ptxt>
</entry>
<entry valign="middle">
<ptxt>S code certification result/NG or OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: S code certification result normal</ptxt>
<ptxt>NG: S code certification result abnormal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>OK is displayed on the intelligent tester.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0014" fin="false">OK</down>
<right ref="RM000001T1I04HX_06_0018" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0014" proc-id="RM23G0E___0000BBD00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (L CODE CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the L code certification is functioning properly.</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.47in"/>
<colspec colname="COL2" colwidth="2.27in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L Code Check</ptxt>
</entry>
<entry valign="middle">
<ptxt>L code certification result/NG or OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: L code certification result normal</ptxt>
<ptxt>NG: L code certification result abnormal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>OK is displayed on the intelligent tester.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0015" fin="false">OK</down>
<right ref="RM000001T1I04HX_06_0020" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0015" proc-id="RM23G0E___0000BBE00001">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY (STEERING LOCK ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the steering lock actuator assembly (steering lock ECU) (See page <xref label="Seep01" href="RM0000039SJ011X"/> for Manual Tilt and Manual Telescopic Steering Column, <xref label="Seep02" href="RM0000039SJ012X"/> for Power Tilt and Power Telescopic Steering Column).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0016" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0016" proc-id="RM23G0E___0000BBF00001">
<testtitle>CHECK STEERING LOCK RELEASE OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Operate the steering wheel and check the steering lock condition.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Steering lock is released.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0022" fin="true">OK</down>
<right ref="RM000001T1I04HX_06_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0018" proc-id="RM23G0E___0000BBG00001">
<testtitle>REPLACE ID CODE BOX</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ID code box.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0019" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0019" proc-id="RM23G0E___0000BBH00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (S CODE CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the S code certification is functioning properly.</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.47in"/>
<colspec colname="COL2" colwidth="2.27in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>S Code Check</ptxt>
</entry>
<entry valign="middle">
<ptxt>S code certification result/NG or OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: S code certification result normal</ptxt>
<ptxt>NG: S code certification result abnormal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>OK is displayed on the intelligent tester.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0023" fin="true">OK</down>
<right ref="RM000001T1I04HX_06_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0020" proc-id="RM23G0E___0000BBI00001">
<testtitle>REPLACE ID CODE BOX</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ID code box.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0021" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0021" proc-id="RM23G0E___0000BBJ00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (L CODE CHECK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the L code certification is functioning properly.</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.47in"/>
<colspec colname="COL2" colwidth="2.27in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.57in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L Code Check</ptxt>
</entry>
<entry valign="middle">
<ptxt>L code certification result/NG or OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: L code certification result normal</ptxt>
<ptxt>NG: L code certification result abnormal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.84in"/>
<colspec colname="COL1" colwidth="2.84in"/>
<colspec colname="COL2" colwidth="1.4in"/>
<thead>
<row>
<entry namest="COLSPEC0" nameend="COL1" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COLSPEC0" nameend="COL1" valign="middle">
<ptxt>OK is displayed on the intelligent tester.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>NG is displayed on the intelligent tester.</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Manual Tilt and Manual Telescopic Steering Column</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>for Power Tilt and Power Telescopic Steering Column</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001T1I04HX_06_0023" fin="true">A</down>
<right ref="RM000001T1I04HX_06_0008" fin="true">B</right>
<right ref="RM000001T1I04HX_06_0025" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0017">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0008">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY (STEERING LOCK ECU)<xref label="Seep01" href="RM0000039SJ011X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0025">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY (STEERING LOCK ECU)<xref label="Seep01" href="RM0000039SJ012X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0006">
<testtitle>GO TO LIGHTING SYSTEM (DOOR COURTESY SWITCH CIRCUIT)<xref label="Seep01" href="RM0000011T3099X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0022">
<testtitle>END (STEERING LOCK ACTUATOR ASSEMBLY (STEERING LOCK ECU) IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0023">
<testtitle>END (ID CODE BOX IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000001T1I04HX_06_0024">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>