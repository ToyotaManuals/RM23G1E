<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12054_S0020" variety="S0020">
<name>METER / GAUGE / DISPLAY</name>
<ttl id="12054_S0020_7B9C1_T00LP" variety="T00LP">
<name>METER / GAUGE SYSTEM</name>
<para id="RM000002ZMC01DX" category="J" type-id="801NC" name-id="ME3L4-04" from="201210">
<dtccode/>
<dtcname>Meter Illumination does not Dim at Night</dtcname>
<subpara id="RM000002ZMC01DX_01" type-id="60" category="03" proc-id="RM23G0E___0000FDN00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the dimmer switch is turned to TAIL, HEAD or AUTO, the main body ECU sends a TAIL relay signal, panel light illumination signal, panel relay signal, and TAIL cancel OFF signal to the combination meter. Then the meter and accessory meter become dim.</ptxt>
<atten4>
<ptxt>TAIL cancel switch: When the headlights are illuminated and the TAIL cancel switch is turned ON, the meter does not dim.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002ZMC01DX_02" type-id="32" category="03" proc-id="RM23G0E___0000FDO00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E202100E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002ZMC01DX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002ZMC01DX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002ZMC01DX_04_0001" proc-id="RM23G0E___0000FDP00001">
<testtitle>CHECK CAN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if a CAN communication DTC is output.</ptxt>
<test2>
<ptxt>for LHD with Entry and Start System (See page <xref label="Seep01" href="RM000000WIB0CMX"/>).</ptxt>
</test2>
<test2>
<ptxt>for LHD without Entry and Start System (See page <xref label="Seep02" href="RM000000WIB0CNX"/>).</ptxt>
</test2>
<test2>
<ptxt>for RHD with Entry and Start System (See page <xref label="Seep03" href="RM000000WIB0COX"/>).</ptxt>
</test2>
<test2>
<ptxt>for RHD without Entry and Start System (See page <xref label="Seep04" href="RM000000WIB0CPX"/>).</ptxt>
<table frame="all">
<title>Result</title>
<tgroup cols="2">
<colspec colname="col1" colwidth="2.07in"/>
<colspec colname="col2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>CAN communication system DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>CAN communication system DTC is output (for LHD with Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>CAN communication system DTC is output (for LHD without Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>CAN communication system DTC is output (for RHD with Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>CAN communication system DTC is output (for RHD without Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002ZMC01DX_04_0002" fin="false">A</down>
<right ref="RM000002ZMC01DX_04_0005" fin="true">B</right>
<right ref="RM000002ZMC01DX_04_0010" fin="true">C</right>
<right ref="RM000002ZMC01DX_04_0011" fin="true">D</right>
<right ref="RM000002ZMC01DX_04_0012" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0002" proc-id="RM23G0E___0000FDQ00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (LIGHT CONTROL RHEOSTAT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Operate the intelligent tester according to the display and select Data List (See page <xref label="Seep01" href="RM0000012G70D6X"/>).</ptxt>
<table pgwide="1">
<title>Combination Meter</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tail Cancel SW*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>TAIL cancel switch condition/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: TAIL cancel switch ON</ptxt>
<ptxt>OFF: TAIL cancel switch OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rheostat value</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Light control rheostat switch input/Min.: 0, Max.: 100</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light control rheostat switch is fully turned left (0) → fully turned right (100)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Unit: %</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/ TAIL Cancel Switch</ptxt>
</item>
</list1>
<spec>
<title>OK</title>
<specitem>
<ptxt>Light brightness can be changed within specified range by manual operation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002ZMC01DX_04_0006" fin="true">OK</down>
<right ref="RM000002ZMC01DX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0003" proc-id="RM23G0E___0000FDR00001">
<testtitle>INSPECT LIGHT CONTROL RHEOSTAT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the light control rheostat. (w/o TAIL cancel switch)</ptxt>
<figure>
<graphic graphicname="E195766E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<test2>
<ptxt>Remove the light control rheostat (See page <xref label="Seep01" href="RM000003QKY00SX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>5 (T) - 1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 12 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7 (RV) - 1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Light control rheostat fully turned right → fully turned left</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 100 Ω → 8 to 12 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>

<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>8 (T) - 4 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 12 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6 (RV) - 4 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Light control rheostat fully turned right → fully turned left</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 100 Ω → 8 to 12 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>Inspect the light control rheostat. (w/ TAIL cancel switch)</ptxt>
<figure>
<graphic graphicname="E195765E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<test2>
<ptxt>Remove the light control rheostat (See page <xref label="Seep02" href="RM000003QKY00SX"/>).</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>5 (T) - 6 (TC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>TAIL cancel switch off → on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher → Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5 (T) - 1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 12 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7 (RV) - 1 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Light control rheostat fully turned right → fully turned left</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 100 Ω → 8 to 12 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>8 (T) - 7 (TC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>TAIL cancel switch off → on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher → Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>8 (T) - 4 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 12 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6 (RV) - 4 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Light control rheostat fully turned right → fully turned left</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 100 Ω → 8 to 12 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002ZMC01DX_04_0004" fin="false">OK</down>
<right ref="RM000002ZMC01DX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0004" proc-id="RM23G0E___0000FDS00001">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER - LIGHT CONTROL RHEOSTAT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G6 meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G16 rheostat connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G6-6 (SW1) - G16-5 (T)</ptxt>
</entry>
<entry morerows="3" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="3" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G6-7 (SW2) - G16-7 (RV)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G6-22 (SW3) - G16-1 (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G6-10 (TC) - G16-6 (TC)*</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G6-6 (SW1) - G16-8 (T)</ptxt>
</entry>
<entry morerows="3" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="3" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G6-7 (SW2) - G16-6 (RV)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G6-22 (SW3) - G16-4 (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G6-10 (TC) - G16-7 (TC)*</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>*: w/ TAIL Cancel Switch</ptxt>
</item>
</list1>
</test1>
</content6>
<res>
<down ref="RM000002ZMC01DX_04_0008" fin="true">OK</down>
<right ref="RM000002ZMC01DX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO086X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0006">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM000003QKY00RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0007">
<testtitle>REPLACE LIGHT CONTROL RHEOSTAT<xref label="Seep01" href="RM000003QKY00SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0008">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM000003QKY00RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0010">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO087X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0011">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO088X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZMC01DX_04_0012">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO089X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>