<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9CW_T00MK" variety="T00MK">
<name>REAR POWER SEAT CONTROL SYSTEM</name>
<para id="RM0000046CC00AX" category="J" type-id="801OM" name-id="SE5BC-04" from="201207" to="201210">
<dtccode/>
<dtcname>Folding Motor Circuit</dtcname>
<subpara id="RM0000046CC00AX_01" type-id="60" category="03" proc-id="RM23G0E___0000G9400000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The fold seat control ECU receives switch operation signals from the No. 1 and No. 2 fold seat switches and activates the power seat motors. At this time, the Hall IC detects the actuation of the seat cushion and sends a seat cushion actuation signal to the fold seat control ECU. The fold seat control ECU uses signals from the Hall IC to detect if an object is caught or if any other abnormal condition has occurred.</ptxt>
</content5>
</subpara>
<subpara id="RM0000046CC00AX_02" type-id="32" category="03" proc-id="RM23G0E___0000G9500000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B246557E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000046CC00AX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000046CC00AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000046CC00AX_04_0011" proc-id="RM23G0E___0000G9900000">
<testtitle>INSPECT SEAT LEG ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for LH Side:</ptxt>
<figure>
<graphic graphicname="B239766E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test2>
<ptxt>Remove the No. 3 seat leg assembly LH (See page <xref label="Seep01" href="RM0000045DH009X"/>).</ptxt>
</test2>
<test2>
<ptxt>Check if the No. 3 seat leg moves smoothly when the battery is connected to the cushion motor connector terminals.</ptxt>
</test2>
</test1>
<test1>
<ptxt>for RH Side:</ptxt>
<test2>
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep02" href="RM0000045DH009X"/>).</ptxt>
</test2>
<test2>
<ptxt>Check if the No. 2 seat leg moves smoothly when the battery is connected to the cushion motor connector terminals.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → 3 (FWD)</ptxt>
<ptxt>Battery negative (-) → 4 (BACK)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Forward</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → 4 (BACK)</ptxt>
<ptxt>Battery negative (-) → 3 (FWD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Backward</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK (for LH Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK (for RH Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for LH Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for RH Side)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000046CC00AX_04_0004" fin="false">A</down>
<right ref="RM0000046CC00AX_04_0007" fin="false">B</right>
<right ref="RM0000046CC00AX_04_0008" fin="true">C</right>
<right ref="RM0000046CC00AX_04_0010" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM0000046CC00AX_04_0004" proc-id="RM23G0E___0000G9700000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOLD SEAT CONTROL ECU - NO. 3 SEAT LEG)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the d2 and d1 ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the d4 No. 3 seat leg connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>d1-17 (SV1) - d4-5 (HLV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d1-17 (SV1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d1-18 (SFLD) - d4-2 (HLS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d1-18 (SFLD) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d1-25 (SG1) - d4-6 (HLG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d1-25 (SG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d2-8 (FLD+) - d4-3 (FWD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d2-8 (FLD+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d2-7 (FLD-) - d4-4 (BACK)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d2-7 (FLD-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000046CC00AX_04_0001" fin="false">OK</down>
<right ref="RM0000046CC00AX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046CC00AX_04_0001" proc-id="RM23G0E___0000G9600000">
<testtitle>CHECK FOLD SEAT CONTROL ECU LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the fold seat control ECU LH with its connectors still connected (See page <xref label="Seep01" href="RM0000045DH009X"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="B193667E17" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>d1-17 (SV1) - d1-25 (SG1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.5 to 8 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d1-18 (SFLD) - d1-25 (SG1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Motor is operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d2-8 (FLD+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No. 1 or No. 2 fold seat switch LH fold</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d2-7 (FLD-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No. 1 or No. 2 fold seat switch LH return</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Fold Seat Control ECU LH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000046CC00AX_04_0002" fin="true">OK</down>
<right ref="RM0000046CC00AX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046CC00AX_04_0002">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000001YWK00PX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000046CC00AX_04_0007" proc-id="RM23G0E___0000G9800000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOLD SEAT CONTROL ECU - NO. 2 SEAT LEG)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the c2 and c1 ECU connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the c4 No. 2 seat leg connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>c1-17 (SV1) - c4-5 (HLV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c1-17 (SV1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c1-18 (SFLD) - c4-2 (HLS)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c1-18 (SFLD) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c1-25 (SG1) - c4-6 (HLG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c1-25 (SG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c2-8 (FLD+) - c4-3 (FWD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c2-8 (FLD+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c2-7 (FLD-) - c4-4 (BACK)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c2-7 (FLD-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000046CC00AX_04_0012" fin="false">OK</down>
<right ref="RM0000046CC00AX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046CC00AX_04_0012" proc-id="RM23G0E___0000G9A00000">
<testtitle>CHECK FOLD SEAT CONTROL ECU RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the fold seat control ECU RH with its connectors still connected (See page <xref label="Seep01" href="RM0000045DP009X"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="B193667E20" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>c1-17 (SV1) - c1-25 (SG1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.5 to 8 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c1-18 (SFLD) - c1-25 (SG1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Motor is operating</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c2-8 (FLD+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No. 1 or No. 2 fold seat switch RH fold</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c2-7 (FLD-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No. 1 or No. 2 fold seat switch RH return</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Fold Seat Control ECU RH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000046CC00AX_04_0002" fin="true">OK</down>
<right ref="RM0000046CC00AX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046CC00AX_04_0008">
<testtitle>REPLACE NO. 3 SEAT LEG ASSEMBLY LH<xref label="Seep01" href="RM0000045DH009X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000046CC00AX_04_0010">
<testtitle>REPLACE NO. 2 SEAT LEG ASSEMBLY<xref label="Seep01" href="RM0000045DH009X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000046CC00AX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>