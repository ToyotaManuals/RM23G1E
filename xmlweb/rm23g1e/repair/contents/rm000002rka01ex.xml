<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12009_S000H" variety="S000H">
<name>1KD-FTV EMISSION CONTROL</name>
<ttl id="12009_S000H_7B8ZQ_T009E" variety="T009E">
<name>EGR VALVE (w/o DPF)</name>
<para id="RM000002RKA01EX" category="G" type-id="8000T" name-id="EC130-07" from="201207">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000002RKA01EX_02" type-id="11" category="10" proc-id="RM23G0E___000062U00000">
<content3 releasenbr="1">
<atten3>
<ptxt>Always stop the engine when installing or removing vacuum gauges, or removing the vacuum hoses.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM000002RKA01EX_01" type-id="01" category="01">
<s-1 id="RM000002RKA01EX_01_0001" proc-id="RM23G0E___000062S00000">
<ptxt>INSPECT ELECTRIC EGR CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 3-way connector, connect a vacuum gauge to the hose between the electric EGR control valve and E-VRV.</ptxt>
<figure>
<graphic graphicname="A223445E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vacuum Gauge</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>3-way Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check the seating of the electric EGR control valve.</ptxt>
<s3>
<ptxt>Start the engine. Check that the engine starts and then idles.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the output vacuum with a vacuum gauge.</ptxt>
<s3>
<ptxt>Connect a vacuum gauge to the output pipe of the electric EGR control valve.</ptxt>
</s3>
<s3>
<ptxt>Warm up the engine and check that the vacuum gauge reading is above 28 kPa (210 mmHg, 8.23 in.Hg).</ptxt>
<ptxt>If the result is not as specified, check for leaks between the electric EGR control valve and vacuum pump, or check the electric EGR control valve.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check "hot" engine condition.</ptxt>
<s3>
<ptxt>Visually check the vacuum hose between the vacuum pump and electric EGR control valve for leaks.</ptxt>
</s3>
<s3>
<ptxt>Warm up the engine.</ptxt>
<ptxt>The coolant temperature should be between 75°C (167°F) and 90°C (194°F).</ptxt>
</s3>
<s3>
<ptxt>Check that the vacuum gauge indicator reading increases by more than 28 kPa (210 mmHg, 8.23 in.Hg) at 1500 rpm.</ptxt>
</s3>
<s3>
<ptxt>When the accelerator pedal is fully depressed quickly, check that the vacuum gauge indicator reading drops momentarily.</ptxt>
</s3>
<s3>
<ptxt>Keep the engine speed at more than 4000 rpm.</ptxt>
</s3>
<s3>
<ptxt>Check that the vacuum gauge reading is as shown below.</ptxt>
<spec>
<title>Standard condition</title>
<specitem>
<ptxt>0 kPa (0 mmHg, 0 in.Hg)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>When the accelerator pedal is released, check that the vacuum gauge indicator reading drops momentarily while the engine speed decreases from more than 4000 rpm to idle.</ptxt>
<ptxt>If the result is not as specified, refer to the inspection section (See page <xref label="Seep01" href="RM000002RK701EX_01_0001"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>Remove the vacuum gauge and connect the vacuum hose to the electric EGR control valve.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002RKA01EX_01_0002" proc-id="RM23G0E___000062T00000">
<ptxt>INSPECT NO. 2 EGR VALVE ASSEMBLY (w/ EGR Cooler)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>With the engine stopped, make sure the EGR cooler by-pass regulating link is in the Non-bypass Position as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A223446E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Non-by-pass Position</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bypass Position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>After the engine idle condition is stable and the and engine coolant temperature is less than 70°C (158°F), make sure the EGR cooler by-pass regulating link is in the Bypass Position as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>When the engine coolant temperature is 75°C (167°F) or more, make sure the EGR cooler by-pass valve actuating link is in the Non-bypass Position as shown in the illustration.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>