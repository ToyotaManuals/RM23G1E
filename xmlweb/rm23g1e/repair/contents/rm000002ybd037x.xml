<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0012" variety="S0012">
<name>A343F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0012_7B93Z_T00DN" variety="T00DN">
<name>SHIFT LEVER</name>
<para id="RM000002YBD037X" category="A" type-id="30014" name-id="AT7ZT-01" from="201207">
<name>INSTALLATION</name>
<subpara id="RM000002YBD037X_01" type-id="01" category="01">
<s-1 id="RM000002YBD037X_01_0001" proc-id="RM23G0E___00007G500000">
<ptxt>INSTALL TRANSMISSION FLOOR SHIFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the transmission floor shift with the 4 bolts. Tighten the bolts equally.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YBD037X_01_0020" proc-id="RM23G0E___00007G700000">
<ptxt>CONNECT TRANSMISSION CONTROL CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C214912" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Connect the transmission control cable to the shift lever retainer, and install a new clip.</ptxt>
</s2>
<s2>
<ptxt>Connect the control cable end to the shift lever.</ptxt>
<figure>
<graphic graphicname="C214910E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure to connect the cable end so that the inner cable is not twisted. Confirm that the ridged side of the cable end is facing upward.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ridged</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002YBD037X_01_0028" proc-id="RM23G0E___00007DI00000">
<ptxt>ADJUST SHIFT LEVER POSITION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear console box (See page <xref label="Seep01" href="RM0000046JD004X"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Refrigerated Cool Box:</ptxt>
<ptxt>Remove the rear console box (See page <xref label="Seep02" href="RM0000046MR009X"/>).</ptxt>
</s2>
<s2>
<ptxt>Move the shift lever to N.</ptxt>
</s2>
<s2>
<ptxt>Loosen the nut of the control cable end.</ptxt>
<figure>
<graphic graphicname="C214920" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>While pushing the control cable slightly toward the rear side of the vehicle, tighten the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Move the shift lever and check that there is less wobble when moving the shift lever from N to D than when moving the shift lever to P.</ptxt>
</s2>
<s2>
<ptxt>Install the rear console box (See page <xref label="Seep03" href="RM0000046JB004X"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Refrigerated Cool Box:</ptxt>
<ptxt>Install the rear console box (See page <xref label="Seep04" href="RM0000046MQ009X"/>).</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002YBD037X_01_0015" proc-id="RM23G0E___00005AT00000">
<ptxt>INSPECT SHIFT LEVER POSITION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>When moving the shift lever from P to R with the ignition switch to ON and the brake pedal depressed, make sure that it moves smoothly and correctly into position.</ptxt>
</s2>
<s2>
<ptxt>Check that the shift lever does not stop when moving the shift lever from R to P, and check that the shift lever does not stick when moving the shift lever from D to L.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and make sure that the vehicle moves forward after moving the shift lever from N to D and moves in reverse after moving the shift lever to R.</ptxt>
<ptxt>If the operation cannot be performed as specified, inspect the park/neutral position switch assembly and check the transmission floor shift assembly installation condition.</ptxt>
<ptxt>If the indicator and shift lever position do not match, carry out the following adjustment procedures.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002YBD037X_01_0002" proc-id="RM23G0E___00007G600000">
<ptxt>INSTALL REAR CONSOLE BOX ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear console box (See page <xref label="Seep01" href="RM0000046JB004X"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Refrigerated Cool Box:</ptxt>
<ptxt>Install the rear console box (See page <xref label="Seep02" href="RM0000046MQ009X"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>