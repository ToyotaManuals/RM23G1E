<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001P" variety="S001P">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001P_7B99L_T00J9" variety="T00J9">
<name>NAVIGATION SYSTEM (for DVD)</name>
<para id="RM00000182V0GNX" category="C" type-id="803MB" name-id="NS70V-02" from="201207" to="201210">
<dtccode>B15C2</dtccode>
<dtcname>Speed Signal Malfunction</dtcname>
<subpara id="RM00000182V0GNX_01" type-id="60" category="03" proc-id="RM23G0E___0000CGN00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The display and navigation module display receives a vehicle speed signal from the combination meter assembly and information about the navigation antenna, and then adjusts the vehicle position.</ptxt>
<ptxt>The display and navigation module display stores this DTC when the difference between the speed information that the navigation antenna receives and the SPD pulse received from the combination meter assembly becomes large. </ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A voltage of 12 V or 5 V is output from each ECU and then input to the combination meter assembly. This signal is changed to a pulse signal at the transistor in the combination meter assembly. Each ECU controls its respective systems based on the pulse signal.</ptxt>
</item>
<item>
<ptxt>If a short occurs in any of the ECUs or in a wire harness connected to an ECU, all systems related to the components in the diagram below will not operate normally.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B15C2</ptxt>
</entry>
<entry valign="middle">
<ptxt>A difference between the GPS speed and SPD pulse is detected.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Meter/gauge System</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Display and navigation module display</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000182V0GNX_04" type-id="32" category="03" proc-id="RM23G0E___0000CGP00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E185489E17" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000182V0GNX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000182V0GNX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000182V0GNX_03_0001" proc-id="RM23G0E___0000CGO00000">
<testtitle>CHECK VEHICLE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter "GPS &amp; Vehicle Sensor" (Vehicle Sensors) (See page <xref label="Seep01" href="RM0000043X902GX"/>).</ptxt>
<figure>
<graphic graphicname="E176549" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>While driving the vehicle, compare the "SPD" indicator to the reading on the speedometer. Check if these readings are equal or almost equal.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The readings are equal or almost equal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000182V0GNX_03_0010" fin="true">OK</down>
<right ref="RM00000182V0GNX_03_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000182V0GNX_03_0010">
<testtitle>REPLACE DISPLAY AND NAVIGATION MODULE DISPLAY<xref label="Seep01" href="RM000003B6H01LX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000182V0GNX_03_0011">
<testtitle>GO TO METER / GAUGE SYSTEM<xref label="Seep01" href="RM000002Z4L031X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>