<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97M_T00HA" variety="T00HA">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Vacuum Brake Booster)</name>
<para id="RM000000XIG0HXX" category="C" type-id="803OL" name-id="BCDPV-01" from="201207" to="201210">
<dtccode>C1422</dtccode>
<dtcname>Master Cylinder Pressure Sensor Zero Point High Malfunction</dtcname>
<subpara id="RM000000XIG0HXX_01" type-id="60" category="03" proc-id="RM23G0E___0000AOD00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1421, C1423 and C1424 (See page <xref label="Seep01" href="RM000000XIG0HWX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1422</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the stop light switch is off, the PMC terminal voltage is higher than 0.86 V for 5 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>STOP fuse</ptxt>
</item>
<item>
<ptxt>Stop light switch</ptxt>
</item>
<item>
<ptxt>Stop light switch circuit</ptxt>
</item>
<item>
<ptxt>Master cylinder pressure sensor circuit</ptxt>
</item>
<item>
<ptxt>Brake actuator assembly (Master cylinder pressure sensor)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XIG0HXX_04" type-id="32" category="03" proc-id="RM23G0E___0000AOI00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC C1425 (See page <xref label="Seep01" href="RM000000XIE0KYX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XIG0HXX_02" type-id="51" category="05" proc-id="RM23G0E___0000AOE00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08JX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When DTC C1425 is output together with DTC C1422, inspect and repair the trouble areas indicated by DTC C1425 first (See page <xref label="Seep02" href="RM000000XIE0KYX"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XIG0HXX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIG0HXX_03_0019" proc-id="RM23G0E___0000AOH00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STOP LIGHT SW)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.18in"/>
<colspec colname="COL2" colwidth="2.00in"/>
<colspec colname="COL3" colwidth="1.95in"/>
<colspec colname="COLSPEC1" colwidth="1.95in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Brake pedal depressed</ptxt>
<ptxt>OFF: Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that the stop light switch display observed on the intelligent tester changes according to the brake pedal operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The intelligent tester displays ON or OFF according to brake pedal operation.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000XIG0HXX_03_0010" fin="false">OK</down>
<right ref="RM000000XIG0HXX_03_0021" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0010" proc-id="RM23G0E___0000AOG00000">
<testtitle>CHECK BRAKE PEDAL AND STOP LIGHT SWITCH INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check the brake pedal height and stop light switch installation (See page <xref label="Seep01" href="RM000002W8X04TX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The brake pedal height and stop light switch installation are normal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XIG0HXX_03_0004" fin="false">OK</down>
<right ref="RM000000XIG0HXX_03_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0004" proc-id="RM23G0E___0000AOF00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0F0X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 40 km/h (25 mph) or more and perform a braking test (decelerate the vehicle by depressing the brake pedal).</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM000000XHV0F0X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.65in"/>
<colspec colname="COL2" colwidth="1.48in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC C1422 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1422 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIG0HXX_03_0005" fin="true">A</down>
<right ref="RM000000XIG0HXX_03_0007" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0021" proc-id="RM23G0E___0000ANY00000">
<testtitle>CHECK TERMINAL VOLTAGE (STOP LIGHT SWITCH POWER SOURCE TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Make sure that there is no looseness at the locking parts and connecting parts of the connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A5 stop light switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C215403E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.67in"/>
<colspec colname="COL2" colwidth="1.09in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A5-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Stop Light Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIG0HXX_03_0022" fin="false">OK</down>
<right ref="RM000000XIG0HXX_03_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0022" proc-id="RM23G0E___0000ANZ00000">
<testtitle>INSPECT STOP LIGHT SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the stop light switch (See page <xref label="Seep01" href="RM000003QJT00TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C215404E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch pin not pushed in</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Switch pin pushed in</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Stop Light Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIG0HXX_03_0020" fin="false">OK</down>
<right ref="RM000000XIG0HXX_03_0023" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0020" proc-id="RM23G0E___0000AO000000">
<testtitle>CHECK TERMINAL VOLTAGE (STP)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Make sure that there is no looseness at the locking parts and connecting parts of the connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure  the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C199357E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>A6-2 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XIG0HXX_03_0007" fin="true">OK</down>
<right ref="RM000000XIG0HXX_03_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0005">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0007">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0013">
<testtitle>ADJUST BRAKE PEDAL OR STOP LIGHT SWITCH<xref label="Seep01" href="RM000002W8X04TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0018">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XIG0HXX_03_0023">
<testtitle>REPLACE STOP LIGHT SWITCH</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>