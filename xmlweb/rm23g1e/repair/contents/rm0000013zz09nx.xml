<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A5_T00JT" variety="T00JT">
<name>REAR VIEW MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM0000013ZZ09NX" category="U" type-id="3001G" name-id="PM2WU-02" from="201207">
<name>TERMINALS OF ECU</name>
<subpara id="RM0000013ZZ09NX_z0" proc-id="RM23G0E___0000CYF00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK PARKING ASSIST ECU</ptxt>
<figure>
<graphic graphicname="E159063E11" width="7.106578999in" height="2.775699831in"/>
</figure>
<step2>
<ptxt>Measure the voltage, resistance and check for pulses according to value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I3-1 (+B) - I3-3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power source signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I3-3 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I3-7 (IG) - I3-3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>IG power source signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I3-8 (ACC) - I3-3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>ACC power source signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I2-11 (CV-) - I3-3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear television camera ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>I2-12 (CV+) - I2-11 (CV-)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>R - W</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Rear television camera display signal (NTSC) input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R, screen blacked out by covering camera lens</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I2-13 (CGND) - I3-3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>GR - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear television camera shielded ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I2-14 (CB+) - I3-3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power source to rear television camera</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.8 to 7.05 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 1.</ptxt>
<figure>
<graphic graphicname="E159949E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Measurement Condition</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>I2-12 (CV+) - I2-11 (CV-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.2 V/DIV., 50 μsec./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 2.</ptxt>
<figure>
<graphic graphicname="E164781E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Measurement Condition</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>I2-12 (CV+) - I2-11 (CV-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.2 V/DIV., 50 μsec./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R, screen blacked out by covering camera lens</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>