<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000L" variety="S000L">
<name>1KD-FTV INTAKE / EXHAUST</name>
<ttl id="12010_S000L_7B90T_T00AH" variety="T00AH">
<name>INTAKE MANIFOLD (w/ DPF)</name>
<para id="RM0000044GD00CX" category="G" type-id="3001K" name-id="IE0Y6-01" from="201207">
<name>INSPECTION</name>
<subpara id="RM0000044GD00CX_01" type-id="01" category="01">
<s-1 id="RM0000044GD00CX_01_0001" proc-id="RM23G0E___00006CJ00000">
<ptxt>INSPECT INTAKE MANIFOLD</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Do not adjust the adjusting screw or 2 swirl control valve actuator installation bolts.</ptxt>
<figure>
<graphic graphicname="A152702" width="2.775699831in" height="2.775699831in"/>
</figure>
</atten3>
<figure>
<graphic graphicname="A245673" width="7.106578999in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Disconnect the 2 vacuum hoses from the actuator.</ptxt>
</s2>
<s2>
<ptxt>Check the operation of the swirl control valve.</ptxt>
<s3>
<ptxt>Check that the valve is fully opened under normal conditions.</ptxt>
</s3>
<s3>
<ptxt>Connect the vacuum pumps as shown in the illustration. First apply negative pressure to the diaphragm closer to the intake manifold. This cause the actuator rod to move, which causes the swirl control valve to move. Then, apply negative pressure to the diaphragm on the outer side. This cause the actuator rod to move further, which also causes the swirl control valve to move further. Check that the valve is fully closed when a negative pressure of 65 kPa (488 mmHg, 19.2 in.Hg) is applied.</ptxt>
</s3>
<s3>
<ptxt>Wait for 1 minute and check that the vacuum pump needle does not move. If the result is not as specified, replace the intake manifold.</ptxt>
<atten4>
<ptxt>When applying negative pressure to the diaphragm on the outer side, the diaphragm closer to the intake manifold must be in a vacuum state.</ptxt>
</atten4>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044GD00CX_01_0002" proc-id="RM23G0E___00006CK00000">
<ptxt>INSPECT VACUUM SWITCHING VALVE (for Swirl Control Valve)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A244980E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>33 to 39 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the vacuum control valve set.</ptxt>
</s2>
<s2>
<ptxt>Check the operation of the VSV.</ptxt>
<s3>
<ptxt>Check that air flows from port E to the filter.</ptxt>
<figure>
<graphic graphicname="A244981E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Port E</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Filter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the vacuum control valve set.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Apply battery voltage across the terminals.</ptxt>
<figure>
<graphic graphicname="A244982E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Port E</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Port F</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check that air flows from port E to port F.</ptxt>
<ptxt>If the result is not as specified, replace the vacuum control valve set.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>