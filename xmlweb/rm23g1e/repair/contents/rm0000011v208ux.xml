<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S0029" variety="S0029">
<name>WINDOW / GLASS</name>
<ttl id="12064_S0029_7B9FM_T00PA" variety="T00PA">
<name>POWER WINDOW CONTROL SYSTEM</name>
<para id="RM0000011V208UX" category="D" type-id="303FL" name-id="WS003M-198" from="201210">
<name>INITIALIZATION</name>
<subpara id="RM0000011V208UX_z0" proc-id="RM23G0E___0000IA000001">
<content5 releasenbr="1">
<step1>
<ptxt>INITIALIZE POWER WINDOW CONTROL SYSTEM (POWER WINDOW REGULATOR MOTOR [ALL DOORS])</ptxt>
<atten2>
<ptxt>When a power window regulator motor assembly is reinstalled or replaced, the power window control system must be initialized. Functions such as auto up/down, jam protection, remote control and key-off operation do not operate if the initialization is not performed.</ptxt>
</atten2>
<atten4>
<ptxt>When the battery is replaced, it is not necessary to initialize the power window control system.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When a power window regulator motor assembly is replaced, DTC B2313 is output. Clear the DTC after the initialization.</ptxt>
</item>
<item>
<ptxt>When performing initialization, do not perform any other procedures.</ptxt>
</item>
<item>
<ptxt>When performing initialization, use the power window switch of each door to initialize each power window regulator motor.</ptxt>
</item>
<item>
<ptxt>After any door glass or a door glass run has been replaced, the jam protection function may operate unexpectedly when the auto up function is used, due to detection of a value different from the operation learned value of the door glass movement speed. In such cases, the auto up function can be reinitialized by repeating the following operation at least 5 times:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Open the power window by fully pushing down the multiplex network master switch assembly or power window regulator switch assembly.</ptxt>
</item>
<item>
<ptxt>Close the power window by fully pulling up the multiplex network master switch assembly or power window regulator switch assembly and holding it at the auto up position.</ptxt>
</item>
</list2>
<item>
<ptxt>If the initialization is not completed properly, the LIN communication system may have a malfunction (See page <xref label="Seep01" href="RM000001RSO08AX"/>).</ptxt>
</item>
</list1>
</atten3>
<step2>
<ptxt>Initialization procedure when replacing a power window regulator motor assembly with a new one:</ptxt>
<step3>
<ptxt>Connect the battery and turn the ignition switch to ON (at this time, the LED on the multiplex network master switch assembly or power window regulator switch assembly blinks to indicate that it is ready for initialization).</ptxt>
</step3>
<step3>
<ptxt>Fully open the window by fully pushing down the multiplex network master switch assembly or power window regulator switch assembly and hold the switch for 1 second or more after the window is fully opened.</ptxt>
</step3>
<step3>
<ptxt>Fully close the power window by fully pulling up the multiplex network master switch assembly or power window regulator switch assembly and hold the switch for 1 second or more after the window is fully closed to reset the glass position. The LED on the multiplex network master switch assembly or power window regulator switch assembly stops blinking and illuminates to indicate that the initialization is complete.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Initialization procedure when removing/installing a power window regulator motor assembly:</ptxt>
<step3>
<ptxt>Connect the battery and turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Fully close the power window by fully pulling up the multiplex network master switch assembly or power window regulator switch assembly and hold the switch for 6 seconds or more after the window is fully closed (if the power window does not move or stops halfway even when the switch is fully pulled, release the switch and fully pull it again).</ptxt>
</step3>
<step3>
<ptxt>Fully open the window by pushing down the multiplex network master switch assembly or power window regulator switch assembly and hold the switch for 1 second or more after the window is fully opened.</ptxt>
</step3>
<step3>
<ptxt>Release the multiplex network master switch assembly or power window regulator switch assembly. Then fully push down and hold the switch for 4 seconds or more.</ptxt>
</step3>
<step3>
<ptxt>Fully close the power window by fully pulling up the multiplex network master switch assembly or power window regulator switch assembly and hold the switch for 1 second or more after the window is fully closed to reset the glass position and complete the initialization.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Initialization procedure when the power window does not fully open:</ptxt>
<step3>
<ptxt>Connect the battery and turn the ignition switch to ON.</ptxt>
</step3>
<step3>
<ptxt>Fully close the power window by fully pulling up the multiplex network master switch assembly or power window regulator switch assembly and hold the switch for 6 seconds or more after the window is fully closed (if the power window does not move or stops halfway even when the switch is fully pulled, release the switch and fully pull it again).</ptxt>
</step3>
<step3>
<ptxt>Fully open the window by pushing down the multiplex network master switch assembly or power window regulator switch assembly and hold the switch for 1 second or more after the window is fully opened.</ptxt>
</step3>
<step3>
<ptxt>Release the multiplex network master switch assembly or power window regulator switch assembly. Then fully push down and hold the switch for 4 seconds or more.</ptxt>
</step3>
<step3>
<ptxt>Fully close the power window by fully pulling up the multiplex network master switch assembly or power window regulator switch assembly and hold the switch for 1 second or more after the window is fully closed to reset the glass position and complete the initialization.</ptxt>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>