<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000000PDG0S3X" category="T" type-id="3001H" name-id="ES00D7-853" from="201210">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM000000PDG0S3X_z0" proc-id="RM23G0E___00000HX00001">
<content5 releasenbr="35">
<atten4>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>SFI System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="7" colsep="1" valign="middle" align="left">
<ptxt>Engine does not crank (Does not start)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Battery</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001AR909RX_01_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Starter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000015XW01IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Starter relay</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001FFP03NX_01_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Park/neutral position switch</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002BKX033X_01_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Clutch start switch</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM0000032S3007X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Entry and start system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000YEF0HBX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Immobiliser system (w/ Entry and Start System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000000QY40BMX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Immobiliser system (w/o Entry and Start System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001TC902VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="6" colsep="1" valign="middle" align="left">
<ptxt>No initial combustion (Does not start)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>ECM power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001DN8092X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Crankshaft position sensor</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000028AP00YX_01_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ignition system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000PDH0L1X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Starter signal circuit</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000000TI4076X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>VC output circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001D6V0PCX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Engine cranks normally but difficult to start</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Compression</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM0000017L8019X_01_0009" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Starter signal circuit</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000000TI4076X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001DN8092X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Difficult to start with cold engine</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Ignition system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Spark plug</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0002" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector assembly</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WQQ091X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Difficult to start with warm engine</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector assembly</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WQQ091X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ignition system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Spark plug</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0002" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="3" colsep="1" valign="middle" align="left">
<ptxt>High engine idle speed</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Electronic throttle control system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PQO03DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>A/C signal circuit (for automatic air conditioning system)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002LIM05CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>A/C signal circuit (for manual air conditioning system)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000002LIM05DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001DN8092X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="7" colsep="1" valign="middle" align="left">
<ptxt>Low engine idle speed (Poor idling)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Electronic throttle control system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PQO03DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>A/C signal circuit (for automatic air conditioning system)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002LIM05CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>A/C signal circuit (for manual air conditioning system)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000002LIM05DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Air induction system (w/ Secondary Air Injection System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000292O010X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Air induction system (w/o Secondary Air Injection System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM00000292O011X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>PCV hose (w/ Secondary Air Injection System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000292O010X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>PCV hose (w/o Secondary Air Injection System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM00000292O011X_01_0010" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="10" colsep="1" valign="middle" align="left">
<ptxt>Rough idling</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000017L8019X_01_0009" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Spark plug</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0002" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector assembly</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WQQ091X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ignition system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Electronic throttle control system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PQO03DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1">
<ptxt>Air induction system (w/ Secondary Air Injection System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000292O010X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Air induction system (w/o Secondary Air Injection System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM00000292O011X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1">
<ptxt>PCV hose (w/ Secondary Air Injection System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000292O010X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>PCV hose (w/o Secondary Air Injection System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM00000292O011X_01_0010" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Mass air flow meter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PPR020X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="3" colsep="1" valign="middle" align="left">
<ptxt>Idle hunting</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Electronic throttle control system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PQO03DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1">
<ptxt>Air induction system (w/ Secondary Air Injection System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000292O010X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Air induction system (w/o Secondary Air Injection System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM00000292O011X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM power source circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000001DN8092X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="9" colsep="1" valign="middle" align="left">
<ptxt>Hesitation/poor acceleration</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Spark plug</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0002" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ignition system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector assembly</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WQQ091X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Mass air flow meter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PPR020X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Electronic throttle control system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PQO03DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1">
<ptxt>Air induction system (w/ Secondary Air Injection System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000292O010X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Air induction system (w/o Secondary Air Injection System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM00000292O011X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000017L8019X_01_0009" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Brake override system</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000004G8F0IVX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="6" colsep="1" valign="middle" align="left">
<ptxt>Surging (Poor driveability)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Spark plug</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0002" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ignition system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector assembly</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WQQ091X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Mass air flow meter</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PPR020X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Variable valve timing system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WBZ0MJX_09_0017" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000017L8019X_01_0009" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="10" colsep="1" valign="middle" align="left">
<ptxt>Engine stalls soon after starting</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel pump control circuit</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000030MH057X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Spark plug</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0002" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Ignition system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000SM605MX_02_0001" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel injector assembly</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WQQ091X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Variable valve timing system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000WBZ0MJX_09_0017" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Electronic throttle control system</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002PQO03DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1">
<ptxt>Air induction system (w/ Secondary Air Injection System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000292O010X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Air induction system (w/o Secondary Air Injection System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM00000292O011X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1">
<ptxt>PCV hose (w/ Secondary Air Injection System)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM00000292O010X_01_0003" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>PCV hose (w/o Secondary Air Injection System)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM00000292O011X_01_0010" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>Compression</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM0000017L8019X_01_0009" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle" align="left">
<ptxt>Engine stalls only during A/C operation</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>A/C signal circuit (for automatic air conditioning system)</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000002LIM05CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>A/C signal circuit (for manual air conditioning system)</ptxt>
</entry>
<entry colsep="1">
<ptxt>
<xref href="RM000002LIM05DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>
<xref href="RM000000PDH0L1X" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>