<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AN_T00KB" variety="T00KB">
<name>CAN COMMUNICATION SYSTEM (for LHD without Entry and Start System)</name>
<para id="RM000001RSZ03YX" category="J" type-id="303TO" name-id="NW2GS-02" from="201207" to="201210">
<dtccode/>
<dtcname>ECM Communication Stop Mode</dtcname>
<subpara id="RM000001RSZ03YX_03" type-id="60" category="03" proc-id="RM23G0E___0000DRT00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECM Communication Stop Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>"ECM (Engine)" is not displayed on "Bus Check".</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>"ECM Communication Stop Mode" in "DTC Combination Table" applies.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Power source or inside ECM</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>For vehicles with a 2TR-FE engine only.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000001RSZ03YX_04" type-id="32" category="03" proc-id="RM23G0E___0000DRU00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C218090E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001RSZ03YX_01" type-id="51" category="05" proc-id="RM23G0E___0000DRS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001RSZ03YX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001RSZ03YX_05_0002" proc-id="RM23G0E___0000DRV00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G59, G60 and C64 ECM connectors.</ptxt>
<figure>
<graphic graphicname="C217328E06" width="7.106578999in" height="1.771723296in"/>
</figure>
</test1>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="center" colwidth="0.71in"/>
<colspec colname="COL4" align="center" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C64-5 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C64-11 (E01) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C64-12 (E02) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C64-13 (ME01) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G59-11 (EC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G59-17 (BATT) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G59-23 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery positive (+) voltage applied to terminal G60-12 (MREL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001RSZ03YX_05_0005" fin="true">OK</down>
<right ref="RM000001RSZ03YX_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001RSZ03YX_05_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001RSZ03YX_05_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>