<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8YW_T008K" variety="T008K">
<name>FUEL SUB TANK</name>
<para id="RM0000045BS004X" category="A" type-id="80001" name-id="FU8CR-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM0000045BS004X_01" type-id="01" category="01">
<s-1 id="RM0000045BS004X_01_0001" proc-id="RM23G0E___00005O300000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000045BS004X_01_0002" proc-id="RM23G0E___00005O400000">
<ptxt>REMOVE NO. 2 FUEL TANK PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and No. 2 fuel tank protector.</ptxt>
<figure>
<graphic graphicname="A220841" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BS004X_01_0003" proc-id="RM23G0E___00005O500000">
<ptxt>DISCONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel evaporation hose, fuel breather hose and 3 fuel hoses.</ptxt>
<figure>
<graphic graphicname="A220849" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BS004X_01_0004" proc-id="RM23G0E___00005O600000">
<ptxt>DISCONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel tank to filler pipe hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A220844" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BS004X_01_0005" proc-id="RM23G0E___00005O700000">
<ptxt>DISCONNECT FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel tank breather hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A223265" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BS004X_01_0006" proc-id="RM23G0E___00005O800000">
<ptxt>REMOVE FUEL SUB TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel sender gauge connector.</ptxt>
<figure>
<graphic graphicname="A220842" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Set a transmission jack underneath the fuel sub tank.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts and disconnect the 2 fuel tank bands.</ptxt>
<figure>
<graphic graphicname="A220845" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the fuel sub tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BS004X_01_0007" proc-id="RM23G0E___00005O900000">
<ptxt>REMOVE FUEL TANK VENT TUBE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="A220851" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 8 screws and fuel tank vent tube.</ptxt>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel tank vent tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BS004X_01_0008" proc-id="RM23G0E___00005OA00000">
<ptxt>REMOVE FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel tank to filler pipe hose from the fuel sub tank.</ptxt>
<figure>
<graphic graphicname="A220847" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BS004X_01_0009" proc-id="RM23G0E___00005OB00000">
<ptxt>REMOVE FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel tank breather hose from the fuel sub tank.</ptxt>
<figure>
<graphic graphicname="A223266" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>