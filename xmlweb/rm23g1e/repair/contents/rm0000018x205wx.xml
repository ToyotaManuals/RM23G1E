<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM0000018X205WX" category="C" type-id="3037I" name-id="ES8ZX-63" from="201207" to="201210">
<dtccode>P2226</dtccode>
<dtcname>Barometric Pressure Circuit</dtcname>
<dtccode>P2228</dtccode>
<dtcname>Barometric Pressure Circuit Low Input</dtcname>
<dtccode>P2229</dtccode>
<dtcname>Barometric Pressure Circuit High Input</dtcname>
<subpara id="RM0000018X205WX_01" type-id="60" category="03" proc-id="RM23G0E___00002E800000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>To detect atmospheric pressure, the built-in atmospheric pressure sensor in the ECM is used. Using the atmospheric pressure, the ECM corrects the injection volume, timing and duration, and adjusts the common rail internal fuel pressure in order to optimize engine combustion.</ptxt>
<table pgwide="1">
<title>P2226</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>Atmospheric pressure sensor voltage is 1.4 V or less, or 4.0 V or higher for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2228</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>Atmospheric pressure sensor voltage is 1.4 V or less for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2229</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>Atmospheric pressure sensor voltage is 4.0 V or higher for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P2226</ptxt>
</entry>
<entry morerows="2" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Atmosphere Pressure</ptxt>
</item>
<item>
<ptxt>MAP</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry>
<ptxt>P2228</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>P2229</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the engine switch is on (IG), the intake manifold pressure, "MAP", is approximately equal to atmospheric pressure.</ptxt>
</item>
<item>
<ptxt>Under standard atmospheric pressure conditions, the output of the atmospheric pressure sensor is 101 kPa. For every 100 m increase in elevation, pressure drops by 1 kPa. This varies by weather (high atmospheric pressure, low atmospheric pressure).</ptxt>
</item>
<item>
<ptxt>If DTC P2226, P2228 and/or P2229 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018X205WX_02" type-id="51" category="05" proc-id="RM23G0E___00002E900000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK062X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See Page <xref label="Seep04" href="RM0000012XK062X"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000018X205WX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000018X205WX_03_0001" proc-id="RM23G0E___00002EA00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018X205WX_03_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018X205WX_03_0004" proc-id="RM23G0E___00002EB00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z7X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and wait for 1 second or more.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000018X205WX_03_0005" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000018X205WX_03_0005">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>