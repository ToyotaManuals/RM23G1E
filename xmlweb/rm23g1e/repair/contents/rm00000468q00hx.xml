<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9D0_T00MO" variety="T00MO">
<name>REAR NO. 1 SEAT ASSEMBLY (for 60/40 Split Double-folding Seat Type LH Side)</name>
<para id="RM00000468Q00HX" category="A" type-id="80002" name-id="SE77Y-02" from="201210">
<name>DISASSEMBLY</name>
<subpara id="RM00000468Q00HX_02" type-id="11" category="10" proc-id="RM23G0E___0000GFR00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
</content3>
</subpara>
<subpara id="RM00000468Q00HX_01" type-id="01" category="01">
<s-1 id="RM00000468Q00HX_01_0001" proc-id="RM23G0E___0000GF600001">
<ptxt>REMOVE REAR SEAT CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237987" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws and 2 cushions.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0002" proc-id="RM23G0E___0000GF700001">
<ptxt>REMOVE REAR SEATBACK PROTECTOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237986" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 hooks and remove the protector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0003" proc-id="RM23G0E___0000GF800001">
<ptxt>REMOVE REAR SEAT CUSHION UNDER COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237988" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the hooks and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0004" proc-id="RM23G0E___0000GF900001">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237990" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Disconnect the seat cushion band from the seat cushion cover.</ptxt>
</s2>
<s2>
<ptxt>Detach the hooks and remove the seat cushion cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0005" proc-id="RM23G0E___0000GFA00001">
<ptxt>REMOVE NO. 1 SEAT CUSHION COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237998E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins and remove the protector from the cushion pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0006" proc-id="RM23G0E___0000GFB00001">
<ptxt>REMOVE SEPARATE TYPE REAR SEAT CUSHION COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237999" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the hog rings and seat cushion cover from the seat cushion pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0007" proc-id="RM23G0E___0000GFC00001">
<ptxt>REMOVE REAR SEAT CUSHION BAND</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237993" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the band as shown in the illustration.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0008" proc-id="RM23G0E___0000GFD00001">
<ptxt>REMOVE REAR SEAT LEG BRACKET COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237994" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 4 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Use the same procedure for both covers.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0009" proc-id="RM23G0E___0000GFE00001">
<ptxt>REMOVE REAR SEAT LEG COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237996" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the claw and guide, and then remove the cover.</ptxt>
<atten4>
<ptxt>Use the same procedure for both covers.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0022" proc-id="RM23G0E___0000GFQ00001">
<ptxt>REMOVE REAR NO. 1 SEAT INNER BELT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241735" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and inner belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468Q00HX_01_0011" proc-id="RM23G0E___0000GFF00001">
<ptxt>REMOVE REAR SEAT INNER RECLINING COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238001" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and guide, and then remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0012" proc-id="RM23G0E___0000GFG00001">
<ptxt>REMOVE REAR SEATBACK LOCK COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238004" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 guides and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0013" proc-id="RM23G0E___0000GFH00001">
<ptxt>REMOVE REAR SEATBACK LOCK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 hog rings.</ptxt>
<figure>
<graphic graphicname="B238006" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<figure>
<graphic graphicname="B238007" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>While moving the release button in the direction of the arrow in the illustration, detach it from the grommet and remove the seatback lock.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0014" proc-id="RM23G0E___0000GFI00001">
<ptxt>REMOVE REAR SEATBACK LOCK RELEASE BUTTON</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238009" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the release button as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Counterclockwise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0015" proc-id="RM23G0E___0000GFJ00001">
<ptxt>REMOVE REAR SEATBACK STOP BUTTON GROMMET</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238011" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 3 claws and remove the grommet.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0016" proc-id="RM23G0E___0000GFK00001">
<ptxt>REMOVE REAR NO. 1 SEAT HEADREST SUPPORT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238003" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the 2 supports.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0017" proc-id="RM23G0E___0000GFL00001">
<ptxt>REMOVE SEATBACK COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238012E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the hog rings and detach the hook, and then remove the seatback cover with pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0018" proc-id="RM23G0E___0000GFM00001">
<ptxt>REMOVE SEPARATE TYPE REAR SEATBACK COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238015" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the hog rings and cover from the seatback pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0019" proc-id="RM23G0E___0000GFN00001">
<ptxt>REMOVE REAR SEATBACK EDGE PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the protector from the seatback frame.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0020" proc-id="RM23G0E___0000GFO00001">
<ptxt>REMOVE REAR NO. 1 SEAT PROTECTOR RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240262" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the claw and remove the protector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468Q00HX_01_0021" proc-id="RM23G0E___0000GFP00001">
<ptxt>REMOVE REAR NO. 1 SEAT PROTECTOR LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238014" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the claw and remove the protector.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>