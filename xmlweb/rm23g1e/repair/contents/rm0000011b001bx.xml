<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12019_S0015" variety="S0015">
<name>R150F MANUAL TRANSMISSION / TRANSAXLE</name>
<ttl id="12019_S0015_7B952_T00EQ" variety="T00EQ">
<name>MANUAL TRANSMISSION ASSEMBLY</name>
<para id="RM0000011B001BX" category="A" type-id="30014" name-id="MT0X8-05" from="201210">
<name>INSTALLATION</name>
<subpara id="RM0000011B001BX_01" type-id="01" category="01">
<s-1 id="RM0000011B001BX_01_0048" proc-id="RM23G0E___00008J500001">
<ptxt>INSTALL TRANSFER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1KD-FTV:</ptxt>
<ptxt>Install the transfer assembly with the 8 bolts and 2 brackets.</ptxt>
<torque>
<torqueitem>
<t-value1>24</t-value1>
<t-value2>245</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Take care not to damage the adaptor rear oil seal with the transfer input gear spline.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>except 1KD-FTV:</ptxt>
<ptxt>Install the transfer assembly with the 8 bolts and bracket.</ptxt>
<torque>
<torqueitem>
<t-value1>24</t-value1>
<t-value2>245</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Take care not to damage the adaptor rear oil seal with the transfer input gear spline.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000011B001BX_01_0001" proc-id="RM23G0E___00008IP00001">
<ptxt>INSTALL MANUAL TRANSMISSION UNIT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Confirm that 2 straight pins are on the manual transmission contact surface of the engine cylinder block before manual transmission installation.</ptxt>
<figure>
<graphic graphicname="C212443" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Align the input shaft with the clutch disc and install the transmission unit to the engine.</ptxt>
</s2>
<s2>
<ptxt>Install the 8 bolts.</ptxt>
<figure>
<graphic graphicname="C215707E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>72</t-value1>
<t-value2>729</t-value2>
<t-value4>52</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>37</t-value1>
<t-value2>379</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0044" proc-id="RM23G0E___00008J200001">
<ptxt>CONNECT TRANSFER AND MANUAL TRANSMISSION BREATHER HOSE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 breather hoses to the bracket.</ptxt>
</s2>
<s2>
<ptxt>Connect the 3 breather hoses to the transfer adapter and transfer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0045" proc-id="RM23G0E___00008J300001">
<ptxt>CONNECT WIRE HARNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 connectors and 4 clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0042" proc-id="RM23G0E___00006GP00000">
<ptxt>INSTALL MANIFOLD STAY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C214941E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the manifold stay with the 3 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>71</t-value1>
<t-value2>724</t-value2>
<t-value4>52</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>44</t-value1>
<t-value2>449</t-value2>
<t-value4>32</t-value4>
</torqueitem>
<subtitle>for bolt C</subtitle>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>360</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000011B001BX_01_0004" proc-id="RM23G0E___00008IQ00001">
<ptxt>INSTALL REAR NO. 1 ENGINE MOUNTING INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C217884E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the mounting insulator and front engine mounting insulator with the 5 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>65</t-value1>
<t-value2>663</t-value2>
<t-value4>48</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>117</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0005" proc-id="RM23G0E___00008IR00001">
<ptxt>INSTALL NO. 3 FRAME CROSSMEMBER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the frame crossmember with the 4 bolts and 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>72</t-value1>
<t-value2>734</t-value2>
<t-value4>53</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the 4 bolts to the mounting insulator.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>306</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0031" proc-id="RM23G0E___00008IW00001">
<ptxt>INSTALL FRONT SUSPENSION MEMBER BRACKET LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the member bracket with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>33</t-value1>
<t-value2>337</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0032" proc-id="RM23G0E___00008IX00001">
<ptxt>INSTALL FRONT SUSPENSION MEMBER BRACKET RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the member bracket with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>33</t-value1>
<t-value2>337</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0035" proc-id="RM23G0E___00008IY00001">
<ptxt>INSTALL STARTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1.4 kW Type:</ptxt>
<ptxt>Install the starter (See page <xref label="Seep02" href="RM00000179J00KX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 2.0 kW Type:</ptxt>
<ptxt>Install the starter (See page <xref label="Seep01" href="RM000003REW00IX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0007" proc-id="RM23G0E___00006YZ00001">
<ptxt>CONNECT CLUTCH RELEASE CYLINDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the clutch release cylinder with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0036" proc-id="RM23G0E___00008IZ00001">
<ptxt>INSTALL FRONT PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front propeller shaft (See page <xref label="Seep01" href="RM00000291O00KX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0029" proc-id="RM23G0E___00008IV00001">
<ptxt>INSTALL PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the propeller shaft (See page <xref label="Seep01" href="RM0000029WN00LX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0037">
<ptxt>ADD MANUAL TRANSMISSION OIL</ptxt>
</s-1>
<s-1 id="RM0000011B001BX_01_0038" proc-id="RM23G0E___00008J000001">
<ptxt>INSTALL TRANSFER CASE LOWER PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the lower protector with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0014" proc-id="RM23G0E___00008IS00001">
<ptxt>INSTALL FLOOR SHIFT SHIFT LEVER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Cover the shift lever cap with a cloth.</ptxt>
</s2>
<s2>
<ptxt>Press down on the shift lever cap and rotate it clockwise to install it.</ptxt>
<atten4>
<ptxt>Apply MP grease to the tip of the shift lever.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0039" proc-id="RM23G0E___00008J100001">
<ptxt>INSTALL NO. 1 SHIFT AND SELECT LEVER BOOT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the shift and select lever boot with the 4 screws.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0047" proc-id="RM23G0E___00008J400000">
<ptxt>INSTALL CONSOLE PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect each connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 8 clips and 2 claws to install the console panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011B001BX_01_0027" proc-id="RM23G0E___00008IU00001">
<ptxt>INSTALL SHIFT LEVER KNOB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the knob to the shift lever.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011B001BX_01_0019" proc-id="RM23G0E___00008IT00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>