<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000000XSN02PX" category="D" type-id="303F2" name-id="ES0U8-56" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000000XSN02PX_z0" proc-id="RM23G0E___00002HK00000">
<content5 releasenbr="1">
<step1>
<ptxt>ENGINE CONTROL SYSTEM</ptxt>
<figure>
<graphic graphicname="A245203E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Supply Pump Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Suction Control Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Pressure Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressure Discharge Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Driver</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>EDU Relay</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*11</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diesel Throttle Body Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*12</ptxt>
</entry>
<entry valign="middle">
<ptxt>Manifold Absolute Pressure Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*13</ptxt>
</entry>
<entry valign="middle">
<ptxt>Electric EGR Control Valve Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*14</ptxt>
</entry>
<entry valign="middle">
<ptxt>Glow Plug Controller</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*15</ptxt>
</entry>
<entry valign="middle">
<ptxt>Glow Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*16</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Coolant Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*17</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor (Turbo)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*18</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intercooler</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*19</ptxt>
</entry>
<entry valign="middle">
<ptxt>Camshaft Position Sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*20</ptxt>
</entry>
<entry valign="middle">
<ptxt>Crankshaft Position Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*21</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mass Air Flow Meter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*22</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor (Built Into Mass Air Flow Meter)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*23</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbocharger Sub-assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*24</ptxt>
</entry>
<entry valign="middle">
<ptxt>DC motor, Nozzle Vane Position Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*25</ptxt>
</entry>
<entry valign="middle">
<ptxt>Differential Pressure Sensor Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*26</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas Temperature Sensor (B1S1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*27</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Exhaust Gas Temperature Sensor (B1S2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*28</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3 Exhaust Gas Temperature Sensor (B1S3)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*29</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*30</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Fuel Addition Injector Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*31</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV for No. 1 EGR Bypass Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*32</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV for No. 2 EGR Bypass Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*33</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR Cooler</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*34</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV for No. 1 Swirl Control Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*35</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV for No. 2 Swirl Control Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*36</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Pressure Sender Gauge Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*37</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Pressure Switching Valve Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*38</ptxt>
</entry>
<entry valign="middle">
<ptxt>2ST OIL PRESS Relay</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*39</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accelerator Pedal Position Sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>DIESEL PARTICULATE FILTER SYSTEM DESCRIPTION</ptxt>
<step2>
<ptxt>Diesel Particulate Filter System comprehensively regulates engine control (consists of a catalytic system and a fuel injection system) that purifies particulate matter (PM) by diesel engines. The catalytic system purifies hydrocarbons (HC) and carbon monoxides (CO), and reduces PM with a catalytic converter with the Diesel Particulate Filter (DPF). The fuel injection system adds fuel into the exhaust port using the exhaust fuel addition injector to produce and maintain a proper catalyst temperature for PM forced regeneration.</ptxt>
<figure>
<graphic graphicname="A245204E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Driver</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Pressure Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressure Discharge Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Supply Pump Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV for No. 1 EGR Bypass Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>VSV for No. 2 EGR Bypass Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR Cooler</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*11</ptxt>
</entry>
<entry valign="middle">
<ptxt>Electric EGR Control Valve Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*12</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diesel Throttle Body Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*13</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbocharger Sub-assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*14</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Fuel Addition Injector Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*15</ptxt>
</entry>
<entry valign="middle">
<ptxt>DPF Catalytic Converter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*16</ptxt>
</entry>
<entry valign="middle">
<ptxt>Differential Pressure Sensor Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*17</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas Temperature Sensor (B1S1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*18</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Exhaust Gas Temperature Sensor (B1S2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*19</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3 Exhaust Gas Temperature Sensor (B1S3)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*20</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*21</ptxt>
</entry>
<entry valign="middle">
<ptxt>CCo Catalyst Converter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*22</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tank</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>DPF components:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.24in"/>
<colspec colname="COL2" colwidth="4.84in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DPF catalytic converter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reduces PM.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CCo catalytic converter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reduces HC and CO.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust Fuel Addition Injector Assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adds fuel into the exhaust port in order to raise catalyst temperature for PM forced regeneration.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust gas temperature sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Used for estimating the DPF catalytic converter temperature and adjusting fuel addition by ECM while PM forced regeneration is performed. Also detects the DPF catalytic converter temperature to prevent the catalytic converter temperature from rising too high.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Differential Pressure Sensor Assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the volume of PM deposits and any incorrect vacuum hose arrangement on the DPF catalytic converter.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air fuel ratio sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Used for controlling the air-fuel ratio. By controlling the air-fuel ratio, combustion control and PM forced regeneration are properly regulated.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Diagnostics Trouble Codes (DTCs) table for DPF:</ptxt>
<atten4>
<ptxt>This table indicates typical DTC combinations for each malfunction occurrence.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DPF catalytic converter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Deteriorated or clogged</ptxt>
</entry>
<entry valign="middle">
<ptxt>P062F, P2002, P200C*, P244A*, P244B*, P244C*, P2463*</ptxt>
</entry>
</row>
<row>
<entry morerows="5" valign="middle">
<ptxt>Exhaust Fuel Addition Injector Assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stuck open</ptxt>
</entry>
<entry valign="middle">
<ptxt>P20CF</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stuck closed</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Low fuel addition volume</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244B*, P244C*, P2463</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Open in exhaust fuel addition injector circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P20CB, P244A*, P244C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Short in exhaust fuel addition injector circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P20CF, P20CB, P244A*, P244C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Open or short in exhaust fuel addition injector circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P200C*, P20CF*, P20CB, P244A*, P244C*</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle">
<ptxt>Exhaust gas temperature sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open in exhaust gas temperature sensor circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0545, P0546, P200C*, P2032, P2033, P2084, P242B, P242C, P242D, P244A*, P244C*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Short in exhaust gas temperature sensor circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0545, P0546, P200C*, P2002*, P2032, P2033, P2084, P242B, P242C, P242D, P244A*, P244C*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust gas temperature sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0545, P0546, P200C*, P2032, P2033, P2084, P242B, P242C, P242D, P244A*, P244C*</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>Differential Pressure Sensor Assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open in differential pressure sensor circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244C* P2454, P2455, P2463*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Short in differential pressure sensor circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244C*, P2454, P2455, P2463*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Differential pressure sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244C*, P2454, P2455, P2463*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Differential pressure sensor clogged</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244C*, P2453, P2463*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Incorrect vacuum hose arrangement of the differential pressure sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244C*, P2453, P2463*</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Air fuel ratio sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in air fuel ratio sensor or heater circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0031, P0032, P2238, P2239, P2252, P2253, P244B*, P2463*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air fuel ratio sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0031, P0032, P2195, P2238, P2239, P2252, P2253, P244B*, P2463*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Exhaust gas leaks</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust gas leaks</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244C*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel leaks</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel leaks in fuel addition injector</ptxt>
</entry>
<entry valign="middle">
<ptxt>P2002*, P200C*, P20CF*, P244A*, P244B*, P244C*, P2463*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel supply pump assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Correct fuel pressure cannot be fed to the exhaust fuel addition injector</ptxt>
</entry>
<entry valign="middle">
<ptxt>P244A*, P244B*, P244C*, P2463*</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*: There may be no DTC output depending on the condition of the malfunction.</ptxt>
</step2>
<step2>
<ptxt>Diagnostics trouble code description for DPF:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.92in"/>
<colspec colname="COL2" colwidth="6.16in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0031</ptxt>
</entry>
<entry>
<ptxt>Open or short in air fuel ratio sensor heater control circuit (Low output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0032</ptxt>
</entry>
<entry>
<ptxt>Open or short in air fuel ratio sensor or heater circuit (High output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0545</ptxt>
</entry>
<entry>
<ptxt>Open or short in exhaust gas temperature sensor circuit (B1S1) (Low output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0546</ptxt>
</entry>
<entry>
<ptxt>Open or short in exhaust gas temperature sensor circuit (B1S1) (High output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2002</ptxt>
</entry>
<entry>
<ptxt>DPF catalytic converter thermal deterioration</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P200C</ptxt>
</entry>
<entry>
<ptxt>DPF catalytic converter abnormally high exhaust gas temperature</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2032</ptxt>
</entry>
<entry>
<ptxt>Open or short in exhaust gas temperature sensor circuit (B1S2) (Low output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2033</ptxt>
</entry>
<entry>
<ptxt>Open or short in exhaust gas temperature sensor circuit (B1S2) (High output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2084</ptxt>
</entry>
<entry>
<ptxt>Exhaust Gas Temperature Sensor Circuit Range/Performance (B1S2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P20CB</ptxt>
</entry>
<entry>
<ptxt>Open in exhaust fuel addition injector circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P20CF</ptxt>
</entry>
<entry>
<ptxt>Exhaust fuel addition injector assembly stuck open</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2195</ptxt>
</entry>
<entry>
<ptxt>Air fuel ratio sensor stuck lean</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2238</ptxt>
</entry>
<entry>
<ptxt>Open or short in air fuel ratio sensor or heater circuit (Low output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2239</ptxt>
</entry>
<entry>
<ptxt>Open or short in air fuel ratio sensor or heater circuit (High output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2252</ptxt>
</entry>
<entry>
<ptxt>Open or short in air fuel ratio sensor or heater circuit (Low output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2253</ptxt>
</entry>
<entry>
<ptxt>Open or short in air fuel ratio sensor or heater circuit (High output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P242B</ptxt>
</entry>
<entry>
<ptxt>Exhaust Gas Temperature Sensor Circuit Range / Performance (B1S3)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P242C</ptxt>
</entry>
<entry>
<ptxt>Open or short in exhaust gas temperature sensor circuit (B1S3) (Low output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P242D</ptxt>
</entry>
<entry>
<ptxt>Open or short in exhaust gas temperature sensor circuit (B1S3) (High output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P244A</ptxt>
</entry>
<entry>
<ptxt>DPF catalytic converter excessive differential pressure (Low input)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P244B</ptxt>
</entry>
<entry>
<ptxt>DPF catalytic converter excessive differential pressure</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P244C</ptxt>
</entry>
<entry>
<ptxt>DPF catalytic converter insufficient temperature increase</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2453</ptxt>
</entry>
<entry>
<ptxt>Differential pressure sensor is clogged or has incorrect vacuum hose arrangement</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2454</ptxt>
</entry>
<entry>
<ptxt>Open or short in differential pressure sensor circuit (Low output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2455</ptxt>
</entry>
<entry>
<ptxt>Open or short in differential pressure sensor circuit (High output)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2463</ptxt>
</entry>
<entry>
<ptxt>DPF catalytic converter soot deposition</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>COMMON RAIL SYSTEM DESCRIPTION</ptxt>
<step2>
<ptxt>Common rail system:</ptxt>
<ptxt>The common rail system uses high-pressure fuel for improved fuel economy. This system also provides robust engine power while suppressing engine vibration and noise. </ptxt>
<ptxt>This system stores fuel in the common rail, which has been pressurized and supplied by the supply pump. By storing fuel at high-pressure, the common rail system can provide fuel at stable fuel injection pressures, regardless of engine speed or engine load. </ptxt>
<ptxt>The ECM, using the injector driver, provides an electric current to the piezo actuator in each injector to regulate the fuel injection timing and volume. The ECM also monitors the internal fuel pressure of the common rail using the fuel pressure sensor. The ECM causes the supply pump to supply the fuel necessary to obtain the target fuel pressure.</ptxt>
<ptxt>In addition, this system uses a piezo actuator inside each injector to open and close the fuel passages. Therefore, both fuel injection time and fuel injection volume can be precisely regulated by the ECM.</ptxt>
<ptxt>The common rail system allows a two stage fuel injection process. In order to soften combustion shock, this system performs "pilot-injection" prior to the main fuel injection. This helps to reduce engine vibration and noise.</ptxt>
<figure>
<graphic graphicname="A268869E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<atten4>
<ptxt>If there is a problem with a fuel return pipe, as bleeding air from the fuel system may not be able to be performed properly in certain instances, such as after replacing a fuel injector, etc., the engine startability may deteriorate.</ptxt>
</atten4>
</step2>
<step2>
<ptxt>Common rail system components:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.58in"/>
<colspec colname="COL2" colwidth="5.50in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Common rail</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stores high-pressure fuel produced by supply pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel supply pump assembly</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Operated by crankshaft via gear</ptxt>
</item>
<item>
<ptxt>Supplies high-pressure fuel to common rail</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Injector assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injects fuel to combustion chamber based on signals from ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Monitors internal fuel pressure of common rail and sends signals to ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pressure discharge valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Based on signals from ECM, opens valve when sudden deceleration occurs, or when engine switch is off to prevent fuel pressure from becoming too high.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Suction control valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Based on signals from ECM, adjusts fuel volume supplied to common rail and regulates internal fuel pressure</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Check valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Keeps pressure that discharges from injector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Diagnostic trouble code (DTC) table for the common rail system:</ptxt>
<atten4>
<ptxt>This table indicates typical DTC combinations for each malfunction occurrence.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle">
<ptxt>Injector assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in injector circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0093*, P0201, P0202, P0203, P0204, P062D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stuck open</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0093</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stuck closed</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0301, P0302, P0303, P0304</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Fuel pressure sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in fuel pressure sensor circuit or pressure sensor output fixed</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0087, P0190, P0191, P0192, P0193</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle">
<ptxt>Pressure discharge valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in pressure discharge valve circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0088*, P0093*, P1229*, P1271, P1272</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stuck open</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0093</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stuck closed</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0088*, P1272</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Suction control valve</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in suction control valve circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0627, P1229, P0088*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stuck open</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0088*, P1229</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Injector driver</ptxt>
</entry>
<entry valign="middle">
<ptxt>Faulty injector driver</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0093*, P0201*, P0202*, P0203*, P0204*, P062D*, P1271*, P1272*</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Common rail system (Fuel system)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel leaks in high-pressure area</ptxt>
</entry>
<entry valign="middle">
<ptxt>P0093</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*: There may be no DTC output depending on the condition of the malfunction.</ptxt>
</step2>
<step2>
<ptxt>Diagnostic trouble code description for the common rail system: </ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.99in"/>
<colspec colname="COL2" colwidth="6.09in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0087</ptxt>
</entry>
<entry>
<ptxt>Fuel pressure sensor output does not change</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0088</ptxt>
</entry>
<entry>
<ptxt>Internal fuel pressure too high (220000 kPa [2243 kgf/cm<sup>2</sup>, 31900 psi] or more)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0093</ptxt>
</entry>
<entry>
<ptxt>Fuel leaks in high-pressure areas</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0190</ptxt>
</entry>
<entry>
<ptxt>Open or short in fuel pressure sensor circuit (output voltage is too low or too high)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0192</ptxt>
</entry>
<entry>
<ptxt>Open or short in fuel pressure sensor circuit (output voltage is too low)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0193</ptxt>
</entry>
<entry>
<ptxt>Open or short in fuel pressure sensor circuit (output voltage is too high)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0201</ptxt>
</entry>
<entry>
<ptxt>Open or short in No. 1 injector circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0202</ptxt>
</entry>
<entry>
<ptxt>Open or short in No. 2 injector circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0203</ptxt>
</entry>
<entry>
<ptxt>Open or short in No. 3 injector circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0204</ptxt>
</entry>
<entry>
<ptxt>Open or short in No. 4 injector circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0301</ptxt>
</entry>
<entry>
<ptxt>Cylinder 1 misfire detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0302</ptxt>
</entry>
<entry>
<ptxt>Cylinder 2 misfire detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0303</ptxt>
</entry>
<entry>
<ptxt>Cylinder 3 misfire detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0304</ptxt>
</entry>
<entry>
<ptxt>Cylinder 4 misfire detected</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0627</ptxt>
</entry>
<entry>
<ptxt>Open or short in suction control valve circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P062D</ptxt>
</entry>
<entry>
<ptxt>Open or short in injector driver or injector circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P1229</ptxt>
</entry>
<entry>
<ptxt>Fuel over-feed</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P1271</ptxt>
</entry>
<entry>
<ptxt>Open or short in pressure discharge valve circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P1272</ptxt>
</entry>
<entry>
<ptxt>Pressure discharge valve stuck close</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>INJECTION CONTROL SYSTEM DESCRIPTION</ptxt>
<ptxt>The ECM controls the fuel injection system through the injector driver, injectors and supply pump.</ptxt>
<ptxt>The ECM determines the injection volume and injection timing based on signals from the accelerator pedal position sensor, crankshaft position sensor and camshaft position sensor. Based on the signals from the ECM, the injector driver controls the injectors. The injector driver also controls the suction control valve installed on the supply pump to help regulate fuel pressure.</ptxt>
<ptxt>The piezo type injector used in the 1KD-FTV engine makes noise when the engine is idling because this injector operates at high speed. Therefore, the injector driver controls the injector to operate at low speed when the engine is idling based on signals from the ECM to achieve noise reduction.</ptxt>
<ptxt>The feed pump is used to pump fuel from the fuel tank to the supply pump.</ptxt>
<figure>
<graphic graphicname="A207642E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</step1>
<step1>
<ptxt>SUPPLY PUMP OPERATION SYSTEM DESCRIPTION</ptxt>
<ptxt>The rotation of the eccentric cam in the supply pump causes the ring cam in the supply pump to push plunger A upward as illustrated below. The spring force pulls plunger B (located opposite to plunger A) upward. As a result, plunger B draws fuel in, and plunger A pumps fuel out at the same time.</ptxt>
<figure>
<graphic graphicname="A208438E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</step1>
<step1>
<ptxt>SUCTION CONTROL VALVE OPERATION SYSTEM DESCRIPTION</ptxt>
<atten4>
<ptxt>The ECM controls the suction control valve operation to regulate the fuel volume that is produced by the supply pump for the common rail. This control is performed to regulate the internal fuel pressure of the common rail to the target injection pressure.</ptxt>
</atten4>
<step2>
<ptxt>Small opening of the suction control valve:</ptxt>
<step3>
<ptxt>When the opening of the suction control valve is small, the volume of supplied fuel is small.</ptxt>
</step3>
<step3>
<ptxt>The suction volume becomes small due to the narrow path despite the plunger stroke being full. The difference between the geometrical volume and suction volume creates a vacuum.</ptxt>
</step3>
<step3>
<ptxt>Pump output will start when the fuel pressure at (A) becomes higher than the common rail pressure (B).</ptxt>
<figure>
<graphic graphicname="A208439E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Large opening of the suction control valve:</ptxt>
<step3>
<ptxt>When the opening of the suction control valve is large, the volume of supplied fuel is large.</ptxt>
</step3>
<step3>
<ptxt>If the plunger stroke is full, the suction volume becomes large because of the wide path.</ptxt>
</step3>
<step3>
<ptxt>Pump output will start when the fuel pressure at (A) becomes higher than the common rail pressure (B).</ptxt>
<figure>
<graphic graphicname="A208440E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>