<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S000W" variety="S000W">
<name>5L-E LUBRICATION</name>
<ttl id="12012_S000W_7B92M_T00CA" variety="T00CA">
<name>ENGINE OIL COOLER</name>
<para id="RM00000146X00CX" category="A" type-id="80001" name-id="LU2AR-03" from="201207">
<name>REMOVAL</name>
<subpara id="RM00000146X00CX_01" type-id="01" category="01">
<s-1 id="RM00000146X00CX_01_0040" proc-id="RM23G0E___00006X000000">
<ptxt>REMOVE GENERATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the generator (See page <xref label="Seep01" href="RM0000013XW03NX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146X00CX_01_0043" proc-id="RM23G0E___00006X100000">
<ptxt>REMOVE EXHAUST MANIFOLD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the exhaust manifold (See page <xref label="Seep01" href="RM000004564002X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146X00CX_01_0030" proc-id="RM23G0E___000014W00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<figure>
<graphic graphicname="A224962E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Drain the coolant by removing the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug and drain the coolant from the engine.</ptxt>
<atten3>
<ptxt>If coolant is not drained from the radiator drain cock plug, cylinder block drain cock plugs and radiator reservoir, clogging in the radiator or coolant leakage from the seal of the water pump may result.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000146X00CX_01_0049" proc-id="RM23G0E___00004YE00000">
<ptxt>DRAIN ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the oil filler cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil drain plug and gasket, and then drain the oil into a container.</ptxt>
</s2>
<s2>
<ptxt>Clean the drain plug and install it with a new gasket.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>352</t-value2>
<t-value4>25</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146X00CX_01_0055" proc-id="RM23G0E___00004ZB00000">
<ptxt>DISCONNECT OIL FILTER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the oil filter.</ptxt>
<sst>
<sstitem>
<s-number>09228-44011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A227791E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000146X00CX_01_0027" proc-id="RM23G0E___000052300000">
<ptxt>DISCONNECT VACUUM PUMP OIL INLET HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the union bolt and gasket, and then disconnect the vacuum pump oil inlet hose from the cylinder block.</ptxt>
<figure>
<graphic graphicname="A118121" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146X00CX_01_0018" proc-id="RM23G0E___000051T00000">
<ptxt>REMOVE OIL FILTER BRACKET SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts, 2 nuts, oil filter bracket and gasket.</ptxt>
<figure>
<graphic graphicname="A118122" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146X00CX_01_0028" proc-id="RM23G0E___00006WZ00000">
<ptxt>REMOVE OIL COOLER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 nuts, oil cooler and 2 gaskets.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>