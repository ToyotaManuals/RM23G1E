<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AM_T00KA" variety="T00KA">
<name>CAN COMMUNICATION SYSTEM (for LHD with Entry and Start System)</name>
<para id="RM000003TN401FX" category="J" type-id="800NY" name-id="NW2JE-02" from="201207" to="201210">
<dtccode/>
<dtcname>Certification ECU Communication Stop Mode</dtcname>
<subpara id="RM000003TN401FX_01" type-id="60" category="03" proc-id="RM23G0E___0000DJU00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Symptoms</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Certification ECU Communication Stop Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>"Certification (Smart)" is not displayed on "Bus Check".</ptxt>
</item>
<item>
<ptxt>"Certification ECU Communication Stop Mode" in "DTC Combination Table" applies.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Power source or inside certification ECU</ptxt>
</item>
<item>
<ptxt>Certification ECU branch wire or connector</ptxt>
</item>
<item>
<ptxt>Certification ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003TN401FX_02" type-id="32" category="03" proc-id="RM23G0E___0000DJV00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C201306E07" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003TN401FX_03" type-id="51" category="05" proc-id="RM23G0E___0000DJW00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Operating the engine switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000003TN401FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003TN401FX_04_0001" proc-id="RM23G0E___0000DJX00000">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the main wire and the branch wire.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000003TN401FX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003TN401FX_04_0002" proc-id="RM23G0E___0000DJY00000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (CERTIFICATION ECU BRANCH WIRE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G38 certification ECU connector.</ptxt>
<figure>
<graphic graphicname="C198479E10" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-9 (CANH) - G38-10 (CANL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>54 to 69 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Certification ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003TN401FX_04_0003" fin="false">OK</down>
<right ref="RM000003TN401FX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003TN401FX_04_0003" proc-id="RM23G0E___0000DJZ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
<figure>
<graphic graphicname="C198479E11" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-15 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-1 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-16 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-17 (CUTB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Certification ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003TN401FX_04_0004" fin="true">OK</down>
<right ref="RM000003TN401FX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003TN401FX_04_0004">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000003TN401FX_04_0005">
<testtitle>REPAIR OR REPLACE CERTIFICATION ECU BRANCH WIRE OR CONNECTOR (CANH, CANL)</testtitle>
</testgrp>
<testgrp id="RM000003TN401FX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>