<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001B" variety="S001B">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001B_7B96L_T00G9" variety="T00G9">
<name>AIR SUSPENSION SYSTEM</name>
<para id="RM000002PE6022X" category="C" type-id="804BU" name-id="SC21O-02" from="201210">
<dtccode>C1715/15</dtccode>
<dtcname>Front Acceleration Sensor RH Malfunction</dtcname>
<dtccode>C1716/16</dtccode>
<dtcname>Front Acceleration Sensor LH Malfunction</dtcname>
<dtccode>C1717/17</dtccode>
<dtcname>Rear Acceleration Sensor Malfunction</dtcname>
<dtccode>C1796/96</dtccode>
<dtcname>(Up &amp; Down) G Sensor FR</dtcname>
<dtccode>C1797/97</dtccode>
<dtcname>(Up &amp; Down) G Sensor FL</dtcname>
<dtccode>C1798/98</dtccode>
<dtcname>(Up &amp; Down) G Sensor Rear</dtcname>
<subpara id="RM000002PE6022X_01" type-id="60" category="03" proc-id="RM23G0E___00009NT00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The acceleration sensor (up and down G sensor) detects the upward and downward acceleration of the vehicle, and outputs it as a voltage to the suspension control ECU. Up and down G sensors are installed in 3 locations: 1) the suspension control ECU, 2) the RH side instrument panel, and 3) behind the deck trim side panel assembly LH. Each up and down G sensor independently detects the upward and downward acceleration. During a test mode inspection, the suspension control ECU reads the fluctuations in each sensor signal. If a sensor signal does not fluctuate, test mode DTCs C1796/96, C1797/97 and C1798/98 are stored.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1715/15</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the ignition switch ON, a voltage of 5.5 V or higher or 4.3 V or less at the power source of the front acceleration sensor RH is detected for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>With the ignition switch ON, a voltage of 4.7 V or higher or 0.3 V or less at the front acceleration sensor RH is detected for 1 second.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Front acceleration sensor RH</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1716/16</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the ignition switch ON, a voltage of 5.5 V or higher or 4.3 V or less at the power source of the front acceleration sensor LH is detected for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>With the ignition switch ON, a voltage of 4.7 V or higher or 0.3 V or less at the front acceleration sensor LH is detected for 1 second.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Suspension control ECU (Front acceleration sensor LH)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1717/17</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the ignition switch ON, a voltage of 5.5 V or higher or 4.3 V or less at the power source of the rear acceleration sensor is detected for 0.5 seconds.</ptxt>
</item>
<item>
<ptxt>With the ignition switch ON, a voltage of 4.7 V or higher or 0.3 V or less at the rear acceleration sensor is detected for 1 second.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Rear acceleration sensor</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1796/96</ptxt>
</entry>
<entry valign="middle">
<ptxt>On a level surface, the G sensor input is +/- 1.96 m/s<sup>2</sup> or more for 1 second or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Front acceleration sensor RH</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1797/97</ptxt>
</entry>
<entry valign="middle">
<ptxt>On a level surface, the G sensor input is +/- 1.96 m/s<sup>2</sup> or more for 1 second or more.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Suspension control ECU (Front acceleration sensor LH)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C1798/98</ptxt>
</entry>
<entry valign="middle">
<ptxt>On a level surface, the G sensor input is +/- 1.96 m/s<sup>2</sup> or more for 1 second or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Rear acceleration sensor</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If DTCs C1716/16 and C1797/97 are output, replace the suspension control ECU (houses the front acceleration sensor LH).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002PE6022X_02" type-id="32" category="03" proc-id="RM23G0E___00009NU00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C179515E05" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002PE6022X_03" type-id="51" category="05" proc-id="RM23G0E___00009NV00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the suspension control ECU, perform registration (See page <xref label="Seep01" href="RM000003X4500LX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002PE6022X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002PE6022X_04_0001" proc-id="RM23G0E___00009NW00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (G SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / Air suspension / Data List.</ptxt>
<table pgwide="1">
<title>Air Suspension</title>
<tgroup cols="4">
<colspec colname="COLSPEC1" colwidth="1.63in"/>
<colspec colname="COL2" colwidth="2.11in"/>
<colspec colname="COL3" colwidth="1.61in"/>
<colspec colname="COL4" colwidth="1.74in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>(Up &amp; Down) G Sensor FR</ptxt>
</entry>
<entry valign="middle">
<ptxt>(Up &amp; Down) G sensor (Front RH)/</ptxt>
<ptxt>Min.: -1045.29 m/s<sup>2</sup>
</ptxt>
<ptxt>Max.: 1045.26 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 +/- 0.98 m/s<sup>2</sup> when stationary</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value changes when the vehicle (Front RH) is bounced.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>(Up &amp; Down) G Sensor FL</ptxt>
</entry>
<entry valign="middle">
<ptxt>(Up &amp; Down) G sensor (Front LH)/</ptxt>
<ptxt>Min.: -1045.29 m/s<sup>2</sup>
</ptxt>
<ptxt>Max.: 1045.26 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 +/- 0.98 m/s<sup>2</sup> when stationary</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value changes when the vehicle (Front LH) is bounced.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>(Up &amp; Down) G Sensor Rear</ptxt>
</entry>
<entry valign="middle">
<ptxt>(Up &amp; Down) G sensor (Rear)/</ptxt>
<ptxt>Min.: -1045.29 m/s<sup>2</sup>
</ptxt>
<ptxt>Max.: 1045.26 m/s<sup>2</sup>
</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 +/- 0.98 m/s<sup>2</sup> when stationary</ptxt>
</entry>
<entry valign="middle">
<ptxt>The value changes when the vehicle (rear) is bounced.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Acceleration value changes.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002PE6022X_04_0008" fin="true">OK</down>
<right ref="RM000002PE6022X_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PE6022X_04_0002" proc-id="RM23G0E___00009NX00001">
<testtitle>INSPECT ACCELERATION SENSOR (FRONT RH OR REAR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C214175E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Front RH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Rear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Acceleration Sensor)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Upside</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Downside</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<test1>
<ptxt>Check the front acceleration sensor RH (when DTC C1715/15 or C1796/96 is output).</ptxt>
<test2>
<ptxt>Remove the front acceleration sensor RH (See page <xref label="Seep01" href="RM000002BGR017X"/>).</ptxt>
</test2>
<test2>
<ptxt>Connect 3 dry cell batteries of 1.5 V in series.</ptxt>
</test2>
<test2>
<ptxt>Connect the positive (+) end of the batteries to terminal 3 (SHB) of the acceleration sensor and the negative (-) end of the batteries to terminal 2 (SHG). Then measure the voltage between terminal 1 (SGFL) and terminal 2 (SHG).</ptxt>
</test2>
<test2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.40in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 (SGFL) - 2 (SHG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sensor in vertical position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 2.0 to 2.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Sensor tilted left or right from vertical position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Changes between approx. 0.9 and 2.3 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>Check the rear acceleration sensor: (when DTC C1717/17 or C1798/98 is output).</ptxt>
<test2>
<ptxt>Remove the rear acceleration sensor (See page <xref label="Seep02" href="RM000002BGR018X"/>).</ptxt>
</test2>
<test2>
<ptxt>Connect 3 dry cell batteries of 1.5 V in series.</ptxt>
</test2>
<test2>
<ptxt>Connect the positive (+) end of the batteries to terminal 3 (SGV) of the acceleration sensor and the negative (-) end of the batteries to terminal 2 (SGND). Then measure the voltage between terminal 1 (SGRR) and terminal 2 (SGND).</ptxt>
</test2>
<test2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.40in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 (SGRR) - 2 (SGND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sensor in vertical position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 2.0 to 2.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Sensor tilted left or right from vertical position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Changes between approx. 0.9 and 2.3 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not apply a voltage of higher than 6 V.</ptxt>
</atten3>
<atten4>
<ptxt>When the acceleration sensor is tilted, it may output a different value.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.88in"/>
<colspec colname="COL2" colwidth="1.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Front acceleration sensor RH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (Rear acceleration sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002PE6022X_04_0003" fin="false">A</down>
<right ref="RM000002PE6022X_04_0010" fin="true">B</right>
<right ref="RM000002PE6022X_04_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002PE6022X_04_0003" proc-id="RM23G0E___00009NY00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ACCELERATION SENSOR - SUSPENSION CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the front acceleration sensor RH (when DTC C1715/15 or C1796/96 is output).</ptxt>
<test2>
<ptxt>Disconnect the A13 front acceleration sensor RH connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the A14 suspension control ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.71in"/>
<colspec colname="COL2" colwidth="1.05in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A13-1 (SGFL) - A14-18 (SGFR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A13-2 (SHG) - A14-10 (SGR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A13-3 (SHB) - A14-9 (SBR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A13-1 (SGFL) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A13-2 (SHG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A13-3 (SHB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
<test1>
<ptxt>Check the rear acceleration sensor: (when DTC C1717/17 or C1798/98 is output).</ptxt>
<test2>
<ptxt>Disconnect the R18 rear acceleration sensor connector.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the A49 suspension control ECU connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.69in"/>
<colspec colname="COL2" colwidth="1.07in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R18-1 (SGRR) - A49-26 (SGRR)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R18-2 (SGND) - A49-16 (SGR3)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R18-3 (SGV) - A49-17 (SBR3)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R18-1 (SGRR) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R18-2 (SGND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R18-3 (SGV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002PE6022X_04_0004" fin="true">OK</down>
<right ref="RM000002PE6022X_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PE6022X_04_0008">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PE6022X_04_0010">
<testtitle>REPLACE FRONT ACCELERATION SENSOR RH<xref label="Seep01" href="RM000002BGR017X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PE6022X_04_0005">
<testtitle>REPLACE REAR ACCELERATION SENSOR<xref label="Seep01" href="RM000002BGR018X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PE6022X_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002PE6022X_04_0004">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000002BGR016X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>