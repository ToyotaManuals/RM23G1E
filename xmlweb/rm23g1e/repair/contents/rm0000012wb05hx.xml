<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM0000012WB05HX" category="D" type-id="3001B" name-id="ES00MO-104" from="201207" to="201210">
<name>HOW TO PROCEED WITH TROUBLESHOOTING</name>
<subpara id="RM0000012WB05HX_z0" proc-id="RM23G0E___00001D400000">
<content5 releasenbr="1">
<atten4>
<ptxt>*: Use the intelligent tester.</ptxt>
</atten4>
<descript-diag>
<descript-testgroup>
<testtitle>VEHICLE BROUGHT TO WORKSHOP</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CUSTOMER PROBLEM ANALYSIS</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONNECT INTELLIGENT TESTER TO DLC3</testtitle>
<atten4>
<ptxt>If the display indicates a communication fault with the tester, inspect the DLC3.</ptxt>
</atten4>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK DTC AND FREEZE FRAME DATA*</testtitle>
<atten4>
<ptxt>Record or print DTCs and freeze frame data if necessary.</ptxt>
</atten4>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CLEAR DTC AND FREEZE FRAME DATA*</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>VISUAL INSPECTION</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRM PROBLEM SYMPTOMS</testtitle>
<atten4>
<ptxt>If the engine does not start, first perform the "Check DTC" and "Basic Inspection" procedures below.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Malfunction occurs</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Malfunction does not occur</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<results>
<result>B</result>
<action-ci-right>SIMULATE SYMPTOMS</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CHECK DTC*</testtitle>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Trouble code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>No trouble code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<results>
<result>B</result>
<action-ci-right>Go to step 9</action-ci-right>
<result-ci-down>A</result-ci-down>
<action-ci-fin>REFER TO DTC CHART</action-ci-fin>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>BASIC INSPECTION</testtitle>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Malfunctioning parts not confirmed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Malfunctioning parts confirmed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<results>
<result>B</result>
<action-ci-right>Go to step 11</action-ci-right>
<result-ci-down>A</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>REFER TO PROBLEM SYMPTOMS TABLE</testtitle>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Refer to Problem Symptoms Table (See page <xref label="Seep02" href="RM0000012WF05RX"/>).</ptxt>
</item>
<item>
<ptxt>Also, refer to the inspection procedure for the problem symptom.</ptxt>
</item>
</list1>
</atten4>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>PARTS INSPECTION</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>IDENTIFY PROBLEM</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>ADJUST AND/OR REPAIR</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
</results>
</descript-testgroup>
<descript-testgroup>
<testtitle>CONFIRMATION TEST</testtitle>
<results>
<result-ci-down>NEXT</result-ci-down>
<action-ci-fin>END</action-ci-fin>
</results>
</descript-testgroup>
</descript-diag>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>