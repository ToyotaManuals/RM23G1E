<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM00000368W05HX" category="C" type-id="803OJ" name-id="BCB3M-02" from="201207" to="201210">
<dtccode>C1417</dtccode>
<dtcname>High Power Supply Voltage Malfunction</dtcname>
<subpara id="RM00000368W05HX_01" type-id="60" category="03" proc-id="RM23G0E___0000A5I00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If a malfunction is detected in the power supply circuit, the skid control ECU (housed in the actuator assembly) stores this DTC and the fail-safe function prohibits brake control system operation.</ptxt>
<ptxt>This DTC is stored when the IG1 terminal voltage deviates from the DTC detection condition due to a malfunction in the power supply or charging circuit, such as the battery or generator circuit, etc.</ptxt>
<ptxt>The DTC is cleared when the IG1 terminal voltage returns to normal.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.92in"/>
<colspec colname="COL2" colwidth="3.09in"/>
<colspec colname="COL3" colwidth="3.07in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1417</ptxt>
</entry>
<entry valign="middle">
<ptxt>The IG1 terminal voltage is 17.4 V or higher for 0.8 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ECU-IG NO. 1 fuse</ptxt>
</item>
<item>
<ptxt>Battery</ptxt>
</item>
<item>
<ptxt>Charging system</ptxt>
</item>
<item>
<ptxt>Power source circuit</ptxt>
</item>
<item>
<ptxt>Internal power supply circuit of the skid control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000368W05HX_02" type-id="32" category="03" proc-id="RM23G0E___0000A5J00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC C1241 (See page <xref label="Seep01" href="RM000000XW60EFX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000368W05HX_03" type-id="51" category="05" proc-id="RM23G0E___0000A5K00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08IX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM00000368W05HX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000368W05HX_05_0011" proc-id="RM23G0E___0000A1500000">
<testtitle>CHECK TERMINAL VOLTAGE (IG1)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C199357E08" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A6-34 (IG1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM00000368W05HX_05_0008" fin="false">OK</down>
<right ref="RM00000368W05HX_05_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000368W05HX_05_0008" proc-id="RM23G0E___0000A5L00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0EZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM000000XHV0EZX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000368W05HX_05_0004" fin="true">A</down>
<right ref="RM00000368W05HX_05_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000368W05HX_05_0009">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W05HX_05_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000368W05HX_05_0004">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01JX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>