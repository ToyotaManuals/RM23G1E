<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM000000PFY0POX" category="C" type-id="302IK" name-id="ESM9J-08" from="201207" to="201210">
<dtccode>P2120</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit</dtcname>
<dtccode>P2122</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit Low Input</dtcname>
<dtccode>P2123</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" Circuit High Input</dtcname>
<dtccode>P2125</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit</dtcname>
<dtccode>P2127</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit Low Input</dtcname>
<dtccode>P2128</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "E" Circuit High Input</dtcname>
<dtccode>P2138</dtccode>
<dtcname>Throttle / Pedal Position Sensor / Switch "D" / "E" Voltage Correlation</dtcname>
<subpara id="RM000000PFY0POX_13" type-id="11" category="10" proc-id="RM23G0E___00003F200000">
<name>CAUTION / NOTICE / HINT</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>These DTCs relate to the accelerator pedal sensor.</ptxt>
</item>
<item>
<ptxt>This electronic throttle control system does not use a throttle cable.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFY0POX_01" type-id="60" category="03" proc-id="RM23G0E___00003ET00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The accelerator pedal sensor is mounted on the accelerator pedal bracket and has 2 sensor circuits: VPA (main) and VPA2 (sub). This sensor is a non-contact type. It uses Hall-effect elements in order to yield accurate signals, even in extreme driving conditions, such as at high speeds as well as very low speeds. The voltage, which is applied to terminals VPA and VPA2 of the ECM, varies between 0 V and 5 V in proportion to the operating angle of the accelerator pedal (throttle valve). A signal from VPA indicates the actual accelerator pedal position (throttle valve opening angle) and is used for engine control. A signal from VPA2 conveys the status of the VPA circuit and is used to check the accelerator pedal sensor itself.</ptxt>
<ptxt>The ECM monitors the actual accelerator pedal position (throttle valve opening angle) through the signals from VPA and VPA2, and controls the throttle actuator according to these signals.</ptxt>
<figure>
<graphic graphicname="A112620E53" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2120</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA fluctuates rapidly beyond the upper and lower malfunction thresholds for 0.5 seconds or more (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal sensor assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2122</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA is 0.4 V or less for 0.5 seconds or more when the accelerator pedal is fully released (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal sensor assembly</ptxt>
</item>
<item>
<ptxt>Open in VCPA circuit</ptxt>
</item>
<item>
<ptxt>Open or ground short in VPA circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2123</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA is 4.8 V or higher for 2.0 seconds or more (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal sensor assembly</ptxt>
</item>
<item>
<ptxt>Open in EPA circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2125</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA2 fluctuates rapidly beyond the upper and lower malfunction thresholds for 0.5 seconds or more (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal sensor assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2127</ptxt>
</entry>
<entry valign="middle">
<ptxt>VPA2 is 1.2 V or less for 0.5 seconds or more when the accelerator pedal is fully released (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal sensor assembly</ptxt>
</item>
<item>
<ptxt>Open in VCP2 circuit</ptxt>
</item>
<item>
<ptxt>Open or ground short in VPA2 circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2128</ptxt>
</entry>
<entry valign="middle">
<ptxt>Conditions (a) and (b) are met for 2.0 seconds or more (1-trip detection logic):</ptxt>
<ptxt>(a) VPA2 is 4.8 V or higher.</ptxt>
<ptxt>(b) VPA is between 0.4 V and 3.45 V.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal sensor assembly</ptxt>
</item>
<item>
<ptxt>Open in EPA2 circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2138</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition (a) or (b) is met for 2.0 seconds or more (1-trip detection logic):</ptxt>
<ptxt>(a) The difference between VPA and VPA2 is 0.02 V or less.</ptxt>
<ptxt>(b) VPA is 0.4 V or less and VPA2 is 1.2 V or less.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short between VPA and VPA2 circuits</ptxt>
</item>
<item>
<ptxt>Accelerator pedal sensor assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When any of these DTCs are output, check the accelerator pedal position sensor voltage using the intelligent tester. Enter the following menus: Powertrain / Engine and ECT / Data List / Accel Sensor Out No.1 and Accel Sensor Out No.2.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="2.27in"/>
<colspec colname="COL2" colwidth="1.20in"/>
<colspec colname="COL3" colwidth="1.20in"/>
<colspec colname="COL4" colwidth="1.20in"/>
<colspec colname="COL5" colwidth="1.21in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Sensor Out No.1</ptxt>
<ptxt>When Accelerator Pedal Released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Sensor Out No.2</ptxt>
<ptxt>When Accelerator Pedal Released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Sensor Out No.1</ptxt>
<ptxt>When Accelerator Pedal Depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accel Sensor Out No.2</ptxt>
<ptxt>When Accelerator Pedal Depressed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>VCPA or VCP2 circuit open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Open or ground short in VPA circuit</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.2 to 2.0 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.4 to 4.75 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Open or ground short in VPA2 circuit</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.6 to 4.5 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0 to 0.2 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>EPA or EPA2 circuit open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.0 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Normal condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.5 to 1.1 V	</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.2 to 2.0 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.6 to 4.5 V</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.4 to 4.75 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
<atten4>
<ptxt>Accelerator pedal positions are expressed as voltages.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFY0POX_11" type-id="62" category="03" proc-id="RM23G0E___00003F100000">
<name>FAIL-SAFE</name>
<content5 releasenbr="1">
<ptxt>When DTC P2120, P2122, P2123, P2125, P2127, P2128 or P2138 is stored, the ECM enters fail-safe mode. If either of the 2 sensor circuits malfunctions, the ECM uses the remaining circuit to calculate the accelerator pedal position to allow the vehicle to continue to be driven. If both of the circuits malfunction, the ECM regards the accelerator pedal as being released. As a result, the throttle valve is closed and the engine idles. The ECM continues operating in fail-safe mode until a pass condition is detected and the ignition switch is turned off.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFY0POX_08" type-id="32" category="03" proc-id="RM23G0E___00003EU00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A162855E11" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000PFY0POX_09" type-id="51" category="05" proc-id="RM23G0E___00003EV00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFY0POX_10" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PFY0POX_10_0001" proc-id="RM23G0E___00003EW00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ACCEL SENSOR OUT NO.1 AND ACCEL SENSOR OUT NO.2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
<figure>
<graphic graphicname="A208621E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Accel Sensor Out No.1 and Accel Sensor Out No.2.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Accelerator Pedal Operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No.1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Accel Sensor Out No.2</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Released</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.5 to 1.1 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.2 to 2.0 V</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.6 to 4.5 V</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.4 to 4.75 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PFY0POX_10_0011" fin="true">OK</down>
<right ref="RM000000PFY0POX_10_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFY0POX_10_0010" proc-id="RM23G0E___00003F000000">
<testtitle>CHECK HARNESS AND CONNECTOR (ACCELERATOR PEDAL SENSOR ASSEMBLY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the accelerator pedal sensor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A19-6 (VPA) - G60-6 (VPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-5 (EPA) - G60-2 (EPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-4 (VCPA) - G60-4 (VCPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-3 (VPA2) - G60-5 (VPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-2 (EPA2) - G60-1 (EPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-1 (VCP2) - G60-3 (VCP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A19-6 (VPA) or G60-6 (VPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-5 (EPA) or G60-2 (EPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-4 (VCPA) or - G60-4 (VCPA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-3 (VPA2) or G60-5 (VPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-2 (EPA2) or G60-1 (EPA2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-1 (VCP2) or G60-3 (VCP2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the accelerator pedal sensor assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFY0POX_10_0003" fin="false">OK</down>
<right ref="RM000000PFY0POX_10_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFY0POX_10_0003" proc-id="RM23G0E___00003EY00000">
<testtitle>INSPECT ECM (VCPA AND VCP2 VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the accelerator pedal sensor assembly connector.</ptxt>
<figure>
<graphic graphicname="A200914E10" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A19-4 (VCPA) - A19-5 (EPA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A19-1 (VCP2) - A19-2 (EPA2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Accelerator Pedal Sensor Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the accelerator pedal sensor assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFY0POX_10_0002" fin="false">OK</down>
<right ref="RM000000PFY0POX_10_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFY0POX_10_0002" proc-id="RM23G0E___00003EX00000">
<testtitle>REPLACE ACCELERATOR PEDAL SENSOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the accelerator pedal sensor assembly (See page <xref label="Seep01" href="RM00000111V007X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFY0POX_10_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PFY0POX_10_0005" proc-id="RM23G0E___00003EZ00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (ACCELERATOR PEDAL SENSOR DTCS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the Ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z8X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for 15 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC P2120, P2122, P2123, P2125, P2127, P2128, and/or P2138 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PFY0POX_10_0007" fin="true">A</down>
<right ref="RM000000PFY0POX_10_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFY0POX_10_0011">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0VKX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFY0POX_10_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (ACCELERATOR PEDAL SENSOR ASSEMBLY - ECM)</testtitle>
</testgrp>
<testgrp id="RM000000PFY0POX_10_0012">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000PFY0POX_10_0007">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>