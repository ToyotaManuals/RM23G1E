<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E8_T00NW" variety="T00NW">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000003UXN04DX" category="C" type-id="80401" name-id="AC96N-05" from="201210">
<dtccode>U0100</dtccode>
<dtcname>Lost Communication with ECM</dtcname>
<dtccode>U0142</dtccode>
<dtcname>Lost Communication with Main Body ECU</dtcname>
<dtccode>U0155</dtccode>
<dtcname>Lost Communication with Combination Meter</dtcname>
<subpara id="RM000003UXN04DX_01" type-id="60" category="03" proc-id="RM23G0E___0000HEY00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The air conditioning amplifier communicates with the ECM, main body ECU (multiplex network body ECU) and combination meter through the CAN communication system.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>U0100</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Lost communication with the ECM.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U0142</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Lost communication with the main body ECU.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>Main body ECU (multiplex network body ECU)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>U0155</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Lost communication with the combination meter.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>CAN communication system</ptxt>
</item>
<item>
<ptxt>Combination meter assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003UXN04DX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003UXN04DX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003UXN04DX_03_0001" proc-id="RM23G0E___0000HEZ00001">
<testtitle>CHECK FOR DTC (CAN COMMUNICATION SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the intelligent tester to check if the CAN communication system is functioning normally.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>CAN DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for LHD with Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for LHD without Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for RHD with Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN DTC is output (for RHD without Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003UXN04DX_03_0003" fin="true">A</down>
<right ref="RM000003UXN04DX_03_0002" fin="true">B</right>
<right ref="RM000003UXN04DX_03_0004" fin="true">C</right>
<right ref="RM000003UXN04DX_03_0005" fin="true">D</right>
<right ref="RM000003UXN04DX_03_0006" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000003UXN04DX_03_0002">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO086X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003UXN04DX_03_0003">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003UXN04DX_03_0004">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO087X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003UXN04DX_03_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO088X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003UXN04DX_03_0006">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000001RSO089X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>