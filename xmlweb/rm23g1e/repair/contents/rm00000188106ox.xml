<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000188106OX" category="C" type-id="803ED" name-id="ESMG3-07" from="201210">
<dtccode>P1496</dtccode>
<dtcname>Intake Air Temperature Sensor 1 Circuit Low</dtcname>
<dtccode>P1497</dtccode>
<dtcname>Intake Air Temperature Sensor 1 Circuit High</dtcname>
<subpara id="RM00000188106OX_01" type-id="60" category="03" proc-id="RM23G0E___000020N00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A208462E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>The Intake Air Temperature (IAT) sensor detects the IAT after the intake air passes through the intercooler. A built-in thermistor in the sensor changes its resistance value according to the intake air temperature. The lower the intake air temperature, the higher the thermistor resistance value. The higher the intake air temperature, the lower the thermistor resistance value (See Fig. 1).</ptxt>
<ptxt>The sensor is connected to the ECM. The 5 V power source voltage in the ECM is applied to the sensor from terminal THIA via resistor R. Resistor R and the sensor are connected in series. When the resistance value of the sensor changes in accordance with changes in the IAT, the potential at terminal THIA also changes. Based on this signal, the ECM corrects the fuel injection volume to improve driveability with a cold engine.</ptxt>
<table pgwide="1">
<title>P1496</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>Short in the intake air temperature sensor (turbo) circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in intake air temperature sensor (turbo) circuit</ptxt>
</item>
<item>
<ptxt>Intake air temperature sensor (turbo)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P1497</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON for 1 second</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open in the intake air temperature sensor (turbo) circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in intake air temperature sensor (turbo) circuit</ptxt>
</item>
<item>
<ptxt>Intake air temperature sensor (turbo)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P1496</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Intake Air Temp (Turbo)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P1497</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="nonmark">
<item>
<ptxt>If DTC P1496 or P1497 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
<item>
<ptxt>Lack of power</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188106OX_02" type-id="32" category="03" proc-id="RM23G0E___000020O00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A210074E16" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000188106OX_03" type-id="51" category="05" proc-id="RM23G0E___000020P00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07OX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188106OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000188106OX_04_0001" proc-id="RM23G0E___000020Q00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INTAKE AIR TEMPERATURE [TURBO])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Intake Air Temp (Turbo).</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Same as air temperature near intake manifold</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (same as air temperature near intake manifold)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If there is an open circuit, the tester indicates -40°C (-40°F).</ptxt>
</item>
<item>
<ptxt>If there is a short circuit, the tester indicates 140°C (284°F) or higher.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000188106OX_04_0002" fin="false">A</down>
<right ref="RM00000188106OX_04_0004" fin="false">B</right>
<right ref="RM00000188106OX_04_0013" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0002" proc-id="RM23G0E___000020R00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR OPEN IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air temperature sensor (turbo) connector.</ptxt>
<figure>
<graphic graphicname="A204915E05" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect terminals 1 and 2 of the intake air temperature sensor (turbo) wire harness side connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Intake Air Temp (Turbo).</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>140°C (284°F) or higher</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor (Turbo)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Intake Air Temperature Sensor (Turbo))</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the intake air temperature sensor (turbo) connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188106OX_04_0010" fin="false">OK</right>
<right ref="RM00000188106OX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0003" proc-id="RM23G0E___000020S00001">
<testtitle>CHECK HARNESS AND CONNECTOR (INTAKE AIR TEMPERATURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air temperature sensor (turbo) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C84-2 - C90-3 (THIA)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C84-1 - C90-9 (ETHI)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the intake air temperature sensor (turbo) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188106OX_04_0012" fin="false">OK</right>
<right ref="RM00000188106OX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0004" proc-id="RM23G0E___000020T00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air temperature sensor (turbo) connector.</ptxt>
<figure>
<graphic graphicname="A198733E12" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Intake Air Temp (Turbo).</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>-40°C (-40°F)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor (turbo)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the intake air temperature sensor (turbo) connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188106OX_04_0008" fin="false">OK</right>
<right ref="RM00000188106OX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0005" proc-id="RM23G0E___000020U00001">
<testtitle>CHECK HARNESS AND CONNECTOR (INTAKE AIR TEMPERATURE SENSOR (TURBO) - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air temperature sensor (turbo) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C84-2 or C90-3 (THIA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the intake air temperature sensor (turbo) connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188106OX_04_0006" fin="false">OK</down>
<right ref="RM00000188106OX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0006" proc-id="RM23G0E___000020V00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188106OX_04_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0010" proc-id="RM23G0E___000020Y00001">
<testtitle>CONFIRM GOOD CONNECTION TO SENSOR. IF OK, REPLACE INTAKE AIR TEMPERATURE SENSOR (TURBO).</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the intake air temperature sensor (turbo) (See page <xref label="Seep01" href="RM0000014XP01KX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188106OX_04_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0009" proc-id="RM23G0E___000020X00001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188106OX_04_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0012" proc-id="RM23G0E___000020Z00001">
<testtitle>CONFIRM GOOD CONNECTION TO ECM. IF OK, REPLACE ECM.</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188106OX_04_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0008" proc-id="RM23G0E___000020W00001">
<testtitle>REPLACE INTAKE AIR TEMPERATURE SENSOR (TURBO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the intake air temperature sensor (turbo) (See page <xref label="Seep01" href="RM0000014XP01KX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188106OX_04_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0013" proc-id="RM23G0E___000021000001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK13TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188106OX_04_0014" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188106OX_04_0014">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>