<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S0009" variety="S0009">
<name>1GR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S0009_7B8WZ_T006N" variety="T006N">
<name>CAMSHAFT</name>
<para id="RM000003B6X011X" category="A" type-id="80001" name-id="EMDF1-01" from="201210">
<name>REMOVAL</name>
<subpara id="RM000003B6X011X_01" type-id="01" category="01">
<s-1 id="RM000003B6X011X_01_0064" proc-id="RM23G0E___00003Q900001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0015" proc-id="RM23G0E___00003PM00001">
<ptxt>REMOVE FRONT BUMPER COVER LOWER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper cover lower.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0029" proc-id="RM23G0E___00000G200001">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Unhook the engine under cover from the vehicle body as shown in the illustration.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0035" proc-id="RM23G0E___00003PT00000">
<ptxt>DRAIN ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the oil filler cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil pan drain plug and drain the engine oil into a container.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket and the oil pan drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0018" proc-id="RM23G0E___00000ET00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<figure>
<graphic graphicname="A273797E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reservoir Cap</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the radiator cap and drain the coolant.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug and drain the coolant from the engine.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0052" proc-id="RM23G0E___00003Q400001">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 13 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0056">
<ptxt>DISCONNECT CABLE FROM POSITIVE BATTERY TERMINAL</ptxt>
</s-1>
<s-1 id="RM000003B6X011X_01_0068" proc-id="RM23G0E___00003QD00001">
<ptxt>REMOVE BATTERY HOLD DOWN CLAMP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 2 nuts and remove the battery hold down clamp.</ptxt>
<figure>
<graphic graphicname="A223416" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0059">
<ptxt>REMOVE BATTERY</ptxt>
</s-1>
<s-1 id="RM000003B6X011X_01_0058">
<ptxt>REMOVE BATTERY TRAY</ptxt>
</s-1>
<s-1 id="RM000003B6X011X_01_0037" proc-id="RM23G0E___00003PV00001">
<ptxt>REMOVE V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A229925E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Raise the front of the V-bank cover to detach the 2 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0020" proc-id="RM23G0E___00000GE00000">
<ptxt>REMOVE AIR CLEANER CAP AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224721" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the air cleaner cap and hose.</ptxt>
<s3>
<ptxt>Disconnect the mass air flow meter connector, vacuum hose and ventilation hose and detach the 4 clamps.</ptxt>
</s3>
<s3>
<ptxt>Loosen the clamp.</ptxt>
</s3>
<s3>
<ptxt>Unfasten the 4 hook clamps, and then remove the bolt and air cleaner cap and hose.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0055" proc-id="RM23G0E___00000GK00000">
<ptxt>REMOVE AIR CLEANER CASE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the air cleaner filter element.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224715" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and air cleaner case.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0078" proc-id="RM23G0E___00003QH00000">
<ptxt>REMOVE NO. 1 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A223076" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0079" proc-id="RM23G0E___00003QI00000">
<ptxt>REMOVE NO. 2 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223077" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the radiator hose from the water inlet.</ptxt>
</s2>
<s2>
<ptxt>Detach the clamp and remove the radiator hose.</ptxt>
<figure>
<graphic graphicname="A223078" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0080" proc-id="RM23G0E___00003QJ00000">
<ptxt>REMOVE RADIATOR RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223079" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the reservoir hose from the upper side of the radiator tank.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and radiator reservoir.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0081" proc-id="RM23G0E___00003QK00000">
<ptxt>DISCONNECT OIL COOLER TUBE (w/ Warmer)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw to open the flexible hose clamp, and then remove the 2 bolts to disconnect the oil cooler tube from the fan shroud.</ptxt>
<figure>
<graphic graphicname="A223080" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0082" proc-id="RM23G0E___00003QL00000">
<ptxt>DISCONNECT OIL COOLER TUBE (w/ Air Cooled Transmission Oil Cooler)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw to open the flexible hose clamp, and then remove the 2 bolts to disconnect the oil cooler tube from the fan shroud.</ptxt>
<figure>
<graphic graphicname="A223081" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0083" proc-id="RM23G0E___00003QM00000">
<ptxt>REMOVE FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 4 nuts holding the fluid coupling fan.</ptxt>
<figure>
<graphic graphicname="A223082" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the fan and generator V-belt (See page <xref label="Seep01" href="RM0000017LA019X"/>).</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223083" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts holding the fan shroud.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts of the fluid coupling fan, and then remove the shroud together with the coupling fan.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the fan pulley from the water pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0036" proc-id="RM23G0E___00003PU00001">
<ptxt>REMOVE AIR TUBE ASSEMBLY (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A221459" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>for Bank 1 Side:</ptxt>
<ptxt>Remove the bolt and disconnect the air tube assembly from the emission control valve set.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221458" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>for Bank 2 Side:</ptxt>
<ptxt>Remove the 2 bolts and disconnect the air tube assembly from the No. 2 emission control valve set.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0022" proc-id="RM23G0E___00003PN00001">
<ptxt>REMOVE INTAKE AIR SURGE TANK
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A221456" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the throttle body connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 4 water by-pass hose.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 5 water by-pass hose.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221457" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 1 fuel vapor feed hose.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 1 vacuum switching valve connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 1 ventilation hose.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221464" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 2 heater hose clamps.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221460" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and throttle body bracket.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221461" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a clip remover, detach the wire harness clamp.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221462" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and No. 1 surge tank stay.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221463" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and No. 2 surge tank stay.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A221465" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 nuts, 4 bolts and intake air surge tank.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0046" proc-id="RM23G0E___00000GI00000">
<ptxt>REMOVE AIR TUBE (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223569" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts, 2 nuts and air tube.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 gaskets from the air tube.</ptxt>
<atten3>
<ptxt>Be careful not to damage the installation surface of the gaskets.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0047" proc-id="RM23G0E___00000GJ00000">
<ptxt>REMOVE NO. 2 AIR TUBE (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223563" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts, 2 nuts and No. 2 air tube.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 gaskets from the No. 2 air tube.</ptxt>
<atten3>
<ptxt>Be careful not to damage the installation surface of the gaskets.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0033" proc-id="RM23G0E___00000GG00000">
<ptxt>REMOVE EMISSION CONTROL VALVE SET (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223568" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the emission control valve set connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 1 air hose from the emission control valve set.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223570" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 nuts and emission control valve set.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0034" proc-id="RM23G0E___00000GH00000">
<ptxt>REMOVE NO. 2 EMISSION CONTROL VALVE SET (w/ Secondary Air Injection System)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223562" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the No. 2 emission control valve set connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 3 air hose.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223564" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 nuts and No. 2 emission control valve set.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0030" proc-id="RM23G0E___00000GD00000">
<ptxt>REMOVE IGNITION COIL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A221430" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the 6 ignition coil connectors.</ptxt>
</s2>
<s2>
<ptxt>Remove the 6 bolts and 6 ignition coils.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0053" proc-id="RM23G0E___00003Q500000">
<ptxt>DISCONNECT VANE PUMP ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223016" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 wire harness clamps.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223015" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and disconnect the vane pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0084" proc-id="RM23G0E___00003QN00001">
<ptxt>REMOVE NO. 2 IDLER PULLEY SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Integrated Type:</ptxt>
<ptxt>Remove the bolt and No. 2 idler pulley.</ptxt>
<figure>
<graphic graphicname="A223415" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for Separate Type:</ptxt>
<ptxt>Remove the bolt, No. 2 idler pulley cover plate, No. 2 idler pulley and idler pulley cover plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0062" proc-id="RM23G0E___00003Q700001">
<ptxt>REMOVE WIRING HARNESS CLAMP BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp.</ptxt>
<figure>
<graphic graphicname="A222903" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and wiring harness clamp bracket.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0063" proc-id="RM23G0E___00003Q800000">
<ptxt>REMOVE NO. 2 EXHAUST MANIFOLD HEAT INSULATOR
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A221484" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts and heat insulator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0049" proc-id="RM23G0E___00003Q300001">
<ptxt>REMOVE GENERATOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Open the terminal cap.</ptxt>
<figure>
<graphic graphicname="A222881" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the wire harness from terminal B.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the generator connector from the generator assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="A222884" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="A222882" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the generator bracket.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and generator assembly.</ptxt>
<figure>
<graphic graphicname="A222883" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and generator bracket.</ptxt>
<figure>
<graphic graphicname="A222885" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0039" proc-id="RM23G0E___00003PW00001">
<ptxt>REMOVE ENGINE OIL LEVEL DIPSTICK GUIDE
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A222114" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the dipstick.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and dipstick guide.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the dipstick guide.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0054" proc-id="RM23G0E___00003Q600001">
<ptxt>REMOVE WATER BY-PASS PIPE SUB-ASSEMBLY (w/ Oil Cooler)
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223013" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Disconnect the 2 hoses.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and water by-pass pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0031" proc-id="RM23G0E___00003PR00001">
<ptxt>REMOVE NO. 1 OIL PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 oil pipe unions, oil control valve filter LH, 3 gaskets and No. 1 oil pipe.</ptxt>
<figure>
<graphic graphicname="A223691" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not touch the mesh when removing the oil control valve filter.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0032" proc-id="RM23G0E___00003PS00001">
<ptxt>REMOVE NO. 2 OIL PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 oil pipe unions, oil control valve filter RH, 3 gaskets and No. 2 oil pipe.</ptxt>
<figure>
<graphic graphicname="A223692" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not touch the mesh when removing the oil control valve filter.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0067" proc-id="RM23G0E___00003QC00001">
<ptxt>REMOVE REAR CYLINDER HEAD COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224695" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts and cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0069" proc-id="RM23G0E___00003QE00001">
<ptxt>DISCONNECT FUEL PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and disconnect the fuel pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0024" proc-id="RM23G0E___00003PP00001">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 12 bolts, seal washer, cylinder head cover and gasket.</ptxt>
<figure>
<graphic graphicname="A223666" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A223665" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 gaskets.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0023" proc-id="RM23G0E___00003PO00001">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 12 bolts, seal washer, cylinder head cover and gasket.</ptxt>
<figure>
<graphic graphicname="A223669" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure the removed parts are returned to the same places they were removed from.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A223668" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 gaskets.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003B6X011X_01_0040" proc-id="RM23G0E___00003PX00001">
<ptxt>REMOVE TIMING CHAIN COVER PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts, timing chain cover plate and gasket.</ptxt>
<figure>
<graphic graphicname="A224737" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0025" proc-id="RM23G0E___00003PQ00001">
<ptxt>SET NO. 1 CYLINDER TO TDC/COMPRESSION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the crankshaft pulley and align the notch with the "0" timing mark of the timing chain cover.</ptxt>
<figure>
<graphic graphicname="A072945E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<figure>
<graphic graphicname="A224754E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Check that the timing marks of the camshaft timing gears are aligned with the timing marks of the bearing caps as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bank 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Bank 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the marks are not aligned, turn the crankshaft again to align the marks.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Place paint marks on the timing marks and sprockets of each camshaft timing gear and on the links of the No. 1 chain.</ptxt>
<atten4>
<ptxt>Be sure to place the paint marks on 2 links of the chain and on the sprockets of the camshaft timing gears at the locations of the timing marks of the camshaft timing gears.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0010" proc-id="RM23G0E___00003PK00001">
<ptxt>REMOVE NO. 1 CHAIN TENSIONER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A226510E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Turn the crankshaft approximately 30° counterclockwise so that there is some slack in the chain.</ptxt>
<atten4>
<ptxt>This prevents the valves and pistons from interfering with each other.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A226547E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Align the hole in the lever of the tensioner with the hole in the tensioner body as shown in the illustration, and then insert a pin with a diameter of 1.27 mm (0.0500 in.) into the hole.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lever Hole</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tensioner Hole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="A072945E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Turn the crankshaft clockwise and align the notch with the "0" timing mark of the timing chain cover.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224739" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and chain tensioner.</ptxt>
<atten3>
<ptxt>Do not drop the No. 1 chain tensioner assembly or bolts into the timing chain cover.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0041" proc-id="RM23G0E___00003PY00001">
<ptxt>DISCONNECT CHAIN SUB-ASSEMBLY (for Bank 1)</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A226507E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Turn the crankshaft clockwise until it is in the position shown in the illustration so that there is some slack in the chain between the banks.</ptxt>
<atten4>
<ptxt>When turning the crankshaft, engine oil may spray out of the oil holes.</ptxt>
</atten4>
<atten2>
<ptxt>As the camshafts turn suddenly, do not touch the camshafts or camshaft timing gears.</ptxt>
</atten2>
</s2>
<s2>
<figure>
<graphic graphicname="A226508E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Turn the crankshaft clockwise until it is in the position shown in the illustration so that the chain can be removed easily.</ptxt>
<atten4>
<ptxt>When turning the crankshaft, engine oil may spray out of the oil holes.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A224759" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the chain from the sprocket of the camshaft timing gear and set it on the gear.</ptxt>
<atten2>
<ptxt>As the camshaft may turn suddenly and pinch your fingers when the chain is removed, pinch the chain and lift it upward to remove it from the sprocket.</ptxt>
</atten2>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0042" proc-id="RM23G0E___00003PZ00001">
<ptxt>REMOVE CAMSHAFT BEARING CAP (for Bank 1)</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A225726E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Remove the bolts and bearing caps in the order shown in the illustration. Immediately after removing a bearing cap, install bolts and washers for temporary installation of the camshaft housing in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Washer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Part Removal</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Installation of Bolts and Washers for Temporary Installation of camshaft housing</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Arrange the removed parts so that they can be reinstalled in their original locations.</ptxt>
</item>
<item>
<ptxt>Part number of bolt for temporary installation of camshaft housing: 91551-G0845 (quantity: 8)</ptxt>
</item>
<item>
<ptxt>Part number of washer for temporary installation of camshaft housing: 90201-12028 (quantity: 16)</ptxt>
</item>
</list1>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not install the bearing caps when installing bolts and washers for temporary installation of the camshaft housing.</ptxt>
</item>
<item>
<ptxt>Be sure to follow the numerical order when performing this procedure.</ptxt>
</item>
<item>
<ptxt>Do not allow bolts and washers for temporary installation of the camshaft housing to contact the camshaft.</ptxt>
</item>
<item>
<ptxt>Do not drop bolts and washers for temporary installation of the camshaft housing into the cylinder head.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0011" proc-id="RM23G0E___00003PL00001">
<ptxt>REMOVE NO. 2 CAMSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224761" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt of the No. 2 chain tensioner assembly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224769" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the No. 2 chain tensioner assembly while lifting up the No. 2 camshaft.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224763" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>While lifting up the No. 2 camshaft, pass it through the No. 2 chain and pull it out towards the front of the vehicle to remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0043" proc-id="RM23G0E___00003Q000001">
<ptxt>REMOVE CAMSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Lift up the rear of the camshaft so that it is at an angle.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224767" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the chain from the camshaft timing gear and pull out the camshaft and No. 2 chain towards the rear of the vehicle to remove them.</ptxt>
<atten3>
<ptxt>Do not drop the chain into the gap between the engine and cover.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="A224765" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Suspend the chain with a string or equivalent.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0065" proc-id="RM23G0E___00003QA00001">
<ptxt>DISCONNECT CHAIN SUB-ASSEMBLY (for Bank 2)</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A072945E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Turn the crankshaft counterclockwise and align the notch with the "0" timing mark of the timing chain cover.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224760" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the chain from the sprocket of the camshaft timing gear and set it on the gear.</ptxt>
<atten2>
<ptxt>As the camshaft may turn suddenly and pinch your fingers when the chain is removed, pinch the chain and lift it upward to remove it from the sprocket.</ptxt>
</atten2>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0066" proc-id="RM23G0E___00003QB00001">
<ptxt>REMOVE CAMSHAFT BEARING CAP (for Bank 2)</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A225727E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Remove the bolts and bearing caps in the order shown in the illustration. Immediately after removing a bearing cap, install bolts and washers for temporary installation of the camshaft housing in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Washer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Part Removal</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Installation of Bolts and Washers for Temporary Installation of camshaft housing</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Arrange the removed parts so that they can be reinstalled in their original locations.</ptxt>
</item>
<item>
<ptxt>Part number of bolt for temporary installation of camshaft housing: 91551-G0845 (quantity: 8)</ptxt>
</item>
<item>
<ptxt>Part number of washer for temporary installation of camshaft housing: 90201-12028 (quantity: 16)</ptxt>
</item>
</list1>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not install the bearing caps when installing bolts and washers for temporary installation of the camshaft housing.</ptxt>
</item>
<item>
<ptxt>Be sure to follow the numerical order when performing this procedure.</ptxt>
</item>
<item>
<ptxt>Do not allow bolts and washers for temporary installation of the camshaft housing to contact the camshaft.</ptxt>
</item>
<item>
<ptxt>Do not drop bolts and washers for temporary installation of the camshaft housing into the cylinder head.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0044" proc-id="RM23G0E___00003Q100001">
<ptxt>REMOVE NO. 4 CAMSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224762" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt of the No. 3 chain tensioner assembly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224770" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the No. 3 chain tensioner assembly while lifting up the No. 4 camshaft.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224764" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>While lifting up the No. 4 camshaft, pass it through the No. 2 chain and pull it out towards the front of the vehicle to remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0045" proc-id="RM23G0E___00003Q200001">
<ptxt>REMOVE NO. 3 CAMSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Lift up the rear of the camshaft so that it is at an angle.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224768" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the chain from the camshaft timing gear and pull out the No. 3 camshaft and No. 2 chain towards the rear of the vehicle to remove them.</ptxt>
<atten3>
<ptxt>Do not drop the chain into the gap between the engine and cover.</ptxt>
</atten3>
</s2>
<s2>
<figure>
<graphic graphicname="A224766" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Suspend the chain with a string or equivalent.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0070" proc-id="RM23G0E___00003QF00001">
<ptxt>REMOVE CAMSHAFT TIMING GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fix the camshaft in place.</ptxt>
<atten3>
<ptxt>Be careful not to damage the camshaft.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the flange bolt and camshaft timing gear assembly.</ptxt>
<figure>
<graphic graphicname="A227959E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Do not remove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Straight Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flange Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not remove the other 3 bolts.</ptxt>
</item>
<item>
<ptxt>If planning to reuse the camshaft timing gear, be sure to release the straight pin lock before installing the camshaft timing gear.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003B6X011X_01_0071" proc-id="RM23G0E___00003QG00001">
<ptxt>REMOVE CAMSHAFT TIMING EXHAUST GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fix the camshaft in place.</ptxt>
<atten3>
<ptxt>Be careful not to damage the camshaft.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the flange bolt and camshaft timing exhaust gear assembly.</ptxt>
<figure>
<graphic graphicname="A227960E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Do not remove</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Straight Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flange Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be sure not to remove the other 4 bolts.</ptxt>
</item>
<item>
<ptxt>If planning to reuse the gear, be sure to release the straight pin lock before installing the gear.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>