<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97M_T00HA" variety="T00HA">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Vacuum Brake Booster)</name>
<para id="RM000000XIM0Q2X" category="C" type-id="803LS" name-id="BCDPX-02" from="201210">
<dtccode>C1434</dtccode>
<dtcname>Steering Angle Sensor Output Malfunction</dtcname>
<subpara id="RM000000XIM0Q2X_01" type-id="60" category="03" proc-id="RM23G0E___0000AMX00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Steering angle sensor signals are sent to the skid control ECU via the CAN communication system. When there is a malfunction in the CAN communication system, it is detected by the steering angle sensor zero point malfunction diagnostic function.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1434</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An error in communication between the skid control ECU and the steering angle sensor, or an abnormal steering angle sensor zero point.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Steering angle sensor power supply</ptxt>
</item>
<item>
<ptxt>Steering angle sensor (Spiral cable sub-assembly)</ptxt>
</item>
<item>
<ptxt>Brake actuator assembly (Skid control ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XIM0Q2X_02" type-id="32" category="03" proc-id="RM23G0E___0000AMY00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC C1432 (See page <xref label="Seep01" href="RM000000XIM0Q0X_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XIM0Q2X_03" type-id="51" category="05" proc-id="RM23G0E___0000AMZ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not remove the steering sensor from the spiral cable sub-assembly.</ptxt>
</item>
<item>
<ptxt>When replacing the spiral cable sub-assembly, confirm that the replacement part is of the correct specification.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When DTC U0073, U0123, U0124 and/or U0126 is output together with DTC C1434, inspect and repair the trouble areas indicated by DTC U0073, U0123, U0124 and/or U0126 first (See page <xref label="Seep01" href="RM000000YUO0M0X"/>).</ptxt>
</item>
<item>
<ptxt>When any of the speed sensors or the yaw rate and acceleration sensor has trouble, DTCs for the steering angle sensor may be stored even when the steering angle sensor is normal. When DTCs for the speed sensors or yaw rate and acceleration sensor are output together with DTCs for the steering angle sensor, inspect and repair the speed sensor and yaw rate and acceleration sensor first, and then inspect and repair the steering angle sensor.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XIM0Q2X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIM0Q2X_04_0002" proc-id="RM23G0E___0000AN000001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0G5X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON again and check that no CAN communication system DTC is output (See page <xref label="Seep02" href="RM000000XHV0G5X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 35 km/h (22 mph) and turn the steering wheel to the right and left, and then check that no speed sensor or yaw rate and acceleration sensor DTCs are output (See page <xref label="Seep03" href="RM000000XHV0G5X"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.60in"/>
<colspec colname="COLSPEC0" colwidth="2.30in"/>
<colspec colname="COL2" colwidth="1.18in"/>
<thead>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle">
<ptxt>No CAN communication system, speed sensor, or yaw rate and acceleration sensor DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle">
<ptxt>Speed sensor or yaw rate and acceleration sensor DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If there is a malfunction in any of the speed sensors or the yaw rate and acceleration sensor, an abnormal value may be output although the steering angle sensor is normal.</ptxt>
</item>
<item>
<ptxt>If speed sensor and yaw rate and acceleration sensor DTCs are output simultaneously, repair these two sensors first, and then inspect the steering angle sensor.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000XIM0Q2X_04_0016" fin="false">A</down>
<right ref="RM000000XIM0Q2X_04_0011" fin="true">B</right>
<right ref="RM000000XIM0Q2X_04_0019" fin="true">C</right>
<right ref="RM000000XIM0Q2X_04_0007" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0016" proc-id="RM23G0E___0000AMQ00001">
<testtitle>CHECK TERMINAL VOLTAGE (BAT)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Make sure that there is no looseness at the locking parts and connecting parts of the connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G33 steering angle sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C197050E13" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G33-6 (BAT) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Steering Angle Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XIM0Q2X_04_0017" fin="false">OK</down>
<right ref="RM000000XIM0Q2X_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0017" proc-id="RM23G0E___0000AMS00001">
<testtitle>CHECK TERMINAL VOLTAGE (IG)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G33 steering angle sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C197050E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G33-5 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Steering Angle Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XIM0Q2X_04_0018" fin="false">OK</down>
<right ref="RM000000XIM0Q2X_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0018" proc-id="RM23G0E___0000AMR00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ESS CIRCUIT)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G33 steering angle sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.61in"/>
<colspec colname="COL2" colwidth="1.15in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>

</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G33-2 (ESS) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000000XIM0Q2X_04_0008" fin="true">OK</down>
<right ref="RM000000XIM0Q2X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0008">
<testtitle>REPLACE SPIRAL CABLE SUB-ASSEMBLY<xref label="Seep01" href="RM000000SS907JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0011">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO087X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0007">
<testtitle>REPAIR CIRCUIT INDICATED BY OUTPUT DTC<xref label="Seep01" href="RM000000XHU0B8X"/>
</testtitle>

</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0012">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XIM0Q2X_04_0019">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO089X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>