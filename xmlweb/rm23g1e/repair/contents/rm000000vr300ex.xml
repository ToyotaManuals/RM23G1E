<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>5L-E ENGINE CONTROL</name>
<ttl id="12005_S0006_7B8VX_T005L" variety="T005L">
<name>ECD SYSTEM</name>
<para id="RM000000VR300EX" category="C" type-id="3031C" name-id="ESVAG-02" from="201207">
<dtccode>24</dtccode>
<dtcname>Air Temperature Circuit Malfunction</dtcname>
<subpara id="RM000000VR300EX_05" type-id="60" category="03" proc-id="RM23G0E___000017V00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The intake air temperature sensor senses the intake air temperature. A thermistor built into this sensor changes its resistance according to the intake air temperature as shown in the illustration. The resistance value of the intake air temperature sensor is one of the factors used to determine the fuel injection volume.</ptxt>
<figure>
<graphic graphicname="G036559E07" width="2.775699831in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="3.19in"/>
<colspec colname="COL3" colwidth="3.18in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>24</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in the intake air temperature sensor circuit for 0.5 sec. or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in intake air temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Intake air temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000VR300EX_06" type-id="32" category="03" proc-id="RM23G0E___000017W00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A227814E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000VR300EX_07" type-id="51" category="05" proc-id="RM23G0E___000017X00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTCs related to different systems that have terminal E2 as the ground terminal are stored simultaneously, there may be an open circuit between terminal E2 and body ground.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000VR300EX_01" type-id="54" category="05">
<name>When using intelligent tester:</name>
</subpara>
<subpara id="RM000000VR300EX_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000VR300EX_02_0001" proc-id="RM23G0E___000017N00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INTAKE AIR TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Intake Air.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Value is same as actual engine coolant temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When DTC 24 is stored, the type of malfunction can be determined based on the intake air temperature (see below).</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Malfunction</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Open circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Short circuit</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000VR300EX_02_0002" fin="false">A</down>
<right ref="RM000000VR300EX_02_0011" fin="false">B</right>
<right ref="RM000000VR300EX_02_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000VR300EX_02_0002" proc-id="RM23G0E___000017O00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR OPEN IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A110535E07" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect terminals 1 and 2 of the intake air temperature sensor harness side connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Read the temperature value on the intelligent tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>140°C (284°F) or higher</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Intake Air Temperature Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the intake air temperature sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VR300EX_02_0003" fin="true">OK</down>
<right ref="RM000000VR300EX_02_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VR300EX_02_0008" proc-id="RM23G0E___000017P00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR OPEN IN ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect terminals THA and E2 of the ECM connector.</ptxt>
<figure>
<graphic graphicname="A110532E17" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Read the temperature value on the intelligent tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>140°C (284°F) or higher</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000VR300EX_02_0014" fin="true">OK</down>
<right ref="RM000000VR300EX_02_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VR300EX_02_0011" proc-id="RM23G0E___000017Q00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A076591E25" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Read the temperature value on the intelligent tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>-40°C (-40°F)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the intake air temperature sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VR300EX_02_0003" fin="true">OK</down>
<right ref="RM000000VR300EX_02_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VR300EX_02_0013" proc-id="RM23G0E___000017R00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR SHORT IN ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="A110533E14" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Read the temperature value on the intelligent tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>-40°C (-40°F)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VR300EX_02_0014" fin="true">OK</down>
<right ref="RM000000VR300EX_02_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VR300EX_02_0007">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000VQG00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000VR300EX_02_0003">
<testtitle>REPLACE INTAKE AIR TEMPERATURE SENSOR<xref label="Seep01" href="RM0000014XP01JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000VR300EX_02_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000VR300EX_02_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
<subpara id="RM000000VR300EX_03" type-id="55" category="05">
<name>When not using intelligent tester:</name>
</subpara>
<subpara id="RM000000VR300EX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000VR300EX_04_0004" proc-id="RM23G0E___000017S00000">
<testtitle>CHECK ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
<figure>
<graphic graphicname="A062416E91" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C102-3 (THA) - C102-9 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.2 to 3.8 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C102-3 (THA) - C102-9 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>80°C (176°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.1 to 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000VR300EX_04_0005" fin="true">OK</down>
<right ref="RM000000VR300EX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VR300EX_04_0006" proc-id="RM23G0E___000017T00000">
<testtitle>INSPECT INTAKE AIR TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the intake air temperature sensor (See page <xref label="Seep01" href="RM0000014XN01SX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VR300EX_04_0016" fin="false">OK</down>
<right ref="RM000000VR300EX_04_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VR300EX_04_0016" proc-id="RM23G0E___000017U00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - INTAKE AIR TEMPERATURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the intake air temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C104-2 - C102-3 (THA)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C104 - C102-9 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C104-2 or C102-3 (THA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the intake air temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000VR300EX_04_0017" fin="true">OK</down>
<right ref="RM000000VR300EX_04_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000VR300EX_04_0005">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000VQG00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000VR300EX_04_0018">
<testtitle>REPLACE INTAKE AIR TEMPERATURE SENSOR<xref label="Seep01" href="RM0000014XP01JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000VR300EX_04_0019">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000VR300EX_04_0017">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202MX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>