<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001M" variety="S001M">
<name>STEERING COLUMN</name>
<ttl id="12038_S001M_7B98P_T00ID" variety="T00ID">
<name>STEERING LOCK SYSTEM</name>
<para id="RM000001T15036X" category="U" type-id="3001G" name-id="SR1Y5-04" from="201207">
<name>TERMINALS OF ECU</name>
<subpara id="RM000001T15036X_z0" proc-id="RM23G0E___0000BA900000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK STEERING LOCK ECU</ptxt>
<figure>
<graphic graphicname="C104383E20" width="2.775699831in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the G35 steering lock ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table frame="all">
<tgroup cols="5">
<colspec colname="col1" colwidth="1.42in"/>
<colspec colname="col2" colwidth="1.42in"/>
<colspec colname="col3" colwidth="1.43in"/>
<colspec colname="col4" colwidth="1.40in"/>
<colspec colname="col5" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G35-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G35-6 (IG2) - G35-1 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG2 signal input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off → engine switch on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V → 11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G35-7 (B) - G35-1 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the G35 steering lock ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G35-3 (IGE) - G35-1 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power source for motor drive</ptxt>
</entry>
<entry valign="middle">
<ptxt>When all conditions are met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Steering lock unlocked</ptxt>
</item>
<item>
<ptxt>Engine switch off</ptxt>
</item>
<item>
<ptxt>Shift lever in P*</ptxt>
</item>
<item>
<ptxt>Driver side door closed → opened</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V (Steering lock motor not operating) → Below 1 V (Steering lock motor operating) → 11 to 14 V (Steering lock motor not operating)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G35-4 (SLP1) - G35-1 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>SB - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unlock position sensor output signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering lock locked → steering lock unlocked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V → Below 1.2 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*: for Automatic Transmission</ptxt>
<ptxt>If the result is not as specified, there may be a malfunction in the steering lock ECU.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>