<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A1_T00JP" variety="T00JP">
<name>TOYOTA PARKING ASSIST-SENSOR SYSTEM (for 8 Sensor Type)</name>
<para id="RM000002R2S019X" category="C" type-id="802QF" name-id="PM39S-04" from="201210">
<dtccode>C1AE3</dtccode>
<dtcname>Front Right Center Sensor</dtcname>
<subpara id="RM000002R2S019X_01" type-id="60" category="03" proc-id="RM23G0E___0000CSP00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The No. 1 ultrasonic sensor (front right center sensor) is installed on the front bumper. The ECU detects obstacles based on signals received from the No. 1 ultrasonic sensor (front right center sensor). If the No. 1 ultrasonic sensor (front right center sensor) has an open or other malfunction, it will not function normally.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1AE3</ptxt>
</entry>
<entry valign="middle">
<ptxt>A malfunction of the No. 1 ultrasonic sensor (front right center sensor).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>No. 1 ultrasonic sensor (front right center sensor)</ptxt>
</item>
<item>
<ptxt>Clearance warning ECU*1</ptxt>
</item>
<item>
<ptxt>Parking assist ECU*2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/o Parking Assist Monitor System and/or Side Monitor System</ptxt>
</item>
<item>
<ptxt>*2: w/ Parking Assist Monitor System and/or Side Monitor System</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002R2S019X_03" type-id="51" category="05" proc-id="RM23G0E___0000CSQ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>If the DTC is output after repairs, turn the ignition switch to ON and turn the TOYOTA parking assist-sensor system on. Then clear the DTC (See page <xref label="Seep01" href="RM000000VKU048X"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002R2S019X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002R2S019X_04_0001" proc-id="RM23G0E___0000CSR00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000VKU048X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only DTC C1AE3 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1AE3 and C1AEC are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002R2S019X_04_0003" fin="false">A</down>
<right ref="RM000002R2S019X_04_0010" fin="true">B</right>
<right ref="RM000002R2S019X_04_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002R2S019X_04_0003" proc-id="RM23G0E___0000CSS00001">
<testtitle>REPLACE NO. 1 ULTRASONIC SENSOR (FRONT RIGHT CENTER SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the No. 1 ultrasonic sensor (front right center sensor) with a normally functioning sensor (See page <xref label="Seep01" href="RM00000470F00MX"/>).</ptxt>
<atten4>
<ptxt>All of the sensors are interchangeable. To confirm whether a sensor is functioning normally, switch it with a known good sensor from the other end of the vehicle.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002R2S019X_04_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002R2S019X_04_0004" proc-id="RM23G0E___0000CST00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000VKU048X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000VKU048X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC C1AE3 is output (w/o Parking Assist Monitor System and/or Side Monitor System [for LHD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1AE3 is output (w/o Parking Assist Monitor System and/or Side Monitor System [for RHD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1AE3 is output (w/ Parking Assist Monitor System and/or Side Monitor System [for LHD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1AE3 is output (w/ Parking Assist Monitor System and/or Side Monitor System [for RHD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002R2S019X_04_0012" fin="true">A</down>
<right ref="RM000002R2S019X_04_0009" fin="true">B</right>
<right ref="RM000002R2S019X_04_0011" fin="true">C</right>
<right ref="RM000002R2S019X_04_0007" fin="true">D</right>
<right ref="RM000002R2S019X_04_0008" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000002R2S019X_04_0010">
<testtitle>GO TO DTC (C1AEC)<xref label="Seep01" href="RM000003N4V02QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2S019X_04_0005">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2S019X_04_0011">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM00000466X01EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2S019X_04_0007">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM00000470500GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2S019X_04_0012">
<testtitle>REPLACE CLEARANCE WARNING ECU<xref label="Seep01" href="RM00000466X01DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2S019X_04_0009">
<testtitle>REPLACE CLEARANCE WARNING ECU<xref label="Seep01" href="RM00000470500FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2S019X_04_0008">
<testtitle>END (NO. 1 ULTRASONIC SENSOR WAS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>