<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S000T" variety="S000T">
<name>1KD-FTV LUBRICATION</name>
<ttl id="12012_S000T_7B927_T00BV" variety="T00BV">
<name>OIL PUMP (w/ DPF)</name>
<para id="RM0000046L200NX" category="A" type-id="80001" name-id="LU3FO-03" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000046L200NX_02" type-id="11" category="10" proc-id="RM23G0E___00006TE00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000046L200NX_01" type-id="01" category="01">
<s-1 id="RM0000046L200NX_01_0044" proc-id="RM23G0E___00006TA00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000046L200NX_01_0050" proc-id="RM23G0E___000044F00000">
<ptxt>DRAIN ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the oil filler cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil pan drain plug and gasket, and then drain the engine oil into a container.</ptxt>
</s2>
<s2>
<ptxt>Wipe the oil pan and drain plug.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket and the oil pan drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>34</t-value1>
<t-value2>347</t-value2>
<t-value4>25</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0114" proc-id="RM23G0E___00004PR00001">
<ptxt>REMOVE FRONT BUMPER LOWER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper lower cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0115" proc-id="RM23G0E___00004PE00001">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and No. 1 engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0051" proc-id="RM23G0E___000032G00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A218364" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Drain the coolant by removing the reservoir cap and, using a wrench, remove the vent plug.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug.</ptxt>
<figure>
<graphic graphicname="A245857E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vent Plug</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0048" proc-id="RM23G0E___00006TB00001">
<ptxt>REMOVE ENGINE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000045F800NX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000046L200NX_01_0049">
<ptxt>INSTALL ENGINE TO ENGINE STAND</ptxt>
</s-1>
<s-1 id="RM0000046L200NX_01_0012" proc-id="RM23G0E___00006T800001">
<ptxt>REMOVE TIMING BELT</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000147C01QX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000046L200NX_01_0015" proc-id="RM23G0E___00006T900001">
<ptxt>REMOVE ELECTRIC EGR CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000004LOY005X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000046L200NX_01_0152" proc-id="RM23G0E___00006TD00001">
<ptxt>REMOVE FUEL SUPPLY PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000045CH009X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000046L200NX_01_0128" proc-id="RM23G0E___00006TC00001">
<ptxt>REMOVE INJECTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000044TN00SX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000046L200NX_01_0052" proc-id="RM23G0E___00004JB00000">
<ptxt>REMOVE GENERATOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and generator.</ptxt>
<figure>
<graphic graphicname="A239368" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0053" proc-id="RM23G0E___00004JC00000">
<ptxt>REMOVE GENERATOR BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and generator bracket.</ptxt>
<figure>
<graphic graphicname="A239369" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0153" proc-id="RM23G0E___00004JZ00000">
<ptxt>REMOVE NO. 2 IDLE PULLEY ASSEMBLY (w/ Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, pulley plate, No. 2 idle pulley and spacer.</ptxt>
<figure>
<graphic graphicname="A239370" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0054" proc-id="RM23G0E___00004JA00000">
<ptxt>REMOVE NO. 1 COMPRESSOR MOUNTING BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and No. 1 compressor mounting bracket.</ptxt>
<figure>
<graphic graphicname="A244867" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0059" proc-id="RM23G0E___00004JH00000">
<ptxt>REMOVE VACUUM PUMP ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts, vacuum pump and 2 O-rings.</ptxt>
<figure>
<graphic graphicname="A098930E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0060" proc-id="RM23G0E___00004JI00000">
<ptxt>REMOVE VANE PUMP ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts, vane pump and O-ring.</ptxt>
<figure>
<graphic graphicname="A224124" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0061" proc-id="RM23G0E___00004JJ00000">
<ptxt>REMOVE CAMSHAFT POSITION SENSOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and camshaft position sensor.</ptxt>
<figure>
<graphic graphicname="A224125" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0062" proc-id="RM23G0E___000032C00000">
<ptxt>REMOVE CRANKSHAFT POSITION SENSOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the crankshaft position sensor connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the clamp labeled A in the illustration.</ptxt>
<figure>
<graphic graphicname="A239609E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that no portion of the clamp labeled A remains in the clamp installation hole. If there is any portion of the clamp remaining, remove it.</ptxt>
</item>
<item>
<ptxt>Do not reuse the clamp labeled A.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Detach the 3 wire harness clamps and remove the bolt and crankshaft position sensor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0154" proc-id="RM23G0E___00004K300000">
<ptxt>REMOVE OIL PRESSURE SWITCHING VALVE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and oil pressure switching valve.</ptxt>
<figure>
<graphic graphicname="A239379" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0116" proc-id="RM23G0E___000049200000">
<ptxt>REMOVE CRANKSHAFT PULLEY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the crankshaft pulley and loosen the pulley bolt.</ptxt>
<figure>
<graphic graphicname="A225341E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09213-58014</s-number>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST, remove the pulley bolt and crankshaft pulley.</ptxt>
<figure>
<graphic graphicname="A225342E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05020</s-subnumber>
<s-subnumber>09954-05021</s-subnumber>
</sstitem>
</sst>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0124" proc-id="RM23G0E___00004JN00000">
<ptxt>REMOVE NO. 1 VACUUM TRANSMITTING PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, nut and No. 1 vacuum transmitting pipe.</ptxt>
<figure>
<graphic graphicname="A239383" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0125" proc-id="RM23G0E___00004JX00000">
<ptxt>REMOVE NO. 1 OIL PAN COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and No. 1 oil pan cover.</ptxt>
<figure>
<graphic graphicname="A224144" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0129" proc-id="RM23G0E___00004RS00001">
<ptxt>REMOVE CAMSHAFT TIMING PULLEY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt of the camshaft timing pulley while holding the camshaft with a wrench.</ptxt>
<figure>
<graphic graphicname="A060182E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure the timing belt is not installed when removing the bolt of camshaft timing pulley.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the camshaft timing pulley.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0130" proc-id="RM23G0E___00004RT00001">
<ptxt>REMOVE NO. 2 TIMING BELT COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts, nut and No. 2 timing belt cover.</ptxt>
<figure>
<graphic graphicname="A225464" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0131" proc-id="RM23G0E___000049H00000">
<ptxt>REMOVE WATER PUMP ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts, 2 nuts, water pump and gasket.</ptxt>
<figure>
<graphic graphicname="A225339" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0132" proc-id="RM23G0E___00004LH00000">
<ptxt>REMOVE TIMING GEAR COVER
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>As the fuel supply pump is not installed, the injection gear is loose inside the timing gear case. Do not allow the injection gear to fall.</ptxt>
</atten3>
<atten4>
<ptxt>To prevent the injection gear from falling, temporarily install the fuel supply pump.</ptxt>
</atten4>
<s2>
<ptxt>Remove the 14 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A239293" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Pry the timing gear cover at the locations shown in the illustration and remove the timing gear cover.</ptxt>
<figure>
<graphic graphicname="A239294" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to drop the injection gear.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 3 O-rings from the timing gear case.</ptxt>
<figure>
<graphic graphicname="A239295" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0133" proc-id="RM23G0E___00004LV00000">
<ptxt>REMOVE INJECTION GEAR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Secure the No. 2 idle sub gear to the No. 1 idle gear with a service bolt.</ptxt>
<figure>
<graphic graphicname="A222329" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>If the bolt hole of the No. 2 idle sub gear is not aligned with the bolt hole of the No. 1 idle gear, rotate the crankshaft counterclockwise to align the bolt holes. Then install the service bolt.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the injection gear.</ptxt>
<figure>
<graphic graphicname="A222330" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0134" proc-id="RM23G0E___00004LW00000">
<ptxt>REMOVE NO. 1 CRANKSHAFT POSITION SENSOR PLATE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 1 crankshaft position sensor plate.</ptxt>
<figure>
<graphic graphicname="A239296" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0135" proc-id="RM23G0E___00004LK00000">
<ptxt>REMOVE CRANKSHAFT TIMING GEAR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the crankshaft timing gear.</ptxt>
<figure>
<graphic graphicname="A222332E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05010</s-subnumber>
<s-subnumber>09954-05021</s-subnumber>
</sstitem>
</sst>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0136" proc-id="RM23G0E___00004LY00000">
<ptxt>REMOVE IDLE GEAR THRUST PLATE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and idle gear thrust plate.</ptxt>
<figure>
<graphic graphicname="A238857" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0137" proc-id="RM23G0E___00004LI00000">
<ptxt>REMOVE NO. 1 IDLE GEAR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 1 idle gear together with the No. 2 idle sub gear.</ptxt>
<figure>
<graphic graphicname="A238858" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0138" proc-id="RM23G0E___00004LZ00000">
<ptxt>REMOVE NO. 1 IDLE GEAR SHAFT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 1 idle gear shaft.</ptxt>
<figure>
<graphic graphicname="A238859" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0139" proc-id="RM23G0E___00004LL00000">
<ptxt>REMOVE NO. 2 OIL PAN SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 11 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A225306" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Insert the blade of an oil pan seal cutter between the oil pans. Cut through the applied sealer and remove the No. 2 oil pan.</ptxt>
<figure>
<graphic graphicname="A225310" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0140" proc-id="RM23G0E___00004LM00000">
<ptxt>REMOVE OIL STRAINER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts, oil strainer and gasket.</ptxt>
<figure>
<graphic graphicname="A228657" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0141" proc-id="RM23G0E___00004M400000">
<ptxt>REMOVE OIL PAN SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 22 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A225307" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a screwdriver, remove the oil pan by prying between the oil pan and cylinder block as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A225311E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>RH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pry</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to damage the contact surfaces of the cylinder block and oil pan.</ptxt>
</atten3>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the gasket.</ptxt>
<figure>
<graphic graphicname="A225308" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046L200NX_01_0142" proc-id="RM23G0E___00004LN00000">
<ptxt>REMOVE TIMING GEAR CASE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the union bolt and 8 bolts.</ptxt>
<figure>
<graphic graphicname="A239298" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Pry the timing gear case at the location shown in the illustration and remove the gear case and gasket.</ptxt>
<figure>
<graphic graphicname="A239299" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 O-rings.</ptxt>
<figure>
<graphic graphicname="A069840E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>