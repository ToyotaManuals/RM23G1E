<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000187V06EX" category="C" type-id="30326" name-id="ESTCI-24" from="201210">
<dtccode>P0200</dtccode>
<dtcname>Injector Circuit / Open</dtcname>
<subpara id="RM00000187V06EX_01" type-id="60" category="03" proc-id="RM23G0E___00001KI00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The injector driver delivers drive signals to the injector assemblies using the DC/DC converter, which provides a high-voltage and quick-charging system.</ptxt>
<ptxt>Soon after the injector driver receives a fuel injection command (IJT) signal from the ECM, the injector driver responds to the command with an injector injection confirmation (IJF) signal when the current is applied to the injector assembly.</ptxt>
<figure>
<graphic graphicname="A221606E04" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>P0200</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idling for 5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in the injector driver or fuel injector circuit (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in injector driver circuit</ptxt>
</item>
<item>
<ptxt>Injector assembly</ptxt>
</item>
<item>
<ptxt>Injector driver</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000187V06EX_02" type-id="64" category="03" proc-id="RM23G0E___00001KJ00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<title>P0200 (Open or short in injector driver or fuel injector circuit):</title>
<ptxt>The ECM continuously monitors both injection command (IJT) signals and injection confirmation (IJF) signals. This DTC will be stored if the ECM determines that the number of IJT signals is inconsistent with the number of IJF signals.</ptxt>
<ptxt>The injector assemblies are grounded through a Field Effect Transistor (FET) and a serial resistor. This resistor creates a voltage drop, which is monitored by the injector driver (injector drive circuit) in relation to the current drawn by the injector assembly. When the injector assembly current becomes too high, the voltage drop over the resistor exceeds a specified level and no IJF signal for that cylinder is sent to the ECM.</ptxt>
<ptxt>After the engine is started, when there is no injection confirmation (IJF) signal sent from the injector driver to the ECM even though the ECM sends injection command (IJT) signals to the injector driver, DTC P062D is stored.</ptxt>
<ptxt>If this DTC is stored, the ECM enters fail-safe mode and limits engine power or stops the engine. The ECM continues operating in fail-safe mode until the ignition switch is turned off.</ptxt>
<figure>
<graphic graphicname="A145849E07" width="7.106578999in" height="3.779676365in"/>
</figure>
</topic>
</content5>
</subpara>
<subpara id="RM00000187V06EX_06" type-id="32" category="03" proc-id="RM23G0E___00001KK00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A205139E09" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A227726E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187V06EX_07" type-id="51" category="05" proc-id="RM23G0E___00001KL00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07OX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep03" href="RM0000012XK07OX_02_0003"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When DTC P0200 is stored, the malfunctioning parts can be narrowed down according to fail-safe behavior.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>When a DTC is stored but the engine does not stall, there may be an open circuit or malfunction in the following components which affect the control of individual cylinders.</ptxt>
</item>
<list3 type="ordered">
<item>
<ptxt>The circuit inside an injector assembly.</ptxt>
</item>
<item>
<ptxt>The wiring between an injector driver and injector assembly (between terminal IJ1+ and the No. 1 injector assembly, terminal IJ1 and the No. 1 injector assembly, etc.).</ptxt>
</item>
<item>
<ptxt>The wiring between an injector driver and the ECM (terminals #10 and #1, etc.).</ptxt>
</item>
</list3>
<item>
<ptxt>When a DTC is stored and the engine stalls, there may be a malfunction in the wiring which affects the control of multiple cylinders.</ptxt>
</item>
<list3 type="ordered">
<item>
<ptxt>Wiring between an injector driver and the ECM (between INJF terminals).</ptxt>
</item>
<item>
<ptxt>The power supply wiring.</ptxt>
</item>
</list3>
</list2>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187V06EX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187V06EX_08_0004" proc-id="RM23G0E___00001KP00001">
<testtitle>CHECK TERMINAL VOLTAGE INJECTOR DRIVER POWER SOURCE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the injector driver connectors.</ptxt>
<figure>
<graphic graphicname="A221614E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C88-2 (+B) - C88-3 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to injector driver)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the injector driver connectors.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0001" fin="false">OK</down>
<right ref="RM00000187V06EX_08_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0001" proc-id="RM23G0E___00001KM00001">
<testtitle>INSPECT INJECTOR ASSEMBLY (RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the injector assembly (See page <xref label="Seep01" href="RM0000044VV005X_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0002" fin="false">OK</down>
<right ref="RM00000187V06EX_08_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0002" proc-id="RM23G0E___00001KN00001">
<testtitle>CHECK HARNESS AND CONNECTOR (INJECTOR ASSEMBLY - INJECTOR DRIVER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the injector assembly connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the injector driver connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1-1 - C88-1 (IJ1+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C1-2 - C99-6 (IJ1-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C2-1 - C88-6 (IJ2+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C2-2 - C88-4 (IJ2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-1 - C88-5 (IJ3+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-2 - C99-2 (IJ3-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-1 - C99-3 (IJ4+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-2 - C99-5 (IJ4-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1-1 or C88-1 (IJ1+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C1-2 or C99-6 (IJ1-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C2-1 or C88-6 (IJ2+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C2-2 or C99-4 (IJ2-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-1 or C88-5 (IJ3+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-2 or C99-2 (IJ3-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-1 or C99-3 (IJ4+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-2 or C99-5 (IJ4-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the injector assembly connectors.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the injector driver connectors.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0003" fin="false">OK</down>
<right ref="RM00000187V06EX_08_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0003" proc-id="RM23G0E___00001KO00001">
<testtitle>CHECK HARNESS AND CONNECTOR (INJECTOR DRIVER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the injector driver connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C89-4 (#10) - C90-21 (#1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C89-8 (#20) - C90-22 (#2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C89-7 (#30) - C90-23 (#3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C89-3 (#40) - C90-24 (#4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C89-5 (INJF) - C90-20 (INJF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C89-4 (#10) or C90-21 (#1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C89-8 (#20) or C90-22 (#2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C89-7 (#30) or C90-23 (#3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C89-3 (#40) or C90-24 (#4) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C89-5 (INJF) or C90-20 (INJF) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the injector driver connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0019" fin="false">OK</down>
<right ref="RM00000187V06EX_08_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0019" proc-id="RM23G0E___00001KX00001">
<testtitle>REPLACE INJECTOR DRIVER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the injector driver (See page <xref label="Seep01" href="RM0000045BG005X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0020" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0020" proc-id="RM23G0E___00001KY00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK13TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and idle it for 10 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0200 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>No output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0010" fin="false">A</down>
<right ref="RM00000187V06EX_08_0016" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0010" proc-id="RM23G0E___00001KT00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187V06EX_08_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0009" proc-id="RM23G0E___00001KS00001">
<testtitle>GO TO INJECTOR CIRCUIT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Go to the injector circuit (See page <xref label="Seep01" href="RM000002V5Q05RX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187V06EX_08_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0007" proc-id="RM23G0E___00001KQ00001">
<testtitle>REPLACE INJECTOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the injector assembly (See page <xref label="Seep01" href="RM0000044TN00TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0017" proc-id="RM23G0E___00001KV00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802NX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0018" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0018" proc-id="RM23G0E___00001KW00001">
<testtitle>REGISTER INJECTOR COMPENSATION CODE AND PERFORM PILOT QUANTITY LEARNING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the injector compensation code (See page <xref label="Seep01" href="RM0000012XK07OX_02_0003"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the injector pilot quantity learning (See page <xref label="Seep02" href="RM0000012XK07OX_02_0009"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187V06EX_08_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0008" proc-id="RM23G0E___00001KR00001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0013" proc-id="RM23G0E___00001KU00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK13TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and idle it for 10 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187V06EX_08_0016" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187V06EX_08_0016">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>