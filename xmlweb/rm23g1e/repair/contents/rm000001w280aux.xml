<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0022" variety="S0022">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0022_7B9C6_T00LU" variety="T00LU">
<name>AIRBAG SYSTEM</name>
<para id="RM000001W280AUX" category="C" type-id="303Q9" name-id="RS94G-04" from="201210">
<dtccode>B1660/43</dtccode>
<dtcname>Passenger Airbag OFF Indicator Circuit Malfunction</dtcname>
<subpara id="RM000001W280AUX_01" type-id="60" category="03" proc-id="RM23G0E___0000FIV00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The airbag cutoff indicator circuit consists of the center airbag sensor and the airbag cutoff indicator.</ptxt>
<ptxt>The airbag cutoff indicator indicates the operation condition of the instrument panel passenger airbag, front seat side airbag and front seat outer belt.</ptxt>
<ptxt>DTC B1660/43 is stored when a malfunction is detected in the airbag cutoff indicator circuit.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1660/43</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor detects open circuit signal, short circuit to ground signal or short circuit to B+ signal in the airbag cutoff indicator circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A airbag cutoff indicator malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>Telltale light assembly</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001W280AUX_02" type-id="32" category="03" proc-id="RM23G0E___0000FIW00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C217346E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001W280AUX_03" type-id="51" category="05" proc-id="RM23G0E___0000FIX00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable from the negative (-) battery terminal while performing repairs, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001W280AUX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001W280AUX_08_0001" proc-id="RM23G0E___0000FIY00001">
<testtitle>CHECK PASSENGER AIRBAG CUTOFF INDICATOR CONDITION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
<figure>
<graphic graphicname="C216915E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Check the airbag cutoff indicator operation.</ptxt>
<atten4>
<ptxt>Refer to the normal condition of the airbag cutoff indicator (See page <xref label="Seep01" href="RM000000XFD0INX"/>).</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to </ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Always on</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Off</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0002" fin="false">A</down>
<right ref="RM000001W280AUX_08_0067" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0002" proc-id="RM23G0E___0000FIZ00001">
<testtitle>CHECK CONNECTION OF CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the center airbag sensor and telltale light.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0025" fin="false">OK</down>
<right ref="RM000001W280AUX_08_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0025" proc-id="RM23G0E___0000FJ700001">
<testtitle>CHECK CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor and telltale light.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors (on the center airbag sensor side and telltale light side) are not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are not deformed or damaged.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="C208244E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Telltale Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0003" fin="false">OK</down>
<right ref="RM000001W280AUX_08_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0003" proc-id="RM23G0E___0000FJ000001">
<testtitle>CHECK PASSENGER AIRBAG CUTOFF INDICATOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connector to the telltale light.</ptxt>
<figure>
<graphic graphicname="C216914" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check the airbag cutoff indicator operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The airbag cutoff indicator does not come on.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0008" fin="false">OK</down>
<right ref="RM000001W280AUX_08_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0008" proc-id="RM23G0E___0000FJ200001">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connectors to the center airbag sensor.</ptxt>
<figure>
<graphic graphicname="C201187E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0IZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0IZX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1660 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1660 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Telltale Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0043" fin="true">OK</down>
<right ref="RM000001W280AUX_08_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0004" proc-id="RM23G0E___0000FJ100001">
<testtitle>CHECK INSTRUMENT PANEL WIRE (CENTER AIRBAG SENSOR - TELLTALE LIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C214256E02" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Telltale Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Service Wire</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Telltale Light)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Center Airbag Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor and telltale light.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G5-7 (OFF) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G5-7 (OFF) - G1-17 (P-AB)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G5-7 (OFF) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0044" fin="true">OK</down>
<right ref="RM000001W280AUX_08_0060" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0067" proc-id="RM23G0E___0000FJ900001">
<testtitle>CHECK CONNECTION OF CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the center airbag sensor and telltale light.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0026" fin="false">OK</down>
<right ref="RM000001W280AUX_08_0063" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0026" proc-id="RM23G0E___0000FJ800001">
<testtitle>CHECK CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor and telltale light.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors (on the center airbag sensor side and telltale light side) are not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are not deformed or damaged.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="C208244E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Telltale Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0010" fin="false">OK</down>
<right ref="RM000001W280AUX_08_0050" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0010" proc-id="RM23G0E___0000FJ300001">
<testtitle>CHECK INSTRUMENT PANEL WIRE (CENTER AIRBAG SENSOR - TELLTALE LIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
<figure>
<graphic graphicname="C214256E02" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Telltale Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Service Wire</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Telltale Light)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Center Airbag Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G5-7 (OFF) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC4" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G5-7 (OFF) - G1-17 (P-AB)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G5-7 (OFF) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0014" fin="false">OK</down>
<right ref="RM000001W280AUX_08_0050" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0014" proc-id="RM23G0E___0000FJ400001">
<testtitle>CHECK PASSENGER AIRBAG CUTOFF INDICATOR (SOURCE VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C217348E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the connectors to the center airbag sensor.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G5-5 (LAPB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Telltale Light)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0015" fin="false">OK</down>
<right ref="RM000001W280AUX_08_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0015" proc-id="RM23G0E___0000FJ500001">
<testtitle>CHECK PASSENGER AIRBAG CUTOFF INDICATOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C213932E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Connect the connector to the telltale light.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check the indicator according to the value(s) in the table below.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Airbag Cutoff Indicator</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G1-17 (P-AB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Indicator comes on</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Center Airbag Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0016" fin="false">OK</down>
<right ref="RM000001W280AUX_08_0055" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0016" proc-id="RM23G0E___0000FJ600001">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C201187E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Connect the connectors to the center airbag sensor.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0IZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0IZX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1660 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1660 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Telltale Light</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001W280AUX_08_0022" fin="true">OK</down>
<right ref="RM000001W280AUX_08_0056" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W280AUX_08_0017">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0018">
<testtitle>REPLACE INSTRUMENT PANEL WIRE</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0019">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000003YQB00UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0043">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0INX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0044">
<testtitle>REPLACE TELLTALE LIGHT ASSEMBLY<xref label="Seep01" href="RM000003YR000ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0060">
<testtitle>REPLACE INSTRUMENT PANEL WIRE</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0063">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0020">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (TELLTALE LIGHT - BATTERY) OR BATTERY</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0055">
<testtitle>REPLACE TELLTALE LIGHT ASSEMBLY<xref label="Seep01" href="RM000003YR000ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0056">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000003YQB00UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0022">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0INX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001W280AUX_08_0050">
<testtitle>REPLACE INSTRUMENT PANEL WIRE</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>