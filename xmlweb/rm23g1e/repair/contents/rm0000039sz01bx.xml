<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12039_S001N" variety="S001N">
<name>STEERING GEAR / LINKAGE</name>
<ttl id="12039_S001N_7B98Y_T00IM" variety="T00IM">
<name>STEERING GEAR</name>
<para id="RM0000039SZ01BX" category="A" type-id="80001" name-id="SL0F4-01" from="201207">
<name>REMOVAL</name>
<subpara id="RM0000039SZ01BX_03" type-id="11" category="10" proc-id="RM23G0E___0000BH300000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039SZ01BX_02" type-id="01" category="01">
<s-1 id="RM0000039SZ01BX_02_0001">
<ptxt>PLACE FRONT WHEELS FACING STRAIGHT AHEAD</ptxt>
</s-1>
<s-1 id="RM0000039SZ01BX_02_0004">
<ptxt>REMOVE FRONT WHEELS</ptxt>
</s-1>
<s-1 id="RM0000039SZ01BX_02_0039" proc-id="RM23G0E___0000BH100000">
<ptxt>REMOVE ENGINE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>Remove the engine (See page <xref label="Seep01" href="RM000002B5L018X"/>).</ptxt>
</s2>
<s2>
<ptxt>for 1KD-FTV:</ptxt>
<ptxt>Remove the engine (See page <xref label="Seep02" href="RM0000045F800KX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 5L-E:</ptxt>
<ptxt>Remove the engine (See page <xref label="Seep03" href="RM000000FYL013X"/>).</ptxt>
</s2>
<s2>
<ptxt>for 2TR-FE:</ptxt>
<ptxt>Remove the engine (See page <xref label="Seep04" href="RM000004637009X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SZ01BX_02_0008" proc-id="RM23G0E___0000BGW00000">
<ptxt>DISCONNECT NO. 2 STEERING INTERMEDIATE SHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Manual Tilt and Manual Telescopic Steering Column:</ptxt>
<ptxt>Disconnect the No. 2 steering intermediate shaft (See page <xref label="Seep01" href="RM0000039SI01RX"/>).</ptxt>
</s2>
<s2>
<ptxt>for Power Tilt and Power Telescopic Steering Column:</ptxt>
<ptxt>Disconnect the No. 2 steering intermediate shaft (See page <xref label="Seep02" href="RM0000039SI01SX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SZ01BX_02_0032" proc-id="RM23G0E___0000BH000000">
<ptxt>DISCONNECT TIE ROD END SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, disconnect the tie rod end assembly.</ptxt>
<sst>
<sstitem>
<s-number>09610-20012</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C215754E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SZ01BX_02_0010" proc-id="RM23G0E___0000BGX00000">
<ptxt>DISCONNECT TIE ROD END SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedures described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000039SZ01BX_02_0011" proc-id="RM23G0E___0000BGY00000">
<ptxt>DISCONNECT PRESSURE FEED TUBE ASSEMBLY (for LHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clamp and disconnect the pressure feed tube (return tube side) from the steering gear.</ptxt>
<figure>
<graphic graphicname="C215750" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a union nut wrench, disconnect the pressure feed tube (pressure feed tube side) from the steering gear.</ptxt>
<figure>
<graphic graphicname="C215751" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the pressure feed tube clamp.</ptxt>
<figure>
<graphic graphicname="C215753E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1GR-FE:</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1KD-FTV:</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 2TR-FE:</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*D</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 5L-E:</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SZ01BX_02_0045" proc-id="RM23G0E___0000BH200000">
<ptxt>DISCONNECT PRESSURE FEED TUBE ASSEMBLY (for RHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clamp and disconnect the pressure feed tube (return tube side) from the steering gear.</ptxt>
<figure>
<graphic graphicname="C218256" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a union nut wrench, disconnect the pressure feed tube (pressure feed tube side) from the steering gear.</ptxt>
<figure>
<graphic graphicname="C218257" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the pressure feed tube clamp.</ptxt>
<figure>
<graphic graphicname="C215752E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1GR-FE:</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1KD-FTV:</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 2TR-FE:</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*D</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 5L-E:</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SZ01BX_02_0012" proc-id="RM23G0E___0000BGZ00000">
<ptxt>REMOVE RACK AND PINION POWER STEERING GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts, 2 nuts and steering gear assembly.</ptxt>
<figure>
<graphic graphicname="C215749" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Because the nut has its own stopper, do not turn the nut. Tighten the bolt with the nut fixed in place.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>