<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000Q" variety="S000Q">
<name>2TR-FE COOLING</name>
<ttl id="12011_S000Q_7B91G_T00B4" variety="T00B4">
<name>WATER PUMP</name>
<para id="RM000000V25019X" category="A" type-id="30014" name-id="CO5AJ-02" from="201207">
<name>INSTALLATION</name>
<subpara id="RM000000V25019X_02" type-id="01" category="01">
<s-1 id="RM000000V25019X_02_0072" proc-id="RM23G0E___00006JX00000">
<ptxt>INSTALL WATER PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A223314E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install a new gasket, and then install the water pump with the 10 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>265</t-value2>
<t-value4>19</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>8.9</t-value1>
<t-value2>91</t-value2>
<t-value3>79</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000000V25019X_02_0098" proc-id="RM23G0E___000057B00000">
<ptxt>INSTALL V-RIBBED BELT TENSIONER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the belt tensioner with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="G035081E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Make sure that the flanges of the bolts are contacting the tensioner surface.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the tensioner by tightening the 3 bolts in the order shown in the illustration.</ptxt>
<torque>
<subtitle>for bolt 1</subtitle>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
<subtitle>for bolt 2</subtitle>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
<subtitle>for bolt 3</subtitle>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0099" proc-id="RM23G0E___00006V400000">
<ptxt>INSTALL NO. 1 IDLER PULLEY SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the spacer and idler pulley with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0091" proc-id="RM23G0E___00006V700000">
<ptxt>INSTALL GENERATOR ASSEMBLY (for 80A Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the generator with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the generator connector.</ptxt>
</s2>
<s2>
<ptxt>Install the generator wire with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the terminal cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0092" proc-id="RM23G0E___0000D5F00000">
<ptxt>INSTALL GENERATOR ASSEMBLY (for 100A Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the generator with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>43</t-value1>
<t-value2>438</t-value2>
<t-value4>32</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the generator wire to terminal B with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>9.8</t-value1>
<t-value2>100</t-value2>
<t-value3>87</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Connect the generator connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0087" proc-id="RM23G0E___00005AL00000">
<ptxt>INSTALL FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fan pulley to the water pump.</ptxt>
</s2>
<s2>
<ptxt>Place the shroud together with the coupling fan between the radiator and engine.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the fluid coupling fan to the water pump with the 4 nuts. Tighten the nuts as much as possible by hand.</ptxt>
</s2>
<s2>
<ptxt>Attach the claws of the shroud to the radiator as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A223328" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the shroud with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the fan and generator V-belt (See page <xref label="Seep01" href="RM000000YMN01ZX"/>).</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 nuts of the fluid coupling fan.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="A223329" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the claw to close the flexible hose clamp as shown in the illustration.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0093" proc-id="RM23G0E___000033O00000">
<ptxt>INSTALL AIR CLEANER AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the air cleaner and hose, align its matchmark with the matchmark of the air cleaner cap as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A246524E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Matchmark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Upper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Attach the 3 clamps and connect the mass air flow meter connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0088" proc-id="RM23G0E___00005AM00000">
<ptxt>INSTALL RADIATOR RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator reservoir with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the reservoir hose to the radiator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0094" proc-id="RM23G0E___000033M00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the cylinder block drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 vinyl hoses.</ptxt>
</s2>
<s2>
<ptxt>Add engine coolant.</ptxt>
<spec>
<title>Standard Capacity</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>for Automatic Transmission</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry>
<ptxt>8.1 liters (8.6 US qts, 7.1 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry>
<ptxt>9.9 liters (10.5 US qts, 8.7 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>for Manual Transmission</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry>
<ptxt>8.3 liters (8.8 US qts, 7.3 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry>
<ptxt>10.1 liters (10.7 US qts, 8.9 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</item>
<item>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the F line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.*1</ptxt>
</s2>
<s2>
<ptxt>Start the engine and stop it immediately.*2</ptxt>
</s2>
<s2>
<ptxt>Allow approximately 10 seconds to pass. Then remove the radiator cap and check the coolant level. If the coolant level has decreased, add coolant.*3</ptxt>
</s2>
<s2>
<ptxt>Repeat steps *1, *2 and *3 until the coolant level does not decrease.</ptxt>
<atten4>
<ptxt>Be sure to perform this step while the engine is cold, as air in the No. 1 radiator hose will flow into the radiator if the engine is warmed up and the thermostat opens.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the radiator cap.*4</ptxt>
</s2>
<s2>
<ptxt>Set the air conditioning as follows.*5</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Fan speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Any setting except off</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Toward WARM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Air conditioning switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Start the engine, warm it up until the thermostat opens, and then continue to run the engine for several minutes to circulate the coolant.*6</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Hot areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful of the fan.</ptxt>
</item>
<item>
<ptxt>Be careful as the engine, radiator and radiator hoses are hot and can cause burns.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Do not start the engine when there is no coolant in the radiator reservoir.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the engine coolant temperature receiver gauge. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air while warming up the engine.</ptxt>
</item>
<item>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand and checking when the engine coolant starts to flow inside the hose.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Stop the engine, wait until the engine coolant cools down to ambient temperature. Then remove the radiator cap and check the coolant level.*7</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>If the coolant level has decreased, add coolant and warm up the engine until the thermostat opens.*8</ptxt>
</s2>
<s2>
<ptxt>If the coolant level has not decreased, check that the coolant level in the radiator reservoir is at the F line.</ptxt>
<ptxt>If the coolant level is below the F line, repeat steps *4 through *8.</ptxt>
<ptxt>If the coolant level is above the F line, drain coolant until the coolant level reaches the F line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0095" proc-id="RM23G0E___000033N00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and then check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hose, radiator and water pump for leakage. If no external leakage is found, check the heater core, cylinder block and cylinder head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0100" proc-id="RM23G0E___00005AB00000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Hook the engine under cover to the vehicle body as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0101" proc-id="RM23G0E___00005AC00000">
<ptxt>INSTALL FRONT BUMPER COVER LOWER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front bumper cover lower with the 5 bolts and clip.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000V25019X_02_0102" proc-id="RM23G0E___00005A800000">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the upper radiator support seal with the 13 clips.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>