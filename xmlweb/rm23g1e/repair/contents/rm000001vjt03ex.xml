<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002A" variety="S002A">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002A_7B9G3_T00PR" variety="T00PR">
<name>SLIDING ROOF SYSTEM</name>
<para id="RM000001VJT03EX" category="J" type-id="304XH" name-id="RF15Z-02" from="201207">
<dtccode/>
<dtcname>Sliding Roof ECU Power Source Circuit</dtcname>
<subpara id="RM000001VJT03EX_04" type-id="60" category="03" proc-id="RM23G0E___0000IJK00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the sliding function and tilt function do not operate, there may be a malfunction in the sliding roof drive gear sub-assembly power source circuit.</ptxt>
</content5>
</subpara>
<subpara id="RM000001VJT03EX_02" type-id="32" category="03" proc-id="RM23G0E___0000IJI00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B243268E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001VJT03EX_03" type-id="51" category="05" proc-id="RM23G0E___0000IJJ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When the sliding roof drive gear sub-assembly is removed and reinstalled or replaced, the sliding roof drive gear sub-assembly must be initialized (See page <xref label="Seep01" href="RM000001VJR065X"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001VJT03EX_01" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001VJT03EX_01_0003" proc-id="RM23G0E___0000IJH00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SLIDING ROOF ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="B243668E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the W2 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>W2-2 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>Standard Voltage</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>W2-1 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Sliding Roof Drive Gear Sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001VJT03EX_01_0012" fin="true">OK</down>
<right ref="RM000001VJT03EX_01_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001VJT03EX_01_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001VJT03EX_01_0012">
<testtitle>REPLACE SLIDING ROOF DRIVE GEAR SUB-ASSEMBLY<xref label="Seep01" href="RM000002LPC01YX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>