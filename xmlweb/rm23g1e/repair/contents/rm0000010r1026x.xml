<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12018_S0014" variety="S0014">
<name>CLUTCH</name>
<ttl id="12018_S0014_7B94M_T00EA" variety="T00EA">
<name>CLUTCH PEDAL (for LHD)</name>
<para id="RM0000010R1026X" category="N" type-id="3000G" name-id="CL018-21" from="201207">
<name>ADJUSTMENT</name>
<subpara id="RM0000010R1026X_01" type-id="01" category="01">
<s-1 id="RM0000010R1026X_01_0001" proc-id="RM23G0E___00008D200000">
<ptxt>INSPECT AND ADJUST CLUTCH PEDAL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fold back the floor carpet.</ptxt>
</s2>
<s2>
<ptxt>Check that the pedal height is correct.</ptxt>
<figure>
<graphic graphicname="F050836E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Pedal height from floor</title>
<specitem>
<ptxt>172.9 to 182.9 mm (6.81 to 7.20 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pedal Height Adjustment Point</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Push Rod Play and Free Play Adjustment Point</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pedal Height</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Adjust the pedal height. </ptxt>
<s3>
<ptxt>w/ Cruise Control:</ptxt>
<ptxt>Loosen the lock nut and turn the clutch switch until the height is correct. Tighten the lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>160</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>w/o Cruise Control:</ptxt>
<ptxt>Loosen the lock nut and turn the clutch pedal stopper bolt until the height is correct. Tighten the lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>260</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>Check the pedal free play and push rod play.</ptxt>
<figure>
<graphic graphicname="D031462E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pedal Free Play</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Push Rod Play</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s3>
<ptxt>Depress the clutch pedal until resistance is felt.</ptxt>
</s3>
<s3>
<ptxt>Measure the distance between the pedal's released position and the position in the previous step.</ptxt>
<spec>
<title>Pedal free play</title>
<specitem>
<ptxt>5.0 to 15.0 mm (0.197 to 0.591 in.)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Release the pedal. Using your finger, gently press the pedal until resistance increases slightly.</ptxt>
</s3>
<s3>
<ptxt>Measure the distance between the pedal's released position and the position in the previous step.</ptxt>
<spec>
<title>Push rod play at pedal top</title>
<specitem>
<ptxt>1.0 to 5.0 mm (0.0394 to 0.197 in.)</ptxt>
</specitem>
</spec>
</s3>
</s2>
<s2>
<ptxt>Adjust the pedal free play and push rod play.</ptxt>
<s3>
<ptxt>Loosen the lock nut and turn the push rod until the pedal free play and push rod play are within the specified ranges.</ptxt>
</s3>
<s3>
<ptxt>Tighten the lock nut.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Check that the pedal height is correct.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the clutch release point.</ptxt>
<figure>
<graphic graphicname="CL00512E24" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Full Stroke End Position</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Release Position</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>25 mm or more</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s3>
<ptxt>Pull the parking brake lever and use wheel chocks to stabilize the vehicle.</ptxt>
</s3>
<s3>
<ptxt>Start the engine and run it at idle.</ptxt>
</s3>
<s3>
<ptxt>Without depressing the clutch pedal, slowly move the shift lever to R until the gears contact.</ptxt>
</s3>
<s3>
<ptxt>Gently depress the clutch pedal and measure the stroke distance from the point that the gear noise stops (release point) up to the full stroke end position.</ptxt>
<spec>
<title>Standard distance</title>
<specitem>
<ptxt>25 mm (0.984 in.) or more </ptxt>
</specitem>
</spec>
<ptxt>If the result is not as specified, perform the following procedures.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Check pedal height.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Check push rod play and pedal free play.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Bleed air from clutch line.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Check clutch cover and disc.</ptxt>
</item>
</list1>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>