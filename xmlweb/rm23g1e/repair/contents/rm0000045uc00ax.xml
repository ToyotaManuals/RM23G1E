<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM0000045UC00AX" category="C" type-id="804R2" name-id="BCDON-01" from="201207" to="201210">
<dtccode>C1282</dtccode>
<dtcname>Center Differential Lock Position Switch</dtcname>
<subpara id="RM0000045UC00AX_01" type-id="60" category="03" proc-id="RM23G0E___0000A6S00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>DTC C1282 is stored only in test mode.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.21in"/>
<colspec colname="COL2" colwidth="2.55in"/>
<colspec colname="COL3" colwidth="3.32in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1282</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stored during test mode.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Transfer system</ptxt>
</item>
<item>
<ptxt>Brake actuator assembly (Skid control ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000045UC00AX_02" type-id="32" category="03" proc-id="RM23G0E___0000A6T00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C212546E03" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000045UC00AX_03" type-id="51" category="05" proc-id="RM23G0E___0000A6U00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08IX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000045UC00AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000045UC00AX_04_0001" proc-id="RM23G0E___0000A6V00000">
<testtitle>INSPECT CENTER DIFFERENTIAL LOCK SHIFT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the center differential can shift from free to lock, and from lock to free.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The center differential can shift from free to lock, and from lock to free.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000045UC00AX_04_0002" fin="false">OK</down>
<right ref="RM0000045UC00AX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000045UC00AX_04_0002" proc-id="RM23G0E___0000A6W00000">
<testtitle>CHECK TEST MODE DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Switch the vehicle to test mode, perform the center differential lock signal check, and then check that DTC C1282 is cleared (See page <xref label="Seep01" href="RM000000XHT0ARX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000045UC00AX_04_0007" fin="true">A</down>
<right ref="RM0000045UC00AX_04_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000045UC00AX_04_0003" proc-id="RM23G0E___0000A6X00000">
<testtitle>CHECK TERMINAL VOLTAGE (EXI4)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Set the vehicle in the center differential locked state using the center differential lock switch.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C199357E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A6-9 (EXI4) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center differential locked (Center differential lock switch on)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Connect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Set the vehicle in the center differential free state using the center differential lock switch.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A6-9 (EXI4) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center differential free (Center differential lock switch off)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000045UC00AX_04_0004" fin="false">OK</down>
<right ref="RM0000045UC00AX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000045UC00AX_04_0004" proc-id="RM23G0E___0000A6Y00000">
<testtitle>CHECK TEST MODE DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Switch the vehicle to test mode, perform the center differential lock signal check, and then check that DTC C1282 is cleared (See page <xref label="Seep01" href="RM000000XHT0ARX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is cleared</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000045UC00AX_04_0007" fin="true">A</down>
<right ref="RM0000045UC00AX_04_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000045UC00AX_04_0005" proc-id="RM23G0E___0000A6Z00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - FOUR WHEEL DRIVE CONTROL ECU/TRANSFER SHIFT ACTUATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G53 four wheel drive control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C43 transfer shift actuator connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
</test1>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.14in"/>
<colspec colname="COL3" colwidth="1.21in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A6-9 (EXI4) - G53-14 (P1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A6-9 (EXI4) - C43-9 (CDL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A6-9 (EXI4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</content6>
<res>
<down ref="RM0000045UC00AX_04_0009" fin="true">OK</down>
<right ref="RM0000045UC00AX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000045UC00AX_04_0006">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000045UC00AX_04_0007">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01JX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000045UC00AX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000045UC00AX_04_0009">
<testtitle>GO TO TRANSFER SYSTEM (PROBLEM SYMPTOMS TABLE)<xref label="Seep01" href="RM000003BB700KX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>