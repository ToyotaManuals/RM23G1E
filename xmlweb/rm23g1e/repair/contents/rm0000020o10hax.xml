<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001P" variety="S001P">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001P_7B99K_T00J8" variety="T00J8">
<name>NAVIGATION SYSTEM (for HDD)</name>
<para id="RM0000020O10HAX" category="J" type-id="303DF" name-id="NS71E-03" from="201210">
<dtccode/>
<dtcname>Reverse Signal Circuit</dtcname>
<subpara id="RM0000020O10HAX_01" type-id="60" category="03" proc-id="RM23G0E___0000C8E00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The display and navigation module display receives a reverse signal from the park/neutral position switch assembly*1 or back-up light switch assembly*2 and information about the GPS antenna, and then adjusts the vehicle position.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for Automatic Transmission</ptxt>
</item>
<item>
<ptxt>*2: for Manual Transmission</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000020O10HAX_02" type-id="32" category="03" proc-id="RM23G0E___0000C8F00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E201422E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000020O10HAX_03" type-id="51" category="05" proc-id="RM23G0E___0000C8G00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000020O10HAX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000020O10HAX_05_0017" proc-id="RM23G0E___0000C8K00001">
<testtitle>CHECK DISPLAY AND NAVIGATION MODULE DISPLAY (DISPLAY CHECK MODE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter the "Display Check" mode and select "Vehicle Signal Check" (See page <xref label="Seep01" href="RM0000044IE00HX"/>).</ptxt>
<figure>
<graphic graphicname="I038223E17" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Check that the display changes between ON and OFF according to the shift lever operation.</ptxt>
<table>
<title>OK</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>This display is updated once per second. As a result, it is normal for the display to lag behind the actual shift lever operation.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000020O10HAX_05_0018" fin="true">OK</down>
<right ref="RM0000020O10HAX_05_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0001" proc-id="RM23G0E___0000C8H00001">
<testtitle>CHECK DISPLAY AND NAVIGATION MODULE DISPLAY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E140413E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>H4-16 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H4-16 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in any position except R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Display and Navigation Module Display)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for Automatic Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for Manual Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000020O10HAX_05_0005" fin="true">A</down>
<right ref="RM0000020O10HAX_05_0002" fin="false">B</right>
<right ref="RM0000020O10HAX_05_0020" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0002" proc-id="RM23G0E___0000C8I00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
<figure>
<graphic graphicname="E146355E08" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-2 (RB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Park/Neutral Position Switch Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000020O10HAX_05_0003" fin="false">OK</down>
<right ref="RM0000020O10HAX_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0003" proc-id="RM23G0E___0000C8J00001">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
<figure>
<graphic graphicname="E179071E11" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 (RL) - 2 (RB)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift lever in P</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for A750F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for A343F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000020O10HAX_05_0022" fin="true">A</down>
<right ref="RM0000020O10HAX_05_0013" fin="true">B</right>
<right ref="RM0000020O10HAX_05_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0020" proc-id="RM23G0E___0000C8L00001">
<testtitle>CHECK HARNESS AND CONNECTOR (BACK-UP LIGHT SWITCH - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C41 back-up light switch assembly connector.</ptxt>
<figure>
<graphic graphicname="E195965E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C41-2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Back-up Light Switch Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000020O10HAX_05_0021" fin="false">OK</down>
<right ref="RM0000020O10HAX_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0021" proc-id="RM23G0E___0000C8M00001">
<testtitle>INSPECT BACK-UP LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C41 back-up light switch assembly connector.</ptxt>
<figure>
<graphic graphicname="E179072" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift lever in P</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for RA61F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for R150F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for G52F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000020O10HAX_05_0022" fin="true">A</down>
<right ref="RM0000020O10HAX_05_0015" fin="true">B</right>
<right ref="RM0000020O10HAX_05_0016" fin="true">C</right>
<right ref="RM0000020O10HAX_05_0019" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0018">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000046SY00JX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0005">
<testtitle>REPLACE DISPLAY AND NAVIGATION MODULE DISPLAY<xref label="Seep01" href="RM000003B6H01QX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0013">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06GX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0014">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06FX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0015">
<testtitle>REPLACE BACK-UP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000047AR002X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0016">
<testtitle>REPLACE BACK-UP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000046SW00MX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0019">
<testtitle>REPLACE BACK-UP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM00000471D00EX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000020O10HAX_05_0022">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (DISPLAY AND NAVIGATION MODULE DISPLAY - SWITCH)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>