<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B9_T00KX" variety="T00KX">
<name>ENGINE IMMOBILISER SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000TUF0ATX" category="C" type-id="800L9" name-id="TD3A4-02" from="201210">
<dtccode>B278A</dtccode>
<dtcname>Short to GND in Immobiliser System Power Source Circuit</dtcname>
<subpara id="RM000000TUF0ATX_01" type-id="60" category="03" proc-id="RM23G0E___0000EZM00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when the engine switch power source supply line is open or shorted.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B278A</ptxt>
</entry>
<entry valign="middle">
<ptxt>The engine switch power source supply line is open or shorted.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Engine switch</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Certification ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TUF0ATX_02" type-id="32" category="03" proc-id="RM23G0E___0000EZN00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B153714E21" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TUF0ATX_03" type-id="51" category="05" proc-id="RM23G0E___0000EZO00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Before replacing the certification ECU, refer to the Service Bulletin.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000TUF0ATX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TUF0ATX_05_0001" proc-id="RM23G0E___0000EZP00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU - ENGINE SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G38 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G36 switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G38-28 (VC5) - G36-14 (VC5)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G38-36 (AGND) - G36-8 (AGND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G38-28 (VC5) or G36-14 (VC5) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G38-36 (AGND) or G36-8 (AGND) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TUF0ATX_05_0002" fin="false">OK</down>
<right ref="RM000000TUF0ATX_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TUF0ATX_05_0002" proc-id="RM23G0E___0000EZQ00001">
<testtitle>CHECK CERTIFICATION ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="B226518E03" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>G36-14 (VC5) - G36-8 (AGND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>2 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, key not in cabin, and 30 seconds or less after engine switch pushed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is output normally (refer to illustration).</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Component with harness connected</ptxt>
<ptxt>(Engine Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for 1GR-FE [for LHD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for 1GR-FE [for RHD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for 1KD-FTV [for LHD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for 1KD-FTV [for RHD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TUF0ATX_05_0009" fin="true">A</down>
<right ref="RM000000TUF0ATX_05_0014" fin="true">B</right>
<right ref="RM000000TUF0ATX_05_0015" fin="true">C</right>
<right ref="RM000000TUF0ATX_05_0016" fin="true">D</right>
<right ref="RM000000TUF0ATX_05_0003" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000000TUF0ATX_05_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000TUF0ATX_05_0003">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000000TUF0ATX_05_0009">
<testtitle>REPLACE ENGINE SWITCH<xref label="Seep01" href="RM000002YWC02ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TUF0ATX_05_0014">
<testtitle>REPLACE ENGINE SWITCH<xref label="Seep01" href="RM000002YWC030X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TUF0ATX_05_0015">
<testtitle>REPLACE ENGINE SWITCH<xref label="Seep01" href="RM000002YWC031X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TUF0ATX_05_0016">
<testtitle>REPLACE ENGINE SWITCH<xref label="Seep01" href="RM000002YWC032X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>