<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12009_S000H" variety="S000H">
<name>1KD-FTV EMISSION CONTROL</name>
<ttl id="12009_S000H_7B8ZX_T009L" variety="T009L">
<name>EGR VALVE (w/ DPF)</name>
<para id="RM000004KSH004X" category="A" type-id="30014" name-id="EC3O6-04" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000004KSH004X_04" type-id="11" category="10" proc-id="RM23G0E___000064G00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipes and fuel inlet pipe, clean them with a brush and compressed air.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM000004KSH004X_03" type-id="01" category="01">
<s-1 id="RM000004KSH004X_03_0013" proc-id="RM23G0E___000063Y00001">
<ptxt>INSTALL ELECTRIC EGR CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the electric EGR control valve to the intake air connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the electric EGR control valve connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004KSH004X_03_0039" proc-id="RM23G0E___000064100001">
<ptxt>TEMPORARILY INSTALL EGR COOLER WITH NO. 2 EGR VALVE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the stud bolts of the intake air connector.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket to the stud bolts of the cylinder head.</ptxt>
</s2>
<s2>
<ptxt>Install the EGR valve adapter to the electric EGR control valve and intake air connector with the 2 nuts and bolt labeled A.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the EGR cooler to the intake manifold with the 2 bolts labeled B in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install the EGR cooler to the cylinder head with the 2 nuts labeled C in the illustration.</ptxt>
<figure>
<graphic graphicname="A239656E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>New Gasket</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR Valve Adapter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>EGR Cooler</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Electric EGR Control Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Air Connector</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0040" proc-id="RM23G0E___000064200001">
<ptxt>TIGHTEN EGR COOLER WITH NO. 2 EGR VALVE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the 2 bolts labeled A in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 2 nuts labeled B in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the air connector stay with the 3 bolts labeled C in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 2 nuts and bolt labeled D in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the No. 1 fuel pipe to the air connector stay with the bolt labeled E.</ptxt>
<torque>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A239657E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Connector Stay</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Fuel Pipe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Connect the fuel pressure sensor connector to the common rail.</ptxt>
</s2>
<s2>
<ptxt>Connect the 4 injector connectors and attach the 3 wire harness clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0041" proc-id="RM23G0E___000064300001">
<ptxt>INSTALL NO. 4 ENGINE COVER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 4 engine cover bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0042" proc-id="RM23G0E___000064400001">
<ptxt>INSTALL ENGINE COVER BRACKET INSULATOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 engine cover bracket insulators with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A239398E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Claw</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure that the claws of the engine cover bracket insulators contact the engine cover brackets.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0043" proc-id="RM23G0E___000064500001">
<ptxt>INSTALL NO. 3 ENGINE COVER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 engine cover bracket with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>7.5</t-value1>
<t-value2>76</t-value2>
<t-value3>66</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0044" proc-id="RM23G0E___000064600001">
<ptxt>INSTALL WIRING HARNESS CLAMP BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the wiring harness clamp bracket.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 2 wire harness clamps and connect the glow plug connector to the wiring harness clamp bracket.</ptxt>
</s2>
<s2>
<ptxt>Connect the glow plug connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0045" proc-id="RM23G0E___000064700001">
<ptxt>INSTALL NO. 1 VACUUM TRANSMITTING PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 vacuum transmitting pipe with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>178</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the vacuum hose to the intake manifold.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0046" proc-id="RM23G0E___000064800001">
<ptxt>INSTALL WATER BY-PASS HOSE
</ptxt>
<content1 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>When connecting the hoses, make sure the paint marks and clips are as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>The direction of each hose clamp is indicated in the illustration.</ptxt>
</item>
</list1>
</atten4>
<s2>
<ptxt>Connect the No. 5 water by-pass hose labeled A in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 6 water by-pass hose labeled B in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 8 water by-pass hose labeled C in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 3 water by-pass hose labeled D in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 4 water by-pass hose labeled E in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 7 water by-pass hose labeled F in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 water by-pass hose clamps.</ptxt>
<figure>
<graphic graphicname="A239675E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upper Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>LH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0047" proc-id="RM23G0E___000064900001">
<ptxt>CONNECT HEATER WATER PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the heater water pipe with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>260</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the water by-pass hose clamp.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0048" proc-id="RM23G0E___000064A00001">
<ptxt>INSTALL NO. 1, NO. 2 AND NO. 3 INJECTION PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing an injector, it is necessary to replace the 4 injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>Keep the joints of the injection pipe clean.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Temporarily install the No. 1, No. 2 and No. 3 injection pipes with the union nuts.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 No. 2 injection pipe clamps with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the No. 3 injection pipe clamp with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, tighten the injection pipe union nuts on the common rail side.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, tighten the injection pipe union nuts on the injector side.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0049" proc-id="RM23G0E___000064B00001">
<ptxt>INSTALL FUEL INLET PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the fuel inlet pipe with the union nuts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the fuel supply pump, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>Keep the fuel inlet pipe free of foreign matter.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the No. 2 injection pipe clamp with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, tighten the fuel inlet pipe union nut on the common rail side.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, tighten the fuel inlet pipe union nut on the fuel supply pump side.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0050" proc-id="RM23G0E___000064C00001">
<ptxt>INSTALL MANIFOLD STAY WITH VACUUM SWITCHING VALVE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the manifold stay with vacuum switching valve with the 2 bolts and connect the No. 4 vacuum transmitting hose and No. 1 vacuum transmitting hose.</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A244966E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>White Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Blue Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure the vacuum transmitting hose color matches the connection area color.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the 2 No. 2 vacuum transmitting hoses shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A245681E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yellow Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 EGR Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Make sure that the vacuum transmitting hose with the yellow mark is connected to the side of the No. 2 EGR valve that has a yellow mark as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Push on the vacuum transmitting hose until it reaches the bent part of the pipe.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Connect the No. 1 vacuum transmitting hose.</ptxt>
</s2>
<s2>
<ptxt>Connect the 3 vacuum switching valve connectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0051" proc-id="RM23G0E___000064D00001">
<ptxt>CONNECT VANE PUMP OIL RESERVOIR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the vane pump oil reservoir with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A239651E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Tighten the 3 bolts of the vane pump oil reservoir in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0052" proc-id="RM23G0E___00005IH00001">
<ptxt>INSTALL INJECTOR DRIVER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the injector driver assembly with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 4 connectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0053" proc-id="RM23G0E___000064E00001">
<ptxt>INSTALL FRONT FENDER APRON SEAL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front fender apron seal LH with the 5 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0029" proc-id="RM23G0E___0000JLC00001">
<ptxt>INSTALL FRONT WHEEL LH</ptxt>
<content1 releasenbr="1">
<torque>
<torqueitem>
<t-value1>112</t-value1>
<t-value2>1137</t-value2>
<t-value4>82</t-value4>
</torqueitem>
</torque>
</content1>
</s-1>
<s-1 id="RM000004KSH004X_03_0030" proc-id="RM23G0E___000063Z00001">
<ptxt>INSTALL DIESEL THROTTLE BODY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000013ZB01VX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000004KSH004X_03_0031" proc-id="RM23G0E___000064000001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000004KSH004X_03_0054" proc-id="RM23G0E___000032400000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
<s2>
<ptxt>Tighten the cylinder block drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Fill the radiator with TOYOTA Super Long Life Coolant (SLLC) to the B line of the reservoir tank.</ptxt>
<spec>
<title>Standard Capacity</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>for Automatic Transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>14.9 liters (15.7 US qts, 13.1 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>13.1 liters (13.8 US qts, 11.5 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>for Manual Transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>15.0 liters (15.8 US qts, 13.2 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>13.2 liters (13.9 US qts, 11.6 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</item>
<item>
<ptxt>Please contact your TOYOTA dealer for further details.</ptxt>
</item>
<item>
<ptxt>for Cold Area Specification Vehicles:</ptxt>
<ptxt>Please contact any authorized TOYOTA dealer or repairer or another duly qualified and equipped professional for further details.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<ptxt>Never use water as a substitute for engine coolant.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 inlet and No. 1 and No. 2 outlet radiator hoses several times by hand, and then check the level of the coolant.</ptxt>
<ptxt>If the coolant level drops below the B line, add TOYOTA SLLC to the B line.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Using a wrench, install the vent plug.</ptxt>
<torque>
<torqueitem>
<t-value1>2.0</t-value1>
<t-value2>20</t-value2>
<t-value3>18</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed air from the cooling system.</ptxt>
<s3>
<ptxt>Warm up the engine until the thermostat opens. While the thermostat is open, circulate the coolant for several minutes.</ptxt>
</s3>
<s3>
<ptxt>Maintain the engine speed at a speed between 2500 and 3000 rpm.</ptxt>
</s3>
<s3>
<ptxt>Press the inlet and outlet radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<ptxt>When pressing the radiator hoses:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the radiator fan.</ptxt>
</item>
</list1>
</atten2>
</s3>
<s3>
<ptxt>Stop the engine and wait until the coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s3>
</s2>
<s2>
<ptxt>After the coolant cools down, check that the coolant level is at the FULL line.</ptxt>
<ptxt>If the coolant level is below the FULL line, add TOYOTA SLLC to the FULL line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0055" proc-id="RM23G0E___00005PY00000">
<ptxt>BLEED AIR FROM FUEL SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using the hand pump mounted on the fuel filter cap, bleed air from the fuel system. Continue pumping until the pump resistance increases.</ptxt>
<figure>
<graphic graphicname="A223284" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The maximum hand pump pumping speed is 2 strokes per second.</ptxt>
</item>
<item>
<ptxt>The hand pump must be pushed with a full stroke during pumping.</ptxt>
</item>
<item>
<ptxt>When the fuel pressure at the supply pump inlet port reaches a saturated pressure, the hand pump resistance increases.</ptxt>
</item>
<item>
<ptxt>If pumping is interrupted during the air bleeding process, fuel in the fuel line may return to the fuel tank. Continue pumping until the hand pump resistance increases.</ptxt>
</item>
<item>
<ptxt>If the hand pump resistance does not increase despite consecutively pumping 200 times or more, there may be a fuel leak between the fuel tank and fuel filter, the hand pump may be malfunctioning, or the vehicle may have run out of fuel.</ptxt>
</item>
<item>
<ptxt>If air bleeding using the hand pump is incomplete, the common rail pressure does not rise to the pressure range necessary for normal use and the engine cannot be started.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Check if the engine starts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even if air bleeding using the hand pump has been completed, the starter may need to be cranked for 10 seconds or more to start the engine.</ptxt>
</item>
<item>
<ptxt>Do not crank the engine continuously for more than 20 seconds. The battery may be discharged.</ptxt>
</item>
<item>
<ptxt>Use a fully-charged battery.</ptxt>
</item>
</list1>
</atten3>
<s3>
<ptxt>When the engine can be started, proceed to the next step.</ptxt>
</s3>
<s3>
<ptxt>If the engine cannot be started, bleed air again using the hand pump until the hand pump resistance increases (refer to the procedures above). Then start the engine.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Turn the engine switch off.</ptxt>
</s2>
<s2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG) and turn the intelligent tester on.</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z7X"/>).</ptxt>
</s2>
<s2>
<ptxt>Start the engine.*1</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Test the Fuel Leak.*2</ptxt>
<figure>
<graphic graphicname="A194065E05" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reference</ptxt>
<ptxt>(Active Test Operation)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Perform the following test 5 times with on/off intervals of 10 seconds: Active Test / Test the Fuel Leak.*3</ptxt>
</s2>
<s2>
<ptxt>Allow the engine to idle for 3 minutes or more after performing the Active Test for the 5th time.</ptxt>
<figure>
<graphic graphicname="A192848E04" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>When the Active Test "Test the Fuel Leak" is used to change the pump control mode, the actual fuel pressure inside the common rail drops below the target fuel pressure when the Active Test is off, but this is normal and does not indicate a pump malfunction.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</s2>
<s2>
<ptxt>Read Current DTCs.</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK0Z7X"/>).</ptxt>
<atten4>
<ptxt>It is necessary to clear the DTCs as DTC P1604 or P1605 may be stored when air is bled from the fuel system after replacing or repairing fuel system parts.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Repeat steps *1 to *3.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</s2>
<s2>
<ptxt>Read Current DTCs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0056" proc-id="RM23G0E___00001CD00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Before each inspection, turn the A/C switch off.</ptxt>
</atten3>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leaks. If no external leaks are found, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0057" proc-id="RM23G0E___00005PX00000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Perform the Active Test.</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the engine switch on (IG).</ptxt>
</s3>
<s3>
<ptxt>Turn the intelligent tester on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</s3>
<s3>
<ptxt>Perform the Active Test.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Intelligent Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Test the Fuel Leak</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pressurize common rail interior and check for fuel leaks</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Stop/Start</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>The fuel pressure inside the common rail increases to the specified value and the engine speed increases to 2000 rpm when the Active Test is performed.</ptxt>
</item>
<item>
<ptxt>The above conditions are maintained while the Active Test is being performed.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0058" proc-id="RM23G0E___00004O800001">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0059" proc-id="RM23G0E___00004O900001">
<ptxt>INSTALL FRONT BUMPER LOWER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front bumper lower cover with the clip and 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000004KSH004X_03_0060" proc-id="RM23G0E___000064F00001">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the upper radiator support seal with the 13 clips.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>