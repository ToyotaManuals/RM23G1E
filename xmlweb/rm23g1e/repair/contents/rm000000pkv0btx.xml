<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12053_S001Z" variety="S001Z">
<name>LIGHTING (INT)</name>
<ttl id="12053_S001Z_7B9BM_T00LA" variety="T00LA">
<name>LIGHTING SYSTEM</name>
<para id="RM000000PKV0BTX" category="U" type-id="303FJ" name-id="LI042-95" from="201207">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000000PKV0BTX_z0" proc-id="RM23G0E___0000F6H00000">
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ACC SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ACC signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Ignition switch ACC</ptxt>
<ptxt>OFF: Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>IG SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Ignition switch ON</ptxt>
<ptxt>OFF: Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RR Door Courtesy SW*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear door courtesy light switch RH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear door RH open</ptxt>
<ptxt>OFF: Rear door RH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Door Courtesy SW*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear door courtesy light switch LH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear door LH open</ptxt>
<ptxt>OFF: Rear door LH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back door courtesy switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door open</ptxt>
<ptxt>OFF: Back door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Glass Hatch Courtesy Switch*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Glass hatch courtesy switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Glass hatch open</ptxt>
<ptxt>OFF: Glass hatch closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FR Door Courtesy</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door courtesy light switch RH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front door RH closed</ptxt>
<ptxt>OFF: Front door RH open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Door Courtesy</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door courtesy light switch LH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front door LH closed</ptxt>
<ptxt>OFF: Front door LH open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lounge Illumination Value SW (UP)*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Room light switch (up) signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Room light switch (up) on</ptxt>
<ptxt>OFF: Room light switch (up) off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lounge Illumination Value SW (Dwn)*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Room light switch (down) signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Room light switch (down) on</ptxt>
<ptxt>OFF: Room light switch (down) off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for 5 Door</ptxt>
</item>
<item>
<ptxt>*2: w/ Glass Hatch Opener System</ptxt>
</item>
<item>
<ptxt>*3: w/ Door Illumination</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Body / Main Body / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the intelligent tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Illuminated Entry System</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Map light, No. 1 room light and No. 2 room light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Step Light Operation*</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Interior foot light, inside handle illumination, ornament illumination and door pocket illumination</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Interior Panel Relay</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Interior panel relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Cent Consol Spot LGT</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Center console spot light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Dimmer Signal</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Illumination dimming</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Interior Foot Light</ptxt>
</item>
</list1>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>