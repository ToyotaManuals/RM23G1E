<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S0028" variety="S0028">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S0028_7B9FB_T00OZ" variety="T00OZ">
<name>FRONT CONSOLE BOX (for Automatic Transmission)</name>
<para id="RM0000046JD004X" category="A" type-id="80001" name-id="IT349-01" from="201207">
<name>REMOVAL</name>
<subpara id="RM0000046JD004X_01" type-id="11" category="10" proc-id="RM23G0E___0000HYZ00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000046JD004X_02" type-id="01" category="01">
<s-1 id="RM0000046JD004X_02_0017" proc-id="RM23G0E___00006YI00000">
<ptxt>REMOVE INTEGRATION CONTROL AND PANEL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E204227" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the integration control and panel assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JD004X_02_0002" proc-id="RM23G0E___00006YE00000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238132E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the No. 2 instrument panel finish panel cushion.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 5 clips and remove the No. 2 instrument panel finish panel cushion.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0003" proc-id="RM23G0E___00006YG00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL FINISH PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the No. 2 instrument panel finish panel cushion.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0004" proc-id="RM23G0E___00006YF00000">
<ptxt>REMOVE INSTRUMENT PANEL FINISH PANEL END LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238134" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips and remove the instrument panel finish panel end.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0005" proc-id="RM23G0E___00006YH00000">
<ptxt>REMOVE INSTRUMENT PANEL FINISH PANEL END RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238136" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the instrument panel finish panel end.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0006" proc-id="RM23G0E___00006YJ00000">
<ptxt>REMOVE FRONT CONSOLE UPPER PANEL GARNISH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238147E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the front console upper panel garnish.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 5 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and remove the front console upper panel garnish.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0007" proc-id="RM23G0E___00008CO00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238138" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>for LHD:</ptxt>
<s3>
<ptxt>Remove the screw.</ptxt>
</s3>
<s3>
<ptxt>Detach the 2 clips and 2 guides and remove the No. 1 instrument panel under cover.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B298554" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for RHD:</ptxt>
<s3>
<ptxt>Remove the screw.</ptxt>
</s3>
<s3>
<ptxt>Detach the 3 clips and 2 guides and remove the No. 1 instrument panel under cover.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0008" proc-id="RM23G0E___000034800000">
<ptxt>REMOVE FRONT NO. 1 CONSOLE BOX INSERT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238139" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 clips and guide and remove the front No. 1 console box insert.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0009" proc-id="RM23G0E___00007C000000">
<ptxt>REMOVE NO. 2 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 clips and 2 guides and remove the No. 2 instrument panel under cover.</ptxt>
<figure>
<graphic graphicname="B298555E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0010" proc-id="RM23G0E___0000HZ000000">
<ptxt>REMOVE FRONT NO. 2 CONSOLE BOX INSERT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238145" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 clips and guide and remove the front No. 2 console box insert.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0011" proc-id="RM23G0E___0000C2300000">
<ptxt>REMOVE SHIFT LEVER KNOB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238140E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Move the shifting hole cover downward.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shifting Hole Cover</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Twist the shift lever knob in the direction indicated by the arrow and remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0012" proc-id="RM23G0E___0000C2400000">
<ptxt>REMOVE CONSOLE PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238141E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the console panel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 8 clips and 2 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and remove the console panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0013" proc-id="RM23G0E___0000HZ100000">
<ptxt>REMOVE NO. 1 HEATER TO REGISTER DUCT SUB-ASSEMBLY (w/ Cool Box)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238153" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 clamps and remove the No. 1 heater to register duct.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0014" proc-id="RM23G0E___0000HZ200000">
<ptxt>REMOVE NO. 1 CONSOLE BOX DUCT (for Single Air Conditioning System)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238151" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip and No. 1 console box duct.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0015" proc-id="RM23G0E___0000C2900000">
<ptxt>REMOVE REAR CONSOLE END PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238142" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips and 4 claws.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and remove the rear console end panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JD004X_02_0016" proc-id="RM23G0E___0000HZ300000">
<ptxt>REMOVE REAR CONSOLE BOX ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238154" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws, clip and 5 bolts.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and remove the rear console box.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>