<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S001X" variety="S001X">
<name>DOOR LOCK</name>
<ttl id="12051_S001X_7B9AV_T00KJ" variety="T00KJ">
<name>REAR DOOR LOCK</name>
<para id="RM0000039H4020X" category="A" type-id="80001" name-id="DL6OD-02" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM0000039H4020X_03" type-id="11" category="10" proc-id="RM23G0E___0000ELO00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039H4020X_01" type-id="01" category="01">
<s-1 id="RM0000039H4020X_01_0016" proc-id="RM23G0E___0000ELI00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039H4020X_01_0018" proc-id="RM23G0E___0000BZO00000">
<ptxt>REMOVE NO. 2 DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using moulding remover A, detach the 3 claws and remove the rear door inside handle bezel as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239600" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0019" proc-id="RM23G0E___0000BZP00000">
<ptxt>REMOVE ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using moulding remover A, detach the 8 claws and remove the assist grip cover.</ptxt>
<figure>
<graphic graphicname="B239602" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0020" proc-id="RM23G0E___0000BZQ00000">
<ptxt>REMOVE REAR DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B239604" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Using a clip remover, detach the 9 clips.</ptxt>
</s2>
<s2>
<ptxt>Pull out the rear door trim board sub-assembly in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B239606" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Raise the rear door trim board sub-assembly to detach the 4 claws and remove the rear door trim board sub-assembly together with the rear door inner glass weatherstrip.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the rear door lock remote control cable assembly and rear door inside locking cable assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect each connector.</ptxt>
<figure>
<graphic graphicname="B239616E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 14 Speakers</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 9 Speakers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the claw and remove the clamp.</ptxt>
<figure>
<graphic graphicname="B241320" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0021" proc-id="RM23G0E___0000BZT00000">
<ptxt>REMOVE REAR DOOR INNER GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 claws and remove the rear door inner glass weatherstrip from the rear door trim board sub-assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239615" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0022" proc-id="RM23G0E___0000ELJ00000">
<ptxt>REMOVE REAR DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B239620" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 3 clamps, move the wire harness out of the way and remove the service hole cover.</ptxt>
<atten4>
<ptxt>Remove any remaining butyl tape from the door.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0023" proc-id="RM23G0E___0000ELK00000">
<ptxt>REMOVE REAR DOOR GLASS RUN LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear door glass run.</ptxt>
<figure>
<graphic graphicname="B239628" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0024" proc-id="RM23G0E___0000ELL00000">
<ptxt>REMOVE REAR DOOR WINDOW DIVISION BAR SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the temporary bolt.</ptxt>
<figure>
<graphic graphicname="B239630E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temporary Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts and screw.</ptxt>
</s2>
<s2>
<ptxt>Remove the rear door window division bar sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the temporary bolt from the rear door window division bar sub-assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0025" proc-id="RM23G0E___0000ELM00000">
<ptxt>REMOVE REAR DOOR QUARTER WINDOW GLASS LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear door quarter window glass and rear door quarter window weatherstrip as a unit as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239632" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0026" proc-id="RM23G0E___0000ELN00000">
<ptxt>REMOVE REAR DOOR GLASS SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the cable to the negative (-) battery terminal and rear power window regulator motor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the power window regulator switch assembly and move the rear door glass sub-assembly so that the door glass bolts can be seen.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the power window regulator switch assembly and rear power window regulator motor connectors.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>w/ Navigation System (for HDD):</ptxt>
<ptxt>After the ignition switch is turned off, the HDD navigation system requires approximately a minute to record various types of memory and settings. As a result, after turning the ignition switch off, wait a minute or more before disconnecting the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the rear door glass sub-assembly from the rear door window regulator sub-assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239636" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>After the bolts are removed, do not allow the door glass to fall.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the rear door glass sub-assembly as indicated by the arrows in the order shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239640E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not damage the door glass.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0006" proc-id="RM23G0E___0000ELF00000">
<ptxt>REMOVE REAR DOOR LOCK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" wrench, remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B237876E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Slide</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Move the rear door lock assembly downward, pull the release plate out of the rear door outside handle frame, and remove the rear door lock assembly and cables as a unit.</ptxt>
</s2>
<s2>
<ptxt>Remove the door lock wiring harness seal from the rear door lock assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039H4020X_01_0014" proc-id="RM23G0E___0000ELG00000">
<ptxt>REMOVE REAR DOOR LOCK REMOTE CONTROL CABLE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the claw.</ptxt>
<figure>
<graphic graphicname="B239650E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the rear door lock remote control cable assembly.</ptxt>
<figure>
<graphic graphicname="B239652E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039H4020X_01_0015" proc-id="RM23G0E___0000ELH00000">
<ptxt>REMOVE REAR DOOR INSIDE LOCKING CABLE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 claws.</ptxt>
<figure>
<graphic graphicname="B239654E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the rear door inside locking cable assembly.</ptxt>
<figure>
<graphic graphicname="B239656E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>