<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM000000PDV0MZX" category="C" type-id="302GR" name-id="ESM77-08" from="201207" to="201210">
<dtccode>P0016</dtccode>
<dtcname>Crankshaft Position - Camshaft Position Correlation (Bank 1 Sensor A)</dtcname>
<subpara id="RM000000PDV0MZX_01" type-id="60" category="03" proc-id="RM23G0E___00003C600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The Variable Valve Timing (VVT) system controls the intake camshaft and exhaust camshaft to achieve optimal valve timing according to various driving conditions. This control is performed based on the intake air volume, throttle valve position, engine coolant temperature and other engine operating conditions. The ECM controls the camshaft timing oil control valves based on the signals transmitted by several sensors. As a result, the relative positions of the camshafts are optimized, improving engine torque and fuel economy and reducing exhaust emissions. In addition, the ECM detects the actual valve timing based on the signals from the VVT sensors and performs feedback control, achieving optimal valve timing.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0016</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A deviation in the crankshaft position sensor signal and VVT sensor (bank 1) signal (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Valve timing</ptxt>
</item>
<item>
<ptxt>Camshaft timing oil control valve assembly</ptxt>
</item>
<item>
<ptxt>Oil control valve filter</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>To monitor the correlation of the intake camshaft position and crankshaft position, the ECM checks the VVT learned value while the engine is idling. The VVT learned value is calibrated based on the camshaft position and crankshaft position. The intake valve timing is set to the most retarded angle while the engine is idling. If the VVT learned value is out of the specified range in consecutive driving cycles, the ECM illuminates the MIL and stores DTC P0016. </ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000PDV0MZX_06" type-id="32" category="03" proc-id="RM23G0E___00003C700000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0010 (See page <xref label="Seep01" href="RM000000PDW0NRX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDV0MZX_07" type-id="51" category="05" proc-id="RM23G0E___00003C800000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PDV0MZX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PDV0MZX_08_0007" proc-id="RM23G0E___00003CB00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0016)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.  </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.08in"/>
<colspec colname="COL2" colwidth="2.05in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC P0016 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P0016 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0016 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDV0MZX_08_0008" fin="false">A</down>
<right ref="RM000000PDV0MZX_08_0013" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0008" proc-id="RM23G0E___00003CC00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (OPERATE CAMSHAFT TIMING OIL CONTROL VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the VVT System (Bank 1).</ptxt>
</test1>
<test1>
<ptxt>Check the engine speed when the camshaft timing oil control valve is operated using the intelligent tester when the engine coolant temperature is 50°C (122°F) or less.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When performing the Active Test, make sure the air conditioning is on.</ptxt>
</item>
<item>
<ptxt>Make sure the engine coolant temperature when the engine is started is 30°C (86°F) or less.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.13in"/>
<colspec colname="COL2" colwidth="4.95in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Camshaft timing oil control valve OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal engine speed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Camshaft timing oil control valve ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine idles roughly or stalls soon after camshaft timing oil control valve switched from OFF to ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDV0MZX_08_0009" fin="false">OK</down>
<right ref="RM000000PDV0MZX_08_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0009" proc-id="RM23G0E___00003CD00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0016)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z8X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the tester (See page <xref label="Seep02" href="RM000000PDL0PHX"/>).</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for more than 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P0016 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>DTC P0016 is stored when foreign objects in the engine oil are caught in some parts of the system. This code remains stored for a short time even after the system returns to normal. These foreign objects may then be captured by the oil control valve filter, thus eliminating the source of the problem.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDV0MZX_08_0005" fin="true">A</down>
<right ref="RM000000PDV0MZX_08_0006" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0010" proc-id="RM23G0E___00003CE00000">
<testtitle>INSPECT CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft timing oil control valve assembly (See page <xref label="Seep01" href="RM000000Q7403PX"/>).</ptxt>
</test1>
<test1>
<ptxt>Connect the positive (+) battery terminal to terminal 1 and the negative (-) battery terminal to terminal 2. Check the valve operation.</ptxt>
<figure>
<graphic graphicname="A183795E07" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Valve moves quickly.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Reinstall the camshaft timing oil control valve assembly.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDV0MZX_08_0012" fin="false">OK</down>
<right ref="RM000000PDV0MZX_08_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0012" proc-id="RM23G0E___00003CG00000">
<testtitle>INSPECT CAMSHAFT TIMING GEAR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft timing gear assembly (See page <xref label="Seep01" href="RM000000YN001UX_01_0031"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDV0MZX_08_0011" fin="false">OK</down>
<right ref="RM000000PDV0MZX_08_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0011" proc-id="RM23G0E___00003CF00000">
<testtitle>INSPECT OIL CONTROL VALVE FILTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the oil control valve filter.</ptxt>
</test1>
<test1>
<ptxt>Check that the filter is not clogged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Filter is not clogged.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Reinstall the oil control valve filter.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDV0MZX_08_0006" fin="false">OK</down>
<right ref="RM000000PDV0MZX_08_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0006" proc-id="RM23G0E___00003CA00000">
<testtitle>ADJUST VALVE TIMING</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>There are no marks on the cylinder head to align for the purpose of checking valve timing. Valve timing can only be inspected by aligning the colored plates on the timing chain with the marks on the pulleys. It may be necessary to remove and reinstall the chain to align the timing marks (See page <xref label="Seep01" href="RM000000YMY01YX_01_0006"/>).</ptxt>
</atten4>
</content6>
<res>
<down ref="RM000000PDV0MZX_08_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0004" proc-id="RM23G0E___00003C900000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0016)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z8X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the tester (See page <xref label="Seep02" href="RM000000PDL0PHX"/>).</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for more than 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P0016 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000PDV0MZX_08_0017" fin="true">A</down>
<right ref="RM000000PDV0MZX_08_0002" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0013">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000000PDF0G7X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0014">
<testtitle>REPLACE CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY<xref label="Seep01" href="RM00000111G008X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0016">
<testtitle>REPLACE CAMSHAFT TIMING GEAR ASSEMBLY<xref label="Seep01" href="RM000000YN001UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0015">
<testtitle>REPLACE OIL CONTROL VALVE FILTER</testtitle>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0017">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0002">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDV0MZX_08_0005">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0VKX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>