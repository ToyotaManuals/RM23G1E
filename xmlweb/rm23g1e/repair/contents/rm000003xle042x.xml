<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A2_T00JQ" variety="T00JQ">
<name>PARKING ASSIST MONITOR SYSTEM</name>
<para id="RM000003XLE042X" category="J" type-id="80410" name-id="PM57U-02" from="201210">
<dtccode/>
<dtcname>Message indicating Back Door is Open is Displayed even after Back Door is Closed</dtcname>
<subpara id="RM000003XLE042X_01" type-id="60" category="03" proc-id="RM23G0E___0000CW200001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The parking assist ECU receives back door courtesy light switch assembly open/close signals from the main body ECU via CAN communication. When the back door is open, the camera aiming cannot be adjusted correctly because the rear television camera assembly is installed on the back door. Therefore, when adjusting the camera aiming calibration while the back door is open, a back door open warning message will be displayed on the screen and camera aiming adjustment will be canceled.</ptxt>
<atten4>
<ptxt>The back door courtesy light switch assembly is connected to the main body ECU by the vehicle wire harness.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000003XLE042X_02" type-id="32" category="03" proc-id="RM23G0E___0000CW300001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E198737E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003XLE042X_03" type-id="51" category="05" proc-id="RM23G0E___0000CW400001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When "System initializing" is displayed on the display and navigation module display after the battery terminal disconnected, correct the steering angle neutral point (See page <xref label="Seep01" href="RM0000035DE02IX"/>).</ptxt>
</item>
<item>
<ptxt>Depending on the parts that are replaced or operations that are performed during vehicle inspection or maintenance, calibration of other systems as well as the parking assist monitor system may be needed (See page <xref label="Seep02" href="RM0000035DD02YX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000003XLE042X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003XLE042X_04_0001" proc-id="RM23G0E___0000CW500001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (BACK DOOR COURTESY SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</test1>
<test1>
<ptxt>Check the Data List for proper functioning of the following item.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Back Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Back door courtesy switch signal/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Back door open</ptxt>
<ptxt>OFF: Back door closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The back door courtesy switch functions as specified in the normal condition column.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLE042X_04_0006" fin="true">OK</down>
<right ref="RM000003XLE042X_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLE042X_04_0003" proc-id="RM23G0E___0000CW600001">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - BACK DOOR COURTESY LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G64 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the R9 back door courtesy light switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G64-19 (BCTY) - R9-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-19 (BCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLE042X_04_0004" fin="false">OK</down>
<right ref="RM000003XLE042X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLE042X_04_0004" proc-id="RM23G0E___0000CW700001">
<testtitle>INSPECT BACK DOOR COURTESY LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the back door courtesy light switch assembly.</ptxt>
<figure>
<graphic graphicname="E139234" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pin pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Pin not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLE042X_04_0005" fin="true">OK</down>
<right ref="RM000003XLE042X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLE042X_04_0005">
<testtitle>REPLACE MAIN BODY ECU<xref label="Seep01" href="RM000003WBO03DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLE042X_04_0006">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000035D702UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLE042X_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003XLE042X_04_0009">
<testtitle>REPLACE BACK DOOR COURTESY LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM000003YNI010X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>