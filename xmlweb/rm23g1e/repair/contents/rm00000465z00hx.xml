<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9CY_T00MM" variety="T00MM">
<name>FRONT SEAT ASSEMBLY (for Manual Seat)</name>
<para id="RM00000465Z00HX" category="A" type-id="80002" name-id="SE8LP-01" from="201207" to="201210">
<name>DISASSEMBLY</name>
<subpara id="RM00000465Z00HX_02" type-id="11" category="10" proc-id="RM23G0E___0000GBA00000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000465Z00HX_01" type-id="01" category="01">
<s-1 id="RM00000465Z00HX_01_0001" proc-id="RM23G0E___0000GAK00000">
<ptxt>REMOVE RECLINING ADJUSTER RELEASE HANDLE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238105E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Raise the reclining adjuster release handle to reveal the claw. Using a screwdriver, detach the claw and remove the handle.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0002" proc-id="RM23G0E___0000GAL00000">
<ptxt>REMOVE VERTICAL ADJUSTING HANDLE LH (for Driver Side)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238107" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw and handle.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0003" proc-id="RM23G0E___0000GAM00000">
<ptxt>REMOVE FRONT SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
<figure>
<graphic graphicname="B238108" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 4 claws and clip, and then remove the cushion shield.</ptxt>
</s2>
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>Detach the 5 wire harness clamps and disconnect the 2 connectors.</ptxt>
<figure>
<graphic graphicname="B239165" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0031" proc-id="RM23G0E___0000GB500000">
<ptxt>REMOVE LUMBAR SWITCH ASSEMBLY (for Driver Side)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239166" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws and switch.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000465Z00HX_01_0005" proc-id="RM23G0E___0000GAN00000">
<ptxt>REMOVE FRONT SEAT INNER CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239167" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the cushion shield.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0026" proc-id="RM23G0E___0000GB100000">
<ptxt>REMOVE FRONT SEAT INNER CUSHION SHIELD RH (for Front Passenger Side)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239169" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 4 claws and remove the cushion shield.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0035" proc-id="RM23G0E___0000GB900000">
<ptxt>REMOVE FRONT SEAT INNER BELT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM000003RQO013X_01_0006"/>)</ptxt>
</s2>
<s2>
<ptxt>for Front Passenger Side:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM000003RQO013X_01_0001"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0008" proc-id="RM23G0E___0000GAO00000">
<ptxt>REMOVE SEPARATE TYPE FRONT SEATBACK ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B239173" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the rubber band from the seat cushion spring.</ptxt>
</s2>
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>Detach the clamp and disconnect the No. 2 seat wire.</ptxt>
<figure>
<graphic graphicname="B241809" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Disconnect the seat heater connector and detach the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="B241810" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for Clip Type:</ptxt>
<ptxt>Using a clip remover, remove the 2 clips.</ptxt>
<figure>
<graphic graphicname="B239176" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>for Hook Type:</ptxt>
<ptxt>Detach the 2 hooks.</ptxt>
<figure>
<graphic graphicname="B239177" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<figure>
<graphic graphicname="B239174E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Detach the claw and disconnect the airbag connector.</ptxt>
</s3>
<s3>
<ptxt>Detach the 3 airbag wire harness clamps.</ptxt>
</s3>
<s3>
<ptxt>Detach the fastening tape and open the cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastening Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Disconnect the airbag wire harness.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Remove the 4 bolts and seatback assembly.</ptxt>
<figure>
<graphic graphicname="B239178" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0009" proc-id="RM23G0E___0000GAP00000">
<ptxt>REMOVE CAP</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239170" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 caps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0010" proc-id="RM23G0E___0000GAQ00000">
<ptxt>REMOVE FRONT SEAT LEG COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239171" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws.</ptxt>
</s2>
<s2>
<ptxt>Move the cover in the direction of the arrow to remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0011" proc-id="RM23G0E___0000GAR00000">
<ptxt>REMOVE SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Disconnect the seat heater connector and detach the 3 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="B241811" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the hooks.</ptxt>
<figure>
<graphic graphicname="B239180" width="7.106578999in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>for Front Passenger Side:</ptxt>
<ptxt>Detach the claw and disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B239181" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the seat cushion cover with pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0012" proc-id="RM23G0E___0000GAS00000">
<ptxt>REMOVE SEPARATE TYPE FRONT SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239182" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the hog rings and seat cushion cover from the seat cushion pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0032" proc-id="RM23G0E___0000GB600000">
<ptxt>REMOVE FRONT SEAT CUSHION HEATER ASSEMBLY LH (w/ Seat Heater System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239184E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins and remove the front seat cushion heater from the front seat cushion cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000465Z00HX_01_0014" proc-id="RM23G0E___0000GAT00000">
<ptxt>REMOVE RECLINING ADJUSTER INSIDE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239186" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the claw and guide, and then remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0015" proc-id="RM23G0E___0000GAU00000">
<ptxt>REMOVE FRONT SEAT LOWER CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239187" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the hook and clip, and then remove the cushion shield.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0016" proc-id="RM23G0E___0000GAV00000">
<ptxt>REMOVE FRONT SEAT LOWER CUSHION SHIELD RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the hook and clip, and then remove the cushion shield.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0017" proc-id="RM23G0E___0000GAW00000">
<ptxt>REMOVE FRONT SEAT CUSHION EDGE PROTECTOR LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239188" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Move the protector in the direction of the arrow to detach the hook and remove the protector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0018" proc-id="RM23G0E___0000GAX00000">
<ptxt>REMOVE FRONT SEAT CUSHION EDGE PROTECTOR RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0033" proc-id="RM23G0E___0000GB700000">
<ptxt>REMOVE SEAT HEATER CONTROL SUB-ASSEMBLY LH (w/ Seat Heater System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239190" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clamps and remove the seat heater control.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000465Z00HX_01_0021" proc-id="RM23G0E___0000GAY00000">
<ptxt>REMOVE FRONT SEAT WIRE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the seat wire.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0029" proc-id="RM23G0E___0000GB300000">
<ptxt>REMOVE RECLINING ADJUSTER INSIDE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239256" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the guide.</ptxt>
</s2>
<s2>
<ptxt>Move the cover in the direction of the arrow to remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0030" proc-id="RM23G0E___0000GB400000">
<ptxt>REMOVE RECLINING ADJUSTER INSIDE COVER RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0023" proc-id="RM23G0E___0000GAZ00000">
<ptxt>REMOVE SEPARATE TYPE FRONT SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B239192" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 hog rings.</ptxt>
</s2>
<s2>
<ptxt>Open the 2 fasteners, and then open the seatback cover.</ptxt>
<figure>
<graphic graphicname="B242232E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Cloth Seat</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Leather Seat</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<figure>
<graphic graphicname="B239201" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Remove the nut and seatback cover bracket from the seat frame.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the seatback cover bracket from the seatback pad.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the 2 headrest supports.</ptxt>
<figure>
<graphic graphicname="B239198" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the hog rings and seatback cover.</ptxt>
<atten4>
<ptxt>The number of hog rings differs according to seat type.</ptxt>
</atten4>
<figure>
<graphic graphicname="B242230E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0034" proc-id="RM23G0E___0000GB800000">
<ptxt>REMOVE FRONT SEATBACK HEATER ASSEMBLY LH (w/ Seat Heater System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239207E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Cut off the tack pins which fasten the seatback heater to the seatback cover, and then remove the seatback heater from the seatback cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000465Z00HX_01_0024" proc-id="RM23G0E___0000GB000000">
<ptxt>REMOVE SEPARATE TYPE FRONT SEATBACK PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the seatback pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0027" proc-id="RM23G0E___0000GB200000">
<ptxt>REMOVE NO. 2 SEAT WIRE (for Driver Side)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239209" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clamps and remove the seat wire.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000465Z00HX_01_0036" proc-id="RM23G0E___0000JU600000">
<ptxt>REMOVE FRONT SEAT SIDE AIRBAG ASSEMBLY (w/ Front Seat Side Airbag)
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>When storing the front seat side airbag assembly, keep the airbag deployment side facing upward.</ptxt>
</atten2>
<atten4>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 2 clamps.</ptxt>
<figure>
<graphic graphicname="B251815" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts and front seat side airbag assembly.</ptxt>
<figure>
<graphic graphicname="B333276" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>The nuts must not be reused.</ptxt>
</item>
<item>
<ptxt>Make sure that the separate type front seatback spring assembly is not deformed. If it is, replace it with a new one.</ptxt>
</item>
</list1>
</atten2>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>