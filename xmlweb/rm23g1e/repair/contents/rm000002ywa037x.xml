<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12013_S0010" variety="S0010">
<name>1KD-FTV STARTING</name>
<ttl id="12013_S0010_7B934_T00CS" variety="T00CS">
<name>ENGINE SWITCH (for LHD)</name>
<para id="RM000002YWA037X" category="G" type-id="3001K" name-id="ST1EB-41" from="201207">
<name>INSPECTION</name>
<subpara id="RM000002YWA037X_01" type-id="01" category="01">
<s-1 id="RM000002YWA037X_01_0001" proc-id="RM23G0E___000071Z00000">
<ptxt>INSPECT ENGINE SWITCH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A129763E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>7 (SS1) - 5 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (SS2) - 5 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7 (SS1) - 5 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (SS2) - 5 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Engine Switch)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the engine switch.</ptxt>
</s2>
<s2>
<ptxt>Apply battery voltage to the terminals of the switch and check the illumination condition of the switch.</ptxt>
<figure>
<graphic graphicname="A109252E74" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Indicator Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Engine Switch)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.48in"/>
<colspec colname="COL2" colwidth="1.65in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 11 (SWIL)</ptxt>
<ptxt>Battery negative (-) → Terminal 5 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminates (illumination of lettering)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 12 (INDS)</ptxt>
<ptxt>Battery negative (-) → Terminal 5 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminates (green)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → Terminal 13 (INDW)</ptxt>
<ptxt>Battery negative (-) → Terminal 5 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminates (amber)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If the positive (+) lead and negative (-) lead are incorrectly connected, the engine switch indicator will not illuminate.</ptxt>
</item>
<item>
<ptxt>If the voltage is too low, the indicator will not illuminate.</ptxt>
</item>
</list1>
</atten3>
<ptxt>If the result is not as specified, replace the engine switch.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>