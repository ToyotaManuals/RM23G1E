<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AL_T00K9" variety="T00K9">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002XHV079X" category="C" type-id="304MR" name-id="RS8GC-05" from="201207" to="201210">
<dtccode>B2785</dtccode>
<dtcname>Communication Malfunction between ECUs Connected by LIN</dtcname>
<subpara id="RM000002XHV079X_01" type-id="60" category="03" proc-id="RM23G0E___0000D9F00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The certification ECU intermittently monitors the LIN communication bus between the components related to certification. DTC B2785 is stored when a malfunction in the LIN communication bus between the components related to certification is detected continuously 3 times.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2785</ptxt>
</entry>
<entry valign="middle">
<ptxt>The certification ECU detects a malfunction in the LIN communication bus between components related to certification continuously 3 times.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Certification ECU</ptxt>
</item>
<item>
<ptxt>Power management control ECU</ptxt>
</item>
<item>
<ptxt>ID code box</ptxt>
</item>
<item>
<ptxt>Steering lock actuator assembly (steering lock ECU)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002XHV079X_02" type-id="32" category="03" proc-id="RM23G0E___0000D9G00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C212389E04" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002XHV079X_03" type-id="51" category="05" proc-id="RM23G0E___0000D9H00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot: </ptxt>
<ptxt>Connect the intelligent tester to the vehicle and turn a courtesy light switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</atten3>
<atten4>
<ptxt>When DTC B2785 and B2287 are output simultaneously, perform troubleshooting for DTC B2785 first.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002XHV079X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002XHV079X_05_0031" proc-id="RM23G0E___0000D9M00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0032" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0032" proc-id="RM23G0E___0000D9N00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2785 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0036" fin="true">OK</down>
<right ref="RM000002XHV079X_05_0021" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0021" proc-id="RM23G0E___0000D9I00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU - POWER MANAGEMENT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G38 certification ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G51 power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-29 (LIN) - G51-24 (LIN2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-29 (LIN) or G51-24 (LIN2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0022" fin="false">OK</down>
<right ref="RM000002XHV079X_05_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0022" proc-id="RM23G0E___0000D9J00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the G38 certification ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G51 power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0023" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0023" proc-id="RM23G0E___0000D9K00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2785 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2785 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0024" fin="false">OK</down>
<right ref="RM000002XHV079X_05_0038" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0024" proc-id="RM23G0E___0000D9L00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the G51 power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G35 steering lock actuator assembly (steering lock ECU) connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0033" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0033" proc-id="RM23G0E___0000D9O00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2785 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2785 is not output (for Power Tilt and Power Telescopic Steering Column)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2785 is not output (for Manual Tilt and Manual Telescopic Steering Column)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0034" fin="false">A</down>
<right ref="RM000002XHV079X_05_0040" fin="true">B</right>
<right ref="RM000002XHV079X_05_0041" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0034" proc-id="RM23G0E___0000D9P00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the G35 steering lock actuator assembly (steering lock ECU) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G37 ID code box connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0035" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0035" proc-id="RM23G0E___0000D9Q00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.30in"/>
<colspec colname="COL2" colwidth="0.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B2785 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B2785 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002XHV079X_05_0017" fin="true">A</down>
<right ref="RM000002XHV079X_05_0037" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002XHV079X_05_0017">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000002XHV079X_05_0036">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002XHV079X_05_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002XHV079X_05_0038">
<testtitle>REPLACE POWER MANAGEMENT CONTROL ECU<xref label="Seep01" href="RM0000039R5015X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002XHV079X_05_0040">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000039SJ012X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002XHV079X_05_0041">
<testtitle>REPLACE STEERING LOCK ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000039SJ011X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002XHV079X_05_0037">
<testtitle>REPLACE ID CODE BOX</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>