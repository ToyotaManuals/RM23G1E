<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B946_T00DU" variety="T00DU">
<name>VALVE BODY ASSEMBLY</name>
<para id="RM0000013CK04IX" category="A" type-id="30014" name-id="AT802-02" from="201207">
<name>INSTALLATION</name>
<subpara id="RM0000013CK04IX_01" type-id="01" category="01">
<s-1 id="RM0000013CK04IX_01_0001" proc-id="RM23G0E___00007QR00000">
<ptxt>INSTALL TRANSMISSION VALVE BODY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the spring and check ball body.</ptxt>
</s2>
<s2>
<ptxt>Insert the pin of the manual valve into the hole of the manual valve lever.</ptxt>
<figure>
<graphic graphicname="C157293E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the transmission valve body with the 19 bolts.</ptxt>
<figure>
<graphic graphicname="C157292E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>112</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Each bolt length is indicated below.</ptxt>
</atten4>
<ptxt>32 mm (1.26 in.) for bolt A</ptxt>
<ptxt>25 mm (0.984 in.) for bolt B</ptxt>
</s2>
<s2>
<ptxt>Install the detent spring and detent spring cover with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013CK04IX_01_0013" proc-id="RM23G0E___00007QB00000">
<ptxt>CONNECT TRANSMISSION WIRE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 7 connectors to the solenoid valves.</ptxt>
<figure>
<graphic graphicname="C125285E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Orange</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Blue</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Connect the 2 ATF temperature sensors with the 2 clamps and 2 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>112</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Each bolt length is indicated below.</ptxt>
<ptxt>12 mm (0.472 in.) for bolt A</ptxt>
</atten4>
<ptxt>36 mm (1.42 in.) for bolt B</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK04IX_01_0014" proc-id="RM23G0E___00007QC00000">
<ptxt>INSTALL VALVE BODY OIL STRAINER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Coat a new O-ring with ATF and install it to the oil strainer.</ptxt>
</s2>
<s2>
<ptxt>Install the oil strainer with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK04IX_01_0015" proc-id="RM23G0E___00007QD00000">
<ptxt>INSTALL AUTOMATIC TRANSMISSION OIL PAN SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Remove the gasket and be careful not to spill oil on the contacting surfaces of the transmission case and oil pan.</ptxt>
</atten3>
<s2>
<ptxt>Install a new gasket and the oil pan with the 20 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>7.0</t-value1>
<t-value2>71</t-value2>
<t-value3>62</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK04IX_01_0018" proc-id="RM23G0E___00007QH00000">
<ptxt>INSTALL REAR NO. 1 ENGINE MOUNTING INSULATOR (for 1KD-FTV)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear engine mounting insulator to the transmission with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>65</t-value1>
<t-value2>663</t-value2>
<t-value4>48</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK04IX_01_0016" proc-id="RM23G0E___00007QT00000">
<ptxt>INSTALL NO. 3 FRAME CROSSMEMBER SUB-ASSEMBLY (for 1KD-FTV)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the frame crossmember to the rear engine mounting insulator with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>306</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the frame crossmember with the 4 bolts and 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>72</t-value1>
<t-value2>734</t-value2>
<t-value4>53</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK04IX_01_0019" proc-id="RM23G0E___00007QG00000">
<ptxt>INSTALL FRONT SUSPENSION MEMBER BRACKET LH AND RH (for 1KD-FTV)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front suspension member bracket LH and front suspension member bracket RH with the 8 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>33</t-value1>
<t-value2>337</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000013CK04IX_01_0009" proc-id="RM23G0E___00007QS00000">
<ptxt>ADD AUTOMATIC TRANSMISSION FLUID</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add automatic transmission fluid (See page <xref label="Seep01" href="RM000002BL003JX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000013CK04IX_01_0017" proc-id="RM23G0E___00007QU00000">
<ptxt>PERFORM RESET MEMORY</ptxt>
<content1 releasenbr="1">
<ptxt>Perform the RESET MEMORY procedures (A/T initialization) (See page 1GR-FE: <xref label="Seep01" href="RM000000W7F0LFX"/> 1KD-FTV: <xref label="Seep02" href="RM000000W7F0LEX"/>).</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>