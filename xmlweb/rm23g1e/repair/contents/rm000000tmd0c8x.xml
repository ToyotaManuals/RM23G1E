<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM000000TMD0C8X" category="C" type-id="300Y7" name-id="ESM8K-12" from="201210">
<dtccode>P0420</dtccode>
<dtcname>Catalyst System Efficiency Below Threshold (Bank 1)</dtcname>
<subpara id="RM000000TMD0C8X_15" type-id="60" category="03" proc-id="RM23G0E___000039H00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses sensors mounted in front of and behind the Three Way Catalytic Converter (TWC) to monitor its efficiency.</ptxt>
<ptxt>The first sensor, the air fuel ratio sensor, sends pre-catalyst information to the ECM. The second sensor, the heated oxygen sensor, sends post-catalyst information to the ECM. </ptxt>
<ptxt>In order to detect any deterioration in the TWC, the ECM calculates the Oxygen Storage Capacity (OSC) of the TWC. This calculation is based on the voltage output of the heated oxygen sensor while performing active air fuel ratio control.</ptxt>
<ptxt>The OSC value is an indication of the oxygen storage capacity of the TWC. When the vehicle is being driven with a warm engine, active air fuel ratio control is performed for approximately 15 to 20 seconds. When it is performed, the ECM deliberately sets the air fuel ratio to lean or rich levels. If the rich-lean cycle of the heated oxygen sensor is long, the OSC becomes greater. There is a direct correlation between the OSCs of the heated oxygen sensor and the TWC.</ptxt>
<ptxt>The ECM uses the OSC value to determine the state of the TWC. If any deterioration has occurred, it illuminates the MIL and stores the DTC.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0420</ptxt>
</entry>
<entry valign="middle">
<ptxt>The Oxygen Storage Capacity (OSC) value is less than the standard value under active air fuel ratio control (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front exhaust pipe assembly (TWC: Front catalyst and rear catalyst)</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TMD0C8X_16" type-id="87" category="03" proc-id="RM23G0E___000039I00001">
<name>CATALYST LOCATION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A229907E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Manifold</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>TWC: Front Catalyst</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>TWC: Rear Catalyst</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Exhaust Pipe Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated Oxygen Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Exhaust Pipe Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tailpipe Assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TMD0C8X_14" type-id="73" category="03" proc-id="RM23G0E___000039G00001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Performing this confirmation driving pattern will activate the catalyst monitor. This is very useful for verifying the completion of a repair.</ptxt>
</atten4>
<figure>
<graphic graphicname="A207276E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="nonmark">
<item>
<ptxt>(a) Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>(b) Turn the ignition switch to ON and turn the intelligent tester on.</ptxt>
</item>
<item>
<ptxt>(c) Enter the following menus: Powertrain / Engine and ECT / DTC / Clear.</ptxt>
</item>
<item>
<ptxt>(d) Clear DTCs (if output).</ptxt>
</item>
<item>
<ptxt>(e) Enter the following menus: Powertrain / Engine and ECT / Data List / Catalyst Monitor.</ptxt>
</item>
<item>
<ptxt>(f) Check that Catalyst Monitor is INCMPL (incomplete).</ptxt>
</item>
<item>
<ptxt>(g) Start the engine and warm it up.</ptxt>
</item>
<item>
<ptxt>(h) Drive the vehicle at a speed between 64 km/h and 113 km/h (40 mph and 70 mph) for at least 10 minutes.</ptxt>
</item>
<item>
<ptxt>(i) The monitor items will change to COMPL (Complete) as Catalyst Monitor operates.</ptxt>
</item>
<item>
<ptxt>(j) Enter the following menus: Powertrain / Engine and ECT / DTC / Pending; and then check if any DTCs (any pending DTCs) are output.</ptxt>
<atten4>
<ptxt>If Catalyst Monitor does not change to Compl (Complete) and no pending DTCs are stored, extend the driving time.</ptxt>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000TMD0C8X_12" type-id="74" category="03" proc-id="RM23G0E___000039F00001">
<name>CONDITIONING FOR SENSOR TESTING</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Perform the operation with the engine speeds and time durations described below prior to checking the waveforms of the air fuel ratio and heated oxygen sensors. This is in order to activate the sensors sufficiently to obtain the appropriate inspection results.</ptxt>
</atten4>
<figure>
<graphic graphicname="A118003E51" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="nonmark">
<item>
<ptxt>(a) Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>(b) Start the engine and warm it up with all the accessories switched off until the engine coolant temperature stabilizes.</ptxt>
</item>
<item>
<ptxt>(c) Run the engine at an engine speed of between 2500 rpm and 3000 rpm for at least 3 minutes.</ptxt>
</item>
<item>
<ptxt>(d) While running the engine at 3000 rpm and 2000 rpm alternating at 2 second intervals, check the waveforms of the air fuel ratio and heated oxygen sensors using the tester.</ptxt>
</item>
</list1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the voltage output of the air fuel ratio or heated oxygen sensor does not fluctuate, or there is noise in the waveform of either sensor, the sensor may be malfunctioning.</ptxt>
</item>
<item>
<ptxt>If the voltage outputs of both sensors remain lean or rich, the air fuel ratio may be extremely lean or rich. In such cases, enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for air fuel ratio sensor.</ptxt>
</item>
<item>
<ptxt>If the Three Way Catalytic Converter (TWC) has deteriorated, the heated oxygen sensor (located behind the TWC) voltage output fluctuates up and down frequently, even under normal driving conditions (active air fuel ratio control is not performed).</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="A121610E42" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TMD0C8X_09" type-id="51" category="05" proc-id="RM23G0E___000039800001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a malfunction cannot be found when troubleshooting DTC P0420 a lean or rich abnormality may be the cause. Perform troubleshooting by following the inspection procedure for P0171 (System Too Lean) and P0172 (System Too Rich).</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TMD0C8X_11" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TMD0C8X_11_0001" proc-id="RM23G0E___000039900001">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0420)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC P0420 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P0420 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0420 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TMD0C8X_11_0011" fin="false">A</down>
<right ref="RM000000TMD0C8X_11_0015" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0011" proc-id="RM23G0E___000039C00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (INJECTION VOLUME CONTROL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine at an engine speed of 2500 rpm for approximately 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F sensor.</ptxt>
</test1>
<test1>
<ptxt>Perform the Control the Injection Volume for A/F sensor operation with the engine idling (press the RIGHT or LEFT button to change the fuel injection volume).</ptxt>
</test1>
<test1>
<ptxt>Monitor the voltage outputs of the air fuel ratio and heated oxygen sensors (AFS Voltage B1S1 and O2S B1S2) displayed on the tester.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The Control the Injection Volume for A/F sensor operation lowers the fuel injection volume by 12.5% or increases the injection volume by 12.5%.</ptxt>
</item>
<item>
<ptxt>Each sensor reacts in accordance with increases and decreases in the fuel injection volume.</ptxt>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
<ptxt>(Sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 3.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 3.4 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 0.55 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</item>
</list1>
</atten4>
</test1>
<table pgwide="1">
<title>Result</title>
<tgroup cols="6">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="0.71in"/>
<colspec colname="COL3" colwidth="1.98in"/>
<colspec colname="COL4" colwidth="0.71in"/>
<colspec colname="COL5" colwidth="2.41in"/>
<colspec colname="COL6" colwidth="0.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Status of</ptxt>
<ptxt>AFS Voltage B1S1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status of</ptxt>
<ptxt>O2S B1S2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A/F Condition and A/F and</ptxt>
<ptxt>HO2 Sensor Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Misfire</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Suspected Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Three Way Catalytic Converter (TWC)</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated oxygen sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated oxygen sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual air fuel ratio lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>May occur</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Extremely rich or lean actual air fuel ratio</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual air fuel ratio rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Extremely rich or lean actual air fuel ratio</ptxt>
</item>
<item>
<ptxt>Gas leak from exhaust system</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Lean: During Control the Injection Volume for A/F sensor, the air fuel ratio sensor output voltage (AFS) is consistently higher than 3.4 V, and the heated oxygen sensor output voltage (O2S) is consistently below 0.4 V.</ptxt>
<ptxt>Rich: During Control the Injection Volume for A/F sensor, the AFS voltage is consistently below 3.1 V, and the O2S voltage is consistently higher than 0.55 V.</ptxt>
<ptxt>Lean/Rich: During Control the Injection Volume for A/F sensor of the Active Test, the output voltages of the air fuel ratio sensor and heated oxygen sensor alternate correctly.</ptxt>
</content6>
<res>
<down ref="RM000000TMD0C8X_11_0002" fin="false">A</down>
<right ref="RM000000TMD0C8X_11_0012" fin="true">B</right>
<right ref="RM000000TMD0C8X_11_0005" fin="false">C</right>
<right ref="RM000000TMD0C8X_11_0016" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0002" proc-id="RM23G0E___000039A00001">
<testtitle>INSPECT FOR EXHAUST GAS LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect for exhaust gas leakage.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No gas leakage.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TMD0C8X_11_0014" fin="false">OK</down>
<right ref="RM000000TMD0C8X_11_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0014" proc-id="RM23G0E___000039D00001">
<testtitle>REPLACE FRONT EXHAUST PIPE ASSEMBLY (TWC: FRONT CATALYST AND REAR CATALYST)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the front exhaust pipe assembly (See page <xref label="Seep01" href="RM0000046ET005X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TMD0C8X_11_0019" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0019" proc-id="RM23G0E___000039E00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z8X"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed between 64 km/h and 113 km/h (40 mph and 70 mph) for at least 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs (Pending DTCs).</ptxt>
<spec>
<title>Result</title>
<specitem>
<ptxt>Pending DTC is not output</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TMD0C8X_11_0020" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0020">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0015">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000000PDF0HGX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0012">
<testtitle>REPLACE AIR FUEL RATIO SENSOR<xref label="Seep01" href="RM00000175900FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0016">
<testtitle>CHECK EXTREMELY RICH OR LEAN ACTUAL AIR FUEL RATIO, REPAIR CAUSE AND GO TO STEP 3<xref label="Seep01" href="RM000000WC30PSX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0006">
<testtitle>REPAIR OR REPLACE EXHAUST GAS LEAK POINT</testtitle>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0005" proc-id="RM23G0E___000039B00001">
<testtitle>INSPECT FOR EXHAUST GAS LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect for exhaust gas leakage.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No gas leakage.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TMD0C8X_11_0013" fin="true">OK</down>
<right ref="RM000000TMD0C8X_11_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0013">
<testtitle>REPLACE HEATED OXYGEN SENSOR<xref label="Seep01" href="RM00000175F00MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TMD0C8X_11_0017">
<testtitle>REPAIR OR REPLACE EXHAUST GAS LEAK POINT</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>