<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000S" variety="S000S">
<name>5L-E COOLING</name>
<ttl id="12011_S000S_7B91W_T00BK" variety="T00BK">
<name>RADIATOR</name>
<para id="RM00000144D01VX" category="A" type-id="30014" name-id="CO5AR-02" from="201207">
<name>INSTALLATION</name>
<subpara id="RM00000144D01VX_01" type-id="01" category="01">
<s-1 id="RM00000144D01VX_01_0026" proc-id="RM23G0E___00006PY00000">
<ptxt>INSTALL NO. 1 RADIATOR SUPPORT SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new No. 1 radiator support seal to the radiator.</ptxt>
<figure>
<graphic graphicname="A225189" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0027" proc-id="RM23G0E___00006PZ00000">
<ptxt>INSTALL NO. 2 RADIATOR SUPPORT SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new No. 2 radiator support seal to the radiator.</ptxt>
<figure>
<graphic graphicname="A225188" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0028" proc-id="RM23G0E___00006Q000000">
<ptxt>INSTALL NO. 2 RADIATOR SUPPORT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 radiator supports and 2 No. 1 radiator support bushes as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A225187" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0029" proc-id="RM23G0E___00006Q100000">
<ptxt>INSTALL NO. 1 RADIATOR SUPPORT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 radiator supports and 2 No. 1 radiator support bushes as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A225186" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0001" proc-id="RM23G0E___00004XP00000">
<ptxt>INSTALL RADIATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert the tabs of the radiator support into the radiator service holes.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A218380" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Install the radiator with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0013" proc-id="RM23G0E___00004VX00000">
<ptxt>INSTALL FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fan pulley to the water pump.</ptxt>
</s2>
<s2>
<ptxt>Install the shroud together with the coupling fan between the radiator and engine.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the fluid coupling fan to the fan pulley with the 4 nuts. Tighten the nuts as much as possible by hand.</ptxt>
</s2>
<s2>
<ptxt>Attach the claws of the shroud as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A224939" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the shroud with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the fan and generator V belt and vane pump V belt (See page <xref label="Seep01" href="RM00000122V00EX"/>).</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 nuts of the fluid coupling fan.</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>189</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000144D01VX_01_0039" proc-id="RM23G0E___00004VY00000">
<ptxt>INSTALL RADIATOR RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator reservoir with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the radiator reservoir hose to the radiator tank side.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144D01VX_01_0024" proc-id="RM23G0E___00004XQ00000">
<ptxt>INSTALL NO. 2 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator hose.</ptxt>
<figure>
<graphic graphicname="A218414E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Upper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Front Side of Vehicle</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry>
<ptxt>LH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Position the hose clamps as shown in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000144D01VX_01_0025" proc-id="RM23G0E___00004VZ00000">
<ptxt>INSTALL NO. 1 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the hose clamp with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.5</t-value1>
<t-value2>87</t-value2>
<t-value3>75</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the radiator hose.</ptxt>
<figure>
<graphic graphicname="A218412E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Upper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>RH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Position the hose clamps as shown in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM00000144D01VX_01_0009" proc-id="RM23G0E___00004XS00000">
<ptxt>INSTALL RADIATOR SIDE DEFLECTOR LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Install the deflector with the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0010" proc-id="RM23G0E___00004XR00000">
<ptxt>INSTALL RADIATOR SIDE DEFLECTOR RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws.</ptxt>
</s2>
<s2>
<ptxt>Install the deflector with the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0031" proc-id="RM23G0E___00004XT00000">
<ptxt>INSTALL UPPER FRONT BUMPER RETAINER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the upper retainer with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0015" proc-id="RM23G0E___00006PV00000">
<ptxt>INSTALL FRONT BUMPER COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front bumper cover (See page <xref label="Seep01" href="RM0000038JV01AX_01_0031"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144D01VX_01_0014" proc-id="RM23G0E___000014O00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
<s2>
<ptxt>Tighten the cylinder block drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Slowly fill the system with engine coolant.</ptxt>
<spec>
<title>Standard capacity</title>
<specitem>
<ptxt>8.6 liters (9.0 US qts, 7.6 Imp. qts)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use only Toyota Super Long Life Coolant or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, and non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids.)</ptxt>
</item>
<item>
<ptxt>New Toyota vehicles are filled with Toyota Super Long Life Coolant. When replacing the coolant, Toyota Super Long Life Coolant (color is pink, premixed ethylene glycol concentration is approximately 50% and freezing temperature is -35°C (-31°F)) is recommended</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the Full line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and warm it up until the thermostat opens.</ptxt>
<atten4>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand and checking when the engine coolant starts to flow inside the hose.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Maintain the engine speed at 2000 to 2500 rpm.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the radiator reservoir still has some coolant in it.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the water temperature meter. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Run the engine at 2000 rpm until the coolant level has stabilized.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Heat areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the fan.</ptxt>
</item>
</list1>
</atten2>
</s2>
<s2>
<ptxt>Stop the engine, and wait until the engine coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Check that the coolant level is between the Full and Low lines.</ptxt>
<ptxt>If the coolant level is below the Low line, repeat all of the steps above.</ptxt>
<ptxt>If the coolant level is above the Full line, drain coolant so that the coolant level is between the Full and Low lines.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144D01VX_01_0034" proc-id="RM23G0E___000014P00000">
<ptxt>INSPECT FOR ENGINE COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester to the radiator.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using a radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator or water pump for leaks. If no external leaks are found, check the heater core, cylinder block, and cylinder head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144D01VX_01_0035" proc-id="RM23G0E___00004W100000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Hook the engine under cover to the vehicle body as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000144D01VX_01_0036" proc-id="RM23G0E___00004W200000">
<ptxt>INSTALL FRONT BUMPER COVER LOWER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front bumper cover lower with the 5 bolts and clip.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000144D01VX_01_0037" proc-id="RM23G0E___000014S00000">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the upper radiator support seal with the 13 clips.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>