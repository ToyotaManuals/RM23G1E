<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S000W" variety="S000W">
<name>5L-E LUBRICATION</name>
<ttl id="12012_S000W_7B92M_T00CA" variety="T00CA">
<name>ENGINE OIL COOLER</name>
<para id="RM00000146V00CX" category="A" type-id="30014" name-id="LU2AS-03" from="201207">
<name>INSTALLATION</name>
<subpara id="RM00000146V00CX_01" type-id="01" category="01">
<s-1 id="RM00000146V00CX_01_0030" proc-id="RM23G0E___00006WS00000">
<ptxt>INSTALL OIL COOLER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new gaskets and the oil cooler to the filter bracket with the 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>158</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146V00CX_01_0002" proc-id="RM23G0E___000050L00000">
<ptxt>INSTALL OIL FILTER BRACKET SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the oil filter bracket with the 10 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A118122E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>301</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nut</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146V00CX_01_0032" proc-id="RM23G0E___00004XY00000">
<ptxt>INSTALL OIL FILTER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check and clean the oil filter installation surface.</ptxt>
</s2>
<s2>
<ptxt>Apply clean engine oil to the gasket of a new oil filter.</ptxt>
</s2>
<s2>
<ptxt>Install the oil filter and tighten it by hand until the gasket contacts the installation surface.</ptxt>
</s2>
<s2>
<ptxt>Using SST, tighten the oil filter. Choose from the following to further tighten the oil filter.</ptxt>
<sst>
<sstitem>
<s-number>09228-44011</s-number>
</sstitem>
</sst>
<s3>
<ptxt>Using a torque wrench, tighten the oil filter.</ptxt>
<figure>
<graphic graphicname="A227790E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<torque>
<torqueitem>
<t-value1>17</t-value1>
<t-value2>173</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Tighten the oil filter a 3/4 turn by hand or with a common wrench.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>3/4 turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM00000146V00CX_01_0003" proc-id="RM23G0E___000051100000">
<ptxt>CONNECT VACUUM PUMP OIL INLET HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the vacuum pump oil inlet hose with a new gasket and the union bolt.</ptxt>
<figure>
<graphic graphicname="A118121E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>140</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>When tightening the inlet hose, hang the stopper-bar of the inlet hose on the filter bracket to prevent the inlet hose from rotating.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stopper-bar</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146V00CX_01_0033" proc-id="RM23G0E___00006WT00000">
<ptxt>INSTALL EXHAUST MANIFOLD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the exhaust manifold (See page <xref label="Seep01" href="RM000004562002X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146V00CX_01_0035" proc-id="RM23G0E___00006WU00000">
<ptxt>INSTALL GENERATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the generator (See page <xref label="Seep01" href="RM0000014PF03QX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146V00CX_01_0054" proc-id="RM23G0E___00004XA00000">
<ptxt>ADD ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add fresh engine oil.</ptxt>
<spec>
<title>Standard Engine Oil</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="3.54in"/>
<colspec colname="COL2" align="center" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>Oil Grade</ptxt>
</entry>
<entry>
<ptxt>Oil Viscosity (SAE)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>API CF-4 or CF</ptxt>
</entry>
<entry>
<ptxt>10W-30</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Capacity</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="3.54in"/>
<colspec colname="COL2" align="center" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Drain and refill without oil filter change</ptxt>
</entry>
<entry>
<ptxt>6.0 liters (6.3 US qts, 5.3 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Drain and refill with oil filter change</ptxt>
</entry>
<entry>
<ptxt>6.9 liters (7.3 US qts, 6.1 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Dry fill</ptxt>
</entry>
<entry>
<ptxt>7.6 liters (8.0 US qts, 6.7 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Install the oil filler cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146V00CX_01_0043" proc-id="RM23G0E___000014O00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
<s2>
<ptxt>Tighten the cylinder block drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Slowly fill the system with engine coolant.</ptxt>
<spec>
<title>Standard capacity</title>
<specitem>
<ptxt>8.6 liters (9.0 US qts, 7.6 Imp. qts)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use only Toyota Super Long Life Coolant or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, and non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids.)</ptxt>
</item>
<item>
<ptxt>New Toyota vehicles are filled with Toyota Super Long Life Coolant. When replacing the coolant, Toyota Super Long Life Coolant (color is pink, premixed ethylene glycol concentration is approximately 50% and freezing temperature is -35°C (-31°F)) is recommended</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the Full line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and warm it up until the thermostat opens.</ptxt>
<atten4>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand and checking when the engine coolant starts to flow inside the hose.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Maintain the engine speed at 2000 to 2500 rpm.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the radiator reservoir still has some coolant in it.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the water temperature meter. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Run the engine at 2000 rpm until the coolant level has stabilized.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Heat areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the fan.</ptxt>
</item>
</list1>
</atten2>
</s2>
<s2>
<ptxt>Stop the engine, and wait until the engine coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Check that the coolant level is between the Full and Low lines.</ptxt>
<ptxt>If the coolant level is below the Low line, repeat all of the steps above.</ptxt>
<ptxt>If the coolant level is above the Full line, drain coolant so that the coolant level is between the Full and Low lines.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146V00CX_01_0057" proc-id="RM23G0E___00004Y100000">
<ptxt>INSPECT FOR ENGINE OIL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine. Make sure that there are no oil leaks from the areas that were worked on.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146V00CX_01_0044" proc-id="RM23G0E___000014P00000">
<ptxt>INSPECT FOR ENGINE COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester to the radiator.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using a radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator or water pump for leaks. If no external leaks are found, check the heater core, cylinder block, and cylinder head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146V00CX_01_0058" proc-id="RM23G0E___00004XV00000">
<ptxt>INSPECT FOR EXHAUST GAS LEAK
</ptxt>
<content1 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>If gas is leaking, tighten the areas necessary to stop the leak. Replace damaged parts as necessary.</ptxt>
</item>
</list1>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>