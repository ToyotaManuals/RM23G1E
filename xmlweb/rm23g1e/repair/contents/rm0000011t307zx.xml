<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12053_S001Z" variety="S001Z">
<name>LIGHTING (INT)</name>
<ttl id="12053_S001Z_7B9BM_T00LA" variety="T00LA">
<name>LIGHTING SYSTEM</name>
<para id="RM0000011T307ZX" category="J" type-id="3011B" name-id="LI73W-07" from="201207" to="201210">
<dtccode/>
<dtcname>Door Courtesy Switch Circuit</dtcname>
<subpara id="RM0000011T307ZX_01" type-id="60" category="03" proc-id="RM23G0E___0000F6700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU receives a door open/closed signal from each door courtesy light switch.</ptxt>
</content5>
</subpara>
<subpara id="RM0000011T307ZX_02" type-id="32" category="03" proc-id="RM23G0E___0000F6800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E200984E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000011T307ZX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000011T307ZX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000011T307ZX_05_0001" proc-id="RM23G0E___0000F6900000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR COURTESY LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000000PKV0BTX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>RR Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear door courtesy light switch RH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear door RH open</ptxt>
<ptxt>OFF: Rear door RH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RL Door Courtesy SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear door courtesy light switch LH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Rear door LH open</ptxt>
<ptxt>OFF: Rear door LH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FR Door Courtesy</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door courtesy light switch RH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front door RH open</ptxt>
<ptxt>OFF: Front door RH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Door Courtesy</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door courtesy light switch LH signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front door LH open</ptxt>
<ptxt>OFF: Front door LH closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition column.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (Front door courtesy light switch does not operate)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (Rear door courtesy light switch does not operate)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011T307ZX_05_0006" fin="true">A</down>
<right ref="RM0000011T307ZX_05_0002" fin="false">B</right>
<right ref="RM0000011T307ZX_05_0008" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0006">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002CE20DIX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0002" proc-id="RM23G0E___0000F6A00000">
<testtitle>INSPECT FRONT DOOR COURTESY LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the front door courtesy light switch (See page <xref label="Seep01" href="RM000003YNE01BX"/>).</ptxt>
<figure>
<graphic graphicname="E139234" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 - Switch body</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011T307ZX_05_0003" fin="false">OK</down>
<right ref="RM0000011T307ZX_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0003" proc-id="RM23G0E___0000F6B00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - FRONT DOOR COURTESY LIGHT SWITCH ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the main body ECU (See page <xref label="Seep01" href="RM000003WBO02VX"/>).</ptxt>
</test1>
<test1>
<ptxt>Disconnect the R7 or Q8 front door courtesy light switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COLSPEC2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A-2 (FLCY) - R7-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-2 (FLCY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RH</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COLSPEC2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A-3 (FRCY) - Q8-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-3 (FRCY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011T307ZX_05_0007" fin="true">OK</down>
<right ref="RM0000011T307ZX_05_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0007">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO02VX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0008" proc-id="RM23G0E___0000F6C00000">
<testtitle>INSPECT REAR DOOR COURTESY LIGHT SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the rear door courtesy light switch (See page <xref label="Seep01" href="RM000003YNI00ZX"/>).</ptxt>
<figure>
<graphic graphicname="E139234" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 - Switch body</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin not pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin pushed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011T307ZX_05_0010" fin="false">OK</down>
<right ref="RM0000011T307ZX_05_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0010" proc-id="RM23G0E___0000F6D00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - REAR DOOR COURTESY LIGHT SWITCH ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G64 or G65 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the R8 or Q9 rear door courtesy light switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LH</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COLSPEC1" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G65-3 (LCTY) - R8-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G65-3 (LCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RH</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COLSPEC1" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G64-6 (RCTY) - Q9-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-6 (RCTY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011T307ZX_05_0007" fin="true">OK</down>
<right ref="RM0000011T307ZX_05_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0004">
<testtitle>REPLACE FRONT DOOR COURTESY LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM000003YNE01BX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000011T307ZX_05_0012">
<testtitle>REPLACE REAR DOOR COURTESY LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM000003YNI00ZX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>