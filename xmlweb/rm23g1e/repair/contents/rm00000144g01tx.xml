<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000S" variety="S000S">
<name>5L-E COOLING</name>
<ttl id="12011_S000S_7B91W_T00BK" variety="T00BK">
<name>RADIATOR</name>
<para id="RM00000144G01TX" category="A" type-id="80001" name-id="CO5AQ-02" from="201207">
<name>REMOVAL</name>
<subpara id="RM00000144G01TX_01" type-id="01" category="01">
<s-1 id="RM00000144G01TX_01_0028" proc-id="RM23G0E___000014X00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 13 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144G01TX_01_0029" proc-id="RM23G0E___00004WI00000">
<ptxt>REMOVE FRONT BUMPER COVER LOWER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper cover lower.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144G01TX_01_0030" proc-id="RM23G0E___00004WJ00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Unhook the engine under cover from the vehicle body as shown in the illustration.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144G01TX_01_0013" proc-id="RM23G0E___000014W00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<figure>
<graphic graphicname="A224962E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Drain the coolant by removing the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug and drain the coolant from the engine.</ptxt>
<atten3>
<ptxt>If coolant is not drained from the radiator drain cock plug, cylinder block drain cock plugs and radiator reservoir, clogging in the radiator or coolant leakage from the seal of the water pump may result.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000144G01TX_01_0014" proc-id="RM23G0E___00006Q500000">
<ptxt>REMOVE FRONT BUMPER COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front bumper cover (See page <xref label="Seep01" href="RM0000038JX01AX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144G01TX_01_0023" proc-id="RM23G0E___00004Z400000">
<ptxt>REMOVE UPPER FRONT BUMPER RETAINER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and retainer.</ptxt>
<figure>
<graphic graphicname="A225008" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144G01TX_01_0003" proc-id="RM23G0E___00004Z500000">
<ptxt>REMOVE RADIATOR SIDE DEFLECTOR LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 3 claws and remove the clip. Then move the side deflector so that the radiator can be removed in the step below.</ptxt>
<figure>
<graphic graphicname="A225009" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144G01TX_01_0004" proc-id="RM23G0E___00004Z600000">
<ptxt>REMOVE RADIATOR SIDE DEFLECTOR RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 3 claws and remove the clip. Then move the side deflector so that the radiator can be removed in the step below.</ptxt>
<figure>
<graphic graphicname="A223074" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144G01TX_01_0021" proc-id="RM23G0E___00004WK00000">
<ptxt>REMOVE NO. 1 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp and remove the No. 1 radiator hose.</ptxt>
<figure>
<graphic graphicname="A218411" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts and hose clamp.</ptxt>
<figure>
<graphic graphicname="A218417" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144G01TX_01_0022" proc-id="RM23G0E___00004Z700000">
<ptxt>DISCONNECT NO. 2 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A218413" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1></s-1>
<s-1 id="RM00000144G01TX_01_0031" proc-id="RM23G0E___00004WL00000">
<ptxt>REMOVE RADIATOR RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the reservoir hose from the upper side of the radiator tank.</ptxt>
<figure>
<graphic graphicname="A224937" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and radiator reservoir.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144G01TX_01_0012" proc-id="RM23G0E___00004WM00000">
<ptxt>REMOVE FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 4 nuts holding the fluid coupling fan.</ptxt>
<figure>
<graphic graphicname="A218410" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the vane pump V belt and the fan and generator V belt (See page <xref label="Seep01" href="RM00000122Y00CX"/>).</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts holding the fan shroud.</ptxt>
<figure>
<graphic graphicname="A224938" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 nuts of the fluid coupling fan, and then remove the shroud together with the coupling fan.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the fan pulley from the water pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144G01TX_01_0011" proc-id="RM23G0E___00004Z800000">
<ptxt>REMOVE RADIATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A218379" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and radiator.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144G01TX_01_0024" proc-id="RM23G0E___00006Q800000">
<ptxt>REMOVE NO. 1 RADIATOR SUPPORT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 radiator supports and 2 No. 1 radiator support bushes.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144G01TX_01_0025" proc-id="RM23G0E___00006Q900000">
<ptxt>REMOVE NO. 2 RADIATOR SUPPORT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 radiator supports and 2 No. 1 radiator support bushes.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144G01TX_01_0026" proc-id="RM23G0E___00006QA00000">
<ptxt>REMOVE NO. 2 RADIATOR SUPPORT SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 2 radiator support seal from the radiator.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144G01TX_01_0027" proc-id="RM23G0E___00006QB00000">
<ptxt>REMOVE NO. 1 RADIATOR SUPPORT SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 1 radiator support seal from the radiator.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>