<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0011" variety="S0011">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0011_7B93H_T00D5" variety="T00D5">
<name>CRUISE CONTROL SYSTEM</name>
<para id="RM0000044AS00TX" category="J" type-id="300WK" name-id="CC3FP-02" from="201207" to="201210">
<dtccode/>
<dtcname>Cruise Control Switch Circuit</dtcname>
<subpara id="RM0000044AS00TX_01" type-id="60" category="03" proc-id="RM23G0E___000076900000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit sends a signal to the ECM depending on the cruise control switch condition. The battery supplies positive (+) battery voltage to the cruise control switch. Terminal 11 (CCS)*1 or 14 (CCS)*2 of the ECM receives the voltage which varies according to the switch condition.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 1GR-FE</ptxt>
</item>
<item>
<ptxt>*2: for 1KD-FTV</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000044AS00TX_02" type-id="32" category="03" proc-id="RM23G0E___000076A00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E200723E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000044AS00TX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000044AS00TX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000044AS00TX_04_0001" proc-id="RM23G0E___000076B00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the cruise control switch is functioning properly (See page <xref label="Seep01" href="RM0000044B200SX"/>).</ptxt>
<table pgwide="1">
<title>Cruise Control</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Main SW M-CPU</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Cruise control switch (Main CPU) / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Cruise control switch on</ptxt>
<ptxt>OFF: Cruise control switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Cancel Switch</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>CANCEL switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: CANCEL switch on</ptxt>
<ptxt>OFF: CANCEL switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>SET/COAST Switch</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>-SET switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: -SET switch on</ptxt>
<ptxt>OFF: -SET switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>RES/ACC Switch</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>+RES switch signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: +RES switch on</ptxt>
<ptxt>OFF: +RES switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On screen, each item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000044AS00TX_04_0006" fin="true">OK</down>
<right ref="RM0000044AS00TX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0002" proc-id="RM23G0E___000076C00000">
<testtitle>INSPECT SPIRAL CABLE SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the spiral cable sub-assembly (See page <xref label="Seep02" href="RM000002O8X01FX"/>).</ptxt>
</test1>
<test1>
<ptxt>If there are any defects as follows, replace the spiral cable with a new one: scratches, cracks, dents or chips on the connector or spiral cable.</ptxt>
<figure>
<graphic graphicname="E176804E05" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Check the spiral cable.</ptxt>
<test2>
<ptxt>Set the spiral cable to the center position (See page <xref label="Seep01" href="RM000002O8U01IX_01_0004"/>).</ptxt>
</test2>
<test2>
<ptxt>Rotate the spiral cable 2.5 times clockwise and measure the resistance according to the value(s) in the table below. Then rotate the spiral cable 5 times counterclockwise and measure the resistance according to the value(s) in the table below.</ptxt>
<atten3>
<ptxt>As the spiral cable may break, do not rotate the spiral cable more than the specified amount.</ptxt>
</atten3>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3 (CCS) - 1 (CCS)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4 (ECC) - 2 (ECC)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
<test2>
<ptxt>Set the spiral cable to the center position and rotate the spiral cable 2.5 times clockwise. Then, while rotating the spiral cable 5 times counterclockwise, measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3 (CCS) - 1 (CCS)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4 (ECC) - 2 (ECC)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000044AS00TX_04_0003" fin="false">OK</down>
<right ref="RM0000044AS00TX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0003" proc-id="RM23G0E___000076D00000">
<testtitle>INSPECT CRUISE CONTROL SWITCH WIRE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the z7 spiral cable connector.</ptxt>
<figure>
<graphic graphicname="E178683E18" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the A switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>z7-3 (CCS) - A-3 (CCS)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 2.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z7-4 (ECC) - A-1 (ECC)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z7-3 (CCS) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z7-4 (ECC) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000044AS00TX_04_0004" fin="false">OK</down>
<right ref="RM0000044AS00TX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0004" proc-id="RM23G0E___000076E00000">
<testtitle>INSPECT CRUISE CONTROL SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the cruise control switch (See page <xref label="Seep01" href="RM000002MBZ01XX"/>).</ptxt>
<figure>
<graphic graphicname="B215369E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="4" valign="middle" align="center">
<ptxt>3 (CCS) - 1 (ECC)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 2.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Cruise control switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>+RES switch held on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>235 to 245 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>-SET switch held on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>617 to 643 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CANCEL switch held on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1509 to 1571 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000044AS00TX_04_0005" fin="false">OK</down>
<right ref="RM0000044AS00TX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0005" proc-id="RM23G0E___000076F00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SPIRAL CABLE SUB-ASSEMBLY - ECM AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G40 spiral cable connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G56*1 or G57*2 ECM connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 1GR-FE</ptxt>
</item>
<item>
<ptxt>*2: for 1KD-FTV</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for 1GR-FE</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G40-1 (CCS) - G56-11 (CCS)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G40-2 (ECC) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G40-1 (CCS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1KD-FTV</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G40-1 (CCS) - G57-14 (CCS)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G40-2 (ECC) - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G40-1 (CCS) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for 1KD-FTV)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044AS00TX_04_0010" fin="true">A</down>
<right ref="RM0000044AS00TX_04_0011" fin="true">B</right>
<right ref="RM0000044AS00TX_04_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0006">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000044AR00YX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0007">
<testtitle>REPLACE SPIRAL CABLE SUB-ASSEMBLY<xref label="Seep01" href="RM000002O8X01FX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0008">
<testtitle>REPLACE CRUISE CONTROL SWITCH WIRE</testtitle>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0009">
<testtitle>REPLACE CRUISE CONTROL SWITCH<xref label="Seep01" href="RM000002MBZ01XX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202EX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0011">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001OX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044AS00TX_04_0012">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>