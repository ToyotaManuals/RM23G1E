<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000188406UX" category="C" type-id="3038U" name-id="ESU0O-22" from="201207" to="201210">
<dtccode>P0088</dtccode>
<dtcname>Fuel Rail / System Pressure - Too High</dtcname>
<subpara id="RM00000188406UX_01" type-id="60" category="03" proc-id="RM23G0E___00001OX00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<title>P0088</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>After idling for 60 seconds, quickly increase engine speed to 2500 rpm repeatedly for 30 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>The fuel pressure of the common rail exceeds 200000 kPa (2039 kgf/cm<sup>2</sup>, 29000 psi) (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Suction control valve</ptxt>
</item>
<item>
<ptxt>Pressure discharge valve (common rail)</ptxt>
</item>
<item>
<ptxt>Open or short in suction control valve circuit</ptxt>
</item>
<item>
<ptxt>Fuel pressure sensor (common rail)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0088</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Fuel Press</ptxt>
</item>
<item>
<ptxt>Target Common Rail Pressure</ptxt>
</item>
<item>
<ptxt>Target Pump SCV Current</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>For more information on the fuel supply pump assembly (suction control valve) and common rail system, refer to System Description (See page <xref label="Seep02" href="RM000000XSN02OX"/>).</ptxt>
</item>
<item>
<ptxt>When DTC P0088 is stored, check the internal fuel pressure of the common rail by entering the following menus: Powertrain / Engine and ECT / Data List / Fuel Press.</ptxt>
<table pgwide="1">
<title>Reference</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fuel Press</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approximately 30000 to 40000 kPa</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3000 rpm (No engine load)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approximately 45000 to 75000 kPa</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
<item>
<ptxt>Check "Fuel Press", "Target Common Rail Pressure" and "Target Pump SCV Current" in the freeze frame data.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188406UX_02" type-id="64" category="03" proc-id="RM23G0E___00001OY00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<title>P0088 (Internal fuel pressure too high):</title>
<ptxt>The ECM stores this DTC if the fuel pressure inside the common rail is higher than 200000 kPa (2039 kgf/cm<sup>2</sup>, 29000 psi). This DTC indicates that: 1) the suction control valve may be stuck open, or 2) there may be a short in the suction control valve circuit.</ptxt>
<ptxt>If this DTC is stored, the ECM enters fail-safe mode and limits the engine power. The ECM continues operating in fail-safe mode until the ignition switch is turned off.</ptxt>
</topic>
</content5>
</subpara>
<subpara id="RM00000188406UX_06" type-id="32" category="03" proc-id="RM23G0E___00001OZ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A205135E10" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000188406UX_07" type-id="51" category="05" proc-id="RM23G0E___00001P000000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK061X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05KX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188406UX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000188406UX_08_0029" proc-id="RM23G0E___00001P900000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0088)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read Current DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0088</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0088 and "P0190, P0192 and/or P0193"</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When there is an open circuit in the fuel pressure sensor circuit, the fuel pressure sensor outputs the maximum value. Therefore, P0088 may be stored together with DTCs which indicate common rail fuel pressure sensor circuit malfunctions.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0030" fin="false">A</down>
<right ref="RM00000188406UX_08_0034" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0030" proc-id="RM23G0E___00001PA00000">
<testtitle>CHECK FREEZE FRAME DATA (FUEL PRESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Record the stored DTCs and freeze frame data.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Value of Fuel Press is shifting towards 160000 kPa or higher in 3rd and 4th set of freeze frame data</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0031" fin="false">A</down>
<right ref="RM00000188406UX_08_0014" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0031" proc-id="RM23G0E___00001PB00000">
<testtitle>INSPECT ECM (FUEL PRESSURE SENSOR OUTPUT VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the positive (+) probe of an oscilloscope to the fuel pressure sensor input terminal of the ECM connector and the negative (-) probe to the body ground.</ptxt>
<figure>
<graphic graphicname="A177861E21" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check for any noise in the waveform when vibration is applied to the wire harness and connectors between the fuel pressure sensor and ECM.</ptxt>
<figure>
<graphic graphicname="A192850E03" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="A192849E03" width="7.106578999in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C90-16 (PCR1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vibration is applied to the wire harness and connectors</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No noise in the waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>MIL comes on or noise present in the waveform</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0014" fin="false">A</down>
<right ref="RM00000188406UX_08_0025" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0014" proc-id="RM23G0E___00001P300000">
<testtitle>REPLACE SUCTION CONTROL VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the suction control valve (See page <xref label="Seep01" href="RM000004MF600CX"/>).</ptxt>
<atten4>
<ptxt>Before replacing the suction control valve, check that the wire harness between the suction control valve and ECM is not shorted to ground.</ptxt>
<ptxt>When there is a malfunction in the wire harness, Target Pump SCV Current is outside the range of 923 to 1123 mA when idling, and outside the range of 1013 to 1212 mA when the engine is running at 2500 rpm without load.</ptxt>
<ptxt>If there are no problems with the engine condition during idling, it is not necessary to perform the above wire harness inspection.</ptxt>
<ptxt>When the wire harness between the suction control valve and ECM is not shorted to ground, it can be assumed that DTC P0088 was stored due to a malfunction in the operation of the suction control valve. Therefore, it is necessary to replace the suction control valve.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0026" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0026" proc-id="RM23G0E___00001P700000">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802NX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0027" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0027" proc-id="RM23G0E___00001P800000">
<testtitle>PERFORM FUEL SUPPLY PUMP INITIALIZATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform fuel supply pump initialization (See page <xref label="Seep01" href="RM000000TIN05KX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0004" proc-id="RM23G0E___00001P100000">
<testtitle>CHECK DTC OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z6X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and start the engine.</ptxt>
</test1>
<test1>
<ptxt>After idling for 60 seconds, increase the engine speed from idling to 2500 rpm repeatedly for 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the tester to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0088.</ptxt>
</test2>
<test2>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Pending DTC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P0088 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>All Readiness</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>NORMAL</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If STATUS is INCOMPLETE or N/A, race the engine to 2500 rpm repeatedly for 30 seconds and increase the idling time.</ptxt>
</atten4>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0024" fin="false">A</down>
<right ref="RM00000188406UX_08_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0024" proc-id="RM23G0E___00001P500000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188406UX_08_0006" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0025" proc-id="RM23G0E___00001P600000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0032" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0032" proc-id="RM23G0E___00001PC00000">
<testtitle>INSPECT ECM (FUEL PRESSURE SENSOR OUTPUT VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the positive (+) probe of an oscilloscope to the fuel pressure sensor input terminal of the ECM connector and the negative (-) probe to the body ground.</ptxt>
<figure>
<graphic graphicname="A177861E21" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check for any noise in the waveform when vibration is applied to the wire harness and connectors between the fuel pressure sensor and ECM.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C90-16 (PCR1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vibration is applied to the wire harness and connectors</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No noise in the waveform</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>MIL comes on or noise present in the waveform</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0020" fin="false">A</down>
<right ref="RM00000188406UX_08_0006" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0020" proc-id="RM23G0E___00001P400000">
<testtitle>REPLACE COMMON RAIL ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the common rail (See page <xref label="Seep01" href="RM0000044W300JX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0033" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0033" proc-id="RM23G0E___00001PD00000">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802NX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0006" proc-id="RM23G0E___00001P200000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z6X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and start the engine.</ptxt>
</test1>
<test1>
<ptxt>After idling for 60 seconds, increase the engine speed from idling to 2500 rpm repeatedly for 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
<atten4>
<ptxt>Perform the following procedure using the tester to determine whether or not the DTC judgment has been carried out.</ptxt>
</atten4>
<test2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</test2>
<test2>
<ptxt>Input DTC P0088.</ptxt>
</test2>
<test2>
<ptxt>Check that STATUS is NORMAL. If STATUS is INCOMPLETE or N/A, race the engine to 2500 rpm repeatedly for 30 seconds and increase the idling time.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000188406UX_08_0012" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188406UX_08_0012">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM00000188406UX_08_0034">
<testtitle>GO TO FUEL RAIL PRESSURE SENSOR CIRCUIT<xref label="Seep01" href="RM00000188006NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>