<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B7_T00KV" variety="T00KV">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000002VHD09JX" category="J" type-id="802XH" name-id="TD3AO-05" from="201210">
<dtccode/>
<dtcname>All Door Entry Lock/Unlock Functions and Wireless Functions do not Operate</dtcname>
<subpara id="RM000002VHD09JX_01" type-id="60" category="03" proc-id="RM23G0E___0000EUD00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the entry operation and wireless operation of the door lock functions do not operate, a malfunction or wave interference may be occurring in either of the following: 1) the signal communication line between the door control receiver and certification ECU (the line is used by entry and wireless operations); or 2) the electrical key transmitter.</ptxt>
</content5>
</subpara>
<subpara id="RM000002VHD09JX_02" type-id="32" category="03" proc-id="RM23G0E___0000EUE00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B211013E07" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002VHD09JX_03" type-id="51" category="05" proc-id="RM23G0E___0000EUF00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>The entry and start system (for Entry Function) uses a multiplex communication system (LIN communication system) and the CAN communication system. Inspect the communication function by following How to Proceed with Troubleshooting (See page <xref label="Seep01" href="RM000000XU70CAX"/>). Troubleshoot the entry and start system (for Entry Function) after confirming that the communication systems are functioning properly.</ptxt>
</item>
<item>
<ptxt>When using the intelligent tester with the engine switch off to troubleshoot: Connect the intelligent tester to the DLC3 and turn a courtesy light switch on and off at 1.5-second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</item>
<item>
<ptxt>Check that there are no electrical key transmitters in the vehicle.</ptxt>
</item>
<item>
<ptxt>Before performing the inspection, check that DTC B1242 (wireless door lock control) is not output (See page <xref label="Seep02" href="RM000002BZF045X"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>When checking the entry lock operation multiple times, the lock operation may be limited to 2 consecutive operations depending on the settings. In order to perform the entry lock operation 3 or more times, an unlock operation must be performed once (any type of unlock operation is sufficient). However, only consecutive entry lock operations are limited. Using the wireless lock or other types of lock operations, it is possible to perform consecutive lock operations without this limitation.</ptxt>
</item>
<item>
<ptxt>Before replacing the certification ECU, refer to the Service Bulletin.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000002VHD09JX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002VHD09JX_07_0001" proc-id="RM23G0E___0000EUG00001">
<testtitle>CHECK POWER DOOR LOCK CONTROL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the door control switch on the multiplex network master switch assembly is operated, check that the doors unlock and lock according to the switch operation (See page <xref label="Seep01" href="RM000002T6K05GX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door locks operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0002" fin="false">OK</down>
<right ref="RM000002VHD09JX_07_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0002" proc-id="RM23G0E___0000EUH00001">
<testtitle>CHECK ELECTRICAL KEY TRANSMITTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When another registered electrical key transmitter is used, check that the wireless and entry functions operate normally (See page <xref label="Seep01" href="RM000002U9W05DX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Entry function operates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Entry function does not operate</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0017" fin="false">A</down>
<right ref="RM000002VHD09JX_07_0004" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0017" proc-id="RM23G0E___0000EUO00001">
<testtitle>CHECK ELECTRICAL KEY TRANSMITTER (LED)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the transmitter LED illuminates 3 times when a switch is pressed 3 times.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Transmitter LED does not illuminate when switch is pressed 3 times</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Transmitter LED illuminates 3 times when switch is pressed 3 times</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Transmitter LED does not illuminate second or third time</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the transmitter LED does not illuminate the second or third time, replace the transmitter battery as it is depleted.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0003" fin="false">A</down>
<right ref="RM000002VHD09JX_07_0008" fin="true">B</right>
<right ref="RM000002VHD09JX_07_0009" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0003" proc-id="RM23G0E___0000EUI00001">
<testtitle>INSPECT TRANSMITTER BATTERY (VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the battery from the electrical key transmitter that does not operate (See page <xref label="Seep02" href="RM0000039HF030X"/>).</ptxt>
<figure>
<graphic graphicname="B246343E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Attach a lead wire (0.6 mm (0.0236 in.) in diameter or less including wire sheath) with tape or equivalent to the negative terminal.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lead Wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not wrap the lead wire around a terminal, wedge it between terminals, or solder it. The terminal may be deformed or damaged, and the battery will not be able to be installed correctly.</ptxt>
</atten3>
</test1>
<test1>
<ptxt>Carefully pull the lead wire out from the position shown in the illustration and install the previously removed transmitter battery.</ptxt>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the transmitter battery voltage waveform.</ptxt>
<atten4>
<ptxt>When measuring the battery voltage, bring the electrical key transmitter within the entry operating range while operating the lock sensor of a door handle to perform the measurement. For the entry operating range, refer to System Description (See page <xref label="Seep01" href="RM000000XUC0ACX"/>).</ptxt>
</atten4>
<figure>
<graphic graphicname="B246344E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery positive (+) - Battery negative (-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.5 V/DIV., 100 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, all doors closed and lock sensor touched</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.5 to 3.2 V (Refer to waveform)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0022" fin="true">OK</down>
<right ref="RM000002VHD09JX_07_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0004" proc-id="RM23G0E___0000EUJ00001">
<testtitle>CHECK WAVE ENVIRONMENT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bring the electrical key transmitter near the door control receiver and perform a wireless and entry function operation check.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the electrical key transmitter is brought near the door control receiver, the possibility of wave interference decreases, and it can be determined if wave interference is causing the problem symptom.</ptxt>
</item>
<item>
<ptxt>If the inspection result is that the problem only occurs in certain locations or at certain times of day, the possibility of wave interference is high. Also, added vehicle components may cause wave interference. If installed, remove them and perform the operation check.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Wireless and entry functions operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0010" fin="true">OK</down>
<right ref="RM000002VHD09JX_07_0014" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0014" proc-id="RM23G0E___0000EUM00001">
<testtitle>CHECK POWER SOURCE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the combination meter display turns off when the engine switch is turned off.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Combination meter turns off.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>This confirms that systems other than the entry and start system are correctly detecting the state of the power source.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0015" fin="false">OK</down>
<right ref="RM000002VHD09JX_07_0028" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0015" proc-id="RM23G0E___0000EUN00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CERTIFICATION ECU - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G38 ECU connector.</ptxt>
<figure>
<graphic graphicname="B199585E11" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-17 (CUTB) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-1 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-15 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Certification ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0005" fin="false">OK</down>
<right ref="RM000002VHD09JX_07_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0005" proc-id="RM23G0E___0000EUK00001">
<testtitle>CHECK HARNESS AND CONNECTOR (DOOR CONTROL RECEIVER - CERTIFICATION ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the R17 receiver connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G39 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R17-4 (+5) - G39-5 (RCO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R17-5 (DATA) - G39-15 (RDA)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R17-2 (RSSI) - G39-16 (RSSI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G39-5 (RCO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G39-15 (RDA) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G39-16 (RSSI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R17-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0006" fin="false">OK</down>
<right ref="RM000002VHD09JX_07_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0006" proc-id="RM23G0E___0000EUL00001">
<testtitle>CHECK DOOR CONTROL RECEIVER (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the door control receiver with a new or normally functioning one.</ptxt>
<list1 type="unordered">
<item>
<ptxt>for 5 Door: (See page <xref label="Seep01" href="RM0000046JL01NX"/>).</ptxt>
</item>
<item>
<ptxt>for 3 Door: (See page <xref label="Seep02" href="RM0000046JQ00IX"/>).</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Check that the wireless and entry functions operate normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Wireless and entry functions operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002VHD09JX_07_0013" fin="true">OK</down>
<right ref="RM000002VHD09JX_07_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0007">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM0000011G908TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0008">
<testtitle>REPLACE ELECTRICAL KEY TRANSMITTER</testtitle>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0009">
<testtitle>REPLACE TRANSMITTER BATTERY<xref label="Seep01" href="RM0000039HF030X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0022">
<testtitle>REPLACE ELECTRICAL KEY TRANSMITTER</testtitle>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0010">
<testtitle>AFFECTED BY WAVE INTERFERENCE</testtitle>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0028">
<testtitle>GO TO ENTRY AND START SYSTEM (FOR START FUNCTION)<xref label="Seep01" href="RM000000YEF0HBX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0012">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000002VHD09JX_07_0013">
<testtitle>END (DOOR CONTROL RECEIVER IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>