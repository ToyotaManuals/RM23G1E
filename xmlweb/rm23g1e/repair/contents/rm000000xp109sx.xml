<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94I_T00E6" variety="T00E6">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1GR-FE)</name>
<para id="RM000000XP109SX" category="C" type-id="302QH" name-id="AT7MN-04" from="201210">
<dtccode>P0781</dtccode>
<dtcname>1-2 Shift (1-2 Shift Valve)</dtcname>
<subpara id="RM000000XP109SX_01" type-id="60" category="03" proc-id="RM23G0E___00008AS00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<topic>
<ptxt>The 1-2 shift valve performs shifting to 1st gear and other gears.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0781</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gear required by the ECM does not match the actual gear when driving (2-trip detection logic*1, 1-trip detection logic*2).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Valve body is blocked or stuck (1-2 shift valve)</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SLT remains open or closed</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ OBD</ptxt>
</item>
<item>
<ptxt>*2: w/o OBD</ptxt>
</item>
</list1>
</topic>
</content5>
</subpara>
<subpara id="RM000000XP109SX_02" type-id="64" category="03" proc-id="RM23G0E___00008AT00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates that the 1-2 shift valve in the valve body is locked in the direction that the spring compresses.</ptxt>
<ptxt>The ECM commands gear shifts by turning the shift solenoid valves ON/OFF and controls the oil pressure applied to the valves in the valve body.</ptxt>
<ptxt>The ECM calculates the actual transmission gear by comparing the signals from the input speed sensor NT and output speed sensor SP2. The ECM can detect mechanical problems in the shift solenoids, valve body and automatic transmission (clutch, brake, gear, etc.). If the ECM detects that the actual gear position and commanded gear position are different, it will illuminate the MIL and store the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XP109SX_06" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000XP109SX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XP109SX_07_0001" proc-id="RM23G0E___00008AU00001">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P0781)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only P0781 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0781 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any other codes besides P0781 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000XP109SX_07_0002" fin="false">A</down>
<right ref="RM000000XP109SX_07_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XP109SX_07_0002" proc-id="RM23G0E___00008AV00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (RUNNING TEST)</testtitle>
<content6 releasenbr="1">
<atten2>
<ptxt>This test should always be performed with at least 2 people.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the test while the ATF temperature is 50 to 80°C (122 to 176°F).</ptxt>
</item>
<item>
<ptxt>Perform the test with the A/C turned off.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000W770V4X"/>).</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
<atten4>
<ptxt>While driving, the shift position can be forcibly changed with the intelligent tester.</ptxt>
<ptxt>Comparing the shift position commanded by the Active Test with the actual shift position enables you to confirm the problem (See page <xref label="Seep02" href="RM000000O8L0N0X"/>).</ptxt>
</atten4>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Control the Shift Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve and set each shift position</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Press "→" button: Shift up</ptxt>
</item>
<item>
<ptxt>Press "←" button: Shift down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Possible to check operation of the shift solenoid valves.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<ptxt>50 km/h (31 mph) or less.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This test can be conducted when the vehicle speed is 50 km/h (31 mph) or less.</ptxt>
</item>
<item>
<ptxt>The 4th to 5th up-shift must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>The 5th to 4th down-shift must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>Do not operate the accelerator pedal for at least 2 seconds after shifting and do not shift successively.</ptxt>
</item>
<item>
<ptxt>The shift position commanded by the ECM is shown in the Data List display on the tester.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Gear position changes in accordance with the tester command.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XP109SX_07_0010" fin="false">OK</down>
<right ref="RM000000XP109SX_07_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XP109SX_07_0010" proc-id="RM23G0E___000088F00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SHIFT SOLENOID VALVE SLT)
</testtitle>
<content6 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the test while the ATF temperature is 50 to 80°C (122 to 176°F).</ptxt>
</item>
<item>
<ptxt>Be careful to prevent SST's hose from interfering with the exhaust pipe.</ptxt>
</item>
<item>
<ptxt>Perform the test with the A/C off.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<test1>
<ptxt>Remove the test plug from the transmission case and connect SST.</ptxt>
<figure>
<graphic graphicname="C214323E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<sst>
<sstitem>
<s-number>09992-00095</s-number>
<s-subnumber>09992-00231</s-subnumber>
<s-subnumber>09992-00271</s-subnumber>
</sstitem>
</sst>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Measure the line pressure with SST.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
</test1>
<test1>
<ptxt>Measure the line pressure.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLT)*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid SLT and raise line pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>OFF: Line pressure up (when Active Test "Activate the Solenoid (SLT)" is performed, ECM commands SLT solenoid to turn OFF)</ptxt>
</item>
<item>
<ptxt>ON: No action (normal operation)</ptxt>
</item>
</list1>
</atten4>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Vehicle stopped.</ptxt>
</item>
<item>
<ptxt>Engine idling.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Activate the Solenoid (SLT) in the Active Test is performed to check the line pressure changes by connecting SST to the automatic transmission, which is used in the Hydraulic Test (See page <xref label="Seep01" href="RM000000W7B0NFX"/>) as well. Note that the pressure values in the Active Test and Hydraulic Test are different.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The line pressure changes as specified when performing the Active Test.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000XP109SX_07_0003" fin="false">OK</down>
<right ref="RM000000XP109SX_07_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XP109SX_07_0003" proc-id="RM23G0E___00008AW00001">
<testtitle>CLEAR DTC AND PERFORM RUNNING TEST</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000W770V4X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs again after conducting the Monitor Drive Pattern (See page <xref label="Seep02" href="RM000000W7K0PUX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC code output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XP109SX_07_0006" fin="true">OK</down>
<right ref="RM000000XP109SX_07_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XP109SX_07_0006">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000XP109SX_07_0004">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G9081X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XP109SX_07_0005">
<testtitle>REPAIR OR REPLACE TRANSMISSION VALVE BODY ASSEMBLY<xref label="Seep01" href="RM0000013CM04BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XP109SX_07_0008">
<testtitle>REPLACE SHIFT SOLENOID VALVE SLT<xref label="Seep01" href="RM0000013FG02LX_01_0005"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XP109SX_07_0009">
<testtitle>REPAIR OR REPLACE AUTOMATIC TRANSMISSION ASSEMBLY<xref label="Seep01" href="RM0000018ZD046X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>