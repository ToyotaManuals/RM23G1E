<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S0029" variety="S0029">
<name>WINDOW / GLASS</name>
<ttl id="12064_S0029_7B9FN_T00PB" variety="T00PB">
<name>WINDOW DEFOGGER SYSTEM</name>
<para id="RM000000WPS09CX" category="U" type-id="3001G" name-id="WS3TE-01" from="201207">
<name>TERMINALS OF ECU</name>
<subpara id="RM000000WPS09CX_z0" proc-id="RM23G0E___0000ICY00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY</ptxt>
<figure>
<graphic graphicname="E132743E15" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the G25 amplifier connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G25-21 (B) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>V - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery power source</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G25-1 (IG+) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>G25-1 (IG+) - G25-14 (GND)</ptxt>
</entry>
<entry>
<ptxt>L - W-B</ptxt>
</entry>
<entry>
<ptxt>Ignition power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G25-14 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Reconnect the G25 air conditioning amplifier connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G25-38 (RDFG) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear defogger signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, rear window defogger switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G25-38 (RDFG) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear defogger signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, rear window defogger switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction in the air conditioning amplifier.</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>CHECK INTEGRATION CONTROL AND PANEL ASSEMBLY</ptxt>
<figure>
<graphic graphicname="E196915E06" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the G23 air conditioning control connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G23-1 (+B) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-5 (IG) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-6 (ACC) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction in the wire harness.</ptxt>
</item>
</list1>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>