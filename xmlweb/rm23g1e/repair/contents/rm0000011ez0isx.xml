<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001P" variety="S001P">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001P_7B99K_T00J8" variety="T00J8">
<name>NAVIGATION SYSTEM (for HDD)</name>
<para id="RM0000011EZ0ISX" category="J" type-id="3016Z" name-id="NS71G-03" from="201210">
<dtccode/>
<dtcname>Illumination Circuit</dtcname>
<subpara id="RM0000011EZ0ISX_01" type-id="60" category="03" proc-id="RM23G0E___0000C7K00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Power is supplied to the display and navigation module display and the steering pad switch illumination when the light control switch is in the TAIL or HEAD position.</ptxt>
</content5>
</subpara>
<subpara id="RM0000011EZ0ISX_02" type-id="32" category="03" proc-id="RM23G0E___0000C7L00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E205035E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000011EZ0ISX_03" type-id="51" category="05" proc-id="RM23G0E___0000C7M00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten2>
<ptxt>The vehicle is equipped with an SRS (Supplemental Restraint System) which includes components such as airbags. Before servicing (including removal or installation of parts), be sure to read the Precaution in the SRS section (See page <xref label="Seep01" href="RM000000KT10J3X"/>).</ptxt>
</atten2>
</content5>
</subpara>
<subpara id="RM0000011EZ0ISX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000011EZ0ISX_05_0001" proc-id="RM23G0E___0000C7N00001">
<testtitle>CHECK ILLUMINATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the illumination for the display and navigation display, steering pad switch, glove box or other parts (hazard switch, transmission control switch, etc.) comes on when the light control switch is turned to the HEAD or TAIL position.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Illumination comes on for all components except steering pad switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Illumination comes on for all components except display and navigation module display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No illumination comes on (display and navigation module display, hazard switch, glove box, etc.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Illumination comes on for all components except steering pad switch and display and navigation module display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011EZ0ISX_05_0003" fin="false">A</down>
<right ref="RM0000011EZ0ISX_05_0017" fin="false">B</right>
<right ref="RM0000011EZ0ISX_05_0007" fin="true">C</right>
<right ref="RM0000011EZ0ISX_05_0018" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0003" proc-id="RM23G0E___0000C7P00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SPIRAL CABLE - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G40 spiral cable sub-assembly connector.</ptxt>
<figure>
<graphic graphicname="E121218E30" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G40-12 (IL+2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light control switch TAIL or HEAD</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Spiral Cable Sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011EZ0ISX_05_0010" fin="false">OK</down>
<right ref="RM0000011EZ0ISX_05_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0010" proc-id="RM23G0E___0000C7R00001">
<testtitle>INSPECT STEERING PAD SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the steering pad switch assembly (See page <xref label="Seep01" href="RM0000039SA01CX"/>).</ptxt>
<figure>
<graphic graphicname="E192966E13" width="7.106578999in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the positive (+) lead of the battery to terminal 5 (IL+2) and the negative (-) lead of the battery to terminal 8 (EAU) of the steering pad switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Check if the illumination for the steering pad switch assembly comes on.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Illumination for the steering pad switch assembly comes on.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011EZ0ISX_05_0009" fin="false">OK</down>
<right ref="RM0000011EZ0ISX_05_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0009" proc-id="RM23G0E___0000C7Q00001">
<testtitle>INSPECT SPIRAL CABLE SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the spiral cable sub-assembly (See page <xref label="Seep01" href="RM000002O8X01NX"/>).</ptxt>
<figure>
<graphic graphicname="C152441E21" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>8 (EAU) - 4 (EAU)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Spiral cable is turned 2.5 rotations counterclockwise</ptxt>
</entry>
<entry morerows="5" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Spiral cable is centered</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Spiral cable is turned 2.5 rotations clockwise</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>5 (IL+2) - 12 (IL+2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Spiral cable is turned 2.5 rotations counterclockwise</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Spiral cable is centered</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Spiral cable is turned 2.5 rotations clockwise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering Pad Switch Assembly Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten2>
<ptxt>The spiral cable is an important part of the SRS airbag system. Incorrect removal or installation of the spiral cable may prevent the airbag from deploying. Be sure to read Precaution in the SRS section (See page <xref label="Seep02" href="RM000000KT10J3X"/>).</ptxt>
</atten2>
</test1>
</content6>
<res>
<down ref="RM0000011EZ0ISX_05_0002" fin="false">OK</down>
<right ref="RM0000011EZ0ISX_05_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0002" proc-id="RM23G0E___0000C7O00001">
<testtitle>CHECK HARNESS AND CONNECTOR (DISPLAY AND NAVIGATION MODULE DISPLAY - SPIRAL CABLE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H4 display and navigation module display connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G40 spiral cable sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H4-23 (SWG) - G40-4 (EAU)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H4-23 (SWG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011EZ0ISX_05_0030" fin="true">OK</down>
<right ref="RM0000011EZ0ISX_05_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0017" proc-id="RM23G0E___0000C7S00001">
<testtitle>CHECK HARNESS AND CONNECTOR (DISPLAY AND NAVIGATION MODULE DISPLAY - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H4 display and navigation module display connector.</ptxt>
<figure>
<graphic graphicname="B122460E45" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>H4-12 (ILL+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light control switch TAIL or HEAD</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Display and Navigation Module Display)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011EZ0ISX_05_0030" fin="true">OK</down>
<right ref="RM0000011EZ0ISX_05_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0018" proc-id="RM23G0E___0000C7T00001">
<testtitle>CHECK HARNESS AND CONNECTOR (DISPLAY AND NAVIGATION MODULE DISPLAY - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H4 display and navigation module display connector.</ptxt>
<figure>
<graphic graphicname="B122460E50" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>H4-11 (ILL-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Display and Navigation Module Display)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000011EZ0ISX_05_0030" fin="true">OK</down>
<right ref="RM0000011EZ0ISX_05_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0007">
<testtitle>GO TO LIGHTING SYSTEM<xref label="Seep01" href="RM000000PKJ0IUX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0021">
<testtitle>REPLACE STEERING PAD SWITCH ASSEMBLY<xref label="Seep01" href="RM0000039SA01CX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0022">
<testtitle>REPLACE SPIRAL CABLE SUB-ASSEMBLY<xref label="Seep01" href="RM000002O8X01NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0030">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000046SY00JX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011EZ0ISX_05_0031">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>