<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8Z4_T008S" variety="T008S">
<name>COMMON RAIL (w/ DPF)</name>
<para id="RM0000044W100SX" category="A" type-id="30014" name-id="FU8CG-03" from="201210">
<name>INSTALLATION</name>
<subpara id="RM0000044W100SX_01" type-id="11" category="10" proc-id="RM23G0E___00005RJ00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipes and fuel inlet pipe, clean them with a brush and compressed air.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000044W100SX_02" type-id="01" category="01">
<s-1 id="RM0000044W100SX_02_0001" proc-id="RM23G0E___00005RK00001">
<ptxt>INSTALL COMMON RAIL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the common rail with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>38</t-value1>
<t-value2>387</t-value2>
<t-value4>28</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the pressure discharge valve connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044W100SX_02_0047" proc-id="RM23G0E___000045J00000">
<ptxt>INSTALL OIL FILTER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check and clean the oil filter installation surface.</ptxt>
</s2>
<s2>
<ptxt>Apply clean engine oil to the gasket of a new oil filter.</ptxt>
</s2>
<s2>
<ptxt>Lightly screw the oil filter into place by hand. Tighten it until the gasket contacts the seat.</ptxt>
</s2>
<s2>
<ptxt>Using SST, tighten the oil filter. Depending on the space available, choose from the following.</ptxt>
<figure>
<graphic graphicname="A223503E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09228-07501</s-number>
</sstitem>
</sst>
<s3>
<ptxt>If enough space is available, use a torque wrench to tighten the oil filter.</ptxt>
<torque>
<torqueitem>
<t-value1>17</t-value1>
<t-value2>173</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>If enough space is not available to use a torque wrench, tighten the oil filter 3/4 of a turn by hand or with a common wrench.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0048" proc-id="RM23G0E___00006C500001">
<ptxt>INSTALL VACUUM CONTROL VALVE SET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the vacuum control valve set with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 3 vacuum hoses and 2 vacuum switching valve connectors.</ptxt>
<figure>
<graphic graphicname="A246255E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>White Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Green Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Swirl Control Valve Actuator</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>When connecting the hoses, match the colors of the paint marks on the hoses to the colors of the paint marks on the swirl control valve as shown in the illustration.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0042" proc-id="RM23G0E___00005Q800001">
<ptxt>INSTALL NO. 2 NOZZLE LEAKAGE PIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the No. 2 nozzle leakage pipe with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A239626E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Temporarily install a new gasket and the union bolt.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 3 bolts and union bolt.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
<subtitle>for union bolt</subtitle>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0043" proc-id="RM23G0E___00005Q900001">
<ptxt>INSTALL NO. 3 NOZZLE LEAKAGE PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of fuel to the O-ring of the fuel check valve.</ptxt>
<figure>
<graphic graphicname="A239519E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Temporarily install the No. 3 nozzle leakage pipe with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="A239625E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Fuel Check Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Temporarily install a new gasket and fuel check valve.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 2 bolts and fuel check valve.</ptxt>
<torque>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
<subtitle>for fuel check valve</subtitle>
<torqueitem>
<t-value1>32</t-value1>
<t-value2>321</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Make sure the gasket of fuel check valve A contacts the No. 3 nozzle leakage pipe as shown in the illustration when tightening the fuel check valve.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the 2 fuel hoses.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0044" proc-id="RM23G0E___00005QA00001">
<ptxt>INSTALL NO. 2 FUEL PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of fuel to the O-ring of the fuel check valve.</ptxt>
<figure>
<graphic graphicname="A239520E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Temporarily install the No. 2 fuel pipe with a new gasket and the union bolt.</ptxt>
<figure>
<graphic graphicname="A239623E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Fuel Check Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Temporarily install a new gasket and the fuel check valve.</ptxt>
</s2>
<s2>
<ptxt>Using a 6 mm hexagon wrench, tighten the union bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Make sure the gasket of union bolt A contacts the No. 2 fuel pipe as shown in the illustration when tightening the union bolt.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Tighten the fuel check valve.</ptxt>
<torque>
<torqueitem>
<t-value1>32</t-value1>
<t-value2>321</t-value2>
<t-value4>23</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0045" proc-id="RM23G0E___00005QB00001">
<ptxt>INSTALL NO. 4 INJECTION PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing an injector, it is necessary to replace the 4 injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>Keep the joints of the injection pipe clean.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Temporarily install the No. 4 injection pipe with the union nuts.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 No. 2 injection pipe clamps with the bolt and nut.</ptxt>
<torque>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, tighten the injection pipe union nut on the common rail side.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, tighten the injection pipe union nuts on the injector side.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0049" proc-id="RM23G0E___00005QE00001">
<ptxt>INSTALL NO. 1 FUEL PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the No. 1 fuel pipe with the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install a new gasket with the union bolt.</ptxt>
</s2>
<s2>
<ptxt>Tighten the union bolt and 4 bolts in the order shown in the illustration.</ptxt>
<torque>
<subtitle>for union bolt</subtitle>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>306</t-value2>
<t-value4>22</t-value4>
</torqueitem>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the No. 2 fuel pipe (See page <xref label="Seep01" href="RM0000028RU03BX"/>).</ptxt>
<figure>
<graphic graphicname="A239663E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" align="center" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Fuel Pipe</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0050" proc-id="RM23G0E___00006C800001">
<ptxt>INSTALL INTAKE AIR CONNECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set a new gasket on the intake manifold.</ptxt>
<figure>
<graphic graphicname="A222611E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Claw</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Make sure the claw of the gasket faces the intake manifold as shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the intake air connector with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0051" proc-id="RM23G0E___00006C900001">
<ptxt>INSTALL GAS FILTER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the gas filter bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the clamp and connect the wire harness.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0052" proc-id="RM23G0E___00006CA00001">
<ptxt>INSTALL NO. 1 GAS FILTER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1gas filter to the gas filter bracket.</ptxt>
</s2>
<s2>
<ptxt>Connect the vacuum hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0053" proc-id="RM23G0E___00006CB00001">
<ptxt>INSTALL THROTTLE BODY BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the throttle body bracket with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A244614E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Tighten the 3 bolts of the throttle body bracket in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0054" proc-id="RM23G0E___00006CC00001">
<ptxt>INSTALL EMISSION CONTROL VALVE BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the emission control valve bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0055" proc-id="RM23G0E___000032Q00000">
<ptxt>INSTALL MANIFOLD ABSOLUTE PRESSURE SENSOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the manifold absolute pressure sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the vacuum hose and manifold absolute pressure sensor connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0056" proc-id="RM23G0E___00006CD00001">
<ptxt>CONNECT WIRE HARNESS
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 clamps and connect the wire harness to the cowl top panel.</ptxt>
</s2>
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Connect the wire harness with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>22</t-value1>
<t-value2>219</t-value2>
<t-value4>16</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the wire harness with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>131</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0046" proc-id="RM23G0E___00005RM00001">
<ptxt>INSTALL ELECTRIC EGR CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000004KSH004X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000044W100SX_02_0057" proc-id="RM23G0E___00005RN00001">
<ptxt>INSTALL COWL TOP VENTILATOR LOUVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000001OV103XX_01_0003"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000044W100SX_02_0028" proc-id="RM23G0E___00005RL00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000044W100SX_02_0029" proc-id="RM23G0E___00001CC00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
<s2>
<ptxt>Tighten the cylinder block drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Fill the radiator with TOYOTA Super Long Life Coolant (SLLC) to the B line of the reservoir tank.</ptxt>
<spec>
<title>Standard Capacity</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>for Automatic Transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>14.9 liters (15.7 US qts, 13.1 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>13.1 liters (13.8 US qts, 11.5 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>for Manual Transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>15.0 liters (15.8 US qts, 13.2 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>13.2 liters (13.9 US qts, 11.6 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</item>
<item>
<ptxt>Please contact your TOYOTA dealer for further details.</ptxt>
</item>
<item>
<ptxt>for Cold Area Specification Vehicles:</ptxt>
<ptxt>Please contact any authorized TOYOTA dealer or repairer or another duly qualified and equipped professional for further details.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<ptxt>Never use water as a substitute for engine coolant.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Press the inlet and outlet radiator hoses several times by hand, and then check the level of the coolant.</ptxt>
<ptxt>If the coolant level drops below the B line, add TOYOTA SLLC to the B line.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Using a wrench, install the vent plug.</ptxt>
<torque>
<torqueitem>
<t-value1>2.0</t-value1>
<t-value2>20</t-value2>
<t-value3>18</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed air from the cooling system.</ptxt>
<s3>
<ptxt>Warm up the engine until the thermostat opens. While the thermostat is open, circulate the coolant for several minutes.</ptxt>
</s3>
<s3>
<ptxt>Maintain the engine speed at 2500 to 3000 rpm.</ptxt>
</s3>
<s3>
<ptxt>Press the inlet and outlet radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<ptxt>When pressing the radiator hoses:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the radiator fan.</ptxt>
</item>
</list1>
</atten2>
</s3>
<s3>
<ptxt>Stop the engine and wait until the coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s3>
</s2>
<s2>
<ptxt>After the coolant cools down, check that the coolant level is at the FULL line.</ptxt>
<ptxt>If the coolant level is below the FULL line, add TOYOTA SLLC to the FULL line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0030" proc-id="RM23G0E___00005PY00000">
<ptxt>BLEED AIR FROM FUEL SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using the hand pump mounted on the fuel filter cap, bleed air from the fuel system. Continue pumping until the pump resistance increases.</ptxt>
<figure>
<graphic graphicname="A223284" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The maximum hand pump pumping speed is 2 strokes per second.</ptxt>
</item>
<item>
<ptxt>The hand pump must be pushed with a full stroke during pumping.</ptxt>
</item>
<item>
<ptxt>When the fuel pressure at the supply pump inlet port reaches a saturated pressure, the hand pump resistance increases.</ptxt>
</item>
<item>
<ptxt>If pumping is interrupted during the air bleeding process, fuel in the fuel line may return to the fuel tank. Continue pumping until the hand pump resistance increases.</ptxt>
</item>
<item>
<ptxt>If the hand pump resistance does not increase despite consecutively pumping 200 times or more, there may be a fuel leak between the fuel tank and fuel filter, the hand pump may be malfunctioning, or the vehicle may have run out of fuel.</ptxt>
</item>
<item>
<ptxt>If air bleeding using the hand pump is incomplete, the common rail pressure does not rise to the pressure range necessary for normal use and the engine cannot be started.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Check if the engine starts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even if air bleeding using the hand pump has been completed, the starter may need to be cranked for 10 seconds or more to start the engine.</ptxt>
</item>
<item>
<ptxt>Do not crank the engine continuously for more than 20 seconds. The battery may be discharged.</ptxt>
</item>
<item>
<ptxt>Use a fully-charged battery.</ptxt>
</item>
</list1>
</atten3>
<s3>
<ptxt>When the engine can be started, proceed to the next step.</ptxt>
</s3>
<s3>
<ptxt>If the engine cannot be started, bleed air again using the hand pump until the hand pump resistance increases (refer to the procedures above). Then start the engine.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Turn the engine switch off.</ptxt>
</s2>
<s2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the engine switch on (IG) and turn the intelligent tester on.</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z7X"/>).</ptxt>
</s2>
<s2>
<ptxt>Start the engine.*1</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Test the Fuel Leak.*2</ptxt>
<figure>
<graphic graphicname="A194065E05" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reference</ptxt>
<ptxt>(Active Test Operation)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Perform the following test 5 times with on/off intervals of 10 seconds: Active Test / Test the Fuel Leak.*3</ptxt>
</s2>
<s2>
<ptxt>Allow the engine to idle for 3 minutes or more after performing the Active Test for the 5th time.</ptxt>
<figure>
<graphic graphicname="A192848E04" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>When the Active Test "Test the Fuel Leak" is used to change the pump control mode, the actual fuel pressure inside the common rail drops below the target fuel pressure when the Active Test is off, but this is normal and does not indicate a pump malfunction.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</s2>
<s2>
<ptxt>Read Current DTCs.</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK0Z7X"/>).</ptxt>
<atten4>
<ptxt>It is necessary to clear the DTCs as DTC P1604 or P1605 may be stored when air is bled from the fuel system after replacing or repairing fuel system parts.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Repeat steps *1 to *3.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</s2>
<s2>
<ptxt>Read Current DTCs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0031" proc-id="RM23G0E___00001CD00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Before each inspection, turn the A/C switch off.</ptxt>
</atten3>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leaks. If no external leaks are found, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0032" proc-id="RM23G0E___00005PX00000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Perform the Active Test.</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the engine switch on (IG).</ptxt>
</s3>
<s3>
<ptxt>Turn the intelligent tester on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</s3>
<s3>
<ptxt>Perform the Active Test.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Intelligent Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Test the Fuel Leak</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pressurize common rail interior and check for fuel leaks</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Stop/Start</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>The fuel pressure inside the common rail increases to the specified value and the engine speed increases to 2000 rpm when the Active Test is performed.</ptxt>
</item>
<item>
<ptxt>The above conditions are maintained while the Active Test is being performed.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0033" proc-id="RM23G0E___000042S00000">
<ptxt>INSPECT FOR OIL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine. Make sure that there are no oil leaks from the areas that were worked on.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W100SX_02_0034" proc-id="RM23G0E___00005Q700001">
<ptxt>INSPECT ENGINE OIL LEVEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up the engine, stop the engine and wait 5 minutes. The engine oil level should be between the dipstick low level mark and full level mark.</ptxt>
<ptxt>If low, check for leakage and add oil up to the full level mark.</ptxt>
<atten3>
<ptxt>Do not fill engine oil above the full level mark.</ptxt>
</atten3>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>