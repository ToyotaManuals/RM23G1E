<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B98Z_T00IN" variety="T00IN">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM000003XYS05EX" category="C" type-id="804GS" name-id="AVBIM-02" from="201210">
<dtccode>CB-13</dtccode>
<dtcname>USB Over Current Detection</dtcname>
<subpara id="RM000003XYS05EX_01" type-id="60" category="03" proc-id="RM23G0E___0000BPL00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>CB-13</ptxt>
</entry>
<entry valign="middle">
<ptxt>A "iPod" or USB device overcurrent malfunction</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>"iPod" or USB device</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>No. 1 stereo jack adapter assembly</ptxt>
</item>
<item>
<ptxt>Multi-media interface ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003XYS05EX_02" type-id="32" category="03" proc-id="RM23G0E___0000BPM00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E183263E69" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003XYS05EX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003XYS05EX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003XYS05EX_04_0001" proc-id="RM23G0E___0000BPN00001">
<testtitle>REPLACE USB Device OR "iPod"</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the USB device or "iPod" from the stereo jack adapter.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<atten4>
<ptxt>When this DTC has been stored, it is necessary to turn off the ignition switch to make it possible for the vehicle to recognize a new device when it is connected.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Turn the ignition switch to ACC.</ptxt>
</test1>
<test1>
<ptxt>Connect a known good USB device or "iPod" to the stereo jack adapter.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003XYS05EX_04_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003XYS05EX_04_0003" proc-id="RM23G0E___0000BPO00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000014CZ0CBX"/>). </ptxt>
</test1>
<test1>
<ptxt>Check for DTCs and check if the same DTC occurs again.</ptxt>
<atten4>
<ptxt>If DTCs are output frequently, replace the multi-media interface ECU.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears. </ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XYS05EX_04_0004" fin="true">OK</down>
<right ref="RM000003XYS05EX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XYS05EX_04_0004">
<testtitle>USB DEVICE OR "iPod" WAS DEFECTIVE</testtitle>
</testgrp>
<testgrp id="RM000003XYS05EX_04_0005" proc-id="RM23G0E___0000BPP00001">
<testtitle>CHECK MULTI-MEDIA INTERFACE ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H39 No. 1 stereo jack adapter assembly connector.</ptxt>
<figure>
<graphic graphicname="E195969E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H39-1 (UJSG) - H39-4 (UPI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 1 Stereo Jack Adapter Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK (except Bench Seat Type)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for Bench Seat Type)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XYS05EX_04_0006" fin="true">A</down>
<right ref="RM000003XYS05EX_04_0010" fin="true">B</right>
<right ref="RM000003XYS05EX_04_0007" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000003XYS05EX_04_0006">
<testtitle>REPLACE NO. 1 STEREO JACK ADAPTER ASSEMBLY<xref label="Seep01" href="RM000003AWF02XX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XYS05EX_04_0007" proc-id="RM23G0E___0000BPQ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTI-MEDIA INTERFACE ECU - STEREO JACK ADAPTER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H40 multi-media interface ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the H39 No. 1 stereo jack adapter assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H40-4 (UPO) - H39-4 (UPI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H40-1 (UESG) - H39-1 (UJSG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H40-4 (UPO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H40-1 (UESG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XYS05EX_04_0008" fin="true">OK</down>
<right ref="RM000003XYS05EX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XYS05EX_04_0008">
<testtitle>REPLACE MULTI-MEDIA INTERFACE ECU<xref label="Seep01" href="RM00000427800ZX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XYS05EX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003XYS05EX_04_0010">
<testtitle>REPLACE NO. 1 STEREO JACK ADAPTER ASSEMBLY<xref label="Seep01" href="RM00000501Z002X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>