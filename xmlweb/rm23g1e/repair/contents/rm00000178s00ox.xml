<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12019_S0016" variety="S0016">
<name>RA61F MANUAL TRANSMISSION / TRANSAXLE</name>
<ttl id="12019_S0016_7B95G_T00F4" variety="T00F4">
<name>MANUAL TRANSMISSION ASSEMBLY (for 1KD-FTV)</name>
<para id="RM00000178S00OX" category="A" type-id="80001" name-id="MT18T-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM00000178S00OX_01" type-id="01" category="01">
<s-1 id="RM00000178S00OX_01_0001" proc-id="RM23G0E___00008UP00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected  (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0004" proc-id="RM23G0E___00008UQ00000">
<ptxt>REMOVE SHIFT LEVER KNOB SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the knob from the shift lever.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0049" proc-id="RM23G0E___00008PU00000">
<ptxt>REMOVE CONSOLE PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238178" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 7 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and clip and remove the upper console panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000178S00OX_01_0006" proc-id="RM23G0E___00008UR00000">
<ptxt>REMOVE SHIFT LEVER BOOT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215612" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 screws and shift lever boot.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0007" proc-id="RM23G0E___00008US00000">
<ptxt>REMOVE FLOOR SHIFT SHIFT LEVER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="F051062" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the shift lever cap boot from the manual transmission.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C215583E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Cover the shift lever cap with a cloth.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cloth</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Down</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Counterclockwise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>While pressing down on the shift lever cap, turn it counterclockwise to remove the shift lever.</ptxt>
</s2>
<s2>
<ptxt>Pull out the shift lever to remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0009" proc-id="RM23G0E___00008UT00000">
<ptxt>DRAIN MANUAL TRANSMISSION OIL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the drain plug and gasket, and then drain the manual transmission oil.</ptxt>
</s2>
<s2>
<ptxt>Install a new gasket and the drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>37</t-value1>
<t-value2>377</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0051" proc-id="RM23G0E___00008V100000">
<ptxt>REMOVE FRONT EXHAUST PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>w/o DPF: (See page <xref label="Seep02" href="RM00000456A00EX"/>)</ptxt>
<ptxt>w/ DPF: (See page <xref label="Seep01" href="RM00000456A00FX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0062" proc-id="RM23G0E___00008V500000">
<ptxt>REMOVE TRANSFER CASE LOWER PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and transfer case lower protector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0053" proc-id="RM23G0E___00008V200000">
<ptxt>REMOVE FRONT PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM00000291R00KX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0054" proc-id="RM23G0E___00008V300000">
<ptxt>REMOVE PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000029WP00LX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0055" proc-id="RM23G0E___00008V400000">
<ptxt>REMOVE STARTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>for 2.2 kW Type: (See page <xref label="Seep01" href="RM000000IY500MX"/>)</ptxt>
<ptxt>for 2.7 kW Type: (See page <xref label="Seep02" href="RM000000IY500NX"/>)</ptxt>
<ptxt>for 3.0 kW Type: (See page <xref label="Seep03" href="RM000002PDT00EX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0057" proc-id="RM23G0E___000073300000">
<ptxt>DISCONNECT CLUTCH RELEASE CYLINDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C215613" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and clip, and then disconnect the release cylinder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0045" proc-id="RM23G0E___00008UZ00000">
<ptxt>REMOVE FRONT SUSPENSION MEMBER BRACKET LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215587" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and front suspension member bracket LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0046" proc-id="RM23G0E___00008V000000">
<ptxt>REMOVE FRONT SUSPENSION MEMBER BRACKET RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215588" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and front suspension member bracket RH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0020" proc-id="RM23G0E___00008UU00000">
<ptxt>REMOVE NO. 3 FRAME CROSSMEMBER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Support the rear side of the transmission with a support stand.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C215589" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 4 bolts from the No. 3 frame crossmember sub-assembly.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="C215614" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 4 nuts, 4 bolts and No. 3 frame crossmember sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0021" proc-id="RM23G0E___00008UV00000">
<ptxt>REMOVE REAR NO. 1 ENGINE MOUNTING INSULATOR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215591" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and engine mounting insulator rear from the manual transmission.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0023" proc-id="RM23G0E___00008UW00000">
<ptxt>DISCONNECT WIRE HARNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o DPF:</ptxt>
<s3>
<ptxt>Detach the 5 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="C215616" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the 2 connectors and wire harness.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ DPF:</ptxt>
<s3>
<ptxt>Disconnect the 3 connectors and 3 connector clamps.</ptxt>
<figure>
<graphic graphicname="C231088E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Detach the 8 wire harness clamps.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0065" proc-id="RM23G0E___00008V600000">
<ptxt>DISCONNECT TRANSFER AND MANUAL TRANSMISSION BREATHER HOSE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o DPF:</ptxt>
<s3>
<ptxt>Detach the breather hose clamp.</ptxt>
<figure>
<graphic graphicname="C215615" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the 3 breather hoses.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ DPF:</ptxt>
<s3>
<ptxt>Detach the 2 breather hose clamps.</ptxt>
<figure>
<graphic graphicname="C232587" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Disconnect the 3 breather hoses.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0024" proc-id="RM23G0E___00008UX00000">
<ptxt>REMOVE MANUAL TRANSMISSION WITH TRANSFER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215617" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 8 bolts.</ptxt>
</s2>
<s2>
<ptxt>Remove the manual transmission with transfer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0025" proc-id="RM23G0E___00008UY00000">
<ptxt>REMOVE TRANSMISSION UPPER COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215595" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and upper cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0067" proc-id="RM23G0E___00008V700000">
<ptxt>REMOVE DIFFERENTIAL PRESSURE SENSOR ASSEMBLY (w/ DPF)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the nut and differential pressure sensor from the sensor bracket.</ptxt>
<figure>
<graphic graphicname="C231087" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000178S00OX_01_0066" proc-id="RM23G0E___00008JO00000">
<ptxt>REMOVE TRANSFER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1KD-FTV:</ptxt>
<figure>
<graphic graphicname="C233861E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1KD-FTV, RA61F</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1KD-FTV, A750F</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Remove the 8 bolts and 2 brackets.</ptxt>
</s2>
<s2>
<ptxt>except 1KD-FTV:</ptxt>
<figure>
<graphic graphicname="C235226" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 8 bolts and bracket.</ptxt>
</s2>
<s2>
<ptxt>Remove the transfer from the transmission.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>