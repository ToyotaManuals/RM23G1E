<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9DA_T00MY" variety="T00MY">
<name>FRONT SEAT CUSHION HEATER (for Manual Seat)</name>
<para id="RM000002XF802CX" category="G" type-id="3001K" name-id="SE1QQ-06" from="201207">
<name>INSPECTION</name>
<subpara id="RM000002XF802CX_01" type-id="01" category="01">
<s-1 id="RM000002XF802CX_01_0003" proc-id="RM23G0E___0000GRK00000">
<ptxt>INSPECT FRONT SEAT CUSHION HEATER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239582E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Check the seat cushion heater.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title> Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A-1 - B-1</ptxt>
</entry>
<entry morerows="3" valign="middle">
<ptxt>Seat cushion heater temperature 20°C (68°F)</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>3.4 to 4.2 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-4 - B-2</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-1 - A-4</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-2 - A-3</ptxt>
</entry>
<entry valign="middle">
<ptxt>8 to 12 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the seat cushion heater assembly.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Front Seat Cushion Heater)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002XF802CX_01_0002" proc-id="RM23G0E___0000GRJ00000">
<ptxt>INSPECT FRONT SEAT CUSHION HEATER ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239583E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Check the seat cushion heater.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title> Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A-1 - B-1</ptxt>
</entry>
<entry morerows="3" valign="middle">
<ptxt>Seat cushion heater temperature 20°C (68°F)</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>3.4 to 4.2 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-4 - B-2</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-1 - A-4</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-2 - A-3</ptxt>
</entry>
<entry valign="middle">
<ptxt>8 to 12 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the seat cushion heater assembly.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Front Seat Cushion Heater)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>