<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000000T6X0CPX" category="C" type-id="3041M" name-id="ESMBT-06" from="201210">
<dtccode>P2610</dtccode>
<dtcname>ECM / PCM Internal Engine Off Timer Performance</dtcname>
<subpara id="RM000000T6X0CPX_02" type-id="60" category="03" proc-id="RM23G0E___00000XI00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The soak timer operates after the ignition switch is turned off. When a certain amount of time has elapsed after turning the ignition switch off, the soak timer activates the ECM to perform malfunction checks which can only be performed after the engine is stopped. The soak timer is built into the ECM.</ptxt>
<figure>
<graphic graphicname="A266626E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL3" colwidth="3.12in"/>
<colspec colname="COL4" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2610</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM internal malfunction (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000T6X0CPX_03" type-id="64" category="03" proc-id="RM23G0E___00000XJ00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="ordered">
<item>
<ptxt>While the engine is running, the ECM monitors the synchronization of the soak timer and the CPU clock. If these two are not synchronized, the ECM interprets this as a malfunction, illuminates the MIL and stores the DTC.</ptxt>
</item>
<item>
<ptxt>If the soak timer activates the ECM even though only a short amount of time has elapsed since the ignition switch was turned off, or if the soak timer does not activate the ECM even though a considerable amount of time has elapsed since the ignition switch was turned off, the ECM determines that the soak timer is malfunctioning, illuminates the MIL and stores a DTC the next time the ignition switch is turned to ON.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000T6X0CPX_05" type-id="51" category="05" proc-id="RM23G0E___00000XK00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTC P2610 is stored if an internal ECM problem is detected. Diagnostic procedures are not required. ECM replacement is necessary.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T6X0CPX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000T6X0CPX_06_0004" proc-id="RM23G0E___00000XM00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202NX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000T6X0CPX_06_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000T6X0CPX_06_0002" proc-id="RM23G0E___00000XL00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK0Z5X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and wait for 10 minutes or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes / Pending.</ptxt>
</test1>
<test1>
<ptxt>If no pending DTC is output, the repair has been successfully completed.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000T6X0CPX_06_0003" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000T6X0CPX_06_0003">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>