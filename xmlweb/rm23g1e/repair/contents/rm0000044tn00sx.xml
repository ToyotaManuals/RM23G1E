<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8Z2_T008Q" variety="T008Q">
<name>FUEL INJECTOR (w/ DPF)</name>
<para id="RM0000044TN00SX" category="A" type-id="80001" name-id="FU8CV-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000044TN00SX_02" type-id="11" category="10" proc-id="RM23G0E___00005QV00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipes and fuel inlet pipe, clean them with a brush and compressed air.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000044TN00SX_01" type-id="01" category="01">
<s-1 id="RM0000044TN00SX_01_0040" proc-id="RM23G0E___00005QM00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0054" proc-id="RM23G0E___00005QT00001">
<ptxt>REMOVE COWL TOP VENTILATOR LOUVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000001OV303UX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0047" proc-id="RM23G0E___00005QN00001">
<ptxt>REMOVE ELECTRIC EGR CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000004KSK005X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0063" proc-id="RM23G0E___00006CV00001">
<ptxt>DISCONNECT WIRE HARNESS
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="A245686" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Remove the bolt and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="A246260" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 5 clamps and disconnect the wire harness from the cowl top panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044TN00SX_01_0064" proc-id="RM23G0E___000032S00000">
<ptxt>REMOVE MANIFOLD ABSOLUTE PRESSURE SENSOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the manifold absolute pressure sensor connector and vacuum hose.</ptxt>
<figure>
<graphic graphicname="A239598" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and manifold absolute pressure sensor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044TN00SX_01_0065" proc-id="RM23G0E___00006CW00001">
<ptxt>REMOVE EMISSION CONTROL VALVE BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and emission control valve bracket.</ptxt>
<figure>
<graphic graphicname="A239639" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044TN00SX_01_0066" proc-id="RM23G0E___00006CZ00001">
<ptxt>REMOVE THROTTLE BODY BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and throttle body bracket.</ptxt>
<figure>
<graphic graphicname="A244614" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044TN00SX_01_0067" proc-id="RM23G0E___00006CX00001">
<ptxt>REMOVE NO. 1 GAS FILTER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the vacuum hose and remove the No. 1 gas filter from the gas filter bracket.</ptxt>
<figure>
<graphic graphicname="A244608" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044TN00SX_01_0068" proc-id="RM23G0E___00006CY00001">
<ptxt>REMOVE GAS FILTER BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="A246247" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and gas filter bracket.</ptxt>
<figure>
<graphic graphicname="A244609" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044TN00SX_01_0069" proc-id="RM23G0E___00006D000001">
<ptxt>REMOVE INTAKE AIR CONNECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts, intake air connector and gasket.</ptxt>
<figure>
<graphic graphicname="A245684" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044TN00SX_01_0062" proc-id="RM23G0E___00005QU00001">
<ptxt>REMOVE NO. 1 FUEL PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 fuel pipe (See page <xref label="Seep01" href="RM0000028RU03BX"/>).</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts, union bolt, gasket and No. 1 fuel pipe.</ptxt>
<figure>
<graphic graphicname="A239663E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" align="center" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Fuel Pipe</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0048" proc-id="RM23G0E___00005QO00001">
<ptxt>REMOVE NO. 4 INJECTION PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, nut and 2 No. 2 injection pipe clamps.</ptxt>
<figure>
<graphic graphicname="A239621" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, loosen the union nuts and remove the No. 4 injection pipe.</ptxt>
<figure>
<graphic graphicname="A239619E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0049" proc-id="RM23G0E___00005QP00001">
<ptxt>REMOVE NO. 2 FUEL PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 6 mm hexagon wrench, remove the union bolt and gasket.</ptxt>
<figure>
<graphic graphicname="A239622" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Fuel Check Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the fuel check valve, gasket and No. 2 fuel pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0050" proc-id="RM23G0E___00005QQ00001">
<ptxt>REMOVE NO. 3 NOZZLE LEAKAGE PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 fuel hoses.</ptxt>
<figure>
<graphic graphicname="A239624" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Fuel Check Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Remove the fuel check valve, gasket and No. 3 nozzle leakage pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0051" proc-id="RM23G0E___00005QR00001">
<ptxt>REMOVE NO. 2 NOZZLE LEAKAGE PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A239626" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the union bolt, gasket and No. 2 nozzle leakage pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0053" proc-id="RM23G0E___00005QS00001">
<ptxt>REMOVE NO. 2 CYLINDER HEAD COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and No. 2 cylinder head cover.</ptxt>
<figure>
<graphic graphicname="A244984" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0027" proc-id="RM23G0E___00006EB00001">
<ptxt>REMOVE VENTILATION PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the 2 ventilation hoses and ventilation pipe.</ptxt>
<figure>
<graphic graphicname="A245674" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044TN00SX_01_0024" proc-id="RM23G0E___00005QK00001">
<ptxt>REMOVE CYLINDER HEAD COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>If the cylinder head cover is removed, replace the 4 No. 3 cylinder head cover gaskets with new ones.</ptxt>
</atten3>
<s2>
<ptxt>Using a small screwdriver, remove the nozzle holder seal by prying the portion between the nozzle holder seal and the cutout part of the cylinder head cover.</ptxt>
<figure>
<graphic graphicname="A060180E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 10 bolts, 2 nuts, cylinder head cover and cylinder head cover gasket.</ptxt>
<figure>
<graphic graphicname="A223193" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 No. 3 cylinder head cover gaskets from the cylinder head cover.</ptxt>
<figure>
<graphic graphicname="A289511" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044TN00SX_01_0025" proc-id="RM23G0E___00005QL00001">
<ptxt>REMOVE INJECTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the union bolt, 4 injector hollow screws, 5 gaskets and nozzle leakage pipe.</ptxt>
<figure>
<graphic graphicname="A076779E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When removing the nozzle leakage pipe, place a cushion under the pipe.</ptxt>
</item>
<item>
<ptxt>Be careful not to deform or scratch the union seal surface.</ptxt>
</item>
<item>
<ptxt>After removing the nozzle leakage pipe, put it in a plastic bag to prevent foreign matter from contaminating its injector inlet.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the 4 bolts, 4 washers, 4 No. 1 nozzle holder clamps and 4 injectors.</ptxt>
<figure>
<graphic graphicname="A076780E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Arrange the injectors, No. 1 nozzle holder clamps, washers and bolts in the correct order.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the O-ring from each injector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 injection nozzle seats from the cylinder head.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>