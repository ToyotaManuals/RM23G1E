<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000188D05SX" category="C" type-id="804FO" name-id="ESX48-12" from="201210">
<dtccode>P2084</dtccode>
<dtcname>Exhaust Gas Temperature Sensor Circuit Range/Performance Bank 1 Sensor 2</dtcname>
<dtccode>P242B</dtccode>
<dtcname>Exhaust Gas Temperature Sensor Circuit Range / Performance Bank 1 Sensor 3</dtcname>
<subpara id="RM00000188D05SX_01" type-id="60" category="03" proc-id="RM23G0E___00002SV00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0545 (See page <xref label="Seep01" href="RM00000188D05RX_01"/>).</ptxt>
<table pgwide="1">
<title>P2084</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>Stop the engine, leave the vehicle as is for 1 to 2 hours and confirm that "Exhaust Temperature B1S2" is approximately the same value as "Intake Air".</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>All conditions are met (2 trip detection logic):</ptxt>
<ptxt>(a) The battery voltage is 8 V or higher.</ptxt>
<ptxt>(b) After the engine is stopped and the vehicle is left as is for 10 hours or more, the difference between "Exhaust Temperature B1S2" and "Coolant Temp" is 400°C (752 °F) or more when the ignition switch is turned to ON.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Exhaust gas temperature sensor B1S2</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM (soak timer)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P242B</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>Stop the engine, leave the vehicle as is for 1 to 2 hours and confirm that "Exhaust Temperature B1S3" is approximately the same value as "Intake Air".</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>All conditions are met (2 trip detection logic):</ptxt>
<ptxt>(a) The battery voltage is 8 V or higher.</ptxt>
<ptxt>(b) After the engine is stopped and the vehicle is left as is for 10 hours or more, the difference between "Exhaust Temperature B1S3" and "Coolant Temp" is 400°C (752 °F) or more when the ignition switch is turned to ON.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Exhaust gas temperature sensor B1S3</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM (soak timer)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2084</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Exhaust Temperature B1S2</ptxt>
</item>
<item>
<ptxt>Exhaust Temperature B1S3</ptxt>
</item>
<item>
<ptxt>Coolant temp</ptxt>
</item>
<item>
<ptxt>Intake Air</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P242B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Sensor 2 represents the sensor located upstream of the DPF catalytic converter.</ptxt>
</item>
<item>
<ptxt>Sensor 3 represents the sensor located downstream of the DPF catalytic converter.</ptxt>
</item>
<item>
<ptxt>If DTC P2084 and/or P242B is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>White smoke</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188D05SX_07" type-id="64" category="03" proc-id="RM23G0E___00002T700001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM compares the values from exhaust temperature sensors B1S2 and B1S3 with the engine coolant temperature when the ignition switch is turned to ON after the vehicle has been left soaking for 10 hours or more. If the difference is 400°C (752 °F) or more, a DTC indicating an exhaust gas temperature sensor malfunction is stored and the MIL is illuminated (2 trip detection logic).</ptxt>
</content5>
</subpara>
<subpara id="RM00000188D05SX_03" type-id="32" category="03" proc-id="RM23G0E___00002SW00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0545 (See page <xref label="Seep01" href="RM00000188D05RX_03"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000188D05SX_04" type-id="51" category="05" proc-id="RM23G0E___00002SX00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07MX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07MX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188D05SX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000188D05SX_05_0045" proc-id="RM23G0E___00002SY00001">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P2084 AND P242B)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.74in"/>
<colspec colname="COL2" colwidth="1.39in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2084 and/or P242B is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
<ptxt>P2084 and/or P242B and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000188D05SX_05_0050" fin="false">A</down>
<right ref="RM00000188D05SX_05_0027" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0050" proc-id="RM23G0E___00002T200001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (EXHAUST TEMPERATURE B1S2 AND B1S3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Leave the vehicle as is for 1 to 2 hours with the engine stopped.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Exhaust Temperature B1S2 and Exhaust Temperature B1S3.</ptxt>
</test1>
<test1>
<ptxt>Read the values.</ptxt>
<spec>
<title>Result</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.66in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Value of "Exhaust Temperature B1S2" is 400°C (752°F) or higher</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Value of "Exhaust Temperature B1S3" is 400°C (752°F) or higher</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>If the exhaust gas temperature sensor is normal, the exhaust gas temperature drops when the vehicle is left as is for 1 to 2 hours with the engine stopped. If the exhaust gas temperature does not drop, the exhaust gas temperature sensor may be malfunctioning.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000188D05SX_05_0052" fin="false">C</down>
<right ref="RM00000188D05SX_05_0047" fin="false">A</right>
<right ref="RM00000188D05SX_05_0058" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0052" proc-id="RM23G0E___00002T300001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ENGINE COOLANT TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Allow the engine to soak for 1 to 2 hours.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>It is not necessary to allow the engine to soak if the engine is cold.</ptxt>
</item>
<item>
<ptxt>The engine cools faster if the hood is opened.</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Coolant temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The difference between the displayed value and outside air temperature is below 5°C (9°F).</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>If the difference between "Coolant Temp" and the outside air temperature is 5°C (9°F) or higher, the heat from the engine affects the intake air temperature sensor, causing an incorrect value to be displayed. In this case, allow the engine to soak again.</ptxt>
</atten3>
</test1>
</content6>
<res>
<right ref="RM00000188D05SX_05_0048" fin="false">OK</right>
<right ref="RM00000188D05SX_05_0053" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0053" proc-id="RM23G0E___00002T400001">
<testtitle>INSPECT ENGINE COOLANT TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the engine coolant temperature sensor (See page <xref label="Seep01" href="RM0000013Z801EX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188D05SX_05_0048" fin="false">OK</right>
<right ref="RM00000188D05SX_05_0054" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0054" proc-id="RM23G0E___00002T500001">
<testtitle>REPLACE ENGINE COOLANT TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the engine coolant temperature sensor (See page <xref label="Seep01" href="RM0000013ZA01FX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188D05SX_05_0049" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0048" proc-id="RM23G0E___00002T000001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188D05SX_05_0049" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0047" proc-id="RM23G0E___00002SZ00001">
<testtitle>REPLACE EXHAUST GAS TEMPERATURE SENSOR B1S2</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the exhaust gas temperature sensor B1S2 (See page <xref label="Seep01" href="RM000003TEF00TX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188D05SX_05_0049" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0058" proc-id="RM23G0E___00002T600001">
<testtitle>REPLACE EXHAUST GAS TEMPERATURE SENSOR B1S3</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the exhaust gas temperature sensor B1S3 (See page <xref label="Seep01" href="RM000003TEF00TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188D05SX_05_0049" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0049" proc-id="RM23G0E___00002T100001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Leave the vehicle as is for 1 to 2 hours with the engine stopped.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>It is not necessary to allow the engine to soak if the engine is cold.</ptxt>
</item>
<item>
<ptxt>The engine cools faster if the hood is opened.</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Exhaust Temperature B1S2, Exhaust Temperature B1S3 and Intake Air.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The values of "Exhaust Temperature B1S2" and "Exhaust Temperature B1S3" are both approximately the same as "Intake Air".</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000188D05SX_05_0026" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188D05SX_05_0027">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW050X"/>
</testtitle>
</testgrp>
<testgrp id="RM00000188D05SX_05_0026">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>