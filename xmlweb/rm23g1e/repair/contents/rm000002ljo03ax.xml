<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12060_S0025" variety="S0025">
<name>SEAT BELT</name>
<ttl id="12060_S0025_7B9E4_T00NS" variety="T00NS">
<name>FRONT SEAT OUTER BELT ASSEMBLY (for 3 Door)</name>
<para id="RM000002LJO03AX" category="A" type-id="30014" name-id="SB55R-01" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM000002LJO03AX_02" type-id="11" category="10" proc-id="RM23G0E___0000H8M00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0CLX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000002LJO03AX_01" type-id="01" category="01">
<s-1 id="RM000002LJO03AX_01_0032" proc-id="RM23G0E___0000H8I00000">
<ptxt>INSTALL FRONT SHOULDER BELT ANCHOR PLATE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B239046" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Align the claw with the anchor plate positioning hole and install the anchor plate with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>First install the lower bolt, and then install the upper bolt.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LJO03AX_01_0002" proc-id="RM23G0E___0000H8G00000">
<ptxt>INSTALL FRONT SEAT OUTER BELT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239045E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>When installing the retractor, make sure the claws of the vehicle (labeled "a" in the illustration) only contact the installation areas of the retractor.</ptxt>
</atten3>
<s2>
<ptxt>Align the seat belt retractor positioning holes with the claws of the vehicle.</ptxt>
</s2>
<s2>
<ptxt>Install the seat belt with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.5</t-value1>
<t-value2>87</t-value2>
<t-value3>75</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="B242876E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Connect the pretensioner connector labeled A as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>w/ Pre-Crash Safety System:</ptxt>
<ptxt>Connect the pre-crash safety connector labeled B.</ptxt>
</s2>
<s2>
<ptxt>Install the shoulder anchor to the shoulder belt anchor adjuster with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LJO03AX_01_0041" proc-id="RM23G0E___0000H8K00000">
<ptxt>INSTALL FRONT QUARTER TRIM PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Pass the front seat outer belt floor anchor through the front quarter trim panel.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 clips and 2 guides to install the front quarter trim panel.</ptxt>
</s2>
<s2>
<ptxt>Install the bolt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0042" proc-id="RM23G0E___0000H8L00000">
<ptxt>INSTALL DECK TRIM SIDE PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 9 claws and 11 clips to install the deck trim side panel.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Install the rear No. 1 seat outer belt floor anchor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the front seat outer belt floor anchor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002LJO03AX_01_0043" proc-id="RM23G0E___0000C4O00000">
<ptxt>INSTALL FRONT DECK SIDE TRIM COVER (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for the other front deck side trim cover.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the front deck side trim cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0044" proc-id="RM23G0E___0000C4P00000">
<ptxt>INSTALL NO. 1 TONNEAU COVER HOLDER CAP (w/ o Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for the other tonneau cover holder cap.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the tonneau cover holder cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0045" proc-id="RM23G0E___0000C4Q00000">
<ptxt>INSTALL NO. 1 LUGGAGE COMPARTMENT TRIM HOOK
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for the other No. 1 luggage compartment trim hook.</ptxt>
</atten4>
<s2>
<ptxt>Install the No. 1 luggage compartment trim hook so that it is positioned horizontally.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0046" proc-id="RM23G0E___0000C4R00000">
<ptxt>INSTALL REAR FLOOR CARPET ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear floor carpet.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0047" proc-id="RM23G0E___0000C4S00000">
<ptxt>INSTALL QUARTER TRIM COVER HOLE LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws and 2 guides to install the quarter trim cover hole.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0048" proc-id="RM23G0E___00005N400000">
<ptxt>INSTALL REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 6 claws to install the rear floor mat rear support plate.</ptxt>
</s2>
<s2>
<ptxt>Install the 5 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0049" proc-id="RM23G0E___0000C4T00000">
<ptxt>INSTALL MAT SET PLATE COVER
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for all mat set plate covers.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the mat set plate cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0050" proc-id="RM23G0E___0000C4U00000">
<ptxt>INSTALL TONNEAU COVER ASSEMBLY (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the tonneau cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0035" proc-id="RM23G0E___0000C4V00000">
<ptxt>INSTALL FRONT DOOR OPENING TRIM WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238779E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Align the paint mark on the front door opening trim weatherstrip with the mark position on the vehicle and install the front door opening trim weatherstrip as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark Position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0017" proc-id="RM23G0E___00008CE00000">
<ptxt>INSTALL DOOR SCUFF PLATE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 clips, 10 claws and 2 guides to install the door scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002LJO03AX_01_0033" proc-id="RM23G0E___0000H8J00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002LJO03AX_01_0026" proc-id="RM23G0E___0000H8H00000">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000000XFD0GRX"/>)</ptxt>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>