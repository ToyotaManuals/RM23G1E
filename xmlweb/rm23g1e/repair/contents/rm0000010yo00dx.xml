<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000G" variety="S000G">
<name>5L-E FUEL</name>
<ttl id="12008_S000G_7B8ZN_T009B" variety="T009B">
<name>FUEL INJECTION NOZZLE</name>
<para id="RM0000010YO00DX" category="A" type-id="30014" name-id="FU55R-03" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM0000010YO00DX_01" type-id="01" category="01">
<s-1 id="RM0000010YO00DX_01_0001" proc-id="RM23G0E___000060M00000">
<ptxt>INSTALL NOZZLE HOLDER AND NOZZLE SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 4 new injection nozzle seat gaskets and the 4 injection nozzle seats to the injection nozzle holes of the cylinder head.</ptxt>
<figure>
<graphic graphicname="A122389E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, install the 4 nozzle holder and nozzle sets.</ptxt>
<sst>
<sstitem>
<s-number>09268-64010</s-number>
<s-subnumber>09268-64020</s-subnumber>
</sstitem>
</sst>
<torque>
<torqueitem>
<t-value1>64</t-value1>
<t-value2>650</t-value2>
<t-value4>47</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010YO00DX_01_0002" proc-id="RM23G0E___000060N00000">
<ptxt>INSTALL NOZZLE LEAKAGE PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 4 new ring packing washers and the leakage pipe with the 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>301</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the fuel hose to the leakage pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010YO00DX_01_0012" proc-id="RM23G0E___000060O00000">
<ptxt>INSTALL NO. 1 GLOW PLUG CONNECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 glow plug resistor insulator and No. 1 glow plug connector.</ptxt>
</s2>
<s2>
<ptxt>Install the glow plug connector with the 4 nuts. Uniformly tighten the nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>1.0</t-value1>
<t-value2>10</t-value2>
<t-value3>9</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A224319E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Nut</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Washer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>No. 2 Glow Plug Resistor Insulator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Engine Wire</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>No. 1 Glow Plug Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>No. 1 Glow Plug Resistor Insulator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry>
<ptxt>Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the 4 screw grommets.</ptxt>
</s2>
<s2>
<ptxt>Connect the engine wire and install the No. 2 glow plug resistor insulator and washer with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000010YO00DX_01_0004" proc-id="RM23G0E___00004ZY00000">
<ptxt>INSTALL INJECTION PIPE SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 lower clamps to the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 injection pipes.</ptxt>
<figure>
<graphic graphicname="A230069E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>250</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Injection Nozzle Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Injection Pump Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the 2 upper pipe clamps with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010YO00DX_01_0013" proc-id="RM23G0E___000060P00000">
<ptxt>INSTALL DIESEL THROTTLE BODY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the diesel throttle body (See page <xref label="Seep01" href="RM0000012UX00AX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010YO00DX_01_0022" proc-id="RM23G0E___000060Q00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000010YO00DX_01_0024" proc-id="RM23G0E___000050300000">
<ptxt>BLEED INJECTION PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Move the hand pump on the upper part of the fuel filter up and down and fill the injection pump and fuel system with fuel.</ptxt>
</s2>
<s2>
<ptxt>Loosen one of the union nuts (on the nozzle side).</ptxt>
</s2>
<s2>
<ptxt>Crank the engine until fuel comes out from the union nut connection (on the nozzle side).</ptxt>
</s2>
<s2>
<ptxt>Tighten the union nut.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>250</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Perform the procedures above for each injection pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010YO00DX_01_0016" proc-id="RM23G0E___00004XB00000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that there are no fuel leaks anywhere in the fuel system after performing maintenance.</ptxt>
<atten4>
<ptxt>When checking for fuel leaks, make sure that there is pressure in the fuel line.</ptxt>
</atten4>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>