<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000000W950HUX" category="C" type-id="302I7" name-id="ESG3T-84" from="201207" to="201210">
<dtccode>P0604</dtccode>
<dtcname>Internal Control Module Random Access Memory (RAM) Error</dtcname>
<dtccode>P0606</dtccode>
<dtcname>ECM / PCM Processor</dtcname>
<dtccode>P060A</dtccode>
<dtcname>Internal Control Module Monitoring Processor Performance</dtcname>
<dtccode>P060B</dtccode>
<dtcname>Internal Control Module A/D Processing Performance</dtcname>
<dtccode>P060D</dtccode>
<dtcname>Internal Control Module Accelerator Pedal Position Performance</dtcname>
<dtccode>P060E</dtccode>
<dtcname>Internal Control Module Throttle Position Performance</dtcname>
<dtccode>P0657</dtccode>
<dtcname>Actuator Supply Voltage Circuit / Open</dtcname>
<dtccode>P1607</dtccode>
<dtcname>Cruise Control Input Processor</dtcname>
<subpara id="RM000000W950HUX_01" type-id="60" category="03" proc-id="RM23G0E___00000Q300000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM continuously monitors its internal memory status, internal circuits, and output signals transmitted to the throttle actuator. This self-check ensures that the ECM is functioning properly. If any malfunction is detected, the ECM stores the appropriate DTC and illuminates the MIL.</ptxt>
<ptxt>The ECM memory status is diagnosed by internal "mirroring" of the main CPU and sub CPU to detect Random Access Memory (RAM) errors. The 2 CPUs also perform continuous mutual monitoring. The ECM illuminates the MIL and stores a DTC if: 1) outputs from the 2 CPUs are different or deviate from the standard, 2) the signals sent to the throttle actuator deviate from the standard, 3) a malfunction is found in the throttle actuator supply voltage, and 4) any other ECM malfunction is found.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0604</ptxt>
<ptxt>P0606</ptxt>
<ptxt>P060A</ptxt>
<ptxt>P060B</ptxt>
<ptxt>P060D</ptxt>
<ptxt>P060E</ptxt>
<ptxt>P0657</ptxt>
<ptxt>P1607</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ECM internal error (1 trip detection logic)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W950HUX_05" type-id="51" category="05" proc-id="RM23G0E___00000Q400000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<ptxt>Read freeze frame data using the intelligent tester. The ECM records vehicle and driving condition information as freeze frame data the moment a DTC is stored. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W950HUX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W950HUX_06_0001" proc-id="RM23G0E___00000Q500000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off and turn the tester off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the intelligent tester.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal and wait for 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
</test1>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="4.95in"/>
<colspec colname="COL2" colwidth="2.13in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC P0604, P0606, P060A, P060B, P060D, P060E, P0657 and/or P1607 are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P0604, P0606, P060A, P060B, P060D, P060E, P0657, P1607 and other DTCs are output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000W950HUX_06_0003" fin="true">A</down>
<right ref="RM000000W950HUX_06_0002" fin="true">B</right>
<right ref="RM000000W950HUX_06_0004" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000W950HUX_06_0002">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000002ZSO00PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W950HUX_06_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W950HUX_06_0004">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0VHX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>