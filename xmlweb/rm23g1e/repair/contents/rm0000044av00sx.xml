<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0011" variety="S0011">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0011_7B93H_T00D5" variety="T00D5">
<name>CRUISE CONTROL SYSTEM</name>
<para id="RM0000044AV00SX" category="U" type-id="3011K" name-id="CC2HM-02" from="201207">
<name>TERMINALS OF ECM</name>
<subpara id="RM0000044AV00SX_z0" proc-id="RM23G0E___000076M00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK ECM (for 1GR-FE)</ptxt>
<figure>
<graphic graphicname="E195784E06" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the C36, G55 and G56 ECM connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G55-24 (BATT) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power source circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G56-21 (IGSW) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>IG power source circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G55-8 (ST1-) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Stop light switch signal circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G56-18 (STP) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>V - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Stop light switch signal circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>G56-11 (CCS) - Body ground</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>Cruise control switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 2.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Cruise control switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>+RES switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>235 to 245 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>-SET switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>617 to 643 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CANCEL switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>1509 to 1571 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C36-12 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the C36, G55 and G56 ECM connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>G56-24 (S) - Body ground*1</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>V - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Transmission control switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G56-16 (SFTU) - Body ground*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>GR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transmission control switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in S → shift lever moved to "+"</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V → Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G56-22 (SFTD) - Body ground*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transmission control switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in S → shift lever moved to "-"</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V → Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>C34-27 (D) - Body ground*1</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>C34-27 (D) - Body ground*2</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Clutch switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, clutch pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, clutch pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>C34-35 (P) - Body ground*1</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in P</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in P</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>C34-28 (R) - Body ground*1</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>C34-29 (N) - Body ground*1</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in N</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in N</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for Automatic Transmission</ptxt>
</item>
<item>
<ptxt>*2: for Manual Transmission</ptxt>
</item>
</list1>
<ptxt>If the result is not as specified, the ECM may be malfunctioning.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK ECM (for 1KD-FTV)</ptxt>
<figure>
<graphic graphicname="E195784E07" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the C93, G57 and G58 ECM connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G57-23 (BATT) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power source circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G58-24 (IGSW) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>W - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>IG power source circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G57-9 (ST1-) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Stop light switch signal circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G57-8 (STP) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>V - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Stop light switch signal circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>G57-14 (CCS) - Body ground</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>Cruise control switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 2.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Cruise control switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>+RES switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>235 to 245 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>-SET switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>617 to 643 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CANCEL switch held on</ptxt>
</entry>
<entry valign="middle">
<ptxt>1509 to 1571 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C93-1 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the C93, G57 and G58 ECM connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>C91-15 (CLSW) - Body ground*1</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>C19-15 (D) - Body ground*2</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Clutch switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, clutch pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, clutch pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for Automatic Transmission</ptxt>
</item>
<item>
<ptxt>*2: for Manual Transmission</ptxt>
</item>
</list1>
<ptxt>If the result is not as specified, the ECM may be malfunctioning.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK TRANSMISSION CONTROL ECU (for 1KD-FTV and Automatic Transmission)</ptxt>
<figure>
<graphic graphicname="E200170E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the G70 ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G70-5 (BATT) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power source circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G70-6 (IG2) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>IG power source circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the G70 ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>G70-15 (S) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>V - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Transmission control switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in S</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G70-22 (SFTU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>GR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transmission control switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in S → shift lever moved to "+"</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V → Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G70-23 (SFTD) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transmission control switch circuit</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in S → shift lever moved to "-"</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V → Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G71-8 (D) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in D</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G69-13 (P) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in P</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in P</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G71-9 (R) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G69-14 (N) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>G - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Park/neutral position switch signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in N</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever not in N</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, the ECU may be malfunctioning.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>