<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A7_T00JV" variety="T00JV">
<name>SIDE MONITOR SYSTEM (w/ Rear View Monitor System)</name>
<para id="RM000003XM001AX" category="D" type-id="303F2" name-id="PM38F-02" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000003XM001AX_z0" proc-id="RM23G0E___0000D1C00000">
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>*: w/ Multi-terrain monitor</ptxt>
</item>
</list1>
<step1>
<ptxt>GENERAL</ptxt>
<step2>
<ptxt>This system has a side television camera assembly built into the front passenger side outer rear view mirror assembly and driver side outer rear view mirror assembly* to display the passenger side view and driver side view* of the vehicle on the accessory meter assembly.</ptxt>
</step2>
<step2>
<ptxt>This system consists of the following components: steering pad switch assembly (Wide view front and side monitor switch) and combination meter assembly.</ptxt>
<step3>
<ptxt>Front passenger side television camera assembly</ptxt>
</step3>
<step3>
<ptxt>Driver side television camera assembly*</ptxt>
</step3>
<step3>
<ptxt>Parking assist ECU</ptxt>
</step3>
<step3>
<ptxt>Accessory meter assembly</ptxt>
</step3>
<step3>
<ptxt>Steering pad switch assembly (Wide view front and side monitor switch)</ptxt>
</step3>
<step3>
<ptxt>Spiral cable sub-assembly</ptxt>
</step3>
<step3>
<ptxt>Driving support switch control ECU</ptxt>
</step3>
<step3>
<ptxt>Skid control ECU</ptxt>
</step3>
<step3>
<ptxt>ECM</ptxt>
</step3>
</step2>
<step2>
<ptxt>This system is equipped with a self-diagnosis system, which is operated on a designated window that appears on the display panel, just as in the navigation system.</ptxt>
</step2>
</step1>
<step1>
<ptxt>FUNCTION OF COMPONENTS</ptxt>
<step2>
<ptxt>The parking assist ECU controls the system by using information from the following components.</ptxt>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Front Passenger Side Television Camera Assembly</ptxt>
</item>
<item>
<ptxt>Driver Side Television Camera Assembly*</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Built into the front passenger side outer rear view mirror assembly and driver side outer rear view mirror assembly* to transmit the passenger side view and driver side view* of the vehicle to the parking assist ECU.</ptxt>
</item>
<item>
<ptxt>Has a color video camera that uses a Charge-coupled Device (CCD) and a wide-angle lens.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Parking Assist ECU</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Transmits video signals which contain a composite consisting of the passenger side view and driver side view* of the vehicle taken with the television camera to the accessory meter assembly.</ptxt>
</item>
<item>
<ptxt>Performs overall control of the system by receiving signals from the sensors and accessory meter assembly.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Accessory Meter Assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Receives the video signals containing a composite of the front passenger side view and driver side view* of the vehicle from the parking assist ECU and displays them on the display panel.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>ECM</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Transmits the destination signal to the parking assist ECU through CAN communication.</ptxt>
</item>
<item>
<ptxt>Transmits the shift position signal to the parking assist ECU through CAN communication.</ptxt>
</item>
<item>
<ptxt>Transmits the steering wheel information signal to the parking assist ECU through CAN communication.</ptxt>
</item>
<item>
<ptxt>Transmits the transmission information signal to the parking assist ECU through CAN communication.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Outer Mirror Switch Assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Transmits the outer mirror switch signal to the parking assist ECU.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Spiral Cable Sub-assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Transmits a steering angle sensor signal to the parking assist ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Steering Pad Switch Assembly</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Transmits the wide view front and side monitor switch signal to the parking assist ECU through CAN communication.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Skid Control ECU</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Transmits a vehicle speed signal to the parking assist ECU through CAN communication.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>OPERATION EXPLANATION</ptxt>
<step2>
<ptxt>The wide view front and side monitor switch ON signal is sent from the steering pad switch assembly to the parking assist ECU.</ptxt>
<ptxt>After receiving the wide view front and side monitor switch ON signal, the parking assist ECU switches the display signal for the accessory meter assembly from the meter and gauge system to the side monitor system.</ptxt>
</step2>
</step1>
<step1>
<ptxt>DISPLAY CONDITIONS FOR SIDE MONITOR DISPLAY</ptxt>
<step2>
<ptxt>Screen display operation</ptxt>
<figure>
<graphic graphicname="E199672E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side Monitor Display</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step3>
<ptxt>After the display conditions are met while the side monitor system is operating normally (when no DTCs are stored), the side monitor display will be displayed.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Display conditions for manual display mode, when shift lever is in R</ptxt>
<step3>
<ptxt>The side monitor system changes the displayed image when the wide view front and side monitor switch is operated while the following conditions are met and either the ignition switch is ON or the engine is running (no DTCs stored).</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL3" colwidth="0.99in"/>
<colspec colname="COLSPEC2" colwidth="1.49in"/>
<colspec colname="COLSPEC3" colwidth="0.99in"/>
<colspec colname="COLSPEC1" colwidth="0.99in"/>
<colspec colname="COL6" colwidth="2.62in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Automatic Mode Display Button</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accessory Meter Screen</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Screen</ptxt>
<ptxt>(Changes Due to Wide View Front and Side Monitor Switch Operation)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking assist monitor display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>12 km/h (7.5 mph) or less</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side monitor display → Rear monitor display</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E199602" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Wide view front and side monitor switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Display conditions for manual display mode, when shift lever is not in R</ptxt>
<step3>
<ptxt>The side monitor system changes the displayed image when the wide view front and side monitor switch is operated while the following conditions are met and either the ignition switch is ON or the engine is running (no DTCs stored).</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL3" colwidth="0.99in"/>
<colspec colname="COLSPEC2" colwidth="1.49in"/>
<colspec colname="COLSPEC3" colwidth="0.99in"/>
<colspec colname="COLSPEC1" colwidth="0.99in"/>
<colspec colname="COL6" colwidth="2.62in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Automatic Mode Display Button</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Accessory Meter Screen</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Screen</ptxt>
<ptxt>(Changes Due to Wide View Front and Side Monitor Switch Operation)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parking assist monitor display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>12 km/h (7.5 mph) or less</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side monitor display → Accessory meter display → Wide view front monitor display</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E199603" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Wide view front and side monitor switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="V100137" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>When the vehicle speed is more than 12 km/h (7.5 mph)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>DISPLAY CONDITIONS FOR MULTI-TERRAIN MONITOR</ptxt>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
<atten3>
<ptxt>Make sure that the vehicle does not move by applying the parking brake firmly.</ptxt>
</atten3>
</step2>
<step2>
<ptxt>Turn multi-terrain select mode on.</ptxt>
</step2>
<step2>
<ptxt>Select "CAM. POSITION".</ptxt>
<figure>
<graphic graphicname="E199651" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>The following are the display conditions for camera selection in multi-terrain select mode when the shift lever is not in R.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Multi-terrain Select</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Less than 10 km/h (6.2 mph)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E199652E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Multi-information Display Outline</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Accessory Meter Outline</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Multi-information display changes when multi-function switch is pushed up/down</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Accessory meter changes when multi-function switch is pushed up/down</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>The following are the display conditions for camera selection in multi-terrain select mode when the shift lever is in R.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Multi-terrain Select</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Less than 10 km/h (6.2 mph)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="E199653E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Multi-information Display Outline</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Accessory Meter Outline</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Multi-information display changes when multi-function switch is pushed up/down</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Accessory meter changes when multi-function switch is pushed up/down</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>