<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002E" variety="S002E">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002E_7B9GZ_T00QN" variety="T00QN">
<name>LIGHTING SYSTEM</name>
<para id="RM0000011SV04MX" category="C" type-id="303CB" name-id="LE0VQ-21" from="201207" to="201210">
<dtccode>B2414</dtccode>
<dtcname>Steering Position Sensor Malfunction</dtcname>
<subpara id="RM0000011SV04MX_01" type-id="60" category="03" proc-id="RM23G0E___0000J0600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The headlight swivel ECU receives signals indicating the steering angle from the spiral cable sub-assembly (steering angle sensor) using CAN communication.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2414</ptxt>
</entry>
<entry valign="middle">
<ptxt>A malfunction in the steering angle sensor.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Spiral cable sub-assembly (steering angle sensor)</ptxt>
</item>
<item>
<ptxt>Headlight swivel ECU assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000011SV04MX_05" type-id="32" category="03" proc-id="RM23G0E___0000J0B00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E188441E04" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000011SV04MX_03" type-id="51" category="05" proc-id="RM23G0E___0000J0700000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>First perform the communication function inspections in How to Proceed with Troubleshooting to confirm that there are no CAN communication malfunctions before troubleshooting this problem.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000011SV04MX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000011SV04MX_04_0011" proc-id="RM23G0E___0000J0900000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PKT06UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000PKT06UX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2414 output does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SV04MX_04_0013" fin="true">OK</down>
<right ref="RM0000011SV04MX_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SV04MX_04_0007" proc-id="RM23G0E___0000J0800000">
<testtitle>REPLACE SPIRAL CABLE SUB-ASSEMBLY (STEERING ANGLE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the spiral cable with a new or normally functioning one (See page <xref label="Seep01" href="RM000002O8X01FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000011SV04MX_04_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000011SV04MX_04_0015" proc-id="RM23G0E___0000J0A00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PKT06UX"/>).</ptxt>
</test1>
<test1>
<ptxt>Recheck for DTCs (See page <xref label="Seep02" href="RM000000PKT06UX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2414 output does not reoccur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000011SV04MX_04_0005" fin="true">OK</down>
<right ref="RM0000011SV04MX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000011SV04MX_04_0013">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMI00EX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SV04MX_04_0003">
<testtitle>REPLACE HEADLIGHT SWIVEL ECU ASSEMBLY<xref label="Seep01" href="RM0000038VH017X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000011SV04MX_04_0005">
<testtitle>END (REPLACE SPIRAL CABLE SUB-ASSEMBLY)<xref label="Seep01" href="RM000002O8X01FX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>