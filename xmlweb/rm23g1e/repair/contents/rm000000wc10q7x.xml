<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000000WC10Q7X" category="C" type-id="302GV" name-id="ESMEC-06" from="201207" to="201210">
<dtccode>P0031</dtccode>
<dtcname>Oxygen (A/F) Sensor Heater Control Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P0032</dtccode>
<dtcname>Oxygen (A/F) Sensor Heater Control Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P0051</dtccode>
<dtcname>Oxygen (A/F) Sensor Heater Control Circuit Low (Bank 2 Sensor 1)</dtcname>
<dtccode>P0052</dtccode>
<dtcname>Oxygen (A/F) Sensor Heater Control Circuit High (Bank 2 Sensor 1)</dtcname>
<dtccode>P101D</dtccode>
<dtcname>A/F Sensor Heater Circuit Performance Bank 1 Sensor 1 Stuck ON</dtcname>
<dtccode>P103D</dtccode>
<dtcname>A/F Sensor Heater Circuit Performance Bank 2 Sensor 1 Stuck ON</dtcname>
<subpara id="RM000000WC10Q7X_01" type-id="60" category="03" proc-id="RM23G0E___00000OP00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40PAX_01"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When any of these DTCs is stored, the ECM enters fail-safe mode. The ECM turns off the air fuel ratio sensor heater in fail-safe mode. The ECM continues operating in fail-safe mode until the ignition switch is turned off.</ptxt>
</item>
<item>
<ptxt>Although the DTC titles say heated oxygen sensor, these DTCs relate to the air fuel ratio sensor.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor mounted in front of the Three-Way Catalytic Converter (TWC) and located near the engine assembly.</ptxt>
</item>
<item>
<ptxt>The ECM provides a pulse-width modulated control circuit to adjust the current through the heater. The air fuel ratio sensor heater circuit uses a relay on the +B side of the circuit.</ptxt>
<figure>
<graphic graphicname="A160615E09" width="7.106578999in" height="3.779676365in"/>
</figure>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0031</ptxt>
<ptxt>P0051</ptxt>
</entry>
<entry valign="middle">
<ptxt>The air fuel ratio sensor heater current is below 0.8 A (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in air fuel ratio sensor heater circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor heater (for bank 1, 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>No. 1 integration relay (A/F)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0032</ptxt>
<ptxt>P0052</ptxt>
</entry>
<entry valign="middle">
<ptxt>An air fuel ratio sensor heater current failure (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in air fuel ratio sensor heater circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor heater (for bank 1, 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>No. 1 integration relay (A/F)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P101D</ptxt>
<ptxt>P103D</ptxt>
</entry>
<entry valign="middle">
<ptxt>The heater current is higher than a specified value while the heater is not operating (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor closest to the engine assembly.</ptxt>
</item>
<item>
<ptxt>Sensor 2 refers to the sensor farthest away from the engine assembly.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000WC10Q7X_02" type-id="64" category="03" proc-id="RM23G0E___00000OQ00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses information from the air fuel ratio sensor to regulate the air fuel ratio and keep it close to the stoichiometric level. This maximizes the ability of the Three-Way Catalytic Converter (TWC) to purify the exhaust gases.</ptxt>
<ptxt>The air fuel ratio sensor detects oxygen levels in the exhaust gas and transmits the information to the ECM. The inner surface of the sensor element is exposed to the outside air. The outer surface of the sensor element is exposed to the exhaust gas. The sensor element is made of platinum-coated zirconia and includes an integrated heating element.</ptxt>
<ptxt>The zirconia element generates a small voltage when there is a large difference in the oxygen concentrations between the exhaust gas and outside air. The platinum coating amplifies this voltage generation.</ptxt>
<ptxt>The air fuel ratio sensor is more efficient when heated. When the exhaust gas temperature is low, the sensor cannot generate useful voltage signals without supplementary heating. The ECM regulates the supplementary heating using a duty-cycle approach to adjust the average current in the sensor heater element. If the heater current is outside the normal range, the signal transmitted by the air fuel ratio sensor becomes inaccurate, as a result, the ECM is unable to regulate air fuel ratio properly.</ptxt>
<ptxt>When the current in the air fuel ratio sensor heater is outside the normal operating range, the ECM interprets this as a malfunction in the sensor heater and stores a DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000WC10Q7X_07" type-id="32" category="03" proc-id="RM23G0E___00000OR00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40PAX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000WC10Q7X_08" type-id="51" category="05" proc-id="RM23G0E___00000OS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000WC10Q7X_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000WC10Q7X_09_0001" proc-id="RM23G0E___00000OT00000">
<testtitle>INSPECT AIR FUEL RATIO SENSOR (HEATER RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the air fuel ratio sensor (See page <xref label="Seep01" href="RM000002RV201TX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WC10Q7X_09_0004" fin="false">OK</down>
<right ref="RM000000WC10Q7X_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0004" proc-id="RM23G0E___00000OV00000">
<testtitle>CHECK TERMINAL VOLTAGE (+B OF AIR FUEL RATIO SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A150242E35" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C25-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bank 1</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bank 2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Air Fuel Ratio Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the air fuel ratio sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WC10Q7X_09_0002" fin="false">OK</down>
<right ref="RM000000WC10Q7X_09_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0002" proc-id="RM23G0E___00000OU00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR FUEL RATIO SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C25-1 (HA1A) - C37-17 (HA1A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-1 (HA2A) - C37-19 (HA2A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C25-1 (HA1A) or C37-17 (HA1A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-1 (HA2A) or C37-19 (HA2A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WC10Q7X_09_0010" fin="false">OK</down>
<right ref="RM000000WC10Q7X_09_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0010" proc-id="RM23G0E___00000OY00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z5X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for 1 minute or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
</test1>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>No output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>P0031, P0032, P0051, P0052, P101D or P103D</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000WC10Q7X_09_0011" fin="true">A</down>
<right ref="RM000000WC10Q7X_09_0012" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0005" proc-id="RM23G0E___00000OW00000">
<testtitle>INSPECT NO. 1 INTEGRATION RELAY (A/F)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the air fuel ratio sensor relay (See page <xref label="Seep01" href="RM000003BLB02MX_01_0015"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WC10Q7X_09_0008" fin="false">OK</down>
<right ref="RM000000WC10Q7X_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0008" proc-id="RM23G0E___00000OX00000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR FUEL RATIO SENSOR - NO. 1 INTEGRATION RELAY (A/F))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C25-2 (+B) - 1B-8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-2 (+B) - 1B-8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C25-2 (+B) or 1B-8 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-2 (+B) or 1B-8 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WC10Q7X_09_0003" fin="true">OK</down>
<right ref="RM000000WC10Q7X_09_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0007">
<testtitle>REPLACE AIR FUEL RATIO SENSOR<xref label="Seep01" href="RM000002W9Q00RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0012">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0006">
<testtitle>REPLACE NO. 1 INTEGRATION RELAY (A/F)</testtitle>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0013">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0011">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0VHX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WC10Q7X_09_0003">
<testtitle>CHECK ECM POWER SOURCE CIRCUIT<xref label="Seep01" href="RM000001DN8085X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>