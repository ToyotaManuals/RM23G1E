<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S0009" variety="S0009">
<name>1GR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S0009_7B8X2_T006Q" variety="T006Q">
<name>REAR CRANKSHAFT OIL SEAL</name>
<para id="RM000002B4V016X" category="A" type-id="30014" name-id="EMCK9-02" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000002B4V016X_01" type-id="01" category="01">
<s-1 id="RM000002B4V016X_01_0001" proc-id="RM23G0E___00003SH00001">
<ptxt>INSTALL REAR CRANKSHAFT OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply MP grease to the lip of a new oil seal.</ptxt>
<figure>
<graphic graphicname="A076329E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap in the oil seal until its surface is flush with the rear oil seal retainer edge.</ptxt>
<sst>
<sstitem>
<s-number>09223-78010</s-number>
</sstitem>
</sst>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Keep the lip free from foreign matter.</ptxt>
</item>
<item>
<ptxt>Do not tap the oil seal at an angle.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002B4V016X_01_0006" proc-id="RM23G0E___00003T000001">
<ptxt>INSTALL DRIVE PLATE AND RING GEAR SUB-ASSEMBLY (for Automatic Transmission)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the crankshaft.</ptxt>
<sst>
<sstitem>
<s-number>09213-54015</s-number>
<s-subnumber>91651-60855</s-subnumber>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<figure>
<graphic graphicname="A227952E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Install the front spacer, drive plate and rear spacer to the crankshaft.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Spacer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Drive Plate and Ring Gear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Spacer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Automatic Transmission Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>As the front spacer, drive plate and ring gear and rear spacer are not reversible, be sure to install it in the direction shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Apply adhesive to 2 or 3 threads at the end of the 8 bolts.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1324, Three Bond 1324 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<figure>
<graphic graphicname="A224777E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Uniformly install and tighten the 8 bolts in several steps in the sequence shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>83</t-value1>
<t-value2>846</t-value2>
<t-value4>61</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not start the engine for at least 1 hour after installing.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002B4V016X_01_0010" proc-id="RM23G0E___00003TJ00001">
<ptxt>INSTALL FLYWHEEL SUB-ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the crankshaft.</ptxt>
<sst>
<sstitem>
<s-number>09213-54015</s-number>
<s-subnumber>91651-60855</s-subnumber>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Temporarily install the flywheel with 8 new bolts.</ptxt>
</s2>
<s2>
<ptxt>Install and tighten the 8 bolts uniformly in several steps.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>306</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Mark the top of the bolts with paint.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 8 bolts 90° in the same sequence.</ptxt>
</s2>
<s2>
<ptxt>Check that the paint marks are now at a 90° angle to the top.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002B4V016X_01_0011" proc-id="RM23G0E___00008GI00001">
<ptxt>INSTALL CLUTCH DISC ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert SST into the clutch disc, and then install SST and the clutch disc together to the flywheel.</ptxt>
<figure>
<graphic graphicname="C215507E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<sst>
<sstitem>
<s-number>09301-00110</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Flywheel Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be sure to install the clutch disc so that it is facing in the correct direction.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000002B4V016X_01_0012" proc-id="RM23G0E___00008GJ00001">
<ptxt>INSTALL CLUTCH COVER ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215508E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Align the matchmark on the clutch cover with the one on the flywheel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Following the order shown in the illustration, tighten the 6 bolts, starting with the bolt located near the knock pin at the top.</ptxt>
<sst>
<sstitem>
<s-number>09301-00110</s-number>
</sstitem>
</sst>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Following the order in the illustration, tighten the bolts evenly one at a time.</ptxt>
</item>
<item>
<ptxt>Move SST up and down, and right and left lightly after checking that the clutch disc assembly is in the center, and then tighten the bolts.</ptxt>
</item>
</list1>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000002B4V016X_01_0013" proc-id="RM23G0E___00008GK00001">
<ptxt>INSPECT AND ADJUST CLUTCH COVER ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215509E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a dial indicator with a roller instrument, check the diaphragm spring tip alignment.</ptxt>
<sst>
<sstitem>
<s-number>09333-00013</s-number>
</sstitem>
</sst>
<spec>
<title>Maximum misalignment</title>
<specitem>
<ptxt>1.3 mm (0.0512 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the misalignment is more than the maximum, using SST, adjust the diaphragm spring tip alignment.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002B4V016X_01_0003" proc-id="RM23G0E___00003SI00001">
<ptxt>INSTALL AUTOMATIC TRANSMISSION ASSEMBLY (for Automatic Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the automatic transmission (See page <xref label="Seep01" href="RM0000018ZB047X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002B4V016X_01_0009" proc-id="RM23G0E___00003SK00001">
<ptxt>INSTALL MANUAL TRANSMISSION ASSEMBLY (for Manual Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the manual transmission (See page <xref label="Seep01" href="RM00000178Q00RX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>