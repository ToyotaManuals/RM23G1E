<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000Q" variety="S000Q">
<name>2TR-FE COOLING</name>
<ttl id="12011_S000Q_7B91G_T00B4" variety="T00B4">
<name>WATER PUMP</name>
<para id="RM000000V28017X" category="A" type-id="80001" name-id="CO5AI-02" from="201207">
<name>REMOVAL</name>
<subpara id="RM000000V28017X_02" type-id="01" category="01">
<s-1 id="RM000000V28017X_02_0095" proc-id="RM23G0E___00005BQ00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 13 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0096" proc-id="RM23G0E___00005BC00000">
<ptxt>REMOVE FRONT BUMPER COVER LOWER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper cover lower.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0097" proc-id="RM23G0E___00005BD00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Unhook the engine under cover from the vehicle body as shown in the illustration.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0092" proc-id="RM23G0E___000033V00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<figure>
<graphic graphicname="A223311E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reservoir Cap</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s2>
<ptxt>Install a vinyl hose to the radiator side.</ptxt>
<figure>
<graphic graphicname="G038197E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Vinyl Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install a vinyl hose to the engine side.</ptxt>
<figure>
<graphic graphicname="G038198E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Vinyl Hose</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Remove the radiator cap and drain the coolant.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug and drain the coolant from the engine.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0084" proc-id="RM23G0E___00005BK00000">
<ptxt>REMOVE RADIATOR RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223323" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the reservoir hose from the radiator.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and radiator reservoir.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0085" proc-id="RM23G0E___00005BL00000">
<ptxt>REMOVE FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A223324" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the claw to open the flexible hose clamp.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223325" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Loosen the 4 nuts holding the fluid coupling fan.</ptxt>
</s2>
<s2>
<ptxt>Remove the fan and generator V-belt (See page <xref label="Seep01" href="RM000000YMP01ZX"/>).</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A223326" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts holding the fan shroud.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts of the fluid coupling fan, and then remove the shroud together with the coupling fan.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0087" proc-id="RM23G0E___000033W00000">
<ptxt>REMOVE AIR CLEANER AND HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 clamps and disconnect the mass air flow meter connector.</ptxt>
<figure>
<graphic graphicname="A223920" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 4 clamps.</ptxt>
<figure>
<graphic graphicname="A223921" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Loosen the hose clamp and remove the air cleaner and hose.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0088" proc-id="RM23G0E___00006VJ00000">
<ptxt>REMOVE GENERATOR ASSEMBLY (for 80A Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the terminal cap.</ptxt>
<figure>
<graphic graphicname="A223928" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and generator wire.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the generator connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and generator.</ptxt>
<figure>
<graphic graphicname="A223930" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0089" proc-id="RM23G0E___0000D5N00000">
<ptxt>REMOVE GENERATOR ASSEMBLY (for 100A Type)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the generator connector.</ptxt>
<figure>
<graphic graphicname="A223928" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the wire harness from terminal B.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and generator.</ptxt>
<figure>
<graphic graphicname="A223929" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0093" proc-id="RM23G0E___00006VH00000">
<ptxt>REMOVE NO. 1 IDLER PULLEY SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, idler pulley and spacer.</ptxt>
<figure>
<graphic graphicname="A222362" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0094" proc-id="RM23G0E___000056600000">
<ptxt>REMOVE V-RIBBED BELT TENSIONER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and belt tensioner.</ptxt>
<figure>
<graphic graphicname="G035081E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000V28017X_02_0076" proc-id="RM23G0E___00006K300000">
<ptxt>REMOVE WATER PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 10 bolts, and then remove the water pump and gasket.</ptxt>
<figure>
<graphic graphicname="A223314" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>