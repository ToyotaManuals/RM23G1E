<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E8_T00NW" variety="T00NW">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000001J2806FX" category="J" type-id="802CK" name-id="AC96O-06" from="201207" to="201210">
<dtccode/>
<dtcname>Air Conditioning Control Panel Circuit</dtcname>
<subpara id="RM000001J2806FX_01" type-id="60" category="03" proc-id="RM23G0E___0000HEC00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit consists of the integration control and panel assembly and air conditioning amplifier assembly. When the integration control and panel assembly is operated, signals are transmitted to the air conditioning amplifier assembly through the LIN communication system.</ptxt>
<ptxt>If the LIN communication system malfunctions, the air conditioning amplifier assembly does not operate when the integration control and panel assembly is operated.</ptxt>
</content5>
</subpara>
<subpara id="RM000001J2806FX_02" type-id="32" category="03" proc-id="RM23G0E___0000HED00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E198323E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001J2806FX_03" type-id="51" category="05" proc-id="RM23G0E___0000HEE00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001J2806FX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001J2806FX_05_0018" proc-id="RM23G0E___0000HEH00000">
<testtitle>CHECK HARNESS AND CONNECTOR (INTEGRATION CONTROL AND PANEL - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G25 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G23 panel connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G23-3 (LIN1) - G25-37 (LIN1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-3 (LIN1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2806FX_05_0014" fin="false">OK</down>
<right ref="RM000001J2806FX_05_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2806FX_05_0014" proc-id="RM23G0E___0000HEG00000">
<testtitle>CHECK HARNESS AND CONNECTOR (INTEGRATION CONTROL AND PANEL - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G23 panel connector.</ptxt>
<figure>
<graphic graphicname="E196918E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G23-1 (+B) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-5 (IG) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-5 (IG) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-6 (ACC) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G23-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Integration Control and Panel Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001J2806FX_05_0004" fin="false">OK</down>
<right ref="RM000001J2806FX_05_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2806FX_05_0004" proc-id="RM23G0E___0000HEF00000">
<testtitle>REPLACE INTEGRATION CONTROL AND PANEL ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the integration control and panel assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM000003B46019X"/>).</ptxt>
</test1>
<test1>
<ptxt>Operate the integration control and panel assembly to check that it functions properly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Integration control and panel assembly operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2806FX_05_0017" fin="true">OK</down>
<right ref="RM000001J2806FX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2806FX_05_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001J2806FX_05_0009">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000045E301IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001J2806FX_05_0017">
<testtitle>END (INTEGRATION CONTROL AND PANEL ASSEMBLY IS FAULTY)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>