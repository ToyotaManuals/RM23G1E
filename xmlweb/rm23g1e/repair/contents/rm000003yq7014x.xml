<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0022" variety="S0022">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0022_7B9CG_T00M4" variety="T00M4">
<name>FRONT SEAT SIDE AIRBAG ASSEMBLY</name>
<para id="RM000003YQ7014X" category="N" type-id="3000N" name-id="RS93F-09" from="201207" to="201210">
<name>DISPOSAL</name>
<subpara id="RM000003YQ7014X_01" type-id="11" category="10" proc-id="RM23G0E___0000FS900000">
<content3 releasenbr="1">
<atten2>
<ptxt>Before performing pre-disposal deployment of any SRS part, review and closely follow all applicable environmental and hazardous material regulations. Pre-disposal deployment may be considered hazardous material treatment.</ptxt>
</atten2>
</content3>
</subpara>
<subpara id="RM000003YQ7014X_02" type-id="01" category="01">
<s-1 id="RM000003YQ7014X_02_0003" proc-id="RM23G0E___0000FSC00000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>When scrapping a vehicle equipped with an SRS or disposing of the front seat side airbag, be sure to deploy the airbag first in accordance with the procedure described below. If any abnormality occurs with the airbag deployment, contact the service department of the distributor.</ptxt>
</atten4>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Never dispose of a front seat side airbag that has an unactivated airbag.</ptxt>
</item>
<item>
<ptxt>The airbag produces an exploding sound when it is deployed, so perform the operation outdoors and where it will not create a nuisance to nearby residents.</ptxt>
</item>
<item>
<ptxt>When deploying the airbag, always use the specified SST (SRS Airbag Deployment Tool). Perform the operation in a place away from electrical noise.</ptxt>
</item>
<item>
<ptxt>When deploying the airbag, perform the operation at least 10 m (32.8 ft.) away from the front seat side airbag.</ptxt>
</item>
<item>
<ptxt>The front seat side airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a front seat side airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water, etc. to a front seat side airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
<item>
<ptxt>An airbag or pretensioner may be activated by static electricity. To prevent this, be sure to touch a metal surface with bare hands to discharge static electricity before performing this procedure.</ptxt>
</item>
</list1>
</atten2>
</content1>
</s-1>
<s-1 id="RM000003YQ7014X_02_0001" proc-id="RM23G0E___0000FSA00000">
<ptxt>DISPOSE OF FRONT SEAT SIDE AIRBAG ASSEMBLY (WHEN INSTALLED IN VEHICLE)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Prepare a battery as the power source to deploy the airbag.</ptxt>
</atten4>
<s2>
<ptxt>Check the function of SST (See page <xref label="Seep01" href="RM000000XFD0GRX"/>). </ptxt>
<figure>
<graphic graphicname="C110371E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Read the precaution (See page <xref label="Seep02" href="RM000000XFD0GRX"/>).</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>w/ Navigation System (for HDD):</ptxt>
<ptxt>After the ignition switch is turned off, the HDD navigation system requires approximately a minute to record various types of memory and settings. As a result, after turning the ignition switch off, wait a minute or more before disconnecting the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>for Manual Seat:</ptxt>
<s3>
<ptxt>Remove the front seat (See page <xref label="Seep04" href="RM00000465Y00FX"/>).</ptxt>
<atten4>
<ptxt>Keep the front seat in the cabin.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>for Power Seat:</ptxt>
<s3>
<ptxt>Remove the front seat (See page <xref label="Seep06" href="RM00000467200HX"/>).</ptxt>
<atten4>
<ptxt>Keep the front seat in the cabin.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>for Walk in Seat Type:</ptxt>
<s3>
<ptxt>Remove the front seat (See page <xref label="Seep07" href="RM0000046B8005X"/>).</ptxt>
<atten4>
<ptxt>Keep the front seat in the cabin.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Install SST.</ptxt>
<figure>
<graphic graphicname="B238359E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Disconnect the connector (yellow-colored) from the front seat side airbag.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect SST connector to the front seat side airbag connector.</ptxt>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
<sstitem>
<s-number>09082-00820</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>To avoid damaging SST connector and wire harness, do not lock the secondary lock of the twin lock.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>for Manual Seat:</ptxt>
<ptxt>Install the front seat (See page <xref label="Seep05" href="RM00000465W00GX"/>).</ptxt>
</s3>
<s3>
<ptxt>for Power Seat:</ptxt>
<ptxt>Install the front seat (See page <xref label="Seep08" href="RM00000466Z00HX"/>).</ptxt>
</s3>
<s3>
<ptxt>for Walk in Seat Type:</ptxt>
<ptxt>Install the front seat (See page <xref label="Seep09" href="RM0000046B6005X"/>).</ptxt>
</s3>
<s3>
<ptxt>Move SST at least 10 m (32.8 ft.) away from the vehicle front side window.</ptxt>
<figure>
<graphic graphicname="C110476E26" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Battery</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>10 m or more</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Maintaining enough clearance for SST wire harness in the front side window, close all doors and windows of the vehicle.</ptxt>
<atten3>
<ptxt>Take care not to damage SST wire harness.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect the red clip of SST to the positive (+) battery terminal and the black clip of SST to the negative (-) battery terminal.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Deploy the airbag.</ptxt>
<s3>
<ptxt>Check that no one is inside the vehicle or within a 10 m (32.8 ft.) radius of the vehicle.</ptxt>
</s3>
<s3>
<ptxt>Press SST activation switch to deploy the airbag.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>When deploying the airbag, make sure that no one is near the vehicle.</ptxt>
</item>
<item>
<ptxt>The front seat side airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a front seat side airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a front seat side airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
</list1>
</atten2>
<atten4>
<ptxt>The airbag is deployed as the LED of SST activation switch comes on.</ptxt>
</atten4>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003YQ7014X_02_0002" proc-id="RM23G0E___0000FSB00000">
<ptxt>DISPOSE OF FRONT SEAT SIDE AIRBAG ASSEMBLY (WHEN NOT INSTALLED IN VEHICLE)</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Be sure to follow the procedure detailed below when deploying the airbag.</ptxt>
</atten3>
<atten4>
<ptxt>Prepare a battery as the power source to deploy the airbag.</ptxt>
</atten4>
<s2>
<ptxt>Check the function of SST (See page <xref label="Seep01" href="RM000000XFD0GRX"/>).</ptxt>
<figure>
<graphic graphicname="C110371E15" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the front seat side airbag assembly (See page <xref label="Seep02" href="RM00000465Z00PX"/>).</ptxt>
</s2>
<s2>
<ptxt>Using a service-purpose wire harness for the vehicle, tie the front seat side airbag to a tire.</ptxt>
<figure>
<graphic graphicname="B104882E29" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Wire Harness Diameter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Stripped Wire Harness Section</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Cross-sectional area of stripped wire harness section</title>
<specitem>
<ptxt>1.25 mm<sup>2</sup> (0.00192 in.<sup>2</sup>) or more</ptxt>
</specitem>
</spec>
<atten2>
<ptxt>If the wire harness is too thin or an alternative object is used to tie the front seat side airbag, it may be snapped by the shock when the airbag is deployed. Always use a wire harness for vehicle use with a cross-sectional area of at least 1.25 mm<sup>2</sup> (0.00192 in.<sup>2</sup>).</ptxt>
</atten2>
<atten4>
<ptxt>To calculate the cross-sectional area of the stripped wire harness section: Cross-sectional area = 3.14 x (Diameter)<sup>2</sup> / 4</ptxt>
</atten4>
<s3>
<ptxt>Install the 2 nuts to the front seat side airbag.</ptxt>
<figure>
<graphic graphicname="C107070" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Wind the wire harness around the stud bolts of the front seat side airbag as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="C107071" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Position the front seat side airbag inside a tire with the airbag deployment side facing inside.</ptxt>
<figure>
<graphic graphicname="C107072E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Width</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Inner Diameter</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Tire size</title>
<specitem>
<ptxt>Must exceed the following dimensions</ptxt>
</specitem>
<subtitle>Width</subtitle>
<specitem>
<ptxt>185 mm (7.28 in.)</ptxt>
</specitem>
<subtitle>Inner diameter</subtitle>
<specitem>
<ptxt>360 mm (14.2 in.)</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Tie the front seat side airbag to the tire with several wire harnesses.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Make sure that the wire harnesses are tight. If there is slack in the wire harnesses, the front seat side airbag may become loose due to the shock when the airbag is deployed.</ptxt>
</item>
<item>
<ptxt>Always tie the front seat side airbag with the airbag deployment side facing inside.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<ptxt>As the tire may be damaged by the airbag deployment, use a tire that you are planning to throw away.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Connect SST connector to the front seat side airbag connector.</ptxt>
<sst>
<sstitem>
<s-number>09082-00820</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C107073E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Place tires.</ptxt>
<figure>
<graphic graphicname="C107074E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Tire</ptxt>
<ptxt>(2 or more)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s3>
<ptxt>Place at least 2 tires under the tire which the front seat side airbag is tied to.</ptxt>
</s3>
<s3>
<ptxt>Place at least 2 tires over the tire which the front seat side airbag is tied to. The top tire should have a disc wheel installed.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not place SST connector under the tires because it could be damaged.</ptxt>
</item>
<item>
<ptxt>As the disc wheel may be damaged by the airbag deployment, use a disc wheel that you are planning to throw away.</ptxt>
</item>
<item>
<ptxt>As the tires may be damaged by the airbag deployment, use tires that you are planning to throw away.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Tie the tires together with 2 wire harnesses.</ptxt>
<figure>
<graphic graphicname="B105087" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<ptxt>Make sure that the wire harnesses are tight. Looseness in the wire harnesses results in the tires coming free due to the shock when the airbag is deployed.</ptxt>
</atten2>
</s3>
</s2>
<s2>
<ptxt>Connect SST connector.</ptxt>
<sst>
<sstitem>
<s-number>09082-00700</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C107075E27" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Front Seat Side Airbag</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Battery</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>10 m or more</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>To avoid damaging SST connector and wire harness, do not lock the secondary lock of the twin lock. Also, secure some slack for SST wire harness inside the tires.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Deploy the airbag.</ptxt>
<s3>
<ptxt>Connect the red clip of SST to the positive (+) battery terminal and the black clip of SST to the negative (-) battery terminal.</ptxt>
</s3>
<s3>
<ptxt>Check that no one is within a 10 m (32.8 ft.) radius of the tire which the front seat side airbag is tied to.</ptxt>
</s3>
<s3>
<ptxt>Press SST activation switch to deploy the airbag.</ptxt>
<atten2>
<ptxt>When deploying the airbag, make sure that no one is near the tires.</ptxt>
</atten2>
<atten4>
<ptxt>The airbag is deployed as the LED of SST activation switch comes on.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Dispose of the front seat side airbag.</ptxt>
<figure>
<graphic graphicname="H000544E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten2>
<list1 type="unordered">
<item>
<ptxt>The front seat side airbag becomes extremely hot when the airbag is deployed, so do not touch it for at least 30 minutes after deployment.</ptxt>
</item>
<item>
<ptxt>Use gloves and safety glasses when handling a front seat side airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Do not apply water to a front seat side airbag with a deployed airbag.</ptxt>
</item>
<item>
<ptxt>Always wash your hands with water after completing the operation.</ptxt>
</item>
</list1>
</atten2>
<s3>
<ptxt>Remove the front seat side airbag from the tire.</ptxt>
</s3>
<s3>
<ptxt>Place the front seat side airbag in a plastic bag, tie it tightly and dispose of it in the same way as other general parts.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>