<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001M" variety="S001M">
<name>STEERING COLUMN</name>
<ttl id="12038_S001M_7B98U_T00II" variety="T00II">
<name>STEERING WHEEL</name>
<para id="RM0000039SA01CX" category="A" type-id="80001" name-id="SR3LT-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000039SA01CX_02" type-id="11" category="10" proc-id="RM23G0E___0000BFU00001">
<content3 releasenbr="1">
<atten2>
<ptxt>Some of these service operations affect the SRS airbag system. Read the precautionary notices concerning the SRS airbag system before servicing the steering wheel (See page <xref label="Seep01" href="RM000000KT10J3X"/>).</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000039SA01CX_01" type-id="01" category="01">
<s-1 id="RM0000039SA01CX_01_0008">
<ptxt>PLACE FRONT WHEELS FACING STRAIGHT AHEAD</ptxt>
</s-1>
<s-1 id="RM0000039SA01CX_01_0002" proc-id="RM23G0E___0000BFO00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000039SA01CX_01_0017" proc-id="RM23G0E___00007BD00001">
<ptxt>REMOVE NO. 3 STEERING WHEEL LOWER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
<figure>
<graphic graphicname="B238307" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000039SA01CX_01_0018" proc-id="RM23G0E___00007BE00001">
<ptxt>REMOVE NO. 2 STEERING WHEEL LOWER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
<figure>
<graphic graphicname="B238308E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>w/o Cruise Control System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>w/ Cruise Control System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000039SA01CX_01_0019" proc-id="RM23G0E___00007BC00001">
<ptxt>REMOVE STEERING PAD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" socket wrench, loosen the 2 screws until the groove along the screw circumference catches on the screw case.</ptxt>
<figure>
<graphic graphicname="B238309E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Screw Case</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Pull out the steering pad from the steering wheel as shown in the illustration. Then support the steering pad with one hand.</ptxt>
<figure>
<graphic graphicname="B238310" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>When removing the steering pad, do not pull the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Disconnect the horn connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 connectors and remove the steering pad.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000039SA01CX_01_0012" proc-id="RM23G0E___0000BFR00001">
<ptxt>REMOVE NO. 1 STEERING WHEEL ORNAMENT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="C217183" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the claw and remove the ornament.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SA01CX_01_0015" proc-id="RM23G0E___0000BFS00001">
<ptxt>REMOVE NO. 2 STEERING WHEEL ORNAMENT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the ornament.</ptxt>
<atten4>
<ptxt>Refer to "Remove No. 1 Steering Wheel Ornament".</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SA01CX_01_0010" proc-id="RM23G0E___0000BFQ00001">
<ptxt>REMOVE STEERING PAD SWITCH ASSEMBLY (w/ Steering Pad Switch)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the steering pad switch assembly.</ptxt>
<atten4>
<ptxt>Refer to "Remove No. 1 Steering Wheel Ornament".</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SA01CX_01_0016" proc-id="RM23G0E___0000BFT00001">
<ptxt>REMOVE STEERING PAD SWITCH RH (w/ Steering Pad Switch)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach claw and remove the steering pad switch RH.</ptxt>
<atten4>
<ptxt>Refer to "REMOVE NO. 1 STEERING WHEEL ORNAMENT".</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000039SA01CX_01_0014" proc-id="RM23G0E___00007BA00001">
<ptxt>REMOVE CRUISE CONTROL MAIN SWITCH (w/ Cruise Control System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B239943" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 screws and switch.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000039SA01CX_01_0009" proc-id="RM23G0E___0000BFP00001">
<ptxt>REMOVE STEERING WHEEL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the steering wheel set nut.</ptxt>
</s2>
<s2>
<ptxt>Put matchmarks on the steering wheel and main shaft.</ptxt>
<figure>
<graphic graphicname="C215868E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using SST, remove the steering wheel assembly.</ptxt>
<sst>
<sstitem>
<s-number>09950-50013</s-number>
<s-subnumber>09951-05010</s-subnumber>
<s-subnumber>09952-05010</s-subnumber>
<s-subnumber>09953-05020</s-subnumber>
<s-subnumber>09954-05011</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>