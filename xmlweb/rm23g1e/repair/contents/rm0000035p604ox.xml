<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM0000035P604OX" category="C" type-id="803LJ" name-id="BC3TK-44" from="201207" to="201210">
<dtccode>C1420</dtccode>
<dtcname>Acceleration Sensor Malfunction</dtcname>
<subpara id="RM0000035P604OX_01" type-id="60" category="03" proc-id="RM23G0E___0000AC200000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1419 and C1435 (See page <xref label="Seep01" href="RM0000035P4051X_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1420</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>After the difference between GL1 and GL2 becomes 0.6 G or more with the vehicle stationary, the difference remains 0.4 G or more for 60 seconds or more.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Sensor installation</ptxt>
</item>
<item>
<ptxt>Yaw rate and acceleration sensor</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000035P604OX_02" type-id="32" category="03" proc-id="RM23G0E___0000AC300000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1232, C1243 and C1245 (See page <xref label="Seep01" href="RM000001EA301XX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000035P604OX_03" type-id="51" category="05" proc-id="RM23G0E___0000AC400000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the yaw rate and acceleration sensor, perform calibration (See page <xref label="Seep01" href="RM00000452J00IX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>When DTC U0123, U0124 and/or U0126 is output together with DTC C1420, inspect and repair the trouble areas indicated by DTC U0123, U0124 and/or U0126 first (See page <xref label="Seep02" href="RM000000YUO0J1X"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000035P604OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000035P604OX_04_0001" proc-id="RM23G0E___0000AC500000">
<testtitle>CHECK YAW RATE AND ACCELERATION SENSOR INSTALLATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the yaw rate and acceleration sensor is installed properly (See page <xref label="Seep01" href="RM000000SS5051X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The sensor is tightened to the specified torque.</ptxt>
</specitem>
<specitem>
<ptxt>The sensor is not installed at an angle.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000035P604OX_04_0002" fin="true">OK</down>
<right ref="RM0000035P604OX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000035P604OX_04_0002">
<testtitle>REPLACE YAW RATE AND ACCELERATION SENSOR<xref label="Seep01" href="RM000000SS5051X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000035P604OX_04_0003">
<testtitle>INSTALL YAW RATE AND ACCELERATION SENSOR CORRECTLY<xref label="Seep01" href="RM000000SS205AX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>