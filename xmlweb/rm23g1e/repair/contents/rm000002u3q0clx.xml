<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B98Z_T00IN" variety="T00IN">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM000002U3Q0CLX" category="J" type-id="804HC" name-id="AV9PH-03" from="201210">
<dtccode/>
<dtcname>Mute Signal Circuit between Radio Receiver and Multi-media Interface ECU</dtcname>
<subpara id="RM000002U3Q0CLX_01" type-id="60" category="03" proc-id="RM23G0E___0000BQ700001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The multi-media interface ECU sends a mute signal to the radio receiver.</ptxt>
<ptxt>The radio receiver assembly controls the volume according to the MUTE signal from the multi-media interface ECU.</ptxt>
<ptxt>The MUTE signal is sent to reduce noise and popping sounds generated when switching modes.</ptxt>
<ptxt>If there is an open in the circuit, noise can be heard from the speakers when changing the sound source.</ptxt>
<ptxt>If there is a short in the circuit, even though the radio receiver assembly is functioning, no sound, or only an extremely faint sound, can be heard.</ptxt>
</content5>
</subpara>
<subpara id="RM000002U3Q0CLX_02" type-id="32" category="03" proc-id="RM23G0E___0000BQ800001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E175068E34" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002U3Q0CLX_04" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002U3Q0CLX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002U3Q0CLX_03_0003" proc-id="RM23G0E___0000BQA00001">
<testtitle>CHECK MULTI-MEDIA INTERFACE ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E195974E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>H33-6 (MUT1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ACC, audio system is playing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Above 3.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Audio system is changing modes</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Multi-media Interface ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002U3Q0CLX_03_0006" fin="true">OK</down>
<right ref="RM000002U3Q0CLX_03_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002U3Q0CLX_03_0002" proc-id="RM23G0E___0000BQ900001">
<testtitle>CHECK HARNESS AND CONNECTOR (RADIO RECEIVER - MULTI-MEDIA INTERFACE ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H36 radio receiver assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the H33 multi-media interface ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H36-6 (MUTE) - H33-6 (MUT1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H36-6 (MUTE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (w/o Accessory Meter)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (w/ Accessory Meter)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002U3Q0CLX_03_0007" fin="true">A</down>
<right ref="RM000002U3Q0CLX_03_0008" fin="true">B</right>
<right ref="RM000002U3Q0CLX_03_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002U3Q0CLX_03_0006">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000012A80F4X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U3Q0CLX_03_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002U3Q0CLX_03_0007">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01QX_01_0011"/>
</testtitle>
</testgrp>
<testgrp id="RM000002U3Q0CLX_03_0008">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01QX_01_0028"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>