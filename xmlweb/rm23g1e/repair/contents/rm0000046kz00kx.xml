<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001A" variety="S001A">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001A_7B968_T00FW" variety="T00FW">
<name>STEERING KNUCKLE</name>
<para id="RM0000046KZ00KX" category="A" type-id="80001" name-id="AD1A7-02" from="201207">
<name>REMOVAL</name>
<subpara id="RM0000046KZ00KX_01" type-id="11" category="10" proc-id="RM23G0E___00009CR00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000046KZ00KX_02" type-id="01" category="01">
<s-1 id="RM0000046KZ00KX_02_0001" proc-id="RM23G0E___00009CS00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>w/ Navigation System (for HDD):</ptxt>
<ptxt>After the ignition switch is turned off, the HDD navigation system requires approximately a minute to record various types of memory and settings. As a result, after turning the ignition switch off, wait a minute or more before disconnecting the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>). </ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000046KZ00KX_02_0002" proc-id="RM23G0E___00009AJ00000">
<ptxt>REMOVE FRONT SPEED SENSOR LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and speed sensor from the knuckle.</ptxt>
<atten3>
<ptxt>Pull out the sensor while trying as much as possible not to rotate it.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000046KZ00KX_02_0004" proc-id="RM23G0E___00009CT00000">
<ptxt>REMOVE FRONT AXLE HUB SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the front axle hub (See page <xref label="Seep01" href="RM0000016X602BX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046KZ00KX_02_0008" proc-id="RM23G0E___0000BH000000">
<ptxt>DISCONNECT TIE ROD END SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, disconnect the tie rod end assembly.</ptxt>
<sst>
<sstitem>
<s-number>09610-20012</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="C215754E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046KZ00KX_02_0006" proc-id="RM23G0E___00009CU00000">
<ptxt>DISCONNECT FRONT LOWER BALL JOINT ATTACHMENT LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="F042737E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and disconnect the front lower ball joint attachment from the axle.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046KZ00KX_02_0007" proc-id="RM23G0E___00009CV00000">
<ptxt>REMOVE STEERING KNUCKLE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Support the front suspension lower arm LH with a jack.</ptxt>
</s2>
<s2>
<ptxt>Remove the clip and nut.</ptxt>
</s2>
<s2>
<ptxt>Using SST, disconnect the upper ball joint from the steering knuckle.</ptxt>
<sst>
<sstitem>
<s-number>09628-62011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="F042736E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not the damage the ball joint dust cover.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the steering knuckle.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>