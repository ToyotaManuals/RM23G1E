<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>5L-E ENGINE CONTROL</name>
<ttl id="12005_S0006_7B8VX_T005L" variety="T005L">
<name>ECD SYSTEM</name>
<para id="RM000000VQT00GX" category="S" type-id="3001E" name-id="ES12H-10" from="201207">
<name>DIAGNOSTIC TROUBLE CODE CHART</name>
<subpara id="RM000000VQT00GX_z0" proc-id="RM23G0E___000015G00000">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Parameters listed in the chart may not be exactly the same as yours depending on the type of instrument and other factors.</ptxt>
</item>
<item>
<ptxt>If a trouble code is output during the DTC check, inspect the trouble areas listed for that code. For details of the code, refer to the "See page" below.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>ECD System</title>
<tgroup cols="5" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="1.42in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="1.42in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.42in" colsep="0"/>
<colspec colnum="4" colname="4" colwidth="1.42in" colsep="0"/>
<colspec colnum="5" colname="5" colwidth="1.4in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>MIL</ptxt>
</entry>
<entry colname="4" colsep="1" align="center">
<ptxt>Memory</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>12</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Crankshaft Position Sensor Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000VQU00EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>13 (2)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Engine Speed Sensor Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000001MFL00CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>14 (4)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Timing Control System Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000001MFM00BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>15 (4)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Throttle Motor Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000001MFN00BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>17 (1)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Interior IC Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000001MFO00DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>18 (5)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Spill Control Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000001MFP00CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>19 (1)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Accelerator Position Sensor Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000Y7000GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>19 (2)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Accelerator Position Sensor Range / Performance Problem</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000Y7100DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>22</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Engine Coolant Temperature Sensor Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000001MH300CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>24</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Air Temperature Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000VR300EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>32 (1)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Injection Pump System Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000001MFR00DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>35</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Manifold Absolute Pressure / Barometric Pressure Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000VR400EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>39 (5)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Fuel Temperature Sensor Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000001MFS00CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>42</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Vehicle Speed Sensor Signal Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>Comes on</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000VQW00EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>51</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Stop Light Switch Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" align="left">
<ptxt>DTC stored</ptxt>
</entry>
<entry colname="5" colsep="1" align="center">
<ptxt>
<xref href="RM000000VQX00EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>