<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>5L-E ENGINE CONTROL</name>
<ttl id="12005_S0006_7B8VX_T005L" variety="T005L">
<name>ECD SYSTEM</name>
<para id="RM000001MFN00BX" category="C" type-id="801HK" name-id="ESMFB-03" from="201207">
<dtccode>15 (4)</dtccode>
<dtcname>Throttle Motor Circuit Malfunction</dtcname>
<subpara id="RM000001MFN00BX_01" type-id="60" category="03" proc-id="RM23G0E___000019K00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The throttle control motor is operated by the ECM and it opens and closes the throttle valve.</ptxt>
<ptxt>The fully open position of the throttle valve is detected by the throttle open switch mounted on the venturi.</ptxt>
<ptxt>If this DTC is stored, the ECM shuts down the power for the throttle control motor.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="3.19in"/>
<colspec colname="COL3" colwidth="3.18in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>15 (4)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open or short in the throttle control motor circuit.</ptxt>
</entry>
<entry morerows="1" valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open or short in throttle control motor circuit</ptxt>
</item>
<item>
<ptxt>Throttle control motor</ptxt>
</item>
<item>
<ptxt>Throttle valve</ptxt>
</item>
<item>
<ptxt>Open or short in throttle open switch circuit</ptxt>
</item>
<item>
<ptxt>Throttle open switch</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>Open or short in the throttle open switch circuit.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001MFN00BX_02" type-id="32" category="03" proc-id="RM23G0E___000019L00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A227798E05" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001MFN00BX_03" type-id="51" category="05" proc-id="RM23G0E___000019M00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001MFN00BX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001MFN00BX_04_0001" proc-id="RM23G0E___000019N00000">
<testtitle>INSPECT DIESEL THROTTLE BODY (THROTTLE OPEN SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the throttle open switch (See page <xref label="Seep01" href="RM0000012UY009X_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001MFN00BX_04_0002" fin="false">OK</down>
<right ref="RM000001MFN00BX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0002" proc-id="RM23G0E___000019O00000">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE OPEN SWITCH - ECM AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle open switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C110-1 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C102-15 (THOC) - C110-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.37in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C102-15 (THOC) or C110-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the throttle open switch connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001MFN00BX_04_0003" fin="false">OK</down>
<right ref="RM000001MFN00BX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0003" proc-id="RM23G0E___000019P00000">
<testtitle>CHECK ECM (THROTTLE CONTROL MOTOR CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>During engine racing, check the waveforms according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="A106827E08" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>OK</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C101-10 (LU+A) - C101-13 (E01)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine racing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C101-9 (LU-A) - C101-13 (E01)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine racing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C101-8 (LU+B) - C101-13 (E01)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine racing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Correct waveform is as shown</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C101-7 (LU-B) - C101-13 (E01)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine racing</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Correct waveform is as shown</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Signal waveforms</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001MFN00BX_04_0010" fin="true">OK</down>
<right ref="RM000001MFN00BX_04_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0012" proc-id="RM23G0E___000019R00000">
<testtitle>INSPECT DIESEL THROTTLE BODY (THROTTLE CONTROL MOTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the throttle control motor (See page <xref label="Seep01" href="RM0000012UY009X_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001MFN00BX_04_0005" fin="false">OK</down>
<right ref="RM000001MFN00BX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0005" proc-id="RM23G0E___000019Q00000">
<testtitle>CHECK HARNESS AND CONNECTOR (THROTTLE CONTROL MOTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the throttle control motor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title> Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C105-6 (LU+A) - C101-10 (LU+A)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C105-4 (LU-A) - C101-9 (LU-A)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C105-3 (LU+B) - C101-8 (LU+B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C105-1 (LU-B) - C101-7 (LU-B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C105-6 or C101-10 (LU+A) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C105-4 or C101-9 (LU-A) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C105-3 or C101-8 (LU+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C105-1 or C101-7 (LU-B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the throttle control motor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001MFN00BX_04_0011" fin="true">OK</down>
<right ref="RM000001MFN00BX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0006">
<testtitle>REPLACE DIESEL THROTTLE BODY<xref label="Seep01" href="RM0000012V0009X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0008">
<testtitle>REPLACE DIESEL THROTTLE BODY<xref label="Seep01" href="RM0000012V0009X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001MFN00BX_04_0011">
<testtitle>CHECK ECM POWER SOURCE CIRCUIT<xref label="Seep01" href="RM000000ZQP019X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>