<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A8_T00JW" variety="T00JW">
<name>CLEARANCE WARNING ECU (for RHD)</name>
<para id="RM00000470300CX" category="A" type-id="30014" name-id="PM4CT-02" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM00000470300CX_01" type-id="01" category="01">
<s-1 id="RM00000470300CX_01_0001" proc-id="RM23G0E___0000D2900000">
<ptxt>INSTALL CLEARANCE WARNING ECU ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the clearance warning ECU with the bolt.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000470300CX_01_0013" proc-id="RM23G0E___0000D2D00000">
<ptxt>INSTALL DRIVER SIDE JUNCTION BLOCK ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the connector as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E178382" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be sure to connect each connector securely.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Attach the 2 claws to lock the connector lock as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E178380" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the claw to connect the connector as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="E178378" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be sure to connect the connector securely.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the driver side junction block assembly with the bolt and 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector labeled (1).</ptxt>
<figure>
<graphic graphicname="E197201E07" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Be sure to connect each connector securely.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Attach the 2 claws to connect the 2 connectors labeled (2) as shown in the illustration.</ptxt>
<atten3>
<ptxt>Be sure to connect each connector securely.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the 3 connectors.</ptxt>
<atten3>
<ptxt>Be sure to connect each connector securely.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000470300CX_01_0003" proc-id="RM23G0E___0000D2A00000">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL AIRBAG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the lower No. 1 instrument panel airbag with the 4 bolts.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000470300CX_01_0004" proc-id="RM23G0E___0000CL200000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL FINISH PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect each connector and each cable.</ptxt>
</s2>
<s2>
<ptxt>Attach the 14 clips to install the lower instrument panel finish panel.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 claws to close the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000470300CX_01_0005" proc-id="RM23G0E___00007BI00000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 clips and 2 guides to install the No. 2 instrument panel under cover.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
<figure>
<graphic graphicname="B298555E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000470300CX_01_0012" proc-id="RM23G0E___00008C800000">
<ptxt>INSTALL INSTRUMENT PANEL FINISH PLATE GARNISH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect each connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 clips to install the instrument panel finish plate garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000470300CX_01_0006" proc-id="RM23G0E___0000CL100000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL FINISH PANEL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect each connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 clips to install the instrument panel finish panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000470300CX_01_0007" proc-id="RM23G0E___0000CL400000">
<ptxt>INSTALL INSTRUMENT SIDE PANEL RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 clips, claw and 3 guides to install the instrument side panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000470300CX_01_0008" proc-id="RM23G0E___0000B9Q00000">
<ptxt>INSTALL COWL SIDE TRIM BOARD RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and claw to install the cowl side trim board.</ptxt>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000470300CX_01_0009" proc-id="RM23G0E___0000B9R00000">
<ptxt>INSTALL DOOR SCUFF PLATE ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 clips, 10 claws and 2 guides to install the door scuff plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000470300CX_01_0010" proc-id="RM23G0E___0000D2B00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000470300CX_01_0011" proc-id="RM23G0E___0000D2C00000">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0GRX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>