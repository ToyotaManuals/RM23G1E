<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1KD-FTV ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7B8XF_T0073" variety="T0073">
<name>REAR CRANKSHAFT OIL SEAL</name>
<para id="RM00000126G031X" category="A" type-id="80001" name-id="EM6OH-05" from="201210">
<name>REMOVAL</name>
<subpara id="RM00000126G031X_01" type-id="01" category="01">
<s-1 id="RM00000126G031X_01_0002" proc-id="RM23G0E___00004E100001">
<ptxt>REMOVE MANUAL TRANSMISSION ASSEMBLY (for Manual Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the manual transmission (See page <xref href="RM00000178S00SX" label="Seep01"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126G031X_01_0012" proc-id="RM23G0E___00004E400001">
<ptxt>REMOVE AUTOMATIC TRANSMISSION ASSEMBLY (for Automatic Transmission)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the automatic transmission (See page <xref label="Seep01" href="RM0000013F601BX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126G031X_01_0010" proc-id="RM23G0E___00008HG00001">
<ptxt>REMOVE CLUTCH COVER ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215513E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Place matchmarks on the clutch cover and flywheel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Loosen each set bolt one turn at a time until spring tension is released.</ptxt>
</s2>
<s2>
<ptxt>Remove the 6 set bolts and pull off the clutch cover.</ptxt>
<atten3>
<ptxt>Do not drop the clutch disc.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM00000126G031X_01_0011" proc-id="RM23G0E___00008HH00001">
<ptxt>REMOVE CLUTCH DISC ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Keep the lining part of the clutch disc, the pressure plate and the surface of the flywheel away from oil and foreign matter.</ptxt>
</atten3>
</content1></s-1>
<s-1 id="RM00000126G031X_01_0006" proc-id="RM23G0E___00004PM00001">
<ptxt>REMOVE FLYWHEEL SUB-ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the crankshaft pulley.</ptxt>
<figure>
<graphic graphicname="A225337E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09213-58014</s-number>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the 8 bolts and flywheel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000126G031X_01_0014" proc-id="RM23G0E___00004PN00001">
<ptxt>REMOVE PUMP IMPELLER DRIVE PLATE (for Automatic Transmission)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the crankshaft pulley.</ptxt>
<figure>
<graphic graphicname="A225337E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09213-58014</s-number>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the 8 bolts, the rear drive plate spacer, the pump impeller drive plate and the flywheel and ring gear.</ptxt>
<figure>
<graphic graphicname="A225158" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000126G031X_01_0004" proc-id="RM23G0E___00004E200001">
<ptxt>REMOVE REAR CRANKSHAFT OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a knife, cut off the lip of the rear crankshaft oil seal.</ptxt>
<figure>
<graphic graphicname="A225345E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cut Position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a screwdriver, pry out the rear crankshaft oil seal.</ptxt>
<atten3>
<ptxt>Be careful not to damage the crankshaft.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>