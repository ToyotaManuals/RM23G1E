<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7B8TZ_T003N" variety="T003N">
<name>1GR-FE EMISSION CONTROL</name>
<para id="RM00000322C00IX" category="F" type-id="30027" name-id="SS3TP-05" from="201207">
<name>SERVICE DATA</name>
<subpara id="RM00000322C00IX_z0" proc-id="RM23G0E___000009L00000">
<content5 releasenbr="1">
<spec>
<title>Air Pump (Heater)</title>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="4" valign="middle" align="center">
<ptxt>1 (APH+) - 3 (APHG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-20°C (-4°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.6 to 17.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>-10°C (14°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.4 to 16.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>0°C (32°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.1 to 15.4 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.8 to 14.3 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2.7 to 13.2 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Emission control Valve Set</title>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>No. 2 Emission control Valve Set</title>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Purge VSV</title>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>26 to 30 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>