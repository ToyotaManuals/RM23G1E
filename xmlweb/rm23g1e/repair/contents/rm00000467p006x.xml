<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9D9_T00MX" variety="T00MX">
<name>FRONT SEATBACK HEATER (for Manual Seat)</name>
<para id="RM00000467P006X" category="A" type-id="30014" name-id="SE8LZ-02" from="201210">
<name>INSTALLATION</name>
<subpara id="RM00000467P006X_02" type-id="11" category="10" proc-id="RM23G0E___0000GR800001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000467P006X_01" type-id="01" category="01">
<s-1 id="RM00000467P006X_01_0001" proc-id="RM23G0E___0000GBW00001">
<ptxt>INSTALL FRONT SEATBACK HEATER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239208E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Set the seatback heater with the name stamp side facing the seatback cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Heater</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Name Stamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the seatback heater with new tack pins.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000467P006X_01_0002" proc-id="RM23G0E___0000GR600001">
<ptxt>INSTALL SEPARATE TYPE FRONT SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using hog ring pliers, install the seat cushion cover to the seat cushion pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>The number of hog rings differs according to seat type.</ptxt>
</atten4>
<figure>
<graphic graphicname="B242231E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the 2 headrest supports.</ptxt>
</s2>
<s2>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<figure>
<graphic graphicname="B239202" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Connect the seatback cover bracket to the seatback pad.</ptxt>
</s3>
<s3>
<ptxt>Install the seatback cover bracket to the seat frame with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>After the seatback trim cover is assembled, make sure the side airbag strap is not twisted.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Close the 2 fasteners, and then close the seatback cover.</ptxt>
<figure>
<graphic graphicname="B242233E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Cloth Seat</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Leather Seat</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="B239193E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Using hog ring pliers, install 3 new hog rings.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Connect the seat heater connector and attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Install the rubber band to the seat cushion spring.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000467P006X_01_0003" proc-id="RM23G0E___0000GR700001">
<ptxt>INSTALL FRONT SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front seat assembly (See page <xref label="Seep01" href="RM00000465W00KX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>