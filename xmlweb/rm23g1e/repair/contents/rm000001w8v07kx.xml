<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A2_T00JQ" variety="T00JQ">
<name>PARKING ASSIST MONITOR SYSTEM</name>
<para id="RM000001W8V07KX" category="J" type-id="303DF" name-id="PM57S-01" from="201207" to="201210">
<dtccode/>
<dtcname>Reverse Signal Circuit</dtcname>
<subpara id="RM000001W8V07KX_01" type-id="60" category="03" proc-id="RM23G0E___0000CU500000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The parking assist ECU receives a reverse shift position signal from the park/neutral position switch assembly*1 or back-up light switch assembly*2.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for Automatic Transmission</ptxt>
</item>
<item>
<ptxt>*2: for Manual Transmission</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000001W8V07KX_02" type-id="32" category="03" proc-id="RM23G0E___0000CU600000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E195952E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001W8V07KX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001W8V07KX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001W8V07KX_04_0001" proc-id="RM23G0E___0000CU700000">
<testtitle>CHECK PARKING ASSIST ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
<figure>
<graphic graphicname="E185668E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON, shift lever in any position except R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Parking Assist ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Automatic Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Manual Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001W8V07KX_04_0007" fin="true">A</down>
<right ref="RM000001W8V07KX_04_0002" fin="false">B</right>
<right ref="RM000001W8V07KX_04_0016" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000001W8V07KX_04_0007">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000035D702UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001W8V07KX_04_0002" proc-id="RM23G0E___0000CU800000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARKING ASSIST ECU - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - C40-1 (RL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001W8V07KX_04_0009" fin="true">OK</down>
<right ref="RM000001W8V07KX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W8V07KX_04_0009">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001W8V07KX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001W8V07KX_04_0016" proc-id="RM23G0E___0000CU900000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARKING ASSIST ECU - BACK-UP LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C41 back-up light switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - C41-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001W8V07KX_04_0017" fin="true">OK</down>
<right ref="RM000001W8V07KX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001W8V07KX_04_0013">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001W8V07KX_04_0017">
<testtitle>REPLACE BACK-UP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000047AR002X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>