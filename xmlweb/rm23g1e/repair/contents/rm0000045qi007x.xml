<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>2TR-FE FUEL</name>
<ttl id="12008_S000F_7B8ZG_T0094" variety="T0094">
<name>FUEL PUMP (for 5 Door)</name>
<para id="RM0000045QI007X" category="A" type-id="30014" name-id="FU6SN-02" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM0000045QI007X_01" type-id="01" category="01">
<s-1 id="RM0000045QI007X_01_0001" proc-id="RM23G0E___00005U400000">
<ptxt>INSTALL FUEL SUCTION WITH PUMP AND GAUGE TUBE ASSEMBLY</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Apply a light coat of gasoline or grease to a new gasket and install the gasket to the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Align the protrusion of the fuel suction with pump and gauge tube with the groove of the fuel tank.</ptxt>
<figure>
<graphic graphicname="A220825E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel suction with pump and gauge tube to the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Put a new retainer on the fuel tank. While holding the fuel suction with pump and gauge tube, tighten the retainer one complete turn by hand.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Start Mark (Fuel Tank Side)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Start Mark (Retainer Side)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure the start marks on the retainer and fuel tank are aligned and then tighten the retainer.</ptxt>
<figure>
<graphic graphicname="A220826E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A243483E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Set SST on the retainer.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Hold the fuel suction tube assembly upright by hand to ensure that the fuel suction tube gasket in not moved out of position.</ptxt>
</item>
<item>
<ptxt>Engage the claws of SST securely with the fuel pump gauge retainer holes to secure SST.</ptxt>
</item>
<item>
<ptxt>Install SST while pressing the claws of SST against the fuel pump gauge retainer (toward the center of SST).</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Using SST, tighten the retainer until the mark on the retainer is within range A on the fuel tank as shown in the illustration.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A243481E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tank Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer Side Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Fit the tips of SST onto the ribs of the retainer.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045QI007X_01_0002" proc-id="RM23G0E___00005X400000">
<ptxt>INSTALL FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel tank sub-assembly (See page <xref label="Seep01" href="RM0000045EX00QX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>