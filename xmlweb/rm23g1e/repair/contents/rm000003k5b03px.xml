<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B990_T00IO" variety="T00IO">
<name>REAR SEAT ENTERTAINMENT SYSTEM</name>
<para id="RM000003K5B03PX" category="C" type-id="803M6" name-id="AV6VF-04" from="201207" to="201210">
<dtccode>B15B0</dtccode>
<dtcname>Display Screen Malfunction</dtcname>
<subpara id="RM000003K5B03PX_04" type-id="60" category="03" proc-id="RM23G0E___0000BRT00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when a malfunction occurs in the television display assembly.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B15B0</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Picture circuit (TFT unit) malfunction.</ptxt>
</item>
<item>
<ptxt>Back light malfunction (excess current).</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Television display assembly</ptxt>
</item>
<item>
<ptxt>Display and navigation module display</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003K5B03PX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003K5B03PX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003K5B03PX_03_0001" proc-id="RM23G0E___0000BRR00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000003XO0020X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003K5B03PX_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003K5B03PX_03_0002" proc-id="RM23G0E___0000BRS00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000003XO0020X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC B15B0 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC B15B0 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This DTC is output from the display and navigation module display or television display assembly.</ptxt>
</item>
<item>
<ptxt>When the DTC is checked using the intelligent tester, whether the DTC is output from the display and navigation module display or television display assembly cannot be distinguished. For this reason, recheck the DTC using the "System Check Mode" screen.</ptxt>
</item>
</list1>
</atten4>
<test2>
<ptxt>Enter diagnostic mode and display the "Service Menu" screen (See page <xref label="Seep02" href="RM000003XO0020X"/>).</ptxt>
</test2>
<test2>
<ptxt>Select "Failure Diagnosis Service Check" on the "Service Menu" screen to display the "Failure Diagnosis Service Check" screen.</ptxt>
</test2>
<test2>
<ptxt>Select "Service Check Mode" on the "Failure Diagnosis Service Check" screen and perform the service check.</ptxt>
</test2>
<test2>
<ptxt>Recheck for DTCs and proceed to the next step according to the table below.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.68in"/>
<colspec colname="COL2" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC is output from display and navigation module display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC is output from television display assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000003K5B03PX_03_0004" fin="true">A</down>
<right ref="RM000003K5B03PX_03_0003" fin="true">B</right>
<right ref="RM000003K5B03PX_03_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003K5B03PX_03_0003">
<testtitle>REPLACE DISPLAY AND NAVIGATION MODULE DISPLAY<xref label="Seep01" href="RM000003B6H01LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003K5B03PX_03_0005">
<testtitle>REPLACE TELEVISION DISPLAY ASSEMBLY<xref label="Seep01" href="RM000002BC8019X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003K5B03PX_03_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>