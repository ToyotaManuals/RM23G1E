<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM00000368W06QX" category="C" type-id="803OJ" name-id="BCDP9-02" from="201210">
<dtccode>C1417</dtccode>
<dtcname>High Power Supply Voltage Malfunction</dtcname>
<subpara id="RM00000368W06QX_01" type-id="60" category="03" proc-id="RM23G0E___0000ADQ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If a malfunction is detected in the power supply circuit, the skid control ECU (housed in the master cylinder solenoid) stores this DTC and the fail-safe function prohibits ABS/VSC/TRC operation.</ptxt>
<ptxt>This DTC is stored when the IG1 terminal voltage deviates from the DTC detection condition due to a malfunction in the power supply or charging circuit such as the battery or generator circuit, etc.</ptxt>
<ptxt>This DTC is cleared when the IG1 terminal voltage returns to normal.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.92in"/>
<colspec colname="COL2" colwidth="3.09in"/>
<colspec colname="COL3" colwidth="3.07in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1417</ptxt>
</entry>
<entry valign="middle">
<ptxt>The IG1 terminal voltage is 17.4 V or higher for 0.8 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Charging system</ptxt>
</item>
<item>
<ptxt>Power source circuit</ptxt>
</item>
<item>
<ptxt>Internal power supply circuit of the skid control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM00000368W06QX_02" type-id="32" category="03" proc-id="RM23G0E___0000ADR00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC C1241 (See page <xref label="Seep01" href="RM000001DXW02EX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000368W06QX_03" type-id="51" category="05" proc-id="RM23G0E___0000ADS00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the master cylinder solenoid, perform calibration (See page <xref label="Seep01" href="RM00000452J00KX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000368W06QX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000368W06QX_04_0009" proc-id="RM23G0E___0000ADT00001">
<testtitle>CHECK TERMINAL VOLTAGE (IG1)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A7 skid control ECU connector. </ptxt>
</test1>
<figure>
<graphic graphicname="C214161E28" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A7-46 (IG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.49in"/>
<colspec colname="COL2" colwidth="1.64in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1KD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000368W06QX_04_0013" fin="false">A</down>
<right ref="RM00000368W06QX_04_0007" fin="true">B</right>
<right ref="RM00000368W06QX_04_0012" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000368W06QX_04_0013" proc-id="RM23G0E___0000ADU00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00LX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00LX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.49in"/>
<colspec colname="COL2" colwidth="1.64in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000368W06QX_04_0014" fin="true">A</down>
<right ref="RM00000368W06QX_04_0010" fin="true">B</right>
<right ref="RM00000368W06QX_04_0011" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000368W06QX_04_0010">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01NX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W06QX_04_0007">
<testtitle>GO TO CHARGING SYSTEM (ON-VEHICLE INSPECTION)<xref label="Seep01" href="RM000001AR909RX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W06QX_04_0011">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01OX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W06QX_04_0012">
<testtitle>GO TO CHARGING SYSTEM (ON-VEHICLE INSPECTION)<xref label="Seep01" href="RM0000013Y001UX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000368W06QX_04_0014">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>