<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000187G04ZX" category="C" type-id="302B6" name-id="ESVK8-02" from="201207" to="201210">
<dtccode>P0340</dtccode>
<dtcname>Camshaft Position Sensor "A" Circuit (Bank 1 or Single Sensor)</dtcname>
<subpara id="RM00000187G04ZX_01" type-id="60" category="03" proc-id="RM23G0E___00001FR00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The camshaft position sensor (G signal) consists of a magnet, iron core and pickup coil.</ptxt>
<ptxt>The G signal plate (timing pulley) has one tooth on its outer circumference and is installed on the camshaft. When the camshaft rotates, the protrusion on the signal plate and the air gap on the pickup coil change, causing fluctuations in the magnetic field that generate a voltage in the pickup coil.</ptxt>
<ptxt>The NE signal plate has 34 teeth and is mounted on the crankshaft angle sensor plate. The NE signal sensor generates 34 signals for every revolution. The ECM detects the camshaft angle based on the G signal, and detects the crankshaft angle based on the NE signal.</ptxt>
<table pgwide="1">
<title>P0340</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Crank engine for 4 seconds (STA on)</ptxt>
</entry>
<entry valign="middle">
<ptxt>STA on:</ptxt>
<ptxt>No camshaft position sensor signal is sent to the ECM while cranking for 4 seconds or more (2 trip detection logic)*1 (1 trip detection logic)*2.</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in camshaft position sensor circuit</ptxt>
</item>
<item>
<ptxt>Camshaft position sensor</ptxt>
</item>
<item>
<ptxt>Camshaft timing pulley</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Idle engine for 1 second (STA off)</ptxt>
</entry>
<entry valign="middle">
<ptxt>STA off:</ptxt>
<ptxt>Either condition is met for 1 second (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>G signal is not input with an engine speed of 650 rpm or more.</ptxt>
</item>
<item>
<ptxt>No camshaft position sensor signal is sent to the ECM with an engine speed of 650 to 3000 rpm 20 times or more.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: Only for vehicle Euro-OBD.</ptxt>
</item>
<item>
<ptxt>*2: Only for vehicle except Euro-OBD.</ptxt>
</item>
</list1>
<atten4>
<list1 type="nonmark">
<item>
<ptxt>If DTC P0340 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Difficulty starting</ptxt>
</item>
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
<item>
<ptxt>Lack of power</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187G04ZX_02" type-id="32" category="03" proc-id="RM23G0E___00001FS00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0335 (See page <xref label="Seep01" href="RM00000187F04YX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000187G04ZX_03" type-id="51" category="05" proc-id="RM23G0E___00001FT00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK061X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187G04ZX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187G04ZX_04_0001" proc-id="RM23G0E___00001FU00000">
<testtitle>INSPECT CAMSHAFT POSITION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft position sensor (See page <xref label="Seep01" href="RM0000013ZH01CX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187G04ZX_04_0002" fin="false">OK</down>
<right ref="RM00000187G04ZX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0002" proc-id="RM23G0E___00001FV00000">
<testtitle>CHECK HARNESS AND CONNECTOR (CAMSHAFT POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the camshaft position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C52-1 - C92-13 (G+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C52-2 - C92-14 (G-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C52-1 or C92-13 (G+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C52-2 or C92-14 (G-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the camshaft position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187G04ZX_04_0003" fin="false">OK</down>
<right ref="RM00000187G04ZX_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0003" proc-id="RM23G0E___00001FW00000">
<testtitle>CHECK CAMSHAFT POSITION SENSOR (SENSOR INSTALLATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the sensor installation.</ptxt>
<figure>
<graphic graphicname="A208464E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor is installed correctly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187G04ZX_04_0004" fin="false">OK</down>
<right ref="RM00000187G04ZX_04_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0004" proc-id="RM23G0E___00001FX00000">
<testtitle>CHECK CAMSHAFT TIMING PULLEY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the condition of the camshaft timing pulley.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Timing pulley does not have any cracks or deformation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000187G04ZX_04_0005" fin="false">OK</down>
<right ref="RM00000187G04ZX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0005" proc-id="RM23G0E___00001FY00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187G04ZX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0006" proc-id="RM23G0E___00001FZ00000">
<testtitle>REPLACE CAMSHAFT POSITION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the camshaft position sensor (See page <xref label="Seep01" href="RM0000013ZJ01FX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187G04ZX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0007" proc-id="RM23G0E___00001G000000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187G04ZX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0008" proc-id="RM23G0E___00001G100000">
<testtitle>SECURELY REINSTALL SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Securely reinstall the sensor (See page <xref label="Seep01" href="RM0000013ZG01HX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187G04ZX_04_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0009" proc-id="RM23G0E___00001G200000">
<testtitle>REPLACE CAMSHAFT TIMING PULLEY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the camshaft timing pulley (See page <xref label="Seep01" href="RM0000045CH007X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187G04ZX_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0010" proc-id="RM23G0E___00001G300000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z6X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and idle it for 4 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187G04ZX_04_0011" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187G04ZX_04_0011">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>