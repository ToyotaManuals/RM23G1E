<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A5_T00JT" variety="T00JT">
<name>REAR VIEW MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM000003Y8P00RX" category="C" type-id="803MT" name-id="PM2WQ-08" from="201210">
<dtccode>C168B</dtccode>
<dtcname>AVC-LAN Communication Malfunction</dtcname>
<dtccode>C168C</dtccode>
<dtcname>AVC-LAN Command Time Out</dtcname>
<subpara id="RM000003Y8P00RX_01" type-id="60" category="03" proc-id="RM23G0E___0000CZI00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored if the parking assist ECU judges that there is a problem with the AVC-LAN communication system as a result of self check.</ptxt>
<atten4>
<ptxt>This DTC may be stored due to the battery condition or changes in the power supply waveform when starting the engine, even though there is no problem with the AVC-LAN communication system.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.00in"/>
<colspec colname="COL2" colwidth="3.00in"/>
<colspec colname="COL3" colwidth="3.08in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C168B</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>AVC-LAN communication malfunction: No master</ptxt>
</item>
<item>
<ptxt>AVC-LAN communication malfunction: Master reset</ptxt>
</item>
<item>
<ptxt>AVC-LAN communication malfunction: Master malfunction</ptxt>
</item>
<item>
<ptxt>AVC-LAN communication malfunction: Abnormal connection check</ptxt>
</item>
<item>
<ptxt>AVC-LAN communication malfunction: Transmission not completed</ptxt>
</item>
<item>
<ptxt>AVC-LAN communication malfunction: Abnormal transmission completion notification</ptxt>
</item>
<item>
<ptxt>AVC-LAN communication malfunction: Registration request transmission</ptxt>
</item>
<item>
<ptxt>AVC-LAN communication malfunction: Abnormal ON/OFF parameter</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle">
<ptxt>AVC-LAN Communication</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C168C</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>AVC-LAN command time out: Output ON request notification command time out</ptxt>
</item>
<item>
<ptxt>AVC-LAN command time out: Output OFF request notification command time out</ptxt>
</item>
<item>
<ptxt>AVC-LAN command time out: Voice production start request notification command time out</ptxt>
</item>
<item>
<ptxt>AVC-LAN command time out: Voice production end request notification command time out</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003Y8P00RX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003Y8P00RX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003Y8P00RX_03_0001" proc-id="RM23G0E___0000CZJ00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000003Y8H00OX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000003Y8H00OX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC C168B or C168C is output (w/ Navigation System [for DVD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>DTC C168B or C168C is output (w/ Navigation System [for HDD])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This DTC may be stored due to the battery condition or changes in the power supply waveform when starting the engine, even though there is no problem with the AVC-LAN communication system.</ptxt>
</item>
<item>
<ptxt>If DTC C168B or C168C is output frequently, duplicate the problem symptoms and perform the inspection again, even though the DTC is not output when rechecking for DTCs (See page <xref label="Seep03" href="RM0000014000BEX"/>).</ptxt>
</item>
</list1>
</atten4>
</content6>
<res>
<down ref="RM000003Y8P00RX_03_0003" fin="true">A</down>
<right ref="RM000003Y8P00RX_03_0002" fin="true">B</right>
<right ref="RM000003Y8P00RX_03_0004" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003Y8P00RX_03_0003">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003Y8P00RX_03_0002">
<testtitle>GO TO NAVIGATION SYSTEM<xref label="Seep01" href="RM0000043TC02AX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003Y8P00RX_03_0004">
<testtitle>GO TO NAVIGATION SYSTEM<xref label="Seep01" href="RM0000043TC02BX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>