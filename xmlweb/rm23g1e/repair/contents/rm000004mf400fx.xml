<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8Z5_T008T" variety="T008T">
<name>SUCTION CONTROL VALVE</name>
<para id="RM000004MF400FX" category="A" type-id="30014" name-id="FU8CK-01" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM000004MF400FX_02" type-id="11" category="10" proc-id="RM23G0E___00005S300000">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipes and fuel inlet pipe, clean them with a brush and compressed air.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM000004MF400FX_01" type-id="01" category="01">
<s-1 id="RM000004MF400FX_01_0001" proc-id="RM23G0E___00005RX00000">
<ptxt>INSTALL SUCTION CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before replacing the suction control valve, be sure to clean the surrounding area.</ptxt>
</item>
<item>
<ptxt>When replacing the suction control valve, make sure that your hands are clean and do not use gloves, etc.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Apply engine oil to a new O-ring.</ptxt>
<atten3>
<ptxt>Be sure to use clean engine oil.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the O-ring to the O-ring groove of the fuel supply pump.</ptxt>
<figure>
<graphic graphicname="A228327" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure that the O-ring and O-ring groove are free of foreign matter and are not damaged.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install guide pins used for the insertion of the suction control valve to the bolt holes.</ptxt>
<figure>
<graphic graphicname="A228328E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Guide Pin</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When installing the guide pins, it is sufficient to fix them lightly in place by hand.</ptxt>
</item>
<item>
<ptxt>The guide pins are used to make sure that the suction control valve remains perpendicular to the fuel supply pump when it is inserted.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Apply engine oil to the O-ring at the end of the suction control valve.</ptxt>
<figure>
<graphic graphicname="A228438" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be sure to use clean engine oil.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>While making sure that the suction control valve and fuel supply pump remain perpendicular to each other, slide the suction control valve along the guide pins and insert it into the fuel supply pump as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A228332E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Guide Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Fuel Supply Pump</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the contact surfaces of the fuel supply pump and suction control valve are free of foreign matter and are not damaged.</ptxt>
</item>
<item>
<ptxt>Do not insert the suction control valve at an angle.</ptxt>
</item>
<item>
<ptxt>Make sure that the O-ring does not get pinched between the parts. If it does, replace it with a new one.</ptxt>
</item>
<item>
<ptxt>Insert the suction control valve until it contacts the fuel supply pump.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>While holding the suction control valve in place, remove the guide pins, temporarily install 2 new bolts and uniformly tighten them by hand.</ptxt>
<atten3>
<ptxt>Do not use tools. Tighten the bolts by hand until the surfaces of the suction control valve and fuel supply pump contact each other.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a 5 mm hexagon wrench, uniformly tighten the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector to the suction control valve.</ptxt>
<atten3>
<ptxt>Make sure that there is not excessive slack or tension in the wire harness of the suction control valve.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000004MF400FX_01_0002" proc-id="RM23G0E___00005RY00000">
<ptxt>INSTALL FUEL SUPPLY PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM0000045CE008X"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM0000045CE007X"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000004MF400FX_01_0008" proc-id="RM23G0E___00005S200000">
<ptxt>ADD ENGINE COOLANT</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM000002SLF00XX_01_0002"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM000002SLF00WX_01_0002"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000004MF400FX_01_0007" proc-id="RM23G0E___00005S100000">
<ptxt>BLEED AIR FROM FUEL SYSTEM</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM000002SY802NX_01_0002"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000004MF400FX_01_0004" proc-id="RM23G0E___00005RZ00000">
<ptxt>PERFORM FUEL SUPPLY PUMP INITIALIZATION</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM000000TIN05LX"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM000000TIN05KX"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM000004MF400FX_01_0010" proc-id="RM23G0E___00001CD00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Before each inspection, turn the A/C switch off.</ptxt>
</atten3>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leaks. If no external leaks are found, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004MF400FX_01_0006" proc-id="RM23G0E___00005S000000">
<ptxt>INSPECT FOR FUEL LEAK</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM000002SY802OX_01_0001"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM000002SY802NX_01_0001"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>