<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001P" variety="S001P">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001P_7B99L_T00J9" variety="T00J9">
<name>NAVIGATION SYSTEM (for DVD)</name>
<para id="RM0000043X902XX" category="D" type-id="303FF" name-id="NS4RA-07" from="201210">
<name>OPERATION CHECK</name>
<subpara id="RM0000043X902XX_z0" proc-id="RM23G0E___0000CEW00001">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK SYSTEM NORMAL CONDITION</ptxt>
<step2>
<ptxt>If the cause of a symptom is any of the following, the corresponding symptom is normal; it is not a malfunction.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Answer</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Longer route than expected is chosen</ptxt>
</entry>
<entry valign="middle">
<ptxt>Depending on road conditions, display and navigation module display may determine that longer route is quicker</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Even when distance priority is high, shortest route is not shown</ptxt>
</entry>
<entry valign="middle">
<ptxt>Some routes may not be advised due to safety concerns</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>When vehicle is put into motion immediately after engine starts, navigation system deviates from actual position</ptxt>
</entry>
<entry valign="middle">
<ptxt>If vehicle starts before navigation system activates, system may not react</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>When running on certain types of roads, especially new roads, vehicle position deviates from actual position</ptxt>
</entry>
<entry valign="middle">
<ptxt>When vehicle is driving on new roads not available on map disc, system attempts to match vehicle position to another nearby road, causing position mark to deviate</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>The following symptoms are not malfunctions, but are caused by errors inherent in the GPS, gyro sensor, speed sensor or display and navigation module display.</ptxt>
<figure>
<graphic graphicname="I100066" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>The current position mark may be displayed on a nearby parallel road.</ptxt>
</step3>
<step3>
<ptxt>Immediately after a fork in the road, the current vehicle position mark may be displayed on the wrong road.</ptxt>
<figure>
<graphic graphicname="I100067" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>When the vehicle turns right or left at an intersection, the current vehicle position mark may be displayed on a nearby parallel road.</ptxt>
<figure>
<graphic graphicname="I100068" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>When the vehicle is carried, such as on a ferry, and the vehicle itself is not being driven, the current vehicle position mark may be displayed in the position where the vehicle was until a measurement can be performed using GPS.</ptxt>
<figure>
<graphic graphicname="I100069" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>When the vehicle travels on a steep hill, the current vehicle position mark may deviate from the correct position.</ptxt>
<figure>
<graphic graphicname="I100070" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>When the vehicle makes a continuous turn (e.g. 360, 720 or 1080 degrees), the current vehicle position mark may deviate from the correct position.</ptxt>
<figure>
<graphic graphicname="I100071" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>When the vehicle moves erratically, such as when making constant lane changes, the current vehicle position mark may deviate from the correct position.</ptxt>
<figure>
<graphic graphicname="I100072" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>When the ignition switch is turned to ACC or ON on a turntable before parking, the current vehicle position mark may not indicate the correct direction. The same will occur when the vehicle comes out of the parking garage.</ptxt>
<figure>
<graphic graphicname="I038146E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>When the vehicle travels on a snowy road or a mountain path with tire chains installed or using a spare tire, the current vehicle position mark may deviate from the correct position.</ptxt>
<figure>
<graphic graphicname="I100074" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>When the tires are changed, the current vehicle position mark may deviate from the correct position.</ptxt>
<figure>
<graphic graphicname="I100075" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A change in tire diameter may cause a speed sensor error.</ptxt>
</item>
<item>
<ptxt>Performing "tire change" in calibration mode will allow the system to correct the current vehicle position faster.</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK PANEL &amp; STEERING SWITCH</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The display and navigation module display panel switches, radio receiver assembly panel switches and steering switches are checked in the following procedure.</ptxt>
</item>
<item>
<ptxt>Illustrations may differ from the actual vehicle screen depending on the device settings and options. Therefore, some detailed areas may not be exactly the same as on the actual vehicle screen.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Enter diagnostic mode (See page <xref label="Seep01" href="RM0000044WO01MX"/>).</ptxt>
</step2>
<step2>
<ptxt>Select "Function Check/Setting" from the "Service Menu" screen.</ptxt>
<figure>
<graphic graphicname="E176512" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Select "Panel &amp; Steering Switch" from the "Function Check/Setting" screen.</ptxt>
<figure>
<graphic graphicname="E195550" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Panel &amp; Steering Switch Check Mode</ptxt>
<step3>
<ptxt>Operate each switch and check that the switch names and conditions are correctly displayed.</ptxt>
<figure>
<graphic graphicname="E176522E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Screen Description</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1: Push switch name</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Name of pressed switch is displayed</ptxt>
</item>
<item>
<ptxt>If more than one switch is pressed, "MULTIPLE" is displayed</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2: Rotary switch name</ptxt>
</entry>
<entry valign="middle">
<ptxt>Name of rotary switch is displayed</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3: Rotary switch direction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Direction of rotary switch is displayed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK TOUCH SWITCH</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The touch switches on the screen are checked in the following procedure.</ptxt>
</item>
<item>
<ptxt>Illustrations may differ from the actual vehicle screen depending on the device settings and options. Therefore, some detailed areas may not be exactly the same as on the actual vehicle screen.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Enter diagnostic mode (See page <xref label="Seep02" href="RM0000044WO01MX"/>).</ptxt>
</step2>
<step2>
<ptxt>Select "Function Check/Setting" from the "Service Menu" screen.</ptxt>
<figure>
<graphic graphicname="E176512" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Select "Touch Switch" from the "Function Check/Setting" screen.</ptxt>
<figure>
<graphic graphicname="E195551" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Touch Switch Check</ptxt>
<figure>
<graphic graphicname="E176521" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Touch the display anywhere in the open area to perform the check when the "Touch Switch Check" screen is displayed.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A "+" mark is displayed where the display is touched.</ptxt>
</item>
<item>
<ptxt>The "+" mark remains on the display even after the finger is removed.</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK MIC &amp; VOICE RECOGNITION</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The microphone and microphone input level are checked in the following procedure.</ptxt>
</item>
<item>
<ptxt>Illustrations may differ from the actual vehicle screen depending on the device settings and options. Therefore, some detailed areas may not be exactly the same as on the actual vehicle screen.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Enter diagnostic mode (See page <xref label="Seep10" href="RM0000044WO01MX"/>).</ptxt>
</step2>
<step2>
<ptxt>Select "Function Check/Setting" from the "Service Menu" screen.</ptxt>
<figure>
<graphic graphicname="E176512" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Select "Mic &amp; Voice Recognition" from the "Function Check/Setting" screen.</ptxt>
<figure>
<graphic graphicname="E195552" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Microphone &amp; Voice Recognition Check</ptxt>
<step3>
<ptxt>When speaking into the microphone, check that the microphone input level meter changes according to the input level.</ptxt>
<figure>
<graphic graphicname="E187218E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>Push the recording switch and perform voice recording.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Select the recording switch with the blower motor of the air conditioning system stopped. If an outlet of the air conditioning system is facing the microphone, noise may be recorded.</ptxt>
</item>
<item>
<ptxt>Voice can be recorded for up to 5 seconds.</ptxt>
</item>
</list1>
</atten4>
</step3>
<step3>
<ptxt>Check that the recording indicator remains on while recording and that the recording can be played normally.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Microphone Input Level Meter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Recording Switch</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop Switch</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Play Switch</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Recording Indicator</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>For details of this function, refer to Diagnosis Display Detailed Description (See page <xref label="Seep11" href="RM0000043X8019X"/>).</ptxt>
</item>
<item>
<ptxt>This function is controlled by the display and navigation module display (built-in navigation ECU).</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK COLOR BAR</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The display color on the screen is checked in the following procedure.</ptxt>
</item>
<item>
<ptxt>Illustrations may differ from the actual vehicle screen depending on the device settings and options. Therefore, some detailed areas may not be exactly the same as on the actual vehicle screen.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Enter diagnostic mode (See page <xref label="Seep03" href="RM0000044WO01MX"/>).</ptxt>
</step2>
<step2>
<ptxt>Select "Function Check/Setting" from the "Service Menu" screen.</ptxt>
<figure>
<graphic graphicname="E176512" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Select "Color Bar" from the "Function Check/Setting" screen.</ptxt>
<figure>
<graphic graphicname="E195553" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Color Bar Check Mode</ptxt>
<step3>
<ptxt>Select a color bar from the "Color Bar Check Mode" screen.</ptxt>
<figure>
<graphic graphicname="E176520" width="2.775699831in" height="3.779676365in"/>
</figure>
</step3>
<step3>
<ptxt>Check the display color.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The entire screen turns to the color or stripe selected.</ptxt>
</item>
<item>
<ptxt>Touching the display returns to the "Color Bar Check" screen.</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK GPS &amp; VEHICLE SENSORS</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>GPS information, vehicle signals and sensor signals are checked in the following procedure.</ptxt>
</item>
<item>
<ptxt>Illustrations may differ from the actual vehicle screen depending on the device settings and options. Therefore, some detailed areas may not be exactly the same as on the actual vehicle screen.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Enter diagnostic mode (See page <xref label="Seep04" href="RM0000044WO01MX"/>).</ptxt>
</step2>
<step2>
<ptxt>Select "Function Check/Setting" from the "Service Menu" screen.</ptxt>
<figure>
<graphic graphicname="E176512" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Select "GPS &amp; Vehicle Sensors" from the "Function Check/Setting" screen.</ptxt>
<figure>
<graphic graphicname="E195554" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>GPS information</ptxt>
<step3>
<ptxt>When GPS information is displayed, check the GPS conditions.</ptxt>
<figure>
<graphic graphicname="E176561" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The signals from the vehicle sensors are updated once per second and if any have changed, the screen is updated.</ptxt>
</item>
<item>
<ptxt>For details of this function, refer to Diagnosis Display Detailed Description in System Description (See page <xref label="Seep05" href="RM0000043X8019X"/>).</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
<step2>
<ptxt>Select "Sensors Check" from the "GPS Information" screen.</ptxt>
</step2>
<step2>
<ptxt>Vehicle Sensors</ptxt>
<step3>
<ptxt>Check all the signals and sensors when vehicle signal information is displayed.</ptxt>
<figure>
<graphic graphicname="E176525" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The signals from the vehicle sensors are updated once per second and if any have changed, the screen is updated.</ptxt>
</item>
<item>
<ptxt>This screen displays vehicle signals input to the display and navigation module display (built-in navigation ECU).</ptxt>
</item>
<item>
<ptxt>For details of this function, refer to Diagnosis Display Detailed Description in System Description (See page <xref label="Seep06" href="RM0000043X8019X"/>).</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK VEHICLE SIGNAL</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Vehicle signals received by the display and navigation module display are checked in the following procedure.</ptxt>
</item>
<item>
<ptxt>Illustrations may differ from the actual vehicle screen depending on the device settings and options. Therefore, some detailed areas may not be exactly the same as on the actual vehicle screen.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Enter diagnostic mode (See page <xref label="Seep07" href="RM0000044WO01MX"/>).</ptxt>
</step2>
<step2>
<ptxt>Select "Function Check/Setting" from the "Service Menu" screen.</ptxt>
<figure>
<graphic graphicname="E176512" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Select "Vehicle Signal" from the "Function Check/Setting" screen.</ptxt>
<figure>
<graphic graphicname="E195555" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Vehicle Signal Check Mode</ptxt>
<step3>
<ptxt>When the "Vehicle Signal Check Mode" screen is displayed, check all the vehicle signal conditions.</ptxt>
<figure>
<graphic graphicname="E176523" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Only conditions having inputs are displayed.</ptxt>
</item>
<item>
<ptxt>This screen displays vehicle signals input to the display and navigation module display.</ptxt>
</item>
<item>
<ptxt>For details of this function, refer to Diagnosis Display Detailed Description in System Description (See page <xref label="Seep08" href="RM0000043X8019X"/>).</ptxt>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK HANDS-FREE VOLUME SETTING</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The hands-free volume of a "Bluetooth" compatible phone can be adjusted using the following procedure.</ptxt>
</item>
<item>
<ptxt>Illustrations may differ from the actual vehicle screen depending on the device settings and options. Therefore, some detailed areas may not be exactly the same as on the actual vehicle screen.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Enter diagnostic mode (See page <xref label="Seep09" href="RM0000044WO01MX"/>).</ptxt>
</step2>
<step2>
<ptxt>Select "Function Check/Setting" from the "Service Menu" screen.</ptxt>
<figure>
<graphic graphicname="E176512" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Select "Hands-free Volume Setting" from the "Function Check/Setting" screen.</ptxt>
<figure>
<graphic graphicname="E195556" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Hands-free Volume Setting</ptxt>
<step3>
<ptxt>Check the hands-free volume level.</ptxt>
<figure>
<graphic graphicname="E235759E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Screen Description</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a: Receive voice level adjustment</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Setting possible for voice level received from "Bluetooth" compatible phones</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b: Receive voice level adjustment</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Setting possible for the voice level sent from "Bluetooth" compatible phones.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Sound quality may deteriorate when the receive voice level is changed more than necessary. For this reason, check that the received voice quality is still acceptable after changing this setting. </ptxt>
</atten3>
<atten4>
<ptxt>Sound quality of sent voice may deteriorate when the send voice level is adjusted.</ptxt>
</atten4>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>