<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97M_T00HA" variety="T00HA">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Vacuum Brake Booster)</name>
<para id="RM000000XIH0EPX" category="J" type-id="300E0" name-id="BCB9L-02" from="201207" to="201210">
<dtccode/>
<dtcname>ABS Warning Light Remains ON</dtcname>
<subpara id="RM000000XIH0EPX_01" type-id="60" category="03" proc-id="RM23G0E___0000AI100000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU is connected to the combination meter via CAN communication.</ptxt>
<ptxt>If any of the following is detected, the ABS warning light remains on:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The skid control ECU connector is disconnected from the skid control ECU.</ptxt>
</item>
<item>
<ptxt>There is a malfunction in the skid control ECU internal circuit.</ptxt>
</item>
<item>
<ptxt>There is an open in the harness between the combination meter and the skid control ECU.</ptxt>
</item>
<item>
<ptxt>The anti-lock brake system is defective.</ptxt>
</item>
<item>
<ptxt>The voltage at terminal IG1 is high.</ptxt>
</item>
</list1>
<atten4>
<ptxt>In some cases, the intelligent tester cannot be used when the skid control ECU is abnormal.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000XIH0EPX_02" type-id="32" category="03" proc-id="RM23G0E___0000AI200000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C215178E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XIH0EPX_03" type-id="51" category="05" proc-id="RM23G0E___0000AI300000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08JX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XIH0EPX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIH0EPX_04_0001" proc-id="RM23G0E___0000AI400000">
<testtitle>CHECK CAN COMMUNICATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if CAN communication system DTCs are output (for LHD without Entry and Start System: See page <xref label="Seep01" href="RM000000WIB0BRX"/>, for RHD without Entry and Start System: See page <xref label="Seep02" href="RM000000WIB0BTX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.43in"/>
<colspec colname="COLSPEC0" colwidth="1.64in"/>
<colspec colname="COL2" colwidth="1.06in"/>
<thead>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>DTC not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIH0EPX_04_0003" fin="false">A</down>
<right ref="RM000000XIH0EPX_04_0015" fin="true">B</right>
<right ref="RM000000XIH0EPX_04_0018" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0003" proc-id="RM23G0E___0000AI500000">
<testtitle>CHECK IF SKID CONTROL ECU CONNECTOR IS SECURELY CONNECTED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the skid control ECU connector is securely connected.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connector is securely connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XIH0EPX_04_0022" fin="false">OK</down>
<right ref="RM000000XIH0EPX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0022" proc-id="RM23G0E___0000AI700000">
<testtitle>CHECK TERMINAL VOLTAGE (IG1)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C199357E08" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A6-34 (IG1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XIH0EPX_04_0023" fin="false">OK</down>
<right ref="RM000000XIH0EPX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0023" proc-id="RM23G0E___0000AI800000">
<testtitle>CHECK HARNESS AND CONNECTOR (GND1 TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A6-1 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000000XIH0EPX_04_0007" fin="false">OK</down>
<right ref="RM000000XIH0EPX_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0007" proc-id="RM23G0E___0000AI600000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ABS WARNING LIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
</test1>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="2.26in"/>
<colspec colname="COL3" colwidth="1.60in"/>
<colspec colname="COL4" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS warning light / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<test1>
<ptxt>When performing the ABS Warning Light Active Test, check ABS Warning Light in the Data List (See page <xref label="Seep02" href="RM000000XHW0BXX"/>).</ptxt>
</test1>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ABS warning light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Warning light ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Observe the combination meter.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.69in"/>
<colspec colname="COL2" colwidth="3.91in"/>
<colspec colname="COL3" colwidth="1.48in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Data List Display</ptxt>
</entry>
<entry>
<ptxt>Data List Display when Performing Active Test ON/OFF Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not change between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not change between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000XIH0EPX_04_0008" fin="true">A</down>
<right ref="RM000000XIH0EPX_04_0021" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0015">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO07CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0018">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO07EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0008">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0011">
<testtitle>CONNECT CONNECTOR TO ECU CORRECTLY</testtitle>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0013">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (IG1 CIRCUIT)</testtitle>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (GND1 CIRCUIT)</testtitle>
</testgrp>
<testgrp id="RM000000XIH0EPX_04_0021">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L031X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>