<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002G" variety="S002G">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002G_7B9HT_T00RH" variety="T00RH">
<name>SIDE STEP</name>
<para id="RM0000038LX00KX" category="A" type-id="8000E" name-id="ET4JV-01" from="201207">
<name>REASSEMBLY</name>
<subpara id="RM0000038LX00KX_01" type-id="11" category="10" proc-id="RM23G0E___0000JE700000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0CLX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000038LX00KX_02" type-id="01" category="01">
<s-1 id="RM0000038LX00KX_02_0020" proc-id="RM23G0E___0000J6G00000">
<ptxt>INSTALL STEP LIGHT ASSEMBLY (w/ Illumination)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 5 Door:</ptxt>
<s3>
<ptxt>Install the 2 lights with the 4 bolts.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 3 Door:</ptxt>
<s3>
<ptxt>Install the light with the 2 bolts.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000038LX00KX_02_0009" proc-id="RM23G0E___0000JE800000">
<ptxt>INSTALL STEP LIGHT BRACKET (w/ Illumination)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236314" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install a nose piece to an air riveter or hand riveter.</ptxt>
</s2>
<s2>
<ptxt>Insert the mandrel part of a new rivet into the nose piece.</ptxt>
</s2>
<s2>
<ptxt>Using the riveter, install the step light bracket with the 3 rivets as shown in the illustration.</ptxt>
<atten4>
<ptxt>If the rivet cannot be cut, pull it once and cut it.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<figure>
<graphic graphicname="B171664E15" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Do not pry the rivet with the riveter as this will cause damage to the riveter and mandrel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Riveter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mandrel</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
<item>
<figure>
<graphic graphicname="B172390E14" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Confirm that the rivets are seated properly against the step light bracket.</ptxt>
</item>
<item>
<ptxt>Do not tilt the riveter when installing the rivet to the step light bracket.</ptxt>
</item>
<item>
<ptxt>Do not leave any space between the rivet head and step light bracket.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Riveter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
<item>
<figure>
<graphic graphicname="B172391E13" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Do not leave any space between the step light bracket and step plate. Firmly hold together the 2 items while installing the rivet.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Riveter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0008" proc-id="RM23G0E___0000J6F00000">
<ptxt>INSTALL STEP LIGHT ASSEMBLY (w/ Illumination)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the bulb.</ptxt>
<figure>
<graphic graphicname="E198148" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Turn the socket in the direction indicated by the arrow to install it.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038LX00KX_02_0010" proc-id="RM23G0E___0000J6E00000">
<ptxt>INSTALL STEP PANEL WIRE LH (w/ Illumination)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 5 Door:</ptxt>
<s3>
<ptxt>Attach the 9 clamps to install the wire.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 3 Door:</ptxt>
<s3>
<ptxt>Attach the 5 clamps to install the wire.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000038LX00KX_02_0011" proc-id="RM23G0E___0000JE900000">
<ptxt>INSTALL SIDE STEP BRACKET SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a nose piece to an air riveter or hand riveter.</ptxt>
</s2>
<s2>
<ptxt>Insert the mandrel part of a new rivet into the nose piece.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B236316" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Using the riveter, install the side step bracket with 3 rivets as shown in the illustration.</ptxt>
<atten4>
<ptxt>If the rivet cannot be cut, pull it once and cut it.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<figure>
<graphic graphicname="B171664E15" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Do not pry the rivet with the riveter as this will cause damage to the riveter and mandrel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Riveter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mandrel</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
<item>
<figure>
<graphic graphicname="B172390E14" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Confirm that the rivets are seated properly against the side step bracket.</ptxt>
</item>
<item>
<ptxt>Do not tilt the riveter when installing the rivet to the side step bracket.</ptxt>
</item>
<item>
<ptxt>Do not leave any space between the rivet head and side step bracket.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Riveter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
<item>
<figure>
<graphic graphicname="B172391E13" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Do not leave any space between the side step bracket and step plate. Firmly hold together the 2 items while installing the rivet.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Riveter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0012" proc-id="RM23G0E___0000JEA00000">
<ptxt>INSTALL NO. 2 SIDE STEP PLATE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 side step plate cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0013" proc-id="RM23G0E___0000JEB00000">
<ptxt>INSTALL STEP PLATE COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the side step plate cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0019" proc-id="RM23G0E___0000JEH00000">
<ptxt>INSTALL STEP PLATE LH (for 5 Door)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 17 claws to install the step plate.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and clip.</ptxt>
<torque>
<torqueitem>
<t-value1>2.3</t-value1>
<t-value2>24</t-value2>
<t-value3>20</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0014" proc-id="RM23G0E___0000JEC00000">
<ptxt>INSTALL STEP PLATE LH (for 3 Door)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 14 claws to install the step plate.</ptxt>
</s2>
<s2>
<ptxt>Install the 3 bolts and clip.</ptxt>
<torque>
<torqueitem>
<t-value1>2.3</t-value1>
<t-value2>24</t-value2>
<t-value3>20</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0015" proc-id="RM23G0E___0000JED00000">
<ptxt>INSTALL NO. 2 ROCKER PANEL MOULDING PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 rocker panel moulding protector with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>3.0</t-value1>
<t-value2>31</t-value2>
<t-value3>27</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0016" proc-id="RM23G0E___0000JEE00000">
<ptxt>INSTALL NO. 3 SIDE STEP BRACKET LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 side step bracket with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>6.0</t-value1>
<t-value2>61</t-value2>
<t-value3>53</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0017" proc-id="RM23G0E___0000JEF00000">
<ptxt>INSTALL NO. 2 SIDE STEP BRACKET LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 side step bracket with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>6.0</t-value1>
<t-value2>61</t-value2>
<t-value3>53</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038LX00KX_02_0018" proc-id="RM23G0E___0000JEG00000">
<ptxt>INSTALL SIDE STEP BRACKET LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the side step bracket with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>6.0</t-value1>
<t-value2>61</t-value2>
<t-value3>53</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>