<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9BA_T00KY" variety="T00KY">
<name>ENGINE IMMOBILISER SYSTEM (w/o Entry and Start System)</name>
<para id="RM000001TCL02KX" category="C" type-id="3018W" name-id="TD6D5-01" from="201207" to="201210">
<dtccode>B2799</dtccode>
<dtcname>Engine Immobiliser System Malfunction</dtcname>
<subpara id="RM000001TCL02KX_01" type-id="60" category="03" proc-id="RM23G0E___0000F1N00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when one of the following occurs: 1) the ECM detects errors in its own communications with the transponder key ECU assembly; 2) the ECM detects errors in the communication lines; or 3) the ECU - ECM communication ID between the transponder key ECU assembly and ECM is different and an engine start is attempted. Before troubleshooting for this DTC, make sure no engine immobiliser DTCs are output. If output, troubleshoot the engine immobiliser DTCs first.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2799</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>An error in communication between the ECM and transponder key ECU assembly.</ptxt>
</item>
<item>
<ptxt>An error in the communication lines.</ptxt>
</item>
<item>
<ptxt>The ECU - ECM communication ID is different during communication with the transponder key ECU assembly.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Transponder key ECU assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001TCL02KX_02" type-id="32" category="03" proc-id="RM23G0E___0000F1O00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B235863E22" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="B235863E21" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001TCL02KX_03" type-id="51" category="05" proc-id="RM23G0E___0000F1P00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the transponder key ECU assembly and/or ECM, refer to the Service Bulletin.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001TCL02KX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001TCL02KX_05_0008" proc-id="RM23G0E___0000F1T00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001TCQ024X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0006" proc-id="RM23G0E___0000F1S00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001TCQ024X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2799 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0007" fin="true">OK</down>
<right ref="RM000001TCL02KX_05_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0009" proc-id="RM23G0E___0000F1U00000">
<testtitle>REREGISTER ECU - ECM COMMUNICATION ID</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reregister the ECU - ECM communication ID (Refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0010" proc-id="RM23G0E___0000F1V00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001TCQ024X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0011" proc-id="RM23G0E___0000F1W00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001TCQ024X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2799 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0017" fin="true">OK</down>
<right ref="RM000001TCL02KX_05_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0012" proc-id="RM23G0E___0000F1X00000">
<testtitle>CHECK CONNECTION OF CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the ECM and transponder key ECU assembly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Connectors are properly connected. </ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0001" fin="false">OK</down>
<right ref="RM000001TCL02KX_05_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0001" proc-id="RM23G0E___0000F1Q00000">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSPONDER KEY ECU - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G138*1 or G74*2 ECU connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 5L-E</ptxt>
</item>
<item>
<ptxt>*2: except 5L-E</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Disconnect the G60*1, G56*2, G58*3 or G61*4 ECM connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 2TR-FE</ptxt>
</item>
<item>
<ptxt>*2: for 1GR-FE</ptxt>
</item>
<item>
<ptxt>*3: for 1KD-FTV</ptxt>
</item>
<item>
<ptxt>*4: for 5L-E</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for 2TR-FE</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G74-13 (EFIO) - G60-31 (IMI)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G74-12 (EFII) - G60-32 (IMO)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G74-13 (EFIO) or G60-31 (IMI) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>G74-12 (EFII) or G60-32 (IMO) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1GR-FE</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G74-13 (EFIO) - G56-14 (IMI)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G74-12 (EFII) - G56-20 (IMO)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G74-13 (EFIO) or G56-14 (IMI) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>G74-12 (EFII) or G56-20 (IMO) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1KD-FTV</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G74-13 (EFIO) - G58-28 (IMI)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G74-12 (EFII) - G58-29 (IMO)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G74-13 (EFIO) or G58-28 (IMI) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>G74-12 (EFII) or G58-29 (IMO) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 5L-E</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G138-13 (EFIO) - G61-17 (IMI)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G138-12 (EFII) - G61-6 (IMO)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G138-13 (EFIO) or G61-17 (IMI) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>G138-12 (EFII) or G61-6 (IMO) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0002" fin="false">OK</down>
<right ref="RM000001TCL02KX_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0002" proc-id="RM23G0E___0000F1R00000">
<testtitle>REPLACE TRANSPONDER KEY ECU ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the transponder key ECU assembly with a new one (Refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0025" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0025" proc-id="RM23G0E___0000F2000000">
<testtitle>REREGISTER ECU - ECM COMMUNICATION ID</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reregister the ECU - ECM communication ID (Refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0014" proc-id="RM23G0E___0000F1Y00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001TCQ024X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0015" proc-id="RM23G0E___0000F1Z00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001TCQ024X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2799 is not output.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 2TR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1KD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 5L-E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001TCL02KX_05_0003" fin="true">A</down>
<right ref="RM000001TCL02KX_05_0005" fin="true">B</right>
<right ref="RM000001TCL02KX_05_0027" fin="true">C</right>
<right ref="RM000001TCL02KX_05_0028" fin="true">D</right>
<right ref="RM000001TCL02KX_05_0026" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0003">
<testtitle>END (TRANSPONDER KEY ECU IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMI00EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0016">
<testtitle>CONNECT CONNECTOR CORRECTLY</testtitle>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0017">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0026">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0027">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001TCL02KX_05_0028">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>