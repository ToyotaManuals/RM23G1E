<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0022" variety="S0022">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0022_7B9C6_T00LU" variety="T00LU">
<name>AIRBAG SYSTEM</name>
<para id="RM000002P5G03SX" category="C" type-id="802V1" name-id="RS8NK-08" from="201210">
<dtccode>B1656/38</dtccode>
<dtcname>Seat Belt Buckle Switch LH Circuit Malfunction</dtcname>
<subpara id="RM000002P5G03SX_01" type-id="60" category="03" proc-id="RM23G0E___0000FM900001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The seat belt buckle switch LH circuit consists of the center airbag sensor and front seat inner belt LH.</ptxt>
<ptxt>DTC B1656/38 is stored when a malfunction is detected in the seat belt buckle switch LH circuit.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COLSPEC0" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1656/38</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor detects a line short circuit signal, open circuit signal, short circuit to ground signal or short circuit to B+ signal in the seat belt buckle switch LH circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>A front seat inner belt LH malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>No. 2 floor wire</ptxt>
</item>
<item>
<ptxt>Front seat inner belt assembly LH (Seat belt buckle switch LH)</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002P5G03SX_02" type-id="32" category="03" proc-id="RM23G0E___0000FMA00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C213926E07" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002P5G03SX_03" type-id="51" category="05" proc-id="RM23G0E___0000FMB00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="2">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable from the negative (-) battery terminal while performing repairs, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000002P5G03SX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002P5G03SX_04_0001" proc-id="RM23G0E___0000FMC00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0IZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0IZX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1656 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1656 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G03SX_04_0008" fin="true">OK</down>
<right ref="RM000002P5G03SX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0002" proc-id="RM23G0E___0000FMD00001">
<testtitle>CHECK CONNECTION OF CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the center airbag sensor and front seat inner belt LH.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002P5G03SX_04_0003" fin="false">OK</down>
<right ref="RM000002P5G03SX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0003" proc-id="RM23G0E___0000FME00001">
<testtitle>CHECK CONNECTORS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor and front seat inner belt LH.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors (on the center airbag sensor side and front seat inner belt LH side) are not damaged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The connectors are not deformed or damaged.</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="C215710E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Seat Inner Belt LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002P5G03SX_04_0004" fin="false">OK</down>
<right ref="RM000002P5G03SX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0004" proc-id="RM23G0E___0000FMF00001">
<testtitle>CHECK NO. 2 FLOOR WIRE (CENTER AIRBAG SENSOR - FRONT SEAT INNER BELT LH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
<figure>
<graphic graphicname="C217040E10" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Seat Inner Belt LH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Floor Wire</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Front Seat Inner Belt LH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Center Airbag Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>R10-2 (LBE+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R10-1 (LBE-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>R10-2 (LBE+) - R1-11 (LBE+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R10-1 (LBE-) - R1-12 (LBE-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R10-2 (LBE+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R10-1 (LBE-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002P5G03SX_04_0005" fin="false">OK</down>
<right ref="RM000002P5G03SX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0005" proc-id="RM23G0E___0000FMG00001">
<testtitle>CHECK FRONT SEAT INNER BELT LH (SEAT BELT BUCKLE SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connectors to the center airbag sensor and front seat inner belt LH.</ptxt>
<figure>
<graphic graphicname="C157773E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0IZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0IZX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1656 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1656 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Seat Inner Belt LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002P5G03SX_04_0012" fin="true">OK</down>
<right ref="RM000002P5G03SX_04_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0006" proc-id="RM23G0E___0000FMH00001">
<testtitle>REPLACE FRONT SEAT INNER BELT ASSEMBLY LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Replace the front seat inner belt LH (See page <xref label="Seep01" href="RM000003RQO019X"/>).</ptxt>
<atten4>
<ptxt>Perform the inspection using parts from a normally functioning vehicle if possible.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002P5G03SX_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0007" proc-id="RM23G0E___0000FMI00001">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
<figure>
<graphic graphicname="C157773E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XFE0IZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0IZX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1656 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1656 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Seat Inner Belt LH</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002P5G03SX_04_0014" fin="true">OK</down>
<right ref="RM000002P5G03SX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0008">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0INX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0009">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0010">
<testtitle>REPLACE NO. 2 FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0011">
<testtitle>REPLACE NO. 2 FLOOR WIRE</testtitle>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0012">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0INX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0013">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000003YQB00UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002P5G03SX_04_0014">
<testtitle>REPLACE FRONT SEAT INNER BELT ASSEMBLY LH<xref label="Seep01" href="RM000003RQO019X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>