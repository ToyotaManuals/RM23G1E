<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001H" variety="S001H">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001H_7B97Z_T00HN" variety="T00HN">
<name>BRAKE PEDAL (for Hydraulic Brake Booster)</name>
<para id="RM000001Q8G02AX" category="A" type-id="30014" name-id="BR3VE-04" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000001Q8G02AX_02" type-id="11" category="10" proc-id="RM23G0E___0000ATX00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000001Q8G02AX_01" type-id="01" category="01">
<s-1 id="RM000001Q8G02AX_01_0028" proc-id="RM23G0E___0000ATQ00001">
<ptxt>INSTALL BRAKE PEDAL SUPPORT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the reinforcement set bolt.</ptxt>
</s2>
<s2>
<ptxt>for LHD:</ptxt>
<s3>
<ptxt>Install the hydraulic brake booster (See page <xref label="Seep01" href="RM00000171Q026X"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<s3>
<ptxt>Install the hydraulic brake booster (See page <xref label="Seep02" href="RM00000171Q027X"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install the brake pedal support sub-assembly with the 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>145</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the brake pedal support reinforcement set bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8G02AX_01_0030" proc-id="RM23G0E___0000ATR00001">
<ptxt>INSTALL PUSH ROD PIN</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of lithium soap base glycol grease to the inner surface of the hole on the brake pedal lever.</ptxt>
<figure>
<graphic graphicname="C215915E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lithium soap base glycol grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Set the master cylinder push rod clevis in place, insert the push rod pin from the left side of the vehicle, and then install a new clip.</ptxt>
<figure>
<graphic graphicname="C213444" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8G02AX_01_0058" proc-id="RM23G0E___0000ATW00001">
<ptxt>INSTALL BRAKE PEDAL RETURN SPRING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of lithium soap base glycol grease to inner surface of the hole in the brake pedal support sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Install the brake pedal return spring to the brake pedal support sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8G02AX_01_0043" proc-id="RM23G0E___0000ATS00001">
<ptxt>INSTALL STOP LIGHT SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the stop light switch (See page <xref label="Seep01" href="RM000003QJQ00TX_01_0002"/>).</ptxt>
</s2>
<s2>
<ptxt>Connect the stop light switch connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8G02AX_01_0048" proc-id="RM23G0E___0000ATT00001">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL AIRBAG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the lower No. 1 instrument panel airbag assembly (See page <xref label="Seep01" href="RM000003YPT02BX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q8G02AX_01_0055" proc-id="RM23G0E___0000ATV00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001Q8G02AX_01_0059" proc-id="RM23G0E___0000ASS00001">
<ptxt>BLEED BRAKE SYSTEM
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>If air is bled without using the intelligent tester, damage or accidents may result. Therefore, always use the intelligent tester when bleeding air.</ptxt>
</atten2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</s2>
<s2>
<ptxt>Remove the brake master cylinder reservoir filler cap assembly.</ptxt>
</s2>
<s2>
<ptxt>Add brake fluid until the fluid level is between the MIN and MAX lines of the reservoir.</ptxt>
</s2>
<s2>
<ptxt>Repeatedly depress the brake pedal and bleed air from the bleeder plug of the front disc brake cylinder RH.</ptxt>
</s2>
<s2>
<ptxt>Repeat the step above until the air is completely bled, and then tighten the bleeder plug while depressing the brake pedal.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed the air from the bleeder plug of the front disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>With the brake pedal depressed, loosen the bleeder plug of the rear disc brake cylinder RH, continue to hold the brake pedal and allow brake fluid to be drained from the bleeder plug while the pump motor operates.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Air is bled as the pump motor operates while the brake pedal is being depressed.</ptxt>
</item>
<item>
<ptxt>Be sure to release the brake pedal to stop the motor after approximately 100 seconds of continuous operation.</ptxt>
</item>
<item>
<ptxt>As brake fluid is continuously drained while the pump operates, it is not necessary to repeatedly depress the brake pedal.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>When there is no more air in the brake fluid, tighten the bleeder plug, and then release the brake pedal.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed the air from the bleeder plug of the rear disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch off and connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</s2>
<s2>
<ptxt>Turn the intelligent tester on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Utility / Air Bleeding.</ptxt>
<atten3>
<ptxt>To protect the solenoid from overheating, the solenoid operation stops automatically in 4 seconds, and then the solenoid will not respond to commands for an additional 20 seconds.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Repeatedly depress the brake pedal several times, and then, with the brake pedal depressed, turn FR Line on and bleed air.</ptxt>
<atten4>
<ptxt>Air returns to the brake master cylinder reservoir together with the brake fluid and is bled from the brake system.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>As it is not possible to visually confirm that air is being bled, repeat this step 10 times.</ptxt>
</item>
<item>
<ptxt>Do not loosen the bleeder plug.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Turn FL Line on and bleed air using the same procedures as for FR.</ptxt>
</s2>
<s2>
<ptxt>Turn RR Line on, loosen the bleeder plug of the rear disc brake cylinder RH and drain brake fluid.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Do not depress the brake pedal.</ptxt>
</item>
<item>
<ptxt>As brake fluid is automatically drained while the pump and solenoid operate, it is not necessary to operate the brake pedal.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Repeat the step above until the air is completely bled, and then tighten the bleeder plug.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Turn RL Line on and bleed air from the bleeder plug of the rear disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>Turn the intelligent tester off and turn the ignition switch off.</ptxt>
</s2>
<s2>
<ptxt>Inspect for brake fluid leaks.</ptxt>
</s2>
<s2>
<ptxt>Check and adjust the brake fluid level (See page <xref label="Seep01" href="RM0000012XQ02ZX"/>).</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM0000046KV00LX"/>).</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02AX_01_0063" proc-id="RM23G0E___00008CZ00000">
<ptxt>FILL RESERVOIR WITH BRAKE FLUID
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215410E01" width="2.775699831in" height="5.787629434in"/>
</figure>
<s2>
<ptxt>Fill the reservoir with brake fluid.</ptxt>
<spec>
<title>Brake fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD (2TR-FE, 5L-E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD (1GR-FE, 1KD-FTV)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02AX_01_0062" proc-id="RM23G0E___00008CY00000">
<ptxt>BLEED CLUTCH LINE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bleeder plug cap of the release cylinder.</ptxt>
</s2>
<s2>
<ptxt>Connect a vinyl tube to the bleeder plug.</ptxt>
</s2>
<s2>
<ptxt>Depress the clutch pedal several times, and then loosen the bleeder plug while the pedal is depressed.</ptxt>
</s2>
<s2>
<ptxt>When fluid no longer comes out, tighten the bleeder plug, and then release the clutch pedal.</ptxt>
</s2>
<s2>
<ptxt>Repeat the previous 2 steps until all the air in the fluid is completely bled.</ptxt>
</s2>
<s2>
<ptxt>Tighten the bleeder plug. </ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the bleeder plug cap.</ptxt>
</s2>
<s2>
<ptxt>Check that all the air has been bled from the clutch line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02AX_01_0064" proc-id="RM23G0E___00008D000000">
<ptxt>CHECK FLUID LEVEL IN RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fluid level.</ptxt>
<ptxt>If the brake fluid level is low, check for leaks and inspect the disc brake pad. If necessary, refill the reservoir with brake fluid after repair or replacement.</ptxt>
<spec>
<title>Brake fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM000001Q8G02AX_01_0065">
<ptxt>CHECK FOR BRAKE FLUID LEAK FROM CLUTCH LINE</ptxt>
</s-1>
<s-1 id="RM000001Q8G02AX_01_0054" proc-id="RM23G0E___0000ATU00001">
<ptxt>CHECK AND ADJUST BRAKE PEDAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check and adjust brake pedal (See page <xref label="Seep01" href="RM000001QHK01SX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>