<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000G" variety="S000G">
<name>5L-E FUEL</name>
<ttl id="12008_S000G_7B8ZL_T0099" variety="T0099">
<name>FUEL SYSTEM</name>
<para id="RM0000047AH002X" category="L" type-id="3001A" name-id="FU55H-01" from="201207">
<name>PRECAUTION</name>
<subpara id="RM0000047AH002X_z0" proc-id="RM23G0E___00005ZS00000">
<content5 releasenbr="1">
<step1>
<ptxt>FUEL SYSTEM</ptxt>
<step2>
<ptxt>Observe these precautions when disconnecting the fuel tube connector:</ptxt>
<step3>
<ptxt>Check if there is any dirt like mud on the pipe and around the connector before disconnecting them. Clean the dirt away if necessary.</ptxt>
</step3>
<step3>
<ptxt>Disconnect them by your hands.</ptxt>
</step3>
<step3>
<ptxt>When the connector and the pipe are stuck, push and pull the connector to free to disconnect and pull it out.</ptxt>
<figure>
<graphic graphicname="A223368" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
<figure>
<graphic graphicname="A062772E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Inspect if there is any dirt or the like on the seal surface of the disconnected pipe. Clean it away if necessary.</ptxt>
</step3>
<step3>
<ptxt>Prevent the disconnected pipe and connector from damaging and foreign objects from being mixed into by covering them with a plastic bag.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when connecting the fuel tube connector (for quick type B):</ptxt>
<step3>
<ptxt>Match the axis of the connector with the axis of the pipe, and push in the connector until the connector makes a "click" sound. In case that the connection is tight, apply a little amount of new engine oil on the tip of the pipe.</ptxt>
<figure>
<graphic graphicname="A223369" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>After having finished the connection, check if the pipe and the connector are securely connected by pulling them.</ptxt>
<figure>
<graphic graphicname="A223370" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Inspect for fuel leak.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when disconnecting the fuel tank breather tube connector:</ptxt>
<step3>
<ptxt>Pinch the retainer and pull out the fuel tank breather tube connector with the fuel tank breather tube connector pushed to the pipe side to disconnect the fuel tank breather tube from the pipe.</ptxt>
<figure>
<graphic graphicname="A224871E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Breather Tube Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pinch</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="A224872E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Remove dirt or foreign objects on the fuel tank breather tube connector before this procedure.</ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign objects on the parts when disconnecting them as the fuel tank breather tube connector has the O-ring that seals the pipe.</ptxt>
</item>
<item>
<ptxt>Perform this work by hand. Do not use any tools.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, kink or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the connecting part by covering it with a plastic bag after disconnecting the fuel tank breather tube.</ptxt>
</item>
<item>
<ptxt>If the fuel tank breather tube connector and pipe are stuck, push and pull them to release them.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Breather Tube Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>O-ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when connecting the fuel tank breather tube connector:</ptxt>
<step3>
<ptxt>Align the fuel tank breather tube connector with the pipe, then push in the fuel tank breather tube connector until the retainer makes a "click" sound to install the fuel tank breather tube to the pipe.</ptxt>
<figure>
<graphic graphicname="A224875" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check that there are no scratches or foreign objects around the connected part of the fuel tank breather tube connector and pipe before this procedure.</ptxt>
</item>
<item>
<ptxt>After connecting the fuel tank breather tube, check that the fuel tank breather tube is securely connected by pulling the fuel tank breather tube connector.</ptxt>
</item>
</list1>
</atten3>
</step3>
<step3>
<ptxt>After having finished the connection, check if the pipe and the connector are securely connected by pulling them.</ptxt>
<figure>
<graphic graphicname="A224876" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Inspect for fuel leak.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>CHECK FOR FUEL LEAK</ptxt>
<step2>
<ptxt>Check that there are no fuel leaks after performing maintenance anywhere on the fuel system (See page <xref label="Seep01" href="RM000001OM5008X_01_0001"/>).</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>