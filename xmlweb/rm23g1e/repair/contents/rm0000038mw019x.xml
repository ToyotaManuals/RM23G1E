<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0022" variety="S0022">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0022_7B9CD_T00M1" variety="T00M1">
<name>REAR FLOOR AIRBAG SENSOR</name>
<para id="RM0000038MW019X" category="A" type-id="80001" name-id="RSDK4-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000038MW019X_01" type-id="01" category="01">
<s-1 id="RM0000038MW019X_01_0001" proc-id="RM23G0E___0000FRB00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work. (See page <xref label="Seep02" href="RM000003YMD00GX"/>)</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000038MW019X_01_0014" proc-id="RM23G0E___0000FRE00001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 60/40 Split Double-folding Seat Type LH Side:</ptxt>
<s3>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM00000468P00JX"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 60/40 Split Double-folding Seat Type RH Side:</ptxt>
<s3>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep02" href="RM00000468U00MX"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 60/40 Split Tumble Seat Type LH Side:</ptxt>
<s3>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep03" href="RM0000046ZI003X"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 60/40 Split Tumble Seat Type RH Side:</ptxt>
<s3>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep04" href="RM0000046ZN003X"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 60/40 Split Slide Walk-in Seat Type LH Side:</ptxt>
<s3>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep05" href="RM00000468A00LX"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>for 60/40 Split Slide Walk-in Seat Type RH Side:</ptxt>
<s3>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep06" href="RM00000468F00GX"/>).</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038MW019X_01_0018" proc-id="RM23G0E___0000C3R00001">
<ptxt>REMOVE REAR NO. 1 FLOOR STEP COVER (w/ Rear No. 2 Seat)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238730E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for all rear No. 1 floor step covers.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the rear No. 1 floor step cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000038MW019X_01_0019" proc-id="RM23G0E___0000C3S00001">
<ptxt>REMOVE QUARTER SCUFF PLATE LH (w/ Rear No. 2 Seat)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238731" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and quarter scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038MW019X_01_0020" proc-id="RM23G0E___0000F3Z00001">
<ptxt>REMOVE QUARTER SCUFF PLATE RH (w/ Rear No. 2 Seat)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM0000038MW019X_01_0021" proc-id="RM23G0E___0000C3T00001">
<ptxt>REMOVE REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238733E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the rear door scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 3 clips, 6 claws and 2 guides and remove the rear door scuff plate.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000038MW019X_01_0022" proc-id="RM23G0E___0000F4000001">
<ptxt>REMOVE REAR DOOR SCUFF PLATE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM0000038MW019X_01_0004" proc-id="RM23G0E___0000FRC00001">
<ptxt>REMOVE REAR FLOOR SIDE AIRBAG SENSOR COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Fold back the floor carpet.</ptxt>
</s2>
<s2>
<ptxt>Detach the clip and claw and remove the cover.</ptxt>
<figure>
<graphic graphicname="B238364" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038MW019X_01_0012" proc-id="RM23G0E___0000FRD00001">
<ptxt>REMOVE REAR FLOOR SIDE AIRBAG SENSOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and sensor.</ptxt>
<figure>
<graphic graphicname="B238365" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>