<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001B" variety="S001B">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001B_7B971_T00GP" variety="T00GP">
<name>KINETIC DYNAMIC SUSPENSION SYSTEM</name>
<para id="RM0000045UJ00GX" category="C" type-id="804SS" name-id="SC1FI-11" from="201207" to="201210">
<dtccode>C1887/87</dtccode>
<dtcname>Acceleration Sensor Circuit Malfunction</dtcname>
<subpara id="RM0000045UJ00GX_01" type-id="60" category="03" proc-id="RM23G0E___00009V200000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The stabilizer control ECU receives forward, backward and lateral acceleration information from the yaw rate and acceleration sensor via CAN communication.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1887/87</ptxt>
</entry>
<entry valign="middle">
<ptxt>The stabilizer control ECU receives an error signal from the yaw rate and acceleration sensor for 1 second.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Yaw rate and acceleration sensor</ptxt>
</item>
<item>
<ptxt>Yaw rate and acceleration sensor circuit</ptxt>
</item>
<item>
<ptxt>Stabilizer control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000045UJ00GX_02" type-id="51" category="05" proc-id="RM23G0E___00009V300000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>If DTC U0124/71 and C1887/87 are output at the same time, perform the inspection necessary for the CAN communication system first (See page <xref label="Seep01" href="RM000002PEG01UX"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000045UJ00GX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000045UJ00GX_03_0002" proc-id="RM23G0E___00009V400000">
<testtitle>CHECK DTC (VEHICLE STABILITY CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for yaw rate and acceleration sensor malfunction DTCs (See page <xref label="Seep01" href="RM0000046KV00IX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000045UJ00GX_03_0003" fin="false">A</down>
<right ref="RM0000045UJ00GX_03_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000045UJ00GX_03_0003" proc-id="RM23G0E___00009V500000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000001N9I014X"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform a road test.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000001N9I014X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000045UJ00GX_03_0007" fin="true">A</down>
<right ref="RM0000045UJ00GX_03_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000045UJ00GX_03_0004">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM (DIAGNOSTIC TROUBLE CODE CHART)<xref label="Seep01" href="RM0000045Z600OX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000045UJ00GX_03_0006">
<testtitle>REPLACE STABILIZER CONTROL ECU<xref label="Seep01" href="RM000002KLG00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000045UJ00GX_03_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>