<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM0000041P802KX" category="C" type-id="3038W" name-id="ESTE3-11" from="201207" to="201210">
<dtccode>P0400</dtccode>
<dtcname>Exhaust Gas Recirculation Flow</dtcname>
<subpara id="RM0000041P802KX_01" type-id="60" category="03" proc-id="RM23G0E___00002GM00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The EGR system recirculates exhaust gases. The recirculated gas mingles with the intake air so that the EGR system can slow combustion speed and keep the combustion temperature down. This helps reduce NOx emissions.</ptxt>
<ptxt>In order to increase EGR circulation efficiency, the ECM adjusts the EGR valve angle and the throttle valve angle.</ptxt>
<table pgwide="1">
<title>P0400</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>After warming up the engine, decelerate from a speed of 80 km/h (50 mph) or more (fully release the accelerator pedal for approximately 5 seconds).</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mass air flow rate is not changed when turning on the electric EGR control valve assembly while decelerating.</ptxt>
<ptxt>(2 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>EGR valve deposit</ptxt>
</item>
<item>
<ptxt>EGR valve passage deposit</ptxt>
</item>
<item>
<ptxt>EGR cooler</ptxt>
</item>
<item>
<ptxt>Mass air flow meter</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0400</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>MAF</ptxt>
</item>
<item>
<ptxt>Target EGR Position</ptxt>
</item>
<item>
<ptxt>Actual EGR Valve Pos.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000041P802KX_02" type-id="64" category="03" proc-id="RM23G0E___00002GN00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the EGR valve is forcibly operated during deceleration but the intake air amount does not vary, the ECM determines that the EGR valve is malfunctioning. The ECM then illuminates the MIL (2 trip detection logic).</ptxt>
<figure>
<graphic graphicname="A194528E03" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000041P802KX_04" type-id="51" category="05" proc-id="RM23G0E___00002GO00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK062X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See Page <xref label="Seep04" href="RM0000012XK062X"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000041P802KX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000041P802KX_05_0001" proc-id="RM23G0E___00002GP00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0400)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on. </ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC. </ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0400 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0400 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000041P802KX_05_0004" fin="false">A</down>
<right ref="RM0000041P802KX_05_0030" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0004" proc-id="RM23G0E___00002GQ00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (ACTIVATE THE EGR VALVE CLOSE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up, and make sure the A/C switch and all accessory switches are off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off. Wait for 30 seconds, and then restart the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / MAF.</ptxt>
</test1>
<test1>
<ptxt>Read the MAF value displayed on the tester while the engine is idling.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the EGR Valve Close.</ptxt>
</test1>
<test1>
<ptxt>Read the MAF value when the EGR valve is closed using the Active Test function.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If idling continues for 20 minutes or more, the EGR valve target opening angle becomes 0% (EGR valve fully closed). As this makes diagnosis impossible, it becomes necessary to drive the vehicle or to restart the engine.</ptxt>
</item>
<item>
<ptxt>Before performing the diagnosis, confirm that the EGR valve target opening angle is not 0%.</ptxt>
</item>
<item>
<ptxt>When "Coolant Temp" is 75°C (167°F) or higher during idling, the exhaust gas flows through the EGR cooler. If MAF does not change when the EGR is cut even though the EGR valve is operating normally, the EGR cooler may be clogged.</ptxt>
</item>
<item>
<ptxt>When idling while warming up the engine, the actuator operates and the EGR cooler is bypassed. However, if the connector of the VSV for the EGR cooler is disconnected, the EGR cooler is not bypassed and it is possible to check how clogged the EGR cooler is.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.37in"/>
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Active Test</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1">
<ptxt>Activate the EGR Valve Close:</ptxt>
<ptxt>Off (Open) to On (Closed)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>MAF value does not change</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MAF value changes</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>As the measured values may differ from those shown below due to factors such as differences in measuring environments and changes in vehicle condition due to aging, do not use these values to determine whether the vehicle is malfunctioning or not.</ptxt>
</atten3>
<atten4>
<ptxt>The problem may be a temporary one, due to the entry of deposits or foreign matter. Check that there are no deposits or foreign matter in the electric vacuum regulating valve assembly or mass air flow meter.</ptxt>
</atten4>
<spec>
<title>Reference</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.37in"/>
<colspec colname="COL2" colwidth="2.37in"/>
<colspec colname="COL3" colwidth="2.34in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>EGR Valve Condition (Opening)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measuring Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>MAF (Reference)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Open (55%)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Atmosphere pressure: 101 kPa</ptxt>
</item>
<item>
<ptxt>Intake air temperature: 30°C (86°F)</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature: 88°C (190°F)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>3 to 10 gm/s</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Closed (0%)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>13 to 20 gm/s</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000041P802KX_05_0036" fin="false">A</down>
<right ref="RM0000041P802KX_05_0038" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0036" proc-id="RM23G0E___00002GS00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (MAF)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / MAF.</ptxt>
</test1>
<test1>
<ptxt>With the engine switch on (IG) and engine stopped, read the value when 30 seconds has elapsed.</ptxt>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Standard</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>MAF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch on (IG) (do not start engine)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Less than 0.3 gm/sec</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000041P802KX_05_0037" fin="false">OK</down>
<right ref="RM0000041P802KX_05_0039" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0037" proc-id="RM23G0E___00002GT00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (ACTIVATE THE VSV FOR EGR COOLER BYPASS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up, and make sure the A/C switch and all accessory switches are off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and wait for 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / MAF.</ptxt>
</test1>
<test1>
<ptxt>Read the MAF value displayed on the tester while the engine is idling.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the VSV for EGR Cooler Bypass / Data List / EGR Cooler Bypass Position.</ptxt>
<atten4>
<ptxt>Whether the EGR is passing through the bypass side or cooler side during the Active Test can be confirmed by checking EGR Cooler Bypass Position.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Using the Active Test function, switch the EGR valve bypass switching valve between "Cooler" and "Bypass" a few times, and then check whether the MAF value changes after switching the valve from "Cooler" to "Bypass".</ptxt>
<atten4>
<ptxt>If idling continues for 20 minutes or more, the EGR valve target opening angle becomes 0% (EGR valve fully closed). As this makes diagnosis impossible, it becomes necessary to drive the vehicle or restart the engine.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Activate the VSV for EGR Cooler Bypass:</ptxt>
<ptxt>Cooler to Bypass</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>MAF value does not change</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MAF value changes</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the EGR cooler is clogged, the mass air flow changes.</ptxt>
</item>
<item>
<ptxt>When confirming the change in the MAF value, do not determine the change at "Half Position". Perform the Active Test so that the system changes from "Cooler" to "Bypass" to confirm the change in the MAF value.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<right ref="RM0000041P802KX_05_0040" fin="false">A</right>
<right ref="RM0000041P802KX_05_0041" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0038" proc-id="RM23G0E___00002GU00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (ACTIVATE THE VSV FOR EGR COOLER BYPASS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up, and make sure the A/C switch and all accessory switches are off.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off and wait for 30 seconds.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / MAF.</ptxt>
</test1>
<test1>
<ptxt>Read the MAF value displayed on the tester while the engine is idling.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Activate the VSV for EGR Cooler Bypass / Data List / EGR Cooler Bypass Position.</ptxt>
<atten4>
<ptxt>Whether the EGR is passing through the bypass side or cooler side during the Active Test can be confirmed by checking EGR Cooler Bypass Position.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Using the Active Test function, switch the EGR valve bypass switching valve between "Cooler" and "Bypass" a few times, and then check whether the MAF value changes after switching the valve from "Cooler" to "Bypass".</ptxt>
<atten4>
<ptxt>If idling continues for 20 minutes or more, the EGR valve target opening angle becomes 0% (EGR valve fully closed). As this makes diagnosis impossible, it becomes necessary to drive the vehicle or restart the engine.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Activate the VSV for EGR Cooler Bypass:</ptxt>
<ptxt>Cooler to Bypass</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>MAF value does not change</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>MAF value changes</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the EGR cooler is clogged, the mass air flow changes.</ptxt>
</item>
<item>
<ptxt>When confirming the change in the MAF value, do not determine the change at "Half Position". Perform the Active Test so that the system changes from "Cooler" to "Bypass" to confirm the change in the MAF value.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000041P802KX_05_0023" fin="false">A</down>
<right ref="RM0000041P802KX_05_0041" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0023" proc-id="RM23G0E___00002GR00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0400)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z7X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Move the shift lever to 2nd gear and decelerate from a speed of 50 km/h (31 mph) or more (fully release the accelerator for approximately 5 seconds or more).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<atten4>
<ptxt>When a DTC is output, the mass air flow meter may be malfunctioning.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000041P802KX_05_0024" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0039" proc-id="RM23G0E___00002GV00000">
<testtitle>REPLACE MASS AIR FLOW METER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the mass air flow meter (See page <xref label="Seep01" href="RM000000VHD04IX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM0000041P802KX_05_0042" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0040" proc-id="RM23G0E___00002GW00000">
<testtitle>CHECK FOR DEPOSIT (ELECTRIC EGR CONTROL VALVE ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the electric EGR control valve assembly (See page <xref label="Seep01" href="RM000004KSK004X"/>).</ptxt>
</test1>
<test1>
<ptxt>Visually check the electric EGR control valve assembly for deposits.</ptxt>
<ptxt>If there are deposits, clean the electric EGR control valve assembly.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the EGR valve does not open properly or is stuck closed, the amount of intake air increases and combustion sounds and engine vibration may increase.</ptxt>
</item>
<item>
<ptxt>If the EGR valve does not close properly or is stuck open, EGR becomes excessive and combustion becomes unstable. Also, there may be a lack of power.</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Reinstall the electric EGR control valve assembly (See page <xref label="Seep02" href="RM000004KSH003X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000041P802KX_05_0041" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0041" proc-id="RM23G0E___00002GX00000">
<testtitle>REPLACE EGR COOLER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the EGR cooler (See page <xref label="Seep01" href="RM000004LOY004X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000041P802KX_05_0042" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0042" proc-id="RM23G0E___00002GY00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0400)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z7X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Move the shift lever to 2nd gear and decelerate from a speed of 50 km/h (31 mph) or more (fully release the accelerator for approximately 5 seconds or more).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<atten4>
<ptxt>When a DTC is output, the mass air flow meter may be malfunctioning.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000041P802KX_05_0024" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000041P802KX_05_0024">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000041P802KX_05_0030">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW03MX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>