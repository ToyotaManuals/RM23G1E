<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94A_T00DY" variety="T00DY">
<name>AUTOMATIC TRANSMISSION ASSEMBLY (for 1GR-FE)</name>
<para id="RM0000018ZB047X" category="A" type-id="30014" name-id="AT947-02" from="201210">
<name>INSTALLATION</name>
<subpara id="RM0000018ZB047X_02" type-id="01" category="01">
<s-1 id="RM0000018ZB047X_02_0025" proc-id="RM23G0E___00007T100001">
<ptxt>INSTALL TORQUE CONVERTER CLUTCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper and straightedge, measure dimension" A" between the transmission fitting surface of the engine *1 and the torque converter fitting surface of the drive plate *2 (step 1).</ptxt>
<figure>
<graphic graphicname="C209683E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Align the matchmarks on the transmission case and torque converter clutch, and then mesh the splines of the input shaft and turbine runner.</ptxt>
<figure>
<graphic graphicname="C209685E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Mesh the splines of the stator shaft and stator while turning the torque converter clutch.</ptxt>
<figure>
<graphic graphicname="C209687E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Turn the torque converter clutch approximately 180°.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Turn the torque converter clutch and align the matchmarks on the torque converter clutch and transmission case to fit the key of the oil pump drive gear into the slot on the torque converter clutch.</ptxt>
<figure>
<graphic graphicname="C217017E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not push on the torque converter when aligning the matchmarks.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a vernier caliper and straightedge, measure dimension B shown in the illustration and check that B is more than A, which was measured in the first step.</ptxt>
<figure>
<graphic graphicname="C209689E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard distance</title>
<specitem>
<ptxt>B = A + 1 mm (0.0394 in.) or more </ptxt>
</specitem>
</spec>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0092" proc-id="RM23G0E___00007TH00001">
<ptxt>INSTALL NO. 1 TRANSMISSION CONTROL CABLE BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 transmission control cable bracket to the transmission assembly with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0062" proc-id="RM23G0E___00007T900001">
<ptxt>INSTALL TRANSFER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the transfer (See page <xref label="Seep01" href="RM000001QHX027X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0045" proc-id="RM23G0E___00007T500001">
<ptxt>INSTALL AUTOMATIC TRANSMISSION ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply clutch spline grease to the surface of the crankshaft that contacts the torque converter clutch centerpiece.</ptxt>
<figure>
<graphic graphicname="C218126E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Clutch spline grease</title>
<specitem>
<ptxt>Toyota Genuine Clutch Spline Grease or equivalent</ptxt>
</specitem>
<subtitle>Maximum grease amount</subtitle>
<specitem>
<ptxt>Approximately 1 g (0.0353 oz.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Torque Converter Clutch Centerpiece</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Crankshaft</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Confirm that the 2 knock pins are on the transmission contact surface of the engine block before transmission installation. </ptxt>
<figure>
<graphic graphicname="C217032E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Install the transmission with the 9 bolts.</ptxt>
<torque>
<subtitle>for 17 mm head bolt A</subtitle>
<torqueitem>
<t-value1>71</t-value1>
<t-value2>724</t-value2>
<t-value4>52</t-value4>
</torqueitem>
<subtitle>for 14 mm head bolt B</subtitle>
<torqueitem>
<t-value1>37</t-value1>
<t-value2>377</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0070" proc-id="RM23G0E___00007TB00001">
<ptxt>CONNECT BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 3 breather plug hoses to the engine.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0055" proc-id="RM23G0E___00007T600001">
<ptxt>CONNECT WIRE HARNESS AND CONNECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the park/neutral position switch connector, transmission wire connector, 2 speed sensor connectors and transfer control side connector.</ptxt>
<atten4>
<ptxt>Push up the lever until the claw of the transmission wire connector makes a connection sound.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Attach the 2 connector clamps and 7 harness clamps.</ptxt>
<figure>
<graphic graphicname="C218115E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Tilt up the automatic transmission.</ptxt>
</s2>
<s2>
<ptxt>Connect the ground cable with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0029" proc-id="RM23G0E___00003U200001">
<ptxt>INSTALL REAR NO. 1 ENGINE MOUNTING INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear engine mounting insulator to the transmission with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>65</t-value1>
<t-value2>663</t-value2>
<t-value4>48</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the rear engine mounting heat insulator to the engine mounting insulator with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0030" proc-id="RM23G0E___00007T200001">
<ptxt>INSTALL NO. 3 FRAME CROSSMEMBER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the frame crossmember to the rear engine mounting insulator with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>30</t-value1>
<t-value2>306</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the frame crossmember with the 4 bolts and 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>72</t-value1>
<t-value2>734</t-value2>
<t-value4>53</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0102" proc-id="RM23G0E___00007TM00001">
<ptxt>INSTALL FRONT SUSPENSION MEMBER BRACKET LH AND RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front suspension member bracket LH and RH with the 8 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>33</t-value1>
<t-value2>337</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0052" proc-id="RM23G0E___00003TZ00001">
<ptxt>INSTALL DRIVE PLATE AND TORQUE CONVERTER CLUTCH SETTING BOLT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Turn the crankshaft to gain access to the installation locations of the 6 torque converter clutch setting bolts and install each bolt while holding the crankshaft pulley bolt with a wrench.</ptxt>
<figure>
<graphic graphicname="C218128" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>48</t-value1>
<t-value2>489</t-value2>
<t-value4>35</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Install the black bolt first, and then the 5 silver bolts.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the flywheel housing side cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0073" proc-id="RM23G0E___00007TE00001">
<ptxt>INSTALL STARTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the starter (See page <xref label="Seep01" href="RM0000022A9013X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0094" proc-id="RM23G0E___00007TI00001">
<ptxt>CONNECT NO. 1 INLET OIL COOLER TUBE AND NO. 1 OUTLET OIL COOLER TUBE (w/o Air Cooled Transmission Oil Cooler)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C218114" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install the 2 oil cooler tubes with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0089" proc-id="RM23G0E___00007TG00001">
<ptxt>INSTALL TRANSMISSION OIL COOLER ASSEMBLY (w/o Air Cooled Transmission Oil Cooler)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the 2 hoses to the oil cooler tube unions.</ptxt>
<figure>
<graphic graphicname="C217315E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Temporarily install the transmission oil cooler with the bolt A. Install the bolt B and C tighten it to the specified torque. Then tighten the bolt A to the specified torque.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the No. 2 oil cooler inlet hose and No. 2 oil cooler outlet hose to the transmission oil thermostat.</ptxt>
<figure>
<graphic graphicname="C218112" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure the pinching portion of each clip are facing the directions shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the 2 water by-pass hoses and install the water by-pass pipe to the automatic transmission with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>214</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="C214593E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>White Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yellow Paint Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure the pinching portion of each clip is facing the direction shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Make sure the paint mark of each hose is facing outward.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0095" proc-id="RM23G0E___00007TJ00001">
<ptxt>CONNECT NO. 1 INLET OIL COOLER TUBE AND NO. 1 OUTLET OIL COOLER TUBE (w/ Air Cooled Transmission Oil Cooler)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the ends of the oil cooler inlet tube and outlet tube to each oil cooler tube union by hand.</ptxt>
</s2>
<s2>
<ptxt>Close the 2 No. 2 flexible hose clamps and install the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a union nut wrench, tighten the inlet and outlet tubes.</ptxt>
<torque>
<torqueitem>
<t-value1>34</t-value1>
<t-value2>346</t-value2>
<t-value4>25</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0096" proc-id="RM23G0E___00007TK00001">
<ptxt>CONNECT TRANSMISSION CONTROL CABLE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the transmission control cable to the transmission control cable bracket with a new clip, and then connect the cable end to the control shaft lever with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0072" proc-id="RM23G0E___00007TD00001">
<ptxt>INSTALL MANIFOLD STAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the stay with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0071" proc-id="RM23G0E___00007TC00001">
<ptxt>INSTALL NO. 2 MANIFOLD STAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the stay with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0038" proc-id="RM23G0E___00007T300001">
<ptxt>INSTALL PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the propeller shaft (See page <xref label="Seep01" href="RM0000029WN00LX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0064" proc-id="RM23G0E___00007TA00001">
<ptxt>INSTALL FRONT PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front propeller shaft (See page <xref label="Seep01" href="RM00000291O00KX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0075" proc-id="RM23G0E___00007TF00001">
<ptxt>INSTALL FRONT EXHAUST PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front exhaust pipe (See page <xref label="Seep01" href="RM00000456H00GX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0101" proc-id="RM23G0E___00007TL00001">
<ptxt>INSTALL TRANSFER CASE LOWER PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the transfer case lower protector with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0056" proc-id="RM23G0E___00007T700001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0106" proc-id="RM23G0E___00000EK00000">
<ptxt>ADD ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the cylinder block drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>130</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
<s2>
<ptxt>Add engine coolant.</ptxt>
<spec>
<title>Standard Capacity</title>
<table>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="1.03in"/>
<colspec colname="COL3" colwidth="1.03in"/>
<colspec colname="COL4" colwidth="1.04in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL3" valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="3" valign="middle" align="center">
<ptxt>for Automatic Transmission</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>w/o Warmer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10.5 liters (11.1 US qts, 9.2 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>12.3 liters (13.0 US qts, 10.8 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>w/ Warmer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11.0 liters (11.6 US qts, 9.7 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>12.8 liters (13.5 US qts, 11.3 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>for Manual Transmission</ptxt>
</entry>
<entry namest="COL2" nameend="COL3" valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10.7 liters (11.3 US qts, 9.4 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry namest="COL2" nameend="COL3" valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>12.5 liters (13.2 US qts, 11.0 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Do not substitute plain water for engine coolant.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</item>
<item>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand, and then check the coolant level. If the coolant level is low, add coolant.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Slowly pour coolant into the radiator reservoir until it reaches the F line.</ptxt>
</s2>
<s2>
<ptxt>Install the reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator cap.*1</ptxt>
</s2>
<s2>
<ptxt>Start the engine and stop it immediately.*2</ptxt>
</s2>
<s2>
<ptxt>Allow approximately 10 seconds to pass. Then remove the radiator cap and check the coolant level. If the coolant level has decreased, add coolant.*3</ptxt>
</s2>
<s2>
<ptxt>Repeat steps *1, *2 and *3 until the coolant level does not decrease.</ptxt>
<atten4>
<ptxt>Be sure to perform this step while the engine is cold, as air in the No. 1 radiator hose will flow into the radiator if the engine is warmed up and the thermostat opens.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Install the radiator cap.*4</ptxt>
</s2>
<s2>
<ptxt>Set the air conditioning as follows.*5</ptxt>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Fan speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Any setting except off</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Toward WARM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Air conditioning switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Start the engine, warm it up until the thermostat opens, and then continue to run the engine for several minutes to circulate the coolant.*6</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves. Hot areas on the parts may injure your hands.</ptxt>
</item>
<item>
<ptxt>Be careful of the fan.</ptxt>
</item>
<item>
<ptxt>Be careful as the engine, radiator and radiator hoses are hot and can cause burns.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Immediately after starting the engine, if the radiator reservoir does not have any coolant, perform the following: 1) stop the engine, 2) wait until the coolant has cooled down, and 3) add coolant until the coolant is filled to the F line.</ptxt>
</item>
<item>
<ptxt>Do not start the engine when there is no coolant in the radiator reservoir.</ptxt>
</item>
<item>
<ptxt>Pay attention to the needle of the engine coolant temperature receiver gauge. Make sure that the needle does not show an abnormally high temperature.</ptxt>
</item>
<item>
<ptxt>If there is not enough coolant, the engine may burn out or overheat.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Press the No. 1 and No. 2 radiator hoses several times by hand to bleed air while warming up the engine.</ptxt>
</item>
<item>
<ptxt>The thermostat opening timing can be confirmed by pressing the No. 2 radiator hose by hand and checking when the engine coolant starts to flow inside the hose.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Stop the engine, wait until the engine coolant cools down to ambient temperature. Then remove the radiator cap and check the coolant level.*7</ptxt>
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s2>
<s2>
<ptxt>If the coolant level has decreased, add coolant and warm up the engine until the thermostat opens.*8</ptxt>
</s2>
<s2>
<ptxt>If the coolant level has not decreased, check that the coolant level in the radiator reservoir is at the F line.</ptxt>
<ptxt>If the coolant level is below the F line, repeat steps *4 through *8.</ptxt>
<ptxt>If the coolant level is above the F line, drain coolant until the coolant level reaches the F line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0041" proc-id="RM23G0E___00007T400001">
<ptxt>ADD AUTOMATIC TRANSMISSION FLUID</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Add automatic transmission fluid (See page <xref label="Seep01" href="RM000002BL003JX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000018ZB047X_02_0107" proc-id="RM23G0E___00000EL00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>To avoid being burned, do not remove the radiator reservoir cap while the engine and radiator are still hot. Thermal expansion may cause hot engine coolant and steam to blow out from the radiator.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with engine coolant, and then attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and then check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leakage. If there are no signs or traces of external engine coolant leakage, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0099" proc-id="RM23G0E___00007SJ00000">
<ptxt>INSPECT SHIFT LEVER POSITION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>When moving the shift lever from P to R with the ignition switch ON and the brake pedal depressed, make sure that it moves smoothly and correctly into position.</ptxt>
</s2>
<s2>
<ptxt>Check that the shift lever does not stop when moving the shift lever from R to P, and check that the shift lever does not stick when moving the shift lever from D to S.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and make sure that the vehicle moves forward after moving the shift lever from N to D and moves in reverse after moving the shift lever to R.</ptxt>
<ptxt>If the operation cannot be performed as specified, inspect the park/neutral position switch assembly and check the transmission floor shift assembly installation condition.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0108" proc-id="RM23G0E___00007SK00000">
<ptxt>ADJUST SHIFT LEVER POSITION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear console box (See page <xref label="Seep01" href="RM0000046JD004X"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Refrigerated Cool Box:</ptxt>
<ptxt>Remove the console box (See page <xref label="Seep02" href="RM0000046MR009X"/>).</ptxt>
</s2>
<s2>
<ptxt>Move the shift lever to N.</ptxt>
</s2>
<s2>
<ptxt>Slide the slider in the direction shown in the illustration and pull out the lock piece.</ptxt>
<figure>
<graphic graphicname="C203381E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Slider</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock Piece</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Push the lock piece into the adjuster case and lock it.</ptxt>
<figure>
<graphic graphicname="C204598E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure that the lock piece is completely locked by the slider.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the rear console box (See page <xref label="Seep03" href="RM0000046JB004X"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Refrigerated Cool Box:</ptxt>
<ptxt>Install the console box (See page <xref label="Seep04" href="RM0000046MQ009X"/>).</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0065" proc-id="RM23G0E___00003UB00000">
<ptxt>INSPECT FOR EXHAUST GAS LEAK
</ptxt>
<content1 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>If gas is leaking, tighten the areas necessary to stop the leak. Replace damaged parts as necessary.</ptxt>
</item>
</list1>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0086" proc-id="RM23G0E___00003TF00001">
<ptxt>INSTALL FRONT NO. 1 FENDER APRON TO FRAME SEAL RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front No. 1 fender apron to frame seal with the 5 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0085" proc-id="RM23G0E___00003TH00001">
<ptxt>INSTALL FRONT FENDER APRON SEAL RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front fender apron seal with the 5 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0098" proc-id="RM23G0E___00003TO00001">
<ptxt>INSTALL REAR ENGINE UNDER COVER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the rear engine under cover with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0097" proc-id="RM23G0E___00000FV00001">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Hook the engine under cover to the vehicle body as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Install the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000018ZB047X_02_0061" proc-id="RM23G0E___00007T800001">
<ptxt>PERFORM RESET MEMORY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Perform the Reset Memory procedures (A/T initialization) (See page <xref label="Seep01" href="RM000000W7F0LFX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>