<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S0028" variety="S0028">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S0028_7B9FA_T00OY" variety="T00OY">
<name>INSTRUMENT PANEL SAFETY PAD</name>
<para id="RM0000046J600CX" category="A" type-id="30014" name-id="IT2BA-03" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM0000046J600CX_02" type-id="11" category="10" proc-id="RM23G0E___0000HXT00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0CLX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000046J600CX_01" type-id="01" category="01">
<s-1 id="RM0000046J600CX_01_0001" proc-id="RM23G0E___0000B9P00000">
<ptxt>INSTALL INSTRUMENT PANEL SAFETY PAD SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<s3>
<ptxt>Attach the 2 guides to install the instrument panel safety pad.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 brackets with the 4 bolts and 2 nuts.</ptxt>
</s3>
<s3>
<ptxt>Connect the connectors and attach the clamps and claws.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 passenger airbag bolts &lt;G&gt;.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Install the 6 bolts &lt;E&gt; and nut &lt;F&gt;.</ptxt>
</s3>
<s3>
<ptxt>Install the front floor carpet.</ptxt>
<figure>
<graphic graphicname="B238225E01" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt &lt;E&gt;</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nut &lt;F&gt;</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt &lt;G&gt;</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Guide</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bracket</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<s3>
<ptxt>Attach the 2 guides to install the instrument panel safety pad.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 brackets with the 4 bolts and 2 nuts.</ptxt>
</s3>
<s3>
<ptxt>Connect the connectors and attach the clamps and claws.</ptxt>
</s3>
<s3>
<ptxt>Install the 2 passenger airbag bolts &lt;G&gt;.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Install the 6 bolts &lt;E&gt; and nut &lt;F&gt;.</ptxt>
</s3>
<s3>
<ptxt>Install the front floor carpet.</ptxt>
<figure>
<graphic graphicname="B238240E01" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt &lt;E&gt;</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nut &lt;F&gt;</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt &lt;G&gt;</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Guide</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bracket</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0055" proc-id="RM23G0E___0000BZU00000">
<ptxt>INSTALL FRONT NO. 3 SPEAKER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and claw to install the front No. 3 speaker.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0056" proc-id="RM23G0E___0000C0900000">
<ptxt>INSTALL FRONT NO. 2 SPEAKER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Align the speaker with the positioning pins of the instrument panel and temporarily install the speaker.</ptxt>
</s2>
<s2>
<ptxt>Install the front No. 2 speaker with the 2 bolts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</item>
<item>
<ptxt>When installing the speaker to the instrument panel, be careful that the wires do not get caught between parts.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0005" proc-id="RM23G0E___0000C0C00000">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL SPEAKER PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 clips, claw and 2 guides to install the No. 1 instrument panel speaker panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0006" proc-id="RM23G0E___0000HXF00000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL SPEAKER PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the No. 1 instrument panel speaker panel.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0007" proc-id="RM23G0E___0000HXG00000">
<ptxt>INSTALL NO. 1 INSTRUMENT PANEL REGISTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 clips to install the No. 1 instrument panel register.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0008" proc-id="RM23G0E___0000HXH00000">
<ptxt>INSTALL NO. 2 INSTRUMENT PANEL REGISTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the No. 1 instrument panel register.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0063" proc-id="RM23G0E___0000C0800000">
<ptxt>INSTALL FRONT NO. 4 SPEAKER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the speaker connector.</ptxt>
</s2>
<s2>
<ptxt>Align the speaker with the positioning pins of the instrument panel and temporarily install the speaker.</ptxt>
</s2>
<s2>
<ptxt>Install the front No. 4 speaker with the 2 bolts.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</item>
<item>
<ptxt>When installing the speaker to the instrument panel, be careful that the wires do not get caught between parts.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0010" proc-id="RM23G0E___0000C0D00000">
<ptxt>INSTALL UPPER INSTRUMENT CLUSTER FINISH PANEL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 clips and 2 guides to install the upper instrument cluster finish panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0011" proc-id="RM23G0E___0000CKZ00000">
<ptxt>INSTALL RADIO RECEIVER ASSEMBLY (w/ Audio)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Upper Side:</ptxt>
<ptxt>Install the radio receiver assembly (See page <xref label="Seep01" href="RM000003AHX01LX_01_0003"/>).</ptxt>
</s2>
<s2>
<ptxt>for Lower Side:</ptxt>
<ptxt>Install the radio receiver assembly (See page <xref label="Seep02" href="RM000003AHX01LX_01_0032"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0012" proc-id="RM23G0E___0000HXI00000">
<ptxt>INSTALL LOWER CENTER INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY (w/o Audio)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 clips and guide to install the lower center instrument cluster finish panel.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts &lt;E&gt;.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0057" proc-id="RM23G0E___0000CL500000">
<ptxt>INSTALL ACCESSORY METER ASSEMBLY (w/ Display, w/o Navigation System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
<figure>
<graphic graphicname="E200492" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Insert the accessory meter and attach the 10 clips on its backside.</ptxt>
</s2>
<s2>
<ptxt>Install the accessory meter with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0058" proc-id="RM23G0E___0000C0B00000">
<ptxt>INSTALL DISPLAY AND NAVIGATION MODULE DISPLAY (w/ Display, w/ Navigation System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connectors.</ptxt>
</s2>
<s2>
<ptxt>Insert the display and navigation module display and attach the 8 clips on the backside of the display and navigation module display.</ptxt>
<figure>
<graphic graphicname="B241764" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>When inserting the display, do not press the knobs on it.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the display and navigation module display with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12.5</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0014" proc-id="RM23G0E___0000CL000000">
<ptxt>INSTALL RADIO TUNER OPENING COVER (w/o Audio)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radio tuner opening cover with the 4 bolts.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0015" proc-id="RM23G0E___0000BY100000">
<ptxt>INSTALL CENTER INSTRUMENT CLUSTER FINISH PANEL ASSEMBLY (w/o Display)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 10 clips to install the center instrument cluster finish panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0059" proc-id="RM23G0E___0000C2J00000">
<ptxt>REMOVE MULTI-MEDIA INTERFACE ECU
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the multi-media interface ECU with the 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 3 connectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0064" proc-id="RM23G0E___000099H00000">
<ptxt>REMOVE 4 WHEEL DRIVE CONTROL ECU
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the four wheel drive control ECU with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 2 connectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0060" proc-id="RM23G0E___0000BGB00000">
<ptxt>INSTALL DRIVING SUPPORT SWITCH CONTROL ECU
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the driving support switch control ECU with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0017" proc-id="RM23G0E___00007BJ00000">
<ptxt>INSTALL GLOVE COMPARTMENT DOOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238219E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Connect each connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 5 clips and claw to install the glove compartment door.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts &lt;C&gt; and 2 screws &lt;A&gt; or &lt;B&gt;.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Screw</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0018" proc-id="RM23G0E___00007BK00000">
<ptxt>INSTALL INSTRUMENT PANEL ORNAMENT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 clips to install the instrument panel ornament.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0019" proc-id="RM23G0E___00008CA00000">
<ptxt>INSTALL INSTRUMENT SIDE PANEL RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 5 clips, claw and 3 guides to install the instrument side panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0061" proc-id="RM23G0E___0000F9U00000">
<ptxt>INSTALL COMBINATION METER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Install the combination meter with the 4 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0048" proc-id="RM23G0E___0000F3A00000">
<ptxt>INSTALL INSTRUMENT CLUSTER FINISH PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws, 2 clips and 2 guides to install the instrument cluster finish panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0062" proc-id="RM23G0E___00008C500000">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL AIRBAG ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
<figure>
<graphic graphicname="B238328" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>When handling the airbag connector, take care not to damage the airbag wire harness.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the airbag assembly with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0021" proc-id="RM23G0E___00008C600000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL FINISH PANEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect each connector and each cable.</ptxt>
</s2>
<s2>
<ptxt>w/o Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 7 clips to install the lower instrument panel finish panel.</ptxt>
</s3>
</s2>
<s2>
<ptxt>w/ Knee Airbag:</ptxt>
<s3>
<ptxt>Attach the 14 clips to install the lower instrument panel finish panel.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install the 2 bolts &lt;C&gt;.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 claws to close the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0022" proc-id="RM23G0E___00008C700000">
<ptxt>INSTALL LOWER INSTRUMENT PANEL FINISH PANEL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect each connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 clips to install the instrument panel finish panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0023" proc-id="RM23G0E___00008C800000">
<ptxt>INSTALL INSTRUMENT PANEL FINISH PLATE GARNISH (for RHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect each connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 clips to install the instrument panel finish plate garnish.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0024" proc-id="RM23G0E___0000CL300000">
<ptxt>INSTALL INSTRUMENT CLUSTER FINISH PANEL ORNAMENT (for LHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 clips to install the instrument cluster finish panel ornament.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0025" proc-id="RM23G0E___00008C900000">
<ptxt>INSTALL INSTRUMENT SIDE PANEL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 5 clips, claw and 3 guides to install the instrument side panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0039" proc-id="RM23G0E___0000BZV00000">
<ptxt>INSTALL FRONT PILLAR GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 guides to install the front pillar garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0027" proc-id="RM23G0E___0000HXJ00000">
<ptxt>INSTALL FRONT PILLAR GARNISH RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0040" proc-id="RM23G0E___0000BZW00000">
<ptxt>INSTALL NO. 1 ASSIST GRIP
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for the other No. 1 assist grip.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the No. 1 assist grip.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0041" proc-id="RM23G0E___0000BZY00000">
<ptxt>INSTALL NO. 1 FRONT ASSIST GRIP PLUG LH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure for the other front No. 1 assist grip plug.</ptxt>
</atten4>
<s2>
<ptxt>Attach the 2 claws to install the front No. 1 assist grip plug.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0030" proc-id="RM23G0E___0000HXK00000">
<ptxt>INSTALL NO. 1 FRONT ASSIST GRIP PLUG RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0042" proc-id="RM23G0E___0000C4V00000">
<ptxt>INSTALL FRONT DOOR OPENING TRIM WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238779E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Align the paint mark on the front door opening trim weatherstrip with the mark position on the vehicle and install the front door opening trim weatherstrip as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark Position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0032" proc-id="RM23G0E___0000HXL00000">
<ptxt>INSTALL FRONT DOOR OPENING TRIM WEATHERSTRIP RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0043" proc-id="RM23G0E___00008CC00000">
<ptxt>INSTALL COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the clip and claw to install the cowl side trim board.</ptxt>
</s2>
<s2>
<ptxt>Install the clip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0034" proc-id="RM23G0E___0000ENF00000">
<ptxt>INSTALL COWL SIDE TRIM BOARD RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0044" proc-id="RM23G0E___00008CE00000">
<ptxt>INSTALL DOOR SCUFF PLATE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 clips, 10 claws and 2 guides to install the door scuff plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046J600CX_01_0036" proc-id="RM23G0E___0000ENG00000">
<ptxt>INSTALL DOOR SCUFF PLATE ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0037" proc-id="RM23G0E___0000HXM00000">
<ptxt>INSTALL REAR CONSOLE BOX ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Automatic Transmission:</ptxt>
<ptxt>Install the rear console box assembly (See page <xref label="Seep01" href="RM0000046JD004X"/>).</ptxt>
</s2>
<s2>
<ptxt>for Manual Transmission:</ptxt>
<ptxt>Install the rear console box assembly (See page <xref label="Seep02" href="RM0000046N6003X"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Refrigerated Cool Box:</ptxt>
<ptxt>Install the rear console box assembly (See page <xref label="Seep03" href="RM0000046MQ009X"/>).</ptxt>
</s2>
<s2>
<ptxt>for Bench Seat Type:</ptxt>
<ptxt>Install the rear console box assembly (See page <xref label="Seep04" href="RM000004ZXW001X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0051" proc-id="RM23G0E___0000HXQ00000">
<ptxt>INSTALL FRONT FLOOR FOOTREST</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Install the front floor footrest (See page <xref label="Seep01" href="RM0000046JG00FX_01_0001"/>).</ptxt>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<ptxt>Install the front floor footrest (See page <xref label="Seep02" href="RM0000046JG00FX_01_0002"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0052" proc-id="RM23G0E___0000HXR00000">
<ptxt>INSTALL FRONT SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Manual Seat:</ptxt>
<ptxt>Install the front seat assembly (See page <xref label="Seep01" href="RM00000465W00GX"/>).</ptxt>
</s2>
<s2>
<ptxt>for Power Seat:</ptxt>
<ptxt>Install the front seat assembly (See page <xref label="Seep02" href="RM00000466Z00HX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0053" proc-id="RM23G0E___0000HXS00000">
<ptxt>INSTALL FRONT SEAT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Manual Seat:</ptxt>
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>for Power Seat:</ptxt>
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>for Walk in Seat Type:</ptxt>
<ptxt>Install the front seat assembly (See page <xref label="Seep01" href="RM0000046B6005X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0038" proc-id="RM23G0E___0000HXN00000">
<ptxt>INSTALL HEADLIGHT DIMMER SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the headlight dimmer switch assembly (See page <xref label="Seep01" href="RM000003QJI00SX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0045" proc-id="RM23G0E___0000HXO00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Reset the AUTO TILT AWAY function setting to the previous condition by changing the customize parameter (See page <xref label="Seep02" href="RM000004KI400CX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000046J600CX_01_0046" proc-id="RM23G0E___0000HXP00000">
<ptxt>CHECK SRS WARNING LIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the SRS warning light (See page <xref label="Seep01" href="RM000000XFD0GRX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>