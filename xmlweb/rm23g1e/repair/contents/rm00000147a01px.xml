<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1KD-FTV ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7B8XB_T006Z" variety="T006Z">
<name>TIMING BELT</name>
<para id="RM00000147A01PX" category="G" type-id="3001K" name-id="EM5H0-02" from="201207">
<name>INSPECTION</name>
<subpara id="RM00000147A01PX_01" type-id="01" category="01">
<s-1 id="RM00000147A01PX_01_0001" proc-id="RM23G0E___00004CG00000">
<ptxt>INSPECT TIMING BELT</ptxt>
<content1 releasenbr="1">
<atten3>
<figure>
<graphic graphicname="A054953E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>OIL</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>WATER</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Do not bend or twist the timing belt, and do not turn the timing belt inside out.</ptxt>
</item>
<item>
<ptxt>Do not allow the timing belt to come into contact with oil, water or steam.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Visually inspect the timing belt.</ptxt>
<ptxt>If defects are present, perform the steps below.</ptxt>
</s2>
<s2>
<ptxt>If there is premature parting:</ptxt>
<s3>
<ptxt>Check for proper installation.</ptxt>
</s3>
<s3>
<ptxt>Check the timing cover gasket for damage and proper installation.</ptxt>
</s3>
</s2>
<s2>
<ptxt>If the belt teeth are cracked or damaged, check if either camshaft is locked.</ptxt>
</s2>
<s2>
<ptxt>If there is noticeable wear or cracks on the belt face, check if there are nicks on the side of the idler pulley lock and water pump.</ptxt>
</s2>
<s2>
<ptxt>If there is wear or damage on only one side of the belt, check the belt guide and the alignment of each pulley.</ptxt>
</s2>
<s2>
<ptxt>If there is noticeable wear on the belt teeth, check the timing belt cover for damage, check that the gasket is installed correctly and check for foreign matter on the pulley teeth.</ptxt>
<ptxt>If necessary, replace the timing belt.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147A01PX_01_0002" proc-id="RM23G0E___00004CH00000">
<ptxt>INSPECT NO. 1 TIMING BELT IDLER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the idler pulley turns smoothly by hand.</ptxt>
<figure>
<graphic graphicname="P022783E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Visually check that the seal portion of the idler pulley has no grease on it.</ptxt>
<figure>
<graphic graphicname="P022782E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If there is oil, water or other foreign matter on the timing belt or a pulley, repair the leak.</ptxt>
</item>
<item>
<ptxt>Do not allow the timing belt and each pulley to come into contact with oil, water or steam.</ptxt>
</item>
<item>
<ptxt>Before installing the idler pulley, be sure to clean each pulley with a cloth or equivalent.</ptxt>
</item>
<item>
<ptxt>When inspecting the No. 1 timing belt idler, do not remove it unless absolutely necessary.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147A01PX_01_0003" proc-id="RM23G0E___00004CI00000">
<ptxt>INSPECT NO. 1 TIMING BELT TENSIONER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Visually check the seal portion of the tensioner for oil leakage.</ptxt>
<figure>
<graphic graphicname="P022742E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If leakage is found, replace the tensioner.</ptxt>
<atten4>
<ptxt>A trace of oil on the tensioner push rod side's seal is acceptable.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Hold the tensioner with both hands as shown in the illustration. Firmly push the push rod and check that it does not move.</ptxt>
<figure>
<graphic graphicname="P022690E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the push rod moves, replace the tensioner.</ptxt>
<atten3>
<ptxt>Never point the tensioner push rod downward.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Measure the protrusion distance of the push rod from the housing end.</ptxt>
<figure>
<graphic graphicname="P022743E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard protrusion distance</title>
<specitem>
<ptxt>8.1 to 8.9 mm (0.319 to 0.350 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion Distance</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the protrusion distance is not as specified, replace the tensioner.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>