<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B99G_T00J4" variety="T00J4">
<name>LUGGAGE SPEAKER (for 3 Door)</name>
<para id="RM000003FS301NX" category="A" type-id="80001" name-id="AVBKE-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM000003FS301NX_02" type-id="11" category="10" proc-id="RM23G0E___0000C5O00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003FS301NX_01" type-id="01" category="01">
<s-1 id="RM000003FS301NX_01_0062" proc-id="RM23G0E___0000C5N00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work. (See page <xref label="Seep02" href="RM000003YMD00GX"/>)</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003FS301NX_01_0061" proc-id="RM23G0E___0000C5M00000">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 60/40 Split Tumble Seat Type LH Side:</ptxt>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM0000046ZI003X"/>).</ptxt>
</s2>
<s2>
<ptxt>for 60/40 Split Tumble Seat Type RH Side</ptxt>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep02" href="RM0000046ZN003X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003FS301NX_01_0024" proc-id="RM23G0E___00008CL00000">
<ptxt>REMOVE DOOR SCUFF PLATE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238732E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the door scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 4 clips, 10 claws and 2 guides and remove the door scuff plate.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0055" proc-id="RM23G0E___0000C5G00000">
<ptxt>REMOVE FRONT DOOR OPENING TRIM WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238778" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the front door opening trim weatherstrip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0025" proc-id="RM23G0E___0000C5700000">
<ptxt>REMOVE TONNEAU COVER ASSEMBLY (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the tonneau cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0026" proc-id="RM23G0E___0000C5800000">
<ptxt>REMOVE MAT SET PLATE COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240525E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for all mat set plate covers.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the mat set plate cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0027" proc-id="RM23G0E___00005NQ00000">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240526" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 5 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 6 claws and remove the rear floor mat rear support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0028" proc-id="RM23G0E___0000C5900000">
<ptxt>REMOVE QUARTER TRIM COVER HOLE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240533" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and 2 guides and remove the quarter trim cover hole.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0029" proc-id="RM23G0E___0000C5A00000">
<ptxt>REMOVE REAR FLOOR CARPET ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240527" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the rear floor carpet.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0056" proc-id="RM23G0E___0000C5H00000">
<ptxt>REMOVE NO. 1 LUGGAGE COMPARTMENT TRIM HOOK
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181695" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other No. 1 luggage compartment trim hook.</ptxt>
</atten4>
<s2>
<ptxt>Remove the luggage compartment trim hook by turning it clockwise.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0057" proc-id="RM23G0E___0000C5I00000">
<ptxt>REMOVE NO. 1 TONNEAU COVER HOLDER CAP (w/o Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240528E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other tonneau cover holder cap.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the tonneau cover holder cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0034" proc-id="RM23G0E___0000C5D00000">
<ptxt>REMOVE FRONT DECK SIDE TRIM COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240051E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other front deck side trim cover.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the front deck side trim cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0031" proc-id="RM23G0E___0000C5B00000">
<ptxt>REMOVE QUARTER TRIM POCKET TRAY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240553E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the quarter trim pocket tray.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 6 claws and 2 guides, and remove the quarter trim pocket tray.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0032" proc-id="RM23G0E___0000C5C00000">
<ptxt>REMOVE OUTER LAP BELT ANCHOR COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240529" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other outer lap belt anchor cover.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 3 claws and remove the outer lap belt anchor cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0035" proc-id="RM23G0E___0000C5E00000">
<ptxt>REMOVE DECK TRIM SIDE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240531" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and disconnect the front seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240534" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear No. 1 seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 9 claws and 12 clips and remove the deck trim side panel.</ptxt>
<figure>
<graphic graphicname="B240536" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0058" proc-id="RM23G0E___0000C5J00000">
<ptxt>REMOVE REAR QUARTER TRIM PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240540" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 7 clips.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240541" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 4 claws and 2 guides, pass the rear No. 1 seat belt floor anchor through the rear quarter trim panel and remove the rear quarter trim panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0003" proc-id="RM23G0E___0000C5600000">
<ptxt>REMOVE REAR HEADER SPEAKER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B239159" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and rear header speaker.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003FS301NX_01_0030" proc-id="RM23G0E___0000C4K00000">
<ptxt>REMOVE DOOR SCUFF PLATE ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0059" proc-id="RM23G0E___0000C5K00000">
<ptxt>REMOVE FRONT DOOR OPENING TRIM WEATHERSTRIP RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0060" proc-id="RM23G0E___0000C5L00000">
<ptxt>REMOVE QUARTER TRIM COVER HOLE RH
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0036" proc-id="RM23G0E___0000C5C00000">
<ptxt>REMOVE OUTER LAP BELT ANCHOR COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240529" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other outer lap belt anchor cover.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 3 claws and remove the outer lap belt anchor cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0040" proc-id="RM23G0E___0000C5F00000">
<ptxt>REMOVE DECK TRIM SIDE PANEL ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240532" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and disconnect the front seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240535" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear No. 1 seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 9 claws and 12 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the deck trim side panel.</ptxt>
<figure>
<graphic graphicname="B240537" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003FS301NX_01_0002" proc-id="RM23G0E___0000C5500000">
<ptxt>REMOVE NO. 1 SPEAKER WITH BOX ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B239106" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 bolts and No. 1 speaker with box.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>