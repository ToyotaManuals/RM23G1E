<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000000XIQ0ELX" category="J" type-id="300NF" name-id="BC8EQ-06" from="201207" to="201210">
<dtccode/>
<dtcname>ABS Warning Light does not Come ON</dtcname>
<subpara id="RM000000XIQ0ELX_04" type-id="60" category="03" proc-id="RM23G0E___0000A1A00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU is connected to the combination meter via CAN communication.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XIQ0ELX_01" type-id="32" category="03" proc-id="RM23G0E___0000A1700000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to ABS Warning Light Remains ON (See page <xref label="Seep01" href="RM000000XIH0EOX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XIQ0ELX_02" type-id="51" category="05" proc-id="RM23G0E___0000A1800000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08IX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XIQ0ELX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIQ0ELX_03_0013" proc-id="RM23G0E___0000A1200000">
<testtitle>CHECK CAN COMMUNICATION SYSTEM
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if CAN communication system DTCs are output (for LHD without Entry and Start System: See page <xref label="Seep01" href="RM000000WIB0BRX"/>, for RHD without Entry and Start System: See page <xref label="Seep02" href="RM000000WIB0BTX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.43in"/>
<colspec colname="COLSPEC0" colwidth="1.64in"/>
<colspec colname="COL2" colwidth="1.06in"/>
<thead>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>DTC not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XIQ0ELX_03_0003" fin="false">A</down>
<right ref="RM000000XIQ0ELX_03_0006" fin="true">B</right>
<right ref="RM000000XIQ0ELX_03_0011" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000XIQ0ELX_03_0003" proc-id="RM23G0E___0000A1900000">
<testtitle>CHECK ABS WARNING LIGHT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check that the ABS warning light comes on.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The ABS warning light comes on.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XIQ0ELX_03_0008" fin="true">OK</down>
<right ref="RM000000XIQ0ELX_03_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIQ0ELX_03_0012" proc-id="RM23G0E___0000A1400000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ABS WARNING LIGHT)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
</test1>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="2.26in"/>
<colspec colname="COL3" colwidth="1.60in"/>
<colspec colname="COL4" colwidth="1.45in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ABS warning light / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<test1>
<ptxt>When performing the ABS Warning Light Active Test, check ABS Warning Light in the Data List (See Page <xref label="Seep02" href="RM000000XHW0BWX"/>).</ptxt>
</test1>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ABS Warning Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ABS warning light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Warning light ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Observe the combination meter.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.69in"/>
<colspec colname="COL2" colwidth="3.91in"/>
<colspec colname="COL3" colwidth="1.48in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Data List Display</ptxt>
</entry>
<entry>
<ptxt>Data List Display when Performing Active Test ON/OFF Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not change between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Does not change between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Changes between ON and OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6><res>
<down ref="RM000000XIQ0ELX_03_0008" fin="true">A</down>
<right ref="RM000000XIQ0ELX_03_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XIQ0ELX_03_0006">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO07CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIQ0ELX_03_0011">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO07EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIQ0ELX_03_0008">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIQ0ELX_03_0009">
<testtitle>GO TO METER / GAUGE SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000002Z4L031X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>