<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12027_S001E" variety="S001E">
<name>REAR SUSPENSION</name>
<ttl id="12027_S001E_7B97D_T00H1" variety="T00H1">
<name>REAR PNEUMATIC CYLINDER (w/ Air Suspension)</name>
<para id="RM000002CKK00IX" category="A" type-id="30014" name-id="RP0NT-01" from="201207">
<name>INSTALLATION</name>
<subpara id="RM000002CKK00IX_02" type-id="11" category="10" proc-id="RM23G0E___00009ZJ00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>A bolt without a torque specification is shown in the standard bolt chart (See page <xref label="Seep01" href="RM00000118W0CLX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000002CKK00IX_01" type-id="01" category="01">
<s-1 id="RM000002CKK00IX_01_0001" proc-id="RM23G0E___00009ZG00000">
<ptxt>INSTALL REAR PNEUMATIC CYLINDER ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the pneumatic cylinder assembly with the clip.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clip</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="C214803E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure that the clip is installed securely to the cylinder.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the height control tube (See page <xref label="Seep01" href="RM000001U5400UX"/>).</ptxt>
</s2>
<s2>
<ptxt>When using safety stands and a jack:</ptxt>
<figure>
<graphic graphicname="C214804E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s3>
<ptxt>Jack up the rear axle assembly until the bottom of the pneumatic cylinder assembly touches the rear axle assembly and install the pin at the lower side of the pneumatic cylinder assembly to the hole in the rear axle housing.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not extend the pneumatic cylinder assembly for pin installation.</ptxt>
</item>
<item>
<ptxt>Make sure that the pin at the lower side of the pneumatic cylinder assembly fits in the hole in the rear axle housing.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Temporarily install the nut.</ptxt>
<figure>
<graphic graphicname="C214771E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Align the matchmarks on the rear pneumatic cylinder with the matchmarks on the rear axle housing.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Remove the safety stands with care not to extend the pneumatic cylinder assembly when lowering the vehicle until it sits on the ground.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not extend the pneumatic cylinder assembly by lowering the rear axle assembly with safety stands installed.</ptxt>
</item>
<item>
<ptxt>Make sure that the diaphragm of the pneumatic cylinder assembly is not deformed.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
<s2>
<ptxt>When using a swing arm type lift:</ptxt>
<s3>
<ptxt>Lower the vehicle until the tires contact the ground, and continue lowering the vehicle slowly until the bottom of the pneumatic cylinder assembly touches the rear axle housing.</ptxt>
</s3>
<s3>
<ptxt>Align the seating pin on the cylinder with the hole in the rear axle assembly and install the pneumatic cylinder assembly to the rear axle housing.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not extend the pneumatic cylinder assembly for pin installation.</ptxt>
</item>
<item>
<ptxt>Make sure that the pin at the lower side of the pneumatic cylinder assembly fits in the hole in the rear axle housing.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Temporarily install the nut.</ptxt>
<figure>
<graphic graphicname="C214771E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Align the matchmarks on the rear pneumatic cylinder with the matchmarks on the rear axle housing.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Lower the lift carefully so as not to extend the pneumatic cylinder assembly.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not extend the pneumatic cylinder assembly by lifting down the rear axle housing after installing the pin.</ptxt>
</item>
<item>
<ptxt>Make sure that the diaphragm of the pneumatic cylinder assembly is not deformed.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Start the engine and replenish the pneumatic cylinder assembly with air.</ptxt>
</s2>
<s2>
<ptxt>Tighten the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000002CKK00IX_01_0002" proc-id="RM23G0E___00009ZH00000">
<ptxt>INSPECT FOR AIR LEAK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect for air leaks (See page <xref label="Seep01" href="RM000001U5400UX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002CKK00IX_01_0003" proc-id="RM23G0E___00009ZI00000">
<ptxt>INSPECT VEHICLE HEIGHT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the vehicle height (See page <xref label="Seep01" href="RM0000038UA00SX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>