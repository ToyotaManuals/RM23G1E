<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000B" variety="S000B">
<name>5L-E ENGINE MECHANICAL</name>
<ttl id="12007_S000B_7B8XO_T007C" variety="T007C">
<name>CYLINDER HEAD</name>
<para id="RM000001LNE00BX" category="A" type-id="30019" name-id="EM66U-02" from="201207">
<name>REPLACEMENT</name>
<subpara id="RM000001LNE00BX_01" type-id="01" category="01">
<s-1 id="RM000001LNE00BX_01_0001" proc-id="RM23G0E___00004S100000">
<ptxt>REPLACE INTAKE VALVE GUIDE BUSH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Gradually heat the cylinder head to approximately 80 to 100°C (176 to 212°F).</ptxt>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap out the valve guide bush.</ptxt>
<figure>
<graphic graphicname="A057248E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09201-10000</s-number>
<s-subnumber>09201-01080</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using a caliper gauge, measure the bush bore diameter of the cylinder head.</ptxt>
</s2>
<s2>
<ptxt>Select a new guide bush.</ptxt>
<spec>
<title>New Guide Bush</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="center" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry namest="COL2" nameend="COL3" align="center">
<ptxt>Specified Conditions</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Bush Bore Diameter</ptxt>
</entry>
<entry align="center">
<ptxt>13.004 to 13.025 mm (0.512 to 0.513 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>13.054 to 13.075 mm (0.514 to 0.515 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Bush to be Used</ptxt>
</entry>
<entry align="center">
<ptxt>STD</ptxt>
</entry>
<entry align="center">
<ptxt>O/S 0.05</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the bush bore diameter of the cylinder head is more than 13.025 mm (0.513 in.), machine the bush bore to a dimension of 13.054 to 13.075 mm (0.514 to 0.515 in.).</ptxt>
<ptxt>If the bush bore diameter of the cylinder head is more than 13.075 mm (0.515 in.), replace the cylinder head.</ptxt>
</s2>
<s2>
<ptxt>Gradually heat the cylinder head to approximately 80 to 100°C (176 to 212°F).  </ptxt>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap in a new guide bush to the specified protrusion height.</ptxt>
<figure>
<graphic graphicname="A070793E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
<sstitem>
<s-number>09201-10000</s-number>
<s-subnumber>09201-01070</s-subnumber>
</sstitem>
</sst>
<spec>
<title>Standard protrusion height</title>
<specitem>
<ptxt>10.8 to 11.2 mm (0.425 to 0.441 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion Height</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a sharp 8.0 mm reamer, ream the guide bush to obtain the specified clearance (See page <xref label="Seep01" href="RM000001LND00BX_01_0008"/>).</ptxt>
<figure>
<graphic graphicname="A056822E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reamer</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000001LNE00BX_01_0002" proc-id="RM23G0E___00004S200000">
<ptxt>REPLACE EXHAUST VALVE GUIDE BUSH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Gradually heat the cylinder head to approximately 80 to 100°C (176 to 212°F).</ptxt>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap out the valve guide bush.</ptxt>
<sst>
<sstitem>
<s-number>09201-10000</s-number>
<s-subnumber>09201-01080</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="A057249E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a caliper gauge, measure the bush bore diameter of the cylinder head.</ptxt>
</s2>
<s2>
<ptxt>Select a new guide bush.</ptxt>
<spec>
<title>New Guide Bush</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" align="center" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry namest="COL2" nameend="COL3" align="center">
<ptxt>Specified Conditions</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Bush Bore Diameter</ptxt>
</entry>
<entry align="center">
<ptxt>13.004 to 13.025 mm (0.512 to 0.513 in.)</ptxt>
</entry>
<entry align="center">
<ptxt>13.054 to 13.075 mm (0.514 to 0.515 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Bush to be Used</ptxt>
</entry>
<entry align="center">
<ptxt>STD</ptxt>
</entry>
<entry align="center">
<ptxt>O/S 0.05</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the bush bore diameter of the cylinder head is more than 13.025 mm (0.513 in.), machine the bush bore to a dimension of 13.054 to 13.075 mm (0.514 to 0.515 in.).</ptxt>
<ptxt>If the bush bore diameter of the cylinder head is more than 13.075 mm (0.515 in.), replace the cylinder head.</ptxt>
</s2>
<s2>
<ptxt>Gradually heat the cylinder head to approximately 80 to 100°C (176 to 212°F).  </ptxt>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap in a new guide bush to the specified protrusion height.</ptxt>
<figure>
<graphic graphicname="A070794E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
<sstitem>
<s-number>09201-10000</s-number>
<s-subnumber>09201-01070</s-subnumber>
</sstitem>
</sst>
<spec>
<title>Standard protrusion height</title>
<specitem>
<ptxt>10.8 mm to 11.2 mm (0.425 to 0.441 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion Height</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a sharp 8.0 mm reamer, ream the guide bush to obtain the specified clearance (See page <xref label="Seep01" href="RM000001LND00BX_01_0009"/>).</ptxt>
<figure>
<graphic graphicname="A056823E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reamer</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000001LNE00BX_01_0007" proc-id="RM23G0E___00004S300000">
<ptxt>REPLACE TIGHT PLUG</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>If coolant leaks from the tight plug or the plug is corroded, replace it.</ptxt>
</atten3>
<s2>
<ptxt>Apply adhesive to new tight plugs.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1324, Three Bond 1324 or equivalent.</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install the tight plugs as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A071262E05" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Head Cover Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Manifold Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stops</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using SST, tap in the 7 tight plugs labeled A.</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00250</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST, tap in the tight plug labeled B.</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00300</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Using SST, tap in the 2 tight plugs labeled C.</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00450</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
</s2>
</content1>
</s-1>
<s-1 id="RM000001LNE00BX_01_0008" proc-id="RM23G0E___00004S400000">
<ptxt>REPLACE RING PIN</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>It is not necessary to remove a ring pin unless it is being replaced.</ptxt>
</atten3>
<s2>
<ptxt>Using a plastic-faced hammer, tap in a new ring pin until it stops.</ptxt>
<figure>
<graphic graphicname="A071264E06" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Height</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diameter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Head Cover Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Until pin stop</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Standard Ring Pin</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Height</ptxt>
</entry>
<entry>
<ptxt>Diameter</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Ring pin</ptxt>
</entry>
<entry>
<ptxt>8 mm (0.315 in.)</ptxt>
</entry>
<entry>
<ptxt>11 mm (0.433 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>