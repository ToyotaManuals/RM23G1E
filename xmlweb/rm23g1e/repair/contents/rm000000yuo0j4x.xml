<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97M_T00HA" variety="T00HA">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Vacuum Brake Booster)</name>
<para id="RM000000YUO0J4X" category="C" type-id="803OS" name-id="BC8KC-04" from="201207" to="201210">
<dtccode>C1437</dtccode>
<dtcname>Lost Communication with ECM</dtcname>
<subpara id="RM000000YUO0J4X_01" type-id="60" category="03" proc-id="RM23G0E___0000AP200000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU receives signals from the ECM via the CAN communication system.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1437</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the IG1 terminal voltage is 10 V or higher and the vehicle speed is 15 km/h (9 mph) or more, data cannot be sent to the ECM for 2 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN communication system (Skid control ECU to ECM)</ptxt>
</item>
<item>
<ptxt>Brake actuator assembly (Skid control ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000YUO0J4X_03" type-id="51" category="05" proc-id="RM23G0E___0000AP300000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08JX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000YUO0J4X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YUO0J4X_04_0001" proc-id="RM23G0E___0000AP400000">
<testtitle>CHECK HARNESS AND CONNECTOR (MOMENTARY INTERRUPTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, check for any momentary interruption in the wire harness and connector corresponding to the DTC (See page <xref label="Seep01" href="RM000000XHS0CIX"/>).</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.18in"/>
<colspec colname="COL2" colwidth="3.13in"/>
<colspec colname="COL3" colwidth="1.48in"/>
<colspec colname="COLSPEC0" colwidth="1.29in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>EFI Communication Open</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM communication open circuit detection/ Error or Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Normal (there are no momentary interruptions).</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Perform the above inspection before removing the sensor and disconnecting the connector.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000YUO0J4X_04_0002" fin="false">OK</down>
<right ref="RM000000YUO0J4X_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000YUO0J4X_04_0002" proc-id="RM23G0E___0000AP500000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000000XHV0F0X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 15 km/h (9 mph) or more for 10 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Check that no CAN communication system DTC is output (See page <xref label="Seep02" href="RM000000XHV0F0X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep03" href="RM000000XHV0F0X"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.35in"/>
<colspec colname="COL2" colwidth="2.65in"/>
<colspec colname="COL3" colwidth="1.08in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>DTC C1437 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>DTC C1437 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YUO0J4X_04_0010" fin="true">A</down>
<right ref="RM000000YUO0J4X_04_0011" fin="true">B</right>
<right ref="RM000000YUO0J4X_04_0005" fin="true">C</right>
<right ref="RM000000YUO0J4X_04_0012" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000000YUO0J4X_04_0003" proc-id="RM23G0E___0000AP600000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (SKID CONTROL ECU - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
<test1>
<ptxt>Check for any momentary interruption between the skid control ECU and ECM (See page <xref label="Seep01" href="RM000000XHS0CIX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check that there is no momentary interruption.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000YUO0J4X_04_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000YUO0J4X_04_0004" proc-id="RM23G0E___0000AP700000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0F0X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 15 km/h (9 mph) or more for 10 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Check that no CAN communication system DTC is output (See page <xref label="Seep02" href="RM000000XHV0F0X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep03" href="RM000000XHV0F0X"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.44in"/>
<colspec colname="COL2" colwidth="2.52in"/>
<colspec colname="COL3" colwidth="1.12in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>DTC C1437 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>DTC C1437 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAN communication system DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YUO0J4X_04_0007" fin="true">A</down>
<right ref="RM000000YUO0J4X_04_0011" fin="true">B</right>
<right ref="RM000000YUO0J4X_04_0005" fin="true">C</right>
<right ref="RM000000YUO0J4X_04_0012" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000000YUO0J4X_04_0010">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUO0J4X_04_0007">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000YUO0J4X_04_0011">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01JX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUO0J4X_04_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO07CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUO0J4X_04_0012">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO07EX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>