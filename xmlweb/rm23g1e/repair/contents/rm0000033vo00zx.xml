<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B990_T00IO" variety="T00IO">
<name>REAR SEAT ENTERTAINMENT SYSTEM</name>
<para id="RM0000033VO00ZX" category="U" type-id="3001G" name-id="AV70S-04" from="201207">
<name>TERMINALS OF ECU</name>
<subpara id="RM0000033VO00ZX_z0" proc-id="RM23G0E___0000BQX00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK TELEVISION DISPLAY ASSEMBLY</ptxt>
<figure>
<graphic graphicname="E191325E06" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Measure the voltage and resistance and check for pulses according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>W14-1 (MI+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>MOST communication signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-2 (MI-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>W</ptxt>
</entry>
<entry valign="middle">
<ptxt>MOST communication signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-3 (SLDI) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shield ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-4 (SLDO) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shield ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-5 (MO-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>R</ptxt>
</entry>
<entry valign="middle">
<ptxt>MOST communication signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-6 (MO+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>G</ptxt>
</entry>
<entry valign="middle">
<ptxt>MOST communication signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>W14-7 (WUI) - W14-10 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - BR</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>MOST communication wake up signal</ptxt>
<ptxt>(Input)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-9 (VMTR) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mute signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE playing → Source changed</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 V or higher → Below 0.7 V → 4V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-10 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-11 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-14 (+B2) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>SB - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-15 (HP1L) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE sound signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE playing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform synchronized with sound is output</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-16 (SLD1) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shielded - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shield ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-17 (HP1R) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE sound signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE playing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform synchronized with sound is output</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-18 (HP2L) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE sound signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE playing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform synchronized with sound is output</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-19 (SLD2) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shielded - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shield ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-20 (HP2R) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Y - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE sound signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE playing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform synchronized with sound is output</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-22 (SGN1) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>R - BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Display signal ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-23 (NTSC) - W14-22 (SGN1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>G - R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Display signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE playing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Waveform synchronized with display signals is output</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>W14-28 (+B) - W14-10 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>SB- BR</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>z4-1 (GVIF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Digital communication signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK DISPLAY AND NAVIGATION MODULE DISPLAY (w/ DVD Navigation System) (See page <xref label="Seep01" href="RM0000044WM01QX"/>)</ptxt>
</step1>
<step1>
<ptxt>CHECK DISPLAY AND NAVIGATION MODULE DISPLAY (w/ HDD Navigation System) (See page <xref label="Seep04" href="RM0000044WM01PX"/>)</ptxt>
</step1>
<step1>
<ptxt>CHECK MULTI-MEDIA INTERFACE ECU (See page <xref label="Seep02" href="RM000003YVI01ZX"/>)</ptxt>
</step1>
<step1>
<ptxt>CHECK RADIO RECEIVER ASSEMBLY (See page <xref label="Seep03" href="RM000003YVI01ZX"/>)</ptxt>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>