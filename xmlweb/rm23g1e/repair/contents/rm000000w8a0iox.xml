<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94H_T00E5" variety="T00E5">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1KD-FTV)</name>
<para id="RM000000W8A0IOX" category="C" type-id="302GI" name-id="AT61Z-04" from="201210">
<dtccode>P2716</dtccode>
<dtcname>Pressure Control Solenoid "D" Electrical (Shift Solenoid Valve SLT)</dtcname>
<subpara id="RM000000W8A0IOX_01" type-id="60" category="03" proc-id="RM23G0E___000085300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2714 (See page <xref label="Seep01" href="RM000000W830JJX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2716</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short is detected in the shift solenoid valve SLT circuit for 1 sec. or more while driving (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in shift solenoid valve SLT circuit</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SLT</ptxt>
</item>
<item>
<ptxt>TCM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W8A0IOX_02" type-id="64" category="03" proc-id="RM23G0E___000085400001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When an open or short in the shift solenoid valve SLT circuit is detected, the TCM interprets this as a fault. The TCM will turn on the MIL and store the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W8A0IOX_07" type-id="32" category="03" proc-id="RM23G0E___000085500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C219217E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W8A0IOX_08" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000W8A0IOX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W8A0IOX_09_0001" proc-id="RM23G0E___000085600001">
<testtitle>INSPECT TRANSMISSION WIRE (SHIFT SOLENOID VALVE SLT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C30 transmission wire connector.</ptxt>
<figure>
<graphic graphicname="C214326E07" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>14 (SLT+) - 6 (SLT-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>14 (SLT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>6 (SLT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Transmission Wire)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W8A0IOX_09_0002" fin="false">OK</down>
<right ref="RM000000W8A0IOX_09_0011" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8A0IOX_09_0002" proc-id="RM23G0E___000085700001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSMISSION WIRE - TCM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G69 TCM connector.</ptxt>
<figure>
<graphic graphicname="C212047E09" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>G69-20 (SLT+) - G69-19 (SLT-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>G69-20 (SLT+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>G69-19 (SLT-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to TCM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W8A0IOX_09_0005" fin="true">OK</down>
<right ref="RM000000W8A0IOX_09_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8A0IOX_09_0010">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W8A0IOX_09_0005">
<testtitle>REPLACE TCM<xref label="Seep01" href="RM0000048F300DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8A0IOX_09_0011" proc-id="RM23G0E___000083X00001">
<testtitle>INSPECT SHIFT SOLENOID VALVE SLT
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove shift solenoid valve SLT.</ptxt>
<figure>
<graphic graphicname="C209928E05" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) with a 21 W bulb → Terminal 2</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Terminal 1</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes an operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve SLT)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6><res>
<down ref="RM000000W8A0IOX_09_0009" fin="true">OK</down>
<right ref="RM000000W8A0IOX_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8A0IOX_09_0007">
<testtitle>REPLACE SHIFT SOLENOID VALVE SLT<xref label="Seep01" href="RM0000013FG02LX_01_0005"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8A0IOX_09_0009">
<testtitle>REPAIR OR REPLACE TRANSMISSION WIRE<xref label="Seep01" href="RM0000013C1049X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>