<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9CY_T00MM" variety="T00MM">
<name>FRONT SEAT ASSEMBLY (for Manual Seat)</name>
<para id="RM00000466000LX" category="A" type-id="8000E" name-id="SEAV1-01" from="201210">
<name>REASSEMBLY</name>
<subpara id="RM00000466000LX_02" type-id="11" category="10" proc-id="RM23G0E___0000GC100001">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000466000LX_01" type-id="01" category="01">
<s-1 id="RM00000466000LX_01_0036" proc-id="RM23G0E___0000JUL00000">
<ptxt>INSTALL FRONT SEAT SIDE AIRBAG ASSEMBLY (w/ Front Seat Side Airbag)
</ptxt>
<content1 releasenbr="1">
<atten2>
<list1 type="unordered">
<item>
<ptxt>The nuts must not be reused.</ptxt>
</item>
<item>
<ptxt>Make sure that the separate type front seatback spring assembly is not deformed. If it is, replace it with a new one.</ptxt>
</item>
</list1>
</atten2>
<atten4>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</atten4>
<s2>
<ptxt>Install the front seat side airbag assembly with 2 new nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 2 clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466000LX_01_0027" proc-id="RM23G0E___0000GBT00001">
<ptxt>INSTALL NO. 2 SEAT WIRE (for Driver Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 clamps to install the seat wire.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0001" proc-id="RM23G0E___0000GBB00001">
<ptxt>INSTALL SEPARATE TYPE FRONT SEATBACK PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seatback pad.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0031" proc-id="RM23G0E___0000GBW00001">
<ptxt>INSTALL FRONT SEATBACK HEATER ASSEMBLY LH (w/ Seat Heater System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239208E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Set the seatback heater with the name stamp side facing the seatback cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Heater</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Name Stamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the seatback heater with new tack pins.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466000LX_01_0003" proc-id="RM23G0E___0000GBC00001">
<ptxt>INSTALL SEPARATE TYPE FRONT SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using hog ring pliers, install the seatback cover to the seatback pad with new hog rings.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>The number of hog rings differs according to seat type.</ptxt>
</atten4>
<figure>
<graphic graphicname="B242231E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Type B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Attach the 4 claws to install the 2 headrest supports.</ptxt>
</s2>
<s2>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<figure>
<graphic graphicname="B239202" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Connect the seatback cover bracket to the seatback pad.</ptxt>
</s3>
<s3>
<ptxt>Install the seatback cover bracket to the seat frame with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>After the seatback trim cover is assembled, make sure the side airbag strap is not twisted.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Close the 2 fasteners, and then close the seatback cover.</ptxt>
<figure>
<graphic graphicname="B242233E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Cloth Seat</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Leather Seat</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="B239193E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Using hog ring pliers, install 3 new hog rings.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0029" proc-id="RM23G0E___0000GBV00001">
<ptxt>INSTALL RECLINING ADJUSTER INSIDE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239257" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Move the cover in the direction of the arrow to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Attach the guide.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0028" proc-id="RM23G0E___0000GBU00001">
<ptxt>INSTALL RECLINING ADJUSTER INSIDE COVER RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0004" proc-id="RM23G0E___0000GBD00001">
<ptxt>INSTALL FRONT SEAT WIRE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seat wire.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0032" proc-id="RM23G0E___0000GBX00001">
<ptxt>INSTALL SEAT HEATER CONTROL SUB-ASSEMBLY LH (w/ Seat Heater System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 clamps to install the seat heater control.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466000LX_01_0008" proc-id="RM23G0E___0000GBE00001">
<ptxt>INSTALL FRONT SEAT CUSHION EDGE PROTECTOR LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239189" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Move the protector in the direction of the arrow to attach the hook and install the protector.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0009" proc-id="RM23G0E___0000GBF00001">
<ptxt>INSTALL FRONT SEAT CUSHION EDGE PROTECTOR RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0010" proc-id="RM23G0E___0000GBG00001">
<ptxt>INSTALL FRONT SEAT LOWER CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the hook and clip to install the cushion shield.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0011" proc-id="RM23G0E___0000GBH00001">
<ptxt>INSTALL FRONT SEAT LOWER CUSHION SHIELD RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the hook and clip to install the cushion shield.</ptxt>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0012" proc-id="RM23G0E___0000GBI00001">
<ptxt>INSTALL RECLINING ADJUSTER INSIDE COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw and guide to install the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0033" proc-id="RM23G0E___0000GBY00001">
<ptxt>INSTALL FRONT SEAT CUSHION HEATER ASSEMBLY LH (w/ Seat Heater System)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239185E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Set the seat cushion heater with the name stamp side facing the seat cushion cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tack Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Cover</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seatback Heater</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Name Stamp</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the seat cushion heater with new tack pins.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466000LX_01_0014" proc-id="RM23G0E___0000GBJ00001">
<ptxt>INSTALL SEPARATE TYPE FRONT SEAT CUSHION COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239183E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using hog ring pliers, install the seat cushion cover to the seat cushion pad with new hog rings.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring Pliers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to damage the cover.</ptxt>
</item>
<item>
<ptxt>When installing the hog rings, avoid wrinkling the cover.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0015" proc-id="RM23G0E___0000GBK00001">
<ptxt>INSTALL SEAT CUSHION COVER WITH PAD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seat cushion cover with pad.</ptxt>
</s2>
<s2>
<ptxt>for Front Passenger Side:</ptxt>
<ptxt>Attach the claw to connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the hooks.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Connect the seat heater connector and attach the 3 wire harness clamps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0016" proc-id="RM23G0E___0000GBL00001">
<ptxt>INSTALL FRONT SEAT LEG COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239172" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Move the cover in the direction of the arrow to install the cover.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 claws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0017" proc-id="RM23G0E___0000GBM00001">
<ptxt>INSTALL CAP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 2 caps.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0018" proc-id="RM23G0E___0000GBN00001">
<ptxt>INSTALL SEPARATE TYPE FRONT SEATBACK ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the seatback assembly with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>42</t-value1>
<t-value2>428</t-value2>
<t-value4>31</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>w/ Front Seat Side Airbag:</ptxt>
<s3>
<ptxt>Connect the airbag wire harness.</ptxt>
</s3>
<s3>
<ptxt>Attach the fastening tape to close the cover.</ptxt>
</s3>
<s3>
<ptxt>Attach the 3 airbag wire harness clamps.</ptxt>
</s3>
<s3>
<ptxt>Attach the claw and connect the airbag connector.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for Clip Type:</ptxt>
<ptxt>Install the 2 clips.</ptxt>
</s2>
<s2>
<ptxt>for Hook Type:</ptxt>
<ptxt>Attach the 2 hooks.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Heater System:</ptxt>
<ptxt>Connect the seat heater connector and attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>Attach the clamp to connect the No. 2 seat wire.</ptxt>
</s2>
<s2>
<ptxt>Install the rubber band to the seat cushion spring.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0035" proc-id="RM23G0E___0000GC000001">
<ptxt>INSTALL FRONT SEAT INNER BELT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>(See page <xref label="Seep01" href="RM000003RQL01BX_01_0001"/>)</ptxt>
</s2>
<s2>
<ptxt>for Front Passenger Side:</ptxt>
<ptxt>(See page <xref label="Seep02" href="RM000003RQL01BX_01_0006"/>)</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0021" proc-id="RM23G0E___0000GBO00001">
<ptxt>INSTALL FRONT SEAT INNER CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239168" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Attach the 3 claws to install the cushion shield.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0026" proc-id="RM23G0E___0000GBS00001">
<ptxt>INSTALL FRONT SEAT INNER CUSHION SHIELD RH (for Front Passenger Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 4 claws to install the cushion shield.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0034" proc-id="RM23G0E___0000GBZ00001">
<ptxt>INSTALL LUMBAR SWITCH ASSEMBLY (for Driver Side)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the switch with the 2 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000466000LX_01_0023" proc-id="RM23G0E___0000GBP00001">
<ptxt>INSTALL FRONT SEAT CUSHION SHIELD LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Driver Side:</ptxt>
<ptxt>Attach the 5 wire harness clamps and connect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>Attach the 4 claws and clip to install the cushion shield.</ptxt>
<figure>
<graphic graphicname="B239164" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0024" proc-id="RM23G0E___0000GBQ00001">
<ptxt>INSTALL VERTICAL ADJUSTING HANDLE LH (for Driver Side)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the handle with the screw.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000466000LX_01_0025" proc-id="RM23G0E___0000GBR00001">
<ptxt>INSTALL RECLINING ADJUSTER RELEASE HANDLE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238106" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Attach the claw to install the handle.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>