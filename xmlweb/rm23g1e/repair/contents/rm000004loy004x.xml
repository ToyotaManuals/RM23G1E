<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12009_S000H" variety="S000H">
<name>1KD-FTV EMISSION CONTROL</name>
<ttl id="12009_S000H_7B902_T009Q" variety="T009Q">
<name>EGR COOLER (w/ DPF)</name>
<para id="RM000004LOY004X" category="A" type-id="80001" name-id="EC4K2-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM000004LOY004X_01" type-id="11" category="10" proc-id="RM23G0E___000066E00000">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipes and fuel inlet pipe, clean them with a brush and compressed air.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM000004LOY004X_02" type-id="01" category="01">
<s-1 id="RM000004LOY004X_02_0001" proc-id="RM23G0E___000066F00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0002" proc-id="RM23G0E___000066G00000">
<ptxt>REMOVE DIESEL THROTTLE BODY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM0000013ZD01UX"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0026" proc-id="RM23G0E___000064P00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 13 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004LOY004X_02_0027" proc-id="RM23G0E___00004PR00000">
<ptxt>REMOVE FRONT BUMPER LOWER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper lower cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004LOY004X_02_0028" proc-id="RM23G0E___00004PE00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and No. 1 engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004LOY004X_02_0029" proc-id="RM23G0E___000032G00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A218364" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Drain the coolant by removing the reservoir cap and, using a wrench, remove the vent plug.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug.</ptxt>
<figure>
<graphic graphicname="A245857E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vent Plug</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000004LOY004X_02_0007">
<ptxt>REMOVE FRONT WHEEL LH</ptxt>
</s-1>
<s-1 id="RM000004LOY004X_02_0008" proc-id="RM23G0E___000064Q00000">
<ptxt>REMOVE FRONT FENDER APRON SEAL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 clips and front fender apron seal LH.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0030" proc-id="RM23G0E___00005IP00000">
<ptxt>REMOVE INJECTOR DRIVER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 4 connectors.</ptxt>
<figure>
<graphic graphicname="A239612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and injector driver assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004LOY004X_02_0010" proc-id="RM23G0E___000064R00000">
<ptxt>DISCONNECT VANE PUMP OIL RESERVOIR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and disconnect the vane pump oil reservoir.</ptxt>
<figure>
<graphic graphicname="A239651" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0011" proc-id="RM23G0E___000064S00000">
<ptxt>REMOVE MANIFOLD STAY WITH VACUUM SWITCHING VALVE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 3 vacuum switching valve connectors.</ptxt>
<figure>
<graphic graphicname="A244615" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 1 vacuum transmitting hose.</ptxt>
<figure>
<graphic graphicname="A244965" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 2 No. 2 vacuum transmitting hoses from the No. 2 EGR valve.</ptxt>
<figure>
<graphic graphicname="A244967" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 1 vacuum transmitting hose and No. 4 vacuum transmitting hose.</ptxt>
<figure>
<graphic graphicname="A244966" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and manifold stay with vacuum switching valve.</ptxt>
<figure>
<graphic graphicname="A244968" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0012" proc-id="RM23G0E___000064T00000">
<ptxt>REMOVE FUEL INLET PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and No. 2 injection pipe clamp.</ptxt>
<figure>
<graphic graphicname="A245462" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, loosen the union nuts and remove the fuel inlet pipe.</ptxt>
<figure>
<graphic graphicname="A239672E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Supply Pump Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0013" proc-id="RM23G0E___000064U00000">
<ptxt>REMOVE NO. 1, NO. 2 AND NO. 3 INJECTION PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After removing the injection pipe, cover the outlets on the common rail with tape to keep out foreign matter.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipe, put it in a plastic bag to prevent foreign matter from contaminating its injector inlet.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Remove the 2 nuts and No. 3 injection pipe clamp.</ptxt>
<figure>
<graphic graphicname="A244613" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and 2 No. 2 injection pipe clamps.</ptxt>
<figure>
<graphic graphicname="A244612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, loosen the union nuts and remove the No. 1, No. 2 and No. 3 injection pipes.</ptxt>
<figure>
<graphic graphicname="A239673E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0014" proc-id="RM23G0E___000064V00000">
<ptxt>DISCONNECT HEATER WATER PIPE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the water by-pass hose clamp.</ptxt>
<figure>
<graphic graphicname="A239652" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the heater water pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0015" proc-id="RM23G0E___000064W00000">
<ptxt>REMOVE WATER BY-PASS HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 water by-pass hose clamps.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 7 water by-pass hose labeled A in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 4 water by-pass hose labeled B in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 3 water by-pass hose labeled C in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 8 water by-pass hose labeled D in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 6 water by-pass hose labeled E in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the No. 5 water by-pass hose labeled F in the illustration.</ptxt>
<figure>
<graphic graphicname="A239653E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0016" proc-id="RM23G0E___000064X00000">
<ptxt>REMOVE NO. 1 VACUUM TRANSMITTING PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the vacuum hose from the intake manifold.</ptxt>
<figure>
<graphic graphicname="A244611" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and No. 1 vacuum transmitting pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0017" proc-id="RM23G0E___000064Y00000">
<ptxt>REMOVE WIRING HARNESS CLAMP BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the glow plug connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 wire harness clamps and disconnect the glow plug connector from the wiring harness clamp bracket.</ptxt>
<figure>
<graphic graphicname="A245469" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and wiring harness clamp bracket.</ptxt>
<figure>
<graphic graphicname="A245466" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0018" proc-id="RM23G0E___000064Z00000">
<ptxt>REMOVE NO. 3 ENGINE COVER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and No. 3 engine cover bracket.</ptxt>
<figure>
<graphic graphicname="A239350" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0019" proc-id="RM23G0E___000065000000">
<ptxt>REMOVE ENGINE COVER BRACKET INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and 2 engine cover bracket insulators.</ptxt>
<figure>
<graphic graphicname="A239397" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0020" proc-id="RM23G0E___000065100000">
<ptxt>REMOVE NO. 4 ENGINE COVER BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and No. 4 engine cover bracket.</ptxt>
<figure>
<graphic graphicname="A239426" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0021" proc-id="RM23G0E___000065200000">
<ptxt>REMOVE AIR CONNECTOR STAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 4 injector connectors and detach the 3 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="A245685" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt labeled A and disconnect the No. 1 fuel pipe from the air connector stay.</ptxt>
<figure>
<graphic graphicname="A239654E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Fuel Pipe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 3 bolts and air connector stay.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0022" proc-id="RM23G0E___000065300000">
<ptxt>REMOVE EGR COOLER WITH NO. 2 EGR VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel pressure sensor connector from the common rail.</ptxt>
<figure>
<graphic graphicname="A239655" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 nuts, 3 bolts and EGR cooler with No. 2 EGR valve.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 gaskets from the cylinder head and electric EGR control valve.</ptxt>
<figure>
<graphic graphicname="A239656E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" align="center" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0023" proc-id="RM23G0E___000066H00000">
<ptxt>REMOVE EGR VALVE ADAPTER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a 6 mm hexagon wrench, remove the 3 hexagon bolts, 3 plate washers, EGR valve adapter and gasket.</ptxt>
<figure>
<graphic graphicname="A239641" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0024" proc-id="RM23G0E___000066I00000">
<ptxt>REMOVE NO. 2 EGR VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, No. 2 EGR valve and gasket.</ptxt>
<figure>
<graphic graphicname="A239642" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000004LOY004X_02_0025" proc-id="RM23G0E___000066J00000">
<ptxt>REMOVE EGR COOLER INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 EGR cooler insulators from the EGR cooler.</ptxt>
<figure>
<graphic graphicname="A239643E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 EGR Cooler Insulator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 EGR Cooler Insulator</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>