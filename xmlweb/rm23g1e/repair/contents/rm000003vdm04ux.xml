<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B7_T00KV" variety="T00KV">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000003VDM04UX" category="C" type-id="803YK" name-id="DL54E-07" from="201210">
<dtccode>B27A7</dtccode>
<dtcname>Open in Inside Luggage Compartment Electrical Key Oscillator Circuit</dtcname>
<subpara id="RM000003VDM04UX_01" type-id="60" category="03" proc-id="RM23G0E___0000EVT00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The certification ECU generates a request signal and transmits the signal to the indoor No. 3 electrical key antenna (inside luggage). The indoor No. 3 electrical key antenna (inside luggage) detects that the electrical key is inside the vehicle and transmits the request signal received from the certification ECU. DTC B27A7 is stored by the certification ECU when an open circuit is detected in the circuit between the certification ECU and indoor No. 3 electrical key antenna (inside luggage) (CLG7 - CLG6, CG7B - CLGB).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B27A7</ptxt>
</entry>
<entry valign="middle">
<ptxt>An open circuit is detected in the circuit between the certification ECU and indoor No. 3 electrical key antenna (inside luggage) (CLG7 - CLG6, CG7B - CLGB).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Indoor No. 3 electrical key antenna (inside luggage)</ptxt>
</item>
<item>
<ptxt>Certification ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003VDM04UX_02" type-id="32" category="03" proc-id="RM23G0E___0000EVU00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B210705E15" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003VDM04UX_03" type-id="51" category="05" proc-id="RM23G0E___0000EVV00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>The entry and start system (for Entry Function) uses a multiplex communication system (LIN communication system) and the CAN communication system. Inspect the communication function by following How to Proceed with Troubleshooting (See page <xref label="Seep01" href="RM000000XU70CAX"/>). Troubleshoot the entry and start system (for Entry Function) after confirming that the communication systems are functioning properly.</ptxt>
</item>
<item>
<ptxt>When using the intelligent tester with the engine switch off to troubleshoot: Connect the intelligent tester to the DLC3 and turn a courtesy light switch on and off at 1.5-second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</item>
<item>
<ptxt>Before replacing the certification ECU, refer to the Service Bulletin.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000003VDM04UX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003VDM04UX_04_0001" proc-id="RM23G0E___0000EVW00001">
<testtitle>CHECK CONNECTOR CONNECTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the certification ECU and indoor No. 3 electrical key antenna (inside luggage).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003VDM04UX_04_0002" fin="false">OK</down>
<right ref="RM000003VDM04UX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003VDM04UX_04_0002" proc-id="RM23G0E___0000EVX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (INDOOR NO. 3 ELECTRICAL KEY ANTENNA - CERTIFICATION ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the R15 antenna connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G38 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>R15-1 (CLG6) - G38-26 (CLG7)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R15-2 (CLGB) - G38-27 (CG7B)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G38-26 (CLG7) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G38-27 (CG7B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G38-15 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003VDM04UX_04_0003" fin="false">OK</down>
<right ref="RM000003VDM04UX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003VDM04UX_04_0003" proc-id="RM23G0E___0000EVY00001">
<testtitle>CHECK CERTIFICATION ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="B224125E19" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Certification ECU)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G38-26 (CLG7) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch off, all doors closed, key not in cabin and lock sensor off → on</ptxt>
</entry>
<entry valign="middle">
<ptxt>No pulse generation → Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G38-27 (CG7B) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Engine switch off, all doors closed, key not in cabin and lock sensor off → on</ptxt>
</entry>
<entry valign="middle">
<ptxt>No pulse generation → Pulse generation</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003VDM04UX_04_0004" fin="false">OK</down>
<right ref="RM000003VDM04UX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003VDM04UX_04_0004" proc-id="RM23G0E___0000EVZ00001">
<testtitle>REPLACE INDOOR NO. 3 ELECTRICAL KEY ANTENNA ASSEMBLY (INSIDE LUGGAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the indoor No. 3 electrical key antenna (inside luggage) with a new or normally functioning one (See page <xref label="Seep01" href="RM00000482S00EX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003VDM04UX_04_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003VDM04UX_04_0005" proc-id="RM23G0E___0000EW000001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002BZF045X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002BZF045X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B27A7 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003VDM04UX_04_0009" fin="true">OK</down>
<right ref="RM000003VDM04UX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003VDM04UX_04_0006">
<testtitle>CONNECT CONNECTORS PROPERLY</testtitle>
</testgrp>
<testgrp id="RM000003VDM04UX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003VDM04UX_04_0008">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000003VDM04UX_04_0009">
<testtitle>END (INDOOR NO. 3 ELECTRICAL KEY ANTENNA ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>