<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0012" variety="S0012">
<name>A343F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0012_7B941_T00DP" variety="T00DP">
<name>AUTOMATIC TRANSMISSION SYSTEM</name>
<para id="RM000002IR0030X" category="C" type-id="305O2" name-id="AT7W8-02" from="201207" to="201210">
<dtccode>P2772</dtccode>
<dtcname>Four Wheel Drive (4WD) Low Switch Circuit Range / Performance</dtcname>
<subpara id="RM000002IR0030X_01" type-id="60" category="03" proc-id="RM23G0E___00007PF00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM detects the signal from the transfer L4 position switch.</ptxt>
<ptxt>This DTC indicates that the transfer L4 position switch remains on.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2772</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transfer L4 position switch remains on while the vehicle is being driven under the following conditions for 1.8 sec. or more (1-trip detection logic):</ptxt>
<ptxt>(a) Output shaft speed is between 1000 and 3000 rpm.</ptxt>
<ptxt>(b) Transfer position switch is in H4.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in transfer L4 position switch circuit</ptxt>
</item>
<item>
<ptxt>Transfer system</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002IR0030X_02" type-id="64" category="03" proc-id="RM23G0E___00007PG00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM monitors the transfer-case L4 position switch to determine when the transfer-case L4 gears are engaged. If the L4 position switch indicates that the transfer-case L4 gears are engaged under the following conditions, the ECM will conclude that there is a malfunction of the L4 position switch, illuminate the MIL and store the DTC:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The transfer-case shifter is in the "H4" position.</ptxt>
</item>
<item>
<ptxt>The transfer-case output shaft speed is between 1000 and 3000 rpm.</ptxt>
</item>
<item>
<ptxt>The specified time period has elapsed.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002IR0030X_06" type-id="32" category="03" proc-id="RM23G0E___00007PH00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C214970E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002IR0030X_07" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002IR0030X_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002IR0030X_08_0001" proc-id="RM23G0E___00007OU00000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOUR WHEEL DRIVE CONTROL ECU - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G53 four wheel drive control ECU connector.</ptxt>
<figure>
<graphic graphicname="C209926E09" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G53-21 (L4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Four Wheel Drive Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002IR0030X_08_0004" fin="true">OK</down>
<right ref="RM000002IR0030X_08_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002IR0030X_08_0004">
<testtitle>GO TO TRANSFER SYSTEM (TERMINALS OF ECU)<xref label="Seep01" href="RM0000030AB00SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002IR0030X_08_0002" proc-id="RM23G0E___00007OV00000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOUR WHEEL DRIVE CONTROL ECU - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G59 ECM connector.</ptxt>
<figure>
<graphic graphicname="C214333E04" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G59-14 (L4) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002IR0030X_08_0003" fin="true">OK</down>
<right ref="RM000002IR0030X_08_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002IR0030X_08_0003">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002IR0030X_08_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>