<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1KD-FTV ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7B8XI_T0076" variety="T0076">
<name>CYLINDER BLOCK</name>
<para id="RM00000147R03ZX" category="A" type-id="30019" name-id="EM6CE-01" from="201207">
<name>REPLACEMENT</name>
<subpara id="RM00000147R03ZX_01" type-id="01" category="01">
<s-1 id="RM00000147R03ZX_01_0001" proc-id="RM23G0E___00004F400000">
<ptxt>REPLACE TIGHT PLUG</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>If water leaks from the tight plug or the plug is corroded, replace it.</ptxt>
</atten3>
<s2>
<ptxt>Remove the tight plugs.</ptxt>
</s2>
<s2>
<ptxt>Apply adhesive to new tight plugs.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1324, Three Bond 1324 or equivalent</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not start the engine for 1 hour after installation.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Position A:</ptxt>
<ptxt>Using a 14 mm steel bar and hammer, tap in the tight plug as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Other Positions:</ptxt>
<ptxt>Using SST and a hammer, tap in the tight plugs as shown in the illustration.</ptxt>
<ptxt>Position B</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00180</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<ptxt>Position C</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00190</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<ptxt>Position D</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00200</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<ptxt>Position E and H</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00350</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<ptxt>Position F</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00400</s-subnumber>
<s-subnumber>09951-00450</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<ptxt>Position G</ptxt>
<sst>
<sstitem>
<s-number>09950-60010</s-number>
<s-subnumber>09951-00450</s-subnumber>
</sstitem>
<sstitem>
<s-number>09950-70010</s-number>
<s-subnumber>09951-07100</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="A152216E05" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stops</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Right Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Left Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Front Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Rear Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147R03ZX_01_0003" proc-id="RM23G0E___00004F500000">
<ptxt>REPLACE STRAIGHT PIN</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>It is not necessary to remove the straight pin unless it is being replaced.</ptxt>
</atten3>
<s2>
<ptxt>Remove the straight pins.</ptxt>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, tap in new straight pins to the cylinder block.</ptxt>
<spec>
<title>Standard Protrusion Height</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Pin A</ptxt>
</entry>
<entry valign="middle">
<ptxt>6 mm (0.234 in.) or less</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin B</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.5 mm (0.295 in.) or less</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin C</ptxt>
</entry>
<entry valign="middle">
<ptxt>12 mm (0.472 in.) or less</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pin D</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 to 8 mm (0.197 to 0.315 in.) or less</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="A225495E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Front Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Rear Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Oil Pan Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147R03ZX_01_0004" proc-id="RM23G0E___00004F600000">
<ptxt>REPLACE RING PIN</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>It is not necessary to remove the ring pin unless it is being replaced.</ptxt>
</atten3>
<s2>
<ptxt>Remove the ring pins.</ptxt>
</s2>
<s2>
<ptxt>Using a plastic-faced hammer, tap in new ring pins to the cylinder block.</ptxt>
<spec>
<title>Standard Protrusion</title>
<specitem>
<ptxt>6 mm (0.236 in.) or less</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="A152219E03" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Head Side</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>