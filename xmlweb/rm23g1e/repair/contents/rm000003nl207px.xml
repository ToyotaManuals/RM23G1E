<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM000003NL207PX" category="C" type-id="8046S" name-id="BC86V-27" from="201207" to="201210">
<dtccode>C1440</dtccode>
<dtcname>Steering Angle Sensor Unusual Bank Angle Detected</dtcname>
<subpara id="RM000003NL207PX_01" type-id="60" category="03" proc-id="RM23G0E___0000AFR00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the skid control ECU determines that the vehicle is being driven at a steep bank angle, the skid control ECU stores DTC C1440 while VSC operation is temporarily disabled.</ptxt>
<ptxt>It is not a malfunction if the system and sensor circuits are normal and if the normal VSC operation resumes, which automatically clears DTC C1440, after the vehicle is returned to a normal position.</ptxt>
<atten4>
<ptxt>DTC C1440 and the yaw rate and acceleration sensor DTC are stored when the skid control ECU determines that the vehicle is being driven at a steep bank angle and either of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The yaw rate and acceleration sensor is installed at an angle.</ptxt>
</item>
<item>
<ptxt>The yaw rate and acceleration sensor signal output is stuck.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.25in"/>
<colspec colname="COL2" colwidth="3.07in"/>
<colspec colname="COL3" colwidth="2.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1440</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driving at a steep bank angle</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yaw rate and acceleration sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003NL207PX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003NL207PX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003NL207PX_04_0001" proc-id="RM23G0E___0000AFS00000">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform a road test.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00IX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.01in"/>
<colspec colname="COL2" colwidth="1.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC C1440 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1440 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Yaw rate and acceleration sensor DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003NL207PX_04_0015" fin="false">A</down>
<right ref="RM000003NL207PX_04_0013" fin="true">B</right>
<right ref="RM000003NL207PX_04_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003NL207PX_04_0015" proc-id="RM23G0E___0000AFT00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM0000046KV00IX"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the skid control ECU determines that the vehicle is being driven at a steep bank angle, the skid control ECU stores DTC C1440 while VSC operation is temporarily disabled.</ptxt>
</item>
<item>
<ptxt>It is not a malfunction if the system and sensor circuits are normal and if the normal VSC operation resumes, which automatically clears DTC C1440, after the vehicle is returned to a normal position.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003NL207PX_04_0013" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003NL207PX_04_0006">
<testtitle>REPAIR CIRCUITS INDICATED BY OUTPUT DTCS<xref label="Seep01" href="RM0000045Z600OX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003NL207PX_04_0013">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>