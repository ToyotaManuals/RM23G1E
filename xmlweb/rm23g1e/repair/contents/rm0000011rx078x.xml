<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12066_S002B" variety="S002B">
<name>DOOR / HATCH</name>
<ttl id="12066_S002B_7B9G9_T00PX" variety="T00PX">
<name>REAR DOOR</name>
<para id="RM0000011RX078X" category="A" type-id="80002" name-id="DH2XB-01" from="201207" to="201210">
<name>DISASSEMBLY</name>
<subpara id="RM0000011RX078X_02" type-id="11" category="10" proc-id="RM23G0E___0000IOV00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000011RX078X_01" type-id="01" category="01">
<s-1 id="RM0000011RX078X_01_0069" proc-id="RM23G0E___0000IOI00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0001" proc-id="RM23G0E___0000BZO00000">
<ptxt>REMOVE NO. 2 DOOR INSIDE HANDLE BEZEL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using moulding remover A, detach the 3 claws and remove the rear door inside handle bezel as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239600" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0086" proc-id="RM23G0E___0000BZP00000">
<ptxt>REMOVE ASSIST GRIP COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using moulding remover A, detach the 8 claws and remove the assist grip cover.</ptxt>
<figure>
<graphic graphicname="B239602" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0004" proc-id="RM23G0E___0000BZQ00000">
<ptxt>REMOVE REAR DOOR TRIM BOARD SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B239604" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Using a clip remover, detach the 9 clips.</ptxt>
</s2>
<s2>
<ptxt>Pull out the rear door trim board sub-assembly in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B239606" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Raise the rear door trim board sub-assembly to detach the 4 claws and remove the rear door trim board sub-assembly together with the rear door inner glass weatherstrip.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the rear door lock remote control cable assembly and rear door inside locking cable assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect each connector.</ptxt>
<figure>
<graphic graphicname="B239616E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 14 Speakers</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 9 Speakers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the claw and remove the clamp.</ptxt>
<figure>
<graphic graphicname="B241320" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0088" proc-id="RM23G0E___0000BZT00000">
<ptxt>REMOVE REAR DOOR INNER GLASS WEATHERSTRIP LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 claws and remove the rear door inner glass weatherstrip from the rear door trim board sub-assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239615" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0121" proc-id="RM23G0E___0000IF500000">
<ptxt>REMOVE REAR POWER WINDOW REGULATOR SWITCH ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the rear power window regulator switch assembly.</ptxt>
<figure>
<graphic graphicname="B238973" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0087" proc-id="RM23G0E___0000F8U00000">
<ptxt>REMOVE REAR DOOR COURTESY LIGHT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the light.</ptxt>
<figure>
<graphic graphicname="E198065" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0109" proc-id="RM23G0E___0000F8G00000">
<ptxt>REMOVE REAR DOOR INSIDE HANDLE ILLUMINATION LIGHT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="E200180E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the claw and remove the light.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0110" proc-id="RM23G0E___0000F8H00000">
<ptxt>REMOVE NO. 2 INTERIOR ILLUMINATION LIGHT SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="E198055" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Turn the light in the direction indicated by the arrow and remove it.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0111" proc-id="RM23G0E___0000IOL00000">
<ptxt>REMOVE REAR DOOR INSIDE HANDLE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 8 screws and rear door armrest.</ptxt>
<figure>
<graphic graphicname="B239610" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
<figure>
<graphic graphicname="B239618" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the guide and 2 claws and remove the rear door inside handle sub-assembly from the rear door trim board sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0104" proc-id="RM23G0E___0000BZS00000">
<ptxt>REMOVE REAR NO. 2 SPEAKER ASSEMBLY (for 14 Speakers)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and No. 2 speaker.</ptxt>
<figure>
<graphic graphicname="B239137" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0089" proc-id="RM23G0E___0000BZN00000">
<ptxt>REMOVE REAR SPEAKER SET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B239136" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the rear speaker set.</ptxt>
<atten3>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0008" proc-id="RM23G0E___0000ELJ00000">
<ptxt>REMOVE REAR DOOR SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B239620" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 3 clamps, move the wire harness out of the way and remove the service hole cover.</ptxt>
<atten4>
<ptxt>Remove any remaining butyl tape from the door.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0009" proc-id="RM23G0E___0000ELK00000">
<ptxt>REMOVE REAR DOOR GLASS RUN LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear door glass run.</ptxt>
<figure>
<graphic graphicname="B239628" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0113" proc-id="RM23G0E___0000IOM00000">
<ptxt>REMOVE REAR DOOR FRAME GARNISH LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clip and remove the rear door frame garnish.</ptxt>
<figure>
<graphic graphicname="B240621" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0010" proc-id="RM23G0E___0000ELL00000">
<ptxt>REMOVE REAR DOOR WINDOW DIVISION BAR SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the temporary bolt.</ptxt>
<figure>
<graphic graphicname="B239630E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temporary Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts and screw.</ptxt>
</s2>
<s2>
<ptxt>Remove the rear door window division bar sub-assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the temporary bolt from the rear door window division bar sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0076" proc-id="RM23G0E___0000ELM00000">
<ptxt>REMOVE REAR DOOR QUARTER WINDOW GLASS LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear door quarter window glass and rear door quarter window weatherstrip as a unit as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239632" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0114" proc-id="RM23G0E___0000ION00000">
<ptxt>REMOVE REAR DOOR QUARTER WINDOW WEATHERSTRIP LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear door quarter window weatherstrip from the rear door quarter window glass.</ptxt>
<figure>
<graphic graphicname="B241321" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0013" proc-id="RM23G0E___0000ELN00000">
<ptxt>REMOVE REAR DOOR GLASS SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the cable to the negative (-) battery terminal and rear power window regulator motor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the power window regulator switch assembly and move the rear door glass sub-assembly so that the door glass bolts can be seen.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the power window regulator switch assembly and rear power window regulator motor connectors.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>w/ Navigation System (for HDD):</ptxt>
<ptxt>After the ignition switch is turned off, the HDD navigation system requires approximately a minute to record various types of memory and settings. As a result, after turning the ignition switch off, wait a minute or more before disconnecting the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the rear door glass sub-assembly from the rear door window regulator sub-assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239636" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>After the bolts are removed, do not allow the door glass to fall.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the rear door glass sub-assembly as indicated by the arrows in the order shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239640E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not damage the door glass.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0122" proc-id="RM23G0E___0000IOT00000">
<ptxt>REMOVE REAR DOOR GLASS CHANNEL SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, remove the rear door glass channel sub-assembly.</ptxt>
<figure>
<graphic graphicname="B234457E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0123" proc-id="RM23G0E___0000IOU00000">
<ptxt>REMOVE REAR DOOR GLASS CHANNEL FILLER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear door glass channel filler.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0014" proc-id="RM23G0E___0000IFZ00000">
<ptxt>REMOVE REAR DOOR WINDOW REGULATOR ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the temporary bolt.</ptxt>
<figure>
<graphic graphicname="B239642E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temporary Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not remove the temporary bolt. If the temporary bolt is removed, the rear door window regulator may fall and cause damage.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the 3 bolts.</ptxt>
</s2>
<s2>
<ptxt>Remove the rear door window regulator assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the temporary bolt from the rear door window regulator assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0124" proc-id="RM23G0E___0000IFY00000">
<ptxt>REMOVE REAR POWER WINDOW REGULATOR MOTOR ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B242868" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T25 "TORX" socket wrench, remove the 3 screws and power window regulator motor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0120" proc-id="RM23G0E___0000ELF00000">
<ptxt>REMOVE REAR DOOR LOCK ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" wrench, remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B237876E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Slide</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Move the rear door lock assembly downward, pull the release plate out of the rear door outside handle frame, and remove the rear door lock assembly and cables as a unit.</ptxt>
</s2>
<s2>
<ptxt>Remove the door lock wiring harness seal from the rear door lock assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0096" proc-id="RM23G0E___0000ELG00000">
<ptxt>REMOVE REAR DOOR LOCK REMOTE CONTROL CABLE ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the claw.</ptxt>
<figure>
<graphic graphicname="B239650E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the rear door lock remote control cable assembly.</ptxt>
<figure>
<graphic graphicname="B239652E04" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0097" proc-id="RM23G0E___0000ELH00000">
<ptxt>REMOVE REAR DOOR INSIDE LOCKING CABLE ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 claws.</ptxt>
<figure>
<graphic graphicname="B239654E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the rear door inside locking cable assembly.</ptxt>
<figure>
<graphic graphicname="B239656E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Double Locking System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Double Locking System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0020" proc-id="RM23G0E___0000IOB00000">
<ptxt>REMOVE REAR DOOR OUTSIDE HANDLE COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" socket wrench, loosen the screw.</ptxt>
<figure>
<graphic graphicname="B239658" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>The screw cannot be removed because it is integrated into the rear door outside handle frame sub-assembly.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Detach the claw and remove the rear door outside handle cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0021" proc-id="RM23G0E___0000IOC00000">
<ptxt>REMOVE REAR DOOR OUTSIDE HANDLE ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Move the lever in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B239662" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the rear door outside handle assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239664" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0022" proc-id="RM23G0E___0000IOD00000">
<ptxt>REMOVE REAR DOOR NO. 1 OUTSIDE HANDLE PAD LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 claws and remove the rear door No. 1 outside handle pad.</ptxt>
<figure>
<graphic graphicname="B239668" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0023" proc-id="RM23G0E___0000IOE00000">
<ptxt>REMOVE REAR DOOR NO. 2 OUTSIDE HANDLE PAD LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the rear door No. 2 outside handle pad.</ptxt>
<figure>
<graphic graphicname="B239670" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0024" proc-id="RM23G0E___0000IOF00000">
<ptxt>REMOVE REAR DOOR OUTSIDE HANDLE FRAME SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a T30 "TORX" socket wrench, loosen the screw.</ptxt>
<figure>
<graphic graphicname="B239672" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Slide the rear door outside handle frame sub-assembly to detach the door handle nut and claw of the rear door outside handle frame sub-assembly, and then remove the rear door outside handle frame sub-assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0092" proc-id="RM23G0E___0000IOJ00000">
<ptxt>REMOVE REAR DOOR NO. 2 WEATHERSTRIP LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 6 clips and guide and remove the rear door No. 2 weatherstrip as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239676" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0026" proc-id="RM23G0E___0000IOG00000">
<ptxt>REMOVE REAR DOOR CHECK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, 2 nuts and rear door check assembly.</ptxt>
<figure>
<graphic graphicname="B239622" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0027" proc-id="RM23G0E___0000IOH00000">
<ptxt>REMOVE REAR DOOR WEATHERSTRIP LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 19 clips and guide and remove the rear door weatherstrip.</ptxt>
<figure>
<graphic graphicname="B239624" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000011RX078X_01_0100" proc-id="RM23G0E___0000IOK00000">
<ptxt>REMOVE REAR DOOR BELT MOULDING LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241010E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 7 claws and remove the rear door belt moulding.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0102" proc-id="RM23G0E___0000IN300000">
<ptxt>REMOVE REAR DOOR FRONT WINDOW FRAME MOULDING LH
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="B243888" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the door window frame moulding clip.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B243889E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Put protective tape around the front door rear window frame moulding.</ptxt>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the clip and remove the double-sided tape to remove the front door rear window frame moulding.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Double-sided Tape</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Moulding Remover</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0116" proc-id="RM23G0E___0000IOP00000">
<ptxt>REMOVE NO. 2 BLACK OUT TAPE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236343" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Pull back an edge of the No. 2 black out tape and pull it parallel to the vehicle body to remove it.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0117" proc-id="RM23G0E___0000IOQ00000">
<ptxt>REMOVE REAR DOOR LOWER OUTSIDE STRIPE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236346" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Pull back an edge of the rear door lower outside stripe and pull it parallel to the vehicle body to remove it.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0118" proc-id="RM23G0E___0000IOR00000">
<ptxt>REMOVE BLACK OUT TAPE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236348" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Pull back an edge of the black out tape and pull it parallel to the vehicle body to remove it.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0119" proc-id="RM23G0E___0000IOS00000">
<ptxt>REMOVE REAR DOOR OUTSIDE STRIPE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236350" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Pull back an edge of the rear door outside stripe and pull it parallel to the vehicle body to remove it.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000011RX078X_01_0115" proc-id="RM23G0E___0000IOO00000">
<ptxt>REMOVE REAR DOOR PANEL CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the clip and remove the door panel cushion.</ptxt>
<figure>
<graphic graphicname="B240622" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>