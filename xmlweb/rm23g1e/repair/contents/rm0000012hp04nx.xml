<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002D" variety="S002D">
<name>WIPER / WASHER</name>
<ttl id="12068_S002D_7B9GY_T00QM" variety="T00QM">
<name>RELAY (w/ Rear Wiper)</name>
<para id="RM0000012HP04NX" category="G" type-id="8000T" name-id="WW3YV-02" from="201207">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000012HP04NX_01" type-id="01" category="01">
<s-1 id="RM0000012HP04NX_01_0002" proc-id="RM23G0E___0000IY800000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000012HP04NX_01_0003" proc-id="RM23G0E___0000IY900000">
<ptxt>REMOVE WASHER RR RELAY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E202166E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Remove the washer RR relay from the engine room relay block.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Washer RR Relay</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000012HP04NX_01_0008" proc-id="RM23G0E___0000IYC00000">
<ptxt>INSPECT WASHER RR RELAY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B242124E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage not applied between terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery voltage applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, replace the washer RR relay.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
<s-1 id="RM0000012HP04NX_01_0007" proc-id="RM23G0E___0000IYB00000">
<ptxt>INSTALL WASHER RR RELAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the washer RR relay to the engine room relay block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000012HP04NX_01_0005" proc-id="RM23G0E___0000IYA00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>