<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM000001EAA025X" category="C" type-id="804R0" name-id="BCB7M-02" from="201207" to="201210">
<dtccode>C1257</dtccode>
<dtcname>Power Supply Drive Circuit</dtcname>
<subpara id="RM000001EAA025X_01" type-id="60" category="03" proc-id="RM23G0E___0000AGO00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The motor relay (semiconductor relay) is built into the master cylinder solenoid and drives the pump motor based on a signal from the skid control ECU.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1257</ptxt>
</entry>
<entry valign="middle">
<ptxt>There is an open in the motor system circuit (motor input circuit).</ptxt>
</entry>
<entry valign="middle">
<ptxt>Master cylinder solenoid (Skid control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001EAA025X_02" type-id="51" category="05" proc-id="RM23G0E___0000AGP00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the master cylinder solenoid, perform calibration (See page <xref label="Seep01" href="RM00000452J00IX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001EAA025X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001EAA025X_03_0001" proc-id="RM23G0E___0000AGQ00000">
<testtitle>CHECK PUMP MOTOR OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off. </ptxt>
</test1>
<test1>
<ptxt>Disconnect the A7 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal more than 40 times. </ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Check pump motor operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt> Pump motor operates normally.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.49in"/>
<colspec colname="COL2" colwidth="1.64in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001EAA025X_03_0002" fin="false">A</down>
<right ref="RM000001EAA025X_03_0003" fin="true">B</right>
<right ref="RM000001EAA025X_03_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001EAA025X_03_0002" proc-id="RM23G0E___0000AGR00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the A7 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00IX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.49in"/>
<colspec colname="COL2" colwidth="1.64in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001EAA025X_03_0004" fin="true">A</down>
<right ref="RM000001EAA025X_03_0003" fin="true">B</right>
<right ref="RM000001EAA025X_03_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001EAA025X_03_0003">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EAA025X_03_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001EAA025X_03_0005">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>