<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0022" variety="S0022">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0022_7B9C6_T00LU" variety="T00LU">
<name>AIRBAG SYSTEM</name>
<para id="RM0000014S404CX" category="C" type-id="305K6" name-id="RS94D-05" from="201210">
<dtccode>B1651/33</dtccode>
<dtcname>Manual Cut off Switch Trouble</dtcname>
<subpara id="RM0000014S404CX_01" type-id="60" category="03" proc-id="RM23G0E___0000FKF00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<ptxt>"Manual cut off switch" refers to the airbag cutoff switch cylinder.</ptxt>
</atten4>
<ptxt>The airbag cutoff switch cylinder circuit consists of the center airbag sensor and airbag cutoff switch cylinder.</ptxt>
<ptxt>The instrument panel passenger airbag can be optionally deactivated via this circuit by turning the airbag cutoff switch cylinder to the "OFF" position.</ptxt>
<ptxt>If the instrument panel passenger airbag is deactivated, the airbag cutoff indicator comes on to inform the passengers.</ptxt>
<ptxt>DTC B1651/33 is stored when a malfunction is detected in the airbag cutoff switch cylinder circuit.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1651/33</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor detects a line short circuit signal, open circuit signal, short circuit to ground signal or short circuit to B+ signal in the airbag cutoff switch cylinder circuit for 2 seconds.</ptxt>
</item>
<item>
<ptxt>An airbag cutoff switch cylinder malfunction.</ptxt>
</item>
<item>
<ptxt>A center airbag sensor malfunction.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Instrument panel wire</ptxt>
</item>
<item>
<ptxt>Airbag cutoff switch cylinder sub-assembly</ptxt>
</item>
<item>
<ptxt>Center airbag sensor assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000014S404CX_02" type-id="32" category="03" proc-id="RM23G0E___0000FKG00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C199310E04" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000014S404CX_03" type-id="51" category="05" proc-id="RM23G0E___0000FKH00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="2">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable from the negative (-) battery terminal while performing repairs, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>The 19-11 (MSW+) and 19-12 (MSW-) terminals in this circuit have an activation prevention mechanism. This mechanism can be used to check for an open circuit in the wire harness. In case of other checks (check for short, short to ground or short to B+), it is necessary to release this mechanism.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000014S404CX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000014S404CX_04_0001" proc-id="RM23G0E___0000FKI00001">
<testtitle>CHECK INSTRUMENT PANEL WIRE (CENTER AIRBAG SENSOR - AIRBAG CUTOFF SWITCH CYLINDER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
<figure>
<graphic graphicname="C201186E04" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the connectors from the center airbag sensor and airbag cut off switch cylinder.</ptxt>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G73-2 (PMC+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G73-1 (PMC-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal, and wait for at least 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G73-2 (PMC+) - G73-1 (PMC-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Release the activation prevention mechanism built into connector B (See page <xref label="Seep01" href="RM000000XFD0INX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL3" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G73-2 (PMC+) - G73-1 (PMC-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G73-2 (PMC+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G73-1 (PMC-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1 MΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Airbag Cutoff Switch Cylinder</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Airbag Cutoff Switch Cylinder)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Connector C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Center Airbag Sensor)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry>
<ptxt>Connector B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM0000014S404CX_04_0005" fin="false">OK</down>
<right ref="RM0000014S404CX_04_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000014S404CX_04_0005" proc-id="RM23G0E___0000FKJ00001">
<testtitle>INSPECT AIRBAG CUTOFF SWITCH CYLINDER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the airbag cutoff switch cylinder (See page <xref label="Seep01" href="RM000003YQW009X"/>).</ptxt>
<figure>
<graphic graphicname="C198536E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2 (PMC+) - 1 (PMC-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cutoff switch in "ON" position (Front passenger side airbag active)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>360 to 440 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (PMC+) - 1 (PMC-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cutoff switch in "OFF" position (Front passenger side airbag not active)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>90 to 110 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000014S404CX_04_0006" fin="false">OK</down>
<right ref="RM0000014S404CX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000014S404CX_04_0006" proc-id="RM23G0E___0000FKK00001">
<testtitle>CHECK CENTER AIRBAG SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the connectors to the center airbag sensor and airbag cutoff switch cylinder.</ptxt>
<figure>
<graphic graphicname="C201187E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the cable to the negative (-) battery terminal, and wait for at least 2 seconds.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs stored in memory (See page <xref label="Seep01" href="RM000000XFE0IZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON, and wait for at least 60 seconds.</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000XFE0IZX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1651 is not output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Codes other than DTC B1651 may be output at this time, but they are not related to this check.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Airbag Cutoff Switch Cylinder</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Center Airbag Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000014S404CX_04_0007" fin="true">OK</down>
<right ref="RM0000014S404CX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000014S404CX_04_0009">
<testtitle>REPLACE AIRBAG CUTOFF SWITCH CYLINDER SUB-ASSEMBLY<xref label="Seep01" href="RM000003YQW009X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000014S404CX_04_0010">
<testtitle>REPLACE CENTER AIRBAG SENSOR ASSEMBLY<xref label="Seep01" href="RM000003YQB00UX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000014S404CX_04_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000000XFD0INX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000014S404CX_04_0011">
<testtitle>REPLACE INSTRUMENT PANEL WIRE</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>