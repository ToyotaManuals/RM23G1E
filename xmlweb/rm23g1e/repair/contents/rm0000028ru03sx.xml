<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000D" variety="S000D">
<name>1GR-FE FUEL</name>
<ttl id="12008_S000D_7B8YA_T007Y" variety="T007Y">
<name>FUEL SYSTEM</name>
<para id="RM0000028RU03SX" category="L" type-id="3001A" name-id="FU52N-08" from="201210">
<name>PRECAUTION</name>
<subpara id="RM0000028RU03SX_z0" proc-id="RM23G0E___00005DK00001">
<content5 releasenbr="1">
<step1>
<ptxt>PRECAUTION</ptxt>
<step2>
<ptxt>Before starting work on the fuel system, including inspections and repairs, disconnect the cable from the negative (-) battery terminal.</ptxt>
</step2>
<step2>
<ptxt>Do not smoke or work near fire when performing work on the fuel system.</ptxt>
</step2>
<step2>
<ptxt>Keep gasoline away from rubber or leather parts.</ptxt>
</step2>
</step1>
<step1>
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Take precautions to prevent gasoline from spilling out before removing fuel system parts.</ptxt>
</item>
<item>
<ptxt>Pressure still remains in the fuel lines even after performing the following procedures. When disconnecting a fuel line, cover it with a piece of cloth to prevent fuel from spraying or coming out.</ptxt>
</item>
</list1>
</atten2>
<step2>
<ptxt>Remove the circuit opening relay from the engine room relay block.</ptxt>
</step2>
<step2>
<ptxt>Start the engine. After the engine stops, turn the ignition switch off.</ptxt>
<atten4>
<ptxt>DTC P0171 (system to lean) may be stored.</ptxt>
</atten4>
</step2>
<step2>
<ptxt>Check that the engine does not start.</ptxt>
</step2>
<step2>
<ptxt>Remove the fuel tank cap and let the air out of the fuel tank.</ptxt>
</step2>
<step2>
<ptxt>Disconnect the cable from the negative battery (-) terminal.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep03" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</step2>
<step2>
<ptxt>Reinstall the circuit opening relay.</ptxt>
</step2>
</step1>
<step1>
<ptxt>FUEL SYSTEM</ptxt>
<step2>
<ptxt>When disconnecting a high pressure fuel line, a large amount of gasoline will spill out. Follow these procedures.</ptxt>
<step3>
<ptxt>Disconnect the fuel pump tube.</ptxt>
</step3>
<step3>
<ptxt>Drain any fuel remaining inside the fuel pump tube.</ptxt>
</step3>
<step3>
<ptxt>To protect the disconnected fuel pump tube from damage and contamination, cover it with a plastic bag.</ptxt>
</step3>
<step3>
<ptxt>Put a container under the connection.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when removing and installing the injectors.</ptxt>
<figure>
<graphic graphicname="A223032E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Never reuse the O-ring.</ptxt>
</step3>
<step3>
<ptxt>When installing a new O-ring to the injector, do not damage it.</ptxt>
</step3>
<step3>
<ptxt>Coat new O-rings with spindle oil or gasoline before installing. Do not use engine oil, gear oil or brake oil.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Install the injector to the delivery pipe and lower intake manifold as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A223033E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Before installing the injector, be sure to apply spindle oil or gasoline to the place where the delivery pipe or cylinder head contacts the O-ring of the injector.</ptxt>
</atten3>
</step2>
<step2>
<ptxt>Observe these precautions when disconnecting the fuel tube connector (for quick type A). </ptxt>
<figure>
<graphic graphicname="A223364E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Fuel Hose Connector Cover Type:</ptxt>
<ptxt>Detach the lock claw by lifting up the cover as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Hose Connector Cover</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check for dirt or mud on the pipe and around the connector before disconnection. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>If the connector and the pipe are stuck, pinch the connector, push and pull the pipe to disconnect the pipe and pull it out.</ptxt>
<figure>
<graphic graphicname="A088336E26" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pinch</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
<figure>
<graphic graphicname="A224870E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube. </ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>Fuel Tube Connector</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check for dirt or mud on the seal surface of the disconnected pipe. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>To protect the disconnected pipe and connector from damage and contamination, cover them with a plastic bag.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when connecting the fuel tube connector (for quick type A). </ptxt>
<step3>
<ptxt>Check that there is no damage or contamination in the connected part of the pipe.</ptxt>
</step3>
<step3>
<ptxt>Align the axis of the connector with the axis of the pipe. Push the pipe into the connector until the connector makes a "click" sound. If the connection is tight, apply a small amount of fresh engine oil to the tip of the pipe.</ptxt>
<figure>
<graphic graphicname="A071366E27" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>After connecting the pipe and the connector, check that the pipe and connector are securely connected by trying to pull them apart.</ptxt>
<figure>
<graphic graphicname="A071366E28" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Fuel Hose Connector Cover Type:</ptxt>
<ptxt>Attach the lock claws to the connector by pushing down on the cover.</ptxt>
</step3>
<step3>
<ptxt>Check for any fuel leaks.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when disconnecting the fuel tube connector (for quick type B):</ptxt>
<step3>
<ptxt>Check if there is any dirt or mud on the pipe and around the connector before disconnecting them. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>Disconnect the parts by hand.</ptxt>
</step3>
<step3>
<ptxt>If the connector and pipe are stuck, push and pull the connector to disconnect the pipe and pull it out.</ptxt>
<figure>
<graphic graphicname="A223368" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for foreign matter in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
<figure>
<graphic graphicname="A062772E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector.</ptxt>
</item>
<item>
<ptxt>Do not forcefully bend or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Check for foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Put the pipe and connector ends in plastic bags to prevent damage and foreign matter contamination.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the connector between your fingers and turn it carefully to disconnect it.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check for dirt or mud on the seal surface of the disconnected pipe. Clean if necessary.</ptxt>
</step3>
<step3>
<ptxt>To protect the disconnected pipe and connector from damage and contamination, cover them with a plastic bag.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when connecting the fuel tube connector (for quick type B):</ptxt>
<step3>
<ptxt>Align the axis of the connector with the axis of the pipe and push in the connector until the connector makes a "click" sound. If the connection is tight, apply a small amount of new engine oil to the tip of the pipe.</ptxt>
<figure>
<graphic graphicname="A223369" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>After connecting the pipe and the connector, check that the pipe and connector are securely connected by trying to pull them apart.</ptxt>
<figure>
<graphic graphicname="A223370" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check for fuel leaks.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when disconnecting the fuel tube connector (for quick type C).</ptxt>
<step3>
<ptxt>Check that there is no damage or foreign matter on the part of the pipe that contacts the connector.</ptxt>
</step3>
<step3>
<ptxt>Detach the 2 claws of the connector retainer. Push down on the connector and disconnect it from the pipe.</ptxt>
<figure>
<graphic graphicname="A224877" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Down</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100137" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the connector and pipe are stuck, pinch the fuel pipe by hand and push and pull the connector to disconnect it.</ptxt>
</atten4>
<figure>
<graphic graphicname="A091246E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check for any dirt and foreign matter contamination in the pipe and around the connector. Clean if necessary. Foreign matter may damage the O-ring or cause leaks in the seal between the pipe and connector.</ptxt>
<figure>
<graphic graphicname="A224889E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
<item>
<ptxt>Do not use any tools to separate the pipe and connector. </ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign matter on the parts when disconnecting them, as the fuel tube joint contains the O-rings that seal the plug.</ptxt>
</item>
<item>
<ptxt>Check for any dirt and foreign matter on the pipe seal surface. Clean if necessary.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, twist or turn the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the disconnected part by covering it with a plastic bag and tape after disconnecting the main tube.</ptxt>
</item>
<item>
<ptxt>If the pipe and connector are stuck together, pinch the tube between your fingers and turn it carefully to free it. Then disconnect the main tube.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Check for foreign matter on the seal surface of the disconnected pipe. Clean it if necessary.</ptxt>
</step3>
<step3>
<ptxt>To protect the disconnected pipe and connector from damage and foreign matter, cover them with a plastic bag.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when connecting the fuel tube connector (for quick type C).</ptxt>
<step3>
<ptxt>Check for foreign matter on the pipe and around the connector before connecting it. Clean it if necessary.</ptxt>
</step3>
<step3>
<ptxt>Align the axis of the connector with the axis of the pipe. Push the pipe into the connector, and then push up on the retainer.</ptxt>
<figure>
<graphic graphicname="A125397E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Retainer</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Up</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before connecting the tube connectors to the pipes, check if there is any damage or foreign matter in the connectors.</ptxt>
</item>
<item>
<ptxt>After connecting the pipe and the connector, check that the pipe and connector are securely connected by trying to pull them apart.</ptxt>
</item>
</list1>
</atten3>
<figure>
<graphic graphicname="A227055" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when handling the nylon tube.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Be careful not to turn the connection part of the nylon tube and the quick connector when connecting them.</ptxt>
</item>
<item>
<ptxt>Do not kink the nylon tube.</ptxt>
</item>
<item>
<ptxt>Do not remove the EPDM protector on the outside of the nylon tube.</ptxt>
</item>
<item>
<ptxt>Do not bend the nylon tube as this may cause blockage.</ptxt>
</item>
</list1>
</atten3>
</step2>
<step2>
<ptxt>Observe these precautions when disconnecting the fuel tank breather tube connector:</ptxt>
<step3>
<ptxt>Pinch the retainer and pull out the fuel tank breather tube connector with the fuel tank breather tube connector pushed toward the pipe to disconnect the fuel tank breather tube from the pipe.</ptxt>
<figure>
<graphic graphicname="A224871E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Breather Tube Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pinch</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Remove dirt or foreign objects on the fuel tank breather tube connector before performing this procedure.</ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign objects on the parts when disconnecting them as the fuel tank breather tube connector has the O-ring that seals the pipe.</ptxt>
</item>
<item>
<ptxt>Perform this work by hand. Do not use any tools.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, kink or twist the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the connecting part by covering it with a plastic bag after disconnecting the fuel tank breather tube.</ptxt>
</item>
<item>
<ptxt>If the fuel tank breather tube connector and pipe are stuck, push and pull them to release them.</ptxt>
</item>
</list1>
</atten3>
<figure>
<graphic graphicname="A224872E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Fuel Tank Breather Tube Connector</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pipe</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Nylon Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Observe these precautions when connecting the fuel tank breather tube connector:</ptxt>
<step3>
<ptxt>Align the fuel tank breather tube connector with the pipe, and then push in the fuel tank breather tube connector until the retainer makes a "click" sound to connect the fuel tank breather tube to the pipe.</ptxt>
<figure>
<graphic graphicname="A224875" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Push</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check that there are no scratches or foreign objects on the connected part of the fuel tank breather tube connector and pipe before performing this procedure.</ptxt>
</item>
<item>
<ptxt>After connecting the fuel tank breather tube, check that the fuel tank breather tube is securely connected by pulling the fuel tank breather tube connector.</ptxt>
</item>
</list1>
</atten3>
</step3>
<step3>
<ptxt>After connecting the pipe and the connector, check that the pipe and connector are securely connected by trying to pull them apart.</ptxt>
<figure>
<graphic graphicname="A224876" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Pull</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Inspect for fuel leaks.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>INSPECT FOR FUEL LEAK</ptxt>
<step2>
<ptxt>Inspect for fuel leaks (See page <xref label="Seep01" href="RM000000Q4609FX_02_0091"/>).</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>