<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B98Z_T00IN" variety="T00IN">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM00000289I05LX" category="J" type-id="804HE" name-id="AV9PM-03" from="201210">
<dtccode/>
<dtcname>Multi-media Interface ECU Communication Error</dtcname>
<subpara id="RM00000289I05LX_03" type-id="32" category="03" proc-id="RM23G0E___0000BQJ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E195968E04" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000289I05LX_01" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM00000289I05LX_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000289I05LX_02_0001" proc-id="RM23G0E___0000BQE00001">
<testtitle>IDENTIFY COMPONENT SHOWN BY SUB-CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter diagnostic mode.</ptxt>
<figure>
<graphic graphicname="E146440E06" width="7.106578999in" height="6.791605969in"/>
</figure>
</test1>
<test1>
<ptxt>Press preset switch "channel 2" to change to "Detailed Information Mode".</ptxt>
</test1>
<test1>
<ptxt>Identify the component shown by the sub-code.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>"190 (radio receiver assembly)" is the component shown by the sub-code in the example shown in the illustration.</ptxt>
</item>
<item>
<ptxt>For details of the DTC display, refer to DTC Check/Clear (See page <xref label="Seep01" href="RM0000014CZ0CBX"/>).</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000289I05LX_02_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000289I05LX_02_0002" proc-id="RM23G0E___0000BQF00001">
<testtitle>CHECK POWER SOURCE CIRCUIT OF COMPONENT SHOWN BY SUB-CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the power source circuit of the component shown by the sub-code.</ptxt>
<ptxt>If the power source circuit is operating normally, proceed to the next step.</ptxt>
<atten4>
<ptxt>The satellite radio tuner is built into the radio receiver assembly. If there is a problem between the satellite radio tuner and radio receiver, replace the radio receiver assembly.</ptxt>
</atten4>
<table pgwide="1">
<title>Component Table</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="4.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Radio receiver assembly (190)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Radio Receiver Power Source Circuit (See page <xref label="Seep01" href="RM0000012CI0IWX"/>)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>"Bluetooth" handsfree module (19D)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Stereo component amplifier assembly (440)*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stereo Component Amplifier Power Source Circuit (See page <xref label="Seep02" href="RM0000012CJ0CKX"/>)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: for 9 Speakers</ptxt>
</item>
</list1>
</test1>
</content6>
<res>
<down ref="RM00000289I05LX_02_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000289I05LX_02_0003" proc-id="RM23G0E___0000BQG00001">
<testtitle>INSPECT RADIO RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H38* and H36 radio receiver assembly connectors.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*: for 9 Speakers</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="E145834E32" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.31in"/>
<colspec colname="COL2" colwidth="1.45in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H38-5 (ATX+) - H38-15 (ATX-)*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H36-9 (TXM+) - H36-10 (TXM-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>*: for 9 Speakers</ptxt>
</item>
</list1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 9 Speakers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/o Accessory Meter)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/ Accessory Meter)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000289I05LX_02_0004" fin="false">A</down>
<right ref="RM00000289I05LX_02_0007" fin="true">B</right>
<right ref="RM00000289I05LX_02_0010" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000289I05LX_02_0004" proc-id="RM23G0E___0000BQH00001">
<testtitle>CHECK HARNESS AND CONNECTOR (RADIO RECEIVER - COMPONENT SHOWN BY SUB-CODE)</testtitle>
<content6 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Start the check from the circuit that is near the component shown by the sub-code first.</ptxt>
</item>
<item>
<ptxt>For details of the connectors, refer to Terminals of ECU (See page <xref label="Seep01" href="RM0000012A70CHX"/>).</ptxt>
</item>
</list1>
</atten4>
<test1>
<ptxt>Referring to the wiring diagram, check the AVC-LAN circuit between the multi-media interface ECU and component shown by the sub-code.</ptxt>
<test2>
<ptxt>Disconnect all connectors between the multi-media interface ECU and component shown by the sub-code.</ptxt>
</test2>
<test2>
<ptxt>Check for an open or short in the AVC-LAN circuit between the multi-media interface ECU and component shown by the sub-code.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no open or short circuit.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000289I05LX_02_0005" fin="false">OK</down>
<right ref="RM00000289I05LX_02_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000289I05LX_02_0005" proc-id="RM23G0E___0000BQI00001">
<testtitle>REPLACE COMPONENT SHOWN BY SUB-CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the component shown by the sub-code with a known good one and check if the same problem occurs again.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Malfunction disappears.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000289I05LX_02_0006" fin="true">OK</down>
<right ref="RM00000289I05LX_02_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000289I05LX_02_0006">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM00000289I05LX_02_0007">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01QX_01_0011"/>
</testtitle>
</testgrp>
<testgrp id="RM00000289I05LX_02_0010">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01QX_01_0028"/>
</testtitle>
</testgrp>
<testgrp id="RM00000289I05LX_02_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000289I05LX_02_0009">
<testtitle>REPLACE MULTI-MEDIA INTERFACE ECU<xref label="Seep01" href="RM00000427800ZX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>