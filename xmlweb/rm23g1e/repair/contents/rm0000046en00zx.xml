<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000M" variety="S000M">
<name>2TR-FE INTAKE / EXHAUST</name>
<ttl id="12010_S000M_7B90X_T00AL" variety="T00AL">
<name>INTAKE MANIFOLD</name>
<para id="RM0000046EN00ZX" category="A" type-id="80001" name-id="IE1YA-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000046EN00ZX_01" type-id="01" category="01">
<s-1 id="RM0000046EN00ZX_01_0007" proc-id="RM23G0E___00006GM00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000046EN00ZX_01_0005" proc-id="RM23G0E___00006GL00001">
<ptxt>REMOVE FRONT FENDER APRON SEAL LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C213692" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 5 clips and front fender apron seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EN00ZX_01_0006" proc-id="RM23G0E___00005CA00001">
<ptxt>REMOVE FRONT NO. 1 FENDER APRON TO FRAME SEAL LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C213693" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 5 clips and front fender apron to frame seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EN00ZX_01_0009" proc-id="RM23G0E___00006GO00000">
<ptxt>REMOVE TRANSMISSION OIL FILLER TUBE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C213708" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the transmission oil dipstick.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and oil filler tube.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the oil filler tube.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046EN00ZX_01_0008" proc-id="RM23G0E___00006GN00001">
<ptxt>REMOVE STARTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1.4 kW Type:</ptxt>
<ptxt>Remove the starter (See page <xref label="Seep01" href="RM000003REY00HX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 2.0 kW Type:</ptxt>
<ptxt>Remove the starter (See page <xref label="Seep02" href="RM00000179M00KX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EN00ZX_01_0001" proc-id="RM23G0E___00006GI00001">
<ptxt>REMOVE THROTTLE WITH MOTOR BODY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the throttle with motor body (See page <xref label="Seep01" href="RM000000VWO02EX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EN00ZX_01_0002" proc-id="RM23G0E___00006GJ00001">
<ptxt>REMOVE FUEL DELIVERY PIPE WITH FUEL INJECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel delivery pipe with fuel injector (See page <xref label="Seep01" href="RM000000Q4809LX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EN00ZX_01_0003" proc-id="RM23G0E___000054V00000">
<ptxt>REMOVE PURGE VSV
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the purge VSV connector.</ptxt>
<figure>
<graphic graphicname="A223339" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 2 purge line hoses from the purge VSV.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and purge VSV.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046EN00ZX_01_0004" proc-id="RM23G0E___00006GK00001">
<ptxt>REMOVE INTAKE MANIFOLD</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C213694" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>for Engine Front Side:</ptxt>
<ptxt>Detach the 2 wire harness clamps from the 2 wire harness clamp brackets.</ptxt>
</s2>
<s2>
<ptxt>for Engine Rear Side:</ptxt>
<ptxt>Detach the No. 2 water by-pass hose and disconnect the No. 3 ventilation hose from the intake manifold.</ptxt>
<figure>
<graphic graphicname="C213695" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 5 bolts, 2 nuts and intake manifold.</ptxt>
<figure>
<graphic graphicname="C213696" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 wire harness clamp brackets and No. 2 fuel vapor feed hose from the intake manifold.</ptxt>
<figure>
<graphic graphicname="C213698" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>