<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94H_T00E5" variety="T00E5">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1KD-FTV)</name>
<para id="RM0000012XR07SX" category="C" type-id="304TL" name-id="AT7M5-13" from="201210">
<dtccode>P2742</dtccode>
<dtcname>Transmission Fluid Temperature Sensor "B" Circuit Low Input</dtcname>
<dtccode>P2743</dtccode>
<dtcname>Transmission Fluid Temperature Sensor "B" Circuit High Input</dtcname>
<subpara id="RM0000012XR07SX_01" type-id="60" category="03" proc-id="RM23G0E___000086W00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The Automatic Transmission Fluid (ATF) temperature sensor is on the transmission, just in front of the oil cooler inlet pipeline.</ptxt>
<ptxt>If the TCM detects an abnormally high ATF temperature near this sensor, it illuminates the warning light.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The temperature of the ATF easily rises when towing, climbing hills, in traffic, etc.</ptxt>
</item>
<item>
<ptxt>If the ATF temperature sensor becomes short-circuited, a signal that indicates that the ATF temperature is 150°C (302°F) or higher is input into the TCM.</ptxt>
</item>
</list1>
</atten4>
<ptxt>Vehicle conditions when the sensor is normal and when the sensor is short-circuited are indicated in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>No. 2 ATF Temperature Sensor State</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Recovery Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>Sensor is normal</ptxt>
</entry>
<entry valign="middle">
<ptxt>ATF temperature higher than 150°C (302°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>ATF temperature warning light remains on</ptxt>
</entry>
<entry valign="middle">
<ptxt>ATF temperature below 135°C (275°F)*1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ATF temperature higher than 130°C (266°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift point too high</ptxt>
</entry>
<entry valign="middle">
<ptxt>ATF temperature below 110°C (230°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>When conditions (a) and (b) are satisfied:</ptxt>
<ptxt>(a) ATF temperature higher than 130°C (266°F)</ptxt>
<ptxt>(b) Engine coolant temperature higher than 95°C (203°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up in 3rd gear*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ATF temperature below 110°C (230°F)*1 and engine coolant temperature higher than 95°C (203°F)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Sensor is short-circuited</ptxt>
</entry>
<entry valign="middle">
<ptxt>Any conditions</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ATF temperature warning light blinks</ptxt>
</item>
<item>
<ptxt>Shift point too high</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Symptoms still occur</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine coolant temperature higher than 95°C (203°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up in 3rd gear*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Symptoms still occur</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*1: When the ATF is normal, it decreases to below 135°C (275°F) within 5 minutes with the shift lever in P or N in an idling state.</ptxt>
<ptxt>*2: When ATF temperature is normal, transmission lock-up occurs in 5th gear with the shift lever in D or with the S5 range selected, and in 4th gear with the S4 range selected.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2742</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 ATF temperature sensor resistance is below 25 Ω (0.046 V) for 0.5 sec. or more (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in No. 2 ATF temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>No. 2 ATF temperature sensor (transmission wire)</ptxt>
</item>
<item>
<ptxt>TCM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2743</ptxt>
</entry>
<entry valign="middle">
<ptxt>When 15 min. or more have elapsed after the engine is started, No. 2 ATF temperature sensor resistance is higher than 156 kΩ (4.915 V) for 0.5 sec. or more (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in No. 2 ATF temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>No. 2 ATF temperature sensor (transmission wire)</ptxt>
</item>
<item>
<ptxt>TCM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000012XR07SX_02" type-id="64" category="03" proc-id="RM23G0E___000086X00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ATF temperature sensor converts the ATF temperature into an electrical resistance value. Based on the resistance, the TCM determines the ATF temperature and detects an open or short in the ATF temperature circuit. If the resistance value of the ATF temperature sensor is below 25 Ω*1 or higher than 156 kΩ*2, the TCM interprets this as a fault in the ATF sensor or wiring. The TCM will blink the ATF temperature warning light and store the DTC*3. The TCM will illuminate the MIL and store the DTC*4.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: 150°C (302°F) or higher is indicated regardless of the actual ATF temperature.</ptxt>
</item>
<item>
<ptxt>*2: -40°C (-40°F) is indicated regardless of the actual ATF temperature.</ptxt>
</item>
<item>
<ptxt>The ATF temperature can be checked on the intelligent tester display.</ptxt>
</item>
<item>
<ptxt>*3: w/ DPF</ptxt>
</item>
<item>
<ptxt>*4: w/o DPF</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM0000012XR07SX_03" type-id="32" category="03" proc-id="RM23G0E___000086Y00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0712 (See page <xref label="Seep01" href="RM0000013US01ZX_03"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000012XR07SX_04" type-id="51" category="05" proc-id="RM23G0E___000086Z00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A/T Oil Temperature 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 ATF temperature sensor value/</ptxt>
<ptxt>Min.: -40.0°C (-40.0°F)</ptxt>
<ptxt>Max.: 215.0°C (419.0°F)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>After stall test: Approximately 80°C (176°F)</ptxt>
</item>
<item>
<ptxt>With engine cold: Equal to ambient temperature</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F) or 215°C (419°F), the No. 2 ATF temperature sensor circuit is open or shorted.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When DTC P2742 is output and the intelligent tester output is 150°C (302°F) or higher, there is a short circuit.</ptxt>
</item>
<item>
<ptxt>When DTC P2743 is output and the intelligent tester output is -40°C (-40°F), there is an open circuit.</ptxt>
<ptxt>Check the temperature displayed on the tester in order to check if a malfunction exists.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Malfunction</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Open circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>150°C (302°F) or higher</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Short circuit</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM0000012XR07SX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012XR07SX_05_0001" proc-id="RM23G0E___000087000001">
<testtitle>INSPECT TRANSMISSION WIRE (NO. 2 ATF TEMPERATURE SENSOR)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C214326E08" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the C30 transmission wire connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>1 (OT2-) - 9 (OT2+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>25 Ω to 156 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>1 (OT2-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>9 (OT2+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>If the resistance is out of the specified range at one of the ATF temperatures shown in the table below, the driveability of the vehicle may decrease.</ptxt>
</atten4>
<spec>
<title>Resistance (Reference)</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>ATF Temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>10°C (50°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 to 8 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.5 to 4.5 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>110°C (230°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.22 to 0.28 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Transmission Wire)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM0000012XR07SX_05_0002" fin="false">OK</down>
<right ref="RM0000012XR07SX_05_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012XR07SX_05_0002" proc-id="RM23G0E___000087100001">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSMISSION WIRE - TCM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="C212047E10" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Disconnect the G69 TCM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="left">
<ptxt>G69-24 (THO2) - G69-23 (E2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>25 Ω to 156 kΩ</ptxt>
</entry>
</row>
<row>
<entry align="left">
<ptxt>G69-24 (THO2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry align="left">
<ptxt>G69-23 (E2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to TCM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000012XR07SX_05_0005" fin="true">OK</down>
<right ref="RM0000012XR07SX_05_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012XR07SX_05_0003">
<testtitle>REPAIR OR REPLACE NO. 2 ATF TEMPERATURE SENSOR (TRANSMISSION WIRE)<xref label="Seep01" href="RM0000013C1049X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012XR07SX_05_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000012XR07SX_05_0005">
<testtitle>REPLACE TCM<xref label="Seep01" href="RM0000048F300DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>