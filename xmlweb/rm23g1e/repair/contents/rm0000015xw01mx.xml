<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12013_S0010" variety="S0010">
<name>1KD-FTV STARTING</name>
<ttl id="12013_S0010_7B939_T00CX" variety="T00CX">
<name>STARTER (for 3.0 kW Type)</name>
<para id="RM0000015XW01MX" category="G" type-id="3001K" name-id="ST3ZG-01" from="201207">
<name>INSPECTION</name>
<subpara id="RM0000015XW01MX_02" type-id="01" category="01">
<s-1 id="RM0000015XW01MX_02_0006" proc-id="RM23G0E___000073Z00000">
<ptxt>INSPECT STARTER</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>As a large electric current passes through the cable during this inspection, a thick cable must be used. If not, the cable may become hot and cause injury.</ptxt>
</atten2>
<atten3>
<ptxt>These tests must be performed within 3 to 5 seconds to prevent the coil from burning out.</ptxt>
</atten3>
<s2>
<ptxt>Mount the starter in a vise between aluminum plates.</ptxt>
</s2>
<s2>
<ptxt>Perform pull-in test.</ptxt>
<s3>
<ptxt>Remove the nut and disconnect the lead wire from terminal C.</ptxt>
<figure>
<graphic graphicname="A146228E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Terminal C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Connect the battery to the magnet starter switch as shown in the illustration. Then check that the clutch pinion gear moves outward.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Terminal C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Terminal 50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the clutch pinion gear does not move, inspect the magnet starter switch assembly. If the magnet starter switch assembly is not as specified, replace it.</ptxt>
<figure>
<graphic graphicname="A222481E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Perform a holding test.</ptxt>
<s3>
<ptxt>When the battery is connected as above with the clutch pinion gear out, disconnect the negative (-) lead from terminal C. Check that the pinion gear remains out.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Terminal C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the clutch pinion gear returns inward, inspect the magnet starter switch assembly. If the magnet starter switch assembly is not as specified, replace it.</ptxt>
<figure>
<graphic graphicname="A222482E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Inspect the clutch pinion gear return.</ptxt>
<s3>
<ptxt>Disconnect the negative (-) lead from the starter body. Check that the clutch pinion gear returns inward.</ptxt>
<figure>
<graphic graphicname="A222483" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>If the clutch pinion gear does not return inward, inspect the magnet starter switch assembly. If the magnet starter switch assembly is not as specified, replace it.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Perform an operation test without load.</ptxt>
<s3>
<ptxt>Connect the lead wire to terminal C with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>24</t-value1>
<t-value2>245</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Connect the battery and an ammeter to the starter as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A222484E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Check that the starter rotates smoothly and steadily while the pinion gear extended. Then measure the current.</ptxt>
<spec>
<title>Standard current</title>
<specitem>
<ptxt>220 A or less at 11.5 V</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Terminal 50</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Terminal 30</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, inspect the starter assembly</ptxt>
<atten4>
<ptxt>Inspect the starter brush holder assembly, starter yoke assembly and starter armature assembly. If there is a malfunction, replace the part and perform this test again.</ptxt>
</atten4>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000015XW01MX_02_0012" proc-id="RM23G0E___000074500000">
<ptxt>INSPECT MAGNET STARTER SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the pull-in coil for an open circuit.</ptxt>
<figure>
<graphic graphicname="A146237E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal 50 - Terminal C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Terminal 50</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Terminal C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the magnet starter switch assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check the hold-in coil for an open circuit.</ptxt>
<figure>
<graphic graphicname="A146238E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal 50 - Starter switch body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 2 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Terminal 50</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the magnet starter switch assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000015XW01MX_02_0009" proc-id="RM23G0E___000074200000">
<ptxt>INSPECT BRUSH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A102612" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a vernier caliper, measure the brush length.</ptxt>
<spec>
<title>Standard length</title>
<specitem>
<ptxt>21.0 mm (0.828 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum length</title>
<specitem>
<ptxt>12.0 mm (0.472 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the length is less than the minimum, replace the starter brush holder assembly and starter yoke assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000015XW01MX_02_0010" proc-id="RM23G0E___000074300000">
<ptxt>INSPECT STARTER BRUSH HOLDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the brush insulation.</ptxt>
<figure>
<graphic graphicname="A146236" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Positive (+) brush holder - Negative (-) brush holder</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the starter brush holder assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Take a pull scale reading the instant the brush spring separates from the brush.</ptxt>
<figure>
<graphic graphicname="A102613" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard spring load</title>
<specitem>
<ptxt>28 to 37 N (3 to 4 kgf, 6.3 to 8.3 lbf)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum spring load</title>
<specitem>
<ptxt>15 N (2 kgf, 3.4 lbf)</ptxt>
</specitem>
</spec>
<ptxt>If the spring load is less than the minimum, replace the starter brush holder assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000015XW01MX_02_0008" proc-id="RM23G0E___000074100000">
<ptxt>INSPECT STARTER YOKE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the field coil for an open circuit.</ptxt>
<figure>
<graphic graphicname="A146234" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal C - Brush</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the starter yoke assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the field coil for ground.</ptxt>
<figure>
<graphic graphicname="A146235" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal C - Starter yoke body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If the result is not as specified, replace the starter yoke assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000015XW01MX_02_0007" proc-id="RM23G0E___000074000000">
<ptxt>INSPECT STARTER ARMATURE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the commutator for dirt and/or burns on the surface.</ptxt>
<ptxt>If the surface is dirty or burnt, restore the surface with sandpaper (No. 400) or on a lathe. If necessary, replace the starter armature assembly.</ptxt>
</s2>
<s2>
<ptxt>Inspect the commutator for an open circuit.</ptxt>
<figure>
<graphic graphicname="A146232E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Segment - Segment</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Segment</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the starter armature assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the commutator for a short circuit.</ptxt>
<figure>
<graphic graphicname="A146233E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Segment - Coil core</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Coil Core</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Segment</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the starter armature assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the commutator for circle runout.</ptxt>
<figure>
<graphic graphicname="A150703" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Place the armature on V-blocks.</ptxt>
</s3>
<s3>
<ptxt>Using a dial gauge, measure the circle runout.</ptxt>
<spec>
<title>Maximum circle runout</title>
<specitem>
<ptxt>0.05 mm (0.00197 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the circle runout is more than the maximum, replace the starter armature assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the commutator diameter.</ptxt>
<figure>
<graphic graphicname="A102608" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard commutator diameter</title>
<specitem>
<ptxt>43.0 mm (1.69 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum commutator diameter</title>
<specitem>
<ptxt>42.0 mm (1.65 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is less than the minimum, replace the starter armature assembly.</ptxt>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the undercut depth of the commutator.</ptxt>
<figure>
<graphic graphicname="A102609" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard undercut depth</title>
<specitem>
<ptxt>0.7 mm (0.0276 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum undercut depth</title>
<specitem>
<ptxt>0.2 mm (0.00787 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the undercut depth is less than the minimum, replace the starter armature assembly.</ptxt>
</s2>
<s2>
<ptxt>Inspect the bearings.</ptxt>
<s3>
<ptxt>Check that the bearings rotate smoothly.</ptxt>
<ptxt>If the result is not as specified, replace the starter armature assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000015XW01MX_02_0011" proc-id="RM23G0E___000074400000">
<ptxt>INSPECT STARTER CLUTCH SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Rotate the pinion gear clockwise and check that it turns freely. Try to rotate the pinion gear counterclockwise and check that it locks.</ptxt>
<figure>
<graphic graphicname="A115056" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Free</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Lock</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the starter brush holder assembly.</ptxt>
</s2>
<s2>
<ptxt>Turn the pinion gear by hand while applying inward force and check the movement of the bearing.</ptxt>
<ptxt>If the result is not as specified, replace the starter brush holder assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>