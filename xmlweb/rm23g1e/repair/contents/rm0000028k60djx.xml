<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM0000028K60DJX" category="C" type-id="302LP" name-id="ESM9L-06" from="201207" to="201210">
<dtccode>P2237</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit / Open (Bank 1 Sensor 1)</dtcname>
<dtccode>P2238</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P2239</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P2252</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P2253</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit High (Bank 1 Sensor 1)</dtcname>
<subpara id="RM0000028K60DJX_02" type-id="60" category="03" proc-id="RM23G0E___00003FX00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40PBX_01"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Although the DTC titles refer to the oxygen sensor, these DTCs relate to the air fuel ratio sensor.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor mounted in front of the three way catalytic converter and located near the engine assembly.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2237</ptxt>
</entry>
<entry valign="middle">
<ptxt>An open in the circuit between terminals A1A+ and A1A- of the air fuel ratio sensor while the engine is running (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2238</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met (2 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>The air fuel ratio sensor output drops while the engine is running.</ptxt>
</item>
<item>
<ptxt>The voltage at terminal A1A+ is 0.5 V or less.</ptxt>
</item>
<item>
<ptxt>The voltage difference between terminals A1A+ and A1A- is 0.1 V or less.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2239</ptxt>
</entry>
<entry valign="middle">
<ptxt>The A1A+ voltage is higher than 4.5 V (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2252</ptxt>
</entry>
<entry valign="middle">
<ptxt>The A1A- voltage is 0.5 V or less (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2253</ptxt>
</entry>
<entry valign="middle">
<ptxt>A1A- voltage is higher than 4.5 V (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000028K60DJX_08" type-id="32" category="03" proc-id="RM23G0E___00003FY00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40PBX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000028K60DJX_09" type-id="51" category="05" proc-id="RM23G0E___00003FZ00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Intelligent tester only:</ptxt>
<ptxt>Malfunctioning areas can be identified by performing the Control the Injection Volume for A/F Sensor function provided in the Active Test. The Control the Injection Volume for A/F Sensor function can help to determine whether the air fuel ratio sensor, heated oxygen sensor and other potential trouble areas are malfunctioning.</ptxt>
<ptxt>The following instructions describe how to conduct the Control the Injection Volume for A/F Sensor operation using the intelligent tester.</ptxt>
<list1 type="ordered">
<item>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Start the engine.</ptxt>
</item>
<item>
<ptxt>Turn the tester on.</ptxt>
</item>
<item>
<ptxt>Warm up the engine at an engine speed of 2500 rpm for approximately 90 seconds.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F Sensor.</ptxt>
</item>
<item>
<ptxt>Perform the Active Test operation with the engine idling (press the RIGHT or LEFT button to change the fuel injection volume).</ptxt>
</item>
<item>
<ptxt>Monitor the output voltages of the air fuel ratio sensor and heated oxygen sensor (AFS Voltage B1S1 and O2S B1S2) displayed on the tester.</ptxt>
</item>
</list1>
</atten4>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The Control the Injection Volume for A/F Sensor operation lowers the fuel injection volume by 12.5% or increases the injection volume by 12.5%.</ptxt>
</item>
<item>
<ptxt>Each sensor reacts in accordance with increases and decreases in the fuel injection volume.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display (Sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 3.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 3.4 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 0.55 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The air fuel ratio sensor has an output delay of a few seconds and the heated oxygen sensor has a maximum output delay of approximately 20 seconds.</ptxt>
</atten3>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" align="center" colwidth="0.50in"/>
<colspec colname="COLSPEC0" colwidth="2.76in"/>
<colspec colname="COLSPEC1" colwidth="2.76in"/>
<colspec colname="COL4" align="center" colwidth="1.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Case</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air Fuel Ratio Sensor</ptxt>
<ptxt>Output Voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Heated Oxygen Sensor</ptxt>
<ptxt>Output Voltage</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main Suspected Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt> 1</ptxt>
</entry>
<entry align="center">
<graphic graphicname="A162605E23" width="2.7559055118110236in" height="1.7716535433070868in"/>
</entry>
<entry align="center">
<graphic graphicname="A162598E15" width="2.7559055118110236in" height="1.7716535433070868in"/>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt> 2</ptxt>
</entry>
<entry align="center">
<graphic graphicname="A162596E18" width="2.7559055118110236in" height="1.7716535433070868in"/>
</entry>
<entry align="center">
<graphic graphicname="A162598E15" width="2.7559055118110236in" height="1.7716535433070868in"/>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor heater</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt> 3</ptxt>
</entry>
<entry align="center">
<graphic graphicname="A162605E23" width="2.7559055118110236in" height="1.7716535433070868in"/>
</entry>
<entry align="center">
<graphic graphicname="A162596E18" width="2.7559055118110236in" height="1.7716535433070868in"/>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor heater</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt> 4</ptxt>
</entry>
<entry align="center">
<graphic graphicname="A162596E18" width="2.7559055118110236in" height="1.7716535433070868in"/>
</entry>
<entry align="center">
<graphic graphicname="A162596E18" width="2.7559055118110236in" height="1.7716535433070868in"/>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Fuel pressure</ptxt>
</item>
<item>
<ptxt>Gas leaks from exhaust system</ptxt>
<ptxt>(air fuel ratio extremely lean or rich)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Performing the Control the Injection Volume for A/F Sensor enables technicians to check and graph the output voltage of both the air fuel ratio sensor and heated oxygen sensor.</ptxt>
</item>
<item>
<ptxt>To display the graph, enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F Sensor / AFS Voltage B1S1 and O2S B1S2.</ptxt>
</item>
</list1>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000028K60DJX_10" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000028K60DJX_10_0001" proc-id="RM23G0E___00003G000000">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR FUEL RATIO SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C56-3 (A1A+) - C63-7 (A1A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C56-4 (A1A-) - C63-1 (A1A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C56-3 (A1A+) or C63-7 (A1A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C56-4 (A1A-) or C63-1 (A1A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the air fuel ratio sensor connector.</ptxt>
<figure>
<graphic graphicname="A227661E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</test1>
</content6>
<res>
<down ref="RM0000028K60DJX_10_0002" fin="false">OK</down>
<right ref="RM0000028K60DJX_10_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000028K60DJX_10_0002" proc-id="RM23G0E___00003G100000">
<testtitle>REPLACE AIR FUEL RATIO SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air fuel ratio sensor (See page <xref label="Seep01" href="RM00000175900FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000028K60DJX_10_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000028K60DJX_10_0003" proc-id="RM23G0E___00003G200000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P2237, P2238, P2239, P2252 OR P2253)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z8X"/>).</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode (See page <xref label="Seep02" href="RM000000PDL0PHX"/>).</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle according to Confirmation Driving Pattern (See page <xref label="Seep03" href="RM000000WC40PBX_08"/>).</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs (Pending DTCs).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC P2237, P2238, P2239, P2252 or P2253 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000028K60DJX_10_0004" fin="true">A</down>
<right ref="RM0000028K60DJX_10_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000028K60DJX_10_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (AIR FUEL RATIO SENSOR - ECM)</testtitle>
</testgrp>
<testgrp id="RM0000028K60DJX_10_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000028K60DJX_10_0004">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>