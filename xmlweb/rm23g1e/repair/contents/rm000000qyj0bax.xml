<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B9_T00KX" variety="T00KX">
<name>ENGINE IMMOBILISER SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000QYJ0BAX" category="C" type-id="302EP" name-id="TD21V-14" from="201210">
<dtccode>B2784</dtccode>
<dtcname>Antenna Coil Open / Short</dtcname>
<subpara id="RM000000QYJ0BAX_01" type-id="60" category="03" proc-id="RM23G0E___0000EZD00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when there is an open or short malfunction in the transponder key amplifier coil (built into the engine switch).</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B2784</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The transponder key amplifier coil is open/shorted.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Certification ECU</ptxt>
</item>
<item>
<ptxt>Engine switch</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000QYJ0BAX_03" type-id="51" category="05" proc-id="RM23G0E___0000EZE00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Before replacing the certification ECU, refer to the Service Bulletin.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000QYJ0BAX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000QYJ0BAX_04_0003" proc-id="RM23G0E___0000EZF00001">
<testtitle>REPLACE ENGINE SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the engine switch with a new or normally functioning one.</ptxt>
<list1 type="unordered">
<item>
<ptxt>for 1GR-FE (for LHD): (See page <xref label="Seep01" href="RM000002YWC02ZX"/>).</ptxt>
</item>
<item>
<ptxt>for 1GR-FE (for RHD): (See page <xref label="Seep02" href="RM000002YWC030X"/>).</ptxt>
</item>
<item>
<ptxt>for 1KD-FTV (for LHD): (See page <xref label="Seep03" href="RM000002YWC031X"/>).</ptxt>
</item>
<item>
<ptxt>for 1KD-FTV (for RHD): (See page <xref label="Seep04" href="RM000002YWC032X"/>).</ptxt>
</item>
</list1>
</test1>
</content6>
<res>
<down ref="RM000000QYJ0BAX_04_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000QYJ0BAX_04_0010" proc-id="RM23G0E___0000EZH00001">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000QYC058X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000QYJ0BAX_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000QYJ0BAX_04_0007" proc-id="RM23G0E___0000EZG00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000QYC058X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2784 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000QYJ0BAX_04_0006" fin="true">OK</down>
<right ref="RM000000QYJ0BAX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000QYJ0BAX_04_0004">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000000QYJ0BAX_04_0006">
<testtitle>END (ENGINE SWITCH IS DEFECTIVE)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>