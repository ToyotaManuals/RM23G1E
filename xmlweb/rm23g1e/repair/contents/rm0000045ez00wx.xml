<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000D" variety="S000D">
<name>1GR-FE FUEL</name>
<ttl id="12008_S000D_7B8YG_T0084" variety="T0084">
<name>FUEL TANK</name>
<para id="RM0000045EZ00WX" category="A" type-id="80001" name-id="FU8FV-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000045EZ00WX_01" type-id="01" category="01">
<s-1 id="RM0000045EZ00WX_01_0001" proc-id="RM23G0E___00005FN00001">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Discharge the fuel system pressure (See page <xref label="Seep01" href="RM0000028RU03SX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0002" proc-id="RM23G0E___00005FO00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0020" proc-id="RM23G0E___00005G500001">
<ptxt>REMOVE REAR SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 60/40 Split Double-folding Seat Type LH Side:</ptxt>
<ptxt>Remove the rear seat assembly LH (See page <xref label="Seep01" href="RM00000468P00JX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 60/40 Split Slide Walk-in Seat Type LH Side:</ptxt>
<ptxt>Remove the rear seat assembly LH (See page <xref label="Seep02" href="RM00000468A00LX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0003" proc-id="RM23G0E___00005FP00001">
<ptxt>REMOVE REAR FLOOR SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws and rear floor service hole cover.</ptxt>
<figure>
<graphic graphicname="A220797" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the fuel pump and sender gauge connector.</ptxt>
<figure>
<graphic graphicname="A228323E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Single Tank Type</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Double Tank Type</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0004" proc-id="RM23G0E___00005FQ00001">
<ptxt>REMOVE NO. 1 FUEL TANK PROTECTOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 6 bolts and No. 1 fuel tank protector.</ptxt>
<figure>
<graphic graphicname="A220805" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for Half Cover Type:</ptxt>
<ptxt>Remove the 4 bolts and No. 1 fuel tank protector.</ptxt>
<figure>
<graphic graphicname="A224491" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0005" proc-id="RM23G0E___00005FR00001">
<ptxt>DISCONNECT FUEL TANK MAIN TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel tank main tube (See page <xref label="Seep01" href="RM0000028RU03SX"/>).</ptxt>
<figure>
<graphic graphicname="A220806" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0022" proc-id="RM23G0E___00005G700001">
<ptxt>DRAIN FUEL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Turn the intelligent tester on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s2>
<s2>
<ptxt>Operate the fuel pump and drain the fuel from the fuel tank main tube.</ptxt>
<atten2>
<list1 type="unordered">
<item>
<ptxt>Do not smoke or be near an open flame when working on the fuel system.</ptxt>
</item>
<item>
<ptxt>Secure good ventilation.</ptxt>
</item>
<item>
<ptxt>Keep gasoline away from rubber or leather parts.</ptxt>
</item>
</list1>
</atten2>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0006" proc-id="RM23G0E___00005FS00001">
<ptxt>DISCONNECT FUEL RETURN TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel return tube (See page <xref label="Seep01" href="RM0000028RU03SX"/>).</ptxt>
<figure>
<graphic graphicname="A223269" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0007" proc-id="RM23G0E___00005FT00001">
<ptxt>DISCONNECT FUEL CUT OFF TUBE (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel cut off tube (See page <xref label="Seep01" href="RM0000028RU03SX"/>).</ptxt>
<figure>
<graphic graphicname="A220810" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0008" proc-id="RM23G0E___00005FU00001">
<ptxt>DISCONNECT FUEL CUT OFF TUBE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel cut off tube (See page <xref label="Seep01" href="RM0000028RU03SX"/>).</ptxt>
<figure>
<graphic graphicname="A220808" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0009" proc-id="RM23G0E___00005FV00001">
<ptxt>DISCONNECT FUEL TANK BREATHER TUBE SUB-ASSEMBLY (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel tank breather tube (See page <xref label="Seep01" href="RM0000028RU03SX"/>).</ptxt>
<figure>
<graphic graphicname="A223270" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0010" proc-id="RM23G0E___00005FW00001">
<ptxt>DISCONNECT FUEL TANK BREATHER TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel tank breather tube (See page <xref label="Seep01" href="RM0000028RU03SX"/>).</ptxt>
<figure>
<graphic graphicname="A223271" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0011" proc-id="RM23G0E___00005FX00001">
<ptxt>DISCONNECT NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 fuel main tube (See page <xref label="Seep01" href="RM0000028RU03SX"/>).</ptxt>
<figure>
<graphic graphicname="A223272" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0012" proc-id="RM23G0E___00005FY00001">
<ptxt>DISCONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel tank to filler pipe hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A220811" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0013" proc-id="RM23G0E___00005FZ00001">
<ptxt>REMOVE FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place a transmission jack under the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 clips, 2 pins and 2 fuel tank bands.</ptxt>
<figure>
<graphic graphicname="A220832" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Slowly lower the transmission jack slightly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0014" proc-id="RM23G0E___00005G000001">
<ptxt>REMOVE FUEL TANK CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 1, No. 2 and No. 3 fuel tank cushions from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A220812E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Fuel Tank Cushion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Fuel Tank Cushion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3 Fuel Tank Cushion</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0015" proc-id="RM23G0E___00005G100001">
<ptxt>REMOVE FUEL TANK MAIN TUBE SUB-ASSEMBLY AND FUEL RETURN TUBE SUB-ASSEMBLY (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 fuel tube joint clips and pull out the fuel tank main tube and fuel return tube.</ptxt>
<figure>
<graphic graphicname="A220814E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Remove any dirt and foreign matter on the fuel tube joint before performing this work.</ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign matter on the parts when disconnecting them, as the fuel tube joint contains the O-rings that seal the plug.</ptxt>
</item>
<item>
<ptxt>Perform this work by hand. Do not use any tools.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, twist or turn the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the disconnected part by covering it with a plastic bag and tape after disconnecting the fuel tubes.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the fuel tank main tube and fuel return tube from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A220818" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0016" proc-id="RM23G0E___00005G200001">
<ptxt>REMOVE FUEL TANK MAIN TUBE SUB-ASSEMBLY, FUEL RETURN TUBE SUB-ASSEMBLY AND NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 fuel tube joint clips and pull out the fuel tank main tube, fuel return tube and No. 2 fuel main tube.</ptxt>
<figure>
<graphic graphicname="A220813E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Remove any dirt and foreign matter on the fuel tube joint before performing this work.</ptxt>
</item>
<item>
<ptxt>Do not allow any scratches or foreign matter on the parts when disconnecting them, as the fuel tube joint contains the O-rings that seal the plug.</ptxt>
</item>
<item>
<ptxt>Perform this work by hand. Do not use any tools.</ptxt>
</item>
<item>
<ptxt>Do not forcibly bend, twist or turn the nylon tube.</ptxt>
</item>
<item>
<ptxt>Protect the disconnected part by covering it with a plastic bag and tape after disconnecting the fuel tubes.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Detach the clamp and remove the No. 2 fuel main tube.</ptxt>
<figure>
<graphic graphicname="A220817" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the fuel tank main tube and fuel return tube from the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0021" proc-id="RM23G0E___00005G600001">
<ptxt>REMOVE FUEL SUCTION WITH PUMP AND GAUGE TUBE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A243483E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Set SST on the fuel pump gauge retainer.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Engage the claws of SST securely with the fuel pump gauge retainer holes to secure SST.</ptxt>
</item>
<item>
<ptxt>Install SST while pressing the claws of SST against the fuel pump gauge retainer (towards the center of SST).</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A243482E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using SST, loosen the retainer.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Fit the tips of SST onto the ribs of the retainer.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When the retainer is loosened, be careful as the pump and gauge tube will spring upward from the force of the spring.</ptxt>
</item>
<item>
<ptxt>Remove any foreign matter around the fuel suction with pump and gauge tube before this operation.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Remove the retainer.</ptxt>
</s2>
<s2>
<ptxt>Remove the fuel suction with pump and gauge tube assembly from the fuel tank.</ptxt>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel tank.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000045EZ00WX_01_0018" proc-id="RM23G0E___00005G300001">
<ptxt>REMOVE NO. 3 FUEL TANK PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="A220823" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 4 clamps and remove the No. 3 fuel tank protector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045EZ00WX_01_0019" proc-id="RM23G0E___00005G400001">
<ptxt>REMOVE FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel tank to filler pipe hose from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A220822" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>