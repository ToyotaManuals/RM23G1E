<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8Z5_T008T" variety="T008T">
<name>SUCTION CONTROL VALVE</name>
<para id="RM000004MF600DX" category="A" type-id="80001" name-id="FU8CJ-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM000004MF600DX_02" type-id="11" category="10" proc-id="RM23G0E___00005S800001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipes and fuel inlet pipe, clean them with a brush and compressed air.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM000004MF600DX_01" type-id="01" category="01">
<s-1 id="RM000004MF600DX_01_0001" proc-id="RM23G0E___00005S500001">
<ptxt>REMOVE FUEL SUPPLY PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM0000045CH009X"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF  (See page <xref label="Seep02" href="RM0000045CH00AX"/>)</ptxt>
</item>
</list1>
<atten3>
<ptxt>When handling the fuel supply pump, be sure to hold it by the housing. Do not hold it by the fuel pipe, fuel temperature sensor or suction control valve.</ptxt>
</atten3>
<atten4>
<ptxt>If the engine room is very dirty, clean the fuel pipe and the area around the fuel temperature sensor connector before removing the fuel supply pump from the vehicle.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000004MF600DX_01_0002" proc-id="RM23G0E___00005S600001">
<ptxt>CLEAN SUCTION CONTROL VALVE AND SURROUNDING AREA</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place the fuel supply pump in a tray of clean diesel fuel and clean the area around the suction control valve as well as any areas that contact other parts using a brush. After that, clean the part using cleaning spray, cloth, etc.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After cleaning with cleaning spray, do not blow air as doing so will cause any dust or sand in the surrounding area to become airborne and adhere to the cleaned area.</ptxt>
</item>
<item>
<ptxt>Do not allow the fuel temperature sensor connector or fuel pipe to face downward while performing this step.</ptxt>
</item>
<item>
<ptxt>Check that there is no dirt or foreign matter between the suction control valve and fuel supply pump housing.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000004MF600DX_01_0003" proc-id="RM23G0E___00005S700001">
<ptxt>REMOVE SUCTION CONTROL VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before replacing the suction control valve, be sure to clean the surrounding area.</ptxt>
</item>
<item>
<ptxt>When replacing the suction control valve, make sure that your hands are clean and do not use gloves, etc.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Disconnect the connector from the suction control valve.</ptxt>
</s2>
<s2>
<ptxt>Using a 5 mm hexagon wrench, remove the 2 bolts, suction control valve and O-ring from the fuel supply pump.</ptxt>
<figure>
<graphic graphicname="A228326" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not reuse the suction control valve and O-ring once it has been removed.</ptxt>
</item>
<item>
<ptxt>Make sure that the O-ring does not remain attached to the fuel supply pump.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>