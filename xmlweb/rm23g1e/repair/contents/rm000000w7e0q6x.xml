<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0012" variety="S0012">
<name>A343F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0012_7B941_T00DP" variety="T00DP">
<name>AUTOMATIC TRANSMISSION SYSTEM</name>
<para id="RM000000W7E0Q6X" category="U" type-id="303FJ" name-id="AT01QX-168" from="201207" to="201210">
<name>DATA LIST / ACTIVE TEST</name>
<subpara id="RM000000W7E0Q6X_z0" proc-id="RM23G0E___00007P500000">
<content5 releasenbr="1">
<step1>
<ptxt>READ DATA LIST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to read the Data List allows the values or states of switches, sensors, actuators and other items to be read without removing any parts. This non-intrusive inspection can be very useful because intermittent conditions or signals may be discovered before parts or wiring is disturbed. Reading the Data List information early in troubleshooting is one way to save diagnostic time.</ptxt>
</atten4>
<atten3>
<ptxt>In the table below, the values listed under "Normal Condition" are reference values. Do not depend solely on these reference values when deciding whether a part is faulty or not.</ptxt>
</atten3>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, read the Data List.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>N Range Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift lever N status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever in P or N</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever not in P or N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Neutral Position SW Signal</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in P or N: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in P or N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep01" href="RM000000W840W6X"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Transfer L4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transfer status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Transfer in L4: ON</ptxt>
</item>
<item>
<ptxt>Transfer not in L4: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Brake pedal depressed: ON</ptxt>
</item>
<item>
<ptxt>Brake pedal released: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (NC0)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input shaft speed/</ptxt>
<ptxt>Min.: 0 rpm</ptxt>
<ptxt>Max.: 12750 rpm</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Lock-up ON (after warming up engine): Input turbine speed (NCO) equal to engine speed</ptxt>
</item>
<item>
<ptxt>Lock-up OFF (idling with shift lever in N): Input turbine speed (NCO) nearly equal to engine speed</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Data is displayed in increments of 50 rpm.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SPD (SP2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output shaft speed/</ptxt>
<ptxt>Min.: 0 km/h (0 mph)</ptxt>
<ptxt>Max.: 255 km/h (158 mph)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle stopped: 0 km/h (0 mph) (output shaft speed equal to vehicle speed)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (P Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in P: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in P: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep02" href="RM000000W840W6X"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (R Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in R: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in R: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep03" href="RM000000W840W6X"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (2 Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in 2: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in 2: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep04" href="RM000000W840W6X"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (L Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in L: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in L: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep05" href="RM000000W840W6X"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in N: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in N: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep06" href="RM000000W840W6X"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N, P Range) Supported</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of PNP switch (N or P) support/</ptxt>
<ptxt>Supp or Unsupp</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (N, P Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Shift lever not in P or N</ptxt>
</item>
<item>
<ptxt>OFF: Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (D Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in D: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in D: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep07" href="RM000000W840W6X"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift SW Status (3 Range)</ptxt>
</entry>
<entry valign="middle">
<ptxt>PNP switch status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in 3: ON</ptxt>
</item>
<item>
<ptxt>Shift lever not in 3: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>When the shift lever position displayed on the intelligent tester differs from the actual position, the adjustment of the PNP switch or shift cable may be incorrect.</ptxt>
<atten4>
<ptxt>When failure still occurs even after adjusting these parts, refer to DTC P0705 (See page <xref label="Seep08" href="RM000000W840W6X"/>).</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A/T Oil Temperature 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 ATF temperature sensor value/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>After stall test: Approximately 80°C (176°F)</ptxt>
</item>
<item>
<ptxt>Equal to ambient temperature when engine cold</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F) or 215°C (419°F), the No. 1 ATF temperature sensor circuit is open or shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A/T Oil Temperature 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 ATF temperature sensor value/</ptxt>
<ptxt>Min.: -40°C (-40°F)</ptxt>
<ptxt>Max.: 215°C (419°F)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>After stall test: Approximately 80°C (176°F)</ptxt>
</item>
<item>
<ptxt>Equal to ambient temperature when engine cold</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>If the value is -40°C (-40°F) or 215°C (419°F), the No. 2 ATF temperature sensor circuit is open or shorted.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lock Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Lock-up operating: ON</ptxt>
</item>
<item>
<ptxt>Lock-up not operating: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Shift Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM gear shift command/</ptxt>
<ptxt>1st, 2nd, 3rd or 4th</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift lever in D: 1st, 2nd, 3rd or 4th</ptxt>
</item>
<item>
<ptxt>Shift lever in 3: 1st, 2nd or 3rd</ptxt>
</item>
<item>
<ptxt>Shift lever in 2: 1st or 2nd</ptxt>
</item>
<item>
<ptxt>Shift lever in L: 1st</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SLT Solenoid Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift solenoid SLT status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Accelerator pedal depressed: OFF</ptxt>
</item>
<item>
<ptxt>Accelerator pedal released: ON</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SLU Solenoid Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift solenoid SLU status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid SLU operating: ON</ptxt>
</item>
<item>
<ptxt>Shift solenoid SLU not operating: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Snow or 2nd Start Mode</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pattern select switch (2ND) status/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Pattern select switch (2ND) pushed: ON</ptxt>
</item>
<item>
<ptxt>Pattern select switch (2ND) not pushed: OFF</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AT Fluid</ptxt>
</entry>
<entry valign="middle">
<ptxt>Flex lock-up judder judgment/</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ON: Judder judgment occurs</ptxt>
</item>
<item>
<ptxt>OFF: Judder judgment does not occur</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>PERFORM ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid valve SLU</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLT)*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid valve SLT and raise line pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>OFF: Line pressure up (when Active Test "Activate the Solenoid (SLT)" is performed, ECM commands SLT solenoid to turn off)</ptxt>
</item>
<item>
<ptxt>ON: No action (normal operation)</ptxt>
</item>
</list1>
</atten4>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Vehicle stopped</ptxt>
</item>
<item>
<ptxt>Engine idling</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid valve S1</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (S2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate the shift solenoid valve S2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Engine stopped</ptxt>
</item>
<item>
<ptxt>Shift lever in P or N</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Lock Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control shift solenoid valve SLU to set automatic transmission to lock-up</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Possible to check shift solenoid valve SLU operation.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Throttle valve opening angle: Less than 35%</ptxt>
</item>
<item>
<ptxt>Vehicle speed: 60 km/h (36 mph) or more</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Control the Shift Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valves and set each shift position</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Press "→" button: Shift up</ptxt>
</item>
<item>
<ptxt>Press "←" button: Shift down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Possible to check operation of the shift solenoid valves.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<ptxt>50 km/h (30 mph) or less</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Activate the Solenoid (SLT) in the Active Test is performed to check the line pressure changes by connecting SST to the automatic transmission, which is used in the Hydraulic Test (See page <xref label="Seep09" href="RM000000W7B0NDX"/>) as well. Note that the pressure values in the Active Test and Hydraulic Test are different.</ptxt>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>