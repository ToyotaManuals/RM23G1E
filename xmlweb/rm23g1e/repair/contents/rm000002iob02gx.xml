<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7B8TM_T003A" variety="T003A">
<name>A343F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<para id="RM000002IOB02GX" category="F" type-id="30027" name-id="SS2UV-24" from="201207">
<name>SERVICE DATA</name>
<subpara id="RM000002IOB02GX_z0" proc-id="RM23G0E___000008X00000">
<content5 releasenbr="1">
<table pgwide="1">
<title>Line Pressure and Shift schedule</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.65in"/>
<colspec colname="COLSPEC0" colwidth="1.32in"/>
<colspec colname="COL2" colwidth="3.11in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Line pressure (Wheel locked)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>AT stall</ptxt>
<ptxt>(Throttle valve fully opened)</ptxt>
</entry>
<entry valign="middle">
<ptxt>D position</ptxt>
</entry>
<entry valign="middle">
<ptxt>1050 to 1200 kPa (10.7 to 12.2 kgf/cm<sup>2</sup>, 152 to 174 psi)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R position</ptxt>
</entry>
<entry valign="middle">
<ptxt>1300 to 1650 kPa (13.3 to 16.8 kgf/cm<sup>2</sup>, 189 to 239 psi)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine stall revolution</ptxt>
</entry>
<entry valign="middle">
<ptxt>D and R positions</ptxt>
</entry>
<entry valign="middle">
<ptxt>1800 to 2200 rpm</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Time lag</ptxt>
</entry>
<entry>
<ptxt>N → D position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Less than 1.2 seconds</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>N → R position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Less than 1.5 seconds</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine idle speed (A/C OFF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>N position</ptxt>
</entry>
<entry valign="middle">
<ptxt>600 to 700 rpm</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Drive plate runout</ptxt>
</entry>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.30 mm (0.0118 in.)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Shift schedule</ptxt>
<ptxt>(Speeds higher than the fuel cut speed are for reference only)</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle">
<ptxt>D position</ptxt>
<ptxt>(Throttle valve fully opened)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 → 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>54 to 62 km/h (34 to 39 mph)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 → 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>100 to 112 km/h (62 to 70 mph)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 → 4</ptxt>
</entry>
<entry valign="middle">
<ptxt>145 to 156 km/h (90 to 97 mph)</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle">
<ptxt>D position</ptxt>
<ptxt>(Throttle valve opening 5%)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 → 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 15 km/h (7 to 9 mph)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 → 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>16 to 20 km/h (10 to 12 mph)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>3 → 4</ptxt>
</entry>
<entry valign="middle">
<ptxt>44 to 49 km/h (27 to 30 mph)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Manual downshift permissible speed</ptxt>
<ptxt>(Speeds higher than the fuel cut speed are for reference only)</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle">
<ptxt>S position</ptxt>
</entry>
<entry valign="middle">
<ptxt>→ 3</ptxt>
</entry>
<entry valign="middle">
<ptxt>160 to 170 km/h (99 to 106 mph)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>→ 2</ptxt>
</entry>
<entry valign="middle">
<ptxt>105 to 114 km/h (65 to 71 mph)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>→ 1</ptxt>
</entry>
<entry valign="middle">
<ptxt>51 to 56 km/h (32 to 35 mph)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Speed Sensor</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.55in"/>
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="3.11in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Speed sensor NC0 and SP2</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F) </ptxt>
</entry>
<entry valign="middle">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>No. 2 ATF Temperature Sensor</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.55in"/>
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="3.11in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>No. 2 temperature sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always </ptxt>
</entry>
<entry valign="middle">
<ptxt>560 to 680 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No. 1 temperature sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>79 Ω to 156 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Shift Solenoid Valve</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.55in"/>
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="3.11in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt> SLU and SLT</ptxt>
</entry>
<entry>
<ptxt>20°C (68°F) </ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S1 and S2</ptxt>
</entry>
<entry>
<ptxt>20°C (68°F) </ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 15 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Transfer Adaptor Oil Seal</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.55in"/>
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="3.11in"/>
<tbody>
<row>
<entry namest="COLSPEC0" nameend="COL1" valign="middle">
<ptxt>Standard depth</ptxt>
</entry>
<entry valign="middle">
<ptxt>0 to 1 mm (0 to 0.0394 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Drive Plate</title>
<tgroup cols="2">
<colspec colname="COLSPEC0" colwidth="3.96in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Maximum runout</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.30 mm (0.0118 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Overdrive Clutch</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COLSPEC17" colwidth="1.39in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Clutch piston return spring free length</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>15.8 mm (0.622 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Overdrive direct clutch drum bush inside diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Maximum </ptxt>
</entry>
<entry valign="middle">
<ptxt>27.11 mm (1.07 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="5" valign="middle">
<ptxt>Flange thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark 21</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.05 to 3.15 mm (0.120 to 0.124 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 20</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.15 to 3.25 mm (0.124 to 0.128 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 19</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.25 to 3.35 mm (0.128 to 0.132 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 18</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.35 to 3.45 mm (0.132 to 0.136 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 17</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.45 to 3.55 mm (0.136 to 0.140 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 16</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.55 to 3.65 mm (0.140 to 0.144 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Overdrive Planetary Gear Assembly</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COLSPEC18" colwidth="1.39in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry>
<ptxt>Planetary gear bush inside diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.2 to 11.221 mm (0.441 to 0.442 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Pinion gear thrust clearance</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.20 to 0.60 mm (0.00787 to 0.0236 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.65 mm (0.0256 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Overdrive Brake</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.60in"/>
<colspec colname="COLSPEC5" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Brake piston return spring free length</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>17.03 mm (0.671 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="6" valign="middle">
<ptxt>Flange thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark 33</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.25 to 3.35 mm (0.128 to 0.132 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 35</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.45 to 3.55 mm (0.136 to 0.140 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 36</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.55 to 3.65 mm (0.140 to 0.144 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 37</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.65 to 3.75 mm (0.144 to 0.148 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 38</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.75 to 3.85 mm (0.148 to 0.152 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 39</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.85 to 3.95 mm (0.152 to 0.156 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 40</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.95 to 4.05 mm (0.156 to 0.159 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake piston return spring free length</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>15.72 mm (0.619 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Direct Clutch</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COLSPEC16" colwidth="1.39in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Clutch piston return spring free length</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>21.32 mm (0.839 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Clutch drum bush inside diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>53.915 to 53.94 mm (2.123 to 2.124 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>53.99 mm (2.13 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="5" valign="middle">
<ptxt>Flange thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark 53</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.25 to 3.35 mm (0.128 to 0.132 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 54</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.35 to 3.45 mm (0.132 to 0.136 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 55</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.45 to 3.55 mm (0.136 to 0.140 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 56</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.55 to 3.65 mm (0.140 to 0.144 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 57</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.65 to 3.75 mm (0.144 to 0.148 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 58</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.75 to 3.85 mm (0.148 to 0.152 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Forward Clutch</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COLSPEC16" colwidth="1.39in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Clutch piston return spring free length</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>19.47 mm (0.767 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Forward clutch input shaft bush inside diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>24.0 to 24.026 mm (0.945 to 0.946 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>24.076 mm (0.948 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="7" valign="middle">
<ptxt>Flange thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark 90</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.95 to 3.05 mm (0.116 to 0.120 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 91</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.15 to 3.25 mm (0.124 to 0.128 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 92</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.35 to 3.45 mm (0.132 to 0.136 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 93</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.55 to 3.65 mm (0.140 to 0.144 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 94</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.75 to 3.85 mm (0.148 to 0.152 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 95</ptxt>
</entry>
<entry valign="middle">
<ptxt>3.95 to 4.05 mm (0.156 to 0.159 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 96</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.15 to 4.25 mm (0.163 to 0.167 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mark 97</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.35 to 4.45 mm (0.171 to 0.175 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Second Coast Brake</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COLSPEC15" colwidth="1.39in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Piston rod length</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Groove mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>78.3 to 78.5 mm (3.08 to 3.09 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>w/o Groove mark</ptxt>
</entry>
<entry valign="middle">
<ptxt>79.8 to 80.0 mm (3.14 to 3.15 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Piston stroke</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>1.5 to 3.0 mm (0.059 to 0.118 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Front Planetary Gear</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.60in"/>
<colspec colname="COLSPEC6" align="left" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Planetary ring gear bush inside diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>24.0 to 24.026 mm (0.945 to 0.946 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>24.076 mm (0.948 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Planetary sun gear bush inside diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>27.00 to 27.026 mm (1.063 to 1.064 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>27.076 mm (1.07 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Planetary pinion gear thrust clearance</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.20 to 0.60 mm (0.00787 to 0.0236 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.65 mm (0.0256 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Second Brake</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.61in"/>
<colspec colname="COL2" align="left" colwidth="1.33in"/>
<colspec colname="COL3" colwidth="3.14in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Piston return spring free length</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>16.05 mm (0.632 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Rear Planetary Gear</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.56in"/>
<colspec colname="COL2" align="left" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="3.14in"/>
<tbody>
<row>
<entry morerows="1">
<ptxt>Planetary pinion gear thrust clearance</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry>
<ptxt>0.2 to 0.6 mm (0.00787 to 0.0236 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.65 mm (0.0256 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>First and Reverse Brake</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.60in"/>
<colspec colname="COLSPEC5" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Brake piston return spring free length</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>18.182 to 18.582 mm (0.716 to 0.732 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Transmission Case</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COLSPEC15" colwidth="1.39in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Transmission case bush inside diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>38.113 to 38.138 mm (1.5005 to 1.5014 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>38.19 mm (1.50 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Output Shaft</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.57in"/>
<colspec colname="COL2" align="left" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="3.15in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Output shaft end play</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.30 to 1.04 mm (0.0118 to 0.0409 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Accumulator Spring</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.54in"/>
<colspec colname="COL2" colwidth="1.39in"/>
<colspec colname="COL3" colwidth="3.15in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>C-0 Inner</ptxt>
</entry>
<entry valign="middle">
<ptxt>Yellow</ptxt>
</entry>
<entry valign="middle">
<ptxt>46.0 mm (1.81 in.)/14.02 mm (0.552 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C-0 Outer</ptxt>
</entry>
<entry valign="middle">
<ptxt>Orange</ptxt>
</entry>
<entry valign="middle">
<ptxt>74.6 mm (2.94 in.)/20.9 mm (0.823 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B-0</ptxt>
</entry>
<entry valign="middle">
<ptxt>Red</ptxt>
</entry>
<entry valign="middle">
<ptxt>63.6 mm (2.50 in.)/16.0 mm (0.630 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C-2 Inner</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pink</ptxt>
</entry>
<entry valign="middle">
<ptxt>42.06 mm (1.66 in.)/14.7 mm (0.579 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C-2 Outer</ptxt>
</entry>
<entry valign="middle">
<ptxt>Blue</ptxt>
</entry>
<entry valign="middle">
<ptxt>768.53 mm (2.70 in.)/20.2 mm (0.795 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>B-2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Light green</ptxt>
</entry>
<entry valign="middle">
<ptxt>70.50 mm (2.78 in.) 19.9 mm (0.783 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Oil Pump</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.61in"/>
<colspec colname="COLSPEC3" align="left" colwidth="1.35in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Body clearance</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.07 to 0.15 mm (0.00276 to 0.00591 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.2 mm (0.00787 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Tip clearance</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.004 to 0.248 mm (0.000157 to 0.00976 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.3 mm (0.0118 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Side clearance</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.02 to 0.05 mm (0.000787 to 0.00197 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.1 mm (0.00394 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>Driver and driven gear thickness</ptxt>
</entry>
<entry valign="middle">
<ptxt>M</ptxt>
</entry>
<entry valign="middle">
<ptxt>M 11.690 to 11.699 mm (0.4602 to 0.4606 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>N</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.700 to 11.709 mm (0.4606 to 0.4610 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.710 to 11.720 mm (0.4610 to 0.4614 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>R</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.721 to 11.730 mm (0.4615 to 0.4618 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>S</ptxt>
</entry>
<entry valign="middle">
<ptxt>11.731 to 11.740 mm (0.4618 to 0.4622 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pump body inside diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Maximum</ptxt>
</entry>
<entry valign="middle">
<ptxt>38.19 mm (1.50 in.)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Stator shaft inside maximum diameter</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front side</ptxt>
</entry>
<entry valign="middle">
<ptxt>21.58 mm (0.8496 in.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear side</ptxt>
</entry>
<entry valign="middle">
<ptxt>27.08 mm (1.0661 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>