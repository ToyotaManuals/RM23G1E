<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002A" variety="S002A">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002A_7B9G3_T00PR" variety="T00PR">
<name>SLIDING ROOF SYSTEM</name>
<para id="RM000001VJL02YX" category="U" type-id="3001G" name-id="RF154-09" from="201207">
<name>TERMINALS OF ECU</name>
<subpara id="RM000001VJL02YX_z0" proc-id="RM23G0E___0000IJ500000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK SLIDING ROOF DRIVE GEAR SUB-ASSEMBLY (SLIDING ROOF ECU)</ptxt>
<figure>
<graphic graphicname="B160074E27" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the W2 ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<atten4>
<ptxt>Measure the values on the wire harness side with the connector disconnected.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>W2-1 (B) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-5 (IG) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-5 (IG) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-2 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Reconnect the W2 ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>W2-7 (OPN) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof motor open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, sliding roof closed</ptxt>
<ptxt>Slide open switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-7 (OPN) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof motor open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, sliding roof closed</ptxt>
<ptxt> Slide open switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-9 (CLS) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof motor close</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, sliding roof open</ptxt>
<ptxt>Slide close switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-9 (CLS) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof motor close</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, sliding roof open</ptxt>
<ptxt>Slide close switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-10 (UP) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof motor up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, sliding roof tilted downward</ptxt>
<ptxt>Tilt up switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-10 (UP) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof motor up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, sliding roof tilted downward</ptxt>
<ptxt>Tilt up switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-8 (DWN) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof motor down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, sliding roof tilted upward</ptxt>
<ptxt>Tilt down switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W2-8 (DWN) - W2-2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sliding roof motor down</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, sliding roof tilted upward</ptxt>
<ptxt>Tilt down switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, the sliding roof drive gear sub-assembly may have a malfunction.</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>CHECK DRIVER SIDE JUNCTION BLOCK ASSEMBLY AND MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)</ptxt>
<figure>
<graphic graphicname="B235899E06" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step2>
<ptxt>Remove the main body ECU (multiplex network body ECU) from the driver side junction block assembly (See page <xref label="Seep01" href="RM000003WBO02VX"/>).</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<atten4>
<ptxt>Measure the values on the wire harness side with the connector disconnected.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A-30 (BECU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-31 (ALTB) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-32 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-32 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-29 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-29 (ACC) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>A-11 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-3 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction in the wire harness.</ptxt>
</step2>
<step2>
<ptxt>Install the main body ECU (multiplex network body ECU).</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<title>for LHD:</title>
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2I-27 (FLCY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front door LH courtesy switch input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door LH open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2I-27 (FLCY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>R - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front door LH courtesy switch input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door LH closed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-11 (L2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>GR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key-linked lock input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key cylinder in lock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-11 (L2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>GR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key-linked lock input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off, all doors closed and driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation (See waveform 1 or 2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-24 (UL3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key-linked unlock input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key cylinder in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-24 (UL3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key-linked unlock input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off, all doors closed and driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation (See waveform 3 or 4)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-26 (RDA) - Body ground*</ptxt>
</entry>
<entry valign="middle">
<ptxt>P - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door control receiver input</ptxt>
</entry>
<entry valign="middle">
<ptxt>No key in ignition key cylinder, all doors closed and transmitter switch off → on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>for RHD:</title>
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>2H-26 (FRCY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front door RH courtesy switch input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door RH open</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2H-26 (FRCY) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front door RH courtesy switch input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front door RH closed</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-11 (L2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>GR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key-linked lock input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key cylinder in lock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-11 (L2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>GR - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key-linked lock input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off, all doors closed and driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation (See waveform 1 or 2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-24 (UL3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key-linked unlock input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key cylinder in unlock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-24 (UL3) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driver side door key-linked unlock input</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off, all doors closed and driver side door key cylinder in neutral position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation (See waveform 3 or 4)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G64-26 (RDA) - Body ground*</ptxt>
</entry>
<entry valign="middle">
<ptxt>P - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Door control receiver input</ptxt>
</entry>
<entry valign="middle">
<ptxt>No key in ignition key cylinder, all doors closed and transmitter switch off → on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/o Entry and Start System</ptxt>
</item>
</list1>
<ptxt>If the result is not as specified, the main body ECU (multiplex network body ECU) or driver side junction block assembly may have a malfunction.</ptxt>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 1.</ptxt>
<figure>
<graphic graphicname="B201885E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 1 (Reference) </title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry>
<ptxt>G64-11 (L2) - Body ground</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Tool Setting</ptxt>
</entry>
<entry>
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry align="left">
<ptxt>Ignition switch off, all doors closed and front door key cylinder in neutral position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 2.</ptxt>
<figure>
<graphic graphicname="B201886E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 2 (Reference) </title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry>
<ptxt>G64-11 (L2) - Body ground</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Tool Setting</ptxt>
</entry>
<entry>
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry align="left">
<ptxt>Ignition switch off, all doors closed and front door key cylinder in neutral position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 3.</ptxt>
<figure>
<graphic graphicname="B199631E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 3 (Reference) </title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry>
<ptxt>G64-24 (UL3) - Body ground</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Tool Setting</ptxt>
</entry>
<entry>
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry align="left">
<ptxt>Ignition switch off, all doors closed and driver side door key cylinder in neutral position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 4.</ptxt>
<figure>
<graphic graphicname="B199632E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Waveform 4 (Reference) </title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>

</entry>
<entry>
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry>
<ptxt>G64-24 (UL3) - Body ground</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Tool Setting</ptxt>
</entry>
<entry>
<ptxt>5 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry align="left">
<ptxt>Ignition switch off, all doors closed and driver side door key cylinder in neutral position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK CERTIFICATION ECU (w/ Smart Key System)</ptxt>
<figure>
<graphic graphicname="B197893E18" width="7.106578999in" height="2.775699831in"/>
</figure>
<step2>
<ptxt>Disconnect the G38 ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<atten4>
<ptxt>Measure the values on the wire harness side with the connector disconnected.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-1 (+B) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>V - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-15 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-16 (IG) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-16 (IG) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-17 (CUTB) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction in the wire harness.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Reconnect the G38 ECU connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G39-5 (RCO) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door control receiver power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off, all doors closed and transmitter switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G39-5 (RCO) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door control receiver power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off, all doors closed and transmitter switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G39-15 (RDA) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door control receiver data input signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V pulse generation at regular intervals</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G39-16 (RSSI) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door control receiver electric wave existence signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>All doors locked, all doors closed and transmitter switch not pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G39-16 (RSSI) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Door control receiver electric wave existence signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>All doors locked, all doors closed and transmitter switch pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 2 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, the certification ECU may have a malfunction.</ptxt>
</item>
</list1>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>