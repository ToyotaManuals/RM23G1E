<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM00000330203AX" category="J" type-id="302L1" name-id="ESMBO-06" from="201207" to="201210">
<dtccode/>
<dtcname>Fuel Injector Circuit</dtcname>
<subpara id="RM00000330203AX_01" type-id="60" category="03" proc-id="RM23G0E___00000S000000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The fuel injector assemblies are located on the intake manifold. They inject fuel into the cylinders based on the signals from the ECM.</ptxt>
</content5>
</subpara>
<subpara id="RM00000330203AX_02" type-id="32" category="03" proc-id="RM23G0E___00000S100000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0300 (See page <xref label="Seep01" href="RM000000XH30RUX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM00000330203AX_03" type-id="51" category="05" proc-id="RM23G0E___00000S200000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000330203AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000330203AX_04_0001" proc-id="RM23G0E___00000S300000">
<testtitle>CHECK FUEL INJECTOR ASSEMBLY (POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A208644E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the fuel injector assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C2-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C5-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C6-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Fuel Injector Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the fuel injector assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000330203AX_04_0002" fin="false">OK</down>
<right ref="RM00000330203AX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000330203AX_04_0002" proc-id="RM23G0E___00000S400000">
<testtitle>INSPECT FUEL INJECTOR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the fuel injector assembly (See page <xref label="Seep01" href="RM000000WQQ080X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000330203AX_04_0003" fin="false">OK</down>
<right ref="RM00000330203AX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000330203AX_04_0003" proc-id="RM23G0E___00000S500000">
<testtitle>CHECK HARNESS AND CONNECTOR (FUEL INJECTOR ASSEMBLY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the fuel injector assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1-2 - C37-6 (#10)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C2-2 - C37-1 (#20)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-2 - C37-7 (#30)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-2 - C37-2 (#40)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C5-2 - C37-8 (#50)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C6-2 - C37-3 (#60)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1-2 or C37-6 (#10) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C2-2 or C37-1 (#20) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C3-2 or C37-7 (#30) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C4-2 or C37-2 (#40) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C5-2 or C37-8 (#50) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C6-2 or C37-3 (#60) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the fuel injector assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000330203AX_04_0010" fin="true">OK</down>
<right ref="RM00000330203AX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000330203AX_04_0005" proc-id="RM23G0E___00000S600000">
<testtitle>INSPECT NO. 1 INTEGRATION RELAY (IG2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the No. 1 integration relay (See page <xref label="Seep01" href="RM000003BLB02MX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000330203AX_04_0011" fin="true">OK</down>
<right ref="RM00000330203AX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000330203AX_04_0006">
<testtitle>REPLACE FUEL INJECTOR ASSEMBLY<xref label="Seep01" href="RM000000Q4808RX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000330203AX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000330203AX_04_0009">
<testtitle>REPLACE NO. 1 INTEGRATION RELAY (IG2)</testtitle>
</testgrp>
<testgrp id="RM00000330203AX_04_0010">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202EX"/>
</testtitle>
</testgrp>
<testgrp id="RM00000330203AX_04_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (NO. 1 INTEGRATION RELAY - FUEL INJECTOR ASSEMBLY)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>