<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AN_T00KB" variety="T00KB">
<name>CAN COMMUNICATION SYSTEM (for LHD without Entry and Start System)</name>
<para id="RM000003ERM028X" category="D" type-id="303F2" name-id="NW07V-44" from="201207" to="201210">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000003ERM028X_z0" proc-id="RM23G0E___0000DRO00000">
<content5 releasenbr="1">
<step1>
<ptxt>BRIEF DESCRIPTION</ptxt>
<step2>
<ptxt>The CAN (Controller Area Network) is a serial data communication system for real-time application. It is a vehicle multiplex communication system which has a high communication speed (500 kbps) and the ability to detect malfunctions.</ptxt>
</step2>
<step2>
<ptxt>By pairing the CANH and CANL bus lines, the CAN performs communication based on differential voltages.</ptxt>
</step2>
<step2>
<ptxt>Many ECUs (sensors) installed on the vehicle operate by sharing information and communicating with each other.</ptxt>
</step2>
<step2>
<ptxt>The CAN has 2 resistors of 120 Ω which are necessary to communicate using the main wire.</ptxt>
</step2>
</step1>
<step1>
<ptxt>DEFINITION OF TERMS</ptxt>
<step2>
<ptxt>Main wire</ptxt>
<step3>
<ptxt>The main wire is a wire harness between the 2 termination circuits on the bus (communication line). This is the main bus in the CAN communication system.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Branch wire</ptxt>
<step3>
<ptxt>The branch wire is a wire harness which diverges from the main wire to an ECU or sensor.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Termination circuits</ptxt>
<step3>
<ptxt>The termination circuit is a circuit which converts the communication current of the CAN communication into the bus voltage. It consists of a resistor and capacitor. 2 termination circuits are necessary on a bus.</ptxt>
</step3>
</step2>
<step2>
<ptxt>CAN junction connector</ptxt>
<step3>
<ptxt>The CAN junction connector is designed so that it can be used with CAN communication which has a termination circuit and CAN communication which does not have a termination circuit.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>CIRCUIT DESCRIPTION</ptxt>
<step2>
<ptxt>The V1 bus have termination circuits with a resistance of 120 Ω x 2. High speed communication at 500 kbps is possible.</ptxt>
</step2>
</step1>
<step1>
<ptxt>ECUS OR SENSORS WHICH COMMUNICATE VIA CAN COMMUNICATION SYSTEM</ptxt>
<step2>
<ptxt>ECM*1</ptxt>
</step2>
<step2>
<ptxt>Main body ECU (multiplex network body ECU)</ptxt>
</step2>
<step2>
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</step2>
<step2>
<ptxt>Four wheel drive control ECU</ptxt>
</step2>
<step2>
<ptxt>Yaw rate sensor assembly</ptxt>
</step2>
<step2>
<ptxt>DLC3</ptxt>
</step2>
<step2>
<ptxt>Spiral cable sub-assembly (steering angle sensor)*2</ptxt>
</step2>
<step2>
<ptxt>Power steering ECU assembly*1</ptxt>
</step2>
<step2>
<ptxt>Display and navigation module display (AVN)*3</ptxt>
</step2>
<step2>
<ptxt>Center airbag sensor assembly</ptxt>
</step2>
<step2>
<ptxt>Combination meter assembly</ptxt>
</step2>
<step2>
<ptxt>Air conditioning amplifier assembly</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 2TR-FE</ptxt>
</item>
<item>
<ptxt>*2: w/ Vehicle Stability Control System (for Vacuum Brake Booster)</ptxt>
</item>
<item>
<ptxt>*3: w/ Navigation System</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>DIAGNOSTIC CODES FOR CAN COMMUNICATION SYSTEM</ptxt>
<step2>
<ptxt>DTCs for the CAN communication system are as follows: U0073, U0100, U0114, U0123, U0124, U0126, U0129, U0131, U0142, U0151, U0155, U0164</ptxt>
</step2>
</step1>
<step1>
<ptxt>NOTES REGARDING TROUBLESHOOTING</ptxt>
<step2>
<ptxt>Trouble in the CAN bus (communication line) can be checked through the DLC3 (except when there is a wire break in the branch wire of the DLC3).</ptxt>
</step2>
<step2>
<ptxt>DTCs regarding the CAN communication system can be checked using the intelligent tester.</ptxt>
</step2>
<step2>
<ptxt>The CAN communication system cannot detect trouble in the branch line of the DLC3 even though the DLC3 is also connected to the CAN communication system.</ptxt>
</step2>
</step1>
<step1>
<ptxt>HOW TO DISTINGUISH CAN JUNCTION CONNECTOR CONNECTORS</ptxt>
<step2>
<ptxt>In the CAN communication system, the shape of all connectors connected to the CAN junction connector is the same.</ptxt>
<ptxt>The connectors connected to the CAN junction connector can be distinguished by the colors of the bus line and the connecting side of the connector.</ptxt>
<atten4>
<ptxt>See "Terminals of ECU" (See page <xref label="Seep01" href="RM000000XVK0B1X"/>) for bus line color or the type of connecting surface.</ptxt>
</atten4>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>