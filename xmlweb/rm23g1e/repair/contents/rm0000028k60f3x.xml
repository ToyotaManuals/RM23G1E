<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM0000028K60F3X" category="C" type-id="302LP" name-id="ESMBW-07" from="201210">
<dtccode>P2237</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit / Open (Bank 1 Sensor 1)</dtcname>
<dtccode>P2238</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P2239</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P2240</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit / Open (Bank 2 Sensor 1)</dtcname>
<dtccode>P2241</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit Low (Bank 2 Sensor 1)</dtcname>
<dtccode>P2242</dtccode>
<dtcname>Oxygen (A/F) Sensor Pumping Current Circuit High (Bank 2 Sensor 1)</dtcname>
<dtccode>P2252</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P2253</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P2255</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit Low (Bank 2 Sensor 1)</dtcname>
<dtccode>P2256</dtccode>
<dtcname>Oxygen (A/F) Sensor Reference Ground Circuit High (Bank 2 Sensor 1)</dtcname>
<subpara id="RM0000028K60F3X_02" type-id="60" category="03" proc-id="RM23G0E___00000SH00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Although the DTC titles say heated oxygen sensor, these DTCs relate to the air fuel ratio sensor.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor mounted in front of the Three-Way Catalytic Converter (TWC) and located near the engine assembly.</ptxt>
</item>
</list1>
</atten4>
<ptxt>The air fuel ratio sensor, which is located between the exhaust manifold and catalyst, consists of alloyed metal elements and a heater.</ptxt>
<ptxt>Depending on the engine operating conditions, the heater heats the sensor elements to activate them. Battery voltage is applied to the heater and the sensor ground is controlled by the ECM using a duty ratio.</ptxt>
<ptxt>The sensor elements convert the oxygen concentration in the exhaust gas into voltage values to output. Based on the voltage, the ECM determines the air fuel ratio and regulates the fuel injection volume according on the air fuel ratio and engine operating conditions. The voltage changes between 0.6 V and 4.5 V while the engine is running. If the air fuel ratio is lean, which means the oxygen concentration in the exhaust gas is high, the voltage is high. If the air fuel ratio is rich, which means the oxygen concentration in the exhaust gas is low, the voltage is low. </ptxt>
<figure>
<graphic graphicname="A160615E09" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="A225212E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2237</ptxt>
<ptxt>P2240</ptxt>
</entry>
<entry valign="middle">
<ptxt>An open in the circuit between terminals A1A+/A2A+ and A1A-/A2A- of the air fuel ratio sensor while the engine is running (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in air fuel ratio sensor (bank 1, 2 sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 1, 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2238</ptxt>
<ptxt>P2241</ptxt>
</entry>
<entry valign="middle">
<ptxt>Any of the following conditions are met (2 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>The air fuel ratio sensor output drops while the engine is running.</ptxt>
</item>
<item>
<ptxt>The voltage at terminal A1A+/A2A+ is 0.5 V or less.</ptxt>
</item>
<item>
<ptxt>The voltage difference between terminals A1A+/A2A+ and A1A-/A2A- is 0.1 V or less.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (bank 1, 2 sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 1, 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2239</ptxt>
<ptxt>P2242</ptxt>
</entry>
<entry valign="middle">
<ptxt>The A1A+/A2A+ voltage is higher than 4.5 V (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (bank 1, 2 sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 1, 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2252</ptxt>
<ptxt>P2255</ptxt>
</entry>
<entry valign="middle">
<ptxt>The A1A-/A2A- voltage is 0.5 V or less (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (bank 1, 2 sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 1, 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2253</ptxt>
<ptxt>P2256</ptxt>
</entry>
<entry valign="middle">
<ptxt>The A1A-/A2A- voltage is higher than 4.5 V (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in air fuel ratio sensor (bank 1, 2 sensor 1) circuit</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 1, 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000028K60F3X_03" type-id="64" category="03" proc-id="RM23G0E___00000SI00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>These DTCs are stored when there is an open or short in the air fuel ratio sensor circuit, or if the air fuel ratio sensor output drops. To detect these problems, the voltage of the air fuel ratio sensor is monitored when turning the ignition switch to ON, and the admittance (admittance is an electrical term that indicates the ease of current flow) is checked while driving. If the voltage of the air fuel ratio sensor is between 0.6 V and 4.5 V, it is considered normal. If the voltage is out of the specified range, or the admittance is less than the standard value, the ECM will determine that there is a malfunction in the air fuel ratio sensor. If the same malfunction is detected in the next driving cycle, the MIL will be illuminated and a DTC will be stored. </ptxt>
</content5>
</subpara>
<subpara id="RM0000028K60F3X_08" type-id="32" category="03" proc-id="RM23G0E___00000SJ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2195 (See page <xref label="Seep01" href="RM000000WC40RXX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000028K60F3X_11" type-id="73" category="03" proc-id="RM23G0E___00000SO00001">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>This confirmation driving pattern is used in the "Perform Confirmation Driving Pattern" step of the following diagnostic troubleshooting procedure.</ptxt>
</item>
<item>
<ptxt>Performing this confirmation driving pattern will activate the heated oxygen sensor monitor. This is useful for verifying the completion of the repair.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="A172863E08" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the ignition switch to ON.</ptxt>
</item>
<item>
<ptxt>Turn the tester on.</ptxt>
</item>
<item>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z5X"/>).</ptxt>
</item>
<item>
<ptxt>Switch the ECM from normal mode to check mode (See page <xref label="Seep02" href="RM000000PDL0PEX"/>).</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / O2S (A/FS) Monitor.</ptxt>
</item>
<item>
<ptxt>Check that the item O2S (A/FS) Monitor displays Incmpl (incomplete).</ptxt>
</item>
<item>
<ptxt>Start the engine and warm it up. (A)</ptxt>
</item>
<item>
<ptxt>Drive the vehicle at between 60 km/h and 120 km/h (37 mph and 75 mph) for at least 10 minutes. (B)</ptxt>
</item>
<item>
<ptxt>Note the state of the Data List items. Those items will change to Compl (complete) as the heated oxygen sensor monitor operates.</ptxt>
</item>
<item>
<ptxt>On the tester, enter the following menus: Powertrain / Engine and ECT / DTC; and check if any DTCs (any pending DTCs) are output.</ptxt>
</item>
</list1>
<atten4>
<ptxt>If the item O2S (A/FS) Monitor does not change to Compl (complete) and any pending DTCs fail to be stored, extend the driving time.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000028K60F3X_09" type-id="51" category="05" proc-id="RM23G0E___00000SK00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Malfunctioning areas can be identified by performing the Control the Injection Volume for air fuel ratio sensor function provided in the Active Test. The Control the Injection Volume for air fuel ratio sensor function can help to determine whether the air fuel ratio sensor, heated oxygen sensor and other potential trouble areas are malfunctioning.</ptxt>
<ptxt>The following instructions describe how to conduct the Control the Injection Volume for A/F Sensor operation using the intelligent tester.</ptxt>
<list1 type="ordered">
<item>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Start the engine and turn the tester on.</ptxt>
</item>
<item>
<ptxt>Warm up the engine at an engine speed of 2500 rpm for approximately 90 seconds.</ptxt>
</item>
<item>
<ptxt>On the tester, enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F Sensor.</ptxt>
</item>
<item>
<ptxt>Perform the Active Test operation with the engine idling (press the RIGHT or LEFT button to change the fuel injection volume).</ptxt>
</item>
<item>
<ptxt>Monitor the output voltages of the air fuel ratio and heated oxygen sensors (AFS Voltage B1S1 and O2S B1S2 or AFS Voltage B2S1 and O2S B2S2) displayed on the tester.</ptxt>
</item>
</list1>
</atten4>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The Control the Injection Volume for air fuel ratio sensor operation lowers the fuel injection volume by 12.5% or increases the injection volume by 25%.</ptxt>
</item>
<item>
<ptxt>Each sensor reacts in accordance with increases and decreases in the fuel injection volume.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display (Sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+25%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 3.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 3.4 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+25%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 0.55 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The air fuel ratio sensor has an output delay of a few seconds and the heated oxygen sensor has a maximum output delay of approximately 20 seconds.</ptxt>
</atten3>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.51in"/>
<colspec colname="COL2" colwidth="2.63in"/>
<colspec colname="COL3" colwidth="2.58in"/>
<colspec colname="COL4" colwidth="1.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Case</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air Fuel Ratio Sensor (Sensor 1) Output Voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Heated Oxygen Sensor (Sensor 2) Output Voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Suspected Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A151324E08" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150788E04" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150788E04" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor heater</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A151324E08" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor heater</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Fuel injector assembly</ptxt>
</item>
<item>
<ptxt>Fuel pressure</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system (Air fuel ratio extremely rich or lean)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Following the Control the Injection Volume for air fuel ratio sensor procedure enables technicians to check and graph the voltage outputs of both the air fuel ratio and heated oxygen sensors.</ptxt>
</item>
<item>
<ptxt>To display the graph, enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F Sensor / A/F Control System / AFS Voltage B1S1 and O2S B1S2 or AFS Voltage B2S1 and O2S B2S2.</ptxt>
</item>
</list1>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000028K60F3X_10" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000028K60F3X_10_0007" proc-id="RM23G0E___00000SN00001">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR FUEL RATIO SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C25-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C25-1 (HA1A) - C37-17 (HA1A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C25-3 (A1A+) - C36-1 (A1A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C25-4 (A1A-) - C36-2 (A1A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-1 (HA2A) - C37-19 (HA2A)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-3 (A2A+) - C36-7 (A2A+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-4 (A2A-) - C36-8 (A2A-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C25-1 (HA1A) or C37-17 (HA1A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C25-3 (A1A+) or C36-1 (A1A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C25-4 (A1A-) or C36-2 (A1A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-1 (HA2A) or C37-19 (HA2A) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-3 (A2A+) or C36-7 (A2A+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C26-4 (A2A-) or C36-8 (A2A-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the air fuel ratio sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000028K60F3X_10_0002" fin="false">OK</down>
<right ref="RM0000028K60F3X_10_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000028K60F3X_10_0002" proc-id="RM23G0E___00000SL00001">
<testtitle>REPLACE AIR FUEL RATIO SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air fuel ratio sensor (See page <xref label="Seep01" href="RM000002W9Q00RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000028K60F3X_10_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000028K60F3X_10_0008">
<testtitle>PERFORM CONFIRMATION DRIVING PATTERN</testtitle>
<res>
<down ref="RM0000028K60F3X_10_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000028K60F3X_10_0003" proc-id="RM23G0E___00000SM00001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z5X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for 5 minutes or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC / Pending.</ptxt>
</test1>
<test1>
<ptxt>Read the pending DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.55in"/>
<colspec colname="COL2" colwidth="3.53in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2237, P2240, P2238, P2241, P2239, P2242, P2252, P2255, P2253 or P2256 output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000028K60F3X_10_0004" fin="true">A</down>
<right ref="RM0000028K60F3X_10_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000028K60F3X_10_0004">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000028K60F3X_10_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000028K60F3X_10_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>