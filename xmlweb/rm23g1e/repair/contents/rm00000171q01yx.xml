<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001H" variety="S001H">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001H_7B983_T00HR" variety="T00HR">
<name>HYDRAULIC BRAKE BOOSTER (for LHD)</name>
<para id="RM00000171Q01YX" category="A" type-id="30014" name-id="BR3UX-04" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM00000171Q01YX_01" type-id="01" category="01">
<s-1 id="RM00000171Q01YX_01_0041" proc-id="RM23G0E___0000AV500000">
<ptxt>INSTALL BRAKE BOOSTER GASKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new brake booster gasket to the hydraulic brake booster.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171Q01YX_01_0042" proc-id="RM23G0E___0000AV600000">
<ptxt>INSTALL HYDRAULIC BRAKE BOOSTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the hydraulic brake booster assembly with the 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>145</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 4 brake lines to the correct positions of the hydraulic brake booster assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="F051200E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: To front wheel cylinder RH</ptxt>
</item>
<item>
<ptxt>*2: To front wheel cylinder LH</ptxt>
</item>
<item>
<ptxt>*3: To rear wheel cylinder RH</ptxt>
</item>
<item>
<ptxt>*4: To rear wheel cylinder LH</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Using a union nut wrench, connect the 4 brake lines to the hydraulic brake booster assembly.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the 3 connectors to the hydraulic brake booster.</ptxt>
<figure>
<graphic graphicname="F051439E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171Q01YX_01_0066" proc-id="RM23G0E___0000ATR00000">
<ptxt>INSTALL PUSH ROD PIN
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of lithium soap base glycol grease to the inner surface of the hole on the brake pedal lever.</ptxt>
<figure>
<graphic graphicname="C215915E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lithium soap base glycol grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Set the master cylinder push rod clevis in place, insert the push rod pin from the left side of the vehicle, and then install a new clip.</ptxt>
<figure>
<graphic graphicname="C213444" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q01YX_01_0062" proc-id="RM23G0E___0000AV900000">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL AIRBAG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the lower No. 1 instrument panel airbag assembly (See page <xref label="Seep01" href="RM000003YPT01YX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171Q01YX_01_0050" proc-id="RM23G0E___0000AV700000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000171Q01YX_01_0072" proc-id="RM23G0E___0000ASS00000">
<ptxt>BLEED BRAKE SYSTEM
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>If air is bled without using the intelligent tester, damage or accidents may result. Therefore, always use the intelligent tester when bleeding air.</ptxt>
</atten2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</s2>
<s2>
<ptxt>Remove the brake master cylinder reservoir filler cap assembly.</ptxt>
</s2>
<s2>
<ptxt>Add brake fluid until the fluid level is between the MIN and MAX lines of the reservoir.</ptxt>
</s2>
<s2>
<ptxt>Repeatedly depress the brake pedal and bleed air from the bleeder plug of the front disc brake cylinder RH.</ptxt>
</s2>
<s2>
<ptxt>Repeat the step above until the air is completely bled, and then tighten the bleeder plug while depressing the brake pedal.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed the air from the bleeder plug of the front disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>With the brake pedal depressed, loosen the bleeder plug of the rear disc brake cylinder RH, continue to hold the brake pedal and allow brake fluid to be drained from the bleeder plug while the pump motor operates.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Air is bled as the pump motor operates while the brake pedal is being depressed.</ptxt>
</item>
<item>
<ptxt>Be sure to release the brake pedal to stop the motor after approximately 100 seconds of continuous operation.</ptxt>
</item>
<item>
<ptxt>As brake fluid is continuously drained while the pump operates, it is not necessary to repeatedly depress the brake pedal.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>When there is no more air in the brake fluid, tighten the bleeder plug, and then release the brake pedal.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed the air from the bleeder plug of the rear disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch off and connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</s2>
<s2>
<ptxt>Turn the intelligent tester on.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Utility / Air Bleeding.</ptxt>
<atten3>
<ptxt>To protect the solenoid from overheating, the solenoid operation stops automatically in 4 seconds, and then the solenoid will not respond to commands for an additional 20 seconds.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Repeatedly depress the brake pedal several times, and then, with the brake pedal depressed, turn FR Line on and bleed air.</ptxt>
<atten4>
<ptxt>Air returns to the brake master cylinder reservoir together with the brake fluid and is bled from the brake system.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>As it is not possible to visually confirm that air is being bled, repeat this step 10 times.</ptxt>
</item>
<item>
<ptxt>Do not loosen the bleeder plug.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Turn FL Line on and bleed air using the same procedures as for FR.</ptxt>
</s2>
<s2>
<ptxt>Turn RR Line on, loosen the bleeder plug of the rear disc brake cylinder RH and drain brake fluid.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Do not depress the brake pedal.</ptxt>
</item>
<item>
<ptxt>As brake fluid is automatically drained while the pump and solenoid operate, it is not necessary to operate the brake pedal.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Repeat the step above until the air is completely bled, and then tighten the bleeder plug.</ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Turn RL Line on and bleed air from the bleeder plug of the rear disc brake cylinder LH using the same procedure as for the RH side.</ptxt>
</s2>
<s2>
<ptxt>Turn the intelligent tester off and turn the ignition switch off.</ptxt>
</s2>
<s2>
<ptxt>Inspect for brake fluid leaks.</ptxt>
</s2>
<s2>
<ptxt>Check and adjust the brake fluid level (See page <xref label="Seep01" href="RM0000012XQ02ZX"/>).</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM0000046KV00IX"/>).</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171Q01YX_01_0057" proc-id="RM23G0E___0000AV800000">
<ptxt>CHECK AND ADJUST BRAKE PEDAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check and adjust brake pedal (See page <xref label="Seep01" href="RM000001QHK01SX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171Q01YX_01_0040" proc-id="RM23G0E___0000AV400000">
<ptxt>INSPECT BRAKE MASTER CYLINDER OPERATION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the brake master cylinder operation (See page <xref label="Seep01" href="RM00000171W020X_01_0004"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171Q01YX_01_0073" proc-id="RM23G0E___0000AVA00000">
<ptxt>PERFORM YAW RATE AND ACCELERATION SENSOR ZERO POINT CALIBRATION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When replacing the hydraulic brake booster assembly, perform yaw rate and acceleration sensor zero point calibration and acquire information about the following specification as necessary (See page <xref label="Seep01" href="RM00000452J00IX"/>).</ptxt>
<list1 type="unordered">
<item>
<ptxt>Downhill assist control calibration</ptxt>
</item>
<item>
<ptxt>Crawl control calibration</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>