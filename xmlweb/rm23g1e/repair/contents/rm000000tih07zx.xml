<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM000000TIH07ZX" category="D" type-id="303F3" name-id="ES08Z1-128" from="201210">
<name>DIAGNOSIS SYSTEM</name>
<subpara id="RM000000TIH07ZX_z0" proc-id="RM23G0E___00001R000001">
<content5 releasenbr="1">
<step1>
<ptxt>EURO-OBD</ptxt>
<ptxt>When troubleshooting Europe On-Board Diagnostic (Euro-OBD) vehicles, the vehicle must be connected to the intelligent tester. Various data output from the vehicle Engine Control Module (ECM) can then be read.</ptxt>
<figure>
<graphic graphicname="A208456" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>The Malfunction Indicator Lamp (MIL) is illuminated when the vehicle on-board computer detects a malfunction in the computer itself or in drive system components. In addition, the applicable Diagnostic Trouble Codes (DTCs) are stored in the ECM memory (See page <xref label="Seep01" href="RM0000031HW051X"/>).</ptxt>
<ptxt>When the malfunction does not reoccur, the MIL stays illuminated until the ignition switch is turned off, and the MIL turns off when the engine is started. Be aware that the DTCs remain stored in the ECM memory.</ptxt>
<ptxt>To check DTCs, connect the intelligent tester to the DLC3 on the vehicle.</ptxt>
</step1>
<step1>
<ptxt>M-OBD (EXCEPT EUROPEAN SPECIFICATION)</ptxt>
<ptxt>When troubleshooting Multiplex On-Board Diagnostic (M-OBD) vehicles, the vehicle must be connected to the intelligent tester. Various data output from the vehicle Engine Control Module (ECM) can then be read.</ptxt>
<ptxt>The Malfunction Indicator Lamp (MIL) is illuminated when the vehicle on-board computer detects a malfunction in the computer itself or in drive system components. In addition, the applicable Diagnostic Trouble Codes (DTCs) are stored in the ECM memory (See page <xref label="Seep02" href="RM0000031HW051X"/>).</ptxt>
<ptxt>When the malfunction does not reoccur, the MIL stays illuminated until the ignition switch is turned off, and the MIL turns off when the engine is started. Be aware that the DTCs remain stored in the ECM memory.</ptxt>
<ptxt>To check DTCs, connect the intelligent tester to the DLC3 on the vehicle.</ptxt>
</step1>
<step1>
<ptxt>NORMAL MODE AND CHECK MODE</ptxt>
<ptxt>The diagnosis system operates in "normal mode" during normal vehicle use. In normal mode, "2 trip detection logic" is used to ensure accurate detection of malfunctions. "Check mode" is also available to technicians as an option. In check mode, "1 trip detection logic" is used when simulating malfunction symptoms to increase increasing the ability of the system to detect malfunctions, including intermittent malfunctions (See page <xref label="Seep03" href="RM000000PDL0PFX"/>).</ptxt>
</step1>
<step1>
<ptxt>2 TRIP DETECTION LOGIC</ptxt>
<ptxt>When a malfunction is first detected, the malfunction is temporarily stored in the ECM memory (1st trip). If the same malfunction is detected during the next subsequent drive cycle, the MIL is illuminated (2nd trip).</ptxt>
</step1>
<step1>
<ptxt>FREEZE FRAME DATA</ptxt>
<ptxt>The ECM records vehicle and driving condition information as freeze frame data the moment a DTC is stored. When troubleshooting, freeze frame data can be helpful in determining whether the vehicle was moving or stationary, whether the engine was warmed up or not, as well as other data recorded at the time of a malfunction.</ptxt>
</step1>
<step1>
<ptxt>BATTERY VOLTAGE</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, recharge or replace the battery before proceeding.</ptxt>
</step1>
<step1>
<ptxt>MIL</ptxt>
<step2>
<ptxt>The MIL illuminates when the ignition switch is turned to ON and the engine is not running.</ptxt>
<figure>
<graphic graphicname="A208456" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>If the MIL is not illuminated, check the MIL circuit (See page <xref label="Seep04" href="RM000000WZ110BX"/>).</ptxt>
</atten4>
</step2>
<step2>
<ptxt>When the engine is started, the MIL should turn off. If the MIL remains on, the diagnosis system has detected a malfunction or abnormality in the ECD system.</ptxt>
</step2>
</step1>
<step1>
<ptxt>ALL READINESS</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>With "All Readiness", you can check whether or not the DTC judgment has been completed by using the intelligent tester.</ptxt>
</item>
<item>
<ptxt>Check "All Readiness" after simulating malfunction symptoms or for validation after finishing repairs.</ptxt>
</item>
</list1>
</atten4>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the tester on.</ptxt>
</step2>
<step2>
<ptxt>Clear the DTCs (See page <xref label="Seep05" href="RM000000PDK13TX"/>).</ptxt>
</step2>
<step2>
<ptxt>Perform the DTC judgment driving pattern to run the DTC judgment.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / All Readiness.</ptxt>
</step2>
<step2>
<ptxt>Input the DTCs to be confirmed.</ptxt>
</step2>
<step2>
<ptxt>Check the DTC judgment result.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Description</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NORMAL</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System normal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ABNORMAL</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment completed</ptxt>
</item>
<item>
<ptxt>System abnormal</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>INCOMPLETE</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>DTC judgment not completed</ptxt>
</item>
<item>
<ptxt>Perform driving after confirming DTC enabling conditions</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>N/A</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Unable to perform DTC judgment</ptxt>
</item>
<item>
<ptxt>Number of DTCs which do not fulfill DTC preconditions has reached ECU memory limit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>