<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A5_T00JT" variety="T00JT">
<name>REAR VIEW MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM000003Y8L00RX" category="C" type-id="80390" name-id="PM39J-04" from="201210">
<dtccode>C1613</dtccode>
<dtcname>ACC Voltage is Low or High</dtcname>
<subpara id="RM000003Y8L00RX_01" type-id="60" category="03" proc-id="RM23G0E___0000CZ400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored if the parking assist ECU judges as a result of its self check that the voltage received by terminal ACC is not normal.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.00in"/>
<colspec colname="COL2" colwidth="3.00in"/>
<colspec colname="COL3" colwidth="3.08in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1613</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>ACC voltage is low</ptxt>
</item>
<item>
<ptxt>ACC voltage is high</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Parking assist ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003Y8L00RX_02" type-id="32" category="03" proc-id="RM23G0E___0000CZ500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E195958E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003Y8L00RX_03" type-id="51" category="05" proc-id="RM23G0E___0000CZ600001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003Y8L00RX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003Y8L00RX_04_0001" proc-id="RM23G0E___0000CZ700001">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ACC.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / Parking Assist Monitor System / Data List.</ptxt>
</test1>
<test1>
<ptxt>Check the Data List for proper functioning of the following items.</ptxt>
<table pgwide="1">
<title>Parking Assist Monitor System</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ACC Voltage Low Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>ACC voltage input to parking assist ECU/OK or NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: ACC voltage is normal</ptxt>
<ptxt>NG: ACC voltage is abnormal (too low)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ACC Voltage High Status</ptxt>
</entry>
<entry valign="middle">
<ptxt>ACC voltage input to parking assist ECU/OK or NG</ptxt>
</entry>
<entry valign="middle">
<ptxt>OK: ACC voltage is normal</ptxt>
<ptxt>NG: ACC voltage is abnormal (too high)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK is displayed for both items (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK is displayed for both items (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG is displayed for either item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003Y8L00RX_04_0003" fin="true">A</down>
<right ref="RM000003Y8L00RX_04_0006" fin="true">B</right>
<right ref="RM000003Y8L00RX_04_0002" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000003Y8L00RX_04_0003">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM00000466X01EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003Y8L00RX_04_0006">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM00000470500GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003Y8L00RX_04_0002" proc-id="RM23G0E___0000CZ800001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARKING ASSIST ECU ACC VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
<figure>
<graphic graphicname="E185668E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I3-8 (ACC) - I3-3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Test in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Parking Assist ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003Y8L00RX_04_0004" fin="true">OK</down>
<right ref="RM000003Y8L00RX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003Y8L00RX_04_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003Y8L00RX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>