<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM00000188B06DX" category="C" type-id="303H3" name-id="ESMGL-74" from="201210">
<dtccode>P0180</dtccode>
<dtcname>Fuel Temperature Sensor "A" Circuit</dtcname>
<dtccode>P0182</dtccode>
<dtcname>Fuel Temperature Sensor "A" Circuit Low Input</dtcname>
<dtccode>P0183</dtccode>
<dtcname>Fuel Temperature Sensor "A" Circuit High Input</dtcname>
<subpara id="RM00000188B06DX_01" type-id="60" category="03" proc-id="RM23G0E___00001RS00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The fuel temperature sensor monitors the fuel temperature. The fuel temperature sensor has a thermistor that varies its resistance depending on the fuel temperature. When the fuel temperature is low, the resistance of the thermistor is high. When the temperature is high, the resistance is low. The variations of resistance are communicated to the ECM as changes of voltage (See Fig. 1).</ptxt>
<ptxt>The fuel temperature sensor is connected to the ECM. The 5 V power source voltage in the ECM is applied to the fuel temperature sensor from terminal THF via resistor R.</ptxt>
<ptxt>Resistor R and the fuel temperature sensor are connected in series. When the resistance value of the fuel temperature sensor changes due to a change in fuel temperature, the voltage at terminal THF also changes. Based on this signal, the ECM corrects the pressure control compensation of the fuel supply pump assembly.</ptxt>
<figure>
<graphic graphicname="A208462E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>P0180</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON for 0.5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in the fuel temperature sensor circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in fuel temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Fuel temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0182</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON for 0.5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Short in the fuel temperature sensor circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in fuel temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Fuel temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0183</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON for 0.5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open in the fuel temperature sensor circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in fuel temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Fuel temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0180</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>Fuel Temp</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>P0182</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>P0183</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="nonmark">
<item>
<ptxt>If DTC P0180, P0182 and/or P0183 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>Lack of power</ptxt>
</item>
</list2>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188B06DX_02" type-id="32" category="03" proc-id="RM23G0E___00001RT00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A210074E15" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000188B06DX_03" type-id="51" category="05" proc-id="RM23G0E___00001RU00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07OX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188B06DX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000188B06DX_05_0008" proc-id="RM23G0E___00001RV00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (FUEL TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Fuel Temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Same as the actual fuel temperature</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (Same as actual fuel temperature)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If there is an open circuit, the tester indicates -40°C (-40°F).</ptxt>
</item>
<item>
<ptxt>If there is a short circuit, the tester indicates 140°C (284°F) or higher.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000188B06DX_05_0009" fin="false">A</down>
<right ref="RM00000188B06DX_05_0011" fin="false">B</right>
<right ref="RM00000188B06DX_05_0024" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0009" proc-id="RM23G0E___00001RW00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR OPEN IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the fuel temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A204471E06" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect terminals 2 and 1 of the fuel temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Fuel Temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>140°C (284°F) or higher</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel temperature sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to fuel temperature sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the fuel temperature sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188B06DX_05_0017" fin="false">OK</down>
<right ref="RM00000188B06DX_05_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0017" proc-id="RM23G0E___00001S300001">
<testtitle>REPLACE FUEL SUPPLY PUMP ASSEMBLY (FUEL TEMPERATURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the fuel supply pump assembly (fuel temperature sensor) (See page <xref label="Seep01" href="RM0000045CH00AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188B06DX_05_0026" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0010" proc-id="RM23G0E___00001RX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (FUEL TEMPERATURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the fuel temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-2 - C90-4 (THF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C85-1 - C90-10 (ETHF)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the fuel temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188B06DX_05_0018" fin="false">OK</down>
<right ref="RM00000188B06DX_05_0016" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0018" proc-id="RM23G0E___00001S400001">
<testtitle>CONFIRM GOOD CONNECTION TO ECM. IF OK, REPLACE ECM.</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188B06DX_05_0024" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0011" proc-id="RM23G0E___00001RY00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the fuel temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A204586E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Fuel Temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>-40°C (-40°F)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel temperature sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the fuel temperature sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188B06DX_05_0015" fin="false">OK</down>
<right ref="RM00000188B06DX_05_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0015" proc-id="RM23G0E___00001S100001">
<testtitle>REPLACE FUEL SUPPLY PUMP ASSEMBLY (FUEL TEMPERATURE SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the fuel supply pump assembly (fuel temperature sensor) (See page <xref label="Seep01" href="RM0000045CH00AX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188B06DX_05_0026" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0012" proc-id="RM23G0E___00001RZ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (FUEL TEMPERATURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the fuel temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C85-2 or C90-4 (THF) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the fuel temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188B06DX_05_0013" fin="false">OK</down>
<right ref="RM00000188B06DX_05_0016" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0013" proc-id="RM23G0E___00001S000001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188B06DX_05_0024" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0026" proc-id="RM23G0E___00001S600001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802NX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188B06DX_05_0027" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0027" proc-id="RM23G0E___00001S700001">
<testtitle>PERFORM FUEL SUPPLY PUMP INITIALIZATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform fuel supply pump initialization (See page <xref label="Seep01" href="RM000000TIN05KX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188B06DX_05_0024" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0016" proc-id="RM23G0E___00001S200001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188B06DX_05_0024" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0024" proc-id="RM23G0E___00001S500001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK13TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 1 second or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188B06DX_05_0025" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188B06DX_05_0025">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>