<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000B" variety="S000B">
<name>5L-E ENGINE MECHANICAL</name>
<ttl id="12007_S000B_7B8XV_T007J" variety="T007J">
<name>ENGINE ASSEMBLY</name>
<para id="RM000000FYL015X" category="A" type-id="80001" name-id="EM9WE-04" from="201210">
<name>REMOVAL</name>
<subpara id="RM000000FYL015X_01" type-id="01" category="01">
<s-1 id="RM000000FYL015X_01_0439" proc-id="RM23G0E___00004YP00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0410" proc-id="RM23G0E___00004YH00001">
<ptxt>REMOVE BATTERY HOLD DOWN CLAMP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the nut and disconnect the engine wire.</ptxt>
<figure>
<graphic graphicname="A224625" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts and battery hold down clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0459">
<ptxt>REMOVE BATTERY</ptxt>
</s-1>
<s-1 id="RM000000FYL015X_01_0411">
<ptxt>REMOVE BATTERY TRAY</ptxt>
</s-1>
<s-1 id="RM000000FYL015X_01_0405" proc-id="RM23G0E___00004YG00001">
<ptxt>REMOVE HOOD SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the washer nozzle hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the 8 bolts and hood.</ptxt>
<figure>
<graphic graphicname="A222363" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>If the hood support is detached from the ball joint, it become non-reusable. Therefore, do not detach the hood support from the ball joint unless replacing it.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0442" proc-id="RM23G0E___00004YQ00001">
<ptxt>REMOVE COWL TOP VENTILATOR LOUVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the cowl top ventilator louver (See page <xref label="Seep01" href="RM000001OV303UX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0486" proc-id="RM23G0E___000014X00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 13 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0443" proc-id="RM23G0E___00004WI00001">
<ptxt>REMOVE FRONT BUMPER COVER LOWER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper cover lower.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0444" proc-id="RM23G0E___00004WJ00001">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Unhook the engine under cover from the vehicle body as shown in the illustration.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0495" proc-id="RM23G0E___00004ZD00001">
<ptxt>REMOVE TRANSMISSION UNDER COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and transmission under cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0445" proc-id="RM23G0E___00004YR00001">
<ptxt>REMOVE REAR ENGINE UNDER COVER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and rear engine under cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0363" proc-id="RM23G0E___000014W00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<figure>
<graphic graphicname="A224962E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Cap</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Drain the coolant by removing the radiator cap.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug and drain the coolant from the engine.</ptxt>
<atten3>
<ptxt>If coolant is not drained from the radiator drain cock plug, cylinder block drain cock plugs and radiator reservoir, clogging in the radiator or coolant leakage from the seal of the water pump may result.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0364" proc-id="RM23G0E___00004YE00000">
<ptxt>DRAIN ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the oil filler cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the oil drain plug and gasket, and then drain the oil into a container.</ptxt>
</s2>
<s2>
<ptxt>Clean the drain plug and install it with a new gasket.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>352</t-value2>
<t-value4>25</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0447" proc-id="RM23G0E___00004WU00001">
<ptxt>REMOVE FRONT FENDER APRON SEAL RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 clips and fender apron seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0448" proc-id="RM23G0E___00004YS00001">
<ptxt>REMOVE NO. 1 FRONT FENDER APRON TO FRAME SEAL RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 clips and frame seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0449" proc-id="RM23G0E___00004YT00001">
<ptxt>REMOVE FRONT FENDER APRON SEAL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 clips and fender apron seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0450" proc-id="RM23G0E___00004YU00001">
<ptxt>REMOVE NO. 1 FRONT FENDER APRON TO FRAME SEAL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 clips and frame seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0491" proc-id="RM23G0E___000014100000">
<ptxt>REMOVE RESONATOR WITH AIR CLEANER CAP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224304" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the sensor connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Loosen the hose clamp and remove the resonator with air cleaner cap.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A226728" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 4 hook clamps, and then remove the air cleaner cap and resonator with air cleaner cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0464">
<ptxt>REMOVE AIR CLEANER FILTER ELEMENT SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM000000FYL015X_01_0465" proc-id="RM23G0E___00004WV00001">
<ptxt>REMOVE AIR CLEANER CASE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and air cleaner case.</ptxt>
<figure>
<graphic graphicname="A224627" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0476" proc-id="RM23G0E___0000J2M00001">
<ptxt>REMOVE FRONT BUMPER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the front bumper cover.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts labeled A and 2 bolts labeled B.</ptxt>
</s2>
<s2>
<ptxt>Remove the 6 screws and 6 clips.</ptxt>
<figure>
<graphic graphicname="B236374E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 12 claws.</ptxt>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Disconnect the 3 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/o Fog Light:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>w/o TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/ Headlight Cleaner System:</ptxt>
<ptxt>Disconnect the headlight cleaner hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the front bumper cover.</ptxt>
<figure>
<graphic graphicname="B236375E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0477" proc-id="RM23G0E___00004Z400000">
<ptxt>REMOVE FRONT UPPER BUMPER RETAINER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and retainer.</ptxt>
<figure>
<graphic graphicname="A225008" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0478" proc-id="RM23G0E___00004Z500000">
<ptxt>REMOVE RADIATOR SIDE DEFLECTOR LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 3 claws and remove the clip. Then move the side deflector so that the radiator can be removed in the step below.</ptxt>
<figure>
<graphic graphicname="A225009" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0479" proc-id="RM23G0E___00004Z600000">
<ptxt>REMOVE RADIATOR SIDE DEFLECTOR RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, detach the 3 claws and remove the clip. Then move the side deflector so that the radiator can be removed in the step below.</ptxt>
<figure>
<graphic graphicname="A223074" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0480" proc-id="RM23G0E___00004WK00001">
<ptxt>REMOVE NO. 1 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp and remove the No. 1 radiator hose.</ptxt>
<figure>
<graphic graphicname="A218411" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts and hose clamp.</ptxt>
<figure>
<graphic graphicname="A218417" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0481" proc-id="RM23G0E___00004Z700001">
<ptxt>REMOVE NO. 2 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A218413" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0482" proc-id="RM23G0E___00004WM00001">
<ptxt>REMOVE FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 4 nuts holding the fluid coupling fan.</ptxt>
<figure>
<graphic graphicname="A218410" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the vane pump V belt and the fan and generator V belt (See page <xref label="Seep01" href="RM00000122Y00CX"/>).</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts holding the fan shroud.</ptxt>
<figure>
<graphic graphicname="A224938" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 nuts of the fluid coupling fan, and then remove the shroud together with the coupling fan.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the fan pulley from the water pump.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0483" proc-id="RM23G0E___00004Z800000">
<ptxt>REMOVE RADIATOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A218379" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and radiator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0470" proc-id="RM23G0E___00004UR00000">
<ptxt>REMOVE INTAKE PIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A224305E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Loosen the hose clamp and remove the 2 bolts and intake pipe.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0467" proc-id="RM23G0E___00004YY00001">
<ptxt>DISCONNECT COOLER COMPRESSOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and idle pulley bracket.</ptxt>
<figure>
<graphic graphicname="A225293" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="A224370" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and disconnect the cooler compressor.</ptxt>
<atten4>
<ptxt>It is not necessary to completely remove the cooler compressor. With the hoses connected to the compressor, hang the compressor on the vehicle body with a rope.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0468" proc-id="RM23G0E___00004YZ00001">
<ptxt>REMOVE GENERATOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the terminal cap.</ptxt>
<figure>
<graphic graphicname="A224320" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and generator wire.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the generator connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the vacuum pump hose.</ptxt>
<figure>
<graphic graphicname="A224321" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the union bolt to disconnect the vacuum pump oil inlet hose and remove the 2 gaskets.</ptxt>
</s2>
<s2>
<ptxt>Detach the vacuum pump oil inlet hose from the cord clip.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the vacuum pump oil outlet hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and generator.</ptxt>
<figure>
<graphic graphicname="A224322" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0490" proc-id="RM23G0E___00004ZC00001">
<ptxt>REMOVE GLOVE COMPARTMENT DOOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the glove compartment door (See page <xref label="Seep01" href="RM0000046J800FX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0461" proc-id="RM23G0E___00004YV00001">
<ptxt>DISCONNECT ENGINE WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts, detach the clamp and disconnect the engine wire.</ptxt>
<figure>
<graphic graphicname="A224626" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 1 relay block cover.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and detach the 2 claws.</ptxt>
<figure>
<graphic graphicname="A224629" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clamps and disconnect the engine wire.</ptxt>
</s2>
<s2>
<ptxt>for LHD:</ptxt>
<figure>
<graphic graphicname="A225291" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Detach the grommet from the wire harness support.</ptxt>
<figure>
<graphic graphicname="A225401E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 4 claws to remove the wire harness support from the vehicle, and then pull out the ECM connector to remove it from the vehicle.</ptxt>
<figure>
<graphic graphicname="A225402E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the clamp and disconnect the 5 connectors as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A225509E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0493" proc-id="RM23G0E___00004WX00001">
<ptxt>REMOVE WIRING HARNESS CLAMP BRACKET (for LHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and wiring harness clamp bracket.</ptxt>
<figure>
<graphic graphicname="A225292" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0462" proc-id="RM23G0E___00004YW00001">
<ptxt>DISCONNECT HEATER WATER HOSE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the heater water hoses.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Rear Heater</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="A224705E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0408">
<ptxt>LOOSEN FUEL TANK CAP ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM000000FYL015X_01_0475" proc-id="RM23G0E___00004Z200000">
<ptxt>DRAIN FUEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the fuel filter drain plug and drain the fuel from the fuel filter.</ptxt>
<figure>
<graphic graphicname="A225894" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0463" proc-id="RM23G0E___00004YX00001">
<ptxt>DISCONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 fuel hoses.</ptxt>
<figure>
<graphic graphicname="A121131" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0315" proc-id="RM23G0E___00004YD00001">
<ptxt>DISCONNECT VANE PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, hold the pulley and loosen the nut.</ptxt>
<figure>
<graphic graphicname="A071326E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09960-10010</s-number>
<s-subnumber>09962-01000</s-subnumber>
<s-subnumber>09963-01000</s-subnumber>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the nut and vane pump pulley from the vane pump shaft.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and nut and disconnect the vane pump.</ptxt>
<atten4>
<ptxt>Disconnect the vane pump from the vehicle with the hoses connected, and hang it with a rope.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0497" proc-id="RM23G0E___000071100001">
<ptxt>REMOVE CLUTCH RELEASE CYLINDER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="C215983" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and disconnect the release cylinder.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0430" proc-id="RM23G0E___00004YL00001">
<ptxt>REMOVE STARTER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the starter connector.</ptxt>
<figure>
<graphic graphicname="A224315" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the starter wire.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut, 2 bolts and starter assembly.</ptxt>
<figure>
<graphic graphicname="A224316" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0489" proc-id="RM23G0E___00004ZB00000">
<ptxt>REMOVE OIL FILTER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the oil filter.</ptxt>
<sst>
<sstitem>
<s-number>09228-44011</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A227791E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0431" proc-id="RM23G0E___00004YM00000">
<ptxt>REMOVE FRONT EXHAUST PIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt from the clamp.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A222715" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 2 bolts and No. 1 exhaust pipe support bracket.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="A222738" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 nuts and front exhaust pipe.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0484" proc-id="RM23G0E___00003VW00000">
<ptxt>REMOVE FRONT PROPELLER SHAFT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place matchmarks on the propeller shaft flange and differential.</ptxt>
<figure>
<graphic graphicname="C098708E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 4 nuts, 4 bolts, 4 washers and front propeller shaft assembly.</ptxt>
</s2>
<s2>
<ptxt>Place matchmarks on the propeller shaft flange and transfer flange.</ptxt>
<figure>
<graphic graphicname="C098709E10" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 4 nuts, 4 washers and front propeller shaft assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0485" proc-id="RM23G0E___00003VX00000">
<ptxt>REMOVE PROPELLER SHAFT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place matchmarks on the propeller shaft flange and transfer flange.</ptxt>
<figure>
<graphic graphicname="C219704E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 3 Door</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 5 Door</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 4 nuts and 4 washers.</ptxt>
</s2>
<s2>
<ptxt>Place matchmarks on the propeller shaft flange and differential flange.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 3 Door</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 5 Door</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="C219705E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 nuts, 4 bolts and 4 washers.</ptxt>
</s2>
<s2>
<ptxt>Remove the propeller shaft.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0417" proc-id="RM23G0E___00004YI00001">
<ptxt>REMOVE MANUAL TRANSMISSION ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the manual transmission (See page <xref label="Seep01" href="RM0000048B4006X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0472" proc-id="RM23G0E___00004Z100001">
<ptxt>REMOVE REAR NO. 1 ENGINE MOUNTING INSULATOR</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Perform this procedure when replacement of the engine mounting insulator is necessary.</ptxt>
</atten3>
<s2>
<ptxt>Remove the 4 bolts and rear engine mounting insulator from the transmission.</ptxt>
<figure>
<graphic graphicname="A225172" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0432" proc-id="RM23G0E___00008I000001">
<ptxt>REMOVE CLUTCH COVER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place matchmarks on the clutch cover and flywheel.</ptxt>
<figure>
<graphic graphicname="C215521E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Loosen each set bolt one turn at a time until spring tension is released.</ptxt>
</s2>
<s2>
<ptxt>Remove the 6 set bolts and pull off the clutch cover.</ptxt>
<atten3>
<ptxt>Do not drop the clutch disc.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0433" proc-id="RM23G0E___00008I100001">
<ptxt>REMOVE CLUTCH DISC ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Keep the lining part of the clutch disc, the pressure plate and the surface of the flywheel away from oil and foreign matter.</ptxt>
</atten3>
</content1></s-1>
<s-1 id="RM000000FYL015X_01_0421" proc-id="RM23G0E___00004YJ00001">
<ptxt>REMOVE FLYWHEEL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A058716E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using SST, hold the crankshaft.</ptxt>
<sst>
<sstitem>
<s-number>09213-54015</s-number>
<s-subnumber>91651-60855</s-subnumber>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
</s2>
<s2>
<ptxt>Remove the 8 bolts and flywheel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0403" proc-id="RM23G0E___00004YF00001">
<ptxt>REMOVE REAR END PLATE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and rear end plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0496">
<ptxt>REMOVE FLYWHEEL HOUSING DUST SEAL</ptxt>
</s-1>
<s-1 id="RM000000FYL015X_01_0314" proc-id="RM23G0E___00004Y000001">
<ptxt>INSTALL ENGINE HANGER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install an engine hanger to each location shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A225772E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 Engine Hanger</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Engine Hanger</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<tbody>
<row>
<entry>
<ptxt>No. 1 Engine Hanger</ptxt>
</entry>
<entry>
<ptxt>12281-54080</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>No. 2 Engine Hanger</ptxt>
</entry>
<entry>
<ptxt>12282-54070</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Bolt (No. 1 Engine Hanger)</ptxt>
</entry>
<entry>
<ptxt>90119-10736</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Bolt (No. 2 Engine Hanger)</ptxt>
</entry>
<entry>
<ptxt>91622-61022</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
<torque>
<subtitle>for No. 1 Engine Hanger</subtitle>
<torqueitem>
<t-value1>59</t-value1>
<t-value2>602</t-value2>
<t-value4>44</t-value4>
</torqueitem>
<subtitle>for No. 2 Engine Hanger</subtitle>
<torqueitem>
<t-value1>37</t-value1>
<t-value2>377</t-value2>
<t-value4>27</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Install the engine hangers with new bolts.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0422" proc-id="RM23G0E___00004YK00001">
<ptxt>REMOVE ENGINE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach an engine sling device and hang the engine with a chain block.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts and 4 bolts from the front engine mounting insulator LH and RH.</ptxt>
<figure>
<graphic graphicname="A226979E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the engine by operating the engine sling device and chain block.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0471" proc-id="RM23G0E___00004Z000001">
<ptxt>INSTALL ENGINE TO ENGINE STAND</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the engine to an engine stand with bolts.</ptxt>
</s2>
<s2>
<ptxt>Remove engine hanger.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0487" proc-id="RM23G0E___00004Z900001">
<ptxt>REMOVE ENGINE WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the engine wire from the engine.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000FYL015X_01_0488" proc-id="RM23G0E___00004ZA00001">
<ptxt>REMOVE FRONT ENGINE MOUNTING INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and 2 front engine mounting insulators.</ptxt>
<figure>
<graphic graphicname="A226990" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>