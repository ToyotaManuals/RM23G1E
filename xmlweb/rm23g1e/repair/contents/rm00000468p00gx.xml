<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9D0_T00MO" variety="T00MO">
<name>REAR NO. 1 SEAT ASSEMBLY (for 60/40 Split Double-folding Seat Type LH Side)</name>
<para id="RM00000468P00GX" category="A" type-id="80001" name-id="SE77X-02" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM00000468P00GX_02" type-id="11" category="10" proc-id="RM23G0E___0000GF500000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
</content3>
</subpara>
<subpara id="RM00000468P00GX_01" type-id="01" category="01">
<s-1 id="RM00000468P00GX_01_0001" proc-id="RM23G0E___0000GET00000">
<ptxt>REMOVE REAR SEAT CUSHION HINGE COVER RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237973" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0002" proc-id="RM23G0E___0000GEU00000">
<ptxt>REMOVE REAR SEAT CUSHION HINGE COVER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237974" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0003" proc-id="RM23G0E___0000GEV00000">
<ptxt>REMOVE REAR SEAT CUSHION ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237975" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Pull the rear seat cushion band and release the lock.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and seat cushion.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0012" proc-id="RM23G0E___0000GF200000">
<ptxt>REMOVE REAR SEAT HEADREST ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the headrest.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0004" proc-id="RM23G0E___0000GEW00000">
<ptxt>REMOVE REAR SEATBACK COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Press the seatback lock release button to release the lock and fold down the seatback.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B237641" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a screwdriver, detach the 4 claws and remove the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0013" proc-id="RM23G0E___0000GF300000">
<ptxt>REMOVE REAR SEATBACK BOARD CARPET ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237977" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a clip remover, detach the 5 clips.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the carpet.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0007" proc-id="RM23G0E___0000GEX00000">
<ptxt>REMOVE REAR SEATBACK ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237979" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts that secure the seatback to the floor.</ptxt>
</s2>
<s2>
<ptxt>Fold down the seatback, remove the bolt from the hinge, and then remove the seatback.</ptxt>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0008" proc-id="RM23G0E___0000GEY00000">
<ptxt>REMOVE REAR SEATBACK HINGE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237980" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and hinge.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0011" proc-id="RM23G0E___0000GF100000">
<ptxt>REMOVE REAR SEATBACK LOCK CONTROL LEVER BASE LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a moulding remover, detach the 6 claws.</ptxt>
<figure>
<graphic graphicname="B237981" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Move the cover in the direction of the arrow to detach the 2 guides and remove the lever base.</ptxt>
</s2>
<s2>
<ptxt>Detach the cable clamp and disconnect the cable.</ptxt>
<figure>
<graphic graphicname="B237983" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0028" proc-id="RM23G0E___0000GF400000">
<ptxt>REMOVE REAR NO. 2 SEAT ASSEMBLY LH (w/ Rear No. 2 Seat)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Face to Face Seat Type:</ptxt>
<ptxt>Remove the rear No. 2 seat assembly (See page <xref label="Seep01" href="RM0000045DX003X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0014" proc-id="RM23G0E___0000C3T00000">
<ptxt>REMOVE REAR DOOR SCUFF PLATE LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238733E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the rear door scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 3 clips, 6 claws and 2 guides and remove the rear door scuff plate.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0015" proc-id="RM23G0E___0000C3U00000">
<ptxt>REMOVE REAR DOOR OPENING TRIM WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238780" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the rear door opening trim weatherstrip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0016" proc-id="RM23G0E___0000C3W00000">
<ptxt>REMOVE TONNEAU COVER ASSEMBLY (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the tonneau cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0017" proc-id="RM23G0E___0000C3X00000">
<ptxt>REMOVE FRONT LUGGAGE COMPARTMENT TRIM COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240044" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other front luggage compartment trim cover.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 4 claws and remove the cap.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B238749E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt, luggage hold belt striker and front luggage compartment trim cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Luggage Hold Belt Striker</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0018" proc-id="RM23G0E___0000C3Y00000">
<ptxt>REMOVE NO. 1 DECK BOARD SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238750" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the clip and remove the No. 1 deck board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0019" proc-id="RM23G0E___0000C3Z00000">
<ptxt>REMOVE REAR FLOOR REAR MAT SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238748" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 5 clips and 4 claws and remove the rear floor mat rear support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0020" proc-id="RM23G0E___0000C4000000">
<ptxt>REMOVE FLOOR SIDE RAIL LH (w/ Deck Rail)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238764" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 3 bolts and floor side rail.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0021" proc-id="RM23G0E___0000C4100000">
<ptxt>REMOVE FLOOR SIDE RAIL RH (w/ Deck Rail)
</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0022" proc-id="RM23G0E___0000C4200000">
<ptxt>REMOVE REAR FLOOR CARPET ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 claws and remove the rear floor carpet.</ptxt>
<figure>
<graphic graphicname="B238751E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Deck Rail</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Deck Rail</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Face to Face Seat Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0023" proc-id="RM23G0E___0000C4300000">
<ptxt>REMOVE REAR NO. 1 SEAT OUTER LAP BELT ANCHOR COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238752" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other rear No. 1 seat outer lap belt anchor cover.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 3 claws and remove the rear No. 1 seat outer lap belt anchor cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0024" proc-id="RM23G0E___0000C4400000">
<ptxt>REMOVE NO. 1 LUGGAGE COMPARTMENT TRIM HOOK
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181695" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other No. 1 luggage compartment trim hook.</ptxt>
</atten4>
<s2>
<ptxt>Remove the luggage compartment trim hook by turning it clockwise.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0025" proc-id="RM23G0E___0000C4500000">
<ptxt>REMOVE NO. 1 TONNEAU COVER HOLDER CAP (w/o Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238754E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other No. 1 tonneau cover holder cap.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the No. 1 tonneau cover holder cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0026" proc-id="RM23G0E___0000C4600000">
<ptxt>REMOVE FRONT DECK SIDE TRIM COVER (w/ Tonneau Cover)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240051E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other front deck side trim cover.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the front deck side trim cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0027" proc-id="RM23G0E___0000C4800000">
<ptxt>REMOVE DECK TRIM SIDE PANEL ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Rear No. 2 Seat:</ptxt>
<s3>
<figure>
<graphic graphicname="B238753" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear No. 1 seat outer belt floor anchor.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="B238756" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 6 claws and 2 guides and disconnect the rear seatback lock control lever base.</ptxt>
</s3>
<s3>
<ptxt>Remove the 4 bolts and 2 screws.</ptxt>
</s3>
<s3>
<ptxt>Detach the 5 clips and 8 claws.</ptxt>
</s3>
<s3>
<ptxt>Pass the rear seatback lock control lever base through the deck trim side panel and remove the deck trim side panel.</ptxt>
<figure>
<graphic graphicname="B238755" width="7.106578999in" height="3.779676365in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/ Rear No. 2 Seat:</ptxt>
<s3>
<figure>
<graphic graphicname="B238753" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear No. 1 seat outer belt floor anchor.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="B238759" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear No. 2 seat outer belt floor anchor.</ptxt>
</s3>
<s3>
<ptxt>Remove the 2 bolts and 2 screws.</ptxt>
</s3>
<s3>
<ptxt>Detach the 4 clips and 8 claws and remove the deck trim side panel.</ptxt>
<figure>
<graphic graphicname="B238760" width="7.106578999in" height="3.779676365in"/>
</figure>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM00000468P00GX_01_0009" proc-id="RM23G0E___0000GEZ00000">
<ptxt>REMOVE REAR NO. 2 SEATBACK LOCK STRIKER SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237984" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the claw and remove the striker.</ptxt>
<atten3>
<ptxt>Do not wipe off the grease on the sliding portion of the striker.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468P00GX_01_0010" proc-id="RM23G0E___0000GF000000">
<ptxt>REMOVE REAR SEAT CUSHION LOCK STRIKER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B237985" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and striker.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>