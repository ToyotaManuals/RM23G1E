<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000000TI405AX" category="J" type-id="3009O" name-id="ESZI6-02" from="201207" to="201210">
<dtccode/>
<dtcname>Starter Signal Circuit</dtcname>
<subpara id="RM000000TI405AX_01" type-id="60" category="03" proc-id="RM23G0E___00000IS00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>While the engine is being cranked, current flows from terminal ST1 of the ignition switch (w/o entry and start system) or terminal STAR of the power management control ECU (w/ entry and start system) to the park/neutral position switch (for automatic transmission) or clutch start switch (for manual transmission) and also flows to terminal STA of the ECM (STA Signal).</ptxt>
</content5>
</subpara>
<subpara id="RM000000TI405AX_02" type-id="32" category="03" proc-id="RM23G0E___00000IT00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0617 (See page <xref label="Seep01" href="RM000000TA0140X_05"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000TI405AX_03" type-id="51" category="05" proc-id="RM23G0E___00000IU00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>This chart is based on the premise that the engine can crank normally. If the engine cannot crank normally, proceed to the problem symptoms table (See page <xref label="Seep03" href="RM000000PDG0P1X"/>). </ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TI405AX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TI405AX_04_0014" proc-id="RM23G0E___00000IV00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Ignition Switch Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>START</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0004" fin="true">OK</down>
<right ref="RM000000TI405AX_04_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0015" proc-id="RM23G0E___00000IW00000">
<testtitle>CHECK STARTER RELAY (ST) (POWER SOURCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
<figure>
<graphic graphicname="A227736E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 5 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine cranking</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>The engine does not crank because the relay is not installed.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>NG (w/ Entry and start system)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>for Manual transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>for Automatic transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>NG (w/o Entry and start system)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>for Manual transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>for Automatic transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Room Relay Block</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter relay (ST)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0016" fin="false">A</down>
<right ref="RM000000TI405AX_04_0018" fin="false">B</right>
<right ref="RM000000TI405AX_04_0041" fin="false">C</right>
<right ref="RM000000TI405AX_04_0048" fin="false">D</right>
<right ref="RM000000TI405AX_04_0051" fin="false">E</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0016" proc-id="RM23G0E___00000IX00000">
<testtitle>INSPECT STARTER RELAY (ST)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the starter relay (ST) (See page <xref label="Seep01" href="RM000001FFP03NX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0017" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0017" proc-id="RM23G0E___00000IY00000">
<testtitle>CHECK HARNESS AND CONNECTOR (STARTER RELAY (ST) - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the ST relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0060" fin="true">OK</down>
<right ref="RM000000TI405AX_04_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0018" proc-id="RM23G0E___00000IZ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (STARTER RELAY (ST) - CLUTCH START SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the clutch start switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 - A40-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 - r1-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 or A40-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 or r1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the clutch start switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0020" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0024" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0020" proc-id="RM23G0E___00000J100000">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH START SWITCH - POWER MANAGEMENT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the clutch start switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A40-2 - G50-3 (STAR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>r1-2 - G50-3 (STAR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A40-2 or G50-3 (STAR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>r1-2 or G50-3 (STAR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the clutch start switch connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the power management control ECU connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0019" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0035" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0019" proc-id="RM23G0E___00000J000000">
<testtitle>INSPECT CLUTCH START SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the clutch start switch assembly (See page <xref label="Seep01" href="RM0000032S3007X_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0036" fin="true">OK</down>
<right ref="RM000000TI405AX_04_0025" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0041" proc-id="RM23G0E___00000J200000">
<testtitle>CHECK HARNESS AND CONNECTOR (STARTER RELAY (ST) - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 - C40-5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 or C40-5 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0042" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0045" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0042" proc-id="RM23G0E___00000J300000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - POWER MANAGEMENT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-4 - G50-3 (STAR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-4 or G50-3 (STAR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the power management control ECU connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0043" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0047" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0043" proc-id="RM23G0E___00000J400000">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch assembly (See page <xref label="Seep01" href="RM000002BKX033X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0061" fin="true">OK</down>
<right ref="RM000000TI405AX_04_0044" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0048" proc-id="RM23G0E___00000J500000">
<testtitle>CHECK HARNESS AND CONNECTOR (STARTER RELAY (ST) - CLUTCH START SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the clutch start switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 - A40-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 - r1-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 or A40-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 or r1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the clutch start switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0049" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0054" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0049" proc-id="RM23G0E___00000J600000">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH START SWITCH - IGNITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the clutch start switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ignition switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A40-2 - G2-1 (ST1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>r1-2 - G2-1 (ST1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A40-2 or G2-1 (ST1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>r1-2 or G2-1 (ST1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the clutch start switch connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ignition switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0050" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0055" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0050" proc-id="RM23G0E___00000J700000">
<testtitle>INSPECT CLUTCH START SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the clutch start switch assembly (See page <xref label="Seep01" href="RM0000032S3007X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0062" fin="true">OK</down>
<right ref="RM000000TI405AX_04_0056" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0051" proc-id="RM23G0E___00000J800000">
<testtitle>CHECK HARNESS AND CONNECTOR (STARTER RELAY (ST) - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 - C40-5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Starter relay (ST) terminal 1 or C40-5 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0052" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0057" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0052" proc-id="RM23G0E___00000J900000">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - IGNITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ignition switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-4 - G2-1 (ST1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-4 or G2-1 (ST1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ignition switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0053" fin="false">OK</down>
<right ref="RM000000TI405AX_04_0058" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0053" proc-id="RM23G0E___00000JA00000">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch assembly (See page <xref label="Seep01" href="RM000002BKX033X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI405AX_04_0063" fin="true">OK</down>
<right ref="RM000000TI405AX_04_0059" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI405AX_04_0021">
<testtitle>REPLACE STARTER RELAY (ST)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0022">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (STARTER RELAY (ST) - BODY GROUND)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0024">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (STARTER RELAY (ST) - CLUTCH START SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0035">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (CLUTCH START SWITCH - POWER MANAGEMENT CONTROL ECU)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0025">
<testtitle>REPLACE CLUTCH START SWITCH ASSEMBLY<xref label="Seep01" href="RM00000176C00FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0045">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (STARTER RELAY (ST) - PARK/NEUTRAL POSITION SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0047">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (PARK/NEUTRAL POSITION SWITCH - POWER MANAGEMENT CONTROL ECU)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0044">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0054">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (STARTER RELAY (ST) - CLUTCH START SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0055">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (CLUTCH START SWITCH - IGNITION SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0056">
<testtitle>REPLACE CLUTCH START SWITCH ASSEMBLY<xref label="Seep01" href="RM00000176C00FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0057">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (STARTER RELAY (ST) - PARK/NEUTRAL POSITION SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0058">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (PARK/NEUTRAL POSITION SWITCH - IGNITION SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0059">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000000PDG0P1X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0060">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (ECM - CLUTCH PEDAL SWITCH OR PARK/NEUTRAL POSITION SWITCH)</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0036">
<testtitle>GO TO ENTRY AND START SYSTEM<xref label="Seep01" href="RM000000YEF0FUX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0061">
<testtitle>GO TO ENTRY AND START SYSTEM<xref label="Seep01" href="RM000000YEF0FUX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0062">
<testtitle>REPLACE IGNITION SWITCH ASSEMBLY<xref label="Seep01" href="RM000000YK102EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI405AX_04_0063">
<testtitle>REPLACE IGNITION SWITCH ASSEMBLY<xref label="Seep01" href="RM000000YK102EX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>