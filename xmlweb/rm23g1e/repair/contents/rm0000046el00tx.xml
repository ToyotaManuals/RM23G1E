<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000M" variety="S000M">
<name>2TR-FE INTAKE / EXHAUST</name>
<ttl id="12010_S000M_7B90X_T00AL" variety="T00AL">
<name>INTAKE MANIFOLD</name>
<para id="RM0000046EL00TX" category="A" type-id="30014" name-id="IE11V-03" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM0000046EL00TX_01" type-id="01" category="01">
<s-1 id="RM0000046EL00TX_01_0001" proc-id="RM23G0E___00006GA00000">
<ptxt>INSTALL INTAKE MANIFOLD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 fuel vapor feed hose to the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 wire harness clamp brackets to the intake manifold with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install a new gasket to the intake manifold.</ptxt>
<figure>
<graphic graphicname="C213697" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the intake manifold with the 5 bolts and 2 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>for Engine Rear Side:</ptxt>
<ptxt>Attach the No. 2 water by-pass hose and connect the No. 3 ventilation hose to the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>for Engine Front Side:</ptxt>
<ptxt>Attach the 2 wire harness clamps to the 2 wire harness clamp brackets.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EL00TX_01_0002" proc-id="RM23G0E___000053L00000">
<ptxt>INSTALL PURGE VSV
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the purge VSV with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 2 purge line hoses to the purge VSV.</ptxt>
</s2>
<s2>
<ptxt>Connect the purge VSV connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046EL00TX_01_0003" proc-id="RM23G0E___00006GB00000">
<ptxt>INSTALL FUEL DELIVERY PIPE WITH FUEL INJECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel delivery pipe with fuel injector (See page <xref label="Seep01" href="RM000000Q4608LX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EL00TX_01_0004" proc-id="RM23G0E___00006GC00000">
<ptxt>INSTALL THROTTLE WITH MOTOR BODY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the throttle with motor body (See page <xref label="Seep01" href="RM000000VWL02NX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EL00TX_01_0007" proc-id="RM23G0E___00006GE00000">
<ptxt>INSTALL STARTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 1.4 kW Type:</ptxt>
<ptxt>Install the starter (See page <xref label="Seep01" href="RM000003REW00GX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 2.0 kW Type:</ptxt>
<ptxt>Install the starter (See page <xref label="Seep02" href="RM00000179J00IX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EL00TX_01_0009" proc-id="RM23G0E___00006GG00000">
<ptxt>INSTALL TRANSMISSION OIL FILLER TUBE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C213708" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Coat a new O-ring with ATF, and install it to the oil filler tube.</ptxt>
</s2>
<s2>
<ptxt>Install the oil filler tube to the transmission with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>14</t-value1>
<t-value2>143</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the oil dipstick.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046EL00TX_01_0005" proc-id="RM23G0E___00005AS00000">
<ptxt>INSTALL FRONT NO. 1 FENDER APRON TO FRAME SEAL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front fender apron to frame seal with the 5 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EL00TX_01_0006" proc-id="RM23G0E___00006GD00000">
<ptxt>INSTALL FRONT FENDER APRON SEAL LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front fender apron seal with the 5 clips.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046EL00TX_01_0008" proc-id="RM23G0E___00006GF00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>