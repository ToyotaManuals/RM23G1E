<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E8_T00NW" variety="T00NW">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000001GFJ04FX" category="J" type-id="301RT" name-id="ACC34-04" from="201210">
<dtccode/>
<dtcname>PTC Heater Circuit</dtcname>
<subpara id="RM000001GFJ04FX_01" type-id="60" category="03" proc-id="RM23G0E___0000HCD00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>PTC heater relays are closed in accordance with signals from the air conditioning amplifier assembly and power is supplied to the quick heater assembly installed on the radiator heater unit.</ptxt>
</content5>
</subpara>
<subpara id="RM000001GFJ04FX_02" type-id="32" category="03" proc-id="RM23G0E___0000HCE00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E200726E02" width="7.106578999in" height="7.795582503in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001GFJ04FX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001GFJ04FX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001GFJ04FX_05_0001" proc-id="RM23G0E___0000HCF00001">
<testtitle>INSPECT PTC HEATER RELAY (PTC NO. 1, PTC NO. 2, PTC NO. 3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the PTC NO. 1, PTC NO. 2 and PTC NO. 3 relays from the engine room No. 2 relay block.</ptxt>
<figure>
<graphic graphicname="E199507" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage is not applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Battery voltage is applied to terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04FX_05_0003" fin="false">OK</down>
<right ref="RM000001GFJ04FX_05_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0003" proc-id="RM23G0E___0000HCG00001">
<testtitle>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the air conditioning amplifier assembly with its connectors still connected (See page <xref label="Seep01" href="RM0000039R501DX"/>).</ptxt>
<figure>
<graphic graphicname="E197830E10" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.25in"/>
<colspec colname="COLSPEC1" colwidth="1.92in"/>
<colspec colname="COL2" colwidth="0.96in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G25-3 (PTC1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine running (1250 rpm or more)</ptxt>
</item>
<item>
<ptxt>Temperature setting MAX HOT</ptxt>
</item>
<item>
<ptxt>Outside temperature 10°C (50°F) or less</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature 79°C (174°F) or less</ptxt>
</item>
<item>
<ptxt>Headlight dimmer switch off</ptxt>
</item>
<item>
<ptxt>Blower switch off</ptxt>
</item>
</list1>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 V*</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-22 (PTC2) - Body ground</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Engine running (1250 rpm or more)</ptxt>
</item>
<item>
<ptxt>Temperature setting MAX HOT</ptxt>
</item>
<item>
<ptxt>Outside temperature 10°C (50°F) or less</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature 76°C (169°F) or less</ptxt>
</item>
<item>
<ptxt>Headlight dimmer switch off</ptxt>
</item>
<item>
<ptxt>Blower switch off</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-4 (PTC3) - Body ground</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Engine running (1250 rpm or more)</ptxt>
</item>
<item>
<ptxt>Temperature setting MAX HOT</ptxt>
</item>
<item>
<ptxt>Outside temperature 10°C (50°F) or less</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature 73°C (163°F) or less</ptxt>
</item>
<item>
<ptxt>Headlight dimmer switch off</ptxt>
</item>
<item>
<ptxt>Blower switch off</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-3 (PTC1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine running (1250 rpm or more)</ptxt>
</item>
<item>
<ptxt>Temperature setting MAX HOT</ptxt>
</item>
<item>
<ptxt>Outside temperature 10°C (50°F) or less</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature 79°C (174°F) or less</ptxt>
</item>
<item>
<ptxt>Headlight dimmer switch off</ptxt>
</item>
<item>
<ptxt>Blower switch on</ptxt>
</item>
</list1>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>11 to 14 V*</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-22 (PTC2) - Body ground</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Engine running (1250 rpm or more)</ptxt>
</item>
<item>
<ptxt>Temperature setting MAX HOT</ptxt>
</item>
<item>
<ptxt>Outside temperature 10°C (50°F) or less</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature 76°C (169°F) or less</ptxt>
</item>
<item>
<ptxt>Headlight dimmer switch off</ptxt>
</item>
<item>
<ptxt>Blower switch on</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-4 (PTC3) - Body ground</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Engine running (1250 rpm or more)</ptxt>
</item>
<item>
<ptxt>Temperature setting MAX HOT</ptxt>
</item>
<item>
<ptxt>Outside temperature 10°C (50°F) or less</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature 73°C (163°F) or less</ptxt>
</item>
<item>
<ptxt>Headlight dimmer switch off</ptxt>
</item>
<item>
<ptxt>Blower switch on</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Air Conditioning Amplifier Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: After the measurement conditions are met, wait 30 seconds before performing measurements.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04FX_05_0027" fin="false">OK</down>
<right ref="RM000001GFJ04FX_05_0026" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0027" proc-id="RM23G0E___0000HCL00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ENGINE ROOM NO. 2 RELAY BLOCK - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the PTC heater relays from the engine room No. 2 relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G25 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 1 relay terminal 1 - G25-3 (PTC1)</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 2 relay terminal 1 - G25-22 (PTC2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 3 relay terminal 1 - G25-4 (PTC3)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 1 relay terminal 1 - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 2 relay terminal 1 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 3 relay terminal 1 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04FX_05_0015" fin="false">OK</down>
<right ref="RM000001GFJ04FX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0015" proc-id="RM23G0E___0000HCH00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ENGINE ROOM NO. 2 RELAY BLOCK - QUICK HEATER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the PTC heater relays from the engine room No. 2 relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G28 heater connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 1 relay  terminal 3 - G28-2 (B)</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 2 relay  terminal 3 - G28-3 (B)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 3 relay  terminal 3 - G28-1 (B)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 1 relay  terminal 3 - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 2 relay  terminal 3 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 3 relay  terminal 3 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04FX_05_0025" fin="false">OK</down>
<right ref="RM000001GFJ04FX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0025" proc-id="RM23G0E___0000HCK00001">
<testtitle>CHECK HARNESS AND CONNECTOR (ENGINE ROOM NO. 2 RELAY BLOCK - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the PTC heater relays from the engine room No. 2 relay block.</ptxt>
<figure>
<graphic graphicname="E200727E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 1 relay  terminal 5 - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 2 relay  terminal 5 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 3 relay  terminal 5 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 1 relay  terminal 2 - Body ground</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 2 relay  terminal 2 - Body ground</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>PTC NO. 3 relay  terminal 2 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>PTC NO. 1 Relay</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>PTC NO. 2 Relay</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>PTC NO. 3 Relay</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without relay installed</ptxt>
<ptxt>(Engine Room No. 2 Relay Block)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001GFJ04FX_05_0016" fin="false">OK</down>
<right ref="RM000001GFJ04FX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0016" proc-id="RM23G0E___0000HCI00001">
<testtitle>INSPECT QUICK HEATER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the quick heater assembly (See page <xref label="Seep01" href="RM000003AXS02MX"/>).</ptxt>
<figure>
<graphic graphicname="E140255E08" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (B) - 1 (E)</ptxt>
</entry>
<entry morerows="3" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="3" valign="middle" align="center">
<ptxt>Below 1 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (B) - 1 (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (B) - 2 (E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3 (B) - 2 (E)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04FX_05_0022" fin="false">OK</down>
<right ref="RM000001GFJ04FX_05_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0022" proc-id="RM23G0E___0000HCJ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (QUICK HEATER - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G29 heater connector.</ptxt>
<figure>
<graphic graphicname="E179945E10" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G29-1 (E) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G29-2 (E) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Quick Heater Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001GFJ04FX_05_0026" fin="true">OK</down>
<right ref="RM000001GFJ04FX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0008">
<testtitle>REPLACE PTC HEATER RELAY (PTC NO. 1, PTC NO. 2, PTC NO. 3)</testtitle>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0021">
<testtitle>REPLACE QUICK HEATER ASSEMBLY<xref label="Seep01" href="RM000003AXS02MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001GFJ04FX_05_0026">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000045E301UX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>