<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12053_S001Z" variety="S001Z">
<name>LIGHTING (INT)</name>
<ttl id="12053_S001Z_7B9BR_T00LF" variety="T00LF">
<name>PERSONAL LIGHT (for Front Door)</name>
<para id="RM000003YNP00ZX" category="A" type-id="80001" name-id="LI75V-05" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM000003YNP00ZX_02" type-id="11" category="10" proc-id="RM23G0E___0000F8900000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for LHD and RHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
<item>
<ptxt>Use the same procedure for the RH side and LH side.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003YNP00ZX_01" type-id="01" category="01">
<s-1 id="RM000003YNP00ZX_01_0011" proc-id="RM23G0E___0000BZ800000">
<ptxt>REMOVE FRONT DOOR LOWER FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 clips and remove the front door lower frame bracket garnish LH.</ptxt>
<figure>
<graphic graphicname="B240614" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003YNP00ZX_01_0005" proc-id="RM23G0E___0000BZ500000">
<ptxt>REMOVE NO. 2 DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a moulding remover, detach the 3 claws and remove the inside handle bezel as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B238520" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003YNP00ZX_01_0012" proc-id="RM23G0E___0000BZ600000">
<ptxt>REMOVE FRONT DOOR ARMREST COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using moulding remover A, detach the 8 claws and remove the assist grip cover.</ptxt>
<figure>
<graphic graphicname="B238521" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003YNP00ZX_01_0006" proc-id="RM23G0E___0000BZ700000">
<ptxt>REMOVE FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B238522" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 12 clips.</ptxt>
</s2>
<s2>
<ptxt>Pull out the front door trim board sub-assembly in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B238523E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reference Boss</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Raise the front door trim board sub-assembly to detach the 4 claws and remove the front door trim board sub-assembly together with the front door inner glass weatherstrip LH.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 connectors.</ptxt>
<figure>
<graphic graphicname="B238524" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Seat Position Memory System:</ptxt>
<s3>
<ptxt>Disconnect the connectors.</ptxt>
<figure>
<graphic graphicname="B240206" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Disconnect the front door lock remote control cable assembly and front door inside locking cable assembly.</ptxt>
<figure>
<graphic graphicname="B238525" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003YNP00ZX_01_0013" proc-id="RM23G0E___0000BZA00000">
<ptxt>REMOVE FRONT DOOR INNER GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the front door inner glass weatherstrip from the front door trim board sub-assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B238526" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000003YNP00ZX_01_0004" proc-id="RM23G0E___0000F8700000">
<ptxt>REMOVE FRONT DOOR INSIDE HANDLE ILLUMINATION LIGHT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="E198049E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the claw and remove the light.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000003YNP00ZX_01_0007" proc-id="RM23G0E___0000F8800000">
<ptxt>REMOVE NO. 2 INTERIOR ILLUMINATION LIGHT SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="E198050" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Turn the light in the direction indicated by the arrow and remove it.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>