<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001B" variety="S001B">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001B_7B971_T00GP" variety="T00GP">
<name>KINETIC DYNAMIC SUSPENSION SYSTEM</name>
<para id="RM000002KGG00OX" category="C" type-id="3056N" name-id="SC1FD-14" from="201210">
<dtccode>C1879/79</dtccode>
<dtcname>Zero Point Calibration of Acceleration Sensor Undone</dtcname>
<subpara id="RM000002KGG00OX_01" type-id="60" category="03" proc-id="RM23G0E___00009UD00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The stabilizer control ECU receives the yaw rate and acceleration sensor zero point information from the skid control ECU via CAN communication.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1879/79</ptxt>
</entry>
<entry valign="middle">
<ptxt>A signal indicating that the yaw rate and acceleration sensor zero point calibration has not been performed is continuously received from the skid control ECU for 5 seconds.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Brake control system (Yaw rate and acceleration sensor zero point calibration)</ptxt>
</item>
<item>
<ptxt>Stabilizer control ECU </ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002KGG00OX_02" type-id="51" category="05" proc-id="RM23G0E___00009UE00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>When this DTC is output, obtain the calibration value of the yaw rate and acceleration sensor zero point first.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002KGG00OX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002KGG00OX_03_0001" proc-id="RM23G0E___00009UF00001">
<testtitle>PERFORM ZERO POINT CALIBRATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform brake control system calibration (See page <xref label="Seep01" href="RM00000452J00KX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002KGG00OX_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002KGG00OX_03_0002" proc-id="RM23G0E___00009UG00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000001N9I019X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000001N9I019X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002KGG00OX_03_0006" fin="true">A</down>
<right ref="RM000002KGG00OX_03_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000002KGG00OX_03_0006">
<testtitle>REPLACE STABILIZER CONTROL ECU<xref label="Seep01" href="RM000002KLG00IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002KGG00OX_03_0009">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>