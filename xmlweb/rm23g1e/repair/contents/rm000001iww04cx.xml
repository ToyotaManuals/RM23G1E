<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001A" variety="S001A">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001A_7B967_T00FV" variety="T00FV">
<name>FRONT AXLE HUB</name>
<para id="RM000001IWW04CX" category="G" type-id="8000T" name-id="AD1GT-02" from="201207">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000001IWW04CX_01" type-id="01" category="01">
<s-1 id="RM000001IWW04CX_01_0003">
<ptxt>REMOVE FRONT WHEEL</ptxt>
</s-1>
<s-1 id="RM000001IWW04CX_01_0023" proc-id="RM23G0E___00009BQ00000">
<ptxt>REMOVE FRONT DISC BRAKE CYLINDER ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a union nut wrench, disconnect the brake tube from the disc brake cylinder assembly.</ptxt>
<figure>
<graphic graphicname="F043120" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use a container to catch brake fluid as it drains out.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disc brake cylinder assembly.</ptxt>
<figure>
<graphic graphicname="F043121" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000001IWW04CX_01_0024" proc-id="RM23G0E___00009BR00000">
<ptxt>REMOVE FRONT DISC
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place matchmarks on the disc and axle hub if planning to reuse the disc.</ptxt>
<figure>
<graphic graphicname="C215913E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the front disc.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001IWW04CX_01_0025" proc-id="RM23G0E___00009AH00000">
<ptxt>REMOVE FRONT AXLE HUB GREASE CAP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver and hammer, remove the front axle hub grease cap.</ptxt>
<atten3>
<ptxt>Do not damage the axle hub.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000001IWW04CX_01_0001" proc-id="RM23G0E___00009CC00000">
<ptxt>INSPECT FRONT AXLE HUB BEARING LOOSENESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the looseness near the center of the axle hub.</ptxt>
<figure>
<graphic graphicname="C098703" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum looseness</title>
<specitem>
<ptxt>0.05 mm (0.00197 in.)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Make sure that the dial indicator is set at a right angle to the measurement surface.</ptxt>
</atten3>
<ptxt>If the looseness is more than the maximum, replace the axle hub.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001IWW04CX_01_0021" proc-id="RM23G0E___00009CE00000">
<ptxt>INSPECT FRONT AXLE HUB RUNOUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a dial indicator, measure the runout on the surface of the axle hub outside the hub bolts.</ptxt>
<figure>
<graphic graphicname="C098704" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum runout</title>
<specitem>
<ptxt>0.08 mm (0.00315 in.)</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Make sure that the dial indicator is set at a right angle to the measurement surface.</ptxt>
</atten3>
<ptxt>If the runout is more than the maximum, replace the axle hub.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001IWW04CX_01_0026" proc-id="RM23G0E___00009BY00000">
<ptxt>INSTALL FRONT AXLE HUB GREASE CAP
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new axle hub grease cap.</ptxt>
<atten3>
<ptxt>Make sure the grease cap is securely installed to the axle hub.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000001IWW04CX_01_0027" proc-id="RM23G0E___00009BS00000">
<ptxt>INSTALL FRONT DISC
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the matchmarks and install the front disc.</ptxt>
<atten4>
<ptxt>When replacing the disc with a new one, select the installation position where the disc has the smallest runout.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM000001IWW04CX_01_0028" proc-id="RM23G0E___00009BT00000">
<ptxt>INSTALL DISC BRAKE CYLINDER ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the disc brake cylinder assembly with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>123</t-value1>
<t-value2>1254</t-value2>
<t-value4>91</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a union nut wrench, connect the brake tube to the disc brake cylinder assembly.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000001IWW04CX_01_0013" proc-id="RM23G0E___00009CD00000">
<ptxt>INSTALL FRONT WHEEL</ptxt>
<content1 releasenbr="1">
<torque>
<torqueitem>
<t-value1>112</t-value1>
<t-value2>1142</t-value2>
<t-value4>83</t-value4>
</torqueitem>
</torque>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>