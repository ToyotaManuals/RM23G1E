<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000R" variety="S000R">
<name>1KD-FTV COOLING</name>
<ttl id="12011_S000R_7B91N_T00BB" variety="T00BB">
<name>THERMOSTAT (w/o DPF)</name>
<para id="RM00000144C02LX" category="A" type-id="80001" name-id="CO60Q-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM00000144C02LX_01" type-id="01" category="01">
<s-1 id="RM00000144C02LX_01_0065" proc-id="RM23G0E___00006N800000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000144C02LX_01_0063" proc-id="RM23G0E___000044100000">
<ptxt>REMOVE FRONT BUMPER LOWER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper lower cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0020" proc-id="RM23G0E___000043O00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and No. 1 engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0022" proc-id="RM23G0E___00001CH00000">
<ptxt>DRAIN ENGINE COOLANT
</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A218364" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Drain the coolant by removing the reservoir cap and, using a wrench, remove the vent plug.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug.</ptxt>
<figure>
<graphic graphicname="A218365E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vent Plug</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0048" proc-id="RM23G0E___00006AT00000">
<ptxt>REMOVE FRONT FENDER APRON SEAL RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, remove the 4 clips and front fender apron seal RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0049" proc-id="RM23G0E___00006AU00000">
<ptxt>REMOVE FRONT NO. 1 FENDER APRON TO FRAME SEAL RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a clip remover, remove the 5 clips and front No. 1 fender apron to frame seal RH.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0064" proc-id="RM23G0E___00006AX00000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 13 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0050" proc-id="RM23G0E___00004DO00000">
<ptxt>REMOVE FRONT HEATER BRACKET (for Cold Area Specification Vehicles)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and front heater bracket.</ptxt>
<figure>
<graphic graphicname="A223523" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0040" proc-id="RM23G0E___00004DN00000">
<ptxt>REMOVE FAN AND GENERATOR V BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using the pulley set bolt of the tensioner, rotate the tensioner pulley clockwise to loosen the belt tension. Then remove the V belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0051" proc-id="RM23G0E___00006AR00000">
<ptxt>REMOVE NO. 1 AIR CLEANER HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 2 hose clamps and remove the No. 1 air cleaner hose.</ptxt>
<figure>
<graphic graphicname="A223211" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0052" proc-id="RM23G0E___00006AV00000">
<ptxt>REMOVE AIR CLEANER CAP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A246272E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>except Cold Area Specification Vehicles:</ptxt>
<ptxt>Detach the 2 clamps and disconnect the mass air flow meter connector.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>except Cold Area Specification Vehicles</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for Cold Area Specification Vehicles</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>for Cold Area Specification Vehicles:</ptxt>
<ptxt>Detach the 3 clamps and disconnect the mass air flow meter connector.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 clamps and remove the air cleaner cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0034">
<ptxt>REMOVE AIR CLEANER FILTER ELEMENT SUB-ASSEMBLY</ptxt>
</s-1>
<s-1 id="RM00000144C02LX_01_0027" proc-id="RM23G0E___00006AS00000">
<ptxt>REMOVE AIR CLEANER CASE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and air cleaner case.</ptxt>
<figure>
<graphic graphicname="A223214" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0053" proc-id="RM23G0E___00006AY00000">
<ptxt>REMOVE NO. 1 AIR HOSE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 2 clamps.</ptxt>
<figure>
<graphic graphicname="A218373" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the No. 1 air hose from the inlet pipe and intercooler.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0060" proc-id="RM23G0E___00006AE00000">
<ptxt>REMOVE COMPRESSOR OUTLET ELBOW
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 3 wire harness clamps.</ptxt>
<figure>
<graphic graphicname="A223215" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and wire harness bracket.</ptxt>
</s2>
<s2>
<ptxt>Loosen the hose clamp and remove the 2 bolts and compressor outlet elbow.</ptxt>
<figure>
<graphic graphicname="A220719" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0054" proc-id="RM23G0E___000043H00000">
<ptxt>REMOVE VISCOUS WITH MAGNET CLUTCH HEATER ASSEMBLY (for Cold Area Specification Vehicles)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the viscous heater connector.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the water by-pass hose and water hose.</ptxt>
<figure>
<graphic graphicname="A223522" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and viscous heater with magnet clutch.</ptxt>
<figure>
<graphic graphicname="A223524" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0055" proc-id="RM23G0E___000043I00000">
<ptxt>REMOVE VISCOUS HEATER NO. 1 BRACKET SUB-ASSEMBLY (for Cold Area Specification Vehicles)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and No. 1 viscous heater bracket.</ptxt>
<figure>
<graphic graphicname="A223525" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0056" proc-id="RM23G0E___000043J00000">
<ptxt>DISCONNECT COOLER COMPRESSOR ASSEMBLY (w/ Air Conditioning System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the compressor connector.</ptxt>
<figure>
<graphic graphicname="A223526" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 bolts and disconnect the cooler compressor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0066" proc-id="RM23G0E___000047K00000">
<ptxt>REMOVE GENERATOR BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and generator bracket.</ptxt>
<figure>
<graphic graphicname="A224117" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0057" proc-id="RM23G0E___00006AW00000">
<ptxt>REMOVE GENERATOR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the generator connector.</ptxt>
<figure>
<graphic graphicname="A222880" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the generator wire.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and generator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0067" proc-id="RM23G0E___000047I00000">
<ptxt>REMOVE NO. 1 COMPRESSOR MOUNTING BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and No. 1 compressor mounting bracket.</ptxt>
<figure>
<graphic graphicname="A224118" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0058" proc-id="RM23G0E___00005K500000">
<ptxt>REMOVE VENTILATION PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the 2 ventilation hoses and ventilation pipe.</ptxt>
<figure>
<graphic graphicname="A220720" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0059" proc-id="RM23G0E___00006AF00000">
<ptxt>REMOVE ENGINE OIL LEVEL DIPSTICK GUIDE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the engine oil level dipstick.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and engine oil level dipstick guide.</ptxt>
<figure>
<graphic graphicname="A220721" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the O-ring from the engine oil level dipstick guide.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0029" proc-id="RM23G0E___00006MR00000">
<ptxt>DISCONNECT NO. 2 RADIATOR HOSE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A218378" width="2.775699831in" height="1.771723296in"/>
</figure>
</content1></s-1>
<s-1 id="RM00000144C02LX_01_0047" proc-id="RM23G0E___00006N700000">
<ptxt>DISCONNECT NO. 2 WATER BY-PASS HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the No. 2 water by-pass hose from the water inlet.</ptxt>
<figure>
<graphic graphicname="A224943" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144C02LX_01_0010" proc-id="RM23G0E___00006N500000">
<ptxt>REMOVE WATER INLET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Automatic Transmission:</ptxt>
<ptxt>Remove the 2 bolts from the oil cooler tube clamp and bracket.</ptxt>
<figure>
<graphic graphicname="A224946" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts and inlet.</ptxt>
<figure>
<graphic graphicname="A218381" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000144C02LX_01_0013" proc-id="RM23G0E___00006N600000">
<ptxt>REMOVE THERMOSTAT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the thermostat.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket from the thermostat.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>