<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12018_S0014" variety="S0014">
<name>CLUTCH</name>
<ttl id="12018_S0014_7B94R_T00EF" variety="T00EF">
<name>CLUTCH RELEASE CYLINDER (for 1KD-FTV)</name>
<para id="RM0000032RA00HX" category="A" type-id="30014" name-id="CL1F8-01" from="201207">
<name>INSTALLATION</name>
<subpara id="RM0000032RA00HX_01" type-id="01" category="01">
<s-1 id="RM0000032RA00HX_01_0001" proc-id="RM23G0E___00008FF00000">
<ptxt>INSTALL CLUTCH RELEASE CYLINDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the clutch release cylinder with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>120</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032RA00HX_01_0002" proc-id="RM23G0E___00008FG00000">
<ptxt>CONNECT CLUTCH RELEASE CYLINDER TO FLEXIBLE HOSE TUBE</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Install the tube with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a union nut wrench, connect the tube to the clutch release cylinder.</ptxt>
<torque>
<torqueitem>
<t-value1>15</t-value1>
<t-value2>155</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032RA00HX_01_0012" proc-id="RM23G0E___00008FH00000">
<ptxt>INSTALL FRONT PROPELLER SHAFT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front propeller shaft (See page <xref label="Seep01" href="RM00000291O00KX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000032RA00HX_01_0009" proc-id="RM23G0E___00008CZ00000">
<ptxt>FILL RESERVOIR WITH BRAKE FLUID
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215410E01" width="2.775699831in" height="5.787629434in"/>
</figure>
<s2>
<ptxt>Fill the reservoir with brake fluid.</ptxt>
<spec>
<title>Brake fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD (2TR-FE, 5L-E)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD (1GR-FE, 1KD-FTV)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000032RA00HX_01_0004" proc-id="RM23G0E___00008CY00000">
<ptxt>BLEED CLUTCH LINE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bleeder plug cap of the release cylinder.</ptxt>
</s2>
<s2>
<ptxt>Connect a vinyl tube to the bleeder plug.</ptxt>
</s2>
<s2>
<ptxt>Depress the clutch pedal several times, and then loosen the bleeder plug while the pedal is depressed.</ptxt>
</s2>
<s2>
<ptxt>When fluid no longer comes out, tighten the bleeder plug, and then release the clutch pedal.</ptxt>
</s2>
<s2>
<ptxt>Repeat the previous 2 steps until all the air in the fluid is completely bled.</ptxt>
</s2>
<s2>
<ptxt>Tighten the bleeder plug. </ptxt>
<torque>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>110</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the bleeder plug cap.</ptxt>
</s2>
<s2>
<ptxt>Check that all the air has been bled from the clutch line.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000032RA00HX_01_0007" proc-id="RM23G0E___00008D000000">
<ptxt>CHECK FLUID LEVEL IN RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fluid level.</ptxt>
<ptxt>If the brake fluid level is low, check for leaks and inspect the disc brake pad. If necessary, refill the reservoir with brake fluid after repair or replacement.</ptxt>
<spec>
<title>Brake fluid</title>
<specitem>
<ptxt>SAE J1703 or FMVSS No. 116 DOT 3</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM0000032RA00HX_01_0008">
<ptxt>INSPECT FOR BRAKE FLUID LEAK FROM CLUTCH LINE</ptxt>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>