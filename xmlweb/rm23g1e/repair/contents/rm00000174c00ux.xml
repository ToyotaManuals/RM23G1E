<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12018_S0014" variety="S0014">
<name>CLUTCH</name>
<ttl id="12018_S0014_7B94N_T00EB" variety="T00EB">
<name>CLUTCH PEDAL (for RHD)</name>
<para id="RM00000174C00UX" category="A" type-id="30014" name-id="CL1S2-02" from="201207">
<name>INSTALLATION</name>
<subpara id="RM00000174C00UX_01" type-id="01" category="01">
<s-1 id="RM00000174C00UX_01_0016" proc-id="RM23G0E___00008DY00000">
<ptxt>INSTALL CLUTCH MASTER CYLINDER PUSH ROD CLEVIS BUSH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215453E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Apply MP grease to the inside of a new clevis bush.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>MP grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the clevis bush to the clutch pedal.</ptxt>
<atten4>
<ptxt>Install the clevis bush from the left side of the vehicle.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0017" proc-id="RM23G0E___00008DZ00000">
<ptxt>INSTALL CLUTCH PEDAL TURNOVER BUSH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215449E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Apply MP grease to the clutch pedal turnover bush.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>MP grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the turnover bush to the clutch pedal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0001" proc-id="RM23G0E___00008DR00000">
<ptxt>INSTALL NO. 1 CLUTCH PEDAL CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 3 cushions to the clutch pedal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0002" proc-id="RM23G0E___00008DS00000">
<ptxt>INSTALL CLUTCH PEDAL SHAFT COLLAR</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C217708E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Apply MP grease to the shaft collar.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>MP grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the shaft collar to the clutch pedal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0003" proc-id="RM23G0E___00008DT00000">
<ptxt>INSTALL CLUTCH PEDAL BUSH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C217707E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Apply MP grease to 2 new bushes.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>MP grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the 2 bushes to the clutch pedal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0004">
<ptxt>INSTALL CLUTCH PEDAL PAD</ptxt>
</s-1>
<s-1 id="RM00000174C00UX_01_0019" proc-id="RM23G0E___00008E100000">
<ptxt>INSTALL CLUTCH PEDAL STOPPER BOLT (w/o Cruise Control System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the stopper bolt and temporarily install the lock nut.</ptxt>
<atten4>
<ptxt>Tighten the lock nut to the specified torque when adjusting the clutch pedal.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0018" proc-id="RM23G0E___00008E000000">
<ptxt>INSTALL CLUTCH SWITCH ASSEMBLY (w/ Cruise Control System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the clutch switch and temporarily install the lock nut.</ptxt>
<atten4>
<ptxt>Tighten the lock nut to the specified torque when adjusting the clutch pedal.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0005" proc-id="RM23G0E___00008DU00000">
<ptxt>INSTALL CLUTCH PEDAL SPRING HOLDER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215445E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Apply MP grease to the contact surface of the spring holder.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>MP grease</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the spring holder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0006" proc-id="RM23G0E___00008DV00000">
<ptxt>INSTALL CLUTCH PEDAL SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the clutch pedal and washer to the clutch pedal support with the nut and bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>34</t-value1>
<t-value2>350</t-value2>
<t-value4>25</t-value4>
</torqueitem>
</torque>
<atten4>
<ptxt>Install the bolt from the left side of the vehicle.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0007" proc-id="RM23G0E___00008DW00000">
<ptxt>INSTALL TURN OVER SPRING SEAT COMPRESSION SPRING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the compression spring to the clutch pedal and spring holder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0021" proc-id="RM23G0E___00008E200000">
<ptxt>INSTALL CLUTCH START SWITCH ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<s3>
<ptxt>Install the clutch start switch assembly with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>160</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Connect the clutch start switch connector.</ptxt>
</s3>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<s3>
<ptxt>Install the clutch start switch assembly with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>160</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Connect the clutch start switch connector and attach the clamp.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM00000174C00UX_01_0022" proc-id="RM23G0E___00008E300000">
<ptxt>INSTALL CLUTCH PEDAL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the clutch pedal with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0008" proc-id="RM23G0E___00008DX00000">
<ptxt>INSTALL CLUTCH MASTER CYLINDER ASSEMBLY</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Install the clutch master cylinder (See page <xref label="Seep01" href="RM0000032QZ006X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000174C00UX_01_0023" proc-id="RM23G0E___00008E400000">
<ptxt>INSTALL LOWER NO. 1 INSTRUMENT PANEL AIRBAG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the lower No. 1 instrument panel airbag (See page <xref label="Seep01" href="RM0000046J600CX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>