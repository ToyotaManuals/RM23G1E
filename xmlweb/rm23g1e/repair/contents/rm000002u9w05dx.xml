<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B7_T00KV" variety="T00KV">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000002U9W05DX" category="D" type-id="303FF" name-id="TD346-03" from="201207">
<name>OPERATION CHECK</name>
<subpara id="RM000002U9W05DX_z0" proc-id="RM23G0E___0000ERJ00000">
<content5 releasenbr="1">
<step1>
<ptxt>ENTRY AND START SYSTEM OPERATION INSPECTION</ptxt>
<step2>
<ptxt>Check the entry unlock function.</ptxt>
<step3>
<ptxt>Use the wireless lock operation to lock the doors. With the key in your possession, touch a door outside handle (unlock sensor) and check that the door unlocks.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check the entry unlock operation detection area.</ptxt>
<step3>
<ptxt>Step 1: Hold the key at the same height as the door outside handle (approximately 0.8 m [2.62 ft.]). Pay attention to the direction of the key in the illustration.</ptxt>
<figure>
<graphic graphicname="B235104E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.7 to 1.0 m (2.30 to 3.28 ft.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Step 2: Check that when the key is brought within 0.7 to 1.0 m (2.30 to 3.28 ft.) of the vehicle, the system enters unlock standby mode.</ptxt>
<atten4>
<ptxt>Unlock standby mode is signified by the red LED of the key illuminating.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Step 3: After the system enters unlock standby mode, touch the unlock sensor on the outside handle within 3 seconds. Check that the door unlocks.</ptxt>
<figure>
<graphic graphicname="B239769E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unlock Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The key may not be able to communicate with the system within a 0.2 m (0.656 ft.) radius of each outside handle.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>Step 4: Repeat step 2 and step 3 for the other front door.</ptxt>
</step3>
<step3>
<ptxt>Step 5: Inspect the unlock sensor response sensitivity. Wear protective gloves, set the system to unlock standby mode, and check that touching the inner side of the outside handle (the highlighted area in the illustration) with your finger causes the door to unlock.</ptxt>
<figure>
<graphic graphicname="B239769E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unlock Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>When touching the highlighted area, tapping too quickly or having extended contact may not trigger the sensor. In such a case, the door does not unlock.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>Step 6: Repeat step 5 for the other front door.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check the entry lock function.</ptxt>
<step3>
<ptxt>Step 1: Close all the doors of the vehicle. With the key outside the vehicle, check that touching the lock sensor locks the doors.</ptxt>
<figure>
<graphic graphicname="B239770E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock Sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When touching the lock sensor, hold the electrical key transmitter approximately 1 m (3.28 ft.) above the ground and approximately 0.3 m (0.984 ft.) away from the vehicle as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Due to the key being unable to communicate with the system within a 0.2 m (0.656 ft.) radius from the outside handle, do not touch the lock sensor with the same hand that is holding the key. The doors may not lock.</ptxt>
</item>
<item>
<ptxt>If the key lock-in prevention function buzzer sounds, radio waves from the indoor electrical key antenna may be leaking from the vehicle.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="B235105E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Approximately 0.3 m (0.984 ft.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Step 2: Repeat step 1 for the other front door.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check the entry back door unlock function.</ptxt>
<step3>
<ptxt>Lock all the doors and the back door. With the key in your possession, check that pressing the back door unlock switch unlocks all the doors and the back door.</ptxt>
</step3>
<step3>
<ptxt>Inspect the detection area of the entry back door unlock operation. Hold the key at the same height as the back door unlock switch (approximately 0.8 m [2.62 ft.]) Pay attention to the direction of the key shown in the illustration. Check that the when the key is brought within 0.7 to 1.0 m (2.30 to 3.28 ft.) of the vehicle, pressing the back door unlock switch unlocks all the doors and the back door.</ptxt>
<figure>
<graphic graphicname="B235106E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Glass Hatch Opener System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Glass Hatch Opener System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.7 to 1.0 m (2.30 to 3.28 ft.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Check the entry back door lock function.</ptxt>
<step3>
<ptxt>Step 1: Close all of the vehicle doors. With the key in your possession outside of the vehicle, check that pressing the back door lock switch locks all the doors.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When pressing the lock switch, hold the electrical key transmitter approximately 1 m (3.28 ft.) above the ground and approximately 0.3 m (0.984 ft.) away from the vehicle as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>Due to the key being unable to communicate with the system within a 0.2 m (0.656 ft.) radius from the rear of the vehicle, do not press the back door lock switch with the same hand that is holding the key. The doors may not lock.</ptxt>
</item>
<item>
<ptxt>If the key lock-in prevention function buzzer sounds, radio waves from the indoor electrical key antenna may be leaking from the vehicle.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="B235107E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Glass Hatch Opener System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Glass Hatch Opener System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Approximately 0.3 m (0.984 ft.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Check the entry glass hatch open function (w/ Glass Hatch Opener System).</ptxt>
<step3>
<ptxt>Lock all the doors including the back door. With the key in your possession, check that pressing the glass hatch opener switch opens the glass hatch.</ptxt>
</step3>
<step3>
<ptxt>Inspect the detection area of the entry glass hatch open operation. Hold the key at the same height as the back door unlock switch (approximately 0.8 m [2.62 ft.]) Pay attention to the direction of the key shown in the illustration. Check that when the key is brought within 0.7 to 1.0 m (2.30 to 3.28 ft.) of the vehicle, pressing the glass hatch opener switch opens the glass hatch.</ptxt>
<figure>
<graphic graphicname="B243193E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>0.7 to 1.0 m (2.30 to 3.28 ft.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Check the entry ignition function.</ptxt>
<step3>
<ptxt>When the engine control system is off: With the key in your possession and the brake pedal (for Automatic Transmission) or clutch pedal (for Manual Transmission) depressed, check that pressing the engine switch releases the steering wheel lock and starts the engine control system.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The engine control system can be started only when the engine switch indicator illumination is green.</ptxt>
</item>
<item>
<ptxt>Even when not depressing the brake pedal (for Automatic Transmission) or clutch pedal (for Manual Transmission), continually pressing the engine switch for 15 seconds when the engine switch is on (ACC) can start the engine control system.</ptxt>
</item>
</list1>
</atten4>
</step3>
<step3>
<ptxt>When the engine control system is running: With the key in your possession, check that pressing the engine switch stops the engine control system and activates the steering wheel lock.</ptxt>
<atten4>
<ptxt>The shift lever must be in P to be able to stop the engine control system (for Automatic Transmission).</ptxt>
</atten4>
</step3>
<step3>
<ptxt>Inspect the detection area of the entry ignition operation (front seat). Pay attention to the direction of the key in the illustration. When the key is in either of the 2 locations in the illustration, check that the engine control system can be started. Then repeat the check with the key in the other location.</ptxt>
<figure>
<graphic graphicname="B235108E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Inspection Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The engine control system cannot be started when the key is on the instrument panel or in the glove box.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>for 5 Door:</ptxt>
<ptxt>Inspect the detection area of the entry ignition operation (rear seat). Pay attention to the direction of the key in the illustration. When the key is in either of the 2 locations in the illustration, check that the engine control system can be started. Then repeat the check with the key in the other location.</ptxt>
<figure>
<graphic graphicname="B235109E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration (for 5 Door)</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>w/ Rear No. 1 Seat Assembly</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>w/o Rear No. 1 Seat Assembly</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Inspection Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The engine control system cannot be started when the key is on the instrument panel or in the glove box.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>for 3 Door:</ptxt>
<ptxt>Inspect the detection area of the entry ignition operation (rear seat). Pay attention to the direction of the key in the illustration. When the key is in either of the 2 locations in the illustration, check that the engine control system can be started. Then repeat the check with the key in the other location.</ptxt>
<figure>
<graphic graphicname="B235110E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration (for 3 Door)</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>w/ Rear No. 1 Seat Assembly</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>w/o Rear No. 1 Seat Assembly</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Inspection Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The engine control system cannot be started when the key is on the instrument panel or in the glove box.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>for 5 Door:</ptxt>
<ptxt>Inspect the detection area of the entry ignition operation (luggage room). Pay attention to the direction of the key in the illustration. When the key is in either of the 2 locations in the illustration, check that the engine control system can be started. Then repeat the check with the key in the other location.</ptxt>
<figure>
<graphic graphicname="B235111E01" width="2.775699831in" height="4.7836529in"/>
</figure>
<table>
<title>Text in Illustration (for 5 Door)</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>w/ Rear No. 2 Seat Assembly</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>w/o Rear No. 2 Seat Assembly</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Inspection Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The engine control system cannot be started when the key is on the instrument panel or in the glove box.</ptxt>
</atten3>
</step3>
<step3>
<ptxt>for 3 Door:</ptxt>
<ptxt>Inspect the detection area of the entry ignition operation (luggage room). Pay attention to the direction of the key in the illustration. When the key is in either of the 2 locations in the illustration, check that the engine control system can be started. Then repeat the check with the key in the other location.</ptxt>
<figure>
<graphic graphicname="B235112E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration (for 3 Door)</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Inspection Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The engine control system cannot be started when the key is on the instrument panel or in the glove box.</ptxt>
</atten3>
</step3>
</step2>
<step2>
<ptxt>Check the key lock-in prevention function.</ptxt>
<atten3>
<ptxt>To prevent key lock-in, perform the inspection with one of the door windows open.</ptxt>
</atten3>
<step3>
<ptxt>Place the key in the vehicle, close the doors, perform the entry lock operation (touch the lock sensor on the door outside handle) and check that the buzzer sounds for a specified period of time and all the doors are unlocked.</ptxt>
</step3>
<step3>
<ptxt>Open the back door, place the key in the vehicle with the other doors closed and locked, close the back door and check that the buzzer sounds for a specified period of time and the back door does not lock when the lock switch is pressed.</ptxt>
</step3>
<step3>
<ptxt>Open the glass hatch, place the key in the vehicle with the other doors closed and locked, close the glass hatch and check that the buzzer sounds for a specified period of time and the glass hatch open when the glass hatch opener switch is pressed.*</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Glass Hatch Opener System</ptxt>
</item>
</list1>
<atten3>
<ptxt>If the key is in inside a metal storage item, such as a metal briefcase or metal box, the key cannot be detected by the system.</ptxt>
</atten3>
</step3>
</step2>
<step2>
<ptxt>Check the key cancel function.</ptxt>
<step3>
<ptxt>While the engine switch is on (IG), check that the back door unlock switch is the only switch in the entry and start system that can be operated.</ptxt>
</step3>
<step3>
<ptxt>While the key cancel function (entry and start system cancel function) is on, check that all functions of the entry and start system cannot be operated.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check the answer-back function (hazard warning light flashing).</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Entry Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Hazard Warning Light</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Entry Door Lock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Flashes once</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Entry Door Unlock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Flashes twice</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Entry Back Door Lock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Flashes once</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Entry Back Door Unlock</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Flashes twice</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Check that the key reminder warning buzzer sounds.</ptxt>
<step3>
<ptxt>With the key inside the vehicle, close the driver side door. Then turn the engine switch off or on (ACC).</ptxt>
</step3>
<step3>
<ptxt>Open the driver side door and check that the buzzer sounds intermittently.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check that the key reminder warning buzzer stops.</ptxt>
<step3>
<ptxt>When the buzzer is sounding, check that the buzzer stops sounding if either of the following is performed:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Closing the driver side door (front door courtesy light switch is off).</ptxt>
</item>
<item>
<ptxt>Turning the engine switch on (IG).</ptxt>
</item>
</list1>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>