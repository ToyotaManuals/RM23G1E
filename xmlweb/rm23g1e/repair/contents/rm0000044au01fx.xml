<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0011" variety="S0011">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0011_7B93H_T00D5" variety="T00D5">
<name>CRUISE CONTROL SYSTEM</name>
<para id="RM0000044AU01FX" category="C" type-id="300YB" name-id="CC3FT-03" from="201210">
<dtccode>P0500</dtccode>
<dtcname>Vehicle Speed Sensor Malfunction</dtcname>
<subpara id="RM0000044AU01FX_01" type-id="60" category="03" proc-id="RM23G0E___000076J00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The speed sensor detects the wheel speed and sends the appropriate signals to the skid control ECU. The skid control ECU converts these wheel speed signals into a pulse signal and outputs it to the ECM via the combination meter assembly. The ECM determines the vehicle speed based on the frequency of this pulse signal.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0500</ptxt>
</entry>
<entry valign="middle">
<ptxt>The vehicle speed signal from the skid control ECU is cut for 0.14 seconds or more while cruise control is in operation.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Vehicle speed sensor signal circuit</ptxt>
</item>
<item>
<ptxt>Vehicle speed sensor</ptxt>
</item>
<item>
<ptxt>Combination meter assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000044AU01FX_02" type-id="51" category="05" proc-id="RM23G0E___000076K00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<ptxt>This DTC may be stored under the SFI system*1 or ECD system*2 at the same time. In that case, perform troubleshooting for the SFI system*1 or ECD system*2 first.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 1GR-FE</ptxt>
</item>
<item>
<ptxt>*2: for 1KD-FTV</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM0000044AU01FX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000044AU01FX_03_0001" proc-id="RM23G0E___000076L00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000044AZ00PX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the following to make sure the DTC detection conditions are met.</ptxt>
<atten4>
<ptxt>If the detection conditions are not met, the system cannot detect the malfunction.</ptxt>
</atten4>
<test2>
<ptxt>Drive the vehicle for more than 10 seconds when the engine coolant temperature is more than 20°C (68°F).</ptxt>
</test2>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000044AZ00PX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC P0500 is not output.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1KD-FTV [w/o DPF])</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1KD-FTV [w/ DPF])</ptxt>
</entry>
<entry valign="middle">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044AU01FX_03_0002" fin="true">A</down>
<right ref="RM0000044AU01FX_03_0003" fin="true">B</right>
<right ref="RM0000044AU01FX_03_0004" fin="true">C</right>
<right ref="RM0000044AU01FX_03_0005" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM0000044AU01FX_03_0002">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044AU01FX_03_0003">
<testtitle>GO TO SFI SYSTEM<xref label="Seep01" href="RM000000PD90UNX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044AU01FX_03_0004">
<testtitle>GO TO ECD SYSTEM<xref label="Seep01" href="RM0000012WB076X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044AU01FX_03_0005">
<testtitle>GO TO ECD SYSTEM<xref label="Seep01" href="RM0000012WB074X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>