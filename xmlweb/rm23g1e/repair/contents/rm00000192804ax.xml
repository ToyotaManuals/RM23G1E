<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S0029" variety="S0029">
<name>WINDOW / GLASS</name>
<ttl id="12064_S0029_7B9FZ_T00PN" variety="T00PN">
<name>QUARTER WINDOW GLASS (for 3 Door)</name>
<para id="RM00000192804AX" category="A" type-id="30014" name-id="WS58G-02" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM00000192804AX_02" type-id="11" category="10" proc-id="RM23G0E___0000IGT00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the LH and RH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000192804AX_01" type-id="01" category="01">
<s-1 id="RM00000192804AX_01_0045" proc-id="RM23G0E___0000IGQ00000">
<ptxt>INSTALL QUARTER WINDOW ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239032E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<s2>
<ptxt>Using a brush or sponge, apply primer M to the exposed part of the vehicle body.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Allow the primer coating to dry for 3 minutes or more.</ptxt>
</item>
<item>
<ptxt>Throw away any leftover primer.</ptxt>
</item>
<item>
<ptxt>Do not apply too much primer.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Primer M</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Adhesive</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a brush or sponge, apply Primer G to the contact surface of the glass.</ptxt>
<figure>
<graphic graphicname="B239035E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle"/>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Backside</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive Center Line</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>a</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.0 mm (0.276 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>b</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.5 mm (0.217 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c</ptxt>
</entry>
<entry valign="middle">
<ptxt>6.5 mm (0.256 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d</ptxt>
</entry>
<entry valign="middle">
<ptxt>8.0 mm (0.315 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>If primer is applied to an area that is not specified, wipe off the primer with non-residue solvent before it dries.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Allow the primer to dry for 3 minutes or more.</ptxt>
</item>
<item>
<ptxt>Throw away any leftover primer.</ptxt>
</item>
<item>
<ptxt>Do not apply too much primer.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Apply adhesive to the quarter window glass.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Windshield Glass Adhesive or equivalent</ptxt>
</specitem>
</spec>
<s3>
<ptxt>Cut off a tip of the cartridge nozzle as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239036E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle"/>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Backside</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive Center Line</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Area</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>a</ptxt>
</entry>
<entry valign="middle">
<ptxt>12.0 mm (0.472 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>b</ptxt>
</entry>
<entry valign="middle">
<ptxt>8.0 mm (0.315 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>c</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.5 mm (0.217 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>d</ptxt>
</entry>
<entry valign="middle">
<ptxt>6.5 mm (0.256 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>e</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.0 mm (0.276 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>After cutting off the tip, use all adhesive within the time written in the table below.</ptxt>
</atten4>
<spec>
<title>Usage Time Frame</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>Usage Time Frame</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>35°C (95°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>15 minutes</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 hour 40 minutes</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>5°C (41°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>8 hours</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s3>
<s3>
<ptxt>Load a sealer gun with the cartridge.</ptxt>
</s3>
<s3>
<ptxt>Apply adhesive to the quarter window glass as shown in the illustration.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install the quarter window glass to the vehicle body.</ptxt>
<s3>
<figure>
<graphic graphicname="B242857" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the 4 clips to install the quarter window assembly LH.</ptxt>
<atten3>
<ptxt>Allow the primer to dry for 3 minutes or more.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Hold the quarter window glass in place securely with protective tape or equivalent until the adhesive hardens.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Allow the primer coating to dry for 3 minutes or more.</ptxt>
</item>
<item>
<ptxt>Check the clearance between the body and glass.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Lightly press the front surface of the glass to ensure a close fit.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="BO03986E07" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Using a scraper, remove any excess or protruding adhesive.</ptxt>
<atten4>
<ptxt>Apply adhesive to any areas where the amount of adhesive is inadequate.</ptxt>
</atten4>
<atten3>
<ptxt>Do not drive the vehicle within the time written in the table below.</ptxt>
</atten3>
<spec>
<title>Minimum Time</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>Minimum Time Prior to Driving Vehicle</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>35°C (95°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 hour 30 minutes</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 hours</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>5°C (41°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>24 hours</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Adhesive</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000192804AX_01_0002" proc-id="RM23G0E___0000IGP00000">
<ptxt>CHECK FOR LEAK AND REPAIR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Conduct a leak test after the adhesive has completely hardened.</ptxt>
</s2>
<s2>
<ptxt>Seal any leaks with auto glass sealer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000192804AX_01_0106" proc-id="RM23G0E___0000IGS00000">
<ptxt>INSTALL NO. 1 ROOF SIDE RAIL GARNISH LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B242806E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install a new clip A to the roof side rail garnish.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>New Clip</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Attach the 5 clips to install the roof side rail garnish.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000192804AX_01_0079" proc-id="RM23G0E___0000IGR00000">
<ptxt>INSTALL ROOF HEADLINING ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the roof headlining assembly (See page <xref label="Seep01" href="RM0000046JO00GX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>