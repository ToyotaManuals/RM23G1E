<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000001DN8085X" category="J" type-id="30076" name-id="ESZID-02" from="201207" to="201210">
<dtccode/>
<dtcname>ECM Power Source Circuit</dtcname>
<subpara id="RM000001DN8085X_01" type-id="60" category="03" proc-id="RM23G0E___00000IE00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the ignition switch is turned to ON, battery voltage is applied to the IGSW terminal of the ECM. The output signal from the MREL terminal of the ECM causes a current to flow to the coil, closing the contacts of the No. 1 integration relay (EFI) and supplying power to either terminal +B or +B2 of the ECM.</ptxt>
</content5>
</subpara>
<subpara id="RM000001DN8085X_02" type-id="32" category="03" proc-id="RM23G0E___00000IF00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A266627E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001DN8085X_03" type-id="51" category="05" proc-id="RM23G0E___00000IG00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001DN8085X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001DN8085X_05_0036" proc-id="RM23G0E___00000IM00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 INTEGRATION RELAY - BATTERY)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A223349E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1C-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Room Relay Block</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 1 Integration Relay)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0021" fin="false">OK</down>
<right ref="RM000001DN8085X_05_0029" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0021" proc-id="RM23G0E___00000IH00000">
<testtitle>INSPECT NO. 1 INTEGRATION RELAY (EFI AND IG2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the EFI relay (See page <xref label="Seep01" href="RM000003BLB02MX_01_0014"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the IG2 relay (See page <xref label="Seep02" href="RM000003BLB02MX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0037" fin="false">OK</down>
<right ref="RM000001DN8085X_05_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0037" proc-id="RM23G0E___00000IN00000">
<testtitle>CHECK HARNESS AND CONNECTOR (+B, +B2 AND MREL CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the harness and connectors between the No. 1 integration relay and ECM.</ptxt>
<test2>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test2>
<test2>
<ptxt>Disconnect the ECM connector.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G55-9 (MREL) - 1B-2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G55-22 (+B) - 1B-4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G55-23 (+B2) - 1B-4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G55-9 (MREL) or 1B-2 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G55-22 (+B) or 1B-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G55-23 (+B2) or 1B-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
<test2>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test2>
<test2>
<ptxt>Reconnect the ECM connector.</ptxt>
</test2>
</test1>
<test1>
<ptxt>Check the harness and connectors between the No. 1 integration relay and body ground.</ptxt>
<test2>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test2>
<test2>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1B-3 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test2>
<test2>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0022" fin="false">OK</down>
<right ref="RM000001DN8085X_05_0045" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0022" proc-id="RM23G0E___00000II00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ECM - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C36-12 (E1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0023" fin="false">OK</down>
<right ref="RM000001DN8085X_05_0046" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0023" proc-id="RM23G0E___00000IJ00000">
<testtitle>INSPECT ECM (IGSW VOLTAGE)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A177861E37" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G56-21 (IGSW) - C36-12 (E1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0038" fin="true">OK</down>
<right ref="RM000001DN8085X_05_0024" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0024" proc-id="RM23G0E___00000IK00000">
<testtitle>CHECK HARNESS AND CONNECTOR (NO. 1 INTEGRATION RELAY - ECM, NO. 1 INTEGRATION RELAY - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1A-5 - G56-21 (IGSW)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1A-5 or G56-21 (IGSW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0042" fin="false">OK</down>
<right ref="RM000001DN8085X_05_0047" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0042" proc-id="RM23G0E___00000IP00000">
<testtitle>CHECK IF VEHICLE IS EQUIPPED WITH ENTRY AND START SYSTEM</testtitle>
<content6 releasenbr="1">
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="3.54in"/>
<colspec colname="COL2" align="center" colwidth="3.54in"/>
<thead>
<row>
<entry>
<ptxt>Result</ptxt>
</entry>
<entry>
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>w/o Entry and Start System</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001DN8085X_05_0025" fin="false">A</down>
<right ref="RM000001DN8085X_05_0043" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0025" proc-id="RM23G0E___00000IL00000">
<testtitle>CHECK HARNESS AND CONNECTOR (POWER MANAGEMENT CONTROL ECU - NO. 1 INTEGRATION RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G50-8 (IG2D) - 1A-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G50-8 (IG2D) or 1A-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the power management control ECU connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0040" fin="false">OK</down>
<right ref="RM000001DN8085X_05_0048" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0040" proc-id="RM23G0E___00000IO00000">
<testtitle>CHECK ENTRY AND START SYSTEM (POWER SOURCE MODE DOES NOT CHANGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the entry and start system (See page <xref label="Seep01" href="RM000003U3G019X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0051" fin="true">OK</down>
<right ref="RM000001DN8085X_05_0041" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0043" proc-id="RM23G0E___00000IQ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (IGNITION SWITCH - NO. 1 INTEGRATION RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ignition switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G2-6 (IG2) - 1A-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G2-6 (IG2) or 1A-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ignition switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0044" fin="false">OK</down>
<right ref="RM000001DN8085X_05_0049" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0044" proc-id="RM23G0E___00000IR00000">
<testtitle>INSPECT IGNITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the ignition switch assembly (See page <xref label="Seep01" href="RM00000474M004X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000001DN8085X_05_0052" fin="true">OK</down>
<right ref="RM000001DN8085X_05_0050" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001DN8085X_05_0029">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0030">
<testtitle>REPLACE NO. 1 INTEGRATION RELAY</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0045">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0046">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0047">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0048">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0041">
<testtitle>REPAIR ENTRY AND START SYSTEM<xref label="Seep01" href="RM000003U3G019X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0049">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0050">
<testtitle>REPLACE IGNITION SWITCH ASSEMBLY<xref label="Seep01" href="RM000000YK102EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0038">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0051">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001DN8085X_05_0052">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (IGNITION SWITCH - BATTERY)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>