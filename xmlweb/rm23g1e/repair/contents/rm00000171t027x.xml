<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001H" variety="S001H">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001H_7B984_T00HS" variety="T00HS">
<name>HYDRAULIC BRAKE BOOSTER (for RHD)</name>
<para id="RM00000171T027X" category="A" type-id="80001" name-id="BR509-03" from="201210">
<name>REMOVAL</name>
<subpara id="RM00000171T027X_01" type-id="11" category="10" proc-id="RM23G0E___0000AWP00001">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When installing, coat the parts indicated by arrows with lithium soap base glycol grease (See page <xref label="Seep01" href="RM00000171S01MX"/>).</ptxt>
</item>
<item>
<ptxt>Be sure not to allow the pump to contact the floor; support it with the booster body. If the pump contacts the floor, the red brake tube between the pump and booster body may be deformed, the operating sound of the pump may become loud enough to hear within the cabin, and fluid may leak.</ptxt>
</item>
<item>
<ptxt>As high pressure is applied to the brake actuator tube No. 1, never deform it.</ptxt>
</item>
<item>
<ptxt>Do not turn the ignition switch to ON until the procedures are completed.</ptxt>
</item>
<item>
<ptxt>Before starting the work, make sure that the ignition switch is off and depress the brake pedal more than 20 times.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When pressure in the power supply system is released, the reaction force decreases and the stroke becomes longer.</ptxt>
</atten4>
</content3>
</subpara>
<subpara id="RM00000171T027X_02" type-id="01" category="01">
<s-1 id="RM00000171T027X_02_0001" proc-id="RM23G0E___0000AWQ00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000171T027X_02_0031" proc-id="RM23G0E___0000AWS00001">
<ptxt>DRAIN BRAKE FLUID</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Wash off brake fluid immediately if it comes into contact with a painted surface.</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000171T027X_02_0047" proc-id="RM23G0E___0000AWW00001">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL AIRBAG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the lower No. 1 instrument panel airbag assembly (See page <xref label="Seep01" href="RM000003YPV029X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171T027X_02_0042" proc-id="RM23G0E___0000AU000001">
<ptxt>REMOVE PUSH ROD PIN
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip and push rod pin from the brake pedal lever.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000171T027X_02_0044" proc-id="RM23G0E___0000AWV00001">
<ptxt>REMOVE CLUTCH MASTER CYLINDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clutch master cylinder assembly (See page <xref label="Seep01" href="RM0000032R1006X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171T027X_02_0043" proc-id="RM23G0E___0000AWU00001">
<ptxt>DISCONNECT WATER HOSE JOINT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear Heater:</ptxt>
<ptxt>Disconnect the water hose joint.</ptxt>
<figure>
<graphic graphicname="A223567" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171T027X_02_0030" proc-id="RM23G0E___0000AWR00001">
<ptxt>REMOVE HYDRAULIC BRAKE BOOSTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 3 connectors from the hydraulic brake booster assembly.</ptxt>
<figure>
<graphic graphicname="C213446" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Use tags or make a memo to identify the place to reconnect.</ptxt>
<figure>
<graphic graphicname="F051200E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: To front wheel cylinder RH</ptxt>
</item>
<item>
<ptxt>*2: To front wheel cylinder LH</ptxt>
</item>
<item>
<ptxt>*3: To rear wheel cylinder RH</ptxt>
</item>
<item>
<ptxt>*4: To rear wheel cylinder LH</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Remove the brake tube from the 2 brake tube clamps.</ptxt>
<figure>
<graphic graphicname="C215284" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 2 brake tube clamps from the body.</ptxt>
</s2>
<s2>
<ptxt>Using a union nut wrench, disconnect the 4 brake lines from the hydraulic brake booster assembly.</ptxt>
<figure>
<graphic graphicname="C213447E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the clamp from the hydraulic brake booster assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the 4 nuts and pull out the hydraulic brake booster assembly.</ptxt>
<figure>
<graphic graphicname="C215261" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000171T027X_02_0032" proc-id="RM23G0E___0000AWT00001">
<ptxt>REMOVE BRAKE BOOSTER GASKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the brake booster gasket from the hydraulic brake booster assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>