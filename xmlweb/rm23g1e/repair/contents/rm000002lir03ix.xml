<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E8_T00NW" variety="T00NW">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LIR03IX" category="D" type-id="303F2" name-id="AC8TK-05" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000002LIR03IX_z0" proc-id="RM23G0E___0000HD700000">
<content5 releasenbr="1">
<step1>
<ptxt>GENERAL</ptxt>
<step2>
<ptxt>The air conditioning system has the following controls.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.86in"/>
<colspec colname="COLSPEC0" colwidth="1.86in"/>
<colspec colname="COL2" colwidth="3.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Neural Network Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>This control is capable of performing complex control by artificially simulating the information processing method of the nervous system of living organisms in order to establish a complex input/output relationship similar to that of a human brain.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Outlet Air Temperature Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>Based on the temperature set by the temperature control dial, neural network control calculates outlet air temperature based on input signals from various sensors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pollen Removal Mode Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<list1 type="unordered">
<item>
<ptxt>	When the pollen removal mode switch is pressed, pollen removal mode control is activated and the air vent is switched to the FACE mode.</ptxt>
</item>
<item>
<ptxt>	Sends air, which is filtered by the clean air filter to remove pollen, to the area around the upper parts of the bodies of the driver and front passenger.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Left and Right Independent Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>The temperature settings for the driver and front passenger are controlled independently in order to provide separate vehicle interior temperatures for the right and left sides of the vehicle.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Blower Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>Controls the blower motor in accordance with the airflow volume that has been calculated by neural network control based on the input signals from various sensors.</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Air Outlet Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>Automatically switches the air outlets in accordance with the outlet mode that has been calculated by neural network control.</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>In accordance with the engine coolant temperature, ambient air temperature, amount of sunlight, required blower speed, outlet temperature and vehicle speed conditions, this control automatically switches the blower outlet to foot and defroster mode to prevent the windows from becoming fogged up when the ambient air temperature is low.</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Air Inlet Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>Automatically controls the air inlet control damper to help achieve the calculated outlet air temperature that is required.</ptxt>
</entry>
</row>
<row>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>Drives the air inlet control servo motor according to the operation of the air inlet control switch and moves the dampers to the fresh or recirculation position.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Compressor Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>The air conditioning amplifier assembly compares the A/C pulley speed signals, which are transmitted by the ECM (crankshaft position sensor). When the air conditioning amplifier assembly determines that the A/C pulley is locked, it turns off the magnet clutch assembly.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Defroster Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>Defroster control logic is used to improve defroster performance.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear Defogger Control</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>When the ignition switch is ON and the rear defogger switch is pushed, the system is activated to keep the defogger heater on for approx. 15 minutes. However, the operating time of the rear defogger can be extended up to approx. 255 minutes when both of the following requirements are met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Ambient Temperature: -3°C (26.6°F) or less</ptxt>
</item>
<item>
<ptxt>Vehicle Speed: 50 km/h (31.1 mph) or more</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Diagnosis</ptxt>
</entry>
<entry namest="COLSPEC0" nameend="COL2" valign="middle">
<ptxt>A Diagnostic Trouble Code (DTC) is stored in memory when the air conditioning amplifier assembly detects a problem with the air conditioning system.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>NEURAL NETWORK CONTROL</ptxt>
<list1 type="unordered">
<item>
<ptxt>Neural network control collects data under varying environmental conditions and stores it in the air conditioning amplifier assembly. Neural network control uses this data to enhance air conditioning control.</ptxt>
</item>
<item>
<ptxt>Neural network control consists of neurons organized into an input layer, intermediate layer and output layer. The input layer neurons process the input data received from the switches and sensors, which includes the outside temperature, amount of sunlight and room temperature, and outputs this data to the intermediate layer neurons. Based on this data, the intermediate layer neurons adjust the strength of the links among the neurons. The result is then calculated by the output layer neurons in the form of the required outlet temperature, solar correction, target airflow volume and outlet mode control volume. The air conditioning amplifier assembly then controls the servo motors and blower motor in accordance with the control volumes that have been calculated by neural network control.</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="E185705E02" width="7.106578999in" height="7.795582503in"/>
</figure>
</step1>
<step1>
<ptxt>POLLEN REMOVAL MODE CONTROL</ptxt>
<step2>
<ptxt>When the pollen removal mode switch is pressed, the pollen removal mode control is activated. Then, the air vent is switched to the FACE mode and recirculated pollen free air flows in the area around the upper parts of the bodies of the driver and front passenger.</ptxt>
<figure>
<graphic graphicname="E116944E06" width="7.106578999in" height="5.787629434in"/>
</figure>
<step3>
<ptxt>When the pollen removal mode switch signal is input to the air conditioning amplifier assembly, the air conditioning amplifier assembly controls the magnet clutch assembly, recirculation damper servo sub-assembly, damper servo sub-assembly (mode damper servo) and blower with fan motor sub-assembly as shown in the timing chart.</ptxt>
</step3>
<step3>
<ptxt>This control usually operates for approximately 3 minutes.</ptxt>
</step3>
<step3>
<ptxt>After this control stops operating, the air conditioning amplifier assembly automatically returns to the mode it was in just before the pollen removal mode switch was pressed.</ptxt>
</step3>
</step2>
</step1>
<step1>
<ptxt>MODE POSITION AND DAMPER OPERATION</ptxt>
<step2>
<ptxt>Mode Position and Damper Operation</ptxt>
<step3>
<ptxt>Front Air Conditioning Unit</ptxt>
<figure>
<graphic graphicname="E203282E02" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>Function of Main Damper</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.30in"/>
<colspec colname="COL3" colwidth="1.51in"/>
<colspec colname="COL5" colwidth="0.85in"/>
<colspec colname="COL6" colwidth="3.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>Air Inlet Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fresh</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Brings in fresh air.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Recirculation</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Recirculates internal air.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Mix Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>MAX COOL to MAX HOT</ptxt>
</entry>
<entry valign="middle">
<ptxt>E - D - C</ptxt>
<ptxt>E' - D' - C'</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Varies the mixture of cold air and hot air in order to regulate the temperature continuously from hot to cool.</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>MAX COOL Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>MAX COOL</ptxt>
</entry>
<entry valign="middle">
<ptxt>V</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open in the MAX COOL position.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Except MAX COOL</ptxt>
</entry>
<entry valign="middle">
<ptxt>W</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Close in all position except MAX COOL position.</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>MAX HOT Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>MAX HOT</ptxt>
</entry>
<entry valign="middle">
<ptxt>Y</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Open in the MAX HOT position.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Except MAX HOT</ptxt>
</entry>
<entry valign="middle">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Closed in all position except MAX HOT position.</ptxt>
</entry>
</row>
<row>
<entry morerows="4" valign="middle">
<ptxt>Mode Control Damper</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Face</ptxt>
</entry>
<entry valign="middle">
<ptxt>F, I, N, T, X (FACE 1)</ptxt>
<ptxt>F, J, N, T, X (FACE 2)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the front center registers, side registers and rear face registers. (FACE 1)</ptxt>
<ptxt>Air blows out of the front center registers, side registers and rear face registers. In addition, air blows out slightly from the front and rear footwell register ducts. (FACE 2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Bi-level</ptxt>
</entry>
<entry valign="middle">
<ptxt>G, L, N, T, X</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the front center registers, side registers, rear face registers and front and rear footwell register ducts.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot</ptxt>
</entry>
<entry valign="middle">
<ptxt>H, M, P, T, W, Y (MAX HOT position)</ptxt>
<ptxt>H, M, O, T, X (Except MAX HOT Position)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out of the front and rear footwell register ducts.</ptxt>
<ptxt>In addition, air blows out slightly from the front defroster, side registers, front center registers and rear face registers.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106659" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot/Def</ptxt>
</entry>
<entry valign="middle">
<ptxt>H, K, R, T, W, Y (MAX HOT position)</ptxt>
<ptxt>H, M, Q, T, X (Except MAX HOT Position)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Defrosts the windshield through the front defroster and side registers while air is also blown out from the front and rear footwell register ducts.</ptxt>
<ptxt>In addition, air blows out slightly from the front center registers and rear face registers.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106660" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Def</ptxt>
</entry>
<entry valign="middle">
<ptxt>H, I, S, U, X</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Defrosts the windshield through the front defrosters.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Rear Air Conditioning Unit (w/ Rear Air Conditioning System)</ptxt>
<figure>
<graphic graphicname="E167995E03" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Function of Main Damper</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.30in"/>
<colspec colname="COL3" colwidth="1.51in"/>
<colspec colname="COL5" colwidth="0.85in"/>
<colspec colname="COL6" colwidth="3.42in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle">
<ptxt>Mode Control Damper</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Face</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out from the rear roof registers.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Bi-level</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out from the rear roof registers and rear side registers.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air blows out from the rear side registers.</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Air Mix Control Damper</ptxt>
</entry>
<entry valign="middle">
<ptxt>MAX COLD to MAX HOT Temperature Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>D, E</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Varies the mixture of cold air and hot air in order to regulate the temperature continuously from hot to cold.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>AIR OUTLETS AND AIRFLOW VOLUME</ptxt>
<step2>
<ptxt>Air Outlets and Airflow Volume</ptxt>
<step3>
<ptxt>for Front Registers</ptxt>
<figure>
<graphic graphicname="E195804E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" colwidth="1.02in"/>
<colspec colname="COL2" colwidth="1.02in"/>
<colspec colname="COL3" colwidth="1.02in"/>
<colspec colname="COL4" colwidth="1.02in"/>
<colspec colname="COL5" colwidth="1.02in"/>
<colspec colname="COLSPEC0" colwidth="0.98in"/>
<colspec colname="COLSPEC2" colwidth="1in"/>
<thead>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Indication</ptxt>
<ptxt>(Mode)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front Center Register</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Side Register</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front Footwell Register</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Face Register*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Footwell Register</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front Defroster</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>F</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Face 1</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E165952" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E165952" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Face 2</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E165952" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E165952" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Bi-level</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E165952" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106659" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot/Def</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106660" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Def</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E165952" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/o Rear Air Conditioning System</ptxt>
</item>
</list1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The size of the circle indicates the proportion of airflow volume.</ptxt>
</item>
<item>
<ptxt>The same system is used for LHD and RHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</step3>
<step3>
<ptxt>for Rear Registers (w/ Rear Air Conditioning System)</ptxt>
<figure>
<graphic graphicname="E195803E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Indication</ptxt>
<ptxt>(Mode)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Roof Register</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rear Side Register</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106656" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Face</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E165952" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106657" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Bi-level</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155421" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155419" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="E106658" width="0.3937007874015748in" height="0.3937007874015748in"/>
<ptxt>Foot</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="E155422" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The size of the circle ○ indicates the ratio of airflow volume.</ptxt>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>A/C LOCK SENSOR</ptxt>
<ptxt>The A/C lock sensor sends A/C pulley speed signals to the air conditioning amplifier assembly. The air conditioning amplifier assembly determines whether the cooler compressor assembly is locked or not by using those signals and engine speed signals.</ptxt>
</step1>
<step1>
<ptxt>No. 1 COOLER THERMISTOR</ptxt>
<ptxt>The No. 1 cooler thermistor detects the temperature of the cool air immediately after it passes through the evaporator in the form of resistance changes, and outputs the temperature to the air conditioning amplifier assembly.</ptxt>
</step1>
<step1>
<ptxt>BLOWER WITH FAN MOTOR SUB-ASSEMBLY</ptxt>
<ptxt>The blower with fan motor sub-assembly has a built-in blower controller and is controlled by the air conditioning amplifier assembly using duty control.</ptxt>
</step1>
<step1>
<ptxt>AIR CONDITIONING HARNESS ASSEMBLY (BUS CONNECTOR)</ptxt>
<step2>
<ptxt>A BUS connector is used in the wire harness that connects the servo motor to the air conditioning amplifier assembly.</ptxt>
<figure>
<graphic graphicname="E195802E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Connector Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Connected to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>*1: Bus connector</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper servo sub-assembly LH (driver side air mix damper servo)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*2: Bus connector</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper servo sub-assembly LH (mode damper servo)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*3: Bus connector</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper servo sub-assembly RH (front passenger side air mix damper servo)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*4: Bus connector</ptxt>
</entry>
<entry valign="middle">
<ptxt>Recirculation damper servo sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*5: Connector</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air conditioning amplifier assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*6: Connector</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 cooler thermistor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*7: Bus connector</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper servo sub-assembly RH (driver side air mix damper servo)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>*8: Bus connector</ptxt>
</entry>
<entry valign="middle">
<ptxt>Damper servo sub-assembly LH (front passenger side air mix damper servo)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Each BUS connector has a built-in communication/driver IC which communicates with each servo motor connector, actuates the servo motor, and has a position detection function.</ptxt>
<figure>
<graphic graphicname="E187730E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>SERVO MOTOR</ptxt>
<step2>
<ptxt>The pulse pattern type servo motor detects the relative position using 2-bit on/off signals.</ptxt>
<ptxt>The forward and reverse revolutions of this motor are detected using two signals, A and B, which output four types of patterns. The air conditioning amplifier assembly counts the number of pulse patterns in order to determine the stopped position.</ptxt>
<figure>
<graphic graphicname="E187731E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>QUICK HEATER ASSEMBLY (w/ PTC Heater)</ptxt>
<step2>
<ptxt>General</ptxt>
<step3>
<ptxt>The PTC heater is located above the heater core in the air conditioner unit.</ptxt>
</step3>
<step3>
<ptxt>The PTC heater consists of a PTC element, aluminum fins and brass plates. When current is applied to the PTC element, it generates heat to warm the air that passes through the unit.</ptxt>
<figure>
<graphic graphicname="E203279E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>PTC Heater Operating Conditions</ptxt>
<step3>
<ptxt>The on/off operation of the PTC heater is controlled by the air conditioning amplifier assembly in accordance with the engine coolant temperature, ambient temperature, engine speed, air mix setting and electrical load (generator power ratio).</ptxt>
<ptxt>For example, the number of operating PTC heaters varies according to engine coolant temperature as shown in the graph below.</ptxt>
<figure>
<graphic graphicname="E145149E05" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
</step2>
</step1>
<step1>
<ptxt>VISCOUS WITH MAGNET CLUTCH HEATER ASSEMBLY (w/ Viscous Heater)</ptxt>
<step2>
<ptxt>The viscous with magnet clutch heater assembly, which is located on top of the engine, is driven by a drive belt. Pressing the heater switch assembly provided in the instrument panel engages the magnetic clutch, causing the rotor in the viscous with magnet clutch heater assembly to rotate and the silicon oil to mix. The shear heat that is thus generated heats the coolant.</ptxt>
</step2>
<step2>
<ptxt>The viscous with magnet clutch heater assembly is controlled in accordance with the engine speed, coolant temperature and ambient temperature. While the power heater is engaged, the engine idling speed is raised to a predetermined value whenever the shift lever is in N or D.</ptxt>
</step2>
</step1>
<step1>
<ptxt>COOLER THERMISTOR (ROOM TEMPERATURE SENSOR)</ptxt>
<ptxt>The cooler thermistor (room temperature sensor) detects the cabin temperature based on changes in the resistance of its built-in thermistor and sends a signal to the air conditioning amplifier assembly.</ptxt>
</step1>
<step1>
<ptxt>COOLER THERMISTOR (AMBIENT TEMPERATURE SENSOR)</ptxt>
<ptxt>The cooler thermistor (ambient temperature sensor) detects the outside temperature based on changes in the resistance of its built-in thermistor and sends a signal to the air conditioning amplifier assembly.</ptxt>
</step1>
<step1>
<ptxt>AUTOMATIC LIGHT CONTROL SENSOR (SOLAR SENSOR)</ptxt>
<step2>
<ptxt>The automatic light control sensor (solar sensor) consists of a photo diode, 2 amplifier circuits for the solar sensor and a frequency converter circuit for the light control sensor.</ptxt>
</step2>
<step2>
<ptxt>The automatic light control sensor (solar sensor) detects (in the form of changes in the current that flows through the built-in photo diode) the changes in the amount of sunlight from the LH and RH sides (2 directions) and outputs these sunlight intensity signals to the air conditioning amplifier assembly.</ptxt>
<figure>
<graphic graphicname="E116951E04" width="7.106578999in" height="4.7836529in"/>
</figure>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>