<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B997_T00IV" variety="T00IV">
<name>INSTRUMENT PANEL SPEAKER</name>
<para id="RM000000VEO02XX" category="A" type-id="80001" name-id="AV9NW-03" from="201210">
<name>REMOVAL</name>
<subpara id="RM000000VEO02XX_02" type-id="11" category="10" proc-id="RM23G0E___0000C0Q00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000000VEO02XX_01" type-id="01" category="01">
<s-1 id="RM000000VEO02XX_01_0017" proc-id="RM23G0E___0000C0M00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work. (See page <xref label="Seep02" href="RM000003YMD00GX"/>)</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000000VEO02XX_01_0030" proc-id="RM23G0E___0000C0600001">
<ptxt>REMOVE FRONT NO. 1 ASSIST GRIP PLUG LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238741E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other front No. 1 assist grip plug.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the front No. 1 assist grip plug.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000000VEO02XX_01_0016" proc-id="RM23G0E___0000C0300001">
<ptxt>REMOVE NO. 1 ASSIST GRIP
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238742" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other No. 1 assist grip.</ptxt>
</atten4>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the No. 1 assist grip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEO02XX_01_0007" proc-id="RM23G0E___0000C0400001">
<ptxt>REMOVE FRONT PILLAR GARNISH LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238743" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 3 guides and remove the front pillar garnish.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B238744E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Curtain Shield Airbag:</ptxt>
<ptxt>Protect the curtain shield airbag assembly.</ptxt>
<s3>
<ptxt>Completely cover the airbag with a cloth or nylon sheet and secure the ends of the cover with adhesive tape as shown in the illustration.</ptxt>
<atten3>
<ptxt>Cover the curtain shield airbag with a protective cover as soon as the front pillar garnish is removed.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Curtain Shield Airbag Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive Tape</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Cover</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000000VEO02XX_01_0031" proc-id="RM23G0E___0000C0O00001">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL SPEAKER PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238223E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the No. 1 instrument panel speaker panel.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 2 clips, claw, and 2 guides and remove the No. 1 instrument panel speaker panel.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000000VEO02XX_01_0004" proc-id="RM23G0E___0000C0L00001">
<ptxt>REMOVE FRONT NO. 2 SPEAKER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B239150" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the front No. 2 speaker and disconnect the speaker connector.</ptxt>
<atten3>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000VEO02XX_01_0025" proc-id="RM23G0E___00006YI00000">
<ptxt>REMOVE INTEGRATION CONTROL AND PANEL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="E204227" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the integration control and panel assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEO02XX_01_0027" proc-id="RM23G0E___0000C0N00001">
<ptxt>REMOVE DISPLAY AND NAVIGATION MODULE DISPLAY WITH BRACKET (w/ Navigation System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts.</ptxt>
<figure>
<graphic graphicname="B239092" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Pull the display and navigation module display to detach the 8 clips on the backside of the display and navigation module display.</ptxt>
<figure>
<graphic graphicname="B239093" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connectors and remove the display and navigation module display.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEO02XX_01_0032" proc-id="RM23G0E___0000C0P00001">
<ptxt>REMOVE UPPER INSTRUMENT CLUSTER FINISH PANEL
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238203" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 8 clips and 2 guides and remove the upper instrument cluster finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000VEO02XX_01_0003" proc-id="RM23G0E___0000C0K00001">
<ptxt>REMOVE FRONT NO. 4 SPEAKER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B239154" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the front No. 4 speaker and disconnect the speaker connector.</ptxt>
<atten3>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>