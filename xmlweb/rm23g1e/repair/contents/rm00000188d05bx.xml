<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000188D05BX" category="C" type-id="303SA" name-id="ESTEB-05" from="201207" to="201210">
<dtccode>P0545</dtccode>
<dtcname>Exhaust Gas Temperature Sensor Circuit Low (Bank 1 Sensor 1)</dtcname>
<dtccode>P0546</dtccode>
<dtcname>Exhaust Gas Temperature Sensor Circuit High (Bank 1 Sensor 1)</dtcname>
<dtccode>P2032</dtccode>
<dtcname>Exhaust Gas Temperature Sensor Circuit Low (Bank 1 Sensor 2)</dtcname>
<dtccode>P2033</dtccode>
<dtcname>Exhaust Gas Temperature Sensor Circuit High (Bank 1 Sensor 2)</dtcname>
<dtccode>P242C</dtccode>
<dtcname>Exhaust Gas Temperature Sensor Circuit Low Bank 1 Sensor 3</dtcname>
<dtccode>P242D</dtccode>
<dtcname>Exhaust Gas Temperature Sensor Circuit High Bank 1 Sensor 3</dtcname>
<subpara id="RM00000188D05BX_01" type-id="60" category="03" proc-id="RM23G0E___00002JD00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A243953E01" width="7.106578999in" height="6.791605969in"/>
</figure>
<ptxt>The exhaust gas temperature sensors are installed before and after the DPF catalytic converter to sense the exhaust gas temperature.</ptxt>
<ptxt>A thermistor built into the sensor changes its resistance value according to the exhaust gas temperature. The lower the exhaust gas temperature, the higher the thermistor resistance value. The higher the exhaust gas temperature, the lower the thermistor resistance value.</ptxt>
<ptxt>The exhaust gas temperature sensor is connected to the ECM. The 5 V power source voltage in the ECM is applied to the exhaust gas temperature sensor from terminal THCF (B1S1), THCI (B1S2) and THCO (B1S3) via resistor R.</ptxt>
<ptxt>Resistor R and the exhaust gas temperature sensor are connected in series. When the resistance value of the exhaust gas temperature sensor changes in accordance with the exhaust gas temperature, the voltage at terminals THCF (B1S1), THCI (B1S2) and THCO (B1S3) also changes. The No. 1 exhaust gas temperature sensor monitors the temperature upstream of the CCo catalyst. The No. 2 exhaust gas temperature sensor monitors the temperature upstream of the DPF catalyst. The No. 3 exhaust gas temperature sensor monitors the rise in temperature of the DPF catalyst. When PM forced regeneration is needed, the ECM operates the exhaust fuel addition injector to obtain target upstream temperature of the DPF catalytic converter as monitored through sensor 2. In addition, the ECM monitors the temperature increase of the DPF catalytic converter using sensor 3.</ptxt>
<table pgwide="1">
<title>P0545</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idle the engine for 3 seconds after warming up the engine (the engine coolant temperature is 60°C (140°F) or higher) and allowing 11 minutes to elapse after starting the engine.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust gas temperature sensor (B1S1) output voltage is below 0.2 V for 3 seconds or more when all conditions are met. (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>After starting the engine, 11 minutes or more elapses</ptxt>
</item>
<item>
<ptxt>Engine is running</ptxt>
</item>
<item>
<ptxt>Coolant temperature is 60°C (140°F) or more</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in exhaust gas temperature sensor (B1S1) circuit</ptxt>
</item>
<item>
<ptxt>Exhaust gas temperature sensor (B1S1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0546</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idle the engine for 3 seconds after warming up the engine (the engine coolant temperature is 60°C (140°F) or higher) and allowing 11 minutes to elapse after starting the engine.</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust gas temperature sensor (B1S1) output voltage is higher than 4.95 V for 3 seconds or more when all conditions are met. (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>After starting the engine, 11 minutes or more elapses</ptxt>
</item>
<item>
<ptxt>Engine is running</ptxt>
</item>
<item>
<ptxt>Coolant temperature is 60°C (140°F) or more</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in exhaust gas temperature sensor (B1S1) circuit</ptxt>
</item>
<item>
<ptxt>Exhaust gas temperature sensor (B1S1)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2032</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idle the engine for 3 seconds after warming up the engine (the engine coolant temperature is 60°C (140°F) or higher) and allowing 11 minutes to elapse after starting the engine.</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 exhaust gas temperature sensor (B1S2) output voltage is below 0.2 V for 3 seconds or more when all conditions are met. (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>After starting the engine, 11 minutes or more elapses</ptxt>
</item>
<item>
<ptxt>Engine is running</ptxt>
</item>
<item>
<ptxt>Coolant temperature is 60°C (140°F) or more</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in No. 2 exhaust gas temperature sensor (B1S2) circuit</ptxt>
</item>
<item>
<ptxt>No. 2 exhaust gas temperature sensor (B1S2)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P2033</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idle the engine for 3 seconds after warming up the engine (the engine coolant temperature is 60°C (140°F) or higher) and allowing 11 minutes to elapse after starting the engine.</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 exhaust gas temperature sensor (B1S2) output voltage is higher than 4.95 V for 3 seconds or more when all conditions are met. (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>After starting the engine, 11 minutes or more elapses</ptxt>
</item>
<item>
<ptxt>Engine is running</ptxt>
</item>
<item>
<ptxt>Coolant temperature is 60°C (140°F) or more</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in No. 2 exhaust gas temperature sensor (B1S2) circuit</ptxt>
</item>
<item>
<ptxt>No. 2 exhaust gas temperature sensor (B1S2)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P242C</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idle the engine for 3 seconds after warming up the engine (the engine coolant temperature is 60°C (140°F) or higher) and allowing 11 minutes to elapse after starting the engine.</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3 exhaust gas temperature sensor (B1S3) output voltage is below 0.2 V for 3 seconds or more when all conditions are met. (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>After starting the engine, 11 minutes or more elapses</ptxt>
</item>
<item>
<ptxt>Engine is running</ptxt>
</item>
<item>
<ptxt>Coolant temperature is 60°C (140°F) or more</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in No. 3 exhaust gas temperature sensor (B1S3) circuit</ptxt>
</item>
<item>
<ptxt>No. 3 exhaust gas temperature sensor (B1S3)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P242D</title>
<tgroup cols="3">
<colspec colname="COLSPEC0" colwidth="2.36in"/>
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Idle the engine for 3 seconds after warming up the engine (the engine coolant temperature is 60°C (140°F) or higher) and allowing 11 minutes to elapse after starting the engine.</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 3 exhaust gas temperature sensor (B1S3) output voltage is higher than 4.95 V for 3 seconds or more when all conditions are met. (1 trip detection logic):</ptxt>
<list1 type="unordered">
<item>
<ptxt>After starting the engine, 11 minutes or more elapses</ptxt>
</item>
<item>
<ptxt>Engine is running</ptxt>
</item>
<item>
<ptxt>Coolant temperature is 60°C (140°F) or more</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in No. 3 exhaust gas temperature sensor (B1S3) circuit</ptxt>
</item>
<item>
<ptxt>No. 3 exhaust gas temperature sensor (B1S3)</ptxt>
</item>
<item>
<ptxt>Abnormal rise in temperature due to melting of DPF catalytic converter</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0545</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Exhaust Temperature B1S1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0546</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2032</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Exhaust Temperature B1S2</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2033</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P242C</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Exhaust Temperature B1S3</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P242D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTC P20CF (Exhaust fuel addition injector malfunction) may be stored if there is an open malfunction in an exhaust gas temperature sensor circuit.</ptxt>
</item>
<item>
<ptxt>Sensor 1 represents the sensor located upstream of the CCo catalytic converter.</ptxt>
</item>
<item>
<ptxt>Sensor 2 represents the sensor located upstream of the DPF catalytic converter.</ptxt>
</item>
<item>
<ptxt>Sensor 3 represents the sensor located downstream of the DPF catalytic converter.</ptxt>
</item>
<item>
<ptxt>When DTC P0545, P0546, P2032, P2033, P242C and/or P242D is output, check the upstream and downstream exhaust gas temperature indicated by "Powertrain / Engine and ECT / Exhaust Temperature B1S1, Exhaust Temperature B1S2 and Exhaust Temperature B1S3" using the intelligent tester.</ptxt>
</item>
</list1>
<table pgwide="1">
<title>Reference:</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas Temperature</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas Temperature Sensor Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle">
<ptxt>Idling after warm-up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Constant at between 50 and 700°C (122 and 1292°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1000°C (1832°F) or higher</ptxt>
</entry>
<entry valign="middle">
<ptxt>Short circuit</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188D05BX_02" type-id="64" category="03" proc-id="RM23G0E___00002JE00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM constantly monitors the output voltages from the exhaust gas temperature sensors in order to detect problems with the sensors. When the sensor output voltage deviates from the normal operating range (between 0.2 V and 4.95 V) for more than 3 seconds after the engine is warmed up, the ECM interprets this as malfunction of the sensor circuit and illuminates the MIL.</ptxt>
</content5>
</subpara>
<subpara id="RM00000188D05BX_03" type-id="32" category="03" proc-id="RM23G0E___00002JF00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A246634E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000188D05BX_04" type-id="51" category="05" proc-id="RM23G0E___00002JG00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK062X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See Page <xref label="Seep04" href="RM0000012XK062X"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>If DTC P242D is stored and the exhaust gas temperature sensor (B1S3) circuit is not malfunctioning, check if the DPF catalytic converter has melted as DTC P242D may be stored when the DPF catalytic converter melts and the exhaust gas temperature sensor (B1S3) is exposed to high temperatures.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000188D05BX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000188D05BX_05_0001" proc-id="RM23G0E___00002JH00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (EXHAUST TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Exhaust Temperature B1S1, Exhaust Temperature B1S2 and Exhaust Temperature B1S3.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>Same as the actual exhaust gas temperature (50 to 700°C [122 to 1292°F] during idling after warm-up), and varies after an engine speed of 3000 rpm is maintained for 1 minute.</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Temperature Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-40°C (-40°F) (After warming up the engine)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>1000°C (1832°F) or higher</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>OK: Same as the actual exhaust gas temperature (50 to 700°C [122 to 1292°F] during idling after warm-up), and varies after maintaining an engine speed of 3000 rpm for 1 minute after accelerating the engine from idling to 3000 rpm</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If there is a short circuit, the intelligent tester will indicate -40°C (-40°F).</ptxt>
</item>
<item>
<ptxt>If there is an open circuit, the intelligent tester will indicate 1000°C (1832°F) or higher.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000188D05BX_05_0002" fin="false">A</down>
<right ref="RM00000188D05BX_05_0004" fin="false">B</right>
<right ref="RM00000188D05BX_05_0010" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0002" proc-id="RM23G0E___00002JI00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR OPEN IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C133, C134 or C135 exhaust gas temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A246635E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect terminals 1 and 2 of the exhaust gas temperature sensor wire harness side connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Exhaust Temperature B1S1, Exhaust Temperature B1S2 and Exhaust Temperature B1S3.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>1000°C (1832°F) or higher</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 2 Exhaust Gas Temperature Sensor (B1S2))</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 3 Exhaust Gas Temperature Sensor (B1S3))</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Exhaust Gas Temperature Sensor (B1S1))</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the exhaust gas temperature sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188D05BX_05_0011" fin="false">OK</down>
<right ref="RM00000188D05BX_05_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0011" proc-id="RM23G0E___00002JN00000">
<testtitle>REPLACE EXHAUST GAS TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the exhaust gas temperature sensor (See page <xref label="Seep01" href="RM000003TEF00TX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188D05BX_05_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0012" proc-id="RM23G0E___00002JO00000">
<testtitle>CHECK HARNESS AND CONNECTOR (EXHAUST GAS TEMPERATURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the exhaust gas temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COLSPEC1" colwidth="3.54in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C133-1 - C92-5 (THCI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C133-2 - C92-11 (ETCI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C134-1 - C91-1 (THCO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C134-2 - C91-2 (ETCO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C135-1 - C92-17 (THCF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C135-2 - C92-16 (ETCF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the exhaust gas temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188D05BX_05_0013" fin="false">OK</down>
<right ref="RM00000188D05BX_05_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0013" proc-id="RM23G0E___00002JP00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188D05BX_05_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0004" proc-id="RM23G0E___00002JJ00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C133, C134 or C135 exhaust gas temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A246636E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Exhaust Temperature B1S1, Exhaust Temperature B1S2 and Exhaust Temperature B1S3.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>-40°C (-40°F)</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Reconnect the exhaust gas temperature sensor connector.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the exhaust gas temperature sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188D05BX_05_0014" fin="false">OK</down>
<right ref="RM00000188D05BX_05_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0014" proc-id="RM23G0E___00002JQ00000">
<testtitle>REPLACE EXHAUST GAS TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the exhaust gas temperature sensor (See page <xref label="Seep01" href="RM000003TEF00TX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188D05BX_05_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0015" proc-id="RM23G0E___00002JR00000">
<testtitle>CHECK HARNESS AND CONNECTOR (EXHAUST GAS TEMPERATURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the exhaust gas temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COLSPEC1" colwidth="3.54in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C133-1 or C92-5 (THCI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C134-1 or C91-1 (THCO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C135-1 or C92-17 (THCF) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the exhaust gas temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188D05BX_05_0006" fin="false">OK</down>
<right ref="RM00000188D05BX_05_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0006" proc-id="RM23G0E___00002JK00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000188D05BX_05_0010" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0009" proc-id="RM23G0E___00002JL00000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188D05BX_05_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0010" proc-id="RM23G0E___00002JM00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z7X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Idle the engine for 3 seconds or more after warming up the engine (the engine coolant temperature is 60°C (140°F) or higher) and allowing 11 minutes to elapse after starting the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000188D05BX_05_0016" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000188D05BX_05_0016">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>