<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002E" variety="S002E">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002E_7B9GZ_T00QN" variety="T00QN">
<name>LIGHTING SYSTEM</name>
<para id="RM000002ZNJ01IX" category="J" type-id="3016S" name-id="LE29B-04" from="201207" to="201210">
<dtccode/>
<dtcname>Hazard Warning Switch Circuit</dtcname>
<subpara id="RM000002ZNJ01IX_01" type-id="60" category="03" proc-id="RM23G0E___0000IZ500000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The turn signal flasher receives a hazard warning signal switch information signal and illuminates the turn signal lights.</ptxt>
</content5>
</subpara>
<subpara id="RM000002ZNJ01IX_02" type-id="32" category="03" proc-id="RM23G0E___0000IZ600000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E146720E11" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002ZNJ01IX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002ZNJ01IX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002ZNJ01IX_04_0001" proc-id="RM23G0E___0000IZ700000">
<testtitle>INSPECT HAZARD WARNING SIGNAL SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the hazard warning signal switch (See page <xref label="Seep01" href="RM000003QJX00WX"/>).</ptxt>
<figure>
<graphic graphicname="E203231E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>w/ TOYOTA Parking Assist-sensor System</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 (E) - 4 (B)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hazard warning signal switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Hazard warning signal switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o TOYOTA Parking Assist-sensor System</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 - 4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hazard warning signal switch on</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Hazard warning signal switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ TOYOTA Parking Assist-sensor System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o TOYOTA Parking Assist-sensor System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002ZNJ01IX_04_0003" fin="false">OK</down>
<right ref="RM000002ZNJ01IX_04_0002" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZNJ01IX_04_0002">
<testtitle>REPLACE HAZARD WARNING SIGNAL SWITCH ASSEMBLY<xref label="Seep01" href="RM000003QJX00WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZNJ01IX_04_0003" proc-id="RM23G0E___0000IZ800000">
<testtitle>CHECK HARNESS AND CONNECTOR (HAZARD WARNING SIGNAL SWITCH ASSEMBLY - TURN SIGNAL FLASHER ASSEMBLY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>*1: w/ TOYOTA Parking Assist-sensor System</ptxt>
</item>
<item>
<ptxt>*2: w/o TOYOTA Parking Assist-sensor System</ptxt>
</item>
</list1>
<test1>
<ptxt>Disconnect the G14*1 or G15*2 hazard warning signal switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G8 turn signal flasher connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>w/ TOYOTA Parking Assist-sensor System</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G14-4 (B) - 8 (HAZ)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G14-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G14-4 (B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o TOYOTA Parking Assist-sensor System</subtitle>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G15-4 - 8 (HAZ)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G15-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G15-4 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002ZNJ01IX_04_0004" fin="true">OK</down>
<right ref="RM000002ZNJ01IX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002ZNJ01IX_04_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002CE20DJX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002ZNJ01IX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>