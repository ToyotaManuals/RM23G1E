<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E9_T00NX" variety="T00NX">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM000002LIP03ZX" category="U" type-id="3001G" name-id="AC8TL-03" from="201207">
<name>TERMINALS OF ECU</name>
<subpara id="RM000002LIP03ZX_z0" proc-id="RM23G0E___0000HFZ00000">
<content5 releasenbr="1">
<step1>
<ptxt>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY</ptxt>
<figure>
<graphic graphicname="E132743E23" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the G24 and G25 amplifier connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G25-21 (B) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>V - W-B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery power source</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>G25-1 (IG+) - G25-14 (GND)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>L - W-B</ptxt>
</entry>
<entry morerows="1" valign="middle" align="left">
<ptxt>Ignition power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G25-14 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ground for main power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G24-20 (RGND) - Body ground*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ground for main power supply</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G24-24 (+B2) - G24-20 (RGND)*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>GR - W-B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Battery power source</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Reconnect the G24 and G25 amplifier connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the voltage and resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G25-5 (TAM) - G25-13 (SG-2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>V - G</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cooler thermistor (ambient temperature sensor) signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Ambient temperature 25°C (77°F)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>1.35 to 1.75 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-5 (TAM) - G25-13 (SG-2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>V - G</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cooler thermistor (ambient temperature sensor) signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Ambient temperature 40°C (104°F)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>0.9 to 1.2 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-8 (LOCK) - G25-13 (SG-2)*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - G</ptxt>
</entry>
<entry valign="middle">
<ptxt>A/C compressor lock sensor signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine running</ptxt>
</item>
<item>
<ptxt>Blower switch LO</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-9 (PRE) - G25-13 (SG-2)*3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - G</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air conditioner pressure sensor signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure normal (below 3.025 MPa [30.9 kgf/cm<sup>2</sup>, 438.6 psi] and higher than 0.176 MPa [1.8 kgf/cm<sup>2</sup>, 25.5 psi])</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>0.63 to 4.72 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-9 (PRE) - G25-13 (SG-2)*3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - G</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air conditioner pressure sensor signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure abnormal (below 0.176 MPa [1.8 kgf/cm<sup>2</sup>, 25.5 psi])</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.63 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-9 (PRE) - G25-13 (SG-2)*3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - G</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air conditioner pressure sensor signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure abnormal (higher than 3.025 MPa [30.9 kgf/cm<sup>2</sup>, 438.6 psi])</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>4.72 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-9 (PRE) - Body ground*4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 pressure switch signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure normal (below 3.14 MPa [32.0 kgf/cm<sup>2</sup>, 455 psi] and higher than 0.196 MPa [2.0 kgf/cm<sup>2</sup>, 28 psi])</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-9 (PRE) - Body ground*4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 pressure switch signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure abnormal (below 0.196 MPa [2.0 kgf/cm<sup>2</sup>, 28 psi] or higher than 3.14 MPa [32.0 kgf/cm<sup>2</sup>, 455 psi])</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>4.69 V or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-10 (S5-3) - G25-13 (SG-2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B - G</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power supply for air conditioner pressure sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.75 to 5.25 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-15 (COOL) - G25-14 (GND)*5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>V - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cooling box assembly operation signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Engine running</ptxt>
</item>
<item>
<ptxt>Cooler control switch sub-assembly on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-17 (AC1) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor operation signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-18 (MHTR) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>G - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>MIR HTR relay signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Mirror heater switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-20 (MGC) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Magnet clutch assembly operation signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Blower switch LO</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-23 (BLW) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Blower with fan motor sub-assembly control signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Blower switch LO</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-27 (ACT) - G25-14 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>R - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor operation signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>4.75 to 5.25 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-36 (SWVC) - G25-14 (GND)*6</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idle up switch operation signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Idle up switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G24-1 (RBUS) - G24-21 (RBUG)*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - L</ptxt>
</entry>
<entry valign="middle">
<ptxt>BUS IC control signal (for Rear)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>G24-4 (TEC) - G24-19 (SGND)*1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>SB - V</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Rear evaporator temperature sensor signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Rear evaporator temperature 0°C (32°F)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>2.0 to 2.4 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Rear evaporator temperature: 15°C (59°F)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>1.4 to 1.8 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G24-12 (SWCB) - G25-14 (GND)*5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LG - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cooling box assembly operation signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
<item>
<ptxt>Cooler control switch sub-assembly on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G24-19 (SGND) - Body ground*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>V - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground for rear evaporator temperature sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G24-20 (RGND) - Body ground*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground for power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G24-22 (BLWH) - G24-20 (RGND)*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear blower with fan motor sub-assembly control signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Rear blower switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
<ptxt>(See waveform 2)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G24-23 (RBBU) - G24-21 (RBUG)*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - L</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power supply for BUS IC (for Rear)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z1-2 (BUS G) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground for BUS IC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z1-3 (BUS) - z1-2 (BUS G)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>BUS IC control signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>z1-4 (B BUS) - z1-2 (BUS G)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power supply for BUS IC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>z1-6 (TEA) - z1-5 (SGA)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>No. 1 cooler thermistor signal</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Evaporator temperature 0°C (32°F)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>1.7 to 2.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Evaporator temperature 15°C (59°F)</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>0.9 to 1.3 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Rear Air Conditioning System</ptxt>
</item>
<item>
<ptxt>*2: except 5L-E</ptxt>
</item>
<item>
<ptxt>*3: for 1KD-FTV or w/ Rear Air Conditioning System</ptxt>
</item>
<item>
<ptxt>*4: except 1KD-FTV and w/o Rear Air Conditioning System</ptxt>
</item>
<item>
<ptxt>*5: w/ Refrigerated Cool Box</ptxt>
</item>
<item>
<ptxt>*6: w/ Idle Up Switch</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>If the result is not as specified, the air conditioning amplifier assembly may have a malfunction.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 1.</ptxt>
<figure>
<graphic graphicname="E116941E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>A/C Compressor Lock Sensor Signal</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>G25-8 (LOCK) - G25-14 (GND)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>200 mV/DIV., 10 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine running</ptxt>
</item>
<item>
<ptxt>A/C switch on</ptxt>
</item>
<item>
<ptxt>Blower switch LO</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Using an oscilloscope, check waveform 2.</ptxt>
<figure>
<graphic graphicname="E103396E12" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Blower Motor Control Signal</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>G25-23 (BLW) - H46-14 (GND)</ptxt>
</item>
<item>
<ptxt>G24-22 (BLWH) - G24-20 (RGND)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>1 V/DIV., 500 μs/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Blower switch LO</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When the blower level is increased, the duty ratio changes accordingly.</ptxt>
</atten4>
</step2>
</step1>
<step1>
<ptxt>CHECK AIR CONDITIONING AMPLIFIER ASSEMBLY (FOR COOLING BOX) (w/ Refrigerated Cool Box)</ptxt>
<figure>
<graphic graphicname="E197829" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the air conditioning amplifier (for cooling box) connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (+B) - 4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-R - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2 (IG+) - 4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>GR - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
<step2>
<ptxt>Reconnect the air conditioning amplifier (for cooling box) connectors.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5" align="center">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>5 (SW) - 4 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>LG-R - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cooler control switch sub-assembly operation signal</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine running</ptxt>
</item>
<item>
<ptxt>Cooler control switch on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>6 (CBI) -  4 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>L-G - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cooler compressor permission signal</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine running</ptxt>
</item>
<item>
<ptxt>Cooler control switch sub-assembly on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>7 (LED) -  4 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>L-B - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cooling box indicator signal</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine running</ptxt>
</item>
<item>
<ptxt>Cooler control switch sub-assembly on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>8 (CBT) -  4 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>BR-Y - W-B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Air conditioning amplifier assembly request signal</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine running</ptxt>
</item>
<item>
<ptxt>Cooler control switch sub-assembly on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>9 (BLW) -  4 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>W - W-B</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>No. 1 cooler blower motor sub-assembly operation signal</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Engine running</ptxt>
</item>
<item>
<ptxt>Cooler control switch sub-assembly on</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>10 (SG) -  4 (GND)</ptxt>
</entry>
<entry valign="middle">
<ptxt>W-L - W-B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>CHECK INTEGRATION CONTROL AND PANEL ASSEMBLY</ptxt>
<figure>
<graphic graphicname="E196915E04" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the G23 panel connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G23-1 (+B) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Battery power source</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-5 (IG) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-6 (ACC) - G23-4 (GND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>P - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ACC power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G23-4 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK AIR CONDITIONING CONTROL ASSEMBLY (w/ Rear Air Conditioning System)</ptxt>
<figure>
<graphic graphicname="E196915E05" width="7.106578999in" height="1.771723296in"/>
</figure>
<step2>
<ptxt>Disconnect the T3 control connector.</ptxt>
</step2>
<step2>
<ptxt>Measure the resistance and voltage according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal No. (Symbol)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wiring Color</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Terminal Description</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>T3-5 (IG) - T3-8 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>L - W-B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>IG power supply</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>T3-8 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>W-B - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, there may be a malfunction on the wire harness side.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>