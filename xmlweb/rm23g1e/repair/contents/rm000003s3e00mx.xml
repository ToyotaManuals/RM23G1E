<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9CU_T00MI" variety="T00MI">
<name>FRONT POWER SEAT CONTROL SYSTEM (w/ Memory)</name>
<para id="RM000003S3E00MX" category="J" type-id="300RO" name-id="SE53T-07" from="201210">
<dtccode/>
<dtcname>Power Seat Position is not Memorized</dtcname>
<subpara id="RM000003S3E00MX_01" type-id="60" category="03" proc-id="RM23G0E___0000G5T00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the seat memory SET switch and a memory switch (M1 or M2) are pressed simultaneously, the main body ECU commands the position control ECU through CAN communication to record the value of each position sensor.</ptxt>
</content5>
</subpara>
<subpara id="RM000003S3E00MX_02" type-id="32" category="03" proc-id="RM23G0E___0000G5U00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B205120E02" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003S3E00MX_03" type-id="51" category="05" proc-id="RM23G0E___0000G5V00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>First perform the communication function inspections in How to Proceed with Troubleshooting to confirm that there are no CAN communication malfunctions before troubleshooting this symptom.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003S3E00MX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003S3E00MX_04_0001" proc-id="RM23G0E___0000G5W00001">
<testtitle>CHECK FRONT POWER SEAT CONTROL FUNCTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that each function of the power seat operates normally by using the front power seat switches.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Each function of power seat operates normally by using seat switches.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003S3E00MX_04_0002" fin="false">OK</down>
<right ref="RM000003S3E00MX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0002" proc-id="RM23G0E___0000G5X00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (SEAT POSITION MEMORY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the seat memory switch to record the seat position (See page <xref label="Seep01" href="RM000003S3I00XX"/>).</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The seat position will not be recorded if the SET switch and both memory switches are pressed simultaneously.</ptxt>
</item>
<item>
<ptxt>If a memorizing operation has failed, release all switches. The seat memory function does not operate unless the switches are released.</ptxt>
</item>
</list1>
</atten3>
</test1>
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep02" href="RM0000039RN01ZX"/>).</ptxt>
<table pgwide="1">
<title>Driver Seat</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Seat Memory No1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat position memorized with M1 switch / Mem or Not Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mem: Memorized</ptxt>
<ptxt>Not Mem: Not memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Seat Memory No2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat position memorized with M2 switch / Mem or Not Mem</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mem: Memorized</ptxt>
<ptxt>Not Mem: Not memorized</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>"Mem" is displayed on the tester.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003S3E00MX_04_0011" fin="true">OK</down>
<right ref="RM000003S3E00MX_04_0016" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0011">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0016" proc-id="RM23G0E___0000G6100001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (SEAT MEMORY SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM0000039RN01ZX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>M1 Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch M1 signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Seat memory switch M1 on</ptxt>
<ptxt>OFF: Seat memory switch M1 off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>M2 Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch M2 signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Seat memory switch M2 on</ptxt>
<ptxt>OFF: Seat memory switch M2 off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>SET Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch SET signal / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Seat memory switch SET on</ptxt>
<ptxt>OFF: Seat memory switch SET off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On intelligent tester screen, each item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003S3E00MX_04_0006" fin="false">OK</down>
<right ref="RM000003S3E00MX_04_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0006" proc-id="RM23G0E___0000G6000001">
<testtitle>CHECK FRONT POWER SEAT SWITCH (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the front power seat switch with a new one or normally operating one.</ptxt>
</test1>
<test1>
<ptxt>Use the seat memory switch to record the seat position (See page <xref label="Seep01" href="RM000003S3I00XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Refer to the Data List for the driver seat and check that the memory operation is completed normally.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Seat position is recorded normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003S3E00MX_04_0008" fin="true">OK</down>
<right ref="RM000003S3E00MX_04_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0008">
<testtitle>END (FRONT POWER SEAT SWITCH WAS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0004" proc-id="RM23G0E___0000G5Y00001">
<testtitle>INSPECT SEAT MEMORY SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the seat memory switch (See page <xref label="Seep01" href="RM00000472N009X"/>).</ptxt>
<figure>
<graphic graphicname="B199457E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>3 (M1) - 1 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch M1 pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>4 (M2) - 1 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch M2 pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>2 (MRY) - 1 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat memory switch SET pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003S3E00MX_04_0005" fin="false">OK</down>
<right ref="RM000003S3E00MX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0005" proc-id="RM23G0E___0000G5Z00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SEAT MEMORY SWITCH - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the K9*1 or J10*2 switch connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Disconnect the G63 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G63-25 (M1) - K9-3 (M1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-27 (M2) - K9-4 (M2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-26 (MM) - K9-2 (MRY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>K9-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-25 (M1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-27 (M2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-26 (MM) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G63-25 (M1) - J10-3 (M1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-27 (M2) - J10-4 (M2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-26 (MM) - J10-2 (MRY)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>J10-1 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-25 (M1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-27 (M2) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G63-26 (MM) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003S3E00MX_04_0017" fin="true">OK</down>
<right ref="RM000003S3E00MX_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0017">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO03DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0010">
<testtitle>GO TO PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000039RM035X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0013">
<testtitle>REPLACE SEAT MEMORY SWITCH<xref label="Seep01" href="RM00000472P00EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003S3E00MX_04_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>