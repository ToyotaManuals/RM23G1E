<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E8_T00NW" variety="T00NW">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LLI03DX" category="C" type-id="300BJ" name-id="AC87T-17" from="201207" to="201210">
<dtccode>B1441/41</dtccode>
<dtcname>Air Mix Damper Control Servo Motor Circuit (Passenger Side)</dtcname>
<subpara id="RM000002LLI03DX_01" type-id="60" category="03" proc-id="RM23G0E___0000HB100000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The damper servo sub-assembly (front passenger side air mix damper servo) sends pulse signals to inform the air conditioning amplifier assembly of the damper position. The air conditioning amplifier assembly activates the motor (normal or reverse) based on the signals to move the air mix damper (for front passenger side) to any position. As a result, the air mix damper (for front passenger side) varies the mixture of hot air from the heater core and cold air from the evaporator so that the temperature of the air blowing toward the front passenger side is as requested.</ptxt>
<atten4>
<ptxt>Confirm that no mechanical problem is present because this trouble code may be stored when either a damper link or damper is mechanically locked.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1441/41</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The air mix damper position does not change when the air conditioning amplifier assembly operates the damper servo sub-assembly (front passenger side air mix damper servo).</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Damper servo sub-assembly (front passenger side air mix damper servo)</ptxt>
</item>
<item>
<ptxt>Air conditioning harness assembly</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002LLI03DX_02" type-id="32" category="03" proc-id="RM23G0E___0000HB200000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E180810E50" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002LLI03DX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002LLI03DX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002LLI03DX_04_0001" proc-id="RM23G0E___0000HB300000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (FRONT PASSENGER SIDE AIR MIX DAMPER SERVO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the damper servo sub-assembly (front passenger side air mix damper servo) is functioning properly (See page <xref label="Seep01" href="RM000002LIV02CX"/>). </ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Air Mix Servo Targ Pulse (P)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Damper servo sub-assembly (front passenger side air mix damper servo) target pulse / Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>for LHD:</ptxt>
<list1 type="unordered">
<item>
<ptxt>112 or 127 (pulse): MAX COOL</ptxt>
</item>
<item>
<ptxt>21 or 31 (pulse): MAX HOT</ptxt>
</item>
</list1>
<ptxt>for RHD:</ptxt>
<list1 type="unordered">
<item>
<ptxt>107 (pulse): MAX COOL</ptxt>
</item>
<item>
<ptxt>26 (pulse): MAX HOT</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Mix Servo Actual Pulse (P)</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Damper servo sub-assembly (front passenger side air mix damper servo) actual pulse / Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>for LHD:</ptxt>
<list1 type="unordered">
<item>
<ptxt>112 or 127 (pulse): MAX COOL</ptxt>
</item>
<item>
<ptxt>21 or 31 (pulse): MAX HOT</ptxt>
</item>
</list1>
<ptxt>for RHD:</ptxt>
<list1 type="unordered">
<item>
<ptxt>107 (pulse): MAX COOL</ptxt>
</item>
<item>
<ptxt>26 (pulse): MAX HOT</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>The display is as specified in the normal condition column.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to problem symptoms table)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to the DTC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LLI03DX_04_0017" fin="true">A</down>
<right ref="RM000002LLI03DX_04_0015" fin="true">B</right>
<right ref="RM000002LLI03DX_04_0012" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0012" proc-id="RM23G0E___0000HB600000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (FRONT PASSENGER SIDE AIR MIX DAMPER SERVO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the Active Test, use the intelligent tester to generate a control command, and then check that the damper servo sub-assembly (front passenger side air mix damper servo) operates (See page <xref label="Seep01" href="RM000002LIV02CX"/>).</ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Air Mix Servo Targ Pulse (P)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Damper servo sub-assembly (front passenger side air mix damper servo) pulse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Damper servo sub-assembly (front passenger side air mix damper servo) operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLI03DX_04_0005" fin="true">OK</down>
<right ref="RM000002LLI03DX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0002" proc-id="RM23G0E___0000HB400000">
<testtitle>REPLACE DAMPER SERVO SUB-ASSEMBLY (FRONT PASSENGER SIDE AIR MIX DAMPER SERVO)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the damper servo sub-assembly (front passenger side air mix damper servo) (See page <xref label="Seep01" href="RM000003AXS02MX"/>).</ptxt>
<atten4>
<ptxt>Since the damper servo sub-assembly (front passenger side air mix damper servo) cannot be inspected while it is removed from the vehicle, replace the damper servo sub-assembly (front passenger side air mix damper servo) with a new or normally functioning one.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002LLI03DX_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0007" proc-id="RM23G0E___0000HB500000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002LIT02YX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002LIT02YX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1441 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLI03DX_04_0006" fin="true">OK</down>
<right ref="RM000002LLI03DX_04_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0013" proc-id="RM23G0E___0000HB700000">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the air conditioning amplifier assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM0000039R5016X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002LLI03DX_04_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0014" proc-id="RM23G0E___0000HB800000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002LIT02YX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000002LIT02YX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1441 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002LLI03DX_04_0016" fin="true">OK</down>
<right ref="RM000002LLI03DX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0015">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R5016X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0005">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R5016X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0009">
<testtitle>REPLACE AIR CONDITIONING HARNESS ASSEMBLY<xref label="Seep01" href="RM000003AXS02MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0006">
<testtitle>END (DAMPER SERVO SUB-ASSEMBLY [FRONT PASSENGER SIDE AIR MIX DAMPER SERVO] IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0016">
<testtitle>END (AIR CONDITIONING AMPLIFIER ASSEMBLY IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000002LLI03DX_04_0017">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000045E301IX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>