<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002D" variety="S002D">
<name>WIPER / WASHER</name>
<ttl id="12068_S002D_7B9GX_T00QL" variety="T00QL">
<name>HEADLIGHT CLEANER SWITCH</name>
<para id="RM000003SAI01KX" category="A" type-id="80001" name-id="WW5OO-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM000003SAI01KX_02" type-id="11" category="10" proc-id="RM23G0E___0000IY700000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003SAI01KX_01" type-id="01" category="01">
<s-1 id="RM000003SAI01KX_01_0017" proc-id="RM23G0E___00008CL00000">
<ptxt>REMOVE DOOR SCUFF PLATE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238732E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the door scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 4 clips, 10 claws and 2 guides and remove the door scuff plate.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000003SAI01KX_01_0018" proc-id="RM23G0E___00008CN00000">
<ptxt>REMOVE COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238734" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Detach the clip and claw and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003SAI01KX_01_0019" proc-id="RM23G0E___00008CO00000">
<ptxt>REMOVE NO. 1 INSTRUMENT PANEL UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238138" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>for LHD:</ptxt>
<s3>
<ptxt>Remove the screw.</ptxt>
</s3>
<s3>
<ptxt>Detach the 2 clips and 2 guides and remove the No. 1 instrument panel under cover.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B298554" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>for RHD:</ptxt>
<s3>
<ptxt>Remove the screw.</ptxt>
</s3>
<s3>
<ptxt>Detach the 3 clips and 2 guides and remove the No. 1 instrument panel under cover.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003SAI01KX_01_0020" proc-id="RM23G0E___00008CP00000">
<ptxt>REMOVE INSTRUMENT SIDE PANEL LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238205E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the instrument side panel.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 5 clips, claw and 3 guides and remove the instrument side panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003SAI01KX_01_0021" proc-id="RM23G0E___00008CR00000">
<ptxt>REMOVE INSTRUMENT CLUSTER FINISH PANEL ORNAMENT (for LHD)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238207E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the instrument cluster finish panel ornament.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a moulding remover, detach the 3 clips and remove the instrument cluster finish panel ornament.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003SAI01KX_01_0023" proc-id="RM23G0E___00008CT00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL FINISH PANEL ASSEMBLY (for LHD)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238214" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and remove the instrument panel finish panel.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003SAI01KX_01_0024" proc-id="RM23G0E___00008CS00000">
<ptxt>REMOVE INSTRUMENT PANEL FINISH PLATE GARNISH (for RHD)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238239" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect each connector and remove the instrument panel finish plate garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003SAI01KX_01_0025" proc-id="RM23G0E___00008CU00000">
<ptxt>REMOVE LOWER INSTRUMENT PANEL FINISH PANEL SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239280E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and open the cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="B243715" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>w/o Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts &lt;C&gt;.</ptxt>
</s3>
<s3>
<ptxt>Detach the 7 clips.</ptxt>
</s3>
<s3>
<ptxt>Disconnect each connector and each cable and remove the lower instrument panel finish panel.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B238216" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>w/ Knee Airbag:</ptxt>
<s3>
<ptxt>Remove the 2 bolts &lt;C&gt;.</ptxt>
</s3>
<s3>
<ptxt>Detach the 14 clips.</ptxt>
</s3>
<s3>
<ptxt>Disconnect each connector and each cable and remove the lower instrument panel finish panel.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000003SAI01KX_01_0015" proc-id="RM23G0E___0000IY500000">
<ptxt>REMOVE HEADLIGHT CLEANER SWITCH ASSEMBLY (for LHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the switch.</ptxt>
<figure>
<graphic graphicname="E202307" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000003SAI01KX_01_0016" proc-id="RM23G0E___0000IY600000">
<ptxt>REMOVE HEADLIGHT CLEANER SWITCH ASSEMBLY (for RHD)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the switch.</ptxt>
<figure>
<graphic graphicname="E202308" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>