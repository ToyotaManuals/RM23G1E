<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000003QTE01DX" category="C" type-id="803L6" name-id="BC8JJ-08" from="201210">
<dtccode>C1411</dtccode>
<dtcname>Rear Speed Sensor RH Performance</dtcname>
<dtccode>C1412</dtccode>
<dtcname>Rear Speed Sensor LH Performance</dtcname>
<subpara id="RM000003QTE01DX_01" type-id="60" category="03" proc-id="RM23G0E___0000A4400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1401 and C1402 (See page <xref label="Seep01" href="RM000000XI90SRX_01"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.03in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.93in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1411</ptxt>
<ptxt>C1412</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>When the vehicle is driven in reverse at a speed of 3 km/h (1.8 mph) or more and 3 of the wheel sensors detect the reverse signal, the other sensor detects a high frequency pulse 75 times while the ignition switch is ON.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 10 km/h (6 mph) or more, the vehicle speed sensor output drops by 50% for 5 seconds.</ptxt>
</item>
<item>
<ptxt>The lowest wheel speed is 10 km/h (6 mph) or more and the difference between the highest and lowest wheel speed values is 2 km/h (1 mph) or less for 15 seconds.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 30 km/h (18 mph) or more, one wheel direction is different from the other 3 wheels for 1 second.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 100 km/h (60 mph), a reverse signal is output for 1 second or more.</ptxt>
</item>
<item>
<ptxt>At a vehicle speed of 30 km/h (18 mph) or more, the rotation direction of the one of the wheels is not detected normally and the rotation direction of the other 3 wheels is not the same for 1 second.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Rear speed sensor RH/LH</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTC C1411 is for the rear speed sensor RH.</ptxt>
</item>
<item>
<ptxt>DTC C1412 is for the rear speed sensor LH.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000003QTE01DX_02" type-id="32" category="03" proc-id="RM23G0E___0000A4500001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1407 and C1408 (See page <xref label="Seep01" href="RM00000369306CX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000003QTE01DX_03" type-id="51" category="05" proc-id="RM23G0E___0000A4600001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Check the speed sensor signal after replacement (See page <xref label="Seep01" href="RM000000XHT0C0X"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003QTE01DX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003QTE01DX_04_0001" proc-id="RM23G0E___0000A4700001">
<testtitle>REPLACE REAR SPEED SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the rear speed sensor (See page <xref label="Seep01" href="RM000001B2G01FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003QTE01DX_04_0002" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003QTE01DX_04_0002">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>