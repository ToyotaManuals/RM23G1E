<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002G" variety="S002G">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002G_7B9HO_T00RC" variety="T00RC">
<name>FRONT BUMPER</name>
<para id="RM0000038JX01AX" category="A" type-id="80001" name-id="ET63I-02" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM0000038JX01AX_01" type-id="01" category="01">
<s-1 id="RM0000038JX01AX_01_0017" proc-id="RM23G0E___0000J9N00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL (for HID Headlight)</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0057" proc-id="RM23G0E___00003Q400000">
<ptxt>REMOVE UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 13 clips and upper radiator support seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038JX01AX_01_0034" proc-id="RM23G0E___00007CC00000">
<ptxt>REMOVE FRONT BUMPER SIDE SEAL LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236424" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 clips and remove the front bumper side seal.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038JX01AX_01_0035" proc-id="RM23G0E___0000J9Z00000">
<ptxt>REMOVE FRONT BUMPER SIDE SEAL RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0036" proc-id="RM23G0E___00007CB00000">
<ptxt>REMOVE RADIATOR GRILLE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236425E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the radiator grille.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 clips and 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 8 claws and remove the radiator grille.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000038JX01AX_01_0020" proc-id="RM23G0E___0000J2M00000">
<ptxt>REMOVE FRONT BUMPER COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the front bumper cover.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts labeled A and 2 bolts labeled B.</ptxt>
</s2>
<s2>
<ptxt>Remove the 6 screws and 6 clips.</ptxt>
<figure>
<graphic graphicname="B236374E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 12 claws.</ptxt>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Disconnect the 3 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/o Fog Light:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>w/o TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/ Headlight Cleaner System:</ptxt>
<ptxt>Disconnect the headlight cleaner hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the front bumper cover.</ptxt>
<figure>
<graphic graphicname="B236375E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0021" proc-id="RM23G0E___0000J9O00000">
<ptxt>REMOVE FRONT BUMPER ENERGY ABSORBER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236376" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the front bumper energy absorber.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0056" proc-id="RM23G0E___0000JA100000">
<ptxt>REMOVE LOWER FRONT BUMPER COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241006" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip, 5 bolts and lower front bumper cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0022" proc-id="RM23G0E___0000J2K00000">
<ptxt>REMOVE FRONT BUMPER REINFORCEMENT SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236377" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 6 nuts and front bumper reinforcement.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0031" proc-id="RM23G0E___0000J9W00000">
<ptxt>REMOVE NO. 2 FRONT BUMPER EXTENSION SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236380" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 4 bolts and No. 2 front bumper extension.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0032" proc-id="RM23G0E___0000J9X00000">
<ptxt>REMOVE NO. 2 FRONT BUMPER EXTENSION SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0025" proc-id="RM23G0E___0000J9Q00000">
<ptxt>REMOVE FRONT BUMPER BAR REINFORCEMENT LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236381" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws and front bumper bar reinforcement.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0026" proc-id="RM23G0E___0000J9R00000">
<ptxt>REMOVE FRONT BUMPER BAR REINFORCEMENT RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0029" proc-id="RM23G0E___0000J9U00000">
<ptxt>REMOVE HEADLIGHT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Halogen Headlight:</ptxt>
<ptxt>Remove the headlight (See page <xref label="Seep01" href="RM000003QVL00JX_01_0002"/>).</ptxt>
</s2>
<s2>
<ptxt>for HID Headlight:</ptxt>
<ptxt>Remove the headlight (See page <xref label="Seep02" href="RM000003S7200PX_01_0003"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0030" proc-id="RM23G0E___0000J9V00000">
<ptxt>REMOVE HEADLIGHT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0027" proc-id="RM23G0E___0000J9S00000">
<ptxt>REMOVE FRONT BUMPER SIDE SUPPORT LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236382E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Using a clip remover, detach 2 claws and remove the front bumper side support.</ptxt>
<atten3>
<ptxt>Tape the clip remover tip before use.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0028" proc-id="RM23G0E___0000J9T00000">
<ptxt>REMOVE FRONT BUMPER SIDE SUPPORT RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0033" proc-id="RM23G0E___0000J9Y00000">
<ptxt>REMOVE UPPER CENTER FRONT BUMPER RETAINER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236378" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 3 bolts and upper center front bumper retainer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0037" proc-id="RM23G0E___0000JA000000">
<ptxt>REMOVE FRONT BUMPER BRACKET SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236383" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and front bumper bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038JX01AX_01_0024" proc-id="RM23G0E___0000J9P00000">
<ptxt>REMOVE FRONT BUMPER BRACKET SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>