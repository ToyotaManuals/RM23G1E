<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004KML00QX" category="C" type-id="805BT" name-id="ESTEP-19" from="201210">
<dtccode>P1609</dtccode>
<dtcname>Air Fuel Ratio Too Rich</dtcname>
<subpara id="RM000004KML00QX_01" type-id="60" category="03" proc-id="RM23G0E___00002YQ00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored if the ECM detects that the air fuel ratio is too rich due to a malfunction in the fuel system, exhaust fuel addition injector system, mass air flow meter sub-assembly or manifold absolute pressure sensor.</ptxt>
<ptxt>Using the intelligent tester, the conditions present when the DTC was stored can be confirmed by referring to the freeze frame data. Freeze frame data records engine conditions when a malfunction occurs. This information can be useful when troubleshooting.</ptxt>
<table pgwide="1">
<title>P1609</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Both of the following conditions 1 and 2 are met for approximately 2 seconds: (1 trip detection logic)</ptxt>
<list1 type="ordered">
<item>
<ptxt>All of the following conditions are met:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Engine not being started</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor is active (warmed up)</ptxt>
</item>
<item>
<ptxt>Mass air flow meter sub-assembly is normal</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor is normal</ptxt>
</item>
<item>
<ptxt>PM automatic regeneration control has not been performed</ptxt>
</item>
</list2>
<item>
<ptxt>Air fuel ratio sensor output value exceeds the threshold</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Intake system clogged</ptxt>
</item>
<item>
<ptxt>EGR valve stuck closed</ptxt>
</item>
<item>
<ptxt>Deterioration of mass air flow meter (out of range)</ptxt>
</item>
<item>
<ptxt>Intake system hose disconnected or cracked</ptxt>
</item>
<item>
<ptxt>Turbocharger sub-assembly vane stuck fully open</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor</ptxt>
</item>
<item>
<ptxt>Atmospheric pressure sensor (built into ECM)</ptxt>
</item>
<item>
<ptxt>Suction control valve sticking</ptxt>
</item>
<item>
<ptxt>Fuel leak</ptxt>
</item>
<item>
<ptxt>Fuel pressure sensor (built into common rail assembly)</ptxt>
</item>
<item>
<ptxt>Engine coolant leak</ptxt>
</item>
<item>
<ptxt>Radiator assembly clogged</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor </ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor out of range or stuck</ptxt>
</item>
<item>
<ptxt>Exhaust fuel addition injector assembly stuck open</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000004KML00QX_02" type-id="51" category="05" proc-id="RM23G0E___00002YR00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>In contrast to normal malfunction diagnosis for components, circuits and systems, DTC P1609 is used to determine the malfunctioning area from the problem symptoms and freeze frame data when the user mentions problems such as black smoke or white smoke emitted.</ptxt>
<ptxt>As the DTC can be stored as a result of certain user actions, even if the DTC is output, if the customer makes no mention of problems, clear the DTC without performing any troubleshooting and return the vehicle to the customer.</ptxt>
</item>
<item>
<ptxt>If any other DTCs are output, perform troubleshooting for those DTCs first.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>When confirming the freeze frame data, be sure to check all 5 sets of freeze frame data.</ptxt>
</item>
<item>
<ptxt>The fourth set of freeze frame data is the data recorded when the DTC is stored.</ptxt>
<figure>
<graphic graphicname="A103809E46" width="2.775699831in" height="2.775699831in"/>
</figure>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000004KML00QX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004KML00QX_03_0001" proc-id="RM23G0E___00002YS00001">
<testtitle>READ OUTPUT DTC (RECORD STORED DTC AND FREEZE FRAME DATA (PROCEDURE 1))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC. </ptxt>
</test1>
<test1>
<ptxt>Record the stored DTC and freeze frame data. </ptxt>
<atten4>
<ptxt>This freeze frame data shows the actual engine conditions when air fuel ratio too rich trouble occurred.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KML00QX_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KML00QX_03_0002" proc-id="RM23G0E___00002YT00001">
<testtitle>CHECK ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P1609)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P1609 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P1609 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than DTC P1609 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KML00QX_03_0003" fin="true">A</down>
<right ref="RM000004KML00QX_03_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000004KML00QX_03_0003">
<testtitle>GO TO BLACK SMOKE EMITTED<xref label="Seep01" href="RM000000TIR0DNX"/>
</testtitle>
</testgrp>
<testgrp id="RM000004KML00QX_03_0004">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW050X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>