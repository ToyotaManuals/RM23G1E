<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0011" variety="S0011">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0011_7B93I_T00D6" variety="T00D6">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM0000044K400DX" category="C" type-id="305GC" name-id="CC3FL-02" from="201207" to="201210">
<dtccode>P1631</dtccode>
<dtcname>Communication Error from ECM to VSC</dtcname>
<subpara id="RM0000044K400DX_01" type-id="60" category="03" proc-id="RM23G0E___00007A600000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM is the only component that has a function to store and output DTCs in the dynamic radar cruise control system. Therefore, if the skid control ECU detects a communication error from the ECM, it sends a malfunction signal back to the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P1631</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>While the vehicle speed is 50 km/h (31 mph) or more with the cruise control switch on, the ECM continuously receives a logical error signal from the skid control ECU for 1 second.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Master cylinder solenoid (Skid control ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000044K400DX_02" type-id="51" category="05" proc-id="RM23G0E___00007A700000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When the ECM is replaced with a new one, initialization must be performed (See page <xref label="Seep01" href="RM0000044H500AX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000044K400DX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000044K400DX_03_0001" proc-id="RM23G0E___00007A800000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202EX"/>).</ptxt>
</test1>
<test1>
<ptxt>for 1KD-FTV:</ptxt>
<ptxt>Replace the ECM (See page <xref label="Seep02" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000044K400DX_03_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000044K400DX_03_0002" proc-id="RM23G0E___00007A900000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000044GY00AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the following to make sure the DTC detection conditions are met.</ptxt>
<atten4>
<ptxt>If the detection conditions are not met, the system cannot detect the malfunction. </ptxt>
</atten4>
<test2>
<ptxt>Drive the vehicle at a speed of 50 km/h (31 mph) or more.</ptxt>
</test2>
<test2>
<ptxt>Turn the cruise control switch on. </ptxt>
</test2>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000044GY00AX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC P1631 is not output.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044K400DX_03_0003" fin="true">A</down>
<right ref="RM0000044K400DX_03_0004" fin="true">B</right>
<right ref="RM0000044K400DX_03_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000044K400DX_03_0003">
<testtitle>END (ECM IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM0000044K400DX_03_0004">
<testtitle>REPLACE MASTER CYLINDER SOLENOID (SKID CONTROL ECU)<xref label="Seep01" href="RM00000171U01NX_01_0023"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044K400DX_03_0005">
<testtitle>REPLACE MASTER CYLINDER SOLENOID (SKID CONTROL ECU)<xref label="Seep01" href="RM00000171U01OX_01_0023"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>