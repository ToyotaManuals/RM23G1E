<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B995_T00IT" variety="T00IT">
<name>REAR DOOR SPEAKER</name>
<para id="RM000002MKB029X" category="A" type-id="80001" name-id="AV9NM-02" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM000002MKB029X_01" type-id="11" category="10" proc-id="RM23G0E___0000BZM00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000002MKB029X_02" type-id="01" category="01">
<s-1 id="RM000002MKB029X_02_0016" proc-id="RM23G0E___0000BZR00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work. (See page <xref label="Seep02" href="RM000003YMD00GX"/>)</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002MKB029X_02_0006" proc-id="RM23G0E___0000BZO00000">
<ptxt>REMOVE NO. 2 DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using moulding remover A, detach the 3 claws and remove the rear door inside handle bezel as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239600" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002MKB029X_02_0007" proc-id="RM23G0E___0000BZP00000">
<ptxt>REMOVE ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using moulding remover A, detach the 8 claws and remove the assist grip cover.</ptxt>
<figure>
<graphic graphicname="B239602" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002MKB029X_02_0010" proc-id="RM23G0E___0000BZQ00000">
<ptxt>REMOVE REAR DOOR TRIM BOARD SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="B239604" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Using a clip remover, detach the 9 clips.</ptxt>
</s2>
<s2>
<ptxt>Pull out the rear door trim board sub-assembly in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B239606" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Raise the rear door trim board sub-assembly to detach the 4 claws and remove the rear door trim board sub-assembly together with the rear door inner glass weatherstrip.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the rear door lock remote control cable assembly and rear door inside locking cable assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239612" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect each connector.</ptxt>
<figure>
<graphic graphicname="B239616E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 14 Speakers</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 9 Speakers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the claw and remove the clamp.</ptxt>
<figure>
<graphic graphicname="B241320" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002MKB029X_02_0021" proc-id="RM23G0E___0000BZT00000">
<ptxt>REMOVE REAR DOOR INNER GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a screwdriver, detach the 3 claws and remove the rear door inner glass weatherstrip from the rear door trim board sub-assembly as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B239615" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002MKB029X_02_0005" proc-id="RM23G0E___0000BZN00000">
<ptxt>REMOVE REAR SPEAKER SET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="B239136" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the rear speaker set.</ptxt>
<atten3>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000002MKB029X_02_0018" proc-id="RM23G0E___0000BZS00000">
<ptxt>REMOVE REAR NO. 2 SPEAKER ASSEMBLY (for 14 Speakers)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws and No. 2 speaker.</ptxt>
<figure>
<graphic graphicname="B239137" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not touch the cone of the speaker.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>