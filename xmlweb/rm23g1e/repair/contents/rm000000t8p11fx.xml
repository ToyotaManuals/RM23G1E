<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM000000T8P11FX" category="C" type-id="302I5" name-id="ESVKX-02" from="201207" to="201210">
<dtccode>P0560</dtccode>
<dtcname>System Voltage</dtcname>
<subpara id="RM000000T8P11FX_01" type-id="60" category="03" proc-id="RM23G0E___00001HY00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The battery supplies electricity to the ECM even when the ignition switch is off. This power allows the ECM to store data such as DTC history, freeze frame data and fuel trim values. If the battery voltage falls below a minimum level, the ECM memory is cleared and the ECM determines that there is a malfunction in the power supply circuit. When the engine is next started, the ECM illuminates the MIL and stores the DTC.</ptxt>
<table pgwide="1">
<title>P0560</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON for 3 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>The voltage at terminal BATT drops excessively for 3 seconds and part of the stored values in the ECM are initialized (1 trip detection logic).</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Open in backup power source circuit (to BATT terminal)</ptxt>
</item>
<item>
<ptxt>Battery</ptxt>
</item>
<item>
<ptxt>Battery terminals</ptxt>
</item>
<item>
<ptxt>EFI fuse</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0560</ptxt>
</entry>
<entry>
<ptxt>Battery Voltage*</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*: This is not the voltage of the BATT, it is the power supply voltage for the ECM.</ptxt>
<atten4>
<ptxt>If DTC P0560 is stored, the ECM does not store other DTCs or the data stored in the ECM is partly cleared.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T8P11FX_05" type-id="32" category="03" proc-id="RM23G0E___00001HZ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A221594E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000T8P11FX_06" type-id="51" category="05" proc-id="RM23G0E___00001I000000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK061X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>When the ignition switch is turned to ON, even though +B voltage is present, when the voltage at terminal BATT (power supply for maintaining memorized values in the ECM) drops excessively, this DTC is stored.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T8P11FX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000T8P11FX_07_0001" proc-id="RM23G0E___00001I100000">
<testtitle>CHECK ECM TERMINAL VOLTAGE (BATT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
<figure>
<graphic graphicname="A177861E22" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G57-23 (BATT) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000T8P11FX_07_0006" fin="false">OK</down>
<right ref="RM000000T8P11FX_07_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0006" proc-id="RM23G0E___00001I300000">
<testtitle>INSPECT BATTERY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the battery is not depleted.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Battery is not depleted.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8P11FX_07_0010" fin="false">OK</down>
<right ref="RM000000T8P11FX_07_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0010" proc-id="RM23G0E___00001I500000">
<testtitle>CHECK BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the battery terminals are not loose or corroded.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Battery terminals are not loose or corroded.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8P11FX_07_0012" fin="false">OK</down>
<right ref="RM000000T8P11FX_07_0011" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0012" proc-id="RM23G0E___00001I700000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z6X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 3 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0560 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000T8P11FX_07_0004" fin="false">A</down>
<right ref="RM000000T8P11FX_07_0014" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0004" proc-id="RM23G0E___00001I200000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000T8P11FX_07_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0007" proc-id="RM23G0E___00001I400000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000T8P11FX_07_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0015" proc-id="RM23G0E___00001I900000">
<testtitle>RECHARGE OR REPLACE BATTERY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Recharge or replace the battery.</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000T8P11FX_07_0013" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0011" proc-id="RM23G0E___00001I600000">
<testtitle>REPAIR OR REPLACE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the battery terminal.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000T8P11FX_07_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0013" proc-id="RM23G0E___00001I800000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z6X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON for 3 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000T8P11FX_07_0014" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000T8P11FX_07_0014">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>