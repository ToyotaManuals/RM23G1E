<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>2TR-FE FUEL</name>
<ttl id="12008_S000F_7B8ZA_T008Y" variety="T008Y">
<name>FUEL FILTER</name>
<para id="RM0000044IQ00PX" category="A" type-id="30019" name-id="FU6QC-03" from="201210">
<name>REPLACEMENT</name>
<subpara id="RM0000044IQ00PX_01" type-id="01" category="01">
<s-1 id="RM0000044IQ00PX_01_0007" proc-id="RM23G0E___00005TI00001">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Discharge the fuel system pressure (See page <xref label="Seep01" href="RM0000028RU03RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0008" proc-id="RM23G0E___00005TJ00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0001" proc-id="RM23G0E___00005TC00001">
<ptxt>DISCONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the hose from the clamp.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the fuel hose (See page <xref label="Seep01" href="RM0000028RU03RX"/>).</ptxt>
<figure>
<graphic graphicname="A223397" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0002" proc-id="RM23G0E___00005TD00001">
<ptxt>DISCONNECT FUEL MAIN TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel main tube (See page <xref label="Seep01" href="RM0000028RU03RX"/>).</ptxt>
<figure>
<graphic graphicname="A223398" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0003" proc-id="RM23G0E___00005TE00001">
<ptxt>REMOVE FUEL FILTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and fuel filter.</ptxt>
<figure>
<graphic graphicname="A223399" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0004" proc-id="RM23G0E___00005TF00001">
<ptxt>INSTALL FUEL FILTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel filter with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>7.0</t-value1>
<t-value2>71</t-value2>
<t-value3>62</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0005" proc-id="RM23G0E___00005TG00001">
<ptxt>CONNECT FUEL MAIN TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel main tube (See page <xref label="Seep01" href="RM0000028RU03RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0006" proc-id="RM23G0E___00005TH00001">
<ptxt>CONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel hose (See page <xref label="Seep01" href="RM0000028RU03RX"/>).</ptxt>
</s2>
<s2>
<ptxt>Connect the hose to the clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0009" proc-id="RM23G0E___00005TK00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000044IQ00PX_01_0010" proc-id="RM23G0E___00005AQ00001">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Make sure that there are no fuel leaks after performing maintenance on the fuel system.</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch to ON and turn the intelligent tester on.</ptxt>
<atten3>
<ptxt>Do not start the engine.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Fuel Pump / Speed.</ptxt>
</s3>
<s3>
<ptxt>Check that there are no leaks from the fuel system.</ptxt>
<ptxt>If there are fuel leaks, repair or replace parts as necessary.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch off.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the intelligent tester from the DLC3.</ptxt>
</s3>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>