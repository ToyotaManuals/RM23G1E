<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8YU_T008I" variety="T008I">
<name>FUEL TANK (for 5 Door)</name>
<para id="RM0000045BK008X" category="A" type-id="30014" name-id="FU8CO-02" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM0000045BK008X_01" type-id="01" category="01">
<s-1 id="RM0000045BK008X_01_0001" proc-id="RM23G0E___00005LA00000">
<ptxt>INSTALL FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the fuel tank side mark with the hose side mark.</ptxt>
<figure>
<graphic graphicname="A220824E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tank Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hose Side Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the fuel tank to filler pipe hose to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0002" proc-id="RM23G0E___00005LB00000">
<ptxt>INSTALL NO. 3 FUEL TANK PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 3 fuel tank protector and attach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Install the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0024" proc-id="RM23G0E___00005LQ00000">
<ptxt>INSTALL FUEL TANK VENT TUBE ASSEMBLY (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of gasoline or grease to a new gasket and install the gasket to the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Align the protrusion of the fuel tank vent tube with the groove of the fuel tank.</ptxt>
<figure>
<graphic graphicname="A220825E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel tank vent tube to the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Put a new retainer on the fuel tank. While holding the fuel tank vent tube, tighten the retainer one complete turn by hand.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Start Mark (Fuel Tank Side)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Start Mark (Retainer Side)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure the start marks on the retainer and fuel tank are aligned and then tighten the retainer.</ptxt>
<figure>
<graphic graphicname="A220826E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</atten4>
</s2>
<s2>
<ptxt>Set SST on the retainer.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A243483E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Hold the fuel tank vent tube assembly upright by hand to ensure that the gasket is not moved out of position.</ptxt>
</item>
<item>
<ptxt>Engage the claws of SST securely with the retainer holes to secure SST.</ptxt>
</item>
<item>
<ptxt>Install SST while pressing the claws of SST against the retainer (toward the center of SST).</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Using SST, tighten the retainer until the mark on the retainer is within range A on the fuel tank as shown in the illustration.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A243481E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tank Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer Side Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Fit the tips of SST onto the ribs of the retainer.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0028" proc-id="RM23G0E___00005IQ00000">
<ptxt>INSTALL FUEL SUCTION WITH PUMP AND GAUGE TUBE ASSEMBLY (for Double Tank Type)
</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Apply a light coat of gasoline or grease to a new gasket and install the gasket to the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Align the protrusion of the fuel suction with pump and gauge tube with the groove of the fuel tank.</ptxt>
<figure>
<graphic graphicname="A220825E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protrusion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Groove</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel suction with pump and gauge tube to the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Put a new retainer on the fuel tank. While holding the fuel suction with pump and gauge tube, tighten the retainer one complete turn by hand.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Start Mark (Fuel Tank Side)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Start Mark (Retainer Side)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Make sure the start marks on the retainer and fuel tank are aligned and then tighten the retainer.</ptxt>
<figure>
<graphic graphicname="A220826E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</atten4>
</s2>
<s2>
<ptxt>Set SST on the retainer.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A243483E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Hold the fuel suction with pump and gauge tube assembly upright by hand to ensure that the gasket is not moved out of position.</ptxt>
</item>
<item>
<ptxt>Engage the claws of SST securely with the retainer holes to secure SST.</ptxt>
</item>
<item>
<ptxt>Install SST while pressing the claws of SST against the retainer (toward the center of SST).</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Using SST, tighten the retainer until the mark on the retainer is within range A on the fuel tank as shown in the illustration.</ptxt>
<sst>
<sstitem>
<s-number>09808-14030</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A243481E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tank Side Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer Side Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Fit the tips of SST onto the ribs of the retainer.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000045BK008X_01_0004" proc-id="RM23G0E___00005LC00000">
<ptxt>INSTALL FUEL RETURN TUBE SUB-ASSEMBLY AND FUEL TANK MAIN TUBE SUB-ASSEMBLY (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel return tube and fuel tank main tube with the 2 fuel tube joint clips.</ptxt>
<figure>
<graphic graphicname="A223113" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check that there are no scratches or foreign objects on the connecting parts.</ptxt>
<figure>
<graphic graphicname="A118171E07" width="2.775699831in" height="3.779676365in"/>
</figure>
</item>
<item>
<ptxt>Check that the fuel tube joints are inserted securely.</ptxt>
</item>
<item>
<ptxt>Check that the fuel tube joint clips are on the collars of the fuel tube joints.</ptxt>
</item>
<item>
<ptxt>After installing the fuel tube joint clips, check that the fuel tube joints cannot be pulled off.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel return tube and fuel tank main tube to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0006" proc-id="RM23G0E___00005LE00000">
<ptxt>INSTALL FUEL TANK MAIN TUBE SUB-ASSEMBLY, FUEL RETURN TUBE SUB-ASSEMBLY AND NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel tank main tube, fuel return tube and No. 2 fuel main tube with the 3 fuel tube joint clips.</ptxt>
<figure>
<graphic graphicname="A223115" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check that there are no scratches or foreign objects on the connecting parts.</ptxt>
<figure>
<graphic graphicname="A118171E07" width="2.775699831in" height="3.779676365in"/>
</figure>
</item>
<item>
<ptxt>Check that the fuel tube joints are inserted securely.</ptxt>
</item>
<item>
<ptxt>Check that the fuel tube joint clips are on the collars of the fuel tube joints.</ptxt>
</item>
<item>
<ptxt>After installing the fuel tube joint clips, check that the fuel tube joints cannot be pulled off.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel tank main tube to the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Install the fuel return tube to the fuel tank and attach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Install the No. 2 fuel main tube to the fuel tank and attach the clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0005" proc-id="RM23G0E___00005LD00000">
<ptxt>INSTALL NO. 4 FUEL MAIN TUBE SUB-ASSEMBLY AND FUEL SUCTION TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 4 fuel main tube and fuel suction tube with the 2 fuel tube joint clips.</ptxt>
<figure>
<graphic graphicname="A223242" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Check that there are no scratches or foreign objects on the connecting parts.</ptxt>
<figure>
<graphic graphicname="A118171E07" width="2.775699831in" height="3.779676365in"/>
</figure>
</item>
<item>
<ptxt>Check that the fuel tube joints are inserted securely.</ptxt>
</item>
<item>
<ptxt>Check that the fuel tube joint clips are on the collars of the fuel tube joints.</ptxt>
</item>
<item>
<ptxt>After installing the fuel tube joint clips, check that the fuel tube joints cannot be pulled off.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>O-Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Tube Joint Clip</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the fuel suction tube to the fuel tank and attach the clamp.</ptxt>
</s2>
<s2>
<ptxt>Install the No. 4 fuel main tube to the fuel tank and attach the clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0007" proc-id="RM23G0E___00005LF00000">
<ptxt>INSTALL FUEL TANK CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 3 new fuel tank cushions to the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0008" proc-id="RM23G0E___00005LG00000">
<ptxt>INSTALL FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the fuel tank on a transmission jack and lift up the transmission jack.</ptxt>
<atten3>
<ptxt>Do not allow the fuel tank to contact the vehicle, especially the differential.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the 2 fuel tank bands with the 2 pins and 2 clips.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 fuel tank bands with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0009" proc-id="RM23G0E___00005LH00000">
<ptxt>CONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank to filler pipe hose to the filler pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0010" proc-id="RM23G0E___00005LI00000">
<ptxt>CONNECT FUEL TANK BREATHER TUBE SUB-ASSEMBLY (for Single Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank breather tube (See page <xref label="Seep01" href="RM0000028RU03AX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0029">
<ptxt>CONNECT FUEL CUT OFF TUBE (for Single Tank Type)</ptxt>
</s-1>
<s-1 id="RM0000045BK008X_01_0012" proc-id="RM23G0E___00005LJ00000">
<ptxt>CONNECT FUEL TANK BREATHER TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank breather tube (See page <xref label="Seep01" href="RM0000028RU03AX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0013" proc-id="RM23G0E___00005LK00000">
<ptxt>CONNECT FUEL RETURN TUBE SUB-ASSEMBLY AND NO. 2 FUEL MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel return tube and No. 2 fuel main tube (See page <xref label="Seep01" href="RM0000028RU03AX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0014" proc-id="RM23G0E___00005LL00000">
<ptxt>CONNECT FUEL CUT OFF TUBE (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel cut off tube (See page <xref label="Seep01" href="RM0000028RU03AX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0015" proc-id="RM23G0E___00005LM00000">
<ptxt>CONNECT NO. 4 FUEL MAIN TUBE SUB-ASSEMBLY AND FUEL SUCTION TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 4 fuel main tube and fuel suction tube (See page <xref label="Seep01" href="RM0000028RU03AX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0030">
<ptxt>CONNECT FUEL RETURN TUBE SUB-ASSEMBLY (for Single Tank Type)</ptxt>
</s-1>
<s-1 id="RM0000045BK008X_01_0031">
<ptxt>CONNECT FUEL TANK MAIN TUBE SUB-ASSEMBLY (for Single Tank Type)</ptxt>
</s-1>
<s-1 id="RM0000045BK008X_01_0032">
<ptxt>CONNECT FUEL TANK MAIN TUBE SUB-ASSEMBLY (for Double Tank Type)</ptxt>
</s-1>
<s-1 id="RM0000045BK008X_01_0019" proc-id="RM23G0E___00005LN00000">
<ptxt>INSTALL NO. 1 FUEL TANK PROTECTOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 fuel tank protector with the 6 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>for Half Cover Type:</ptxt>
<ptxt>Install the No. 1 fuel tank protector with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0020" proc-id="RM23G0E___00005LO00000">
<ptxt>INSTALL REAR FLOOR SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel pump and fuel sender gauge connector.</ptxt>
</s2>
<s2>
<ptxt>Install the rear floor service hole cover with the 3 screws.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0027" proc-id="RM23G0E___00005LT00000">
<ptxt>INSTALL REAR SEAT ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 60/40 Split Double-folding Seat Type LH Side:</ptxt>
<ptxt>Install the rear seat assembly LH (See page <xref label="Seep01" href="RM00000468N00GX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 60/40 Split Slide Walk-in Seat Type LH Side:</ptxt>
<ptxt>Install the rear seat assembly LH (See page <xref label="Seep02" href="RM00000468800JX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0021" proc-id="RM23G0E___00005LP00000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0025" proc-id="RM23G0E___00005LR00000">
<ptxt>BLEED AIR FROM FUEL SYSTEM</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM000002SY802NX_01_0002"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
<s-1 id="RM0000045BK008X_01_0026" proc-id="RM23G0E___00005LS00000">
<ptxt>INSPECT FOR FUEL LEAK</ptxt>
<content1 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>w/ DPF (See page <xref label="Seep01" href="RM000002SY802OX_01_0001"/>)</ptxt>
</item>
<item>
<ptxt>w/o DPF (See page <xref label="Seep02" href="RM000002SY802NX_01_0001"/>)</ptxt>
</item>
</list1>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>