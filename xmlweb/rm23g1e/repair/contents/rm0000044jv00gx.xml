<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0011" variety="S0011">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0011_7B93I_T00D6" variety="T00D6">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM0000044JV00GX" category="J" type-id="300WL" name-id="CC2J4-04" from="201210">
<dtccode/>
<dtcname>Cruise Main Indicator Light Circuit</dtcname>
<subpara id="RM0000044JV00GX_01" type-id="60" category="03" proc-id="RM23G0E___000078000001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the cruise control switch is on, the cruise control indicator light and RADAR READY indicator turn on. These indicators indicate the control condition (the presence or absence of a vehicle in front, vehicle-to-vehicle distance and set vehicle speed) and fail-safe state through the CAN communication system. When there is a malfunction, the master warning light and cruise control indicator light turn on and vehicle-to-vehicle distance information is displayed on the combination meter assembly as the alarm buzzer sounds. Items such as "CLEAN RADAR SENSOR", "CRUISE NOT AVAILABLE", "CHECK CRUISE SYSTEM" and "RADAR READY" are displayed on the multi-information display in the combination meter assembly when the ECM detects signals from each sensor and actuator and sends them to the combination meter assembly via the CAN.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the vehicle in front in the same lane significantly decreases in speed or another vehicle moves in front of your vehicle, adequate deceleration cannot be applied and the vehicle-to-vehicle distance shortens. At this time, the system sounds the buzzer to warn the driver.</ptxt>
</item>
<item>
<ptxt>Each indicator in the dynamic radar cruise control uses CAN communication. Therefore, if there are any malfunctions in this circuit, check for DTCs in the CAN communication system before troubleshooting this circuit.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM0000044JV00GX_02" type-id="51" category="05" proc-id="RM23G0E___000078100001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When the ECM is replaced with a new one, initialization must be performed (See page <xref label="Seep01" href="RM0000044H500AX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000044JV00GX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000044JV00GX_03_0001" proc-id="RM23G0E___000078200001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the item below in the Active Test, and then check that the cruise control indicator light illuminates (See page <xref label="Seep01" href="RM0000044H200AX"/>).</ptxt>
<table pgwide="1">
<title>Combination Meter</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Indicat. Rader Cruise</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control indicator light</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Cruise control indicator light turns on/off.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000044JV00GX_03_0004" fin="false">OK</down>
<right ref="RM0000044JV00GX_03_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044JV00GX_03_0004" proc-id="RM23G0E___000078300001">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the cruise control switch is functioning properly (See page <xref label="Seep01" href="RM0000044H200AX"/>).</ptxt>
<table pgwide="1">
<title>Radar Cruise</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Main SW M-CPU</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Cruise control switch (Main CPU) signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Cruise control switch on</ptxt>
<ptxt>OFF: Cruise control switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On screen, each item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1KD-FTV)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044JV00GX_03_0002" fin="true">A</down>
<right ref="RM0000044JV00GX_03_0005" fin="true">B</right>
<right ref="RM0000044JV00GX_03_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000044JV00GX_03_0002">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000044GV00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044JV00GX_03_0003">
<testtitle>GO TO METER / GAUGE SYSTEM<xref label="Seep01" href="RM000002Z4L03KX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044JV00GX_03_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044JV00GX_03_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001YX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>