<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12048_S001U" variety="S001U">
<name>1GR-FE BATTERY / CHARGING</name>
<ttl id="12048_S001U_7B9AH_T00K5" variety="T00K5">
<name>GENERATOR</name>
<para id="RM000001ONP03AX" category="A" type-id="80001" name-id="BH12H-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM000001ONP03AX_01" type-id="01" category="01">
<s-1 id="RM000001ONP03AX_01_0027" proc-id="RM23G0E___0000D6L00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000001ONP03AX_01_0052">
<ptxt>DISCONNECT CABLE FROM POSITIVE BATTERY TERMINAL</ptxt>
</s-1>
<s-1 id="RM000001ONP03AX_01_0057" proc-id="RM23G0E___00003QD00000">
<ptxt>REMOVE BATTERY HOLD DOWN CLAMP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Loosen the 2 nuts and remove the battery hold down clamp.</ptxt>
<figure>
<graphic graphicname="A223416" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000001ONP03AX_01_0058">
<ptxt>REMOVE BATTERY</ptxt>
</s-1>
<s-1 id="RM000001ONP03AX_01_0054">
<ptxt>REMOVE BATTERY TRAY</ptxt>
</s-1>
<s-1 id="RM000001ONP03AX_01_0030" proc-id="RM23G0E___00003PV00000">
<ptxt>REMOVE V-BANK COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A229925E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Raise the front of the V-bank cover to detach the 2 pins. Then remove the 2 V-bank cover hooks from the bracket, and remove the V-bank cover.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP03AX_01_0045" proc-id="RM23G0E___00003VJ00000">
<ptxt>REMOVE FAN AND GENERATOR V BELT
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>While turning the belt tensioner counterclockwise, align the service hole for the belt tensioner and the belt tensioner fixing position, and then insert a bar of φ 6 mm (0.236 in.) into the service hole to fix the belt tensioner in place.</ptxt>
<figure>
<graphic graphicname="A217571" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>The pulley bolt for the belt tensioner has a left-hand thread.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Remove the V belt.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP03AX_01_0055" proc-id="RM23G0E___00003QN00000">
<ptxt>REMOVE NO. 2 IDLER PULLEY SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for Integrated Type:</ptxt>
<ptxt>Remove the bolt and No. 2 idler pulley.</ptxt>
<figure>
<graphic graphicname="A223415" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for Separate Type:</ptxt>
<ptxt>Remove the bolt, No. 2 idler pulley cover plate, No. 2 idler pulley and idler pulley cover plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001ONP03AX_01_0051" proc-id="RM23G0E___00003Q700000">
<ptxt>REMOVE WIRING HARNESS CLAMP BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp.</ptxt>
<figure>
<graphic graphicname="A222903" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and wiring harness clamp bracket.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001ONP03AX_01_0056" proc-id="RM23G0E___00003Q800000">
<ptxt>REMOVE NO. 2 EXHAUST MANIFOLD HEAT INSULATOR
</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A221484" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the 3 bolts and heat insulator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000001ONP03AX_01_0004" proc-id="RM23G0E___00003Q300000">
<ptxt>REMOVE GENERATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Open the terminal cap.</ptxt>
<figure>
<graphic graphicname="A222881" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the wire harness from terminal B.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the generator connector from the generator assembly.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and disconnect the wire harness.</ptxt>
<figure>
<graphic graphicname="A222884" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the wire harness clamp.</ptxt>
<figure>
<graphic graphicname="A222882" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and disconnect the generator bracket.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and generator assembly.</ptxt>
<figure>
<graphic graphicname="A222883" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and generator bracket.</ptxt>
<figure>
<graphic graphicname="A222885" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>