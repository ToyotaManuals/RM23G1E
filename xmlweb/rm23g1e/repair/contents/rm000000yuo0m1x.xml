<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000000YUO0M1X" category="C" type-id="304LR" name-id="BC8KD-25" from="201210">
<dtccode>U0073</dtccode>
<dtcname>Control Module Communication Bus OFF</dtcname>
<dtccode>U0114</dtccode>
<dtcname>Lost Communication with 4WD Control ECU</dtcname>
<dtccode>U0124</dtccode>
<dtcname>Lost Communication with Lateral Acceleration Sensor Module</dtcname>
<subpara id="RM000000YUO0M1X_01" type-id="60" category="03" proc-id="RM23G0E___0000A2800001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU receives signals from the acceleration sensor and the four wheel drive control ECU via the CAN communication system.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.36in"/>
<colspec colname="COL3" colwidth="2.66in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0073</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, after the output of data from the skid control ECU is completed, the sending operation continues for 5 seconds or more.</ptxt>
</item>
<item>
<ptxt>The condition that the bus OFF state occurs once or more within 0.1 seconds occurs 10 times in succession (sent signals cannot be received).</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, a delay in receiving data from the yaw rate and acceleration sensor and steering angle sensor continues for 1 second or more.</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, the following occurs 10 times in succession.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>The condition that a delay in receiving data from the yaw rate and acceleration sensor and steering angle sensor occurs more than once within 5 seconds.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0114</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN signal from the four wheel drive control ECU is lost or malfunctioning for 1.2 seconds.</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0124</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, data from the acceleration sensor cannot be received for 1 second or more.</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, the following occurs 10 times in succession.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>The condition that data from the acceleration sensor cannot be received occurs once or more within 5 seconds.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000YUO0M1X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000YUO0M1X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YUO0M1X_04_0002" proc-id="RM23G0E___0000A2900001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000XHV0GFX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.14in"/>
<colspec colname="COLSPEC0" colwidth="1.79in"/>
<colspec colname="COL2" colwidth="1.20in"/>
<thead>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COLSPEC0" valign="middle">
<ptxt>DTC not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for LHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC output</ptxt>
</entry>
<entry valign="middle">
<ptxt>for RHD without Entry and Start System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YUO0M1X_04_0010" fin="true">A</down>
<right ref="RM000000YUO0M1X_04_0005" fin="true">B</right>
<right ref="RM000000YUO0M1X_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YUO0M1X_04_0005">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO087X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUO0M1X_04_0010">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUO0M1X_04_0007">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO089X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>