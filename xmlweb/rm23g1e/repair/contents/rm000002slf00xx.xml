<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12011_S000R" variety="S000R">
<name>1KD-FTV COOLING</name>
<ttl id="12011_S000R_7B91O_T00BC" variety="T00BC">
<name>COOLANT (w/ DPF)</name>
<para id="RM000002SLF00XX" category="A" type-id="30019" name-id="CO4QR-02" from="201207">
<name>REPLACEMENT</name>
<subpara id="RM000002SLF00XX_01" type-id="01" category="01">
<s-1 id="RM000002SLF00XX_01_0013" proc-id="RM23G0E___00004PR00000">
<ptxt>REMOVE FRONT BUMPER LOWER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip, 5 bolts and front bumper lower cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002SLF00XX_01_0014" proc-id="RM23G0E___00004PE00000">
<ptxt>REMOVE NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and No. 1 engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002SLF00XX_01_0001" proc-id="RM23G0E___000032G00000">
<ptxt>DRAIN ENGINE COOLANT</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Loosen the radiator drain cock plug.</ptxt>
<atten4>
<ptxt>Collect the coolant in a container and dispose of it according to the regulations in your area.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A218364" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Drain the coolant by removing the reservoir cap and, using a wrench, remove the vent plug.</ptxt>
</s2>
<s2>
<ptxt>Loosen the cylinder block drain cock plug.</ptxt>
<figure>
<graphic graphicname="A245857E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Reservoir</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vent Plug</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiator Drain Cock Plug</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cylinder Block Drain Cock Plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000002SLF00XX_01_0002" proc-id="RM23G0E___000032400000">
<ptxt>ADD ENGINE COOLANT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Tighten the radiator drain cock plug by hand.</ptxt>
</s2>
<s2>
<ptxt>Tighten the cylinder block drain cock plug.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Fill the radiator with TOYOTA Super Long Life Coolant (SLLC) to the B line of the reservoir tank.</ptxt>
<spec>
<title>Standard Capacity</title>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>for Automatic Transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>14.9 liters (15.7 US qts, 13.1 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>13.1 liters (13.8 US qts, 11.5 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>for Manual Transmission</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>w/ Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>15.0 liters (15.8 US qts, 13.2 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>w/o Rear Heater</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>13.2 liters (13.9 US qts, 11.6 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>TOYOTA vehicles are filled with TOYOTA SLLC at the factory. In order to avoid damage to the engine cooling system and other technical problems, only use TOYOTA SLLC or similar high quality ethylene glycol based non-silicate, non-amine, non-nitrite, non-borate coolant with long-life hybrid organic acid technology (coolant with long-life hybrid organic acid technology consists of a combination of low phosphates and organic acids).</ptxt>
</item>
<item>
<ptxt>Please contact your TOYOTA dealer for further details.</ptxt>
</item>
<item>
<ptxt>for Cold Area Specification Vehicles:</ptxt>
<ptxt>Please contact any authorized TOYOTA dealer or repairer or another duly qualified and equipped professional for further details.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<ptxt>Never use water as a substitute for engine coolant.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Press the No. 1 and No. 2 inlet and No. 1 and No. 2 outlet radiator hoses several times by hand, and then check the level of the coolant.</ptxt>
<ptxt>If the coolant level drops below the B line, add TOYOTA SLLC to the B line.</ptxt>
</s2>
<s2>
<ptxt>Install the radiator reservoir cap.</ptxt>
</s2>
<s2>
<ptxt>Using a wrench, install the vent plug.</ptxt>
<torque>
<torqueitem>
<t-value1>2.0</t-value1>
<t-value2>20</t-value2>
<t-value3>18</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Bleed air from the cooling system.</ptxt>
<s3>
<ptxt>Warm up the engine until the thermostat opens. While the thermostat is open, circulate the coolant for several minutes.</ptxt>
</s3>
<s3>
<ptxt>Maintain the engine speed at a speed between 2500 and 3000 rpm.</ptxt>
</s3>
<s3>
<ptxt>Press the inlet and outlet radiator hoses several times by hand to bleed air.</ptxt>
<atten2>
<ptxt>When pressing the radiator hoses:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Wear protective gloves.</ptxt>
</item>
<item>
<ptxt>Be careful as the radiator hoses are hot.</ptxt>
</item>
<item>
<ptxt>Keep your hands away from the radiator fan.</ptxt>
</item>
</list1>
</atten2>
</s3>
<s3>
<ptxt>Stop the engine and wait until the coolant cools down to ambient temperature.</ptxt>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
</s3>
</s2>
<s2>
<ptxt>After the coolant cools down, check that the coolant level is at the FULL line.</ptxt>
<ptxt>If the coolant level is below the FULL line, add TOYOTA SLLC to the FULL line.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002SLF00XX_01_0006" proc-id="RM23G0E___00001CD00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Before each inspection, turn the A/C switch off.</ptxt>
</atten3>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leaks. If no external leaks are found, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002SLF00XX_01_0015" proc-id="RM23G0E___00004O800000">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 engine under cover with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000002SLF00XX_01_0016" proc-id="RM23G0E___00004O900000">
<ptxt>INSTALL FRONT BUMPER LOWER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front bumper lower cover with the clip and 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>