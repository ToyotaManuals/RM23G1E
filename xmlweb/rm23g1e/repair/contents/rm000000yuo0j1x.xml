<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM000000YUO0J1X" category="C" type-id="304LR" name-id="BCB78-02" from="201207" to="201210">
<dtccode>U0073</dtccode>
<dtcname>Control Module Communication Bus OFF</dtcname>
<dtccode>U0100</dtccode>
<dtcname>Lost Communication with ECM / PCM</dtcname>
<dtccode>U0114</dtccode>
<dtcname>Lost Communication with 4WD Control ECU</dtcname>
<dtccode>U0123</dtccode>
<dtcname>Lost Communication with Yaw Rate Sensor Module</dtcname>
<dtccode>U0124</dtccode>
<dtcname>Lost Communication with Lateral Acceleration Sensor Module</dtcname>
<dtccode>U0126</dtccode>
<dtcname>Lost Communication with Steering Angle Sensor Module</dtcname>
<subpara id="RM000000YUO0J1X_05" type-id="60" category="03" proc-id="RM23G0E___0000A9000000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.31in"/>
<colspec colname="COL2" colwidth="3.41in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Conditions</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0073</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, after the output of data from the skid control ECU is completed, the sending operation continues for 5 seconds or more.</ptxt>
</item>
<item>
<ptxt>The condition that bus OFF state occurs once or more within 0.1 seconds occurs 10 times in succession (Sent signals cannot be received).</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, a delay in receiving data from the yaw rate and acceleration sensor and steering angle sensor continues for 1 second or more.</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, the following occurs 10 times in succession.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>The condition that a delay in receiving data from the yaw rate and acceleration sensor and steering angle sensor occurs more than once within 5 seconds.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0100</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V and the vehicle speed 15 km/h (9 mph) or more, data cannot be sent to the ECM for 2 seconds or more.</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V and the vehicle speed 15 km/h (9 mph) or more, data cannot be received to the ECM for 2 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0114</ptxt>
</entry>
<entry valign="middle">
<ptxt>The CAN signal from the four wheel drive control ECU is lost or malfunctioning for 1.2 seconds.</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0123</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, data from the yaw rate sensor cannot be received for 1 second or more.</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, the following occurs 10 times in succession.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>The condition that data from the yaw rate sensor cannot be received occurs once or more within 5 seconds.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0124</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, data from the acceleration sensor cannot be received for 1 second or more.</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, the following occurs 10 times in succession.</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>The condition that data from the acceleration sensor cannot be received occurs once or more within 5 seconds.</ptxt>
</item>
</list2>
</list1>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0126</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, data from the steering angle sensor cannot be received for 1 second or more</ptxt>
</item>
<item>
<ptxt>When the IG1 terminal voltage is between 10 and 17.4 V, the following occurs 10 times in succession.</ptxt>
</item>
<item>
<ptxt>The condition that data from the steering angle sensor cannot be received occurs once or more within 5 seconds.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000YUO0J1X_06" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000YUO0J1X_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000YUO0J1X_07_0011" proc-id="RM23G0E___0000A9100000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC(s) (See page <xref label="Seep01" href="RM0000046KV00IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC(s) is output (See page <xref label="Seep02" href="RM0000046KV00IX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.04in"/>
<colspec colname="COL2" colwidth="1.09in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000YUO0J1X_07_0013" fin="true">A</down>
<right ref="RM000000YUO0J1X_07_0012" fin="true">B</right>
<right ref="RM000000YUO0J1X_07_0014" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000YUO0J1X_07_0012">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO07BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUO0J1X_07_0013">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000YUO0J1X_07_0014">
<testtitle>GO TO CAN COMMUNICATION SYSTEM (HOW TO PROCEED WITH TROUBLESHOOTING)<xref label="Seep01" href="RM000001RSO07DX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>