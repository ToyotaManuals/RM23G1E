<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12067_S002C" variety="S002C">
<name>MIRROR (EXT)</name>
<ttl id="12067_S002C_7B9GE_T00Q2" variety="T00Q2">
<name>OUTER REAR VIEW MIRROR</name>
<para id="RM000003A0N01VX" category="A" type-id="8000E" name-id="MX2P9-04" from="201207">
<name>REASSEMBLY</name>
<subpara id="RM000003A0N01VX_02" type-id="11" category="10" proc-id="RM23G0E___0000IT100000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>w/ Side Monitor System:</ptxt>
<ptxt>When installing only the side television camera assembly, refer to TELEVISION CAMERA (for Side) (See page <xref label="Seep01" href="RM000003A0N01UX"/>).</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000003A0N01VX_01" type-id="01" category="01">
<s-1 id="RM000003A0N01VX_01_0005" proc-id="RM23G0E___0000ISZ00000">
<ptxt>INSTALL SIDE TELEVISION CAMERA ASSEMBLY (w/ Side Monitor System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the side television camera assembly (See page <xref label="Seep01" href="RM000003A0N01UX_01_0003"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A0N01VX_01_0007" proc-id="RM23G0E___0000IT000000">
<ptxt>INSTALL OUTER MIRROR RETRACTOR LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 6 claws to install the 2 support springs, and then install the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Attach the claw to install the wire clip.</ptxt>
</s2>
<s2>
<ptxt>Install the actuator sub-assembly.</ptxt>
<s3>
<ptxt>Attach the 2 claws to connect the connector of a new wire harness sub-assembly.</ptxt>
<figure>
<graphic graphicname="B279069" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Install the actuator sub-assembly with the 3 screws.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install the wire harness sub-assembly.</ptxt>
<s3>
<ptxt>Pass the wire harness sub-assembly through the hole in the top of the support sub-assembly in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B279040E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Connect the connector and slide the cover as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B278469E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Install the body.</ptxt>
<s3>
<ptxt>Pass the wire harness sub-assembly through the hole in the cover body.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="B278451" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Attach the claw as shown in the illustration to attach the body cover to the support sub-assembly.</ptxt>
</s3>
<s3>
<ptxt>Attach the outer mirror body as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B278476" width="7.106578999in" height="2.775699831in"/>
</figure>
</s3>
<s3>
<ptxt>Install the outer mirror body with the 5 screws.</ptxt>
</s3>
<s3>
<ptxt>w/ Side Monitor System:</ptxt>
<ptxt>Connect the connector of the side television camera assembly to the wire harness sub-assembly and install the connector to the wire clip.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install the cover base.</ptxt>
<s3>
<ptxt>Pass the wire harness sub-assembly through the hole in the cover base.</ptxt>
</s3>
<s3>
<ptxt>Using a T25 "TORX" socket wrench, install the cover base with 3 new screws.</ptxt>
<atten3>
<ptxt>Make sure that the wire harness sub-assembly is not pinched between parts.</ptxt>
</atten3>
<torque>
<torqueitem>
<t-value1>3.8</t-value1>
<t-value2>39</t-value2>
<t-value3>34</t-value3>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>Attach the 6 claws to install the lower mirror cover.</ptxt>
</s2>
<s2>
<ptxt>Install the outer rear view mirror gasket LH.</ptxt>
<s3>
<ptxt>Pass the wire harness sub-assembly through a new outer rear view mirror gasket LH.</ptxt>
</s3>
<s3>
<ptxt>Attach the 5 claws to install the outer rear view mirror gasket LH.</ptxt>
<figure>
<graphic graphicname="B245738E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>Install the screw.</ptxt>
</s3>
<s3>
<ptxt>Align the mark of the wire harness sub-assembly with the end of the protrusion of the outer rear view mirror gasket LH into which the wire harness is inserted, and then secure the wire harness and clamp with tape as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B279047E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clamp</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tape</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>50 mm</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="B279048E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Overlap 2 pieces of tape over the clamp.</ptxt>
</item>
<item>
<ptxt>Make sure there is no gap between the tape and wire harness when applying tape.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>30 mm of overlap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>50 mm</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
<s2>
<ptxt>w/ Side Monitor System:</ptxt>
<ptxt>Assemble the connector.</ptxt>
<s3>
<ptxt>Move the retainer to the unlock position.</ptxt>
<figure>
<graphic graphicname="B276011E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Wire Harness Pin</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear View of Connector</ptxt>
</entry>
</row>

<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer Unlock Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retainer Lock Position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Insert the pins of the wire harness sub-assembly into the back of a new connector until they lock.</ptxt>
<table pgwide="1">
<title>Wire Harness Color Chart</title>
<tgroup cols="7">
<colspec colname="COL1" colwidth="1.01in"/>
<colspec colname="COL2" colwidth="1.01in"/>
<colspec colname="COL3" colwidth="1.01in"/>
<colspec colname="COL4" colwidth="1.01in"/>
<colspec colname="COL5" colwidth="1.01in"/>
<colspec colname="COL6" colwidth="1.01in"/>
<colspec colname="COLSPEC0" colwidth="1.02in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>7</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Purple</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Light Blue</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Black/Green</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Green</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Yellow</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>White</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" colwidth="1.01in"/>
<colspec colname="COL2" colwidth="1.01in"/>
<colspec colname="COL3" colwidth="1.01in"/>
<colspec colname="COL4" colwidth="1.01in"/>
<colspec colname="COL5" colwidth="1.01in"/>
<colspec colname="COL6" colwidth="1.01in"/>
<colspec colname="COLSPEC1" colwidth="1.02in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>8</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>12</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>13</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>14</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Brown</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Red</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Yellow-green</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Black/Green</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Black</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Blue</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pink</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Move the retainer to the lock position.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When inserting the pins of the wire harness, compare the new connector with the connector that was cut off during removal and make sure that the arrangement of the wire colors of the pins is the same as before.</ptxt>
</item>
<item>
<ptxt>Confirm that the pins of the wire harness are securely locked and cannot be pulled out.</ptxt>
</item>
<item>
<ptxt>When a pin of the wire harness is locked, it cannot be pulled out. Therefore, be sure to insert each pin into the correct location.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Install a new plastic sheet to the location shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B269420E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>w/o Side Monitor System:</ptxt>
<ptxt>Assemble the connector.</ptxt>
<s3>
<ptxt>Wrap the longer wire harness around the shorter one as shown in the part of the illustration labeled 1 until they are the same length.</ptxt>
</s3>
<s3>
<ptxt>Attach the 4 claws to connect the 2 connectors to the adapter.</ptxt>
<figure>
<graphic graphicname="B276009E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Connector</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adapter</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Claw</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A0N01VX_01_0004" proc-id="RM23G0E___0000CN000000">
<ptxt>INSTALL SIDE TURN SIGNAL LIGHT ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
<s2>
<ptxt>Install the light with the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003A0N01VX_01_0001" proc-id="RM23G0E___0000CN100000">
<ptxt>INSTALL OUTER MIRROR COVER LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the outer mirror cover.</ptxt>
<s3>
<figure>
<graphic graphicname="B241700E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Insert the rib on the outer edge of the cover into the groove of the mirror body.</ptxt>
<atten3>
<ptxt>Do not scratch the turn light.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Rib</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<figure>
<graphic graphicname="B241701E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<ptxt>Confirm that the cover and mirror body are aligned properly, and then push the outer end of the cover to attach the outer claws.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the upper ribs of the cover shown in part A of the illustration are properly attached to the mirror body.</ptxt>
</item>
<item>
<ptxt>Make sure that the ribs of the mirror body shown in part B of the illustration are not protruding from the cover.</ptxt>
</item>
</list1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Rib</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten3>
</s3>
<s3>
<figure>
<graphic graphicname="B241702" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>While making sure that the rib of the cover fits properly into the groove of the mirror body, squeeze the inner end of the cover and the mirror body together to attach the inner claws.</ptxt>
</s3>
<s3>
<figure>
<graphic graphicname="B241703" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Push on the cover at the locations of the 8 claws to confirm that the claws are attached properly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM000003A0N01VX_01_0006" proc-id="RM23G0E___0000CN200000">
<ptxt>INSTALL OUTER REAR VIEW MIRROR GLASS
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Mirror Heater:</ptxt>
<ptxt>Connect the 2 connectors.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B231563" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Attach the 4 claws to install the mirror glass.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>