<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM000000TA019CX" category="C" type-id="302IA" name-id="ESZI3-02" from="201210">
<dtccode>P0617</dtccode>
<dtcname>Starter Relay Circuit High</dtcname>
<subpara id="RM000000TA019CX_08" type-id="60" category="03" proc-id="RM23G0E___00001IG00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>While the engine is being cranked, positive battery voltage is applied to terminal STA of the ECM.</ptxt>
<ptxt>If the ECM detects the starter control (STA) signal while the vehicle is being driven, it determines that there is a malfunction in the STA circuit. The ECM then illuminates the MIL and stores the DTC.</ptxt>
<ptxt>This monitor runs when the vehicle has been driven at 20 km/h (12.5 mph) or more for more than 20 seconds.</ptxt>
<table pgwide="1">
<title>P0617</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Drive the vehicle for 25 seconds or more at a speed of 20 km/h (12.5 mph) or more and an engine speed of 1000 rpm or more</ptxt>
</entry>
<entry>
<ptxt>Conditions (a), (b) and (c) are met for 20 seconds (1 trip detection logic):</ptxt>
<ptxt>(a) Vehicle speed is more than 20 km/h (12.5 mph).</ptxt>
<ptxt>(b) Engine speed is more than 1000 rpm.</ptxt>
<ptxt>(c) STA signal is on.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Starter relay circuit</ptxt>
</item>
<item>
<ptxt>Ignition switch assembly</ptxt>
</item>
<item>
<ptxt>Clutch pedal switch assembly*1</ptxt>
</item>
<item>
<ptxt>Park/neutral position switch assembly*2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*1: for Manual Transmission</ptxt>
<ptxt>*2: for Automatic Transmission</ptxt>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>P0617</ptxt>
</entry>
<entry>
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000TA019CX_05" type-id="32" category="03" proc-id="RM23G0E___00001IE00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A264725E02" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="A269767E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TA019CX_06" type-id="51" category="05" proc-id="RM23G0E___00001IF00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07OX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The following troubleshooting process is based on the premise that the engine can crank normally. If the engine does not crank, proceed to Problem Symptoms Table (See page <xref label="Seep03" href="RM0000012WF07EX"/>).</ptxt>
</item>
<item>
<ptxt>If this DTC is output, inspect the starter.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TA019CX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TA019CX_09_0048" proc-id="RM23G0E___00001IV00001">
<testtitle>CHECK IF VEHICLE IS EQUIPPED WITH ENTRY AND START SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the vehicle is equipped with the entry and start system.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" align="center" colwidth="1.77in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry>
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>w/ Entry and Start System</ptxt>
</entry>
<entry>
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>w/o Entry and Start System</ptxt>
</entry>
<entry>
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0028" fin="false">A</down>
<right ref="RM000000TA019CX_09_0049" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0028" proc-id="RM23G0E___00001IH00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester when the ignition switch is turned to ON.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for Manual Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG (for Automatic Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000TA019CX_09_0029" fin="false">A</down>
<right ref="RM000000TA019CX_09_0043" fin="false">B</right>
<right ref="RM000000TA019CX_09_0035" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0029" proc-id="RM23G0E___00001II00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH PEDAL SWITCH ASSEMBLY - POWER MANAGEMENT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A40-2 or G50-3 (STAR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>r1-2 or G50-3 (STAR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the power management control ECU connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0030" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0030" proc-id="RM23G0E___00001IJ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH PEDAL SWITCH ASSEMBLY - ST RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the ST relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 or A40-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 or r1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the ST relay.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0031" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0031" proc-id="RM23G0E___00001IK00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH PEDAL SWITCH ASSEMBLY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G57-6 (STA) or A40-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G57-6 (STA) or r1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0037" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0037" proc-id="RM23G0E___00001IO00001">
<testtitle>INSPECT CLUTCH PEDAL SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the clutch pedal switch assembly (See page <xref label="Seep01" href="RM0000032S3007X_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA019CX_09_0032" fin="false">OK</right>
<right ref="RM000000TA019CX_09_0038" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0043" proc-id="RM23G0E___00001IQ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - POWER MANAGEMENT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the power management control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-4 or G50-3 (STAR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the power management control ECU connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0044" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0044" proc-id="RM23G0E___00001IR00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - STARTER RELAY (ST))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 or C40-5 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0045" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0045" proc-id="RM23G0E___00001IS00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-5 or G57-6 (STA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0046" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0046" proc-id="RM23G0E___00001IT00001">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch assembly (See page <xref label="Seep01" href="RM000002BKX033X"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA019CX_09_0032" fin="false">OK</right>
<right ref="RM000000TA019CX_09_0047" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0049" proc-id="RM23G0E___00001IW00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STARTER SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Starter Signal.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester when the ignition switch is turned to ON.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Starter Signal</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<right ref="RM000000TA019CX_09_0035" fin="false">OK</right>
<right ref="RM000000TA019CX_09_0058" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0058" proc-id="RM23G0E___00001J500001">
<testtitle>INSPECT IGNITION SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the ignition switch assembly (See page <xref label="Seep01" href="RM00000474M006X"/>).</ptxt>
</test1>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK (for Manual Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK (for Automatic Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000TA019CX_09_0050" fin="false">A</down>
<right ref="RM000000TA019CX_09_0054" fin="false">B</right>
<right ref="RM000000TA019CX_09_0059" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0050" proc-id="RM23G0E___00001IX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH PEDAL SWITCH ASSEMBLY - IGNITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ignition switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A40-2 or G2-1 (ST1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>r1-2 or G2-1 (ST1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ignition switch assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0051" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0051" proc-id="RM23G0E___00001IY00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH PEDAL SWITCH ASSEMBLY - ST RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the ST relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 or A40-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 or r1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the ST relay.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0052" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0052" proc-id="RM23G0E___00001IZ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLUTCH PEDAL SWITCH ASSEMBLY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G57-6 (STA) or A40-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G57-6 (STA) or r1-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the clutch pedal switch assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0053" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0053" proc-id="RM23G0E___00001J000001">
<testtitle>INSPECT CLUTCH PEDAL SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the clutch pedal switch assembly (See page <xref label="Seep01" href="RM0000032S3007X_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA019CX_09_0032" fin="false">OK</right>
<right ref="RM000000TA019CX_09_0038" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0054" proc-id="RM23G0E___00001J100001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - IGNITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ignition switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-4 or G2-1 (ST1) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ignition switch assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0055" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0055" proc-id="RM23G0E___00001J200001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - STARTER RELAY (ST))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the starter relay (ST) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ST relay terminal 1 or C40-5 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the starter relay (ST).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0056" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0056" proc-id="RM23G0E___00001J300001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARK/NEUTRAL POSITION SWITCH - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the park/neutral position switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C40-5 or G57-6 (STA) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the park/neutral position switch connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0057" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0034" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0057" proc-id="RM23G0E___00001J400001">
<testtitle>INSPECT PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the park/neutral position switch assembly (See page <xref label="Seep01" href="RM000002BKX033X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0032" fin="false">OK</down>
<right ref="RM000000TA019CX_09_0047" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0032" proc-id="RM23G0E___00001IL00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA019CX_09_0035" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0038" proc-id="RM23G0E___00001IP00001">
<testtitle>REPLACE CLUTCH PEDAL SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the clutch pedal switch assembly (See page <xref label="Seep01" href="RM00000176C00FX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA019CX_09_0035" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0047" proc-id="RM23G0E___00001IU00001">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the park/neutral position switch assembly (See page <xref label="Seep01" href="RM0000010NC06GX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA019CX_09_0035" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0059" proc-id="RM23G0E___00001J600001">
<testtitle>REPLACE IGNITION SWITCH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ignition switch assembly (See page <xref label="Seep01" href="RM000000YK102HX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000000TA019CX_09_0035" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0034" proc-id="RM23G0E___00001IM00001">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0035" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0035" proc-id="RM23G0E___00001IN00001">
<testtitle>CONFIRM WHETHER DTC OUTPUT RECURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK13TX"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for 25 seconds or more at a speed of 20 km/h (12.5 mph) or more and an engine speed of 1000 rpm or more.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TA019CX_09_0036" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TA019CX_09_0036">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>