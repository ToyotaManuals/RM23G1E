<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E8_T00NW" variety="T00NW">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000003E0P038X" category="C" type-id="300BD" name-id="AC9BV-07" from="201207" to="201210">
<dtccode>B1423/23</dtccode>
<dtcname>Pressure Sensor Circuit</dtcname>
<subpara id="RM000003E0P038X_01" type-id="60" category="03" proc-id="RM23G0E___0000HAK00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when refrigerant pressure is extremely low (0.176 MPa [1.8 kgf/cm<sup>2</sup>, 25.5 psi] or less) or extremely high (3.025 MPa [30.9 kgf/cm<sup>2</sup>, 438.6 psi] or higher). The air conditioner pressure sensor, which is installed on the pipe of the high pressure side to detect refrigerant pressure, outputs the refrigerant pressure signal to the air conditioning amplifier assembly. The air conditioning amplifier assembly converts the signal to a pressure value according to the sensor characteristics to control the compressor.</ptxt>
<atten4>
<ptxt>Be sure to check the refrigerant volume first when this DTC is output because this DTC may also be stored if there is no refrigerant in the cycle.</ptxt>
</atten4>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B1423/23</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>An open or short in the air conditioner pressure sensor circuit.</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure on the high pressure side is extremely low or extremely high.</ptxt>
</item>
</list1>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Air conditioner pressure sensor</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Cooler expansion valve</ptxt>
</item>
<item>
<ptxt>Cooler condenser assembly</ptxt>
</item>
<item>
<ptxt>Cooler dryer</ptxt>
</item>
<item>
<ptxt>Refrigerant pipe line</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000003E0P038X_02" type-id="32" category="03" proc-id="RM23G0E___0000HAL00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E156766E21" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003E0P038X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003E0P038X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003E0P038X_04_0001" proc-id="RM23G0E___0000HAM00000">
<testtitle>CHECK HARNESS AND CONNECTOR (PRESSURE SENSOR - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A2 sensor connector.</ptxt>
<figure>
<graphic graphicname="E167097E13" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A2-3 (+) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.75 to 5.25 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A2-1 (-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Air Conditioning Pressure Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000003E0P038X_04_0003" fin="false">OK</down>
<right ref="RM000003E0P038X_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0003" proc-id="RM23G0E___0000HAN00000">
<testtitle>CHECK AIR CONDITIONER PRESSURE SENSOR (SENSOR SIGNAL CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the A2 sensor connector.</ptxt>
<figure>
<graphic graphicname="E197830E09" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Remove the air conditioning amplifier assembly with its connectors still connected (See page <xref label="Seep01" href="RM0000039R5016X"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G25-9 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
<ptxt>A/C switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.63 to 4.72 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Air Conditioning Amplifier Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the measured voltage is not within the normal range, there may be a malfunction in the air conditioning amplifier assembly, air conditioner pressure sensor or wire harness. It is also possible that the amount of refrigerant may not be appropriate.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0004" fin="false">OK</down>
<right ref="RM000003E0P038X_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0004" proc-id="RM23G0E___0000HAO00000">
<testtitle>CHECK AIR CONDITIONER PRESSURE SENSOR (SENSOR SIGNAL CIRCUIT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage when the following conditions are met.</ptxt>
<figure>
<graphic graphicname="E197830E09" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry align="center">
<ptxt>2000 rpm</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Vehicle Doors</ptxt>
</entry>
<entry align="center">
<ptxt>Fully open</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature Setting</ptxt>
</entry>
<entry align="center">
<ptxt>MAX COOL</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Blower Speed</ptxt>
</entry>
<entry align="center">
<ptxt>Hi</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A/C Switch</ptxt>
</entry>
<entry align="center">
<ptxt>On</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>R/F Switch</ptxt>
</entry>
<entry align="center">
<ptxt>Recirculation</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Interior Temperature</ptxt>
</entry>
<entry align="center">
<ptxt>25 to 35°C (77 to 95°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>If refrigerant pressure on the high pressure side becomes extremely high during the inspection (the voltage exceeds 4.8 V), the fail-safe function stops compressor operation. Therefore, measure the voltage before the fail-safe operation.</ptxt>
</item>
<item>
<ptxt>It is necessary to measure the voltage for a certain amount of time (approximately 10 minutes) because the problem symptom may recur after a while.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When the outside air temperature is low (below -1.5°C [29.3°F]), the compressor stops due to operation of the ambient temperature sensor and evaporator temperature sensor to prevent the evaporator from freezing. In this case, perform the inspection in a warm indoor environment.</ptxt>
</atten4>
<test2>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G25-9 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
<ptxt>A/C switch on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.63 to 4.72 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Air Conditioning Amplifier Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to problem symptoms table)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (When troubleshooting according to the DTC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test2>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0040" fin="true">A</down>
<right ref="RM000003E0P038X_04_0020" fin="true">B</right>
<right ref="RM000003E0P038X_04_0014" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0005" proc-id="RM23G0E___0000HAP00000">
<testtitle>CHECK HARNESS AND CONNECTOR (PRESSURE SENSOR - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G25 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A2 sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A2-1 (-) - G25-13 (SG-2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A2-3 (+) - G25-10 (S5-3)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-13 (SG-2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-10 (S5-3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0020" fin="true">OK</down>
<right ref="RM000003E0P038X_04_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0007" proc-id="RM23G0E___0000HAQ00000">
<testtitle>CHECK HARNESS AND CONNECTOR (PRESSURE SENSOR - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G25 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A2 sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A2-2 (PR) - G25-9 (PRE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G25-9 (PRE) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0008" fin="false">OK</down>
<right ref="RM000003E0P038X_04_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0008" proc-id="RM23G0E___0000HAR00000">
<testtitle>CHECK FOR AIR CONDITIONING SYSTEM LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Install a manifold gauge set.</ptxt>
</test1>
<test1>
<ptxt>Recover the refrigerant from the air conditioning system using a refrigerant recovery unit.</ptxt>
</test1>
<test1>
<ptxt>Evacuate the air conditioning system and check that vacuum can be maintained in it.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vacuum can be maintained in the air conditioning system.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If vacuum cannot be maintained in the air conditioning system, refrigerant may be leaking from it. In this case, it is necessary to repair or replace the leaking part of the air conditioning system.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0009" fin="false">OK</down>
<right ref="RM000003E0P038X_04_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0009" proc-id="RM23G0E___0000HAS00000">
<testtitle>CHARGE REFRIGERANT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Add an appropriate amount of refrigerant (See page <xref label="Seep01" href="RM000001R8Y03CX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0010" proc-id="RM23G0E___0000HAT00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs when the following conditions are met (See page <xref label="Seep01" href="RM000002LIT02YX"/>).</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry align="center">
<ptxt>2000 rpm</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Vehicle Doors</ptxt>
</entry>
<entry align="center">
<ptxt>Fully open</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature Setting</ptxt>
</entry>
<entry align="center">
<ptxt>MAX COOL</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Blower Speed</ptxt>
</entry>
<entry align="center">
<ptxt>Hi</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A/C Switch</ptxt>
</entry>
<entry align="center">
<ptxt>On</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>R/F Switch</ptxt>
</entry>
<entry align="center">
<ptxt>Recirculation</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Interior Temperature</ptxt>
</entry>
<entry align="center">
<ptxt>25 to 35°C (77 to 95°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If refrigerant pressure on the high pressure side becomes high, a DTC will be stored. Therefore, it is necessary to check for DTCs after a certain amount of time (approximately 10 minutes) has passed because the DTC may be stored after the air conditioning system operates for a while.</ptxt>
</atten3>
<atten4>
<ptxt>When the outside air temperature is low (below -1.5°C [29.3°F]), the compressor stops due to operation of the ambient temperature sensor and evaporator temperature sensor to prevent the evaporator from freezing. In this case, perform the inspection in a warm indoor environment.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1423 is not output.</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>If the DTC was stored due to an insufficient or excessive amount of refrigerant, the problem may have been solved after performing the previous step. However, the root cause of insufficient refrigerant may be refrigerant leakage. Therefore, identify and repair the area where refrigerant leaks as necessary. The root cause of excessive refrigerant may be adding too much refrigerant.</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0045" fin="true">OK</down>
<right ref="RM000003E0P038X_04_0011" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0011" proc-id="RM23G0E___0000HAU00000">
<testtitle>CHECK AIR CONDITIONER PRESSURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Install a manifold gauge set.</ptxt>
<figure>
<graphic graphicname="E197830E12" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Reconnect the A2 sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the air conditioning amplifier assembly with its connectors still connected (See page <xref label="Seep01" href="RM0000039R5016X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="1.17in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle" align="center">
<ptxt>G25-9 (PRE) - G25-13 (SG-2)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure normal (below 3.025 MPa [30.9 kgf/cm<sup>2</sup>, 438.6 psi] and higher than 0.176 MPa [1.8 kgf/cm<sup>2</sup>, 25.5 psi])</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>0.63 to 4.72 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure abnormal (below 0.176 MPa [1.8 kgf/cm<sup>2</sup>, 25.5 psi])</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.63 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON</ptxt>
</item>
<item>
<ptxt>Refrigerant pressure abnormal (higher than 3.025 MPa [30.9 kgf/cm<sup>2</sup>, 438.6 psi])</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>4.72 V or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Air Conditioning Amplifier Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0020" fin="true">OK</down>
<right ref="RM000003E0P038X_04_0029" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0012" proc-id="RM23G0E___0000HAV00000">
<testtitle>REPAIR AIR CONDITIONING SYSTEM LEAK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Identify the area where refrigerant leaks (See page <xref label="Seep01" href="RM000001R8Y03CX"/>).</ptxt>
</test1>
<test1>
<ptxt>Repair the identified area of the air conditioning system.</ptxt>
</test1>
<test1>
<ptxt>Evacuate the air conditioning system.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0032" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0014" proc-id="RM23G0E___0000HAW00000">
<testtitle>CHARGE REFRIGERANT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use a refrigerant recovery unit to recover refrigerant.</ptxt>
</test1>
<test1>
<ptxt>Evacuate the air conditioning system.</ptxt>
</test1>
<test1>
<ptxt>Add an appropriate amount of refrigerant (See page <xref label="Seep01" href="RM000001R8Y03CX"/>).</ptxt>
<atten4>
<ptxt>If refrigerant is added and the system has not been properly evacuated (insufficient vacuum time), moisture in the air remaining in the system will freeze in the expansion valve, blocking the flow on the high pressure side. Therefore, in order to confirm the problem, recover the refrigerant and properly evacuate the system. Add an appropriate amount of refrigerant, and then check for the DTC.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0015" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0015" proc-id="RM23G0E___0000HAX00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs when the following conditions are met (See page <xref label="Seep01" href="RM000002LIT02YX"/>).</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry align="center">
<ptxt>2000 rpm</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Doors</ptxt>
</entry>
<entry align="center">
<ptxt>Fully open</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Temperature Setting</ptxt>
</entry>
<entry align="center">
<ptxt>MAX COOL</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Blower Speed</ptxt>
</entry>
<entry align="center">
<ptxt>Hi</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A/C Switch</ptxt>
</entry>
<entry align="center">
<ptxt>On</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>R/F Switch</ptxt>
</entry>
<entry align="center">
<ptxt>Recirculation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Interior Temperature</ptxt>
</entry>
<entry align="center">
<ptxt>25 to 35°C (77 to 95°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If refrigerant pressure on the high pressure side becomes high, a DTC will be stored. Therefore, it is necessary to check for DTCs after a certain amount of time (approximately 10 minutes) has passed because the DTC may be stored after the air conditioning system operates for a while.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the outside air temperature is low (below -1.5°C [29.3°F]), the compressor stops due to operation of the ambient temperature sensor and evaporator temperature sensor to prevent the evaporator from freezing. In this case, perform the inspection in a warm indoor environment.</ptxt>
</item>
<item>
<ptxt>If refrigerant is added and the system has not been properly evacuated (insufficient vacuum time), moisture in the air remaining in the system will freeze in the expansion valve, blocking the flow on the high pressure side. Therefore, in order to confirm the problem, recover the refrigerant and properly evacuate the system. Add an appropriate amount of refrigerant and check for the DTC. If the DTC is not output after this procedure, it indicates that the cooler dryer in the cooler condenser assembly is not able to absorb moisture in the refrigerant cycle. In this case, to complete the repair, it is necessary to replace the cooler dryer.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1423 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0034" fin="true">OK</down>
<right ref="RM000003E0P038X_04_0016" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0016" proc-id="RM23G0E___0000HAY00000">
<testtitle>REPLACE COOLER EXPANSION VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the cooler expansion valve with a new or normally functioning one (See page <xref label="Seep01" href="RM000003AXS02MX"/>).</ptxt>
<atten4>
<ptxt>Replace the cooler expansion valve with a new or normally functioning one because the cooler expansion valve is either stuck or clogged.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0017" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0017" proc-id="RM23G0E___0000HAZ00000">
<testtitle>CHARGE REFRIGERANT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Add an appropriate amount of refrigerant (See page <xref label="Seep01" href="RM000001R8Y03CX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0018" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0018" proc-id="RM23G0E___0000HB000000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs when the following conditions are met.</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>Engine Speed</ptxt>
</entry>
<entry align="center">
<ptxt>2000 rpm</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Vehicle Doors</ptxt>
</entry>
<entry align="center">
<ptxt>Fully open</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Temperature Setting</ptxt>
</entry>
<entry align="center">
<ptxt>MAX COOL</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Blower Speed</ptxt>
</entry>
<entry align="center">
<ptxt>Hi</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>A/C Switch</ptxt>
</entry>
<entry align="center">
<ptxt>On</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>R/F Switch</ptxt>
</entry>
<entry align="center">
<ptxt>Recirculation</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>Interior Temperature</ptxt>
</entry>
<entry align="center">
<ptxt>25 to 35°C (77 to 95°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>If refrigerant pressure on the high pressure side becomes high, a DTC will be stored. Therefore, it is necessary to check for DTCs after a certain amount of time (approximately 10 minutes) has passed because the DTC may be stored after the air conditioning system operates for a while.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the outside air temperature is low (below -1.5°C [29.3°F]), the compressor stops due to operation of the ambient temperature sensor and evaporator temperature sensor to prevent the evaporator from freezing. In this case, perform the inspection in a warm indoor environment.</ptxt>
</item>
<item>
<ptxt>If refrigerant pressure is not normal after replacing the expansion valve with a new or normally functioning one, the cooler condenser assembly or pipes may be clogged due to dirt, dust or other contaminants. In this case, clean or replace the cooler condenser assembly or pipes.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B1423 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003E0P038X_04_0028" fin="true">OK</down>
<right ref="RM000003E0P038X_04_0035" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003E0P038X_04_0020">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R5016X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P038X_04_0022">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003E0P038X_04_0028">
<testtitle>END (COOLER EXPANSION VALVE IS FAULTY)</testtitle>
</testgrp>
<testgrp id="RM000003E0P038X_04_0029">
<testtitle>REPLACE AIR CONDITIONER PRESSURE SENSOR<xref label="Seep01" href="RM0000030JI02IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P038X_04_0032">
<testtitle>CHARGE REFRIGERANT<xref label="Seep01" href="RM000001R8Y03CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P038X_04_0034">
<testtitle>REPLACE COOLER DRYER<xref label="Seep01" href="RM00000180603IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P038X_04_0035">
<testtitle>CLEAN OR REPLACE COOLER CONDENSER ASSEMBLY OR PIPES<xref label="Seep01" href="RM00000180603IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P038X_04_0040">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000045E301IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003E0P038X_04_0045">
<testtitle>END (AMOUNT OF REFRIGERANT IS FAULTY)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>