<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM000000T8M0NHX" category="C" type-id="300YC" name-id="ES2WJ-96" from="201207" to="201210">
<dtccode>P0505</dtccode>
<dtcname>Idle Control System Malfunction</dtcname>
<subpara id="RM000000T8M0NHX_01" type-id="60" category="03" proc-id="RM23G0E___000039J00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The idling speed is controlled by the electronic throttle control system. The electronic throttle control system is comprised of: 1) a one-valve-type throttle body; 2) the throttle actuator, which operates the throttle valve; 3) the throttle position sensor, which detects the opening angle of the throttle valve; 4) the accelerator pedal position sensor, which detects the accelerator pedal position; and 5) the ECM, which controls the electronic throttle control system. Based on the target idling speed, the ECM controls the throttle actuator to provide the proper throttle valve opening angle.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0505</ptxt>
</entry>
<entry valign="middle">
<ptxt>The idle speed continues to vary greatly from the target speed (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Electronic throttle control system</ptxt>
</item>
<item>
<ptxt>Intake system</ptxt>
</item>
<item>
<ptxt>PCV hose connection</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000T8M0NHX_06" type-id="51" category="05" proc-id="RM23G0E___000039K00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>The following condition may also cause DTC P0505 to be stored:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>The throttle valve is not fully closed (e.g. the accelerator pedal is slightly depressed by something such as the floor carpet overlapping the accelerator pedal).</ptxt>
</item>
</list2>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000T8M0NHX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000T8M0NHX_07_0001" proc-id="RM23G0E___000039L00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0505)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC P0505 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC P0505 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0505 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000T8M0NHX_07_0002" fin="false">A</down>
<right ref="RM000000T8M0NHX_07_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0NHX_07_0002" proc-id="RM23G0E___000039M00000">
<testtitle>CHECK PCV HOSE CONNECTIONS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the PCV hose connection (See page <xref label="Seep01" href="RM00000141J010X_01_0001"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>PCV hose is connected correctly and is not damaged.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8M0NHX_07_0003" fin="false">OK</down>
<right ref="RM000000T8M0NHX_07_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0NHX_07_0003" proc-id="RM23G0E___000039N00000">
<testtitle>CHECK INTAKE SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the intake system for vacuum leakage (See page <xref label="Seep01" href="RM000001A3W02WX_01_0008"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No leakage from intake system.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8M0NHX_07_0004" fin="false">OK</down>
<right ref="RM000000T8M0NHX_07_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0NHX_07_0004" proc-id="RM23G0E___000039O00000">
<testtitle>CHECK THROTTLE VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the throttle valve condition (See page <xref label="Seep01" href="RM000000VWM02CX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Throttle valve is not contaminated with foreign objects and moves smoothly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000T8M0NHX_07_0009" fin="true">OK</down>
<right ref="RM000000T8M0NHX_07_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000T8M0NHX_07_0005">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000000PDF0G7X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000T8M0NHX_07_0006">
<testtitle>REPAIR OR REPLACE PCV HOSE</testtitle>
</testgrp>
<testgrp id="RM000000T8M0NHX_07_0007">
<testtitle>REPAIR OR REPLACE INTAKE SYSTEM</testtitle>
</testgrp>
<testgrp id="RM000000T8M0NHX_07_0008">
<testtitle>REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY<xref label="Seep01" href="RM000000VWO02EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000T8M0NHX_07_0009">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>