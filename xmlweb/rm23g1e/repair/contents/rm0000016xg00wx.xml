<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12023_S001A" variety="S001A">
<name>AXLE AND DIFFERENTIAL</name>
<ttl id="12023_S001A_7B96A_T00FY" variety="T00FY">
<name>REAR AXLE SHAFT</name>
<para id="RM0000016XG00WX" category="A" type-id="8000E" name-id="AD0SP-01" from="201207">
<name>REASSEMBLY</name>
<subpara id="RM0000016XG00WX_02" type-id="11" category="10" proc-id="RM23G0E___00009DZ00000">
<content3 releasenbr="1">
<atten3>
<ptxt>Do not allow foreign matter, etc. to contact the rear axle hub and bearing assembly.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM0000016XG00WX_01" type-id="01" category="01">
<s-1 id="RM0000016XG00WX_01_0001" proc-id="RM23G0E___00009DV00000">
<ptxt>INSTALL BRAKE DRUM OIL DEFLECTOR LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="G020977E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Install a new deflector gasket and deflector to the rear axle shaft.</ptxt>
<atten4>
<ptxt>Align the 2 oil drain holes.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oil Drain Holes</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Insert 6 new hub bolts.</ptxt>
</s2>
<s2>
<ptxt>Temporarily install a washer and nut to each hub bolt as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="G020973" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the hub bolts by tightening each nut.</ptxt>
</s2>
<s2>
<ptxt>Remove the washer and nut from each hub bolt.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016XG00WX_01_0007" proc-id="RM23G0E___00009DW00000">
<ptxt>INSTALL REAR AXLE HUB AND BEARING ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new rear axle hub and bearing to the parking brake plate.</ptxt>
<atten3>
<ptxt>Make sure the bearing is securely installed to the parking brake plate.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using 2 socket wrenches and a press, press in the 4 housing bolts.</ptxt>
<figure>
<graphic graphicname="F042777E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016XG00WX_01_0008" proc-id="RM23G0E___00009DX00000">
<ptxt>INSTALL REAR AXLE SHAFT LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="G022121E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Install a new washer and a new retainer to the axle hub as shown in the illustration.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Install the washer with its tapered surface facing downward.</ptxt>
</item>
<item>
<ptxt>Install the retainer with its chamfered surface facing downward.</ptxt>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tapered Surface</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Chamfered Surface</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using SST and a press, press in the rear axle shaft.</ptxt>
<sst>
<sstitem>
<s-number>09631-12090</s-number>
</sstitem>
<sstitem>
<s-number>09726-40010</s-number>
</sstitem>
<sstitem>
<s-number>09951-01100</s-number>
</sstitem>
</sst>
<atten3>
<ptxt>Make sure the bearing is securely installed to the rear axle shaft.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000016XG00WX_01_0009" proc-id="RM23G0E___00009DY00000">
<ptxt>INSTALL REAR AXLE SHAFT SNAP RING LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="F042773E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using snap ring expander, install a new rear axle shaft snap ring.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>