<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM000000PDU0S9X" category="C" type-id="302GP" name-id="ESM76-09" from="201210">
<dtccode>P0011</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Advanced or System Performance (Bank 1)</dtcname>
<dtccode>P0012</dtccode>
<dtcname>Camshaft Position "A" - Timing Over-Retarded (Bank 1)</dtcname>
<subpara id="RM000000PDU0S9X_01" type-id="60" category="03" proc-id="RM23G0E___00003BV00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<atten4>
<ptxt>If DTC P0011 or P0012 is output, check the Variable Valve Timing (VVT) system.</ptxt>
</atten4>
<ptxt>The VVT system includes the ECM, oil control valve and VVT controller. The ECM sends a target duty cycle control signal to the camshaft timing oil control valve assembly. This control signal regulates the oil pressure applied to the VVT controller. Camshaft timing control is performed according to engine operating conditions such as the intake air volume, throttle valve position and engine coolant temperature. The ECM controls the camshaft timing oil control valve assembly based on the signals transmitted by several sensors. The VVT controller regulates the intake camshaft angle using oil pressure through the camshaft timing oil control valve assembly. As a result, the relative positions of the camshaft and crankshaft are optimized, the engine torque and fuel economy improve, and the exhaust emissions decrease under overall driving conditions. The ECM detects the actual intake valve timing using signals from the camshaft and crankshaft position sensors and performs feedback control. This is how the target intake valve timing is verified by the ECM.</ptxt>
<figure>
<graphic graphicname="A192077E04" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0011</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve timing is not adjusted in the valve timing advance range (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Valve timing</ptxt>
</item>
<item>
<ptxt>Camshaft timing oil control valve assembly</ptxt>
</item>
<item>
<ptxt>Oil control valve filter</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0012</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve timing is not adjusted in the valve timing retard range (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Valve timing</ptxt>
</item>
<item>
<ptxt>Camshaft timing oil control valve assembly</ptxt>
</item>
<item>
<ptxt>Oil control valve filter</ptxt>
</item>
<item>
<ptxt>Camshaft timing gear assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PDU0S9X_07" type-id="32" category="03" proc-id="RM23G0E___00003BW00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0010 (See page <xref label="Seep01" href="RM000000PDW0PXX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PDU0S9X_08" type-id="51" category="05" proc-id="RM23G0E___00003BX00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>DTC P0011 or P0012 may be stored when foreign objects in the engine oil are caught in some parts of the system. The DTC remains stored for a short time even after the system returns to normal. These foreign objects may then be captured by the oil filter.</ptxt>
</item>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PDU0S9X_13" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PDU0S9X_13_0001" proc-id="RM23G0E___00003BY00001">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0011 OR P0012)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC P0011 or P0012 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC P0011 or P0012 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0011 or P0012 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDU0S9X_13_0024" fin="false">A</down>
<right ref="RM000000PDU0S9X_13_0009" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0024" proc-id="RM23G0E___00003C400001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (OPERATE CAMSHAFT TIMING OIL CONTROL VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the VVT System (Bank 1).</ptxt>
</test1>
<test1>
<ptxt>Check the engine speed when the camshaft timing oil control valve is operated using the intelligent tester when the engine coolant temperature is 50°C (122°F) or less.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When performing the Active Test, make sure the air conditioning is on.</ptxt>
</item>
<item>
<ptxt>Make sure the engine coolant temperature when the engine is started is 30°C (86°F) or less.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.13in"/>
<colspec colname="COL2" colwidth="4.95in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Operation</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Camshaft timing oil control valve OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal engine speed</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Camshaft timing oil control valve ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine idles roughly or stalls soon after camshaft timing oil control valve switched from OFF to ON</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000PDU0S9X_13_0025" fin="false">OK</down>
<right ref="RM000000PDU0S9X_13_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0025" proc-id="RM23G0E___00003C500001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0011 OR P0012)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z8X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the tester (See page <xref label="Seep02" href="RM000000PDL0PHX"/>).</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for more than 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P0011 or P0012 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>DTC P0011 or P0012 is stored when foreign objects in the engine oil are caught in some parts of the system. These codes remain stored for a short time even after the system returns to normal. These foreign objects may then be captured by the oil control valve filter, thus eliminating the source of the problem.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDU0S9X_13_0026" fin="true">A</down>
<right ref="RM000000PDU0S9X_13_0010" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0006" proc-id="RM23G0E___00003BZ00001">
<testtitle>INSPECT CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft timing oil control valve assembly (See page <xref label="Seep01" href="RM000000Q7403PX"/>).</ptxt>
</test1>
<test1>
<ptxt>Connect the positive (+) battery terminal to terminal 1 and the negative (-) battery terminal to terminal 2. Check the valve operation.</ptxt>
<figure>
<graphic graphicname="A183795E07" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Valve moves quickly.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Reinstall the camshaft timing oil control valve assembly.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDU0S9X_13_0020" fin="false">OK</down>
<right ref="RM000000PDU0S9X_13_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0020" proc-id="RM23G0E___00003C300001">
<testtitle>INSPECT CAMSHAFT TIMING GEAR ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft timing gear assembly (See page <xref label="Seep01" href="RM000000YN0020X_01_0031"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDU0S9X_13_0007" fin="false">OK</down>
<right ref="RM000000PDU0S9X_13_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0007" proc-id="RM23G0E___00003C000001">
<testtitle>INSPECT OIL CONTROL VALVE FILTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the oil control valve filter.</ptxt>
</test1>
<test1>
<ptxt>Check that the filter is not clogged.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Filter is not clogged.</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>Reinstall the oil control valve filter.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PDU0S9X_13_0010" fin="false">OK</down>
<right ref="RM000000PDU0S9X_13_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0010" proc-id="RM23G0E___00003C100001">
<testtitle>ADJUST VALVE TIMING</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>There are no marks on the cylinder head to align for the purpose of checking valve timing. Valve timing can only be inspected by aligning the colored plates on the timing chain with the marks on the pulleys. It may be necessary to remove and reinstall the chain to align the timing marks (See page <xref label="Seep01" href="RM000000YMY027X_01_0006"/>).</ptxt>
</atten4>
</content6>
<res>
<down ref="RM000000PDU0S9X_13_0019" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0019" proc-id="RM23G0E___00003C200001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0011 OR P0012)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z8X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Switch the ECM from normal mode to check mode using the tester (See page <xref label="Seep02" href="RM000000PDL0PHX"/>).</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for more than 1 minute.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle for more than 10 minutes.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC P0011 or P0012 is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>DTC P0011 or P0012 is stored when foreign objects in the engine oil are caught in some parts of the system. These codes remain stored for a short time even after the system returns to normal. These foreign objects may then be captured by the oil control valve filter, thus eliminating the source of the problem.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PDU0S9X_13_0015" fin="true">A</down>
<right ref="RM000000PDU0S9X_13_0014" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0009">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000000PDF0HGX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0026">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0VKX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0011">
<testtitle>REPLACE CAMSHAFT TIMING OIL CONTROL VALVE ASSEMBLY<xref label="Seep01" href="RM00000111G008X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0021">
<testtitle>REPLACE CAMSHAFT TIMING GEAR ASSEMBLY<xref label="Seep01" href="RM000000YN0020X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0012">
<testtitle>REPLACE OIL CONTROL VALVE FILTER</testtitle>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0014">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PDU0S9X_13_0015">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>