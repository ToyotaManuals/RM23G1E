<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000A" variety="S000A">
<name>1KD-FTV ENGINE MECHANICAL</name>
<ttl id="12007_S000A_7B8XK_T0078" variety="T0078">
<name>ENGINE UNIT (w/ DPF)</name>
<para id="RM00000147O02OX" category="G" type-id="3001K" name-id="EM5HN-03" from="201207">
<name>INSPECTION</name>
<subpara id="RM00000147O02OX_01" type-id="01" category="01">
<s-1 id="RM00000147O02OX_01_0011" proc-id="RM23G0E___00004IH00000">
<ptxt>INSPECT INJECTION GEAR BEARING</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the bearing is not rough or worn.</ptxt>
<ptxt>If necessary, replace the injection gear.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0007" proc-id="RM23G0E___00004IG00000">
<ptxt>INSPECT NO. 1 IDLE GEAR OIL CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a cylinder gauge, measure the inside diameter of the idle gear.</ptxt>
<figure>
<graphic graphicname="A060287E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard idle gear inside diameter</title>
<specitem>
<ptxt>44.000 to 44.025 mm (1.732 to 1.734 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Using a micrometer, measure the diameter of the idle gear shaft.</ptxt>
<figure>
<graphic graphicname="A051343E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard idle gear shaft diameter</title>
<specitem>
<ptxt>43.955 to 43.990 mm (1.730 to 1.732 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Subtract the idle gear shaft diameter measurement from the idle gear inside diameter measurement.</ptxt>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>0.01 to 0.07 mm (0.000394 to 0.00276 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt> 0.20 mm (0.00787 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace the gear and shaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0039" proc-id="RM23G0E___00004IO00000">
<ptxt>INSPECT VALVE LIFTER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the lifter diameter.</ptxt>
<figure>
<graphic graphicname="P016860E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard lifter diameter</title>
<specitem>
<ptxt>30.966 to 30.976 mm (1.219 to 1.220 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Using a caliper gauge, measure the lifter bore diameter of the cylinder head.</ptxt>
<figure>
<graphic graphicname="A058115E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard lifter bore diameter</title>
<specitem>
<ptxt>31.000 to 31.021 mm (1.220 to 1.221 in.)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Measure the oil clearance by subtracting the lifter diameter measurement from the lifter bore diameter measurement.</ptxt>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>0.024 to 0.055 mm (0.000945 to 0.00217 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt> 0.095 mm (0.00374 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the oil clearance is more than the maximum, replace the lifter. If necessary, replace the cylinder head.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0035" proc-id="RM23G0E___00004IL00000">
<ptxt>INSPECT INTAKE MANIFOLD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a precision straightedge and feeler gauge, measure the warpage of the surface of the intake manifold that contacts the cylinder head.</ptxt>
<figure>
<graphic graphicname="A098945E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum warpage</title>
<specitem>
<ptxt>0.1 mm (0.00394 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the warpage is more than the maximum, replace the intake manifold.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0036" proc-id="RM23G0E___00004IM00000">
<ptxt>INSPECT EXHAUST MANIFOLD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a precision straightedge and feeler gauge, measure the warpage of the surface of the exhaust manifold that contacts the cylinder head.</ptxt>
<figure>
<graphic graphicname="A098949E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Maximum warpage</title>
<specitem>
<ptxt>0.2 mm (0.00787 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the warpage is more than the maximum, replace the exhaust manifold.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0033" proc-id="RM23G0E___00004IJ00000">
<ptxt>INSPECT CAMSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the circle runout.</ptxt>
<s3>
<ptxt>Place the camshaft on V-blocks.</ptxt>
</s3>
<s3>
<ptxt>Using a dial indicator, measure the circle runout at the center journal.</ptxt>
<spec>
<title>Maximum circle runout</title>
<specitem>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the circle runout is more than the maximum, replace the camshaft.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the cam lobe height.</ptxt>
<s3>
<ptxt>Using a micrometer, measure the cam lobe height.</ptxt>
<spec>
<title>Standard cam lobe height</title>
<specitem>
<ptxt>46.99 to 47.09 mm (1.850 to 1.854 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum cam lobe height</title>
<specitem>
<ptxt>46.57 mm (1.833 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the cam lobe height is less than the minimum, replace the camshaft.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the diameter of the camshaft journal.</ptxt>
<s3>
<ptxt>Using a micrometer, measure the diameter of the camshaft journal for the camshaft bearing.</ptxt>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>27.969 to 27.985 mm (1.101 to 1.102 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is not as specified, check the oil clearance.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the backlash.</ptxt>
<figure>
<graphic graphicname="A061825E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Install the 2 camshafts.</ptxt>
</s3>
<s3>
<ptxt>Using a dial indicator, measure the backlash.</ptxt>
<spec>
<title>Standard backlash</title>
<specitem>
<ptxt>0.035 to 0.089 mm (0.00138 to 0.00350 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum backlash</title>
<specitem>
<ptxt>0.189 mm (0.00744 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the backlash is more than the maximum, replace the 2 camshafts.</ptxt>
</s3>
<s3>
<ptxt>Remove the 2 camshafts.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0034" proc-id="RM23G0E___00004IK00000">
<ptxt>INSPECT NO. 2 CAMSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect the circle runout.</ptxt>
<s3>
<ptxt>Place the camshaft on V-blocks.</ptxt>
</s3>
<s3>
<ptxt>Using a dial indicator, measure the circle runout at the center journal.</ptxt>
<spec>
<title>Maximum circle runout</title>
<specitem>
<ptxt>0.03 mm (0.00118 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the circle runout is more than the maximum, replace the camshaft.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the cam lobe height.</ptxt>
<s3>
<ptxt>Using a micrometer, measure the cam lobe height.</ptxt>
<spec>
<title>Standard cam lobe height</title>
<specitem>
<ptxt>48.31 to 48.41 mm (1.902 to 1.906 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum cam lobe height</title>
<specitem>
<ptxt>48.16 mm (1.896 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the cam lobe height is less than the minimum, replace the camshaft.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Inspect the diameter of the camshaft journal.</ptxt>
<s3>
<ptxt>Using a micrometer, measure the diameter of the camshaft journal for the camshaft bearing.</ptxt>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>27.969 to 27.985 mm (1.101 to 1.102 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the diameter is not as specified, check the oil clearance.</ptxt>
</s3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0037" proc-id="RM23G0E___00004IN00000">
<ptxt>INSPECT CAMSHAFT OIL CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the bearing caps and journals.</ptxt>
</s2>
<s2>
<ptxt>Check the bearings for flaking and scoring.</ptxt>
<ptxt>If the bearings are damaged, replace the bearing caps and cylinder head as a set.</ptxt>
</s2>
<s2>
<ptxt>Place the camshaft on the cylinder head.</ptxt>
</s2>
<s2>
<ptxt>Lay a strip of Plastigage across each of the journals.</ptxt>
<figure>
<graphic graphicname="A226776E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Plastigage</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the bearing caps.</ptxt>
<atten3>
<ptxt>Do not turn the camshaft.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the bearing caps.</ptxt>
</s2>
<s2>
<ptxt>Measure the Plastigage at its widest point.</ptxt>
<figure>
<graphic graphicname="A226777E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard oil clearance</title>
<specitem>
<ptxt>0.025 to 0.062 mm (0.000984 to 0.00244 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum oil clearance</title>
<specitem>
<ptxt>0.1 mm (0.00394 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Plastigage</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the oil clearance is more than the maximum, replace the camshaft. If necessary, replace the bearing caps and cylinder head as a set.</ptxt>
</s2>
<s2>
<ptxt>Completely remove the Plastigage.</ptxt>
</s2>
<s2>
<ptxt>Remove the camshaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0038" proc-id="RM23G0E___00004EL00000">
<ptxt>INSPECT CAMSHAFT THRUST CLEARANCE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the camshaft.</ptxt>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the thrust clearance while moving the camshaft back and forth.</ptxt>
<figure>
<graphic graphicname="A226778E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard thrust clearance</title>
<specitem>
<ptxt>0.035 to 0.185 mm (0.00138 to 0.0728 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Maximum thrust clearance</title>
<specitem>
<ptxt>0.25 mm (0.00984 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the thrust clearance is more than the maximum, replace the camshaft. If necessary, replace the bearing caps and cylinder head as a set.</ptxt>
</s2>
<s2>
<ptxt>Remove the camshaft.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000147O02OX_01_0032" proc-id="RM23G0E___00004II00000">
<ptxt>INSPECT CYLINDER HEAD SET BOLT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the diameter of the most elongated threads in the measurement area.</ptxt>
<figure>
<graphic graphicname="A226805E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Measuring area</title>
<specitem>
<ptxt>33 mm (1.30 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Standard outside diameter</title>
<specitem>
<ptxt>11.76 to 11.97 mm (0.463 to 0.471 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum outside diameter</title>
<specitem>
<ptxt>11.6 mm (0.457 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measuring Area</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If a visual check reveals no excessively thin areas, check the center of the bolt (see illustration) and find the area that has the smallest diameter.</ptxt>
</atten4>
<ptxt>If the diameter is less than the minimum, replace the cylinder head set bolt.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>