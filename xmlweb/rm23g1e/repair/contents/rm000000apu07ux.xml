<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A1_T00JP" variety="T00JP">
<name>TOYOTA PARKING ASSIST-SENSOR SYSTEM (for 8 Sensor Type)</name>
<para id="RM000000APU07UX" category="J" type-id="301QL" name-id="PM1QW-28" from="201210">
<dtccode/>
<dtcname>Clearance Warning ECU Power Source Circuit</dtcname>
<subpara id="RM000000APU07UX_06" type-id="60" category="03" proc-id="RM23G0E___0000CQ100001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit provides power to operate the clearance warning ECU.</ptxt>
</content5>
</subpara>
<subpara id="RM000000APU07UX_03" type-id="32" category="03" proc-id="RM23G0E___0000CPX00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E172196E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000APU07UX_04" type-id="51" category="05" proc-id="RM23G0E___0000CPY00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000APU07UX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000APU07UX_05_0002" proc-id="RM23G0E___0000CPZ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLEARANCE WARNING ECU - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I1 clearance warning ECU connector.</ptxt>
<figure>
<graphic graphicname="B155759E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) on the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I1-15 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I1-15 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Clearance Warning ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000APU07UX_05_0006" fin="false">OK</down>
<right ref="RM000000APU07UX_05_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000APU07UX_05_0006" proc-id="RM23G0E___0000CQ000001">
<testtitle>CHECK HARNESS AND CONNECTOR (CLEARANCE WARNING ECU - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I1 clearance warning ECU connector.</ptxt>
<figure>
<graphic graphicname="B155759E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I1-17 (E) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Clearance Warning ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000APU07UX_05_0007" fin="true">OK</down>
<right ref="RM000000APU07UX_05_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000APU07UX_05_0007">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="X394000005X09XD"/>
</testtitle>
</testgrp>
<testgrp id="RM000000APU07UX_05_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>