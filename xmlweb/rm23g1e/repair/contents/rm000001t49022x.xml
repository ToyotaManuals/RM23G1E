<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B8_T00KW" variety="T00KW">
<name>THEFT DETERRENT SYSTEM</name>
<para id="RM000001T49022X" category="J" type-id="80095" name-id="TD3AH-05" from="201210">
<dtccode/>
<dtcname>Intrusion Sensor Cancel Switch Circuit</dtcname>
<subpara id="RM000001T49022X_01" type-id="60" category="03" proc-id="RM23G0E___0000EY200001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>When the intrusion sensor OFF switch on the map light assembly is pressed, the sensor off signal is sent to the main body ECU which causes the theft warning radar sensor (intrusion sensor) to stop operating.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000001T49022X_02" type-id="32" category="03" proc-id="RM23G0E___0000EY300001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B246189E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001T49022X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001T49022X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001T49022X_04_0006" proc-id="RM23G0E___0000EY500001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INTRUSION SENSOR OFF SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000000W0S02DX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Intrusion Sens OFF SW</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Intrusion sensor OFF switch /</ptxt>
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Intrusion sensor OFF switch pushed</ptxt>
<ptxt>OFF: Intrusion sensor OFF switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>ON (intrusion sensor OFF switch pushed) and OFF (intrusion sensor OFF switch not pushed) appear on screen according to switch condition.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T49022X_04_0008" fin="true">OK</down>
<right ref="RM000001T49022X_04_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T49022X_04_0007" proc-id="RM23G0E___0000EY600001">
<testtitle>INSPECT MAP LIGHT ASSEMBLY (INTRUSION SENSOR OFF SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the map light (See page <xref label="Seep01" href="RM000003YO900YX"/>).</ptxt>
<figure>
<graphic graphicname="E140936E19" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>5 (GND3) - 26 (SOFF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Intrusion sensor OFF switch pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5 (GND3) - 26 (SOFF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Intrusion sensor OFF switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T49022X_04_0001" fin="false">OK</down>
<right ref="RM000001T49022X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T49022X_04_0001" proc-id="RM23G0E___0000EY400001">
<testtitle>CHECK HARNESS AND CONNECTOR (MAP LIGHT - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the W7 light connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G64 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>W7-26 (SOFF) - G64-23 (SSW1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W7-26 (SOFF) or G64-23 (SSW1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>W7-5 (GND3) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001T49022X_04_0004" fin="true">OK</down>
<right ref="RM000001T49022X_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001T49022X_04_0004">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO03DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T49022X_04_0005">
<testtitle>REPLACE MAP LIGHT ASSEMBLY (INTRUSION SENSOR OFF SWITCH)<xref label="Seep01" href="RM000003YO900YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001T49022X_04_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001T49022X_04_0008">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002TGC01FX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>