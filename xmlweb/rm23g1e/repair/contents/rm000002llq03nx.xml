<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E9_T00NX" variety="T00NX">
<name>AIR CONDITIONING SYSTEM (for Manual Air Conditioning System)</name>
<para id="RM000002LLQ03NX" category="C" type-id="804EH" name-id="ACHLW-02" from="201210">
<dtccode>B1497</dtccode>
<dtcname>Communication Malfunction (Bus Ic)</dtcname>
<subpara id="RM000002LLQ03NX_01" type-id="60" category="03" proc-id="RM23G0E___0000HJ600001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The air conditioning harness assembly connects the air conditioning amplifier assembly and each servo motor. The air conditioning amplifier assembly supplies power and sends operation instructions to each servo motor through the air conditioning harness assembly. Each servo motor sends the damper position information to the air conditioning amplifier assembly.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B1497</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>An open circuit or malfunction in the communication line.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Recirculation damper servo sub-assembly</ptxt>
</item>
<item>
<ptxt>Damper servo sub-assembly RH</ptxt>
</item>
<item>
<ptxt>Damper servo sub-assembly LH</ptxt>
</item>
<item>
<ptxt>Air conditioning harness assembly</ptxt>
</item>
<item>
<ptxt>Air conditioning amplifier assembly</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002LLQ03NX_02" type-id="32" category="03" proc-id="RM23G0E___0000HJ700001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E259840E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002LLQ03NX_03" type-id="51" category="05" proc-id="RM23G0E___0000JNB00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002LLQ03NX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002LLQ03NX_05_0009" proc-id="RM23G0E___0000JND00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the Active Test, use the intelligent tester to generate a control command, and then check that the servo motor operates (See page <xref label="Seep01" href="RM000002LIV02DX"/>).</ptxt>
<table pgwide="1">
<title>Air Conditioner</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Air Mix Servo Targ Pulse (D)</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (driver side air mix damper servo) target pulse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Mix Servo Targ Pulse (P)</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 air conditioning radiator damper servo sub-assembly (front passenger side air mix damper servo) target pulse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Outlet Servo Pulse (D)</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 air conditioning radiator damper servo sub-assembly (mode damper servo) target pulse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air Inlet Damper Targ Pulse</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 1 blower damper servo sub-assembly target pulse</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Min.: 0, Max.: 255</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Arm of the servo motor selected in the Active Test moves smoothly</ptxt>
</specitem>
</spec>
</test1>
<test1>
<ptxt>According to the test, proceed to the next step.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>One of the front servo motors is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>All of the front servo motors are malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LLQ03NX_05_0010" fin="false">A</down>
<right ref="RM000002LLQ03NX_05_0004" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0010" proc-id="RM23G0E___0000JNE00001">
<testtitle>SYSTEM CHECK</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>According to the test, proceed to the next step.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only the recirculation damper servo sub-assembly is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Only the damper servo sub-assembly RH is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Only the damper servo sub-assembly LH is malfunctioning</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LLQ03NX_05_0011" fin="false">A</down>
<right ref="RM000002LLQ03NX_05_0012" fin="false">B</right>
<right ref="RM000002LLQ03NX_05_0013" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0011" proc-id="RM23G0E___0000JNF00001">
<testtitle>REPLACE RECIRCULATION DAMPER SERVO SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the recirculation damper servo sub-assembly (See page <xref label="Seep01" href="RM000003AXS02MX"/>).</ptxt>
<atten4>
<ptxt>Since the servo motor cannot be inspected while it is removed from the vehicle, replace the servo motor with a new or a known good one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Check for the DTC.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497/97 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497/97 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LLQ03NX_05_0015" fin="true">A</down>
<right ref="RM000002LLQ03NX_05_0004" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0012" proc-id="RM23G0E___0000JNG00001">
<testtitle>REPLACE DAMPER SERVO SUB-ASSEMBLY RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the damper servo sub-assembly RH (See page <xref label="Seep01" href="RM000003AXS02MX"/>).</ptxt>
<atten4>
<ptxt>Since the servo motor cannot be inspected while it is removed from the vehicle, replace the servo motor with a new or a known good one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Check for the DTC.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497/97 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497/97 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LLQ03NX_05_0015" fin="true">A</down>
<right ref="RM000002LLQ03NX_05_0004" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0013" proc-id="RM23G0E___0000JNH00001">
<testtitle>REPLACE DAMPER SERVO SUB-ASSEMBLY LH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the damper servo sub-assembly LH (See page <xref label="Seep01" href="RM000003AXS02MX"/>).</ptxt>
<atten4>
<ptxt>Since the servo motor cannot be inspected while it is removed from the vehicle, replace the servo motor with a new or a known good one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Check for the DTC.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497/97 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497/97 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LLQ03NX_05_0015" fin="true">A</down>
<right ref="RM000002LLQ03NX_05_0004" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0004" proc-id="RM23G0E___0000JNC00001">
<testtitle>REPLACE AIR CONDITIONING HARNESS ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air conditioning harness assembly (See page <xref label="Seep01" href="RM000003AXS02MX"/>).</ptxt>
<atten4>
<ptxt>Since the servo motor cannot be inspected while it is removed from the vehicle, replace the servo motor with a new or a known good one and check that the condition returns to normal.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Check for the DTC.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC B1497/97 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC B1497/97 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LLQ03NX_05_0015" fin="true">A</down>
<right ref="RM000002LLQ03NX_05_0014" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0014" proc-id="RM23G0E___0000JNI00001">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING AMPLIFIER - BATTERY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G25 air conditioning amplifier assembly connector.</ptxt>
<figure>
<graphic graphicname="E197831E20" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G25-21 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Air Conditioning Amplifier Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002LLQ03NX_05_0006" fin="true">OK</down>
<right ref="RM000002LLQ03NX_05_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0006">
<testtitle>REPLACE AIR CONDITIONING AMPLIFIER ASSEMBLY<xref label="Seep01" href="RM0000039R501DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0015">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000002LLQ03NX_05_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>