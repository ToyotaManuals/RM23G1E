<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001M" variety="S001M">
<name>STEERING COLUMN</name>
<ttl id="12038_S001M_7B98Q_T00IE" variety="T00IE">
<name>POWER TILT AND POWER TELESCOPIC STEERING COLUMN SYSTEM</name>
<para id="RM000000XZL040X" category="U" type-id="303FP" name-id="SR02Y-43" from="201207">
<name>FAIL-SAFE CHART</name>
<subpara id="RM000000XZL040X_z0" proc-id="RM23G0E___0000BD500000">
<content5 releasenbr="1">
<atten4>
<ptxt>If the power source voltage to the tilt and telescopic ECU returns to normal within 10 seconds during tilt or telescopic operation, the operation will be resumed. If it returns to normal after 10 seconds have elapsed, the operation restarts when a tilt or telescopic operation signal is again input to the tilt and telescopic ECU.</ptxt>
</atten4>
<table pgwide="1">
<title>Fail-safe Operation </title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.24in"/>
<colspec colname="COL2" colwidth="2.90in"/>
<colspec colname="COL3" colwidth="2.95in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Fail-safe Operation</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2603</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tilt and Telescopic Switch Circuit Malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>The tilt and telescopic operation using the switch is suspended.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B2610</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tilt Position Sensor or Tilt Motor Circuit Malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>The tilt operation is suspended.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B2611</ptxt>
</entry>
<entry valign="middle">
<ptxt>Telescopic Position Sensor or Telescopic Motor Circuit Malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>The telescopic operation is suspended.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B2620</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECU Power Source Circuit Malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>The tilt and telescopic operation is suspended.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B2621</ptxt>
</entry>
<entry valign="middle">
<ptxt>Communication Interruption</ptxt>
</entry>
<entry valign="middle">
<ptxt>The tilt and telescopic operation is limited depending on which ECU has lost communication. For details, refer to Fail-safe Function.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>B2624</ptxt>
</entry>
<entry valign="middle">
<ptxt>Speed Signal Malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>The auto away and/or auto return function stops.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<step1>
<ptxt>FAIL-SAFE FUNCTION</ptxt>
<step2>
<ptxt>If a malfunction in the tilt position sensor signal or telescopic position sensor signal occurs while operating the power tilt or power telescopic function, the multiplex tilt and telescopic ECU stops the operation of the power tilt and power telescopic steering column system.</ptxt>
</step2>
<step2>
<ptxt>If the power source voltage to the ECU drops below approximately 8 V while operating the power tilt or power telescopic function, the ECU stops the operation of the power tilt and power telescopic steering column system.</ptxt>
</step2>
<step2>
<ptxt>If the ECU detects that the motor has locked during the operation of the power tilt or power telescopic function, the ECU stops the operation of the power tilt and power telescopic steering column system.</ptxt>
</step2>
<step2>
<ptxt>If the communication of an ECU that is related to the power tilt and power telescopic steering column system is interrupted, the control of functions by the multiplex tilt and telescopic ECU is limited as shown in the table below.</ptxt>
</step2>
</step1>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.78in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<colspec colname="COL3" colwidth="1.78in"/>
<colspec colname="COL5" colwidth="1.74in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tilt and Telescopic Operation</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Body ECU Communication Interruption</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front Power Seat Switch (Position Control ECU) Communication Interruption</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>All ECUs*1 Communication Interruption</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Manual</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X*2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Auto away/return</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Position memory (Memorization)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Position memory (Recall)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>○: Operates</ptxt>
<ptxt>X: Does not operate</ptxt>
</item>
<item>
<ptxt>*1: All ECUs that are related to the power tilt and power telescopic steering column system.</ptxt>
</item>
<item>
<ptxt>*2: The operation is permitted if the engine switch is on (IG) while the signal from the main body ECU is interrupted. The operation is prohibited if the engine switch is off while the signal from the main body ECU is interrupted.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>