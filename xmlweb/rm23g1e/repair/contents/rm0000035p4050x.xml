<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM0000035P4050X" category="C" type-id="803LI" name-id="BC3TN-71" from="201207" to="201210">
<dtccode>C1419</dtccode>
<dtcname>Acceleration Sensor Internal Circuit</dtcname>
<subpara id="RM0000035P4050X_01" type-id="60" category="03" proc-id="RM23G0E___0000A4U00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU receives signals from the acceleration sensor via the CAN communication system.</ptxt>
<ptxt>The acceleration sensor detects the vehicle's condition using 2 circuits (GL1, GL2).</ptxt>
<ptxt>If there is trouble in the bus lines between the acceleration sensor and the CAN communication system, DTC U0124 (malfunction in CAN communication with the acceleration sensor) are stored.</ptxt>
<ptxt>This DTC is also stored when calibration is not completed.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1419</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A data malfunction signal is received from the acceleration sensor.</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Acceleration sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000035P4050X_02" type-id="32" category="03" proc-id="RM23G0E___0000A4V00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1232, C1243 and C1245 (See page <xref label="Seep01" href="RM000001RFV0EHX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000035P4050X_03" type-id="51" category="05" proc-id="RM23G0E___0000A4W00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the acceleration sensor, perform calibration (See page <xref label="Seep01" href="RM000000XHR08IX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>When DTC U0124 is output together with DTC C1419, inspect and repair the trouble areas indicated by DTC U0124 first (See page <xref label="Seep02" href="RM000000YUO0J0X"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000035P4050X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000035P4050X_04_0001" proc-id="RM23G0E___0000A4X00000">
<testtitle>REPLACE ACCELERATION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the acceleration sensor (See page <xref label="Seep01" href="RM000000SS5051X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000035P4050X_04_0002" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000035P4050X_04_0002">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>