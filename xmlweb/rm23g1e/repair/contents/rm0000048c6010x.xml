<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000C" variety="S000C">
<name>2TR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000C_7B8Y2_T007Q" variety="T007Q">
<name>ENGINE UNIT</name>
<para id="RM0000048C6010X" category="A" type-id="80001" name-id="EMDE2-01" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000048C6010X_01" type-id="01" category="01">
<s-1 id="RM0000048C6010X_01_0002" proc-id="RM23G0E___000034Y00000">
<ptxt>REMOVE IGNITION COIL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 4 ignition coil connectors.</ptxt>
<figure>
<graphic graphicname="A222689" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 4 bolts and 4 ignition coils.</ptxt>
<figure>
<graphic graphicname="A222690" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0001" proc-id="RM23G0E___000054M00001">
<ptxt>REMOVE GENERATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for 80 A Type:</ptxt>
<ptxt>Remove the generator (See page <xref label="Seep01" href="RM0000017AV00GX"/>).</ptxt>
</s2>
<s2>
<ptxt>for 100 A Type:</ptxt>
<ptxt>Remove the generator (See page <xref label="Seep02" href="RM000000IYG00YX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048C6010X_01_0003" proc-id="RM23G0E___000054N00000">
<ptxt>REMOVE NO. 1 EXHAUST MANIFOLD HEAT INSULATOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and No. 1 exhaust manifold heat insulator.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>It is only necessary to move the No. 1 exhaust manifold heat insulator so that the intake pipe can be removed in a later step.</ptxt>
</item>
<item>
<ptxt>It is not possible to fully remove the No. 1 exhaust manifold heat insulator in this step.</ptxt>
</item>
</list1>
</atten4>
<figure>
<graphic graphicname="A099543E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0004" proc-id="RM23G0E___000054O00000">
<ptxt>REMOVE NO. 4 INTAKE PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 nuts, No. 4 intake pipe and 2 gaskets.</ptxt>
<figure>
<graphic graphicname="A223926" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to damage the installation surface of the gaskets.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0005" proc-id="RM23G0E___000054P00000">
<ptxt>REMOVE AIR SWITCHING VALVE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Manifold Absolute Pressure Sensor:</ptxt>
<ptxt>Disconnect the vacuum hose.</ptxt>
<figure>
<graphic graphicname="A273734" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="A223927" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 nuts and air switching valve.</ptxt>
</s2>
<s2>
<ptxt>Remove the No. 1 exhaust manifold heat insulator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0006" proc-id="RM23G0E___000054Q00000">
<ptxt>REMOVE EXHAUST MANIFOLD
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C213690" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 8 nuts and exhaust manifold.</ptxt>
</s2>
<s2>
<ptxt>Remove the gasket.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0007" proc-id="RM23G0E___000033U00000">
<ptxt>REMOVE THROTTLE WITH MOTOR BODY ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the water by-pass hose. </ptxt>
<figure>
<graphic graphicname="G037698E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 2 water by-pass hose.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the throttle position sensor and throttle control motor connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 nuts and throttle body with motor.</ptxt>
<figure>
<graphic graphicname="G037699E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the intake manifold.</ptxt>
<figure>
<graphic graphicname="G037700E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0012" proc-id="RM23G0E___000054U00001">
<ptxt>REMOVE FUEL DELIVERY PIPE WITH FUEL INJECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel delivery pipe with fuel injector (See page <xref label="Seep01" href="RM000000Q4809LX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048C6010X_01_0013" proc-id="RM23G0E___000054V00000">
<ptxt>REMOVE PURGE VSV
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the purge VSV connector.</ptxt>
<figure>
<graphic graphicname="A223339" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the 2 purge line hoses from the purge VSV.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and purge VSV.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0008" proc-id="RM23G0E___00006GK00001">
<ptxt>REMOVE INTAKE MANIFOLD
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C213694" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>for Engine Front Side:</ptxt>
<ptxt>Detach the 2 wire harness clamps from the 2 wire harness clamp brackets.</ptxt>
</s2>
<s2>
<ptxt>for Engine Rear Side:</ptxt>
<ptxt>Detach the No. 2 water by-pass hose and disconnect the No. 3 ventilation hose from the intake manifold.</ptxt>
<figure>
<graphic graphicname="C213695" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 5 bolts, 2 nuts and intake manifold.</ptxt>
<figure>
<graphic graphicname="C213696" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the intake manifold.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 wire harness clamp brackets and No. 2 fuel vapor feed hose from the intake manifold.</ptxt>
<figure>
<graphic graphicname="C213698" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0009" proc-id="RM23G0E___00006VG00001">
<ptxt>REMOVE NO. 1 COMPRESSOR MOUNTING BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and compressor mounting bracket.</ptxt>
<figure>
<graphic graphicname="A099203E07" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0010" proc-id="RM23G0E___00006VH00001">
<ptxt>REMOVE NO. 1 IDLER PULLEY SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt, idler pulley and spacer.</ptxt>
<figure>
<graphic graphicname="A222362" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000048C6010X_01_0011" proc-id="RM23G0E___000054T00001">
<ptxt>REMOVE NO. 1 WATER BY-PASS PIPE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts, water by-pass pipe and gasket.</ptxt>
<figure>
<graphic graphicname="A289060" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048C6010X_01_0017" proc-id="RM23G0E___000054Z00001">
<ptxt>REMOVE ENGINE OIL LEVEL DIPSTICK GUIDE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the oil level dipstick.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and oil level dipstick guide.</ptxt>
<figure>
<graphic graphicname="A222417" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048C6010X_01_0014" proc-id="RM23G0E___000054W00001">
<ptxt>REMOVE FRONT ENGINE MOUNTING INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 nuts and 2 engine mounting insulators.</ptxt>
<figure>
<graphic graphicname="A222414E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>LH Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>RH Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048C6010X_01_0015" proc-id="RM23G0E___000054X00001">
<ptxt>REMOVE FRONT NO. 1 ENGINE MOUNTING BRACKET LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and engine mounting bracket.</ptxt>
<figure>
<graphic graphicname="A222415" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048C6010X_01_0016" proc-id="RM23G0E___000054Y00001">
<ptxt>REMOVE FRONT NO. 1 ENGINE MOUNTING BRACKET RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and engine mounting bracket.</ptxt>
<figure>
<graphic graphicname="A222416" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>