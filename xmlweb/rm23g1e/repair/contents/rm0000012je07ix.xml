<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM0000012JE07IX" category="C" type-id="302B6" name-id="ESVJ9-04" from="201210">
<dtccode>P0340</dtccode>
<dtcname>Camshaft Position Sensor "A" Circuit (Bank 1 or Single Sensor)</dtcname>
<subpara id="RM0000012JE07IX_01" type-id="60" category="03" proc-id="RM23G0E___00003AX00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The camshaft position sensor consists of a magnet and an iron core which is wrapped with copper wire, and is installed to the cylinder head. When the camshaft rotates, each of the 3 teeth on the camshaft passes by the camshaft position sensor. This activates the internal magnet in the sensor, generating a voltage in the copper wire. The camshaft rotation is synchronized with the crankshaft rotation. When the crankshaft turns twice, the voltage is generated 3 times in the camshaft position sensor. The generated voltage in the sensor acts as a signal, allowing the ECM to determine the camshaft position. This signal is then used to control the ignition timing, fuel injection timing and VVT system.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Conditions</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Areas</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0340</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Camshaft/crankshaft misalignment is detected at an engine speed of 600 rpm or more (1 trip detection logic).</ptxt>
</item>
<item>
<ptxt>No VVT sensor signal is sent to the ECM during cranking (2 trip detection logic).</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in sensor circuit</ptxt>
</item>
<item>
<ptxt>Camshaft position sensor</ptxt>
</item>
<item>
<ptxt>Camshaft</ptxt>
</item>
<item>
<ptxt>Timing chain has jumped tooth</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>DTC P0340 indicates a malfunction relating to the camshaft position sensor circuit (the wire harness between the ECM and camshaft position sensor, and the camshaft position sensor itself).</ptxt>
</atten4>
<ptxt>Reference: Inspection using an oscilloscope</ptxt>
<figure>
<graphic graphicname="A063955E36" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The correct waveforms are as shown in the illustration.</ptxt>
</item>
<item>
<ptxt>G2+ is the camshaft position sensor signal, and NE+ is the crankshaft position sensor signal.</ptxt>
</item>
<item>
<ptxt>Grounding failure of the shielded wire may cause noise in the waveforms.</ptxt>
</item>
</list1>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Contents</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>ECM Terminal Name</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>CH1: G2 - NE-</ptxt>
<ptxt>CH2: NE+ - NE-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5 V/DIV., 20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Cranking or idling</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM0000012JE07IX_07" type-id="32" category="03" proc-id="RM23G0E___00003AY00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0335 (See page <xref label="Seep01" href="RM000000TCW0UYX_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000012JE07IX_08" type-id="51" category="05" proc-id="RM23G0E___00003AZ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. The ECM records vehicle and driving condition information as freeze frame data the moment a DTC is stored. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000012JE07IX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012JE07IX_09_0001" proc-id="RM23G0E___00003B000001">
<testtitle>INSPECT CAMSHAFT POSITION SENSOR (RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the camshaft position sensor (See page <xref label="Seep01" href="RM0000017EB00DX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012JE07IX_09_0003" fin="false">OK</down>
<right ref="RM0000012JE07IX_09_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0003" proc-id="RM23G0E___00003B100001">
<testtitle>CHECK HARNESS AND CONNECTOR (CAMSHAFT POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the camshaft position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C52-1 (G2) - C62-11 (G2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C52-2 (G-) - C62-5 (NE-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C52-1 (G2) or C62-11 (G2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="bottom" align="center">
<ptxt>C52-2 (G-) or C62-5 (NE-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the camshaft position sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012JE07IX_09_0004" fin="false">OK</down>
<right ref="RM0000012JE07IX_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0004" proc-id="RM23G0E___00003B200001">
<testtitle>CHECK SENSOR INSTALLATION (CAMSHAFT POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the camshaft position sensor installation condition.</ptxt>
<figure>
<graphic graphicname="BR03795E25" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor is installed correctly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012JE07IX_09_0017" fin="false">OK</down>
<right ref="RM0000012JE07IX_09_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0017" proc-id="RM23G0E___00003B600001">
<testtitle>CHECK VALVE TIMING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the valve timing.</ptxt>
<atten4>
<ptxt>There are no marks on the cylinder head to match-up for the purpose of checking valve timing. Valve timing can only be inspected by lining up the colored plates on the timing chain with the marks on the pulleys. It may be necessary to remove and re-install the chain to match-up the timing marks (See page <xref label="Seep01" href="RM000000YMY027X_01_0006"/>).</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000012JE07IX_09_0015" fin="false">OK</down>
<right ref="RM0000012JE07IX_09_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0015" proc-id="RM23G0E___00003B500001">
<testtitle>INSPECT CAMSHAFT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the teeth of the camshaft.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Camshaft teeth do not have any cracks or deformation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012JE07IX_09_0006" fin="false">OK</down>
<right ref="RM0000012JE07IX_09_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0006" proc-id="RM23G0E___00003B300001">
<testtitle>REPLACE CAMSHAFT POSITION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the camshaft position sensor (See page <xref label="Seep01" href="RM0000017EC00DX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012JE07IX_09_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0012" proc-id="RM23G0E___00003B400001">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P0340)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear DTCs (See page <xref label="Seep01" href="RM000000PDK0Z8X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>DTC P0340 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<atten4>
<ptxt>If the engine does not start, replace the ECM.</ptxt>
</atten4>
</content6>
<res>
<down ref="RM0000012JE07IX_09_0008" fin="true">A</down>
<right ref="RM0000012JE07IX_09_0011" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0013">
<testtitle>REPLACE CAMSHAFT POSITION SENSOR<xref label="Seep01" href="RM0000017EC00DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0009">
<testtitle>SECURELY REINSTALL CAMSHAFT POSITION SENSOR<xref label="Seep01" href="RM0000017EC00DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0016">
<testtitle>ADJUST VALVE TIMING<xref label="Seep01" href="RM000000YN0020X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0010">
<testtitle>REPLACE CAMSHAFT<xref label="Seep01" href="RM000000YN0020X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0008">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM0000012JE07IX_09_0011">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202UX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>