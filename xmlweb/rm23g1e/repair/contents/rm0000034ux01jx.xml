<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97V_T00HJ" variety="T00HJ">
<name>BRAKE ACTUATOR</name>
<para id="RM0000034UX01JX" category="A" type-id="80001" name-id="BCDOG-01" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM0000034UX01JX_01" type-id="01" category="01">
<s-1 id="RM0000034UX01JX_01_0001" proc-id="RM23G0E___0000ASA00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000034UX01JX_01_0012" proc-id="RM23G0E___0000ASG00000">
<ptxt>REMOVE AIR CLEANER CASE SUB-ASSEMBLY (for 2TR-FE)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000004637009X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000034UX01JX_01_0011" proc-id="RM23G0E___0000ASF00000">
<ptxt>REMOVE AIR CLEANER CASE ASSEMBLY (for 5L-E)</ptxt>
<content1 releasenbr="1">
<ptxt>(See page <xref label="Seep01" href="RM000000FYL013X"/>)</ptxt>
</content1>
</s-1>
<s-1 id="RM0000034UX01JX_01_0002" proc-id="RM23G0E___0000ASB00000">
<ptxt>REMOVE BRAKE ACTUATOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Release the lock lever and disconnect the brake actuator connector.</ptxt>
<figure>
<graphic graphicname="C213640" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to allow brake fluid to enter the removed connector.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a union nut wrench, disconnect the 6 brake lines from the brake actuator.</ptxt>
</s2>
<s2>
<ptxt>Place tags or marks to identify the installation locations of each brake line.</ptxt>
<figure>
<graphic graphicname="C213641E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: To front wheel cylinder RH</ptxt>
</item>
<item>
<ptxt>*2: To front wheel cylinder LH</ptxt>
</item>
<item>
<ptxt>*3: To rear wheel cylinder RH</ptxt>
</item>
<item>
<ptxt>*4: To rear wheel cylinder LH</ptxt>
</item>
<item>
<ptxt>*5: From front master cylinder</ptxt>
</item>
<item>
<ptxt>*6: From rear master cylinder</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Remove the 3 nuts and brake actuator with bracket from the body.</ptxt>
<figure>
<graphic graphicname="C213642" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not damage the brake lines or wire harness.</ptxt>
</item>
<item>
<ptxt>Do not hold the actuator by the connector.</ptxt>
</item>
<item>
<ptxt>If the actuator is dropped, replace it.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000034UX01JX_01_0003" proc-id="RM23G0E___0000ASC00000">
<ptxt>REMOVE BRAKE ACTUATOR BRACKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and brake actuator bracket from the brake actuator.</ptxt>
<figure>
<graphic graphicname="C213643" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not damage the brake lines or wire harness.</ptxt>
</item>
<item>
<ptxt>Do not hold the actuator by the connector.</ptxt>
</item>
<item>
<ptxt>If the actuator is dropped, replace it.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000034UX01JX_01_0009" proc-id="RM23G0E___0000ASD00000">
<ptxt>REMOVE BRAKE ACTUATOR CASE COLLAR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 brake actuator case collars from the brake actuator bolt cushions.</ptxt>
<figure>
<graphic graphicname="C213644E01" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000034UX01JX_01_0010" proc-id="RM23G0E___0000ASE00000">
<ptxt>REMOVE BRAKE ACTUATOR BOLT CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 brake actuator bolt cushions from the brake actuator bracket.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>