<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0006" variety="S0006">
<name>5L-E ENGINE CONTROL</name>
<ttl id="12005_S0006_7B8VX_T005L" variety="T005L">
<name>ECD SYSTEM</name>
<para id="RM000000VR600GX" category="D" type-id="303F4" name-id="ESMFG-02" from="201207">
<name>DTC CHECK / CLEAR</name>
<subpara id="RM000000VR600GX_z0" proc-id="RM23G0E___000018M00000">
<content5 releasenbr="1">
<atten3>
<ptxt>Intelligent tester only:</ptxt>
<list1 type="nonmark">
<item>
<ptxt>When the diagnosis system is changed from normal mode to check mode or vice versa, all the DTCs and freeze frame data recorded in normal mode will be cleared. Before changing modes, always check and make a note of any DTCs and freeze frame data.</ptxt>
</item>
</list1>
</atten3>
<step1>
<ptxt>CHECK DTC (using the intelligent tester)</ptxt>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
<figure>
<graphic graphicname="A114427E03" width="2.775699831in" height="2.775699831in"/>
</figure>
</step2>
<step2>
<ptxt>Check and make a note of DTCs and freeze frame data.</ptxt>
</step2>
<step2>
<ptxt>Confirm the details of the DTCs (See page <xref label="Seep01" href="RM000000VQT00GX"/>).</ptxt>
</step2>
</step1>
<step1>
<ptxt>CHECK DTC (not using the intelligent tester)</ptxt>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
<figure>
<graphic graphicname="A082779E07" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Using SST, connect terminals 13 (TC) and 4 (CG) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
</step2>
<step2>
<ptxt>Read the DTCs by observing the MIL. If no DTC is stored, the MIL blinks as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A073556E11" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Example</ptxt>
<figure>
<graphic graphicname="A110552E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>DTCs 12 and 31 are stored and the MIL starts outputting the DTCs as shown in the illustration. The MIL blinking pattern of DTC 12 will be output first.</ptxt>
<atten4>
<ptxt>If 2 or more DTCs are stored, the MIL will output the DTCs in numerical order.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>A 2.5 second pause will occur. This pause will occur between the MIL blinking patterns of each DTC.</ptxt>
</step3>
<step3>
<ptxt>The MIL blinking pattern of DTC 31 will be output.</ptxt>
</step3>
<step3>
<ptxt>A 4.5 second pause will occur. This pause will occur when the MIL blinking pattern is the last of a string of multiple DTCs.</ptxt>
</step3>
<step3>
<ptxt>The MIL will repeat the output of the string of DTCs again.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Check the details of the malfunction using the DTC chart (See page <xref label="Seep02" href="RM000000VQT00GX"/>).</ptxt>
</step2>
<step2>
<ptxt>After completing the check, disconnect terminals 13 (TC) and 4 (CG) to turn off the output.</ptxt>
</step2>
<step2>
<ptxt>Confirm the details of the DTCs (See page <xref label="Seep03" href="RM000000VQT00GX"/>).</ptxt>
</step2>
</step1>
<step1>
<ptxt>CLEAR DTCS AND FREEZE FRAME DATA (using the intelligent tester)</ptxt>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
<figure>
<graphic graphicname="A114426E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON (do not start the engine) and turn the tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC / Clear.</ptxt>
</step2>
<step2>
<ptxt>Clear DTCs and freeze frame data by pressing the YES button on the tester.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CLEAR DTCS AND FREEZE FRAME DATA (not using the intelligent tester)</ptxt>
<step2>
<ptxt>Perform either one of the following operations.</ptxt>
<figure>
<graphic graphicname="A227205E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<step3>
<ptxt>Remove the EFI fuse from the engine room relay block, located inside the engine compartment, for more than 1 minute.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Room Relay Block</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>EFI Fuse</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>Disconnect the cable from the negative (-) battery terminal for more than 1 minute.</ptxt>
<atten3>
<ptxt>After reconnecting the battery cable, the radio and clock need to be adjusted as they were previously.</ptxt>
</atten3>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>