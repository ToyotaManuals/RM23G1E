<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000C" variety="S000C">
<name>2TR-FE ENGINE MECHANICAL</name>
<ttl id="12007_S000C_7B8Y8_T007W" variety="T007W">
<name>CAMSHAFT</name>
<para id="RM000000YMY027X" category="A" type-id="30014" name-id="EM8VE-05" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000000YMY027X_01" type-id="01" category="01">
<s-1 id="RM000000YMY027X_01_0001" proc-id="RM23G0E___000056T00001">
<ptxt>INSTALL CAMSHAFT TIMING GEAR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the pin hole and straight pin and install the camshaft timing gear to the camshaft.</ptxt>
<figure>
<graphic graphicname="A101352E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Straight Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Pin Hole</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Lightly press the gear against the camshaft and turn the gear. Push further at the position where the pin enters the groove.</ptxt>
</s2>
<s2>
<ptxt>Check that there is no gap between the flange of the gear and the camshaft.</ptxt>
</s2>
<s2>
<ptxt>With the camshaft timing gear fixed in place, install the flange bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>78</t-value1>
<t-value2>795</t-value2>
<t-value4>58</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Check that the camshaft timing gear can move in the retard direction and becomes locked at the most retarded position.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000YMY027X_01_0003" proc-id="RM23G0E___000056N00001">
<ptxt>INSTALL VALVE LASH ADJUSTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Inspect each valve lash adjuster before installing it (See page <xref label="Seep01" href="RM00000447K00NX_01_0017"/>).</ptxt>
</s2>
<s2>
<ptxt>Install the 16 valve lash adjusters to the cylinder head.</ptxt>
<atten3>
<ptxt>Install each lash adjuster to the same place it was removed from.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000YMY027X_01_0004" proc-id="RM23G0E___000056S00001">
<ptxt>INSTALL NO. 1 VALVE ROCKER ARM SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply clean engine oil to the valve lash adjuster tips and valve stem cap surfaces.</ptxt>
<figure>
<graphic graphicname="A222397E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 16 valve rocker arms as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Valve Stem Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Valve Rocker Arm</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Valve Lash Adjuster</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Install each valve rocker arm to the same place it was removed from.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000YMY027X_01_0005" proc-id="RM23G0E___00005CW00001">
<ptxt>INSTALL CAMSHAFT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply clean engine oil to the camshaft cams and cylinder head journals.</ptxt>
<figure>
<graphic graphicname="A097873E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the timing chain to the camshaft timing gear with the painted mark of the link aligned with the timing mark of the camshaft timing gear.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Timing Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Position the 2 camshafts as shown in the illustration. </ptxt>
<figure>
<graphic graphicname="A101356E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Align the paint mark and timing mark before positioning the camshaft.</ptxt>
</item>
<item>
<ptxt>Before and after positioning the camshaft and No. 2 camshaft, check that the rocker arm is firmly set on the lash adjuster. </ptxt>
<figure>
<graphic graphicname="A222397E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</item>
</list1>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Valve Stem Cap</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Valve Rocker Arm</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Valve Lash Adjuster</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Temporarily install the No. 1 camshaft bearing cap.</ptxt>
<figure>
<graphic graphicname="A101357" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Check the proper location of each camshaft bearing cap and install each one.</ptxt>
</s2>
<s2>
<ptxt>Install a new O-ring to the No. 1 camshaft bearing cap.</ptxt>
<figure>
<graphic graphicname="A101310E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Temporarily install the oil delivery pipe.</ptxt>
</s2>
<s2>
<ptxt>Install the 21 bolts and tighten them in the order shown in the illustration. </ptxt>
<figure>
<graphic graphicname="A170574E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>12</t-value1>
<t-value2>122</t-value2>
<t-value4>9</t-value4>
</torqueitem>
<subtitle>except bolt A</subtitle>
<torqueitem>
<t-value1>16</t-value1>
<t-value2>158</t-value2>
<t-value4>11</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000000YMY027X_01_0006" proc-id="RM23G0E___00005CX00001">
<ptxt>INSTALL CAMSHAFT TIMING SPROCKET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Rotate the camshaft so that the camshaft timing mark and No. 2 camshaft knock pin are as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A097877E05" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Timing Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Knock Pin</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Groove</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Turn the crankshaft pulley and align its groove with the "0" timing mark of the timing chain cover.</ptxt>
</s2>
<s2>
<ptxt>Install the timing chain to the camshaft timing sprocket with the paint mark aligned with the timing marks on the camshaft timing sprocket.</ptxt>
<figure>
<graphic graphicname="A097878E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Paint Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Timing Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Align the No. 2 camshaft knock pin and camshaft timing sprocket pin hole. Then install the camshaft timing sprocket to the No. 2 camshaft. </ptxt>
<atten3>
<ptxt>If the knock pin and pin hole are difficult to align, slightly rotate the No. 2 camshaft back and forth using the hexagonal part of the camshaft. Then attempt alignment again.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Hold the camshaft with a wrench and tighten the sprocket bolt. </ptxt>
<figure>
<graphic graphicname="A097863E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>78</t-value1>
<t-value2>795</t-value2>
<t-value4>58</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Hold</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Tighten</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the hexagon wrench from the chain tensioner.</ptxt>
</s2>
<s2>
<ptxt>Apply adhesive to 2 or 3 threads of the straight screw plug.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1324, Three Bond 1324 or equivalent</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Remove any oil from the bolt hole.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a 10 mm socket hexagon wrench, install the straight screw plug. </ptxt>
<torque>
<torqueitem>
<t-value1>17</t-value1>
<t-value2>169</t-value2>
<t-value4>12</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000000YMY027X_01_0023" proc-id="RM23G0E___000057000001">
<ptxt>INSTALL TIMING CHAIN GUIDE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new O-ring to the camshaft bearing cap.</ptxt>
<figure>
<graphic graphicname="A101304E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the timing chain guide with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>10</t-value1>
<t-value2>102</t-value2>
<t-value4>7</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000000YMY027X_01_0024" proc-id="RM23G0E___000057A00001">
<ptxt>INSTALL CYLINDER HEAD COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new cover gaskets to the head cover.</ptxt>
<figure>
<graphic graphicname="A101368E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Apply seal packing to the places shown in the illustration.</ptxt>
<spec>
<title>Seal packing</title>
<specitem>
<ptxt>Toyota Genuine Seal Packing Black, Three Bond 1207B or equivalent</ptxt>
</specitem>
</spec>
<spec>
<title>Seal packing diameter</title>
<specitem>
<ptxt>4.0 mm (0.157 in.)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Seal Packing</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Remove any oil from the contact surface.</ptxt>
</item>
<item>
<ptxt>Install the head cover within 3 minutes after applying seal packing.</ptxt>
</item>
<item>
<ptxt>Do not start the engine for at least 4 hours after the installation.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the head cover with the 19 bolts and 2 nuts.</ptxt>
<figure>
<graphic graphicname="A101292E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Uniformly tighten the 19 bolts and 2 nuts in the order shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>In numerical order, confirm that the bolts labeled 1 to 8 are tightened to the specified torque. Tighten the bolts as necessary.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0025" proc-id="RM23G0E___000034F00000">
<ptxt>INSTALL CAMSHAFT POSITION SENSOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply a light coat of engine oil to the O-ring of the camshaft position sensor.</ptxt>
<figure>
<graphic graphicname="G037911E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>O-Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the camshaft position sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.5</t-value1>
<t-value2>87</t-value2>
<t-value3>75</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>Make sure that the O-ring is not cracked or jammed when installing.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the camshaft position sensor connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0026" proc-id="RM23G0E___000034W00000">
<ptxt>INSTALL IGNITION COIL ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the 4 ignition coils with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>9.0</t-value1>
<t-value2>92</t-value2>
<t-value3>80</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the 4 ignition coil connectors.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0027" proc-id="RM23G0E___00005AL00000">
<ptxt>INSTALL FAN SHROUD
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fan pulley to the water pump.</ptxt>
</s2>
<s2>
<ptxt>Place the shroud together with the coupling fan between the radiator and engine.</ptxt>
<atten3>
<ptxt>Be careful not to damage the radiator core.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Temporarily install the fluid coupling fan to the water pump with the 4 nuts. Tighten the nuts as much as possible by hand.</ptxt>
</s2>
<s2>
<ptxt>Attach the claws of the shroud to the radiator as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A223328" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the shroud with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the fan and generator V-belt (See page <xref label="Seep01" href="RM000000YMN01ZX"/>).</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 nuts of the fluid coupling fan.</ptxt>
<torque>
<torqueitem>
<t-value1>25</t-value1>
<t-value2>255</t-value2>
<t-value4>18</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<figure>
<graphic graphicname="A223329" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Attach the claw to close the flexible hose clamp as shown in the illustration.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0028" proc-id="RM23G0E___00005AM00000">
<ptxt>INSTALL RADIATOR RESERVOIR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the radiator reservoir with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the reservoir hose to the radiator.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0029" proc-id="RM23G0E___000033L00000">
<ptxt>INSTALL INTAKE AIR CONNECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the intake air connector with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the vacuum hose.</ptxt>
</s2>
<s2>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Connect the No. 2 ventilation hose.</ptxt>
</s2>
<s2>
<ptxt>w/ Manifold absolute pressure sensor:</ptxt>
<s3>
<ptxt>Connect the vacuum hose.</ptxt>
</s3>
<s3>
<ptxt>Attach the 2 clamps.</ptxt>
</s3>
<s3>
<ptxt>Connect the air pressure sensor connector.</ptxt>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0030" proc-id="RM23G0E___00005A700001">
<ptxt>INSTALL AIR CLEANER CAP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the air cleaner hose, align its matchmark with the matchmark of the air cleaner cap as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A230208E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Matchmark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Upper Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Front</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the 4 clamps.</ptxt>
</s2>
<s2>
<ptxt>Install the ground wire and clamp with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.5</t-value1>
<t-value2>87</t-value2>
<t-value3>75</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the mass air flow meter connector and attach the 3 clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0031" proc-id="RM23G0E___00005CY00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000000YMY027X_01_0032" proc-id="RM23G0E___00005AO00000">
<ptxt>ADD ENGINE OIL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean and install the oil drain plug and a new gasket.</ptxt>
<torque>
<torqueitem>
<t-value1>38</t-value1>
<t-value2>382</t-value2>
<t-value4>28</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Add fresh engine oil.</ptxt>
<spec>
<title>Standard Oil Grade</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Oil Grade</ptxt>
</entry>
<entry align="center">
<ptxt>Oil Viscosity (SAE)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>API grade SL "energy-conserving", SM "energy-conserving", SN "resource-conserving" or ILSAC multigrade engine oil</ptxt>
</entry>
<entry valign="middle" align="center">
<list1 type="unordered">
<item>
<ptxt>0W-20</ptxt>
</item>
<item>
<ptxt>5W-20</ptxt>
</item>
<item>
<ptxt>5W-30</ptxt>
</item>
<item>
<ptxt>10W-30</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>API grade SL, SM or SN multigrade engine oil</ptxt>
</entry>
<entry valign="middle" align="center">
<list1 type="unordered">
<item>
<ptxt>15W-40</ptxt>
</item>
<item>
<ptxt>20W-50</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Capacity</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Drain and refill without oil filter change</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 liters (5.3 US qts, 4.4 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Drain and refill with oil filter change</ptxt>
</entry>
<entry>
<ptxt>5.7 liters (6.0 US qts, 5.0 Imp. qts)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Dry fill</ptxt>
</entry>
<entry>
<ptxt>6.1 liters (6.4 US qts, 5.4 Imp. qts)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Install the oil filler cap.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0036" proc-id="RM23G0E___00005AP00000">
<ptxt>INSPECT FOR OIL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine. Make sure that there are no oil leaks from the areas that were worked on.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0034" proc-id="RM23G0E___000053500000">
<ptxt>INSPECT ENGINE OIL LEVEL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up the engine, stop the engine and wait 5 minutes.</ptxt>
</s2>
<s2>
<ptxt>Check that the oil level is between the dipstick low level mark and full level mark. </ptxt>
<ptxt>If the level is low, check for leakage and add oil up to the full level mark.</ptxt>
<atten3>
<ptxt>Do not fill engine oil above the full level mark.</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0037" proc-id="RM23G0E___000053100000">
<ptxt>INSPECT IGNITION TIMING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Warm up and stop the engine.</ptxt>
</s2>
<s2>
<ptxt>When using an intelligent tester:</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Start the engine and idle it.</ptxt>
</s3>
<s3>
<ptxt>Turn the intelligent tester on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / IGN Advance.</ptxt>
<spec>
<title>Standard ignition timing</title>
<specitem>
<ptxt>5 to 15° BTDC @ idle</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Refer to the intelligent tester operator's manual for further details.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When checking the ignition timing, the transaxle should be in neutral or park. </ptxt>
</item>
<item>
<ptxt>Switch off all the accessories and the A/C before connecting the intelligent tester.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Check that the ignition timing advances immediately when the engine speed is increased.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Connect the TC and TE1.</ptxt>
</s3>
<s3>
<ptxt>Monitor IGN Advance.</ptxt>
</s3>
<s3>
<ptxt>Perform the Active Test.</ptxt>
<spec>
<title>Standard ignition timing</title>
<specitem>
<ptxt>3 to 7° BTDC @ idle</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Refer to the intelligent tester operator's manual for further details.</ptxt>
</atten4>
<atten3>
<ptxt>When checking the ignition timing, the transmission should be in neutral or park.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>When not using the intelligent tester:</ptxt>
<s3>
<ptxt>Loosen the hose clamp from the throttle body side.</ptxt>
</s3>
<s3>
<ptxt>Remove the 3 bolts and disconnect the intake air connector.</ptxt>
<atten4>
<ptxt>Move the intake air connector so that the tester probe of a timing light can be connected to the wire of the ignition coil for the No. 1 cylinder.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Connect the tester probe of a timing light to the wire of the ignition coil connector for the No. 1 cylinder.</ptxt>
<figure>
<graphic graphicname="A218392" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten3>
<ptxt>Use a timing light that detects primary signals.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Connect the intake air connector.</ptxt>
<atten4>
<ptxt>Return the parts to their original positions so that the engine can be started.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Start the engine and idle it.</ptxt>
</s3>
<s3>
<ptxt>Using SST, connect terminals 13 (TC) and 4 (CG) of the DLC3.</ptxt>
<figure>
<graphic graphicname="A206183E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Confirm the terminal numbers before connecting them. Connecting the wrong terminals can damage the engine.</ptxt>
</item>
<item>
<ptxt>When checking the ignition timing, the shift lever should be in neutral or P.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Using the timing light, check the ignition timing.</ptxt>
<spec>
<title>Standard ignition timing</title>
<specitem>
<ptxt>3 to 7° BTDC @ idle</ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Turn all the electrical systems and the A/C off.</ptxt>
</item>
<item>
<ptxt>When checking the ignition timing, the shift lever should be in neutral or P.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Remove SST from the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Check the ignition timing.</ptxt>
<spec>
<title>Standard ignition timing</title>
<specitem>
<ptxt>5 to 15° BTDC @ idle</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Check that the ignition timing advances immediately when the engine speed is increased.</ptxt>
</s3>
<s3>
<ptxt>Turn the ignition switch off.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the timing light from the engine.</ptxt>
</s3>
<s3>
<ptxt>Connect the intake air connector with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s3>
<s3>
<ptxt>Tighten the hose clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0038" proc-id="RM23G0E___000053700000">
<ptxt>INSPECT ENGINE IDLE SPEED
</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Warm up and stop the engine.</ptxt>
</s2>
<s2>
<ptxt>When using the intelligent tester:</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s3>
<s3>
<ptxt>Start the engine and idle it.</ptxt>
</s3>
<s3>
<ptxt>Turn the intelligent tester on.</ptxt>
</s3>
<s3>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Engine Speed.</ptxt>
<spec>
<title>Standard idle speed</title>
<specitem>
<ptxt>600 to 700 rpm</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>Refer to the intelligent tester operator's manual for further details.</ptxt>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Turn all the electrical systems and the A/C off.</ptxt>
</item>
<item>
<ptxt>When checking the idling speed, the shift lever should be in neutral or P.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Turn the ignition switch off.</ptxt>
</s3>
<s3>
<ptxt>Disconnect the intelligent tester from the DLC3.</ptxt>
</s3>
</s2>
<s2>
<ptxt>When not using the intelligent tester:</ptxt>
<figure>
<graphic graphicname="A206183E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s3>
<ptxt>Using SST, connect a tachometer probe to terminal 9 (TAC) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Confirm the terminal numbers before connecting them. Connecting the wrong terminals can damage the engine.</ptxt>
</item>
<item>
<ptxt>Turn all the electrical systems and the A/C off.</ptxt>
</item>
<item>
<ptxt>When checking the idling speed, the shift lever should be in neutral or P.</ptxt>
</item>
</list1>
</atten3>
</s3>
<s3>
<ptxt>Check the idle speed.</ptxt>
<spec>
<title>Standard idle speed</title>
<specitem>
<ptxt>600 to 700 rpm</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Disconnect the tachometer probe from the DLC3.</ptxt>
</s3>
</s2>
<s2>
<ptxt>If the engine speed is not as specified, update the idle speed control (ISC) learned value using either procedure 1 or procedure 2 below, and then check the engine speed again.</ptxt>
<s3>
<ptxt>Procedure 1:</ptxt>
<ptxt>After completely warming up the engine, drive the vehicle at 30 km/h (19 mph) for 30 seconds or more and idle the engine for 30 seconds or more. Repeat this pattern 5 times or more.</ptxt>
<figure>
<graphic graphicname="A172908E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine completely warmed up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle being driven at 30 km/h (19 mph)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 time</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Procedure 2:</ptxt>
<ptxt>After completely warming up the engine, stop the engine and then idle it. Repeat this pattern 5 times or more.</ptxt>
<figure>
<graphic graphicname="A172909E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine completely warmed up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine stopped</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine started</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*e</ptxt>
</entry>
<entry valign="middle">
<ptxt>1 time</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Be sure to idle the engine for 150 seconds or more for each time the pattern is repeated.</ptxt>
</item>
<item>
<ptxt>If the engine speed is not as specified after performing the procedure above, inspect the idle speed control system as it is malfunctioning.</ptxt>
</item>
</list1>
</atten4>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0039" proc-id="RM23G0E___00005A800001">
<ptxt>INSTALL UPPER RADIATOR SUPPORT SEAL
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the upper radiator support seal with the 13 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0040" proc-id="RM23G0E___00005AB00001">
<ptxt>INSTALL NO. 1 ENGINE UNDER COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Hook the engine under cover to the vehicle body as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A224743" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>296</t-value2>
<t-value4>21</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000YMY027X_01_0041" proc-id="RM23G0E___00005AC00001">
<ptxt>INSTALL FRONT BUMPER COVER LOWER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front bumper cover lower with the 5 bolts and clip.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>