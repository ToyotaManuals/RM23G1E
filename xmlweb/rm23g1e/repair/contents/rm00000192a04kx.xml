<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S0029" variety="S0029">
<name>WINDOW / GLASS</name>
<ttl id="12064_S0029_7B9FZ_T00PN" variety="T00PN">
<name>QUARTER WINDOW GLASS (for 3 Door)</name>
<para id="RM00000192A04KX" category="A" type-id="80001" name-id="WS582-03" from="201210">
<name>REMOVAL</name>
<subpara id="RM00000192A04KX_01" type-id="11" category="10" proc-id="RM23G0E___0000IGV00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the LH and RH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM00000192A04KX_02" type-id="01" category="01">
<s-1 id="RM00000192A04KX_02_0090" proc-id="RM23G0E___0000IGY00001">
<ptxt>REMOVE ROOF HEADLINING ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the roof headlining assembly (See page <xref label="Seep01" href="RM0000046JQ00KX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000192A04KX_02_0091" proc-id="RM23G0E___0000IGZ00001">
<ptxt>REMOVE NO. 1 ROOF SIDE RAIL GARNISH LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B242805E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 5 clips.</ptxt>
</s2>
<s2>
<ptxt>Cut off clip A.</ptxt>
</s2>
<s2>
<ptxt>Remove the roof side rail garnish.</ptxt>
</s2>
<s2>
<ptxt>Remove clip A from the vehicle body.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Clip A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000192A04KX_02_0025" proc-id="RM23G0E___0000IGW00001">
<ptxt>REMOVE QUARTER WINDOW ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B239030E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>Apply protective tape to the outer surface of the vehicle body to prevent scratches.</ptxt>
</atten4>
<atten3>
<ptxt>When separating the quarter window glass from the vehicle, be careful not to damage the vehicle paint or interior/exterior ornaments.</ptxt>
</atten3>
<s2>
<ptxt>From the interior, insert a piano wire between the vehicle body and quarter window glass as shown in the illustration.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Protective Tape</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Piano Wire</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<figure>
<graphic graphicname="B242804E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Tie objects that can serve as handles (for example, wooden blocks) to both wire ends.</ptxt>
</s2>
<s2>
<ptxt>Cut through the adhesive by pulling the piano wire around the quarter window glass.</ptxt>
<atten3>
<ptxt>Leave as much adhesive on the vehicle body as possible when removing the quarter window glass.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Piano Wire</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Wooden Blocks</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry>
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using suction cups, remove the quarter window glass.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000192A04KX_02_0026" proc-id="RM23G0E___0000IGX00001">
<ptxt>CLEAN VEHICLE BODY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B106081E18" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Clean and shape the contact surface of the vehicle body.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Adhesive</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry>
<ptxt>Vehicle Body</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<s3>
<ptxt>On the contact surface of the vehicle body, use a knife to cut away excess adhesive as shown in the illustration.</ptxt>
<atten4>
<ptxt>Leave as much adhesive on the vehicle body as possible.</ptxt>
</atten4>
<atten3>
<ptxt>Be careful not to damage the vehicle body.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>Clean the contact surface of the vehicle body with cleaner.</ptxt>
<atten4>
<ptxt>Even if all the adhesive has been removed, clean the vehicle body.</ptxt>
</atten4>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>