<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12059_S0024" variety="S0024">
<name>SEAT</name>
<ttl id="12059_S0024_7B9CW_T00MK" variety="T00MK">
<name>REAR POWER SEAT CONTROL SYSTEM</name>
<para id="RM0000046CD005X" category="J" type-id="30303" name-id="SE5B9-02" from="201207" to="201210">
<dtccode/>
<dtcname>Parking Brake Switch Circuit</dtcname>
<subpara id="RM0000046CD005X_01" type-id="60" category="03" proc-id="RM23G0E___0000G8O00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit includes the parking brake switch assembly and fold seat control ECU.</ptxt>
</content5>
</subpara>
<subpara id="RM0000046CD005X_02" type-id="32" category="03" proc-id="RM23G0E___0000G8P00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B239924E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000046CD005X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000046CD005X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000046CD005X_04_0001" proc-id="RM23G0E___0000G8Q00000">
<testtitle>CHECK BRAKE WARNING LIGHT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the brake warning light comes on when the parking brake is applied and goes off when the parking brake is released.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The brake warning light operates as specified above.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000046CD005X_04_0002" fin="false">OK</down>
<right ref="RM0000046CD005X_04_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046CD005X_04_0002" proc-id="RM23G0E___0000G8R00000">
<testtitle>CHECK HARNESS AND CONNECTOR (FOLD SEAT CONTROL ECU - PARKING BRAKE SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the d1*1 or c1*2 ECU connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LH Side</ptxt>
</item>
<item>
<ptxt>*2: for RH Side</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Disconnect the G77 parking brake switch connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LH Side</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>d1-40 (PKB1) - G77-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>d1-40 (PKB1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RH Side</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>c1-40 (PKB1) - G77-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>c1-40 (PKB1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000046CD005X_04_0003" fin="true">OK</down>
<right ref="RM0000046CD005X_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046CD005X_04_0003">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000001YWK00PX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000046CD005X_04_0004" proc-id="RM23G0E___0000G8S00000">
<testtitle>INSPECT PARKING BRAKE SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the parking brake switch assembly (See page <xref label="Seep01" href="RM000003D7500NX"/>).</ptxt>
<figure>
<graphic graphicname="E194874E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - Switch body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On (Shaft is not pressed)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1 - Switch body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off (Shaft is pressed)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000046CD005X_04_0005" fin="true">OK</down>
<right ref="RM0000046CD005X_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000046CD005X_04_0005">
<testtitle>REPLACE FOLD SEAT CONTROL ECU<xref label="Seep01" href="RM0000045DP009X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000046CD005X_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000046CD005X_04_0007">
<testtitle>REPLACE PARKING BRAKE SWITCH ASSEMBLY<xref label="Seep01" href="RM000003D7500NX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>