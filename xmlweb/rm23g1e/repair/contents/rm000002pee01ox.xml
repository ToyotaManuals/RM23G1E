<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001B" variety="S001B">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001B_7B96L_T00G9" variety="T00G9">
<name>AIR SUSPENSION SYSTEM</name>
<para id="RM000002PEE01OX" category="C" type-id="802LC" name-id="SC21S-02" from="201210">
<dtccode>C1761/61</dtccode>
<dtcname>Continuous Current to Compressor Motor</dtcname>
<subpara id="RM000002PEE01OX_01" type-id="60" category="03" proc-id="RM23G0E___00009MI00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The signal from the suspension control ECU operates the AIR SUS relay and the height control compressor motor starts. The height control compressor motor operates until the targeted vehicle height is reached. Then the height control sensor sends the signal to the suspension control ECU, and stops the height control compressor motor.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.35in"/>
<colspec colname="COL2" colwidth="2.61in"/>
<colspec colname="COL3" colwidth="3.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1761/61</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met a total of 2 times or more with the ignition switch ON:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Power is continuously supplied to the height control compressor motor for 100 seconds.</ptxt>
</item>
<item>
<ptxt>Power is supplied to the height control compressor motor for 100 seconds or more within a 240 second period.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air leakage from air tube or each valve</ptxt>
</item>
<item>
<ptxt>Clogging in air tube or each valve</ptxt>
</item>
<item>
<ptxt>AIR SUS relay (Suspension control relay)</ptxt>
</item>
<item>
<ptxt>Leveling solenoid valve (No. 2 height control valve)</ptxt>
</item>
<item>
<ptxt>Gate solenoid valve (No. 2 height control valve)</ptxt>
</item>
<item>
<ptxt>Exhaust valve (Height control compressor)</ptxt>
</item>
<item>
<ptxt>Compressor motor (Height control compressor)</ptxt>
</item>
<item>
<ptxt>Suspension control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002PEE01OX_03" type-id="51" category="05" proc-id="RM23G0E___00009MJ00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the suspension control ECU, perform registration (See page <xref label="Seep02" href="RM000003X4500LX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>If DTC C1782/82 is output at the same time, perform troubleshooting for C1782/82 first (See page <xref label="Seep01" href="RM000001N93017X"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002PEE01OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002PEE01OX_04_0014" proc-id="RM23G0E___00009MN00001">
<testtitle>CUSTOMER PROBLEM ANALYSIS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Ask the customer about the vehicle and usage conditions when the DTC was stored.</ptxt>
<atten4>
<ptxt>DTC C1761/61 can be stored under unusual vehicle or usage conditions.</ptxt>
<table pgwide="1">
<tgroup cols="2" align="left">
<colspec colname="COL1" align="center" colwidth="1.69in"/>
<colspec colname="COL2" align="left" colwidth="5.39in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Points to confirm</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Confirm if there were any foreign objects caught between the body and tires.</ptxt>
</item>
<item>
<ptxt>Confirm if the vehicle has been driven on an unpaved surface or with a wheel not contacting the ground.</ptxt>
</item>
<item>
<ptxt>Confirm if the vehicle has been raised with an excess load.</ptxt>
</item>
<item>
<ptxt>Confirm if the vehicle height mode has been continuously changed using the height control switch, causing the vehicle to raise and lower repeatedly.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000002PEE01OX_04_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0004" proc-id="RM23G0E___00009ML00001">
<testtitle>CHECK DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000001G7P01XX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine and wait for 4 minutes or more.</ptxt>
</test1>
<test1>
<ptxt>Set the vehicle height to HI, wait until the vehicle height change is complete, and then return the vehicle height to normal.</ptxt>
</test1>
<test1>
<ptxt>Check the DTC (See page <xref label="Seep02" href="RM000001G7P01XX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.04in"/>
<colspec colname="COL2" colwidth="1.09in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC C1761/61 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1761/61 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1761/61 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002PEE01OX_04_0001" fin="false">A</down>
<right ref="RM000002PEE01OX_04_0017" fin="true">B</right>
<right ref="RM000002PEE01OX_04_0018" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0001" proc-id="RM23G0E___00009MK00001">
<testtitle>INSPECT AIR TUBE FOR AIR LEAK OR CLOG</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the air tube for air leaks or clogs (See page <xref label="Seep01" href="RM000001U5400UX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No air leaks or clogs are found in the air tube.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002PEE01OX_04_0012" fin="false">OK</down>
<right ref="RM000002PEE01OX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0012" proc-id="RM23G0E___00009MM00001">
<testtitle>INSPECT NO. 2 HEIGHT CONTROL VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Remove the No. 2 height control valve (See page <xref label="Seep01" href="RM000002FOA019X"/>). </ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the values in the table below.</ptxt>
<figure>
<graphic graphicname="C214512E08" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.53in"/>
<colspec colname="COL2" colwidth="1.35in"/>
<colspec colname="COL3" colwidth="1.25in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (LH+) - 2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>15 to 25°C (59 to 77°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>17.5 to 21.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3 (RH+) - 2 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>15 to 25°C (59 to 77°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 to 14 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Component without harness connected</ptxt>
<ptxt>(No. 2 Height Control Valve)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check the leveling solenoid valve.</ptxt>
<test2>
<ptxt>Connect the positive (+) lead of the battery to terminal 3 (RH+) and the negative (-) lead to terminal 2 (E).</ptxt>
</test2>
<test2>
<ptxt>Check the operating sound of the No. 2. height control valve</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>It makes an operating sound (click).</ptxt>
</specitem>
</spec>
</test2>
</test1>
<test1>
<ptxt>Check the gate solenoid valve.</ptxt>
<test2>
<ptxt>Connect the positive (+) lead of the battery to terminal 1 (LH+) and the negative (-) lead to terminal 2 (E). </ptxt>
</test2>
<test2>
<ptxt>Check the operating sound of the No. 2. height control valve.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>It makes an operating sound (click).</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM000002PEE01OX_04_0022" fin="false">OK</down>
<right ref="RM000002PEE01OX_04_0013" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0022" proc-id="RM23G0E___00009M300001">
<testtitle>INSPECT HEIGHT CONTROL COMPRESSOR (EXHAUST SOLENOID VALVE)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Remove the height control compressor (See page <xref label="Seep01" href="RM000002BGX00IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214511E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>2 (L) - 1 (B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 to 14 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Exhaust Valve)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Connect the positive (+) lead of the battery to terminal 2 (L) and the negative (-) lead to terminal 1 (B) of the solenoid valve connector. Then check that the valve makes an operating sound. </ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Operation of solenoid (clicking sound) can be heard.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000002PEE01OX_04_0023" fin="false">OK</down>
<right ref="RM000002PEE01OX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0023" proc-id="RM23G0E___00009MG00001">
<testtitle>INSPECT HEIGHT CONTROL COMPRESSOR (COMPRESSOR MOTOR)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the height control compressor (See page <xref label="Seep01" href="RM000002BGX00IX"/>).</ptxt>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the compressor motor and check the operation of the motor.</ptxt>
</test1>
<figure>
<graphic graphicname="C214513E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>12 V battery positive (+) voltage → Terminal 1 (B)</ptxt>
<ptxt>12 V battery negative (-) voltage → Terminal 2 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Motor operates</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Height Control Compressor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not allow the compressor motor to operate for approximately 90 seconds or more.</ptxt>
</item>
<item>
<ptxt>If the compressor is shorted, locked or has a similar type of malfunction, a large amount of current is flowing. Therefore, if the motor does not operate, immediately stop this inspection.</ptxt>
</item>
</list1>
</atten3>
</content6><res>
<down ref="RM000002PEE01OX_04_0024" fin="false">OK</down>
<right ref="RM000002PEE01OX_04_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0024" proc-id="RM23G0E___00009MA00001">
<testtitle>INSPECT SUSPENSION CONTROL RELAY (AIR SUS)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Remove the AIR SUS relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C149624E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.13in"/>
<colspec colname="COL2" colwidth="1.83in"/>
<colspec colname="COL3" colwidth="1.17in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt> Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>12 V battery voltage is not applied to terminal 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>12 V battery voltage is applied to terminal 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>AIR SUS Relay</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000002PEE01OX_04_0008" fin="true">OK</down>
<right ref="RM000002PEE01OX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0008">
<testtitle>REPLACE SUSPENSION CONTROL ECU<xref label="Seep01" href="RM000002BGR016X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0017">
<testtitle>END (C1761/61 WAS OUTPUT DUE TO UNUSUAL VEHICLE OR USAGE CONDITION)</testtitle>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0018">
<testtitle>GO TO DIAGNOSTIC TROUBLE CODE CHART<xref label="Seep01" href="RM000002Q4X01QX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0005">
<testtitle>REPAIR OR REPLACE AIR TUBE</testtitle>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0013">
<testtitle>REPLACE NO. 2 HEIGHT CONTROL VALVE<xref label="Seep01" href="RM000002FOA019X"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0007">
<testtitle>REPLACE HEIGHT CONTROL COMPRESSOR ASSEMBLY<xref label="Seep01" href="RM000002BGX00IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0020">
<testtitle>REPLACE HEIGHT CONTROL COMPRESSOR ASSEMBLY<xref label="Seep01" href="RM000002BGX00IX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002PEE01OX_04_0006">
<testtitle>REPLACE SUSPENSION CONTROL RELAY (AIR SUS)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>