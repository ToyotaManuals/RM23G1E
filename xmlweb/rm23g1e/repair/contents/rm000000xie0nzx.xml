<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000000XIE0NZX" category="C" type-id="803OO" name-id="BCDOY-02" from="201210">
<dtccode>C1425</dtccode>
<dtcname>Open in Stop Light Switch Circuit</dtcname>
<subpara id="RM000000XIE0NZX_01" type-id="60" category="03" proc-id="RM23G0E___0000A5W00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU detects the brake operating conditions through a signal transmitted by the stop light switch.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.00in"/>
<colspec colname="COL2" colwidth="3.14in"/>
<colspec colname="COL3" colwidth="2.94in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C1425</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the IG1 terminal voltage is between 9.5 and 17.4 V, an open STP terminal circuit continues for 3 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>STP terminal circuit</ptxt>
</item>
<item>
<ptxt>Stop light control ECU</ptxt>
</item>
<item>
<ptxt>Brake actuator assembly (Skid control ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XIE0NZX_02" type-id="32" category="03" proc-id="RM23G0E___0000A5X00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C234379E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XIE0NZX_03" type-id="51" category="05" proc-id="RM23G0E___0000A5Y00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08IX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XIE0NZX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIE0NZX_04_0039" proc-id="RM23G0E___0000A5U00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STOP LIGHT SW)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.18in"/>
<colspec colname="COL2" colwidth="2.00in"/>
<colspec colname="COL3" colwidth="1.95in"/>
<colspec colname="COLSPEC1" colwidth="1.95in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch/ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Brake pedal depressed</ptxt>
<ptxt>OFF: Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that the stop light switch display observed on the intelligent tester changes according to the brake pedal operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The intelligent tester displays ON or OFF according to brake pedal operation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XIE0NZX_04_0007" fin="false">OK</down>
<right ref="RM000000XIE0NZX_04_0046" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0NZX_04_0007" proc-id="RM23G0E___0000A5Z00001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0GFX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal several times to test the stop light circuit.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM000000XHV0GFX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIE0NZX_04_0025" fin="true">A</down>
<right ref="RM000000XIE0NZX_04_0014" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0NZX_04_0046" proc-id="RM23G0E___0000A6000001">
<testtitle>INSPECT STOP LIGHT CONTROL ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the stop light control ECU (See page <xref label="Seep01" href="RM000001ZTV07FX"/>). </ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XIE0NZX_04_0044" fin="false">OK</down>
<right ref="RM000000XIE0NZX_04_0047" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0NZX_04_0044" proc-id="RM23G0E___0000A5D00001">
<testtitle>CHECK TERMINAL VOLTAGE (STP)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Make sure that there is no looseness at the locking parts and connecting parts of the connectors.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure  the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C199357E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>A6-2 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIE0NZX_04_0048" fin="false">OK</down>
<right ref="RM000000XIE0NZX_04_0045" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0NZX_04_0048" proc-id="RM23G0E___0000A6100001">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTC (See page <xref label="Seep01" href="RM000000XHV0GFX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Depress the brake pedal several times to test the stop light circuit.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM000000XHV0GFX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIE0NZX_04_0025" fin="true">A</down>
<right ref="RM000000XIE0NZX_04_0014" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XIE0NZX_04_0014">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIE0NZX_04_0045">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XIE0NZX_04_0025">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIE0NZX_04_0047">
<testtitle>REPLACE STOP LIGHT CONTROL ECU<xref label="Seep01" href="RM000003MWQ01WX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>