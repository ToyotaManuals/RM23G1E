<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12038_S001M" variety="S001M">
<name>STEERING COLUMN</name>
<ttl id="12038_S001M_7B98Q_T00IE" variety="T00IE">
<name>POWER TILT AND POWER TELESCOPIC STEERING COLUMN SYSTEM</name>
<para id="RM000000XZD04YX" category="J" type-id="3014Y" name-id="SR28Y-09" from="201207">
<dtccode/>
<dtcname>Actuator Power Source Circuit</dtcname>
<subpara id="RM000000XZD04YX_01" type-id="60" category="03" proc-id="RM23G0E___0000BCP00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This is the power source for the power tilt and power telescopic steering column system.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XZD04YX_02" type-id="32" category="03" proc-id="RM23G0E___0000BCQ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C149263E17" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XZD04YX_03" type-id="51" category="05" proc-id="RM23G0E___0000BCR00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XZD04YX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XZD04YX_04_0010" proc-id="RM23G0E___0000BC900000">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTIPLEX TILT AND TELESCOPIC ECU - BATTERY)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G116 multiplex tilt and telescopic ECU connector.</ptxt>
<figure>
<graphic graphicname="C197979E13" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G116-2 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Multiplex Tilt and Telescopic ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XZD04YX_04_0011" fin="false">OK</down>
<right ref="RM000000XZD04YX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XZD04YX_04_0011" proc-id="RM23G0E___0000BBY00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MULTIPLEX TILT AND TELESCOPIC ECU - BODY GROUND)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C197979E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.65in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<colspec colname="COL3" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G116-11 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to Multiplex Tilt and Telescopic ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000000XZD04YX_04_0007" fin="true">OK</down>
<right ref="RM000000XZD04YX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XZD04YX_04_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XZD04YX_04_0007">
<testtitle>CHECK IG POWER SOURCE CIRCUIT<xref label="Seep01" href="RM000000XZ704RX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>