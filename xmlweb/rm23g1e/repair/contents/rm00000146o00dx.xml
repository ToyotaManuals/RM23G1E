<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12012_S000W" variety="S000W">
<name>5L-E LUBRICATION</name>
<ttl id="12012_S000W_7B92L_T00C9" variety="T00C9">
<name>OIL PUMP</name>
<para id="RM00000146O00DX" category="A" type-id="30014" name-id="LU2AL-04" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM00000146O00DX_01" type-id="01" category="01">
<s-1 id="RM00000146O00DX_01_0102" proc-id="RM23G0E___000052T00000">
<ptxt>INSTALL FRONT CRANKSHAFT OIL SEAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST and a hammer, tap in a new oil seal until its surface is flush with the timing belt case edge.</ptxt>
<sst>
<sstitem>
<s-number>09214-60010</s-number>
</sstitem>
</sst>
<figure>
<graphic graphicname="A057640E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Apply MP grease to the lip of the oil seal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146O00DX_01_0001" proc-id="RM23G0E___000052U00000">
<ptxt>INSTALL TIMING BELT CASE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Place a new gasket on the cylinder block.</ptxt>
</s2>
<s2>
<ptxt>Install the timing belt case with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>23</t-value1>
<t-value2>230</t-value2>
<t-value4>17</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146O00DX_01_0002" proc-id="RM23G0E___000052V00000">
<ptxt>INSTALL OIL STRAINER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the oil strainer with the 2 bolts and 2 nuts.</ptxt>
<torque>
<subtitle>for nut</subtitle>
<torqueitem>
<t-value1>21</t-value1>
<t-value2>210</t-value2>
<t-value4>17</t-value4>
</torqueitem>
<subtitle>for bolt</subtitle>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146O00DX_01_0003" proc-id="RM23G0E___000052K00000">
<ptxt>INSTALL OIL PAN SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove any old packing (FIPG) material and do not drop any oil on the contact surfaces of the oil pan and cylinder block.</ptxt>
<list1 type="unordered">
<item>
<ptxt>Using a gasket scraper, remove all the old packing (FIPG) material from the installation surface.</ptxt>
</item>
<item>
<ptxt>Thoroughly clean all components to remove all the loose material.</ptxt>
</item>
<item>
<ptxt>Using a non-residue solvent, clean both of the sealing surfaces.</ptxt>
<atten3>
<ptxt>Do not use a solvent which will affect the painted surfaces.</ptxt>
</atten3>
</item>
</list1>
</s2>
<s2>
<ptxt>Apply seal packing to the oil pan as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A227429E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Seal packing</title>
<specitem>
<ptxt>Toyota Genuine Seal Packing Black, Three Bond 1207B or equivalent</ptxt>
</specitem>
</spec>
<spec>
<title>Application Specification</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Seal Packing Diameter</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Seal Packing Application Length</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Dashed line</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>7.0 mm (0.276 in.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>128 mm (5.04 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Continuous line</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.0 mm (0.197 in.)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seal Packing</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Timing Belt Case Contact Portion</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Oil Seal Retainer Contact Portion</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Do not apply an excessive amount to the surface, especially near the oil passages.</ptxt>
</item>
<item>
<ptxt>Parts must be assembled within 5 minutes of application. Otherwise the material must be removed and reapplied.</ptxt>
</item>
</list1>
</atten4>
</s2>
<s2>
<ptxt>Install the oil pan with the 16 bolts and 2 nuts. Uniformly tighten the bolts and nuts in several steps.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146O00DX_01_0095" proc-id="RM23G0E___00004ZG00000">
<ptxt>INSTALL CRANKSHAFT TIMING PULLEY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the key groove of the timing pulley with the pulley set key.</ptxt>
<figure>
<graphic graphicname="A056897E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST and a hammer, tap in the timing pulley with the flange side facing inward.</ptxt>
<sst>
<sstitem>
<s-number>09223-46011</s-number>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Inside</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0096" proc-id="RM23G0E___000052J00000">
<ptxt>INSTALL NO. 2 TIMING BELT IDLER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the spacer and No. 2 timing belt idler with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>33</t-value1>
<t-value2>337</t-value2>
<t-value4>24</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Check that the No. 2 timing belt idler moves smoothly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0093" proc-id="RM23G0E___00004V500000">
<ptxt>INSTALL NO. 1 TIMING BELT IDLER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 belt idler with the 3 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>44</t-value1>
<t-value2>449</t-value2>
<t-value4>32</t-value4>
</torqueitem>
<subtitle>for bolt B, C</subtitle>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>195</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
<atten4>
<list1 type="unordered">
<figure>
<graphic graphicname="A057641E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<item>
<ptxt>The bolt lengths for bolt A, B and C as follows.</ptxt>
</item>
<item>
<ptxt>Bolt C is combined with the No. 1 timing belt idler.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>Standard Bolt</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Length</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>A</ptxt>
</entry>
<entry align="center">
<ptxt>76.5 mm (3.01 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>B</ptxt>
</entry>
<entry align="center">
<ptxt>42.9 mm (1.69 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>C</ptxt>
</entry>
<entry align="center">
<ptxt>41.3 mm (1.63 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="A056838E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0086" proc-id="RM23G0E___00006PA00000">
<ptxt>INSTALL WATER PUMP ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket, the water pump and tension spring bracket with the 6 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>23</t-value1>
<t-value2>236</t-value2>
<t-value4>17</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0094" proc-id="RM23G0E___00004VN00000">
<ptxt>INSTALL NO. 2 TIMING BELT COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the timing belt cover with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0087" proc-id="RM23G0E___00004VO00000">
<ptxt>INSTALL CAMSHAFT TIMING PULLEY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the woodruff key to the key groove of the camshaft.</ptxt>
</s2>
<s2>
<ptxt>Align the timing mark on the camshaft timing pulley with the timing mark on the No. 2 timing belt cover and temporarily install the pulley with the bolt.</ptxt>
<figure>
<graphic graphicname="A229923" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using SST, tighten the bolt.</ptxt>
<sst>
<sstitem>
<s-number>09960-10010</s-number>
<s-subnumber>09962-01000</s-subnumber>
<s-subnumber>09963-01000</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="A224022E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>98</t-value1>
<t-value2>999</t-value2>
<t-value4>72</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0097" proc-id="RM23G0E___000050X00000">
<ptxt>INSTALL NO. 1 GENERATOR BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the generator bracket with the 3 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>49</t-value1>
<t-value2>500</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0103" proc-id="RM23G0E___000050G00000">
<ptxt>INSTALL NO. 1 FRONT ENGINE MOUNTING BRACKET RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the engine mounting bracket with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>49</t-value1>
<t-value2>500</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0104" proc-id="RM23G0E___000050U00000">
<ptxt>INSTALL NO. 1 FRONT ENGINE MOUNTING BRACKET LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the engine mounting bracket with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>49</t-value1>
<t-value2>500</t-value2>
<t-value4>36</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0098" proc-id="RM23G0E___000050I00000">
<ptxt>INSTALL PUMP BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the pump bracket with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A225174E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>57</t-value1>
<t-value2>581</t-value2>
<t-value4>42</t-value4>
</torqueitem>
<subtitle>except bolt A</subtitle>
<torqueitem>
<t-value1>78</t-value1>
<t-value2>795</t-value2>
<t-value4>58</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0099" proc-id="RM23G0E___000050J00000">
<ptxt>INSTALL WATER BY-PASS HOSE UNION
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the threads of the water by-pass hose union and apply adhesive to them.</ptxt>
<spec>
<title>Adhesive</title>
<specitem>
<ptxt>Toyota Genuine Adhesive 1324, Three Bond 1324 or equivalent</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Install the water by-pass hose union.</ptxt>
<torque>
<torqueitem>
<t-value1>39</t-value1>
<t-value2>398</t-value2>
<t-value4>29</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0100" proc-id="RM23G0E___000050K00000">
<ptxt>INSTALL WATER OUTLET HOUSING
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket to the cylinder head.</ptxt>
</s2>
<s2>
<ptxt>Install the outlet hosing with the 3 bolts</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0055" proc-id="RM23G0E___00006WA00000">
<ptxt>INSTALL INJECTION PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the injection pump (See page <xref label="Seep01" href="RM0000010YK00GX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146O00DX_01_0057" proc-id="RM23G0E___00006WB00000">
<ptxt>INSTALL TIMING BELT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the timing belt (See page <xref label="Seep01" href="RM00000126H00DX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000146O00DX_01_0101" proc-id="RM23G0E___000050H00000">
<ptxt>INSTALL NO. 1 COMPRESSOR MOUNTING BRACKET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 compressor mounting bracket with the 4 bolts.</ptxt>
<figure>
<graphic graphicname="A121135" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>81</t-value1>
<t-value2>829</t-value2>
<t-value4>60</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM00000146O00DX_01_0090" proc-id="RM23G0E___00006WC00000">
<ptxt>INSTALL ENGINE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the engine (See page <xref label="Seep01" href="RM000000FYK015X"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>