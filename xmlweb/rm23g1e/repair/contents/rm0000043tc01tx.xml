<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12041_S001P" variety="S001P">
<name>NAVIGATION / MULTI INFO DISPLAY</name>
<ttl id="12041_S001P_7B99K_T00J8" variety="T00J8">
<name>NAVIGATION SYSTEM (for HDD)</name>
<para id="RM0000043TC01TX" category="J" type-id="302YQ" name-id="NS71F-02" from="201207" to="201210">
<dtccode/>
<dtcname>AVC-LAN Circuit</dtcname>
<subpara id="RM0000043TC01TX_01" type-id="60" category="03" proc-id="RM23G0E___0000C7Z00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Each unit of the navigation system connected to the AVC-LAN (communication bus) transfers switch signals by the audio visual communication local area network.</ptxt>
<ptxt>If a short to +B or short to ground occurs in the AVC-LAN, the navigation system does not function normally as communication stops.</ptxt>
</content5>
</subpara>
<subpara id="RM0000043TC01TX_02" type-id="32" category="03" proc-id="RM23G0E___0000C8000000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E193091E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000043TC01TX_03" type-id="51" category="05" proc-id="RM23G0E___0000C8100000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>The display and navigation module display is the master unit.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000043TC01TX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000043TC01TX_04_0001" proc-id="RM23G0E___0000C8200000">
<testtitle>CHECK DISPLAY AND NAVIGATION MODULE DISPLAY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H3 and H15 display and navigation module display connectors.</ptxt>
<figure>
<graphic graphicname="E201096E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H3-1 (TX1+) - H3-2 (TX1-)*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H15-9 (TXM+) - H15-10 (TXM-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Parking Assist Monitor System and/or Side Monitor System</ptxt>
</item>
</list1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Parking Assist Monitor System and/or Side Monitor System</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Display and Navigation Module Display)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000043TC01TX_04_0002" fin="false">OK</down>
<right ref="RM0000043TC01TX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043TC01TX_04_0002" proc-id="RM23G0E___0000C8300000">
<testtitle>CHECK HARNESS AND CONNECTOR (DISPLAY AND NAVIGATION MODULE DISPLAY - WHICH HAS STEREO THIS CODE)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>For details of the connectors, refer to Terminals of ECU (See page <xref label="Seep01" href="RM0000044WM01PX"/>).</ptxt>
</atten4>
<test1>
<ptxt>Referring to the AVC-LAN wiring diagram above, check all AVC-LAN circuits.</ptxt>
<test2>
<ptxt>Disconnect all connectors in all AVC-LAN circuits.</ptxt>
</test2>
<test2>
<ptxt>Check for an open or short in all AVC-LAN circuits.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no open or short circuit.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000043TC01TX_04_0003" fin="false">OK</down>
<right ref="RM0000043TC01TX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043TC01TX_04_0003" proc-id="RM23G0E___0000C8400000">
<testtitle>INSPECT MALFUNCTIONING PARTS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect and reconnect each slave unit one by one until the master unit returns to normal operation.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Check all slave units.</ptxt>
</item>
<item>
<ptxt>If disconnecting a slave unit causes the master unit to return to normal operation, the slave unit is defective and should be replaced.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Master unit returns to normal operation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000043TC01TX_04_0006" fin="true">OK</down>
<right ref="RM0000043TC01TX_04_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043TC01TX_04_0004">
<testtitle>REPLACE DISPLAY AND NAVIGATION MODULE DISPLAY<xref label="Seep01" href="RM000003B6H01LX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043TC01TX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000043TC01TX_04_0006">
<testtitle>REPLACE MALFUNCTIONING PARTS</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>