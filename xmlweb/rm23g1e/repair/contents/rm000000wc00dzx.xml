<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000000WC00DZX" category="C" type-id="300Y7" name-id="ESMDT-07" from="201207" to="201210">
<dtccode>P0420</dtccode>
<dtcname>Catalyst System Efficiency Below Threshold (Bank 1)</dtcname>
<dtccode>P0430</dtccode>
<dtcname>Catalyst System Efficiency Below Threshold (Bank 2)</dtcname>
<subpara id="RM000000WC00DZX_01" type-id="60" category="03" proc-id="RM23G0E___00000LR00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses sensors mounted in front of and behind the Three-Way Catalytic Converter (TWC) to monitor its efficiency.</ptxt>
<ptxt>The first sensor, the air fuel ratio sensor, sends pre-catalyst information to the ECM. The second sensor, the heated oxygen sensor, sends post-catalyst information to the ECM.</ptxt>
<ptxt>In order to detect any deterioration in the TWC, the ECM calculates the Oxygen Storage Capacity (OSC) of the TWC. This calculation is based on the voltage output of the heated oxygen sensor while performing active air-fuel ratio control.</ptxt>
<ptxt>The OSC value is an indication of the oxygen storage capacity of the TWC. When the vehicle is being driven with a warm engine, active air-fuel ratio control is performed for approximately 15 to 20 seconds. When it is performed, the ECM deliberately sets the air-fuel ratio to lean or rich levels. If a rich-lean cycle of the heated oxygen sensor is long, the OSC becomes greater. There is a direct correlation between the OSCs of the heated oxygen sensor and TWC.</ptxt>
<ptxt>The ECM uses the OSC value to determine the state of the TWC. If any deterioration has occurred, it illuminates the MIL and stores a DTC.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0420</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oxygen storage capacity value is smaller than the standard value under active air fuel ratio control (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 1 sensor 1)</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor (bank 1 sensor 2)</ptxt>
</item>
<item>
<ptxt>Exhaust manifold sub-assembly RH (TWC: Front Catalyst)</ptxt>
</item>
<item>
<ptxt>Front exhaust pipe assembly (TWC: Rear Catalyst)</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0430</ptxt>
</entry>
<entry valign="middle">
<ptxt>Oxygen storage capacity value is smaller than the standard value under active air fuel ratio control (2 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor (bank 2 sensor 1)</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor (bank 2 sensor 2)</ptxt>
</item>
<item>
<ptxt>Exhaust manifold sub-assembly LH (TWC: Front Catalyst)</ptxt>
</item>
<item>
<ptxt>Front No. 2 exhaust pipe assembly (TWC: Rear Catalyst)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Bank 1 refers to the bank that includes the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Bank 2 refers to the bank that does not include the No. 1 cylinder.</ptxt>
</item>
<item>
<ptxt>Sensor 1 refers to the sensor closest to the engine assembly.</ptxt>
</item>
<item>
<ptxt>Sensor 2 refers to the sensor farthest away from the engine assembly.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000WC00DZX_12" type-id="87" category="03" proc-id="RM23G0E___00000M200000">
<name>CATALYST LOCATION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A223836E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Manifold Sub-Assembly RH (TWC: Front Catalyst)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Exhaust Manifold Sub-Assembly LH (TWC: Front Catalyst)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Exhaust Pipe (TWC: Rear Catalyst)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front No. 2 Exhaust Pipe (TWC: Rear Catalyst)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Center Exhaust Pipe Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Tailpipe Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor (for Bank 1 Sensor 1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Fuel Ratio Sensor (for Bank 2 Sensor 1)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated Oxygen Sensor (for Bank 1 Sensor 2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated Oxygen Sensor (for Bank 2 Sensor 2)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000WC00DZX_11" type-id="73" category="03" proc-id="RM23G0E___00000M100000">
<name>CONFIRMATION DRIVING PATTERN</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Performing this confirmation driving pattern will activate the catalyst monitor. This is very useful for verifying the completion of a repair.</ptxt>
</atten4>
<figure>
<graphic graphicname="A201252E78" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="ordered">
<item>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Turn the ignition switch to ON.</ptxt>
</item>
<item>
<ptxt>Turn the intelligent tester on.</ptxt>
</item>
<item>
<ptxt>Clear DTCs (even if no DTCs are stored, perform the clear DTC operation).</ptxt>
</item>
<item>
<ptxt>Turn the ignition switch off and wait for at least 30 seconds.</ptxt>
</item>
<item>
<ptxt>Turn the ignition switch to ON and turn the intelligent tester on.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Monitor.</ptxt>
</item>
<item>
<ptxt>Check that Catalyst / Status2 is Incomplete.</ptxt>
</item>
<item>
<ptxt>Start the engine and warm it up until the engine coolant temperature is 75°C (167°F) or higher [A].</ptxt>
</item>
<item>
<ptxt>Drive the vehicle between 40 and 70 mph (64 and 113 km/h) for 10 minutes or more [B].</ptxt>
</item>
<item>
<ptxt>The monitor items will change to Complete as the catalyst monitor operates.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes / Pending.</ptxt>
</item>
<item>
<ptxt>Check if any DTCs (pending DTCs) are stored.</ptxt>
</item>
</list1>
<atten4>
<ptxt>If Catalyst does not change to Complete, and no pending DTCs are stored, extend the driving time.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000WC00DZX_08" type-id="74" category="03" proc-id="RM23G0E___00000M000000">
<name>CONDITIONING FOR SENSOR TESTING</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Perform the operation with the engine speeds and time durations described below prior to checking the waveforms of the air fuel ratio and heated oxygen sensors. This is in order to activate the sensors sufficiently to obtain the appropriate inspection results.</ptxt>
<figure>
<graphic graphicname="A118003E99" width="7.106578999in" height="3.779676365in"/>
</figure>
<list1 type="nonmark">
<item>
<ptxt>(a) Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>(b) Start the engine and warm it up with all the accessories switched off until the engine coolant temperature stabilizes.</ptxt>
</item>
<item>
<ptxt>(c) Run the engine at an engine speed of between 2500 rpm and 3000 rpm for at least 3 minutes.</ptxt>
</item>
<item>
<ptxt>(d) While running the engine at 3000 rpm for 2 seconds, and then 2000 rpm for 2 seconds, check the waveforms of the air fuel ratio and heated oxygen sensors using the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If either of the voltage outputs of the air fuel ratio or heated oxygen sensor does not fluctuate, or either of the sensors makes a noise, the sensor may be malfunctioning.</ptxt>
</item>
<item>
<ptxt>If the voltage outputs of both of the sensors remain lean or rich, the air-fuel ratio may be extremely lean or rich. In such cases, enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F sensor.</ptxt>
</item>
<item>
<ptxt>If the Three-Way Catalytic Converter (TWC) has deteriorated, the heated oxygen sensor (located behind the TWC) voltage output fluctuates up and down frequently, even under normal driving conditions (active air-fuel ratio control is not performed).</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="A121610E45" width="7.106578999in" height="3.779676365in"/>
</figure>
</atten4>
</content5>
</subpara>
<subpara id="RM000000WC00DZX_06" type-id="51" category="05" proc-id="RM23G0E___00000LS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Malfunctioning areas can be identified by performing the Control the Injection Volume for air fuel ratio sensor function provided in the Active Test. The Control the Injection Volume for air fuel ratio sensor function can help to determine whether the air fuel ratio sensor, heated oxygen sensor and other potential trouble areas are malfunctioning.</ptxt>
<ptxt>The following instructions describe how to conduct the Control the Injection Volume for air fuel ratio sensor operation using the intelligent tester.</ptxt>
<list1 type="ordered">
<item>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Start the engine and turn the intelligent tester on.</ptxt>
</item>
<item>
<ptxt>Warm up the engine at an engine speed of 2500 rpm for approximately 90 seconds.</ptxt>
</item>
<item>
<ptxt>On the intelligent tester, enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F Sensor.</ptxt>
</item>
<item>
<ptxt>Perform the Active Test operation with the engine idling (press the RIGHT or LEFT button to change the fuel injection volume).</ptxt>
</item>
<item>
<ptxt>Monitor the output voltages of the air fuel ratio and heated oxygen sensors (AFS Voltage B1S1 and O2S B1S2 or AFS Voltage B2S1 and O2S B2S2) displayed on the intelligent tester.</ptxt>
</item>
</list1>
</atten4>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The Control the Injection Volume for air fuel ratio sensor operation lowers the fuel injection volume by 12.5% or increases the injection volume by 25%.</ptxt>
</item>
<item>
<ptxt>Each sensor reacts in accordance with increases and decreases in the fuel injection volume.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display (Sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+25%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 3.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 3.4 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+25%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 0.55 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>The air fuel ratio sensor has an output delay of a few seconds and the heated oxygen sensor has a maximum output delay of approximately 20 seconds.</ptxt>
</atten3>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.51in"/>
<colspec colname="COL2" colwidth="2.63in"/>
<colspec colname="COL3" colwidth="2.58in"/>
<colspec colname="COL4" colwidth="1.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Case</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air Fuel Ratio Sensor (Sensor 1) Output Voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Heated Oxygen Sensor (Sensor 2) Output Voltage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Suspected Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A151324E08" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150788E04" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150788E04" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor heater</ptxt>
</item>
<item>
<ptxt>Air fuel ratio sensor circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A151324E08" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor heater</ptxt>
</item>
<item>
<ptxt>Heated oxygen sensor circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4</ptxt>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<graphic graphicname="A150787E01" width="2.362204724409449in" height="0.4724409448818898in"/>
<graphic graphicname="A150790E01" width="2.362204724409449in" height="0.4724409448818898in"/>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Fuel injector assembly</ptxt>
</item>
<item>
<ptxt>Fuel pressure</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system (air fuel ratio extremely rich or lean)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>Following the Control the Injection Volume for air fuel ratio sensor procedure enables technicians to check and graph the voltage outputs of both the air fuel ratio and heated oxygen sensors.</ptxt>
</item>
<item>
<ptxt>To display the graph, enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F Sensor / A/F Control System / AFS Voltage B1S1 and O2S B1S2 or AFS Voltage B2S1 and O2S B2S2.</ptxt>
</item>
</list1>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air-fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000WC00DZX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000WC00DZX_07_0001" proc-id="RM23G0E___00000LT00000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P0420 AND/OR P0430)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0420 and/or P0430</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0420 and/or P0430 and other DTCs</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P0420 or P0430 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000WC00DZX_07_0002" fin="false">A</down>
<right ref="RM000000WC00DZX_07_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0002" proc-id="RM23G0E___00000LU00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (CONTROL THE INJECTION VOLUME FOR A/F SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Warm up the engine at an engine speed of 2500 rpm for approximately 90 seconds.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Injection Volume for A/F Sensor.</ptxt>
</test1>
<test1>
<ptxt>Perform the Active Test operation with the engine idling (press the RIGHT or LEFT button to change the fuel injection volume).</ptxt>
</test1>
<test1>
<ptxt>Monitor the voltage outputs of the air fuel ratio and heated oxygen sensors (AFS Voltage B1S1 and O2S B1S2 or AFS Voltage B2S1 and O2S B2S2) displayed on the intelligent tester.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The Control the Injection Volume for A/F Sensor operation lowers the fuel injection volume by 12.5% or increases the injection volume by 25%.</ptxt>
</item>
<item>
<ptxt>Each sensor reacts in accordance with increases and decreases in the fuel injection volume.</ptxt>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
<ptxt>(Sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Injection Volume</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+25%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 3.1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>AFS Voltage B1S1 or AFS Voltage B2S1</ptxt>
<ptxt>(Air fuel ratio sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 3.4 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>+25%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Higher than 0.55 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O2S B1S2 or O2S B2S2</ptxt>
<ptxt>(Heated oxygen sensor)</ptxt>
</entry>

<entry valign="middle" align="center">
<ptxt>-12.5%</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 0.4 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="6">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="0.71in"/>
<colspec colname="COL3" colwidth="1.98in"/>
<colspec colname="COL4" colwidth="0.71in"/>
<colspec colname="COL5" colwidth="2.41in"/>
<colspec colname="COL6" colwidth="0.56in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Status of</ptxt>
<ptxt>AFS Voltage B1S1</ptxt>
<ptxt>or</ptxt>
<ptxt>AFS Voltage B2S1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Status of</ptxt>
<ptxt>O2S B1S2</ptxt>
<ptxt>or</ptxt>
<ptxt>O2S B2S2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air Fuel Ratio Condition and Air Fuel Ratio Sensor and Heated Oxygen Sensor Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Misfire</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Suspected Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>Three-Way Catalytic Converter (TWC)</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air fuel ratio sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air fuel ratio sensor</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated oxygen sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean/Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Heated oxygen sensor malfunction</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Heated oxygen sensor</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Lean</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual air-fuel ratio lean</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>May occur</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Extremely rich or lean actual air-fuel ratio</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Rich</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual air-fuel ratio rich</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Extremely rich or lean actual air-fuel ratio</ptxt>
</item>
<item>
<ptxt>Gas leakage from exhaust system</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>Lean: During Control the Injection Volume for A/F Sensor, the air fuel ratio sensor output voltage (AFS) is consistently higher than 3.4 V, and the heated oxygen sensor output voltage (O2S) is consistently below 0.4 V.</ptxt>
<ptxt>Rich: During Control the Injection Volume for A/F Sensor, the AFS is consistently below 3.1 V, and the O2S is consistently higher than 0.55 V.</ptxt>
<ptxt>Lean/Rich: During Control the Injection Volume for A/F Sensor of the Active Test, the output voltage of the heated oxygen sensor alternates correctly.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000WC00DZX_07_0003" fin="false">A</down>
<right ref="RM000000WC00DZX_07_0013" fin="true">B</right>
<right ref="RM000000WC00DZX_07_0010" fin="false">C</right>
<right ref="RM000000WC00DZX_07_0014" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0003" proc-id="RM23G0E___00000LV00000">
<testtitle>INSPECT FOR EXHAUST GAS LEAK</testtitle>
<content6 releasenbr="1">
<spec>
<title>OK</title>
<specitem>
<ptxt>No gas leakage.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000WC00DZX_07_0015" fin="true">A</down>
<right ref="RM000000WC00DZX_07_0011" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0010" proc-id="RM23G0E___00000LX00000">
<testtitle>INSPECT EXHAUST FOR GAS LEAK</testtitle>
<content6 releasenbr="1">
<spec>
<title>OK</title>
<specitem>
<ptxt>No gas leakage.</ptxt>
</specitem>
</spec>
</content6>
<res>
<down ref="RM000000WC00DZX_07_0016" fin="true">OK</down>
<right ref="RM000000WC00DZX_07_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0011" proc-id="RM23G0E___00000LY00000">
<testtitle>CHECK DTC OUTPUT (DTC P0420 AND/OR P0430)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>According to the DTCs output in the Check for Any Other DTCs Output step, proceed to the next step by referring to the table below.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0420</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0430</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0420 and P0430</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A and B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000WC00DZX_07_0005" fin="false">A</down>
<right ref="RM000000WC00DZX_07_0017" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0005" proc-id="RM23G0E___00000LW00000">
<testtitle>REPLACE EXHAUST MANIFOLD SUB-ASSEMBLY RH (TWC: FRONT CATALYST)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the exhaust manifold sub-assembly RH (See page <xref label="Seep01" href="RM00000456G00GX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WC00DZX_07_0006" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0017" proc-id="RM23G0E___00000LZ00000">
<testtitle>REPLACE EXHAUST MANIFOLD SUB-ASSEMBLY LH (TWC: FRONT CATALYST)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the exhaust manifold sub-assembly LH (See page <xref label="Seep01" href="RM00000456G00GX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000WC00DZX_07_0018" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0006">
<testtitle>REPLACE EXHAUST PIPE ASSEMBLY (TWC: REAR CATALYST)<xref label="Seep01" href="RM00000456J00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0018">
<testtitle>REPLACE FRONT NO. 2 EXHAUST PIPE ASSEMBLY (TWC: REAR CATALYST)<xref label="Seep01" href="RM00000456J00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0004">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000002ZSO00PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0013">
<testtitle>REPLACE AIR FUEL RATIO SENSOR<xref label="Seep01" href="RM000002W9Q00RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0014">
<testtitle>CHECK CAUSE OF EXTREMELY RICH OR LEAN ACTUAL AIR-FUEL RATIO, REPLACE FAULTY AREA AND GO TO NEXT STEP</testtitle>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0007">
<testtitle>REPAIR OR REPLACE EXHAUST GAS LEAKAGE POINT</testtitle>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0015">
<testtitle>REPAIR OR REPLACE EXHAUST GAS LEAK POINT</testtitle>
</testgrp>
<testgrp id="RM000000WC00DZX_07_0016">
<testtitle>REPLACE HEATED OXYGEN SENSOR<xref label="Seep01" href="RM000002W9N00PX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>