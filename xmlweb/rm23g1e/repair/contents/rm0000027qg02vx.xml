<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S0029" variety="S0029">
<name>WINDOW / GLASS</name>
<ttl id="12064_S0029_7B9FM_T00PA" variety="T00PA">
<name>POWER WINDOW CONTROL SYSTEM</name>
<para id="RM0000027QG02VX" category="T" type-id="3001H" name-id="WS0YG-49" from="201207" to="201210">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM0000027QG02VX_z0" proc-id="RM23G0E___0000I9C00000">
<content5 releasenbr="15">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and relays related to this system before inspecting the suspected areas below.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Power Window Control System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row>
<entry morerows="1" colsep="1" valign="middle">
<ptxt>Power window does not operate at all</ptxt>
</entry>
<entry colsep="1">
<ptxt>Main body ECU (Multiplex network body ECU)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000133908CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1">
<ptxt>Driver side junction block assembly</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Remote up/down function does not operate</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Remote Up/Down Function does not Operate"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000027QQ070X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Driver side power window does not operate with power window master switch</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Driver Side Power Window does not Operate with Power Window Master Switch"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XHI09FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Front passenger side power window does not operate with front passenger side power window switch</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Front Passenger Side Power Window does not Operate with Front Passenger Side Power Window Switch"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XI003PX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Rear power window LH does not operate with rear power window switch LH*</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Rear Power Window LH does not Operate with Rear Power Window Switch LH"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XI1033X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Rear power window RH does not operate with rear power window switch RH*</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Rear Power Window RH does not Operate with Rear Power Window Switch RH"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XHZ02QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Driver side power window auto up/down function does not operate with power window master switch</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Driver Side Power Window Auto Up/Down Function does not Operate with Power Window Master Switch"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000Y4604KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Front passenger side power window auto up/down function does not operate with front passenger side power window switch</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Front Passenger Side Power Window Auto Up/Down Function does not Operate with Front Passenger Side Power Window Switch"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000Y4702KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Rear power window LH auto up/down function does not operate with rear power window switch LH*</ptxt>
</entry>
<entry colsep="1">
<ptxt>Proceed to "Rear Power Window LH Auto Up/Down Function does not Operate with Rear Power Window Switch LH"</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000Y4802IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Rear power window RH auto up/down function does not operate with rear power window switch RH*</ptxt>
</entry>
<entry colsep="1">
<ptxt>Proceed to "Rear Power Window RH Auto Up/Down Function does not Operate with Rear Power Window Switch RH"</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000Y4902CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Power window do not operate with driver side door key cylinder or wireless transmitter</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Power Window do not Operate with Driver Side Door Key Cylinder or Wireless Transmitter"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000Y4A05MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colsep="1" valign="middle" align="left">
<ptxt>Key-off operation function operates even if operating conditions are not satisfied</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Key-off Operation Function Operates even if Operating Conditions are not Satisfied"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000016W802SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Auto up operation does not fully close power window (jam protection function is activated)</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Auto Up Operation does not Fully Close Power Window (Jam Protection Function is Activated)"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000027QU05QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="left">
<ptxt>Jam protection function does not operate</ptxt>
</entry>
<entry colname="2" colsep="1" align="left">
<ptxt>Proceed to "Jam Protection Function does not Operate"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000YAT07FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: for 5 Door</ptxt>
</item>
</list1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>