<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM000000TCW0UYX" category="C" type-id="302B5" name-id="ESVJ8-04" from="201210">
<dtccode>P0335</dtccode>
<dtcname>Crankshaft Position Sensor "A" Circuit</dtcname>
<dtccode>P0339</dtccode>
<dtcname>Crankshaft Position Sensor "A" Circuit Intermittent</dtcname>
<subpara id="RM000000TCW0UYX_01" type-id="60" category="03" proc-id="RM23G0E___00003AP00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The crankshaft position sensor system consists of a crankshaft position sensor plate and pickup coil. The sensor plate has 34 teeth, with 2 teeth missing for detecting TDC, and is installed on the crankshaft. The crankshaft position sensor generates 34 signals per crankshaft revolution. The ECM determines the cylinder status based on the G2 signals and detects the crankshaft angle and engine speed from the NE signals.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0335</ptxt>
</entry>
<entry valign="middle">
<ptxt>Either condition is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>No crankshaft position sensor signal is sent to the ECM while cranking (1 trip detection logic).</ptxt>
</item>
<item>
<ptxt>No crankshaft position sensor signal is sent to the ECM at an engine speed of 600 rpm or more (1 trip detection logic).</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in crankshaft position sensor circuit</ptxt>
</item>
<item>
<ptxt>Crankshaft position sensor</ptxt>
</item>
<item>
<ptxt>Crankshaft position sensor plate</ptxt>
</item>
<item>
<ptxt>Camshaft position sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0339</ptxt>
</entry>
<entry valign="middle">
<ptxt>Under conditions (a), (b) and (c), no crankshaft position sensor signal is sent to the ECM for 0.05 seconds or more (1 trip detection logic):</ptxt>
<ptxt>(a) The engine speed is 1000 rpm or more.</ptxt>
<ptxt>(b) The starter signal is off.</ptxt>
<ptxt>(c) 3 seconds or more have elapsed since the starter signal switched from on to off.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in crankshaft position sensor circuit</ptxt>
</item>
<item>
<ptxt>Crankshaft position sensor</ptxt>
</item>
<item>
<ptxt>Crankshaft position sensor plate</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>Reference: Inspection using an oscilloscope</ptxt>
<figure>
<graphic graphicname="A063955E36" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The correct waveforms are as shown.</ptxt>
</item>
<item>
<ptxt>G2 is the camshaft position sensor signal, and NE+ is the crankshaft position sensor signal.</ptxt>
</item>
<item>
<ptxt>Grounding failure of the shielded wire may cause noise in the waveforms.</ptxt>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="1.24in"/>
<colspec colname="COL2" colwidth="2.89in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECM Terminal Name</ptxt>
</entry>
<entry>
<ptxt>CH1: G2 - NE-</ptxt>
<ptxt>CH2: NE+ - NE-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tester Range</ptxt>
</entry>
<entry>
<ptxt>5 V/DIV.</ptxt>
<ptxt>20 ms./DIV.</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Condition</ptxt>
</entry>
<entry>
<ptxt>Idling with warm engine</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000TCW0UYX_07" type-id="32" category="03" proc-id="RM23G0E___00003AQ00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A225774E05" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TCW0UYX_08" type-id="51" category="05" proc-id="RM23G0E___00003AR00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<list1 type="unordered">
<item>
<ptxt>Before inspecting the crankshaft position sensor, perform the following inspections which concern the camshaft position sensor.</ptxt>
</item>
<item>
<ptxt>Check that the camshaft position sensor connector is securely connected, is not loose, does not rattle, etc.</ptxt>
</item>
<item>
<ptxt>Check that the camshaft position sensor is not loose, does not rattle, etc.</ptxt>
</item>
<item>
<ptxt>Check that the camshaft timing gear is installed properly.</ptxt>
</item>
<item>
<ptxt>Perform the Active Test under the conditions* from the time the noise occurred described by the customer (for example, while driving the vehicle) and check if the noise stops.</ptxt>
<atten4>
<ptxt>*: Some examples of conditions are idling, racing the engine and driving at full throttle.</ptxt>
</atten4>
</item>
</list1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Troubleshoot for DTCs P0335 and P0339 first. If the trouble area cannot be determined, there may be a mechanical malfunction.</ptxt>
</item>
<item>
<ptxt>Check the engine speed. The engine speed can be checked by using the intelligent tester. Follow the procedure below:</ptxt>
</item>
<list2 type="ordered">
<item>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</item>
<item>
<ptxt>Start the engine.</ptxt>
</item>
<item>
<ptxt>Turn the tester on.</ptxt>
</item>
<item>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Engine Speed.</ptxt>
</item>
</list2>
<list2 type="nonmark">
<item>
<ptxt>The engine speed may be indicated as zero despite the crankshaft revolving normally. This is caused by a lack of NE signals from the crankshaft position sensor. Alternatively, the engine speed may be indicated as lower than the actual engine speed if the crankshaft position sensor output voltage is insufficient.</ptxt>
</item>
</list2>
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000TCW0UYX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TCW0UYX_09_0010" proc-id="RM23G0E___00003AW00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ENGINE SPEED)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Engine Speed.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Read the values displayed on the tester while the engine is running.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Correct values are displayed.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To check the engine speed change, display the graph on the tester.</ptxt>
</item>
<item>
<ptxt>If the engine does not start, check the engine speed while cranking.</ptxt>
</item>
<item>
<ptxt>If the engine speed indicated on the tester remains at zero (0), there may be an open or short in the crankshaft position sensor circuit.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TCW0UYX_09_0012" fin="true">OK</down>
<right ref="RM000000TCW0UYX_09_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0001" proc-id="RM23G0E___00003AS00001">
<testtitle>INSPECT CRANKSHAFT POSITION SENSOR (RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the crankshaft position sensor (See page <xref label="Seep01" href="RM0000017E800DX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TCW0UYX_09_0002" fin="false">OK</down>
<right ref="RM000000TCW0UYX_09_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0002" proc-id="RM23G0E___00003AT00001">
<testtitle>CHECK HARNESS AND CONNECTOR (CRANKSHAFT POSITION SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the crankshaft position sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C53-1 (NE) - C62-12 (NE+)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-2 (NE-) - C62-5 (NE-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C53-1 (NE) or C62-12 (NE+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C53-2 (NE-) or C62-5 (NE-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the crankshaft position sensor connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TCW0UYX_09_0003" fin="false">OK</down>
<right ref="RM000000TCW0UYX_09_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0003" proc-id="RM23G0E___00003AU00001">
<testtitle>CHECK SENSOR INSTALLATION (CRANKSHAFT POSITION SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the crankshaft position sensor installation condition.</ptxt>
<figure>
<graphic graphicname="A208464E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor is installed correctly.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TCW0UYX_09_0004" fin="false">OK</down>
<right ref="RM000000TCW0UYX_09_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0004" proc-id="RM23G0E___00003AV00001">
<testtitle>CHECK CRANKSHAFT POSITION SENSOR PLATE (TEETH OF SENSOR PLATE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the teeth of the sensor plate.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Sensor plate teeth do not have any cracks or deformation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TCW0UYX_09_0009" fin="true">OK</down>
<right ref="RM000000TCW0UYX_09_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0012">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0VKX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0014">
<testtitle>REPLACE CRANKSHAFT POSITION SENSOR<xref label="Seep01" href="RM0000017E900DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0006">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (CRANKSHAFT POSITION SENSOR - ECM)</testtitle>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0007">
<testtitle>SECURELY REINSTALL CRANKSHAFT POSITION SENSOR<xref label="Seep01" href="RM0000017E900DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0008">
<testtitle>REPLACE CRANKSHAFT POSITION SENSOR PLATE</testtitle>
</testgrp>
<testgrp id="RM000000TCW0UYX_09_0009">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202UX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>