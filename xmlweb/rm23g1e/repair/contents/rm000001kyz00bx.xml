<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001L" variety="S001L">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001L_7B98O_T00IC" variety="T00IC">
<name>DRIVE BELT (for 5L-E)</name>
<para id="RM000001KYZ00BX" category="G" type-id="8000T" name-id="PA0LV-01" from="201207">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM000001KYZ00BX_01" type-id="01" category="01">
<s-1 id="RM000001KYZ00BX_01_0001" proc-id="RM23G0E___0000BA200000">
<ptxt>CHECK DRIVE BELT TENSION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Visually check the belt for cracks, oiliness or wear. Check that the belt does not touch the bottom of the pulley groove.</ptxt>
<figure>
<graphic graphicname="A074147E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If necessary, replace the belt.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Clearance</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Correct</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Incorrect</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check the drive belt deflection by pressing on the belt at the point indicated in the illustration with 98 N (10 kgf, 22 lbf) of force.</ptxt>
<figure>
<graphic graphicname="A121452E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Crankshaft Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vane Pump Pulley</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Measuring Point for Belt Deflection</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Standard Drive Belt Deflection</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>New belt</ptxt>
</entry>
<entry>
<ptxt>7.5 to 9.5 mm</ptxt>
<ptxt>(0.295 to 0.374 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Used belt</ptxt>
</entry>
<entry>
<ptxt>9.0 to 13 mm</ptxt>
<ptxt>(0.354 to 0.512 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<ptxt>If necessary, adjust the drive belt deflection.</ptxt>
</s2>
<s2>
<ptxt>Using a belt tension gauge, check the drive belt tension.</ptxt>
<spec>
<title>Standard Drive Belt Tension</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry>
<ptxt>Item</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>New belt</ptxt>
</entry>
<entry>
<ptxt>441 to 539 N</ptxt>
<ptxt>(45 to 55 kgf, 99.1 to 121 lbf)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Used belt</ptxt>
</entry>
<entry>
<ptxt>245 to 343 N</ptxt>
<ptxt>(25 to 35 kgf, 55.1 to 77.1 lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>If the belt tension is not as specified, adjust it.</ptxt>
</item>
</list1>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>