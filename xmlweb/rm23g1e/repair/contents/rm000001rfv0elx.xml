<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97M_T00HA" variety="T00HA">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Vacuum Brake Booster)</name>
<para id="RM000001RFV0ELX" category="C" type-id="803P5" name-id="BCDPM-01" from="201207" to="201210">
<dtccode>C1381</dtccode>
<dtcname>Acceleration Sensor Power Supply Voltage Malfunction</dtcname>
<subpara id="RM000001RFV0ELX_01" type-id="60" category="03" proc-id="RM23G0E___0000AP800000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU receives signals from the yaw rate and acceleration sensor via the CAN communication system.</ptxt>
<ptxt>The yaw rate sensor has a built-in acceleration sensor and detects the vehicle's condition using 2 circuits (GL1, GL2).</ptxt>
<ptxt>If there is trouble in the bus lines between the yaw rate and acceleration sensor and the CAN communication system, DTCs U0123 (malfunction in CAN communication with the yaw rate sensor) and U0124 (malfunction in CAN communication with the acceleration sensor) are stored.</ptxt>
<ptxt>These DTCs are also stored when calibration has not been completed.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1381</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>At a vehicle speed of more than 3 km/h (2 mph), the acceleration sensor power source malfunction signal is received for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>ECU-IG NO. 1 fuse</ptxt>
</item>
<item>
<ptxt>Yaw rate and acceleration sensor</ptxt>
</item>
<item>
<ptxt>Yaw rate and acceleration sensor power source circuit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001RFV0ELX_02" type-id="32" category="03" proc-id="RM23G0E___0000AP900000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1232, C1243 and C1245 (See page <xref label="Seep01" href="RM000001RFV0EKX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000001RFV0ELX_03" type-id="51" category="05" proc-id="RM23G0E___0000APA00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the yaw rate and acceleration sensor, perform calibration (See page <xref label="Seep01" href="RM000000XHR08JX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000001RFV0ELX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001RFV0ELX_04_0003" proc-id="RM23G0E___0000APB00000">
<testtitle>CHECK TERMINAL VOLTAGE (IG)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G52 yaw rate and acceleration sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C208309E06" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G52-4 (IG) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Yaw Rate and Acceleration Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001RFV0ELX_04_0004" fin="false">OK</down>
<right ref="RM000001RFV0ELX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001RFV0ELX_04_0004" proc-id="RM23G0E___0000APC00000">
<testtitle>CHECK HARNESS AND CONNECTOR (GND TERMINAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G52 yaw rate and acceleration sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G52-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten3>
<ptxt>Check the yaw rate and acceleration sensor signal after replacement (See page <xref label="Seep01" href="RM000000XHT0ASX"/>).</ptxt>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000001RFV0ELX_04_0011" fin="false">OK</down>
<right ref="RM000001RFV0ELX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001RFV0ELX_04_0011" proc-id="RM23G0E___0000APD00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the yaw rate and acceleration sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0F0X"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Perform a road test.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM000000XHV0F0X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.89in"/>
<colspec colname="COL2" colwidth="1.24in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC C1381 is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC C1381 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001RFV0ELX_04_0012" fin="true">A</down>
<right ref="RM000001RFV0ELX_04_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000001RFV0ELX_04_0012">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001RFV0ELX_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001RFV0ELX_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001RFV0ELX_04_0005">
<testtitle>REPLACE YAW RATE AND ACCELERATION SENSOR<xref label="Seep01" href="RM000000SS5051X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>