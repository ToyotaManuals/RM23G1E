<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM000000TIR0DQX" category="J" type-id="804Q5" name-id="ESTCN-03" from="201210">
<dtccode/>
<dtcname>White Smoke Emitted</dtcname>
<subpara id="RM000000TIR0DQX_03" type-id="60" category="03" proc-id="RM23G0E___000021W00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<step1>
<ptxt>Faults and Symptoms of Common Rail Diesel Components </ptxt>
<step2>
<ptxt>Engine Control</ptxt>
<table pgwide="1">
<title>Turbocharger System</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Main fault</ptxt>
</entry>
<entry>
<ptxt>Turbocharger (compressor side seal ring, turbine side seal ring, shaft, oil drain)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>White smoke</ptxt>
<ptxt>Excessive oil consumption</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Glow System</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Main fault</ptxt>
</entry>
<entry>
<ptxt>Open circuit, glow plug relay fault</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>Difficult to start, rough idle, knocking, white smoke (when cold)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Data List</ptxt>
</entry>
<entry>
<ptxt>Check the glow plug indicator light</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Diagnostic Point</ptxt>
</entry>
<entry>
<ptxt>Measure the resistance of the glow plug</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Engine</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Main fault</ptxt>
</entry>
<entry>
<ptxt>Loss of compression</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>Rough idle (constant lack of power)</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Data List</ptxt>
</entry>
<entry>
<ptxt>Engine Speed of Cyl #1 to #4</ptxt>
<list1 type="unordered">
<item>
<ptxt>When cranking during the "Check the Cylinder Compression" Active Test, if there is a high speed cylinder (approx. 100 rpm more than the other cylinders) that cylinder may lose compression.</ptxt>
</item>
</list1>
<ptxt>Injection Feedback Val #1 to #4</ptxt>
<list1 type="unordered">
<item>
<ptxt>If Injection Feedback Val #1 to #4 is more than 3 mm<sup>3</sup>/st, the cylinder may have a fault.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Diesel Injection</ptxt>
<table pgwide="1">
<title>Injector Assembly</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Main fault</ptxt>
</entry>
<entry>
<ptxt>Blockage</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>Rough idle, lack of power, black smoke, white smoke, knocking</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Data List</ptxt>
</entry>
<entry>
<ptxt>Injection Feedback Val #1 to #4</ptxt>
<list1 type="unordered">
<item>
<ptxt>When an Injector Feedback Val is more than 3 mm<sup>3</sup>/st, there may be a malfunction in the corresponding cylinder. Injection Feedback Val #1 to #4 can be read after idling for 1 minute with the engine warmed up (engine coolant temperature is higher than 70°C (158°F)).</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When the sliding resistance of the internal parts of the injector assemblies (i.e. armature shaft, command piston and plunger) has increased due to internal contamination, injection quantity will increase at high common rail pressure due to a delay in injector assembly closure.</ptxt>
</atten4>
<table pgwide="1">
<title>Injector Driver</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Main fault</ptxt>
</entry>
<entry>
<ptxt>Circuit fault: The injector assembly does not open.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>Difficult to start, rough idle, lack of power, black smoke, white smoke, knocking</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Data List</ptxt>
</entry>
<entry>
<ptxt>Same as injector assembly</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Diagnostic Trouble Code</ptxt>
</entry>
<entry>
<ptxt>When the injector driver has a fault, some DTCs may be stored.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Poor Quality Fuel</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.82in"/>
<colspec colname="COL2" colwidth="1.58in"/>
<colspec colname="COL3" colwidth="4.68in"/>
<tbody>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Main fault</ptxt>
</entry>
<entry>
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2">
<ptxt>Symptoms</ptxt>
</entry>
<entry>
<ptxt>Difficult to start, rough idle (especially when cold)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Diesel EGR</ptxt>
<table pgwide="1">
<title>EGR System</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.38in"/>
<colspec colname="COL2" colwidth="4.70in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Main fault</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Does not move smoothly</ptxt>
</item>
<item>
<ptxt>Does not close fully</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Symptoms</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Rough Idle</ptxt>
</item>
<item>
<ptxt>EGR valve stuck closed: A loud turbocharger sound.</ptxt>
</item>
<item>
<ptxt>EGR valve stuck open: Difficult to start (does not stall), black smoke, white smoke (when engine is cold), lack of power, jerking (If there is an excess in the quantity of EGR and there is a heavy load, when the vehicle starts moving, a lack of power will be felt. Also, when racing the engine, there will be some black smoke).</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Data List</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actual EGR Valve Pos., Target EGR Position</ptxt>
<list1 type="unordered">
<item>
<ptxt>Generally, Actual EGR Valve Pos. = Target EGR Pos. +/-5% (fully closed: 0%, fully open: 100%).</ptxt>
</item>
<item>
<ptxt>Using the EGR valve Active Test, check whether Actual EGR Valve Pos. follows Target EGR Position (the engine coolant temperature and intake air temperature should be considered when a malfunction occurs).</ptxt>
</item>
<item>
<ptxt>EGR valve is fully closed when the ignition switch is ON (engine stopped).</ptxt>
</item>
<item>
<ptxt>EGR valve opens to the halfway point at idling after the engine is warmed up.</ptxt>
</item>
<item>
<ptxt>When the EGR valve does not close, MAF (Mass Air Flow) decreases when the vehicle is accelerated at full throttle. MAP also decreases in comparison to Target Booster Pressure, however there is not a large difference.</ptxt>
</item>
</list1>
<ptxt>EGR Close Lrn. Val., EGR Close Lrn. Status</ptxt>
<list1 type="unordered">
<item>
<ptxt>When leaving the vehicle idling, when EGR Close Lrn. Status is OK, the normal range of EGR Close Lrn. Val. is 0 to 1 V.</ptxt>
</item>
<item>
<ptxt>In cases when EGR Close Lrn. Status. is NG or EGR Close Lrn. Val. is out of the normal range (0 to 1 V), it is possible that the EGR valve cannot completely close.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000TIR0DQX_01" type-id="51" category="05" proc-id="RM23G0E___000021V00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM requires registration (See page <xref label="Seep01" href="RM0000012XK07OX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing a fuel supply pump assembly, the ECM requires initialization (See page <xref label="Seep03" href="RM000000TIN05KX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM requires registration (See page <xref label="Seep04" href="RM0000012XK07OX"/>).</ptxt>
</item>
</list1>
</atten3>
<step1>
<ptxt>Explanation of Symptom</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>White Smoke</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the engine is misfiring, any fuel that has been injected but remains unburned is emitted as white smoke.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Turbocharger internal oil leak (See page <xref label="Seep05" href="RM00000416R093X"/>)</ptxt>
<ptxt>If oil leak occurs from turbine side seal, large amount of white smoke will be emitted from exhaust pipe.</ptxt>
<ptxt>Internal oil leak is not visible from outside of turbocharger. </ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
<subpara id="RM000000TIR0DQX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TIR0DQX_04_0129" proc-id="RM23G0E___000022N00001">
<testtitle>CONFIRM CONDITIONS PRESENT WHEN WHITE SMOKE APPEARED WITH CUSTOMER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Ask the customer about the conditions when the white smoke was emitted.</ptxt>
<ptxt>- What were the driving conditions when the white smoke was generated?</ptxt>
<ptxt>- Is the white smoke constantly emitted, or only occasionally?</ptxt>
<ptxt>- Does the white smoke occur only when the vehicle is cold, or when the vehicle is both cold and hot?</ptxt>
<ptxt>- Is the white smoke emitted just after starting the vehicle only, or does it continue while the vehicle is idling?</ptxt>
</test1>
<test1>
<ptxt>Determine the symptoms of the problem to narrow down the possible causes.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0043" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0043" proc-id="RM23G0E___000021X00001">
<testtitle>READ OUTPUT DTC (RELATING TO ENGINE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read pending DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>No DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Engine related DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0101" fin="false">A</down>
<right ref="RM000000TIR0DQX_04_0099" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0101" proc-id="RM23G0E___000022100001">
<testtitle>CHECK WHEN WHITE SMOKE IS EMITTED</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A221583E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Fully depress the accelerator pedal, and then release it.</ptxt>
</test1>
<test1>
<ptxt>Check whether white smoke is emitted or not when racing the engine.</ptxt>
<atten3>
<ptxt>Be sure not to check for white smoke indoors.</ptxt>
</atten3>
</test1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the white smoke is emitted only just after engine start and disappears later, the smoke is not from the turbocharger.</ptxt>
</item>
<item>
<ptxt>If the turbocharger is the cause of the problem, regardless of whether the engine is cold or warmed up, there will be a large amount of white smoke to the extent that visibility is obstructed for a few meters in the area of the smoke (as shown in the illustration).</ptxt>
</item>
<item>
<ptxt>Depending on whether there is oil mixed with the fuel, or whether there is unburned fuel present, the smell of the exhaust gas differs. When oil is mixed in, the exhaust gas smells like burning oil.</ptxt>
</item>
<item>
<ptxt>When continuously traveling with the engine close to idle for 1 hour or more, such as when in heavy traffic, unburned fuel accumulates in the catalytic converter, and can produce white smoke when accelerating.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>White smoke is emitted just after engine start only</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>White smoke is always and continuously emitted</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>There is oil on the exhaust pipe, etc.</ptxt>
<atten4>
<ptxt>The oil on the exhaust pipe may be from the transmission, etc.</ptxt>
</atten4>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0102" fin="false">A</down>
<right ref="RM000000TIR0DQX_04_0127" fin="false">B</right>
<right ref="RM000000TIR0DQX_04_0128" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0102" proc-id="RM23G0E___000022200001">
<testtitle>CHECK TEMPERATURE WHEN WHITE SMOKE IS EMITTED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the white smoke is emitted only when the engine coolant temperature is below 0°C (32°F).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If so, the smoke is not from the turbocharger and may be the smoke of unburned fuel.</ptxt>
</item>
<item>
<ptxt>If misfiring occurs, unburned fuel is emitted.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>White smoke is emitted when the temperature is below 0°C (32°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>White smoke is emitted regardless of temperature</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0103" fin="false">A</down>
<right ref="RM000000TIR0DQX_04_0105" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0103" proc-id="RM23G0E___000022300001">
<testtitle>INSPECT GLOW PLUG RELAY (GLOW)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the glow plug relay (glow) (See page <xref label="Seep01" href="RM000003BIA025X_01_0004"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0104" fin="false">OK</down>
<right ref="RM000000TIR0DQX_04_0070" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0104" proc-id="RM23G0E___000022400001">
<testtitle>INSPECT GLOW PLUG ASSEMBLY (RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the glow plug (See page <xref label="Seep01" href="RM0000044ZK00EX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0105" fin="false">OK</down>
<right ref="RM000000TIR0DQX_04_0071" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0105" proc-id="RM23G0E___000022500001">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Fuel Press, Target Common Rail Pressure.</ptxt>
</test1>
<test1>
<ptxt>Take a snapshot with the intelligent tester.</ptxt>
<figure>
<graphic graphicname="A176220E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Detailed graphs can be displayed by transferring the stored snapshot from the tester to a PC (personal computer) with Intelligent Viewer installed.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the difference between the target fuel pressure (Target Common Rail Pressure) and the actual fuel pressure (Fuel Press) when racing the engine with the accelerator pedal fully depressed.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Fuel pressure is within 5000 kPa of Target Common Rail Pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>"Target Common Rail Pressure" is the target fuel pressure controlled by the ECM.</ptxt>
</item>
<item>
<ptxt>"Fuel Press" is the actual fuel pressure.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0049" fin="false">A</down>
<right ref="RM000000TIR0DQX_04_0114" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0049" proc-id="RM23G0E___000021Y00001">
<testtitle>CHECK INJECTOR COMPENSATION CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Read the injector compensation codes (See page <xref label="Seep01" href="RM0000012XK07OX_02_0006"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Compensation codes stored in the ECM match the compensation codes of the installed injector assemblies.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0050" fin="false">OK</down>
<right ref="RM000000TIR0DQX_04_0072" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0050" proc-id="RM23G0E___000021Z00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INJECTION FEEDBACK VAL #1 TO #4, INJECTION VOLUME)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check Injection Feedback Val #1 to #4 and Injection Volume in the snapshot taken after the engine is warmed up and idled for 1 minute with the A/C off.</ptxt>
<atten4>
<ptxt>Engine coolant temperature is 70°C (158°F) or higher.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Injection Feedback Val #1 to #4 are 3 mm<sup>3</sup>/st or less and Injection Volume is 4.5 to 10 mm<sup>3</sup>/st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Other than above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0106" fin="false">B</down>
<right ref="RM000000TIR0DQX_04_0125" fin="false">A</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0106" proc-id="RM23G0E___000022600001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (CONTROL THE CYLINDER #1 TO #4 FUEL CUT)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>Use this Active Test to determine the malfunctioning cylinder.</ptxt>
</atten4>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Control the Cylinder #1 to #4 Fuel Cut.</ptxt>
<atten4>
<ptxt>If the engine idle speed does not change when an injector assembly is disabled, the cylinder being tested is malfunctioning. Record any malfunctioning cylinders.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0107" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0107" proc-id="RM23G0E___000022700001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (CHECK THE CYLINDER COMPRESSION)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>Use this Active Test to help determine whether a cylinder has compression loss or not.</ptxt>
</atten4>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Check the Cylinder Compression / Data List / Compression / Engine Speed of Cyl #1 to #4.</ptxt>
</test1>
<test1>
<ptxt>Crank the engine.</ptxt>
</test1>
<test1>
<ptxt>Check the engine speed during the Active Test.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>The values of Engine Speed Cyl #1 to #4 are within 10 rpm of each other</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Other than above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When cranking, if the speed of a cylinder is approximately 100 rpm more than the other cylinders, there is probably a complete loss of compression in that cylinder.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0108" fin="false">B</down>
<right ref="RM000000TIR0DQX_04_0109" fin="false">A</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0108" proc-id="RM23G0E___000022800001">
<testtitle>CHECK CYLINDER COMPRESSION PRESSURE OF MALFUNCTIONING CYLINDER</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>Measure the compression of the cylinder that had a high speed during the "Control the All Cylinders Fuel Cut" Active Test.</ptxt>
</atten4>
<test1>
<ptxt>Check cylinder compression pressure (See page <xref label="Seep01" href="RM000001473025X_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0109" fin="false">OK</down>
<right ref="RM000000TIR0DQX_04_0085" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0109" proc-id="RM23G0E___000022900001">
<testtitle>REPLACE INJECTOR ASSEMBLY OF MALFUNCTIONING CYLINDER</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>It can be determined that the injector assembly is faulty as the corresponding cylinder is malfunctioning, but has no compression loss.</ptxt>
</atten4>
<test1>
<ptxt>Replace the injector assembly of the malfunctioning cylinder (See page <xref label="Seep01" href="RM0000044TN00TX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0110" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0110" proc-id="RM23G0E___000022A00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802NX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0111" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0111" proc-id="RM23G0E___000022B00001">
<testtitle>REGISTER INJECTOR COMPENSATION CODE AND PERFORM PILOT QUANTITY LEARNING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the injector compensation codes (See page <xref label="Seep01" href="RM0000012XK07OX_02_0003"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the injector pilot quantity learning (See page <xref label="Seep02" href="RM0000012XK07OX_02_0009"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0112" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0112" proc-id="RM23G0E___000022C00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the white smoke problem has been successfully repaired by starting the engine.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0121" fin="false">NG</down>
<right ref="RM000000TIR0DQX_04_0065" fin="true">OK</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0121" proc-id="RM23G0E___000022G00001">
<testtitle>CHECK FUEL QUALITY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that only diesel fuel is being used.</ptxt>
</test1>
<test1>
<ptxt>Check that the fuel does not contain any impurities.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0065" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0114" proc-id="RM23G0E___000022D00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802NX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0122" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0122" proc-id="RM23G0E___000022H00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the white smoke problem has been successfully repaired by starting the engine.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0115" fin="false">NG</down>
<right ref="RM000000TIR0DQX_04_0065" fin="true">OK</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0115" proc-id="RM23G0E___000022E00001">
<testtitle>CHECK IF FUEL IS BEING SUPPLIED TO FUEL SUPPLY PUMP ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the inlet hose from the fuel supply pump assembly.</ptxt>
</test1>
<test1>
<ptxt>Operate the priming pump and check that fuel is being supplied to the fuel supply pump assembly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Fuel is properly supplied to the fuel supply pump assembly when the priming pump is operated.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A lack of fuel causes the fuel pressure to drop.</ptxt>
</item>
<item>
<ptxt>Check that the fuel filter element sub-assembly is not clogged.</ptxt>
</item>
</list1>
</atten4>
</test1>
<test1>
<ptxt>Reconnect the inlet hose.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0124" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0124" proc-id="RM23G0E___000022I00001">
<testtitle>REPLACE FUEL FILTER ELEMENT SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>If the fuel filter is obviously dirty or clogged, replace the fuel filter element sub-assembly.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0120" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0120" proc-id="RM23G0E___000022F00001">
<testtitle>CHECK AND REPLACE CLOGGED FUEL PIPE (INCLUDING FROZEN FUEL) (FUEL TANK - FUEL SUPPLY PUMP)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check and replace the clogged fuel pipe.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0130" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0130" proc-id="RM23G0E___000022O00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>If the fuel filter element sub-assembly or fuel pipe has been replaced, be sure to remove any air in the system.</ptxt>
</atten4>
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802NX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0095" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0095" proc-id="RM23G0E___000022000001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the white smoke problem has been successfully repaired by starting the engine.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0065" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0125" proc-id="RM23G0E___000022J00001">
<testtitle>CHECK ELECTRIC EGR CONTROL VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the electric EGR control valve assembly (See page <xref label="Seep01" href="RM000002RK701EX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0126" fin="false">OK</down>
<right ref="RM000000TIR0DQX_04_0131" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0126" proc-id="RM23G0E___000022K00001">
<testtitle>CHECK ENGINE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the engine to see whether white smoke is being emitted due to defects in the engine itself.</ptxt>
<test2>
<ptxt>Check for oil leaking into the combustion chamber due to a faulty valve stem oil seal.</ptxt>
</test2>
<test2>
<ptxt>Check for oil leaking into the combustion chamber due to a faulty injector assembly nozzle sheet.</ptxt>
</test2>
<test2>
<ptxt>Check for low compression.</ptxt>
<atten4>
<ptxt>If the engine runs too rough, Injection Feedback Val #1 to #4 cannot be corrected and the readout will (incorrectly) show that Injection Feedback Val #1 to #4 are within the normal range.</ptxt>
</atten4>
<atten4>
<ptxt>If defects are found, perform repairs, and then check that the symptoms are no longer present.</ptxt>
</atten4>
</test2>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0065" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0127" proc-id="RM23G0E___000022L00001">
<testtitle>CHECK IDLING CONDITION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Check the idling condition.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" align="center" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>There are problems such as rough idling or engine stall, and white smoke is emitted.</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Engine runs smoothly at idle, however white smoke is emitted.</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>It is possible to determine the faulty cylinder by carrying out the "Check the Cylinder Compression" Active Test.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0069" fin="true">B</down>
<right ref="RM000000TIR0DQX_04_0100" fin="true">A</right>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0128" proc-id="RM23G0E___000022M00001">
<testtitle>CHECK FOR OIL LEAKAGE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove any oil in the front exhaust pipe assembly.</ptxt>
<atten4>
<ptxt>Oil in the exhaust pipe is producing white smoke.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>If there is an oil leak, repair the leak.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TIR0DQX_04_0065" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0099">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW051X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0070">
<testtitle>REPLACE GLOW PLUG RELAY</testtitle>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0071">
<testtitle>REPLACE GLOW PLUG ASSEMBLY<xref label="Seep01" href="RM000000JIT00XX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0072">
<testtitle>REGISTER INJECTOR COMPENSATION CODE AND PERFORM PILOT QUANTITY LEARNING</testtitle>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0085">
<testtitle>CHECK ENGINE TO DETERMINE CAUSE OF LOW COMPRESSION</testtitle>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0065">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0131">
<testtitle>REPLACE ELECTRIC EGR CONTROL VALVE ASSEMBLY<xref label="Seep01" href="RM000002RK902XX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0100">
<testtitle>REPAIR ENGINE ASSEMBLY</testtitle>
</testgrp>
<testgrp id="RM000000TIR0DQX_04_0069">
<testtitle>GO TO TURBOCHARGER OIL LEAK AND WHITE SMOKE<xref label="Seep01" href="RM00000416R093X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>