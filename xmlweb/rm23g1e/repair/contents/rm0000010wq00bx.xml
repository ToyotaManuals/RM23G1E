<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12048_S001T" variety="S001T">
<name>2TR-FE BATTERY / CHARGING</name>
<ttl id="12048_S001T_7B9AF_T00K3" variety="T00K3">
<name>GENERATOR (for 100A Type)</name>
<para id="RM0000010WQ00BX" category="G" type-id="3001K" name-id="BH0GP-01" from="201207">
<name>INSPECTION</name>
<subpara id="RM0000010WQ00BX_01" type-id="01" category="01">
<s-1 id="RM0000010WQ00BX_01_0001" proc-id="RM23G0E___0000D5H00000">
<ptxt>INSPECT GENERATOR WITH CLUTCH PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Hold the center of the generator with clutch pulley and check that the outer ring turns counterclockwise and does not turn clockwise.</ptxt>
<figure>
<graphic graphicname="A222452" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Free</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Lock</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the generator with clutch pulley.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010WQ00BX_01_0002" proc-id="RM23G0E___0000D5I00000">
<ptxt>INSPECT GENERATOR BRUSH HOLDER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a vernier caliper, measure the brush length.</ptxt>
<spec>
<title>Standard exposed length</title>
<specitem>
<ptxt>9.5 to 11.5 mm (0.374 to 0.453 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum exposed length</title>
<specitem>
<ptxt>4.5 mm (0.177 in.)</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="A113754E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Length</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the brush length is less than the minimum, replace the generator brush holder assembly.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000010WQ00BX_01_0003" proc-id="RM23G0E___0000D5J00000">
<ptxt>INSPECT GENERATOR ROTOR ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that the generator rotor bearing is not rough or worn.</ptxt>
<figure>
<graphic graphicname="A220305" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If necessary, replace the generator rotor assembly.</ptxt>
</s2>
<s2>
<ptxt>Check the generator rotor for an open circuit.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Slip ring - Slip ring</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>2.3 to 2.7 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="A220306E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Slip Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the generator rotor assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Check if the generator rotor is grounded.</ptxt>
<s3>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Slip ring - Rotor core</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="A220307E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Slip Ring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rotor Core</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the result is not as specified, replace the generator rotor assembly.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Using a vernier caliper, measure the slip ring diameter.</ptxt>
<spec>
<title>Standard diameter</title>
<specitem>
<ptxt>14.2 to 14.4 mm (0.559 to 0.567 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum diameter</title>
<specitem>
<ptxt>14.0 mm (0.551 in.)</ptxt>
</specitem>
</spec>
<figure>
<graphic graphicname="A220308E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diameter</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the diameter is less than the minimum, replace the generator rotor assembly.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>