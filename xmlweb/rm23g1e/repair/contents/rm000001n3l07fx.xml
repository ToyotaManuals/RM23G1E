<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM000001N3L07FX" category="C" type-id="803LZ" name-id="BCDP2-03" from="201210">
<dtccode>C1380</dtccode>
<dtcname>Stop Light Control Relay Malfunction</dtcname>
<subpara id="RM000001N3L07FX_01" type-id="60" category="03" proc-id="RM23G0E___0000AD400003">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Upon receiving the hill-start assist control operating signal from the skid control ECU, the stop light control ECU contact turns on and the stop light comes on.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1380</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Either condition is met:</ptxt>
<list1 type="ordered">
<item>
<ptxt>When the voltage at the IG1 terminal is between 10 and 14 V and the stop light control ECU drive output (STPO) is on, a signal is not input to the STP2 terminal for 5 seconds or more.</ptxt>
</item>
<item>
<ptxt>When the voltage at the IG1 terminal is between 10 and 14 V and the stop light control ECU drive output (STPO) is off, the signal at the STP terminal is different from the input signal at the STP2 terminal for 5 seconds or more.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>STOP fuse</ptxt>
</item>
<item>
<ptxt>Stop light switch circuit</ptxt>
</item>
<item>
<ptxt>Stop light control ECU</ptxt>
</item>
<item>
<ptxt>Stop light control ECU circuit</ptxt>
</item>
<item>
<ptxt>Master cylinder solenoid (Skid control ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000001N3L07FX_02" type-id="32" category="03" proc-id="RM23G0E___0000AD500003">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C234374E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001N3L07FX_03" type-id="51" category="05" proc-id="RM23G0E___0000AD600003">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the master cylinder solenoid, perform calibration (See page <xref label="Seep01" href="RM00000452J00KX"/>).</ptxt>
</item>
<item>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When DTC C1425 is output together with DTC C1380, inspect and repair the trouble areas indicated by DTC C1425 first (See page <xref label="Seep02" href="RM000000XIE0NWX"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000001N3L07FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001N3L07FX_04_0017" proc-id="RM23G0E___0000ADD00003">
<testtitle>CHECK STOP LIGHT OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the stop lights come on when the brake pedal is depressed and go off when the brake pedal is released.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.64in"/>
<colspec colname="COL2" colwidth="1.49in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illumination Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Off</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0019" fin="false">OK</down>
<right ref="RM000001N3L07FX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0019" proc-id="RM23G0E___0000ADE00003">
<testtitle>READ VALUE USING INTELLIGENT TESTER (STOP LIGHT SW)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.64in"/>
<colspec colname="COL2" colwidth="1.93in"/>
<colspec colname="COL3" colwidth="2.42in"/>
<colspec colname="COLSPEC1" colwidth="1.09in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light switch / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Brake pedal depressed</ptxt>
<ptxt>OFF: Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check that the stop light switch condition observed on the intelligent tester changes according to brake pedal operation.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The intelligent tester displays ON or OFF according to brake pedal operation.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0020" fin="false">OK</down>
<right ref="RM000001N3L07FX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0020" proc-id="RM23G0E___0000ADF00003">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (STOP LIGHT RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Active Test.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.76in"/>
<colspec colname="COL2" colwidth="1.76in"/>
<colspec colname="COLSPEC0" colwidth="1.76in"/>
<colspec colname="COL3" colwidth="1.8in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light Relay</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light control ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECU ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Observe the stop lights (do not come on for 2 to 5 seconds).</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>While performing the Active Test, check the illumination condition of the stop lights and "Stop Light Relay Output" in the Data List.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.76in"/>
<colspec colname="COL2" colwidth="1.81in"/>
<colspec colname="COLSPEC0" colwidth="2.40in"/>
<colspec colname="COL3" colwidth="1.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Stop Light Relay Output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stop light control ECU output/ ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Stop light control ECU on (Stop lights on)</ptxt>
<ptxt>OFF: Stop light control ECU off (Stop lights off)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="6.01in"/>
<colspec colname="COL2" colwidth="1.07in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>When Active Test performed, Data List item changes between ON and OFF and stop lights turn on and off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>When Active Test performed, Data List item changes between ON and OFF, but stop lights do not turn on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0003" fin="false">A</down>
<right ref="RM000001N3L07FX_04_0021" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0003" proc-id="RM23G0E___0000AD800003">
<testtitle>CHECK TERMINAL VOLTAGE (STP2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A7 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E30" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A7-45 (STP2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0008" fin="false">OK</down>
<right ref="RM000001N3L07FX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0008" proc-id="RM23G0E___0000ADC00003">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000046KV00LX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Perform a road test.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM0000046KV00LX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.64in"/>
<colspec colname="COL2" colwidth="1.49in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0009" fin="true">A</down>
<right ref="RM000001N3L07FX_04_0016" fin="true">B</right>
<right ref="RM000001N3L07FX_04_0023" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0005" proc-id="RM23G0E___0000AD900003">
<testtitle>CHECK TERMINAL VOLTAGE AND RESISTANCE (+B, GND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A54 stop light control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C207207E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.71in"/>
<colspec colname="COL2" colwidth="1.05in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A54-6 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.71in"/>
<colspec colname="COL2" colwidth="1.05in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A54-8 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Stop Light Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0006" fin="false">OK</down>
<right ref="RM000001N3L07FX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0006" proc-id="RM23G0E___0000ADA00003">
<testtitle>CHECK TERMINAL VOLTAGE (STP)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A54 stop light control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C207207E05" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.52in"/>
<colspec colname="COL3" colwidth="1.23in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A54-5 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Stop Light Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0007" fin="false">OK</down>
<right ref="RM000001N3L07FX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0007" proc-id="RM23G0E___0000ADB00003">
<testtitle>CHECK HARNESS AND CONNECTOR (STOP LIGHT CONTROL ECU - SKID CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A54 stop light control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A7 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.93in"/>
<colspec colname="COL2" colwidth="1.00in"/>
<colspec colname="COL3" colwidth="1.20in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A54-1 (OUT) - A7-45 (STP2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A54-1 (OUT) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0018" fin="true">OK</down>
<right ref="RM000001N3L07FX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0002" proc-id="RM23G0E___0000AD700003">
<testtitle>CHECK TERMINAL VOLTAGE (STP)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A7 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C214161E31" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.44in"/>
<colspec colname="COL2" colwidth="1.50in"/>
<colspec colname="COL3" colwidth="1.19in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>A7-7 (STP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Brake pedal depressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>8 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Brake pedal released</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Skid Control ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.64in"/>
<colspec colname="COL2" colwidth="1.49in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0012" fin="true">A</down>
<right ref="RM000001N3L07FX_04_0016" fin="true">B</right>
<right ref="RM000001N3L07FX_04_0023" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0021" proc-id="RM23G0E___0000ADG00003">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - STOP LIGHT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A7 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A54 stop light control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.93in"/>
<colspec colname="COL2" colwidth="1.00in"/>
<colspec colname="COL3" colwidth="1.20in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A7-16 (STP0) - A54-9 (ACC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A7-16 (STP0) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0022" fin="false">OK</down>
<right ref="RM000001N3L07FX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0022" proc-id="RM23G0E___0000ADH00003">
<testtitle>CHECK HARNESS AND CONNECTOR (SKID CONTROL ECU - STOP LIGHT CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A7 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the A54 stop light control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.93in"/>
<colspec colname="COL2" colwidth="1.00in"/>
<colspec colname="COL3" colwidth="1.20in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A7-45 (STP2) - A54-1 (OUT)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A7-45 (STP2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001N3L07FX_04_0018" fin="true">OK</down>
<right ref="RM000001N3L07FX_04_0012" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0009">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0016">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0018">
<testtitle>REPLACE STOP LIGHT CONTROL ECU<xref label="Seep01" href="RM000003MWQ01WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0012">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001N3L07FX_04_0023">
<testtitle>REPLACE MASTER CYLINDER SOLENOID<xref label="Seep01" href="RM00000171U01OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>