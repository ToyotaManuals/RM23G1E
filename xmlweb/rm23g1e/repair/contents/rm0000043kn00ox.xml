<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S0023" variety="S0023">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S0023_7B9CL_T00M9" variety="T00M9">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM0000043KN00OX" category="J" type-id="802YQ" name-id="PC15L-02" from="201210">
<dtccode/>
<dtcname>Pre-crash Safety Brake Cancel Switch Circuit</dtcname>
<subpara id="RM0000043KN00OX_01" type-id="60" category="03" proc-id="RM23G0E___0000FW800001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The driving support ECU receives pre-crash brake on/off signals from the pre-crash brake cancel switch assembly.</ptxt>
</content5>
</subpara>
<subpara id="RM0000043KN00OX_02" type-id="32" category="03" proc-id="RM23G0E___0000FW900001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E169489E29" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000043KN00OX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000043KN00OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000043KN00OX_04_0001" proc-id="RM23G0E___0000FWA00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Use the Data List to check if the pre-crash brake cancel switch assembly is functioning properly (See page <xref label="Seep01" href="RM0000043KP00BX"/>).</ptxt>
<table pgwide="1">
<title>Pre-Crash 2</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Pre-Crash Brake OFF Switch</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Pre-crash brake cancel switch assembly signal / ON or OFF</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>ON: Pre-crash brake cancel switch assembly on</ptxt>
<ptxt>OFF: Pre-crash brake cancel switch assembly off</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>On screen, each item changes between ON and OFF according to above chart.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000043KN00OX_04_0004" fin="true">OK</down>
<right ref="RM0000043KN00OX_04_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043KN00OX_04_0002" proc-id="RM23G0E___0000FWB00001">
<testtitle>INSPECT PRE-CRASH BRAKE CANCEL SWITCH ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the pre-crash brake cancel switch assembly (See page <xref label="Seep01" href="RM000003QHL00HX"/>).</ptxt>
<figure>
<graphic graphicname="E118525E41" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="1" valign="middle">
<ptxt>1 (GND) - 2 (ECU)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Not pressed</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000043KN00OX_04_0003" fin="false">OK</down>
<right ref="RM0000043KN00OX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043KN00OX_04_0003" proc-id="RM23G0E___0000FWC00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PRE-CRASH BRAKE CANCEL SWITCH - DRIVING SUPPORT ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G121 switch connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G119 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G121-2 (ECU) - G119-5 (PBSW)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G121-1 (GND) - Body ground</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>G121-2 (ECU) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000043KN00OX_04_0006" fin="true">OK</down>
<right ref="RM0000043KN00OX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043KN00OX_04_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000043JV00GX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043KN00OX_04_0005">
<testtitle>REPLACE PRE-CRASH BRAKE CANCEL SWITCH ASSEMBLY<xref label="Seep01" href="RM000003QHL00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043KN00OX_04_0006">
<testtitle>REPLACE DRIVING SUPPORT ECU<xref label="Seep01" href="RM000002OJO018X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043KN00OX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>