<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E8_T00NW" variety="T00NW">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000001J2807BX" category="J" type-id="30383" name-id="AC96R-08" from="201210">
<dtccode/>
<dtcname>Rear Air Conditioning Control Panel Circuit</dtcname>
<subpara id="RM000001J2807BX_01" type-id="60" category="03" proc-id="RM23G0E___0000HD100001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This circuit consists of the air conditioning control assembly and air conditioning amplifier. When the air conditioning control assembly is operated, signals are transmitted to the air conditioning amplifier through the LIN communication system.</ptxt>
<ptxt>If the LIN communication system malfunctions, the air conditioning amplifier does not operate even if the air conditioning control assembly is operated.</ptxt>
</content5>
</subpara>
<subpara id="RM000001J2807BX_02" type-id="32" category="03" proc-id="RM23G0E___0000HD200001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E157369E35" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001J2807BX_03" type-id="51" category="05" proc-id="RM23G0E___0000HD300001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001J2807BX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001J2807BX_05_0018" proc-id="RM23G0E___0000HD600001">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING CONTROL - AIR CONDITIONING AMPLIFIER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G24 amplifier connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the T3 control connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>T3-3 (RLIN) - G24-13 (RLIN)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>T3-3 (RLIN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2807BX_05_0014" fin="false">OK</down>
<right ref="RM000001J2807BX_05_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2807BX_05_0014" proc-id="RM23G0E___0000HD500001">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR CONDITIONING CONTROL - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the T3 control connector.</ptxt>
<figure>
<graphic graphicname="E196918E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>T3-5 (IG) - T3-8 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>T3-5 (IG) - T3-8 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>T3-8 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Air Conditioning Control Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000001J2807BX_05_0004" fin="false">OK</down>
<right ref="RM000001J2807BX_05_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2807BX_05_0004" proc-id="RM23G0E___0000HD400001">
<testtitle>REPLACE AIR CONDITIONING CONTROL ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the air conditioning control assembly with a new or normally functioning one (See page <xref label="Seep01" href="RM0000039QL00NX"/>).</ptxt>
</test1>
<test1>
<ptxt>Operate the air conditioning control assembly to check that it functions properly.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Air conditioning control assembly operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001J2807BX_05_0017" fin="true">OK</down>
<right ref="RM000001J2807BX_05_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001J2807BX_05_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000001J2807BX_05_0009">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000045E301UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001J2807BX_05_0017">
<testtitle>END (AIR CONDITIONING CONTROL ASSEMBLY IS FAULTY)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>