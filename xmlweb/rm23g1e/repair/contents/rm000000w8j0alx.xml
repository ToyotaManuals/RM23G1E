<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94I_T00E6" variety="T00E6">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1GR-FE)</name>
<para id="RM000000W8J0ALX" category="C" type-id="800KQ" name-id="AT7MR-02" from="201207" to="201210">
<dtccode>P0751</dtccode>
<dtcname>Shift Solenoid "A" Performance (Shift Solenoid Valve S1)</dtcname>
<subpara id="RM000000W8J0ALX_01" type-id="60" category="03" proc-id="RM23G0E___00008BQ00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses signals from the output shaft speed sensor and input speed sensor to detect the actual gear position (1st, 2nd, 3rd, 4th or 5th gear).</ptxt>
<ptxt>Then the ECM compares the actual gear with the shift schedule in the ECM memory to detect mechanical problems of the shift solenoid valves, valve body or automatic transmission (clutch, brake, gear, etc.).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0751</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gear required by the ECM does not match the actual gear when driving (2-trip detection logic*1, 1-trip detection logic*2).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve S1 remains open or closed</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SLT remains open or closed</ptxt>
</item>
<item>
<ptxt>Valve body is blocked</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ OBD</ptxt>
</item>
<item>
<ptxt>*2: w/o OBD</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000W8J0ALX_02" type-id="64" category="03" proc-id="RM23G0E___00008BR00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates a stuck ON malfunction or stuck OFF malfunction of the shift solenoid valve S1.</ptxt>
<ptxt>The ECM commands gear shifts by turning the shift solenoid valves ON/OFF. When the gear position commanded by the ECM and the actual gear position are not the same, the ECM illuminates the MIL and stores the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM000000W8J0ALX_06" type-id="51" category="05" proc-id="RM23G0E___00008BS00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
<atten4>
<ptxt>While driving, the shift position can be forcibly changed with the intelligent tester.</ptxt>
<ptxt>Comparing the shift position commanded by the Active Test with the actual shift position enables you to confirm the problem (See page <xref label="Seep01" href="RM000000O8L0N0X"/>).</ptxt>
</atten4>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Control the Shift Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve and set each shift position</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Press "→" button: Shift up</ptxt>
</item>
<item>
<ptxt>Press "←" button: Shift down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Possible to check operation of the shift solenoid valves.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<ptxt>50 km/h (31 mph) or less.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This test can be conducted when the vehicle speed is 50 km/h (31 mph) or less.</ptxt>
</item>
<item>
<ptxt>The 4th to 5th up-shift must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>The 5th to 4th down-shift must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>Do not operate the accelerator pedal for at least 2 seconds after shifting and do not shift successively.</ptxt>
</item>
<item>
<ptxt>The shift position commanded by the ECM is shown in the Data List display on the tester.</ptxt>
</item>
<item>
<ptxt>The shift solenoid valve S1 turns ON/OFF normally when the shift lever is in D.</ptxt>
<table pgwide="1">
<tgroup cols="6">
<colspec colname="COL1" align="left" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.13in"/>
<colspec colname="COL3" colwidth="1.13in"/>
<colspec colname="COL4" colwidth="1.13in"/>
<colspec colname="COL5" colwidth="1.13in"/>
<colspec colname="COL6" colwidth="1.14in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>ECM gear shift command</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve S1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W8J0ALX_07" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W8J0ALX_07_0001" proc-id="RM23G0E___00008BT00000">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P0751)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only P0751 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0751 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any other codes besides P0751 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W8J0ALX_07_0011" fin="false">A</down>
<right ref="RM000000W8J0ALX_07_0005" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000W8J0ALX_07_0011" proc-id="RM23G0E___000088F00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SHIFT SOLENOID VALVE SLT)
</testtitle>
<content6 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the test while the ATF temperature is 50 to 80°C (122 to 176°F).</ptxt>
</item>
<item>
<ptxt>Be careful to prevent SST's hose from interfering with the exhaust pipe.</ptxt>
</item>
<item>
<ptxt>Perform the test with the A/C off.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<test1>
<ptxt>Remove the test plug from the transmission case and connect SST.</ptxt>
<figure>
<graphic graphicname="C214323E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<sst>
<sstitem>
<s-number>09992-00095</s-number>
<s-subnumber>09992-00231</s-subnumber>
<s-subnumber>09992-00271</s-subnumber>
</sstitem>
</sst>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Measure the line pressure with SST.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
</test1>
<test1>
<ptxt>Measure the line pressure.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLT)*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid SLT and raise line pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>OFF: Line pressure up (when Active Test "Activate the Solenoid (SLT)" is performed, ECM commands SLT solenoid to turn OFF)</ptxt>
</item>
<item>
<ptxt>ON: No action (normal operation)</ptxt>
</item>
</list1>
</atten4>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Vehicle stopped.</ptxt>
</item>
<item>
<ptxt>Engine idling.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Activate the Solenoid (SLT) in the Active Test is performed to check the line pressure changes by connecting SST to the automatic transmission, which is used in the Hydraulic Test (See page <xref label="Seep01" href="RM000000W7B0NFX"/>) as well. Note that the pressure values in the Active Test and Hydraulic Test are different.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The line pressure changes as specified when performing the Active Test.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000W8J0ALX_07_0002" fin="false">OK</down>
<right ref="RM000000W8J0ALX_07_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8J0ALX_07_0002" proc-id="RM23G0E___000089400000">
<testtitle>INSPECT SHIFT SOLENOID VALVE S1</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove shift solenoid valve S1.</ptxt>
<figure>
<graphic graphicname="C214327E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve S1 connector terminal - Shift solenoid valve S1 body</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 15 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) → Shift solenoid valve S1 connector</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Shift solenoid valve S1 body</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve S1)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000W8J0ALX_07_0010" fin="false">OK</down>
<right ref="RM000000W8J0ALX_07_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8J0ALX_07_0010" proc-id="RM23G0E___000088G00000">
<testtitle>INSPECT TRANSMISSION VALVE BODY ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the transmission valve body assembly (See page <xref label="Seep01" href="RM0000013FG02LX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There are no foreign objects on any valve.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000W8J0ALX_07_0007" fin="true">OK</down>
<right ref="RM000000W8J0ALX_07_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8J0ALX_07_0007">
<testtitle>REPAIR OR REPLACE AUTOMATIC TRANSMISSION ASSEMBLY<xref label="Seep01" href="RM0000018ZD03RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8J0ALX_07_0005">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G906LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8J0ALX_07_0009">
<testtitle>REPLACE SHIFT SOLENOID VALVE SLT<xref label="Seep01" href="RM0000013FG02LX_01_0005"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8J0ALX_07_0006">
<testtitle>REPLACE SHIFT SOLENOID VALVE S1<xref label="Seep01" href="RM0000013FG02LX_01_0007"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8J0ALX_07_0004">
<testtitle>REPAIR OR REPLACE TRANSMISSION VALVE BODY ASSEMBLY<xref label="Seep01" href="RM0000013CM04BX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>