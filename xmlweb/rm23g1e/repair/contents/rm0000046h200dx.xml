<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001B" variety="S001B">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001B_7B971_T00GP" variety="T00GP">
<name>KINETIC DYNAMIC SUSPENSION SYSTEM</name>
<para id="RM0000046H200DX" category="T" type-id="3001H" name-id="SC1HM-10" from="201207" to="201210">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM0000046H200DX_z0" proc-id="RM23G0E___00009TK00000">
<content5 releasenbr="17">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms.  If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table.</ptxt>
<ptxt>Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and relays related to this system before inspecting the suspected areas below.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Kinetic Dynamic Suspension System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="3" colsep="1" valign="middle">
<ptxt>Kinetic dynamic suspension system DTC cannot be output (When using intelligent tester)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>CAN communication system (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO07BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>CAN communication system (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO07DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Harness or connector (power source of stabilizer control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000045UG00GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Stabilizer control ECU</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001N90013X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="4" colsep="1" valign="middle">
<ptxt>Kinetic dynamic suspension system DTC cannot be output (When not using intelligent tester)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>CAN communication system (for LHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO07BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>CAN communication system (for RHD)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO07DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Harness or connector (power source of stabilizer control ECU)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000045UG00GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Stabilizer control ECU</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001N90013X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Combination meter</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002Z4L031X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>KDSS indicator light does not come on</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>CAN communication system (for LHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO07BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>CAN communication system (for RHD)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSO07DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Combination meter</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002Z4L031X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="2" colsep="1" valign="middle">
<ptxt>Vehicle is tilted</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Stabilizer control solenoid valve</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002KGF00KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Fluid leakage</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000038UA00TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Bleed air (check for clogs in hydraulic circuit and lack or excess of fluid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000038U700NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="4" colsep="1" valign="middle">
<ptxt>Roll is not controlled when driving</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Brake control system</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000046KV00IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Stabilizer control solenoid valve</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002KGF00KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Fluid leakage</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000038UA00TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Bleed air (check for clogs in hydraulic circuit and lack or excess of fluid)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000038U700NX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Accumulator pressure sensor</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002KGD00KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>