<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000E" variety="S000E">
<name>1KD-FTV FUEL</name>
<ttl id="12008_S000E_7B8YS_T008G" variety="T008G">
<name>COMMON RAIL (w/o DPF)</name>
<para id="RM0000044W300JX" category="A" type-id="80001" name-id="FU7L0-02" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM0000044W300JX_02" type-id="11" category="10" proc-id="RM23G0E___00005KQ00000">
<content3 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injectors (including shuffling the injectors between the cylinders), common rail or cylinder head, it is necessary to replace the injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>When replacing the fuel supply pump, common rail, cylinder block, cylinder head, cylinder head gasket or timing gear case, it is necessary to replace the fuel inlet pipe with a new one.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipes and fuel inlet pipe, clean them with a brush and compressed air.</ptxt>
</item>
</list1>
</atten3>
</content3>
</subpara>
<subpara id="RM0000044W300JX_01" type-id="01" category="01">
<s-1 id="RM0000044W300JX_01_0019" proc-id="RM23G0E___00005KN00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000044W300JX_01_0002" proc-id="RM23G0E___00005KL00000">
<ptxt>REMOVE ELECTRIC EGR CONTROL VALVE ASSEMBLY (w/ EGR System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the electric EGR control valve assembly (See page <xref label="Seep01" href="RM000002RK902BX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044W300JX_01_0030" proc-id="RM23G0E___00001B700000">
<ptxt>DISCONNECT INLET HEATER WATER HOSE (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the inlet heater water hose.</ptxt>
<figure>
<graphic graphicname="A224418" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0031" proc-id="RM23G0E___00001B800000">
<ptxt>DISCONNECT NO. 4 VACUUM TRANSMITTING PIPE SUB-ASSEMBLY (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and disconnect the No. 4 vacuum transmitting pipe.</ptxt>
<figure>
<graphic graphicname="A222558" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0032" proc-id="RM23G0E___00001B500000">
<ptxt>REMOVE NO. 1 INTAKE PIPE (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 3 connectors from the intake air temperature sensor, throttle control motor and manifold absolute pressure sensor.</ptxt>
<figure>
<graphic graphicname="A222559" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 2 clamps.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the vacuum hose from the manifold absolute pressure sensor.</ptxt>
</s2>
<s2>
<ptxt>Loosen the 2 hose clamps of the No. 1 air hose.</ptxt>
<figure>
<graphic graphicname="A222560" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Loosen the hose clamp of the intercooler air hose.</ptxt>
<figure>
<graphic graphicname="A222561" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and No. 1 intake pipe.</ptxt>
<figure>
<graphic graphicname="A222562" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0020" proc-id="RM23G0E___00004C800000">
<ptxt>REMOVE THROTTLE BODY BRACKET (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the vacuum hose.</ptxt>
<figure>
<graphic graphicname="A230166" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the bolt and gas filter with gas filter bracket.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and throttle body bracket.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0021" proc-id="RM23G0E___00005K700000">
<ptxt>REMOVE NO. 1, NO. 2 AND NO. 3 INJECTION PIPE SUB-ASSEMBLY (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After removing the injection pipe, cover the outlets on the common rail with tape to keep out foreign matter.</ptxt>
</item>
<item>
<ptxt>After removing the injection pipe, put it in a plastic bag to prevent foreign matter from contaminating its injector inlet.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Remove the 2 nuts and No. 3 injection pipe clamp.</ptxt>
<figure>
<graphic graphicname="A223285" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and 2 No. 2 injection pipe clamps.</ptxt>
<figure>
<graphic graphicname="A223286" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, loosen the union nuts and remove the No. 1, No. 2 and No. 3 injection pipes.</ptxt>
<figure>
<graphic graphicname="A223287E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0022" proc-id="RM23G0E___00004C900000">
<ptxt>REMOVE AIR CONNECTOR STAY (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 3 bolts and air connector stay.</ptxt>
<figure>
<graphic graphicname="A230167" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0033" proc-id="RM23G0E___00004CD00000">
<ptxt>DISCONNECT ENGINE WIRE (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Remove the 2 bolts and disconnect the clamp and engine wire.</ptxt>
<figure>
<graphic graphicname="A225769" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<ptxt>Remove the bolt and disconnect the engine wire.</ptxt>
<figure>
<graphic graphicname="A225770" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0034" proc-id="RM23G0E___00004CE00000">
<ptxt>REMOVE INTAKE AIR CONNECTOR WITH DIESEL THROTTLE BODY ASSEMBLY (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the throttle position sensor connector.</ptxt>
<figure>
<graphic graphicname="A230087" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 bolts, intake air connector with diesel throttle and gasket.</ptxt>
<figure>
<graphic graphicname="A230165" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0025" proc-id="RM23G0E___00005K800000">
<ptxt>REMOVE MANIFOLD STAY WITH VACUUM SWITCHING VALVE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the vacuum switching valve connector.</ptxt>
<s3>
<ptxt>w/o EGR System:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="A222570" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>w/ EGR System without EGR Cooler:</ptxt>
<ptxt>Disconnect the 2 connectors.</ptxt>
<figure>
<graphic graphicname="A222575" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
<s3>
<ptxt>w/ EGR System with EGR Cooler:</ptxt>
<ptxt>Disconnect the 3 connectors.</ptxt>
<figure>
<graphic graphicname="A222581" width="2.775699831in" height="1.771723296in"/>
</figure>
</s3>
</s2>
<s2>
<ptxt>Disconnect the No. 1 vacuum transmitting hose.</ptxt>
<figure>
<graphic graphicname="A224689" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ EGR System:</ptxt>
<ptxt>Disconnect the No. 2 vacuum transmitting hose and No. 3 vacuum transmitting hose.</ptxt>
<figure>
<graphic graphicname="A224691" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ EGR Cooler:</ptxt>
<ptxt>Disconnect the No. 3 vacuum transmitting hose.</ptxt>
<figure>
<graphic graphicname="A224690" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the No. 3 vacuum transmitting hose and No. 4 vacuum transmitting hose.</ptxt>
<figure>
<graphic graphicname="A224718E03" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry>
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>w/o EGR System</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>w/ EGR System</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 2 bolts and manifold stay with vacuum switching valve.</ptxt>
<figure>
<graphic graphicname="A222572" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0026" proc-id="RM23G0E___000048X00000">
<ptxt>REMOVE OIL FILTER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using SST, remove the oil filter.</ptxt>
<figure>
<graphic graphicname="A227248E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09228-07501</s-number>
</sstitem>
</sst>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0027" proc-id="RM23G0E___00005KO00000">
<ptxt>REMOVE VACUUM CONTROL VALVE SET
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 2 VSV connectors and 3 vacuum hoses.</ptxt>
<figure>
<graphic graphicname="A152488" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 2 bolts and vacuum control valve set.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0028" proc-id="RM23G0E___00005K100000">
<ptxt>REMOVE NO. 4 INJECTION PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and detach the injection pipe clamp.</ptxt>
<figure>
<graphic graphicname="A152275" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>If an injection pipe clamp is removed from the No. 4 injection pipe, replace the injection clamp with a new one.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, loosen the union nuts and remove the No. 4 injection pipe.</ptxt>
<figure>
<graphic graphicname="A152276E06" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0014" proc-id="RM23G0E___00005K400000">
<ptxt>REMOVE NO. 2 NOZZLE LEAKAGE PIPE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the 3 fuel hoses.</ptxt>
<figure>
<graphic graphicname="A152742E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the union bolt, 4 bolts, No. 2 nozzle leakage pipe and gasket.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Union Bolt</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000044W300JX_01_0029" proc-id="RM23G0E___00005KP00000">
<ptxt>REMOVE FUEL INLET PIPE SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the bolt and clamp.</ptxt>
<figure>
<graphic graphicname="A152732" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, loosen the union nuts and remove the fuel inlet pipe.</ptxt>
<figure>
<graphic graphicname="A152733E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fuel Supply Pump Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000044W300JX_01_0016" proc-id="RM23G0E___00005KM00000">
<ptxt>REMOVE COMMON RAIL ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel pressure sensor connector and pressure discharge valve connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, common rail and No. 2 intake manifold insulator.</ptxt>
<figure>
<graphic graphicname="A152743" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Do not remove the pressure discharge valve and fuel pressure sensor.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>