<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A6_T00JU" variety="T00JU">
<name>REAR VIEW MONITOR SYSTEM (w/o Side Monitor System)</name>
<para id="RM000003QOP00HX" category="J" type-id="303DF" name-id="PM3A3-04" from="201207" to="201210">
<dtccode/>
<dtcname>Reverse Signal Circuit</dtcname>
<subpara id="RM000003QOP00HX_01" type-id="60" category="03" proc-id="RM23G0E___0000D0H00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The display and navigation module display*1 or accessory meter assembly*2 receives a reverse signal from the park/neutral position switch assembly*3 or back-up light switch assembly*4.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Navigation System</ptxt>
</item>
<item>
<ptxt>*2: w/o Navigation System</ptxt>
</item>
<item>
<ptxt>*3: for Automatic Transmission</ptxt>
</item>
<item>
<ptxt>*4: for Manual Transmission</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000003QOP00HX_02" type-id="32" category="03" proc-id="RM23G0E___0000D0I00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E195952E04" width="7.106578999in" height="3.779676365in"/>
</figure>
<figure>
<graphic graphicname="E195952E06" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003QOP00HX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003QOP00HX_06" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003QOP00HX_06_0019" proc-id="RM23G0E___0000D0N00000">
<testtitle>CHECK NAVIGATION SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the navigation system.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>w/ Navigation System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>w/o Navigation System</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003QOP00HX_06_0015" fin="false">A</down>
<right ref="RM000003QOP00HX_06_0020" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0015" proc-id="RM23G0E___0000D0M00000">
<testtitle>CHECK DISPLAY AND NAVIGATION MODULE DISPLAY (DISPLAY CHECK MODE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter the "Failure Check/Setting" mode and select "Vehicle Signal" (See page <xref label="Seep01" href="RM0000043X902GX"/>).</ptxt>
<figure>
<graphic graphicname="E178235E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Check that the display changes between ON and OFF according to the shift lever operation.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Shift Lever Position</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Display</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>This display is updated once per second. As a result, it is normal for the display to lag behind the actual change in the switch.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000003QOP00HX_06_0004" fin="true">OK</down>
<right ref="RM000003QOP00HX_06_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0002" proc-id="RM23G0E___0000D0J00000">
<testtitle>CHECK DISPLAY AND NAVIGATION MODULE DISPLAY (REVERSE SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="B157223E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H4-16 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H4-16 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, shift lever not in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Display and Navigation Module Display)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Automatic Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Manual Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003QOP00HX_06_0016" fin="true">A</down>
<right ref="RM000003QOP00HX_06_0003" fin="false">B</right>
<right ref="RM000003QOP00HX_06_0010" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0003" proc-id="RM23G0E___0000D0K00000">
<testtitle>CHECK HARNESS AND CONNECTOR (DISPLAY AND NAVIGATION MODULE - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H4 display and navigation module display connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H4-16 (REV) - C40-1 (RL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H4-16 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003QOP00HX_06_0006" fin="true">OK</down>
<right ref="RM000003QOP00HX_06_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0010" proc-id="RM23G0E___0000D0L00000">
<testtitle>CHECK HARNESS AND CONNECTOR (DISPLAY AND NAVIGATION MODULE - BACK-UP LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H4 display and navigation module display connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C41 back-up light switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H4-16 (REV) - C41-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H4-16 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003QOP00HX_06_0012" fin="true">OK</down>
<right ref="RM000003QOP00HX_06_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0004">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000003QOO00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0016">
<testtitle>REPLACE DISPLAY AND NAVIGATION MODULE DISPLAY<xref label="Seep01" href="RM000003B6H01LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0006">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0012">
<testtitle>REPLACE BACK-UP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000047AR002X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0014">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0020" proc-id="RM23G0E___0000D0O00000">
<testtitle>CHECK ACCESSORY METER ASSEMBLY (REVERSE SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E194335E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H42-13 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H42-13 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, shift lever not in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Component with harness connected</ptxt>
<ptxt>(Accessory Meter Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Automatic Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Manual Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003QOP00HX_06_0024" fin="true">A</down>
<right ref="RM000003QOP00HX_06_0021" fin="false">B</right>
<right ref="RM000003QOP00HX_06_0022" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0021" proc-id="RM23G0E___0000D0P00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ACCESSORY METER - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H42 accessory meter assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H42-13 (REV) - C40-1 (RL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H42-13 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003QOP00HX_06_0025" fin="true">OK</down>
<right ref="RM000003QOP00HX_06_0026" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0022" proc-id="RM23G0E___0000D0Q00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ACCESSORY METER - BACK-UP LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H42 accessory meter assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C41 back-up light switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H42-13 (REV) - C41-1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H42-13 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003QOP00HX_06_0027" fin="true">OK</down>
<right ref="RM000003QOP00HX_06_0028" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0024">
<testtitle>REPLACE ACCESSORY METER ASSEMBLY<xref label="Seep01" href="RM0000045XR009X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0025">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0026">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0027">
<testtitle>REPLACE BACK-UP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000047AR002X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003QOP00HX_06_0028">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>