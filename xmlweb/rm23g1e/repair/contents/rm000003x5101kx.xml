<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B990_T00IO" variety="T00IO">
<name>REAR SEAT ENTERTAINMENT SYSTEM</name>
<para id="RM000003X5101KX" category="J" type-id="804T4" name-id="AV9PY-02" from="201207" to="201210">
<dtccode/>
<dtcname>Visual Mute Signal Circuit between Navigation ECU and Television Display</dtcname>
<subpara id="RM000003X5101KX_01" type-id="60" category="03" proc-id="RM23G0E___0000BS900000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This is the visual mute signal circuit from the display and navigation module display to the television display assembly.</ptxt>
</content5>
</subpara>
<subpara id="RM000003X5101KX_02" type-id="32" category="03" proc-id="RM23G0E___0000BSA00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E182539E12" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003X5101KX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000003X5101KX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003X5101KX_04_0004" proc-id="RM23G0E___0000BSC00000">
<testtitle>CHECK TELEVISION DISPLAY ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E140936E20" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>W14-9 (VMTR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>RSE playing → Source changed</ptxt>
</entry>
<entry valign="middle">
<ptxt>4 V or higher → Below 0.7 V → 4 V or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Television Display Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003X5101KX_04_0002" fin="true">OK</down>
<right ref="RM000003X5101KX_04_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003X5101KX_04_0001" proc-id="RM23G0E___0000BSB00000">
<testtitle>CHECK HARNESS AND CONNECTOR (DISPLAY AND NAVIGATION MODULE DISPLAY - TELEVISION DISPLAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H2 display and navigation module display connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the W14 television display assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>H2-4 (VMTR) - W14-9 (VMTR)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>H2-4 (VMTR) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003X5101KX_04_0006" fin="true">OK</down>
<right ref="RM000003X5101KX_04_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003X5101KX_04_0002">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM0000033VP012X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003X5101KX_04_0003">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003X5101KX_04_0006">
<testtitle>REPLACE DISPLAY AND NAVIGATION MODULE DISPLAY<xref label="Seep01" href="RM000003B6H01LX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>