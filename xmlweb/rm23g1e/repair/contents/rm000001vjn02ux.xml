<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12065_S002A" variety="S002A">
<name>SLIDING ROOF / CONVERTIBLE</name>
<ttl id="12065_S002A_7B9G3_T00PR" variety="T00PR">
<name>SLIDING ROOF SYSTEM</name>
<para id="RM000001VJN02UX" category="D" type-id="303F2" name-id="RF041-33" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000001VJN02UX_z0" proc-id="RM23G0E___0000IJA00000">
<content5 releasenbr="1">
<step1>
<ptxt>GENERAL</ptxt>
<ptxt>This system has the following functions: manual slide open and close, auto slide open and close, manual tilt up and down, auto tilt up and down, jam protection, key off operation, key-linked operation and wireless transmitter-linked operation.</ptxt>
<atten4>
<ptxt>If the driver side door is opened with the ignition switch off and the sliding roof open, the buzzer sounds.</ptxt>
</atten4>
</step1>
<step1>
<ptxt>FUNCTION OF MAIN COMPONENT</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Sliding roof drive gear sub-assembly (Sliding roof ECU)</ptxt>
</entry>
<entry>
<ptxt>The sliding roof ECU controls the rotational direction of the motor, which tilts or slides the sliding roof glass.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Map light assembly (Sliding roof switch)</ptxt>
</entry>
<entry>
<ptxt>The sliding roof switch outputs operation signals to the sliding roof ECU.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>SYSTEM OPERATION</ptxt>
<ptxt>The sliding roof has the following features.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Function</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Manual slide open and close</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>This function causes the sliding roof to open (or close) when the slide open switch (or slide close switch) is pressed for less than 0.3 seconds.</ptxt>
</item>
<item>
<ptxt>The sliding roof stops as soon as the switch is released.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Manual tilt up and down</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>This function causes the sliding roof to tilt up (or tilt down) when the tilt up switch (or tilt down switch) is pressed for less than 0.3 seconds.</ptxt>
</item>
<item>
<ptxt>The sliding roof stops as soon as the switch is released.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Auto slide open and close</ptxt>
</entry>
<entry>
<ptxt>This function causes the sliding roof to be fully opened (or closed) when the slide open switch (or slide close switch) is pressed for 0.3 seconds or more.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Auto tilt up and down</ptxt>
</entry>
<entry>
<ptxt>This function causes the sliding roof to be fully tilted up (or tilted down) when the tilt up switch (or tilt down switch) is pressed for 0.3 seconds or more.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Jam protection function</ptxt>
</entry>
<entry>
<ptxt>The jam protection function automatically stops the sliding roof and moves it open halfway (or fully tilts it up) if a foreign object gets jammed in the sliding roof during close or tilt down operation.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Key-off operation</ptxt>
</entry>
<entry>
<ptxt>The key-off operation function makes it possible to operate the sliding roof for approximately 43 seconds after the ignition switch is turned off if the driver side door is not opened.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Wireless transmitter-linked operation</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>When the main body ECU (multiplex network body ECU) receives an unlock signal from the door control transmitter continuously for 3.0 seconds or more, the sliding roof ECU controls the sliding roof motor in accordance with the signal to slide open the sliding roof.</ptxt>
</item>
<item>
<ptxt>When the main body ECU (multiplex network body ECU) receives a lock signal from the door control transmitter continuously for 3.0 seconds or more, the sliding roof ECU controls the sliding roof motor in accordance with the signal to slide close the sliding roof.</ptxt>
</item>
<item>
<ptxt>When the main body ECU (multiplex network body ECU) receives an unlock signal from the door control transmitter continuously for 3.0 seconds or more, the sliding roof ECU controls the sliding roof motor in accordance with the signal to tilt up the sliding roof.</ptxt>
</item>
<item>
<ptxt>When the main body ECU (multiplex network body ECU) receives a lock signal from the door control transmitter continuously for 3.0 seconds or more, the sliding roof ECU controls the sliding roof motor in accordance with the signal to tilt down the sliding roof.</ptxt>
</item>
</list1>
<atten4>
<ptxt>This function is linked with the power window wireless transmitter-linked operation.</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Key-linked operation</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>When the key is not in the vehicle, the driver side door is unlocked, and the key in the driver side door is turned and held in the unlock direction for 2.0 seconds or more, the sliding roof ECU activates the sliding roof motor to slide open the sliding roof while the key is turned.</ptxt>
</item>
<item>
<ptxt>When the key is not in the vehicle, the driver side door is locked, and the key in the driver side door is turned and held in the lock direction for 2.0 seconds or more, the sliding roof ECU activates the sliding roof motor to slide close the sliding roof while the key is turned.</ptxt>
</item>
<item>
<ptxt>When the key is not in the vehicle, the driver side door is unlocked, and the key in the driver side door is turned and held in the unlock direction for 2.0 seconds or more, the sliding roof ECU activates the sliding roof motor to tilt up the sliding roof while the key is turned.</ptxt>
</item>
<item>
<ptxt>When the key is not in the vehicle, the driver side door is locked, and the key in the driver side door is turned and held in the lock direction for 2.0 seconds or more, the sliding roof ECU activates the sliding roof motor to tilt down the sliding roof while the key is turned.</ptxt>
</item>
</list1>
<atten4>
<ptxt>This function is linked with the power window key-linked operation.</ptxt>
</atten4>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Sliding roof open warning</ptxt>
</entry>
<entry>
<ptxt>When the ignition switch is turned from ON to off and the driver door is opened with the sliding roof open, the buzzer in the combination meter assembly sounds once.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>