<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97L_T00H9" variety="T00H9">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Hydraulic Brake Booster)</name>
<para id="RM0000035P4051X" category="C" type-id="803LI" name-id="BC3TN-72" from="201207" to="201210">
<dtccode>C1419</dtccode>
<dtcname>Acceleration Sensor Internal Circuit</dtcname>
<dtccode>C1435</dtccode>
<dtcname>Yaw Rate Sensor Internal Circuit</dtcname>
<subpara id="RM0000035P4051X_01" type-id="60" category="03" proc-id="RM23G0E___0000ABY00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU receives signals from the yaw rate and acceleration sensor via the CAN communication system.</ptxt>
<ptxt>The yaw rate and acceleration sensor has a built-in acceleration sensor and detects the vehicle condition using 2 circuits (GL1, GL2).</ptxt>
<ptxt>If there is trouble in the bus lines between the yaw rate and acceleration sensor and the CAN communication system, DTCs U0123 (malfunction in CAN communication with the yaw rate and acceleration sensor) and U0124 (malfunction in CAN communication with the acceleration sensor) are stored.</ptxt>
<ptxt>These DTCs are also stored when calibration has not been completed.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1419</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A data malfunction signal is received from the acceleration sensor.</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Yaw rate and acceleration sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C1435</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A data malfunction signal is received from the yaw rate and acceleration sensor.</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Yaw rate and acceleration sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000035P4051X_02" type-id="32" category="03" proc-id="RM23G0E___0000ABZ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C1232, C1243 and C1245 (See page <xref label="Seep01" href="RM000001EA301XX_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM0000035P4051X_03" type-id="51" category="05" proc-id="RM23G0E___0000AC000000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the yaw rate and acceleration sensor, perform calibration (See page <xref label="Seep01" href="RM00000452J00IX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>When DTC U0123, U0124 and/or U0126 is output together with DTC C1419 and/or C1435, inspect and repair the trouble areas indicated by DTC U0123, U0124 and/or U0126 first (See page <xref label="Seep02" href="RM000000YUO0J1X"/>).</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000035P4051X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000035P4051X_04_0001" proc-id="RM23G0E___0000AC100000">
<testtitle>REPLACE YAW RATE AND ACCELERATION SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the yaw rate and acceleration sensor (See page <xref label="Seep01" href="RM000000SS5051X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000035P4051X_04_0002" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000035P4051X_04_0002">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>