<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A1_T00JP" variety="T00JP">
<name>TOYOTA PARKING ASSIST-SENSOR SYSTEM (for 8 Sensor Type)</name>
<para id="RM000002R2O02YX" category="C" type-id="80401" name-id="PM3A1-03" from="201207" to="201210">
<dtccode>U0100</dtccode>
<dtcname>Lost Communication with ECM</dtcname>
<dtccode>U0155</dtccode>
<dtcname>Lost Communication with Instrument Panel Cluster Control Module (Combination Meter)</dtcname>
<subpara id="RM000002R2O02YX_01" type-id="60" category="03" proc-id="RM23G0E___0000CTR00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>These DTCs are stored when the clearance warning ECU*1 or parking assist ECU*2 cannot receive and recognize certain signals via the CAN communication system.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: w/o Parking Assist Monitor System and/or Side Monitor System</ptxt>
</item>
<item>
<ptxt>*2: w/ Parking Assist Monitor System and/or Side Monitor System</ptxt>
</item>
</list1>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>U0100</ptxt>
</entry>
<entry valign="middle">
<ptxt>A problem with the engine information signal from the ECM.</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>U0155</ptxt>
</entry>
<entry valign="middle">
<ptxt>A problem with the vehicle speed information signal from the combination meter assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication system</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When 2 or more DTCs starting with "U" are output simultaneously, inspect the connectors and wire harness of each ECU.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002R2O02YX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000002R2O02YX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002R2O02YX_05_0005" proc-id="RM23G0E___0000CTS00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000VKU048X"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM000000VKU048X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" align="left" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC (CAN communication system DTC) is output (for LHD with Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC (CAN communication system DTC) is output (for LHD without Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC (CAN communication system DTC) is output (for RHD with Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC (CAN communication system DTC) is output (for RHD without Entry and Start System)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002R2O02YX_05_0006" fin="true">A</down>
<right ref="RM000002R2O02YX_05_0008" fin="true">B</right>
<right ref="RM000002R2O02YX_05_0009" fin="true">C</right>
<right ref="RM000002R2O02YX_05_0010" fin="true">D</right>
<right ref="RM000002R2O02YX_05_0007" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000002R2O02YX_05_0006">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000000WIB0BQX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2O02YX_05_0008">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000000WIB0BRX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2O02YX_05_0009">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000000WIB0BSX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2O02YX_05_0010">
<testtitle>GO TO CAN COMMUNICATION SYSTEM<xref label="Seep01" href="RM000000WIB0BTX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002R2O02YX_05_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>