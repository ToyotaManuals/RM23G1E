<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B7_T00KV" variety="T00KV">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000000XUQ0E0X" category="J" type-id="800Q7" name-id="TD4YK-03" from="201210">
<dtccode/>
<dtcname>Front Passenger Side Door Entry Lock Function does not Operate</dtcname>
<subpara id="RM000000XUQ0E0X_01" type-id="60" category="03" proc-id="RM23G0E___0000ESV00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>If the front passenger side door entry lock function does not operate but the entry unlock function operates, the communication between the vehicle and electrical key transmitter is normal. The malfunctioning part may be an entry lock sensor circuit (front door outside handle assembly [for front passenger side] → certification ECU).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XUQ0E0X_04" type-id="32" category="03" proc-id="RM23G0E___0000ESX00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B243940E04" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XUQ0E0X_02" type-id="51" category="05" proc-id="RM23G0E___0000ESW00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>The entry and start system (for Entry Function) uses a multiplex communication system (LIN communication system) and the CAN communication system. Inspect the communication function by following How to Proceed with Troubleshooting (See page <xref label="Seep01" href="RM000000XU70CAX"/>). Troubleshoot the entry and start system (for Entry Function) after confirming that the communication systems are functioning properly.</ptxt>
</item>
<item>
<ptxt>When using the intelligent tester with the engine switch off to troubleshoot: Connect the intelligent tester to the DLC3 and turn a courtesy light switch on and off at 1.5-second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</item>
<item>
<ptxt>Check that there are no electrical key transmitters in the vehicle.</ptxt>
</item>
<item>
<ptxt>Before performing the inspection, check that DTC B1242 (wireless door lock control) is not output (See page <xref label="Seep02" href="RM000002BZF045X"/>).</ptxt>
</item>
<item>
<ptxt>When checking the entry lock operation multiple times, the lock operation may be limited to 2 consecutive operations depending on the settings. In order to perform the entry lock operation 3 or more times, an unlock operation must be performed once (any type of unlock operation is sufficient). However, only consecutive entry lock operations are limited. Using the wireless lock or other types of lock operations, it is possible to perform consecutive lock operations without this limitation.</ptxt>
</item>
<item>
<ptxt>Before replacing the certification ECU, refer to the Service Bulletin.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XUQ0E0X_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XUQ0E0X_05_0012" proc-id="RM23G0E___0000ESY00001">
<testtitle>CHECK POWER DOOR LOCK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the door control switch on the multiplex network master switch assembly is operated, check that the doors unlock and lock according to the switch operation (See page <xref label="Seep01" href="RM000002T6K05GX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door locks operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0E0X_05_0028" fin="false">OK</down>
<right ref="RM000000XUQ0E0X_05_0019" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0028" proc-id="RM23G0E___0000ET200001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR LOCK POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</test1>
<test1>
<ptxt>Read the Data List according to the display on the intelligent tester.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Door Lock Pos*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front RH side door lock position switch signal / UNLOCK or LOCK</ptxt>
</entry>
<entry>
<ptxt>UNLOCK: Front RH side door unlocked</ptxt>
<ptxt>LOCK: Front RH side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Door Lock Pos*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front LH side door lock position switch signal / UNLOCK or LOCK</ptxt>
</entry>
<entry>
<ptxt>UNLOCK: Front LH side door unlocked</ptxt>
<ptxt>LOCK: Front LH side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between LOCK and UNLOCK as shown in the chart above.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0E0X_05_0013" fin="false">OK</down>
<right ref="RM000000XUQ0E0X_05_0029" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0013" proc-id="RM23G0E___0000ESZ00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (LOCK SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, read the Data List (See page <xref label="Seep01" href="RM000000XUF08HX"/>).</ptxt>
<table pgwide="1">
<title>Entry&amp;Start</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P-Door Trigger Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front passenger side door lock sensor / ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON: Front passenger side door lock sensor touched</ptxt>
<ptxt>OFF: Front passenger side door lock sensor not touched</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>"ON" (lock sensor is touched) and "OFF" (lock sensor is not touched) appear on the screen.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0E0X_05_0025" fin="true">OK</down>
<right ref="RM000000XUQ0E0X_05_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0015" proc-id="RM23G0E___0000ET100001">
<testtitle>CHECK HARNESS AND CONNECTOR (FRONT DOOR OUTSIDE HANDLE - CERTIFICATION ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the N1*1 or O1*2 handle connector.</ptxt>
<ptxt>*1: for LHD</ptxt>
<ptxt>*2: for RHD</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G38 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>N1-1 (TRG+) - G38-19 (TSW2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-19 (TSW2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-15 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>O1-1 (TRG+) - G38-19 (TSW2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-19 (TSW2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-15 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0E0X_05_0014" fin="false">OK</down>
<right ref="RM000000XUQ0E0X_05_0021" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0014" proc-id="RM23G0E___0000ET000001">
<testtitle>CHECK FRONT DOOR OUTSIDE HANDLE ASSEMBLY (LOCK SENSOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="B224125E23" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Certification ECU)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G38-19 (TSW2) - G38-15 (E)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, all doors closed and lock sensor (for front passenger side) off → on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pulse generation → Below 2 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUQ0E0X_05_0025" fin="true">OK</down>
<right ref="RM000000XUQ0E0X_05_0027" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0019">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM0000011G908TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0025">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0021">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0027">
<testtitle>REPLACE FRONT DOOR OUTSIDE HANDLE ASSEMBLY (FOR FRONT PASSENGER SIDE)<xref label="Seep01" href="RM0000011QF0CPX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUQ0E0X_05_0029">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM (Proceed to Only Front Passenger Door Lock/Unlock Functions do not Operate)<xref label="Seep01" href="RM0000011GC05OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>