<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B8_T00KW" variety="T00KW">
<name>THEFT DETERRENT SYSTEM</name>
<para id="RM000000W12084X" category="J" type-id="3027N" name-id="TD4Z0-03" from="201210">
<dtccode/>
<dtcname>Security Indicator Light Circuit</dtcname>
<subpara id="RM000000W12084X_01" type-id="60" category="03" proc-id="RM23G0E___0000EXK00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>When the theft deterrent system is in the disarmed state, the security indicator light flashes continuously when the engine immobiliser system is set, and does not illuminate when the engine immobiliser system is not set.</ptxt>
<ptxt>When the theft deterrent system is in the armed state, the engine immobiliser system is automatically set and the security indicator light flashes continuously.</ptxt>
<ptxt>When the theft deterrent system is in the arming preparation state or alarm sounding state, the main body ECU causes the security indicator light to be illuminated.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000W12084X_02" type-id="32" category="03" proc-id="RM23G0E___0000EXL00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B236698E03" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W12084X_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000000W12084X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W12084X_04_0001" proc-id="RM23G0E___0000EXM00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SECURITY INDICATOR LIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Operate the intelligent tester according to the steps on the display and select Active Test (See page <xref label="Seep01" href="RM000000W0S02DX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Security Indicator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Security indicator light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>The test is possible when the following conditions are met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The key is in the cabin*1</ptxt>
</item>
<item>
<ptxt>The engine switch on (IG)*1</ptxt>
</item>
<item>
<ptxt>The key is in the ignition key cylinder*2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Entry and Start System</ptxt>
</item>
<item>
<ptxt>*2: w/o Entry and Start System</ptxt>
</item>
</list1>
<spec>
<title>OK</title>
<specitem>
<ptxt>Security indicator light can be turned on and off using the intelligent tester.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W12084X_04_0003" fin="true">OK</down>
<right ref="RM000000W12084X_04_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W12084X_04_0008" proc-id="RM23G0E___0000EXO00001">
<testtitle>INSPECT SECURITY INDICATOR LIGHT ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the security indicator light (See page <xref label="Seep01" href="RM00000478I00RX"/>).</ptxt>
<figure>
<graphic graphicname="B238374E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Apply battery voltage between the terminals of the security indicator light assembly and check that the security indicator light illuminates.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Battery positive (+) → 2 (LP)</ptxt>
<ptxt>Battery negative (-) → 5 (E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Security indicator light illuminates</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W12084X_04_0002" fin="false">OK</down>
<right ref="RM000000W12084X_04_0014" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W12084X_04_0002" proc-id="RM23G0E___0000EXN00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SECURITY INDICATOR LIGHT - MAIN BODY ECU AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H23 light connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the main body ECU (See page <xref label="Seep01" href="RM000003WBO03DX"/>).</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H23-2 (LP) - A-23 (IND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H23-5 (E) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H23-2 (LP) or A-23 (IND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W12084X_04_0013" fin="true">OK</down>
<right ref="RM000000W12084X_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W12084X_04_0003">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002TGC01FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W12084X_04_0013">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO03DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W12084X_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W12084X_04_0014">
<testtitle>REPLACE SECURITY INDICATOR LIGHT ASSEMBLY<xref label="Seep01" href="RM00000478I00RX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>