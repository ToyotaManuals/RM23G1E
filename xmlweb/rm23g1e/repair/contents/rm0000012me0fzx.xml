<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WA_T005Y" variety="T005Y">
<name>ECD SYSTEM (w/o DPF)</name>
<para id="RM0000012ME0FZX" category="C" type-id="302I1" name-id="ESMGC-24" from="201207" to="201210">
<dtccode>P0500</dtccode>
<dtcname>Vehicle Speed Sensor "A"</dtcname>
<subpara id="RM0000012ME0FZX_01" type-id="60" category="03" proc-id="RM23G0E___00001HB00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Vehicles which are equipped with a manual transaxle detect the vehicle speed using the speed sensor. The speed sensor transmits a 4-pulse signal for every revolution of the rotor shaft, which is rotated by the transaxle output shaft via the driven gear. The 4-pulse signal is converted into a more precise rectangular waveform by the waveform shaping circuit inside the combination meter. The signal is then transmitted to the ECM. The ECM determines the vehicle speed based on the frequency of the pulse signal.</ptxt>
<figure>
<graphic graphicname="A115899E12" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>P0500</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Drive the vehicle at 10 km/h (6.3 mph) or more</ptxt>
</entry>
<entry>
<ptxt>Conditions (a), (b) and (c) are met for 8 seconds or more (2 trip detection logic):</ptxt>
<ptxt>(a) Engine coolant temperature is higher than 70°C (158°F).</ptxt>
<ptxt>(b) Injection volume is 19 mm<sup>3</sup>/st or more.</ptxt>
<ptxt>(c) No speed signal is input to the ECM.</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in speedometer circuit</ptxt>
</item>
<item>
<ptxt>Speedometer assembly </ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Drive the vehicle at 10 km/h (6.3 mph) or more</ptxt>
</entry>
<entry>
<ptxt>Conditions (a) and (b) are met for 5 seconds or more (2 trip detection logic):</ptxt>
<ptxt>(a) The fuel-cut operation is being performed.</ptxt>
<ptxt>(b) No speed signal is input to the ECM.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0500</ptxt>
</entry>
<entry>
<ptxt>Vehicle Speed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000012ME0FZX_06" type-id="32" category="03" proc-id="RM23G0E___00001HC00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A160610E13" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000012ME0FZX_07" type-id="51" category="05" proc-id="RM23G0E___00001HD00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK061X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05KX"/>).</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000012ME0FZX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012ME0FZX_08_0001" proc-id="RM23G0E___00001HE00000">
<testtitle>CHECK OPERATION OF SPEEDOMETER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Drive the vehicle and check whether the operation of the speedometer in the combination meter assembly is normal.</ptxt>
<atten4>
<ptxt>The vehicle speed sensor is operating normally if the speedometer reading is normal.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The vehicle speed sensor is operating normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000012ME0FZX_08_0002" fin="false">OK</down>
<right ref="RM0000012ME0FZX_08_0006" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0002" proc-id="RM23G0E___00001HF00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (VEHICLE SPEED)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON and start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data / Vehicle Speed.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Read the value displayed on the tester.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vehicle speed displayed on tester and speedometer display are equal.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<right ref="RM0000012ME0FZX_08_0007" fin="false">OK</right>
<right ref="RM0000012ME0FZX_08_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0003" proc-id="RM23G0E___00001HG00000">
<testtitle>CHECK HARNESS AND CONNECTOR</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A127854E29" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Disconnect the combination meter assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COLSPEC1" colwidth="1.36in"/>
<colspec colname="COLSPEC0" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G6-32 (+S) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to combination meter assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the combination meter assembly connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012ME0FZX_08_0004" fin="false">OK</down>
<right ref="RM0000012ME0FZX_08_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0004" proc-id="RM23G0E___00001HH00000">
<testtitle>CHECK COMBINATION METER ASSEMBLY (SPD SIGNAL WAVEFORM)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A205820E06" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Move the shift lever to neutral.</ptxt>
</test1>
<test1>
<ptxt>Jack up the vehicle.</ptxt>
</test1>
<test1>
<ptxt>Check the voltage between the terminal of the combination meter assembly and the body ground while a wheel is turned slowly.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G6-32 (+S) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Voltage generated intermittently</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(combination meter assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>The output voltage should fluctuate up and down, similarly to the diagram, when the wheel is turned slowly.</ptxt>
</atten4>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<right ref="RM0000012ME0FZX_08_0010" fin="false">A</right>
<right ref="RM0000012ME0FZX_08_0008" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0005" proc-id="RM23G0E___00001HI00000">
<testtitle>CHECK HARNESS AND CONNECTOR (COMBINATION METER ASSEMBLY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the combination meter assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G6-32 (+S) - G58-30 (SPD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G6-32 (+S) or G58-30 (SPD) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the combination meter assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012ME0FZX_08_0010" fin="false">OK</down>
<right ref="RM0000012ME0FZX_08_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0010" proc-id="RM23G0E___00001HN00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM0000012ME0FZX_08_0007" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0006" proc-id="RM23G0E___00001HJ00000">
<testtitle>CHECK SPEEDOMETER CIRCUIT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the speedometer circuit (See page <xref label="Seep01" href="RM0000012GD083X"/>).</ptxt>
<atten4>
<ptxt>Inspect the brake actuator assembly and speed sensor.</ptxt>
</atten4>
</test1>
</content6>
<res>
<right ref="RM0000012ME0FZX_08_0007" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0008" proc-id="RM23G0E___00001HL00000">
<testtitle>REPLACE COMBINATION METER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the combination meter assembly (See page <xref label="Seep01" href="RM000003QKY00MX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM0000012ME0FZX_08_0007" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0009" proc-id="RM23G0E___00001HM00000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012ME0FZX_08_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0007" proc-id="RM23G0E___00001HK00000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z6X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at 10 km/h (6.3 mph) or more.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the pending DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000012ME0FZX_08_0012" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000012ME0FZX_08_0012">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>