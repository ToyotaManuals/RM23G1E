<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94H_T00E5" variety="T00E5">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1KD-FTV)</name>
<para id="RM000000W8G0DQX" category="C" type-id="302N8" name-id="AT61U-03" from="201207" to="201210">
<dtccode>P0778</dtccode>
<dtcname>Pressure Control Solenoid "B" Electrical (Shift Solenoid Valve SL2)</dtcname>
<subpara id="RM000000W8G0DQX_01" type-id="60" category="03" proc-id="RM23G0E___000085S00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Shifting from 1st to 5th is performed in combination with the ON and OFF operation of shift solenoid valves SL1, SL2, S1, S2 and SR, which are controlled by the TCM. If an open or short circuit occurs in one of the shift solenoid valves, the TCM controls the remaining normal shift solenoid valves to allow the vehicle to be operated smoothly (See page <xref label="Seep01" href="RM000000O8L0MZX"/>).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0778</ptxt>
</entry>
<entry valign="middle">
<ptxt>The TCM checks for an open or short in the shift solenoid valve SL2 circuit while driving and shifting gears (1-trip detection logic).</ptxt>
<ptxt>Output signal duty ratio equals 100%.</ptxt>
<atten4>
<ptxt>SL2 output signal duty is less than 100% under normal conditions.</ptxt>
</atten4>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in shift solenoid valve SL2 circuit</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SL2</ptxt>
</item>
<item>
<ptxt>TCM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W8G0DQX_02" type-id="64" category="03" proc-id="RM23G0E___000085T00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates an open or short in the shift solenoid valve SL2 circuit. The TCM commands gear shifts by turning the shift solenoid valves ON/OFF. When there is an open or short circuit in any shift solenoid valve circuit, the TCM detects the problem, illuminates the MIL and stores the DTC. Then the TCM performs the fail-safe function and turns the other normal shift solenoid valves ON/OFF. In the case of an open or short circuit, the TCM stops sending current to the circuit.</ptxt>
<ptxt>While driving and shifting gears, if the TCM detects an open or short in the shift solenoid valve SL2 circuit, the TCM determines there is a malfunction (See page <xref label="Seep01" href="RM000000O8L0MZX"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000W8G0DQX_06" type-id="32" category="03" proc-id="RM23G0E___000085U00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C219217E02" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000W8G0DQX_07" type-id="51" category="05" proc-id="RM23G0E___000085V00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Shift solenoid valve SL2 is turned ON/OFF normally when the shift lever is in D.</ptxt>
<table pgwide="1">
<tgroup cols="6">
<colspec colname="COL1" align="left" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.13in"/>
<colspec colname="COL3" colwidth="1.13in"/>
<colspec colname="COL4" colwidth="1.13in"/>
<colspec colname="COL5" colwidth="1.13in"/>
<colspec colname="COL6" colwidth="1.14in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>TCM gear shift command</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve SL2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM000000W8G0DQX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W8G0DQX_08_0001" proc-id="RM23G0E___000085W00000">
<testtitle>INSPECT TRANSMISSION WIRE (SHIFT SOLENOID VALVE SL2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the C30 transmission wire connector.</ptxt>
<figure>
<graphic graphicname="C214326E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>11 (SL2+) - 3 (SL2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>11 (SL2+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>3 (SL2-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Transmission Wire)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W8G0DQX_08_0002" fin="false">OK</down>
<right ref="RM000000W8G0DQX_08_0008" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8G0DQX_08_0002" proc-id="RM23G0E___000085X00000">
<testtitle>CHECK HARNESS AND CONNECTOR (TRANSMISSION WIRE - TCM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G69 TCM connector.</ptxt>
<figure>
<graphic graphicname="C212047E05" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="left">
<ptxt>G69-7 (SL2+) - G69-6 (SL2-)</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>G69-7 (SL2+) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="left">
<ptxt>G69-6 (SL2-) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to TCM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W8G0DQX_08_0006" fin="true">OK</down>
<right ref="RM000000W8G0DQX_08_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8G0DQX_08_0008" proc-id="RM23G0E___000083L00000">
<testtitle>INSPECT SHIFT SOLENOID VALVE SL2
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove shift solenoid valve SL2.</ptxt>
</test1>
<figure>
<graphic graphicname="C197715E09" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) with a 21 W bulb → Terminal 1</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Terminal 2</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes an operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve SL2)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6><res>
<down ref="RM000000W8G0DQX_08_0004" fin="true">OK</down>
<right ref="RM000000W8G0DQX_08_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8G0DQX_08_0004">
<testtitle>REPAIR OR REPLACE TRANSMISSION WIRE<xref label="Seep01" href="RM0000013C1049X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8G0DQX_08_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000W8G0DQX_08_0006">
<testtitle>REPLACE TCM<xref label="Seep01" href="RM0000048F3006X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8G0DQX_08_0007">
<testtitle>REPLACE SHIFT SOLENOID VALVE SL2<xref label="Seep01" href="RM0000013FG02LX_01_0004"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>