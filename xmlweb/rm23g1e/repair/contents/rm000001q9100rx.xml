<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12033_S001J" variety="S001J">
<name>BRAKE (REAR)</name>
<ttl id="12033_S001J_7B98B_T00HZ" variety="T00HZ">
<name>REAR BRAKE</name>
<para id="RM000001Q9100RX" category="G" type-id="3001K" name-id="RB0A6-01" from="201207">
<name>INSPECTION</name>
<subpara id="RM000001Q9100RX_01" type-id="01" category="01">
<s-1 id="RM000001Q9100RX_01_0009" proc-id="RM23G0E___0000B1G00000">
<ptxt>CHECK PAD LINING THICKNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a ruler, measure the pad lining thickness.</ptxt>
<figure>
<graphic graphicname="F043130" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thickness</title>
<specitem>
<ptxt>10.0 mm (0.394 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum thickness</title>
<specitem>
<ptxt>1.0 mm (0.0394 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the pad lining thickness is less than the minimum, replace the pad.</ptxt>
<atten4>
<ptxt>Be sure to check the wear on the rear disc after replacing the brake pad with a new one.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q9100RX_01_0008" proc-id="RM23G0E___0000B1F00000">
<ptxt>CHECK DISC THICKNESS</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using a micrometer, measure the disc thickness.</ptxt>
<figure>
<graphic graphicname="F043131" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard thickness</title>
<specitem>
<ptxt>18.0 mm (0.709 in.)</ptxt>
</specitem>
</spec>
<spec>
<title>Minimum thickness</title>
<specitem>
<ptxt>16.0 mm (0.630 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the disc thickness is less than the minimum, replace the disc.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q9100RX_01_0005" proc-id="RM23G0E___0000B1E00000">
<ptxt>CHECK DISC RUNOUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the disc with the 6 hub nuts.</ptxt>
<figure>
<graphic graphicname="C218754" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>112</t-value1>
<t-value2>1137</t-value2>
<t-value4>82</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Using a dial indicator, measure the disc runout at a position 10 mm (0.394 in.) from the outside edge.</ptxt>
<spec>
<title>Maximum disc runout</title>
<specitem>
<ptxt>0.20 mm (0.00787 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the disc runout is more than the maximum, check the rear axle shaft (See page <xref label="Seep01" href="RM0000016XC00WX_01_0001"/>). If the rear axle shaft is normal, adjust the disc runout or grind the disc using an on-vehicle brake lathe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000001Q9100RX_01_0010" proc-id="RM23G0E___0000B1H00000">
<ptxt>ADJUST DISC RUNOUT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 bolts and rear disc brake cylinder mounting from the backing plate.</ptxt>
</s2>
<s2>
<ptxt>Remove the hub nuts and disc. Rotate the disc 1/5 of a turn from its original position on the hub and install the disc with the hub nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>112</t-value1>
<t-value2>1137</t-value2>
<t-value4>82</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Measure the disc runout. Make a note of the runout and the disc position on the hub.</ptxt>
</s2>
<s2>
<ptxt>Repeat the 2 previous steps until the disc has been installed on the 3 remaining hub positions. If the minimum runout recorded above is less than 0.20 mm (0.00787 in.), install the disc in that position. If the minimum runout recorded above is more than 0.20 mm (0.00787 in.), replace the disc and repeat the "Check Disc Runout" procedure.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>