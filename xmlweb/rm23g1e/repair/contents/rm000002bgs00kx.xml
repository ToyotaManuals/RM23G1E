<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001B" variety="S001B">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001B_7B96R_T00GF" variety="T00GF">
<name>HEIGHT CONTROL SENSOR</name>
<para id="RM000002BGS00KX" category="A" type-id="30014" name-id="SC1Z3-03" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000002BGS00KX_01" type-id="01" category="01">
<s-1 id="RM000002BGS00KX_01_0001" proc-id="RM23G0E___00009R300001">
<ptxt>INSTALL REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the height control sensor with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="C212873E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matchmark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Replace the rear height control sensor sub-assembly with a new one if it is dropped.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Align the matchmarks on the link and bracket.</ptxt>
</s2>
<s2>
<ptxt>Connect the sensor link with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>5.6</t-value1>
<t-value2>57</t-value2>
<t-value3>50</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector, and then attach the connector.</ptxt>
</s2>
<s2>
<ptxt>Attach the clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002BGS00KX_01_0007" proc-id="RM23G0E___00009R400001">
<ptxt>INSTALL REAR HEIGHT CONTROL SENSOR SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM000002BGS00KX_01_0008" proc-id="RM23G0E___00009PJ00000">
<ptxt>INSTALL REAR WHEEL
</ptxt>
<content1 releasenbr="1">
<torque>
<torqueitem>
<t-value1>112</t-value1>
<t-value2>1142</t-value2>
<t-value4>83</t-value4>
</torqueitem>
</torque>
</content1></s-1>
<s-1 id="RM000002BGS00KX_01_0009" proc-id="RM23G0E___00009R500001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002BGS00KX_01_0006" proc-id="RM23G0E___00009P000000">
<ptxt>ADJUST VEHICLE HEIGHT
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>While adjusting vehicle height, do not put anyone or anything on or in the vehicle because doing so will affect vehicle height.</ptxt>
</item>
<item>
<ptxt>Height control sensors are installed on both the left and right sides. However, each side cannot be adjusted individually, as the left and right sides are not controlled independently.</ptxt>
</item>
<item>
<ptxt>Even if the height control sensor of only one side is adjusted, the vehicle height of both the left and right sides will change.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Suspend vehicle height control by pressing the height control switch.</ptxt>
</s2>
<s2>
<ptxt>Put the vehicle on a level surface.</ptxt>
</s2>
<s2>
<ptxt>Measure the vehicle height (C - D measurement) on the right side and left side.</ptxt>
<figure>
<graphic graphicname="C125412E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard vehicle height</title>
<specitem>
<ptxt>(See page <xref label="Seep01" href="RM000001V4301VX_01_0018"/>)</ptxt>
</specitem>
</spec>
<spec>
<title>Difference between right and left side</title>
<specitem>
<ptxt>10 mm (0.394 in.) or less</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>If the actual vehicle height differs from the vehicle height (C - D measurement), adjust it by jacking up the frame, etc. (Procedure "A").</ptxt>
</s2>
<s2>
<ptxt>If the procedure "A" differs from the vehicle height (C - D measurement), adjust it by following the procedures below.</ptxt>
<figure>
<graphic graphicname="C214841" width="2.775699831in" height="2.775699831in"/>
</figure>
<s3>
<ptxt>Loosen the nut.</ptxt>
</s3>
<s3>
<ptxt>Move the height control sensor link up and down along the slotted hole of the bracket.</ptxt>
</s3>
<s3>
<ptxt>Adjust the vehicle height to the vehicle height (C - D measurement) while checking the value on the intelligent tester or voltmeter.</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>2.5 V</ptxt>
</specitem>
</spec>
</s3>
<s3>
<ptxt>Tighten the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>5.6</t-value1>
<t-value2>57</t-value2>
<t-value3>50</t-value3>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>If the vehicle height cannot be adjusted by performing procedure "A", adjust it again by following the procedures below.</ptxt>
<s3>
<ptxt>Loosen the 2 lock nuts of the height control sensor link.</ptxt>
<figure>
<graphic graphicname="C216433E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock Nut</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Adjust the vehicle height to the vehicle height (C - D measurement) by turning the link while checking the value on the intelligent tester or the voltmeter.</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>2.5 V</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The vehicle height will be changed by approximately 3.0 mm (0.118 in.) when changing the link by approximately 2 mm (0.0787 in.).</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Tighten the 2 lock nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>5.4</t-value1>
<t-value2>55</t-value2>
<t-value3>48</t-value3>
</torqueitem>
</torque>
</s3>
</s2>
<s2>
<ptxt>Check that the lengths of the screw parts, labeled "A" in the illustration, are within the standard values.</ptxt>
<spec>
<title>Standard Length</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>LH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6.0 to 12.5 mm (0.24 to 0.49 in.)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>RH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>6.5 to 15.0 mm (0.26 to 0.59 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</s2>
<s2>
<ptxt>Change the vehicle height (from the normal position to the high position, and from the high position to the normal position).</ptxt>
</s2>
<s2>
<ptxt>Measure the vehicle height (C - D measurement) on the right and left side (Procedure "D").</ptxt>
</s2>
<s2>
<ptxt>Check if the vehicle height (C - D measurement) is within the specified range.</ptxt>
<atten4>
<ptxt>If the values are outside the standard range, perform the procedures from procedure "A" to procedure "D" again.</ptxt>
</atten4>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>