<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12064_S0029" variety="S0029">
<name>WINDOW / GLASS</name>
<ttl id="12064_S0029_7B9FN_T00PB" variety="T00PB">
<name>WINDOW DEFOGGER SYSTEM</name>
<para id="RM000002ZCB038X" category="D" type-id="303F2" name-id="WS1J7-20" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000002ZCB038X_z0" proc-id="RM23G0E___0000ID000000">
<content5 releasenbr="1">
<step1>
<ptxt>GENERAL</ptxt>
<ptxt>The thin heater wires of the defogger system are attached to the rear window and defog the rear window surface quickly. The indicator light illuminates while the system is operating. The system automatically turns off approximately 15 minutes after operation starts. For vehicles with the automatic air conditioning system, there is a timer extension function which can operate the window defogger system for up to an additional 255 minutes.</ptxt>
<atten4>
<ptxt>The timer extension function operates when the following conditions are met:</ptxt>
</atten4>
<list1 type="unordered">
<item>
<ptxt>Ambient Temperature: -3°C (26.6°F) or less</ptxt>
</item>
<item>
<ptxt>Vehicle Speed: 31.1 mph (50 km/h) or more</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>FUNCTION OF MAIN COMPONENT</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Integration Control and Panel Assembly</ptxt>
</entry>
<entry>
<ptxt>Detects defogger switch operation and transmits signals to the air conditioning amplifier through the LIN communication line.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Engine Room Relay Block, junction block (DEF relay)</ptxt>
</entry>
<entry>
<ptxt>Receives rear window defogger activation request signals from the air conditioning amplifier and supplies power to the rear window defogger.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Back Door Glass (Window defogger wire)</ptxt>
</entry>
<entry>
<ptxt>Receives power from the defogger relay and heats the defogger wire.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>SYSTEM FUNCTION</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Function</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Defog rear window surface</ptxt>
</entry>
<entry>
<list1 type="unordered">
<item>
<ptxt>The defogger relay receives rear window defogger activation request signals from the air conditioning amplifier and defogs the rear window surface.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Power received from the defogger relay heats the defogger wire.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>