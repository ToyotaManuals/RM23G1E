<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000F" variety="S000F">
<name>2TR-FE FUEL</name>
<ttl id="12008_S000F_7B8ZC_T0090" variety="T0090">
<name>FUEL TANK (for 3 Door)</name>
<para id="RM0000045E6005X" category="A" type-id="80001" name-id="FU8G3-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000045E6005X_01" type-id="01" category="01">
<s-1 id="RM0000045E6005X_01_0001" proc-id="RM23G0E___00005VD00001">
<ptxt>DISCHARGE FUEL SYSTEM PRESSURE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Discharge the fuel system pressure (See page <xref label="Seep01" href="RM0000028RU03RX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0002" proc-id="RM23G0E___00005VE00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0022" proc-id="RM23G0E___00005NQ00001">
<ptxt>REMOVE REAR FLOOR MAT SUPPORT PLATE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240526" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 5 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 6 claws and remove the rear floor mat rear support plate.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000045E6005X_01_0003" proc-id="RM23G0E___00005VF00001">
<ptxt>REMOVE REAR FLOOR SERVICE HOLE COVER</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Fold back the floor mat so that the rear floor service hole cover can be removed.</ptxt>
</atten4>
<s2>
<ptxt>Remove the 3 screws and rear floor service hole cover.</ptxt>
<figure>
<graphic graphicname="A220855" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the fuel sender gauge connector and fuel pump connector.</ptxt>
<figure>
<graphic graphicname="A220856" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0004" proc-id="RM23G0E___00005VG00001">
<ptxt>REMOVE NO. 1 FUEL TANK PROTECTOR SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 bolts and No. 1 fuel tank protector.</ptxt>
<figure>
<graphic graphicname="A220857" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0019" proc-id="RM23G0E___00005VU00001">
<ptxt>DRAIN FUEL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel tank drain plug and gasket and drain the fuel.</ptxt>
<figure>
<graphic graphicname="A220858" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install a new gasket and the fuel tank drain plug.</ptxt>
<torque>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0005" proc-id="RM23G0E___00005VH00001">
<ptxt>DISCONNECT FUEL MAIN TUBE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel main tube (See page <xref label="Seep01" href="RM0000028RU03RX"/>).</ptxt>
<figure>
<graphic graphicname="A223093" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0006" proc-id="RM23G0E___00005VI00001">
<ptxt>DISCONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel evaporation hose and fuel hose.</ptxt>
<figure>
<graphic graphicname="A223277" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0007" proc-id="RM23G0E___00005VJ00001">
<ptxt>DISCONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel tank to filler pipe hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A220860" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0008" proc-id="RM23G0E___00005VK00001">
<ptxt>DISCONNECT FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the fuel tank breather hose from the filler pipe.</ptxt>
<figure>
<graphic graphicname="A223278" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0009" proc-id="RM23G0E___00005VL00001">
<ptxt>REMOVE FUEL TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set a transmission jack underneath the fuel tank.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts, 2 clips and 2 pins and disconnect the 2 fuel tank bands.</ptxt>
<figure>
<graphic graphicname="A220861" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0021" proc-id="RM23G0E___00005VV00001">
<ptxt>REMOVE FUEL SUCTION TUBE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 7 screws, detach the 2 clamps and remove the fuel suction tube.</ptxt>
<figure>
<graphic graphicname="A223095" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel suction tube.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000045E6005X_01_0011" proc-id="RM23G0E___00005VM00001">
<ptxt>REMOVE NO. 1 FUEL EVAPORATION TUBE SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 screws, detach the clamp and remove the No. 1 fuel evaporation tube.</ptxt>
<figure>
<graphic graphicname="A223096" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the No. 1 fuel evaporation tube.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0012" proc-id="RM23G0E___00005VN00001">
<ptxt>REMOVE FUEL CUT OFF VALVE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel cut off valve from the fuel tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0013" proc-id="RM23G0E___00005VO00001">
<ptxt>REMOVE FUEL SENDER GAUGE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 5 screws and fuel sender gauge.</ptxt>
<figure>
<graphic graphicname="A220867" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Be careful not to bend the arm of the fuel sender gauge.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel sender gauge.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0014" proc-id="RM23G0E___00005VP00001">
<ptxt>REMOVE FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel tank to filler pipe hose from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A220868" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0015" proc-id="RM23G0E___00005VQ00001">
<ptxt>REMOVE FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the fuel tank breather hose from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A223279" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0016" proc-id="RM23G0E___00005VR00001">
<ptxt>REMOVE FUEL TANK FILLER PIPE LOWER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 8 screws and fuel tank filler pipe lower.</ptxt>
<figure>
<graphic graphicname="A223091" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the gasket from the fuel tank filler pipe lower.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0017" proc-id="RM23G0E___00005VS00001">
<ptxt>REMOVE FUEL TUBE GROMMET</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the clamp and remove the fuel tube grommet.</ptxt>
<figure>
<graphic graphicname="A223094" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E6005X_01_0018" proc-id="RM23G0E___00005VT00001">
<ptxt>REMOVE NO. 1 FUEL TANK CUSHION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 No. 1 fuel tank cushions from the fuel tank.</ptxt>
<figure>
<graphic graphicname="A223092" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>