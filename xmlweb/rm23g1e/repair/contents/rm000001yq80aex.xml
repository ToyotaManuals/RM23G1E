<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A2_T00JQ" variety="T00JQ">
<name>PARKING ASSIST MONITOR SYSTEM</name>
<para id="RM000001YQ80AEX" category="J" type-id="804E8" name-id="PM6PK-01" from="201210">
<dtccode/>
<dtcname>Image from Camera for Parking Assist Monitor is Abnormal</dtcname>
<subpara id="RM000001YQ80AEX_01" type-id="60" category="03" proc-id="RM23G0E___0000CWG00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The display signal of the rear television camera assembly transmit to the parking assist ECU.</ptxt>
</content5>
</subpara>
<subpara id="RM000001YQ80AEX_02" type-id="32" category="03" proc-id="RM23G0E___0000CWH00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E199659E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001YQ80AEX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM000001YQ80AEX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001YQ80AEX_04_0019" proc-id="RM23G0E___0000CUQ00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARKING ASSIST ECU - REAR TELEVISION CAMERA)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
<figure>
<graphic graphicname="E185668E02" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I3-7 (IG) - I3-3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Parking Assist ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.48in"/>
<colspec colname="COL2" colwidth="1.65in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001YQ80AEX_04_0020" fin="false">OK</down>
<right ref="RM000001YQ80AEX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001YQ80AEX_04_0020" proc-id="RM23G0E___0000CV300001">
<testtitle>CHECK PARKING ASSIST ECU
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I2 parking assist ECU connector.</ptxt>
<figure>
<graphic graphicname="E228800E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistancee</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I2-11 (CV-) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>I2-13 (CGND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Parking Assist ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001YQ80AEX_04_0021" fin="false">A</down>
<right ref="RM000001YQ80AEX_04_0015" fin="true">B</right>
<right ref="RM000001YQ80AEX_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001YQ80AEX_04_0021" proc-id="RM23G0E___0000CV000001">
<testtitle>CHECK PARKING ASSIST ECU
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the X1*1 or v1*2 rear television camera assembly connector.</ptxt>
<figure>
<graphic graphicname="E227664E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<subtitle>w/ Tire Carrier</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>X1-4 (CB+) - X1-3 (CGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.5 to 7.05 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>w/o Tire Carrier</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>v1-4 (CB+) - v1-3 (CGND)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.5 to 7.05 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/ Tire Carrier</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>w/o Tire Carrier</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Rear Television Camera Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001YQ80AEX_04_0022" fin="false">A</down>
<right ref="RM000001YQ80AEX_04_0015" fin="true">B</right>
<right ref="RM000001YQ80AEX_04_0007" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000001YQ80AEX_04_0022" proc-id="RM23G0E___0000CV100001">
<testtitle>CHECK REAR TELEVISION CAMERA ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<figure>
<graphic graphicname="E167080E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COLSPEC1" align="left" colwidth="0.71in"/>
<colspec colname="COLSPEC0" align="left" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(Parking Assist ECU)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Measurement Condition</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>I2-12 (CV+) - I2-11 (CV-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.2 V/DIV., 50 μs/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Ignition switch ON, shift lever in R*1</ptxt>
</item>
<item>
<ptxt>Ignition switch ON, shift lever in R, screen blacked out by covering camera lens*2</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is as shown in illustration.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (w/ Tire Carrier)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG (w/o Tire Carrier)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6><res>
<down ref="RM000001YQ80AEX_04_0015" fin="true">A</down>
<right ref="RM000001YQ80AEX_04_0007" fin="true">B</right>
<right ref="RM000001YQ80AEX_04_0016" fin="true">C</right>
<right ref="RM000001YQ80AEX_04_0017" fin="true">D</right>
</res>
</testgrp>
<testgrp id="RM000001YQ80AEX_04_0016">
<testtitle>REPLACE REAR TELEVISION CAMERA ASSEMBLY<xref label="Seep01" href="RM00000470R004X"/>
</testtitle>
</testgrp>
<testgrp id="RM000001YQ80AEX_04_0017">
<testtitle>REPLACE REAR TELEVISION CAMERA ASSEMBLY<xref label="Seep01" href="RM00000470U00CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001YQ80AEX_04_0015">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM00000466X01EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001YQ80AEX_04_0007">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM00000470500GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001YQ80AEX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>