<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004HM500OX" category="C" type-id="805AS" name-id="ESTE9-07" from="201210">
<dtccode>P0524</dtccode>
<dtcname>Engine Oil Pressure Too Low</dtcname>
<subpara id="RM000004HM500OX_05" type-id="60" category="03" proc-id="RM23G0E___00002X400001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The oil pressure control system reduces and increases engine oil pressure using the oil pressure switching valve in the timing gear case assembly. When the engine speed reaches 3000 rpm while racing the engine after warming it up, the oil pressure switching valve operates and the engine oil pressure system changes from low pressure control mode to high pressure control mode. When the engine speed drops to 2800 rpm, the engine oil pressure system changes from high pressure control mode to low pressure control mode. The oil pressure sender gauge assembly detects the engine oil pressure.</ptxt>
<figure>
<graphic graphicname="A246647E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>P0524</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>After warming up the engine, race the engine at 3000 rpm or higher.*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Conditions (a) and (b) continue for 5 seconds or more (1 trip detection logic):</ptxt>
<ptxt>(a) 0.3 seconds have elapsed after sending a command to change from low oil pressure control mode to high oil pressure control mode.</ptxt>
<ptxt>(b) The oil pressure control system does not change to high oil pressure control mode.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in oil pressure sender gauge assembly circuit</ptxt>
</item>
<item>
<ptxt>Oil pressure switching valve relay</ptxt>
</item>
<item>
<ptxt>Oil pressure sender gauge assembly</ptxt>
</item>
<item>
<ptxt>Oil pressure switching valve assembly</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Oil pump assembly</ptxt>
</item>
<item>
<ptxt>Engine oil amount insufficient</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*: Using the intelligent tester, the oil pressure control system can be inspected by performing Engine Oil Pressure Control Check. Enter the following menus: Powertrain / Engine and ECT/ Utility / Engine Oil Pressure Control Check.</ptxt>
<atten4>
<ptxt>Checking the state of the oil level warning light and oil pressure warning light will help in understanding why this DTC was stored (See page <xref label="Seep01" href="RM000002MQJ04SX_01_0011"/>).</ptxt>
</atten4>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0524</ptxt>
</entry>
<entry>
<ptxt>Engine Oil Pressure</ptxt>
<ptxt>Eng Oil Press Switch Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000004HM500OX_02" type-id="32" category="03" proc-id="RM23G0E___00002WM00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P0522 (See page <xref label="Seep01" href="RM000004HM700JX_02"/>)</ptxt>
</content5>
</subpara>
<subpara id="RM000004HM500OX_03" type-id="51" category="05" proc-id="RM23G0E___00002WN00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07MX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See Page <xref label="Seep04" href="RM0000012XK07MX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</item>
<item>
<ptxt>An output DTC does not necessarily indicate that there is a malfunction. DTCs may have been stored due to an insufficient engine oil amount even though the oil level warning light is currently not illuminated, such as when engine oil is added by the customer.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000004HM500OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004HM500OX_04_0001" proc-id="RM23G0E___00002WO00001">
<testtitle>CHECK ENGINE OIL LEVEL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the engine oil level (See page <xref label="Seep01" href="RM000002MQJ04SX_01_0001"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the state of the oil level warning light.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Engine oil level is low and oil level warning light is illuminated</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Engine oil level is within the specified range and oil level warning light is not illuminated</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the oil level warning light is not illuminated even though the engine oil level is lower than the Low mark, or the oil level warning light is illuminated even though the engine oil level is within the specified range, inspect the oil level sensor and related parts for malfunctions.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0002" fin="false">A</down>
<right ref="RM000004HM500OX_04_0004" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0002" proc-id="RM23G0E___00002WP00001">
<testtitle>CHECK ENGINE OIL LEAKS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for engine oil leaks.</ptxt>
<spec>
<title>Result</title>
<specitem>
<ptxt>If there are engine oil leaks, perform repairs.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0003" proc-id="RM23G0E___00002WQ00001">
<testtitle>ADD ENGINE OIL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Add engine oil.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0004" proc-id="RM23G0E___00002WR00001">
<testtitle>INSPECT OIL PUMP ASSEMBLY (OIL PRESSURE SWITCHING VALVE OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Engine Oil Pressure Control Check (See page <xref label="Seep01" href="RM000002MQJ04SX_01_0011"/>).</ptxt>
</test1>
<test1>
<ptxt>Compare the "valve off" (high pressure control mode) and "valve on" (low pressure control mode) oil pressure values and check the difference.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>50 kPa or higher.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0016" fin="true">OK</down>
<right ref="RM000004HM500OX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0005" proc-id="RM23G0E___00002WS00001">
<testtitle>CHECK OIL PUMP ASSEMBLY (OIL PRESSURE SWITCHING VALVE OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the connector of the oil pressure switching valve assembly.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure that the engine is stopped before disconnecting the connector of the oil pressure switching valve assembly.</ptxt>
</item>
<item>
<ptxt>Make sure that the disconnected connector does not get caught in moving engine parts.</ptxt>
</item>
</list1>
</atten3>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Engine Oil Pressure Control Check (See page <xref label="Seep01" href="RM000002MQJ04SX_01_0011"/>).</ptxt>
</test1>
<test1>
<ptxt>Check the engine oil pressure value.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.30in"/>
<colspec colname="COL2" colwidth="1.78in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Engine oil pressure rises to between 170 and 450 kPa</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Low oil pressure control mode is not performed when the connector of the oil pressure switching valve assembly is disconnected. Therefore, if the measurement results from Engine Oil Pressure Control Check all indicate oil pressure values which are high, it can be determined that there is an electrical malfunction.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0006" fin="false">A</down>
<right ref="RM000004HM500OX_04_0013" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0006" proc-id="RM23G0E___00002WT00001">
<testtitle>INSPECT OIL PRESSURE SWITCHING VALVE RELAY (2ST OIL PRESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the oil pressure switching valve relay (2ST OIL PRESS) (See page <xref label="Seep01" href="RM00000469100VX_01_0003"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0007" fin="false">OK</down>
<right ref="RM000004HM500OX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0007" proc-id="RM23G0E___00002WU00001">
<testtitle>CHECK HARNESS AND CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the oil pressure switching valve relay (2ST OIL PRESS) from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the oil pressure switching valve assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<table pgwide="1">
<title>Standard Resistance (Check for Short)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C130-1 (OSV+) - Body ground*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OSV relay terminal 5 - OSV relay terminal 1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OSV relay terminal 5 - Body ground*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>OSV relay terminal 3 - Body ground*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C92-25 (EOPV) - Body ground*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Make sure that there are no short circuits to the battery</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Reinstall the oil pressure switching valve relay (2ST OIL PRESS).</ptxt>
</test1>
<test1>
<ptxt>Reconnect the oil pressure switching valve assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0008" fin="false">OK</down>
<right ref="RM000004HM500OX_04_0010" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0008" proc-id="RM23G0E___00002WV00001">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001YX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000004HM500OX_04_0019" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0009" proc-id="RM23G0E___00002WW00001">
<testtitle>REPLACE OIL PRESSURE SWITCHING VALVE RELAY (2ST OIL PRESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the oil pressure switching valve relay (2ST OIL PRESS).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000004HM500OX_04_0019" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0010" proc-id="RM23G0E___00002WX00001">
<testtitle>REPAIR OR REPLACE HARNESS AND CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM000004HM500OX_04_0019" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0013" proc-id="RM23G0E___00002WZ00001">
<testtitle>INSPECT ENGINE OIL PRESSURE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the engine oil pressure (See page <xref label="Seep01" href="RM000002MQJ04SX_01_0006"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0012" fin="false">OK</down>
<right ref="RM000004HM500OX_04_0018" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0012" proc-id="RM23G0E___00002WY00001">
<testtitle>REPLACE OIL PRESSURE SENDER GAUGE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the  oil pressure sender gauge assembly (See page <xref label="Seep01" href="RM00000463L012X"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000004HM500OX_04_0019" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0018" proc-id="RM23G0E___00002X200001">
<testtitle>REPLACE OIL PRESSURE SWITCHING VALVE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the oil pressure switching valve assembly (See page <xref label="Seep01" href="RM000000PWP052X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0014" proc-id="RM23G0E___00002X000001">
<testtitle>INSPECT OIL PUMP ASSEMBLY (OIL PRESSURE SWITCHING VALVE OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Engine Oil Pressure Control Check (See page <xref label="Seep01" href="RM000002MQJ04SX_01_0011"/>).</ptxt>
</test1>
<test1>
<ptxt>Compare the "valve off" (high pressure control mode) and "valve on" (low pressure control mode) oil pressure values and check that the oil pressure has risen.</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>50 kPa or higher.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0016" fin="true">OK</down>
<right ref="RM000004HM500OX_04_0017" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0017" proc-id="RM23G0E___00002X100001">
<testtitle>REPAIR OR REPLACE OIL PUMP ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the oil pump assembly.</ptxt>
<atten4>
<ptxt>If the oil pressure does not rise after disconnecting the connector of the oil pressure switching valve assembly, or if the oil pressure changes, but the difference between the low oil pressure and high oil pressure is small, a mechanical malfunction is probably the cause.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The sleeve is locked due to foreign matter.</ptxt>
</item>
<item>
<ptxt>The oil pressure relief valve is locked due to foreign matter.</ptxt>
</item>
<item>
<ptxt>The check valve is stuck.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0019" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0019" proc-id="RM23G0E___00002X300001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Engine Oil Pressure Control Check (See page <xref label="Seep01" href="RM000002MQJ04SX_01_0011"/>).</ptxt>
</test1>
<test1>
<ptxt>Compare the "valve off" (high pressure control mode) and "valve on" (low pressure control mode) oil pressure values and check that the oil pressure has risen.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004HM500OX_04_0016" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004HM500OX_04_0016">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>