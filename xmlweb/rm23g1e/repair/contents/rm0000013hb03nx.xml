<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM0000013HB03NX" category="C" type-id="303OU" name-id="ESZCT-04" from="201210">
<dtccode>P2431</dtccode>
<dtcname>Secondary Air Injection System Air Flow / Pressure Sensor Circuit Range / Performance Bank1</dtcname>
<dtccode>P2432</dtccode>
<dtcname>Secondary Air Injection System Air Flow / Pressure Sensor Circuit Low Bank1</dtcname>
<dtccode>P2433</dtccode>
<dtcname>Secondary Air Injection System Air Flow / Pressure Sensor Circuit High Bank1</dtcname>
<subpara id="RM0000013HB03NX_02" type-id="60" category="03" proc-id="RM23G0E___0000JKI00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The secondary air injection system consists of an air pump, the air switching valve, a air pressure sensor, the air injection control driver and the ECM. For a short time after cold engine starts, the secondary air injection system pumps secondary air to the exhaust port of the cylinder head to purify the exhaust emissions. The secondary air is supplied by the air pump and is pumped to the exhaust port through the air switching valve.</ptxt>
<ptxt>The air injection control driver drives the air switching valve and the air pump according to command signals transmitted by the ECM. The air pressure sensor detects the pressure in the secondary air passage when the secondary air injection system is ON and OFF, and transmits pressure signal to the ECM.</ptxt>
<ptxt>The air injection control driver is not only equipped to drive the pump and valve, but also with a diagnosis function to detect malfunctions in the secondary air injection system circuit.</ptxt>
<atten4>
<ptxt>As a large current is required to drive the air pump and air switching valve, an air injection control driver is included in this system.</ptxt>
</atten4>
<figure>
<graphic graphicname="A121746E09" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2431</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air pressure sensor indicates less than 45.63 kPa (342 mmHg), or more than 135 kPa (1013 mmHg) (2 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air pressure sensor</ptxt>
</item>
<item>
<ptxt>Open or short in air pressure sensor circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2432</ptxt>
</entry>
<entry valign="middle">
<ptxt>While engine running, voltage output of air pressure sensor remains below 0.5 V (1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air pressure sensor</ptxt>
</item>
<item>
<ptxt>Open or short in air pressure sensor circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2433</ptxt>
</entry>
<entry valign="middle">
<ptxt>While engine running, voltage output of air pressure sensor remains above 4.5 V (1 trip detection logic)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Air pressure sensor</ptxt>
</item>
<item>
<ptxt>Open or short in air pressure sensor circuit</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000013HB03NX_03" type-id="64" category="03" proc-id="RM23G0E___0000JKJ00001">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A085160E21" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>The ECM monitors the pressure in the secondary air passage using the air pressure sensor located on the intake air connector.</ptxt>
<ptxt>If there is a defect in the sensor or the sensor circuit, the voltage level deviates from the normal operating range, the ECM interprets this deviation as a malfunction in the air pressure sensor or circuit and sets a DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM0000013HB03NX_08" type-id="32" category="03" proc-id="RM23G0E___0000JKK00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A237375E05" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000013HB03NX_09" type-id="51" category="05" proc-id="RM23G0E___0000JKL00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM0000013HB03NX_10" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000013HB03NX_10_0002" proc-id="RM23G0E___0000JKM00001">
<testtitle>CHECK HARNESS AND CONNECTOR (AIR PRESSURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the air pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connectors.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C137-1 (E2) - C61-7 (E2G)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C137-2 (AIP) - G59-1 (AIP)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C137-3 (VC) - C62-6 (VC)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC1" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C137-2 (AIP) or G59-1 (AIP) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>C137-3 (VC) or C62-6 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the air pressure sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connectors.</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000013HB03NX_10_0007" fin="false">OK</down>
<right ref="RM0000013HB03NX_10_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013HB03NX_10_0007" proc-id="RM23G0E___0000JKO00001">
<testtitle>INSPECT ECM (SENSOR POWER SOURCE CIRCUIT)</testtitle>
<content6 releasenbr="1">
<figure>
<graphic graphicname="A225616E04" width="2.775699831in" height="1.771723296in"/>
</figure>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C61-7 (E2G) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC3" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C62-6 (VC) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.5 to 5.5 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000013HB03NX_10_0003" fin="false">OK</down>
<right ref="RM0000013HB03NX_10_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013HB03NX_10_0003" proc-id="RM23G0E___0000JKN00001">
<testtitle>READ DATA LIST (PUMP PRESSURE (ABSOLUTE))</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect a pressure gauge to the air pressure sensor as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A273766" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Air pump pressure (absolute).</ptxt>
</test1>
<test1>
<ptxt>Check that the pressure displayed on the intelligent tester fluctuates when applying the pressure to the air pressure sensor with the pressure gauge.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Pressure fluctuates in response to pressure applied with pressure gauge.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The intelligent tester displays the air pump pressure (Air pump pressure (absolute)) as absolute pressure.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000013HB03NX_10_0006" fin="true">OK</down>
<right ref="RM0000013HB03NX_10_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000013HB03NX_10_0004">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM0000013HB03NX_10_0008">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202UX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013HB03NX_10_0005">
<testtitle>REPLACE AIR PRESSURE SENSOR<xref label="Seep01" href="RM000004XDX002X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000013HB03NX_10_0006">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202UX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>