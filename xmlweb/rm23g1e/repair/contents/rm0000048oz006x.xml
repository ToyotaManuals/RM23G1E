<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="55">
<name>Steering</name>
<section id="12036_S001L" variety="S001L">
<name>POWER ASSIST SYSTEMS</name>
<ttl id="12036_S001L_7B98K_T00I8" variety="T00I8">
<name>VANE PUMP (for 2TR-FE)</name>
<para id="RM0000048OZ006X" category="G" type-id="8000T" name-id="PA0LF-01" from="201207">
<name>ON-VEHICLE INSPECTION</name>
<subpara id="RM0000048OZ006X_01" type-id="01" category="01">
<s-1 id="RM0000048OZ006X_01_0001" proc-id="RM23G0E___0000B8G00000">
<ptxt>INSPECT DRIVE BELT</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Visually check the belt for excessive wear, frayed cords, etc.</ptxt>
<ptxt>If any defect is found, replace the drive belt.</ptxt>
<atten4>
<ptxt>Cracks on the rib side of a belt are considered acceptable. Replace the belt if there are any missing ribs.</ptxt>
</atten4>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048OZ006X_01_0002" proc-id="RM23G0E___000035N00000">
<ptxt>BLEED AIR FROM POWER STEERING SYSTEM</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check the fluid level.</ptxt>
</s2>
<s2>
<ptxt>Jack up the front of the vehicle and support it with stands.</ptxt>
</s2>
<s2>
<ptxt>Turn the steering wheel.</ptxt>
<s3>
<ptxt>With the engine stopped, turn the wheel slowly from lock to lock several times.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Lower the vehicle.</ptxt>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
<s3>
<ptxt>Run the engine at idle for a few minutes.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Turn the steering wheel.</ptxt>
<s3>
<ptxt>With the engine idling, turn the wheel to the left or right full lock position and keep it there for 2 to 3 seconds, and then turn the wheel to the opposite full lock position and keep it there for 2 to 3 seconds.*1</ptxt>
</s3>
<s3>
<ptxt>Repeat *1 several times.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Check for foaming or emulsification.</ptxt>
<figure>
<graphic graphicname="C124274E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If the system has to be bled twice specifically because of foaming or emulsification, check for fluid leaks in the power steering system.</ptxt>
</s2>
<s2>
<ptxt>Check the fluid level.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048OZ006X_01_0003" proc-id="RM23G0E___0000B8H00000">
<ptxt>CHECK POWER STEERING FLUID LEVEL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Keep the vehicle horizontal. </ptxt>
<figure>
<graphic graphicname="C115381E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>HOT Range</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>COLD Range</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>With the engine stopped, check the fluid level in the reservoir.</ptxt>
<ptxt>If necessary, add fluid.</ptxt>
<spec>
<title>Fluid</title>
<specitem>
<ptxt>ATF "DEXRON" II or III, or equivalent.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>If the fluid is hot (over 40°C (104°F)), make sure that the fluid level is within the HOT range on the reservoir. If the fluid is cold, make sure that it is within the COLD range.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Start the engine and run it at idle.</ptxt>
</s2>
<s2>
<ptxt>Turn the steering wheel from lock to lock several times to raise the fluid temperature.</ptxt>
<spec>
<title>Fluid temperature</title>
<specitem>
<ptxt>80°C (176°F)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>Check for foaming or emulsification.</ptxt>
<figure>
<graphic graphicname="C124274E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>CORRECT</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>INCORRECT</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>If foaming or emulsification is identified, bleed air from the power steering system.</ptxt>
</s2>
<s2>
<ptxt>With the engine idling, measure the fluid level in the reservoir.</ptxt>
<figure>
<graphic graphicname="C115383E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Idling</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Stopped</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>5 mm or less</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Stop the engine.</ptxt>
</s2>
<s2>
<ptxt>Wait a few minutes and remeasure the fluid level in the reservoir.</ptxt>
<spec>
<title>Maximum increase in fluid level</title>
<specitem>
<ptxt>Approximately 5 mm (0.197 in.)</ptxt>
</specitem>
</spec>
<ptxt>If the fluid level increase is more than the maximum, bleed air from the power steering system.</ptxt>
</s2>
<s2>
<ptxt>Check the fluid level.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000048OZ006X_01_0004" proc-id="RM23G0E___0000B8I00000">
<ptxt>CHECK STEERING FLUID PRESSURE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the pressure feed tube from the vane pump.</ptxt>
</s2>
<s2>
<ptxt>Connect SST as shown in the illustration below.</ptxt>
<sst>
<sstitem>
<s-number>09640-10010</s-number>
<s-subnumber>09641-01010</s-subnumber>
<s-subnumber>09641-01030</s-subnumber>
<s-subnumber>09641-01060</s-subnumber>
</sstitem>
</sst>
<figure>
<graphic graphicname="C215573E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>In</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Out</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Attachment</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressure Feed Tube</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Check that the valve of SST is in the open position.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Bleed air from the power steering system.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and run it at idle.</ptxt>
</s2>
<s2>
<ptxt>Turn the steering wheel from lock to lock several times to raise the fluid temperature.</ptxt>
<spec>
<title>Fluid temperature</title>
<specitem>
<ptxt>80°C (176°F)</ptxt>
</specitem>
</spec>
</s2>
<s2>
<ptxt>With the engine idling, close the valve of SST and observe the reading on SST.</ptxt>
<figure>
<graphic graphicname="C162692E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard fluid pressure</title>
<specitem>
<ptxt>8300 to 8800 kPa (84.7 to 89.7 kgf/cm<sup>2</sup>, 1204 to 1276 psi)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Gear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Fluid Reservoir</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Vane Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve Closed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not keep the valve closed for more than 10 seconds.</ptxt>
</item>
<item>
<ptxt>Do not allow the fluid temperature to become too high.</ptxt>
</item>
</list1>
</atten3>
<ptxt>If the pressure is not as specified, check for fluid leaks and replace parts as necessary.</ptxt>
</s2>
<s2>
<ptxt>With the engine idling, open the valve fully.</ptxt>
<figure>
<graphic graphicname="C162693E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Gear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Fluid Reservoir</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Vane Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve Open</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Measure the fluid pressure at engine speeds of 1000 rpm and 3000 rpm.</ptxt>
<spec>
<title>Fluid pressure difference</title>
<specitem>
<ptxt>490 kPa (5.0 kgf/cm<sup>2</sup>, 71 psi) or less</ptxt>
</specitem>
</spec>
<atten3>
<ptxt>Do not turn the steering wheel.</ptxt>
</atten3>
<ptxt>If the pressure difference is not within the specified range, check for fluid leaks and replace parts as necessary.</ptxt>
</s2>
<s2>
<ptxt>With the engine idling and the valve fully open, turn the steering wheel left or right to the full lock position. Observe the reading on SST. </ptxt>
<figure>
<graphic graphicname="C162694E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<spec>
<title>Standard fluid pressure</title>
<specitem>
<ptxt>8300 to 8800 kPa (84.7 to 89.7 kgf/cm<sup>2</sup>, 1204 to 1276 psi)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Gear (Lock Position)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Fluid Reservoir</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Power Steering Vane Pump</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Valve Open</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not keep the steering wheel in the full lock position for more than 10 seconds. </ptxt>
</item>
<item>
<ptxt>Do not allow the fluid temperature to become too high.</ptxt>
</item>
</list1>
</atten3>
<ptxt>If the pressure is not as specified, check for fluid leaks and replace parts as necessary.</ptxt>
</s2>
<s2>
<ptxt>Disconnect SST.</ptxt>
</s2>
<s2>
<ptxt>Connect the pressure feed tube to the vane pump.</ptxt>
</s2>
<s2>
<ptxt>Bleed air from the power steering system.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>