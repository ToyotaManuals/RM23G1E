<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12072_S002G" variety="S002G">
<name>EXTERIOR PANELS / TRIM</name>
<ttl id="12072_S002G_7B9HX_T00RL" variety="T00RL">
<name>FRONT DOOR WINDOW FRAME MOULDING</name>
<para id="RM0000038KK027X" category="A" type-id="30014" name-id="ET63B-02" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM0000038KK027X_02" type-id="11" category="10" proc-id="RM23G0E___0000JFA00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for the RH and LH sides.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>When installing the window frame moulding, heat the vehicle body and window frame moulding using a heat light.</ptxt>
<spec>
<title>Standard</title>
<table>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Temperature</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vehicle Body</ptxt>
</entry>
<entry valign="middle">
<ptxt>40 to 60°C (104 to 140°F)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Window Frame Moulding</ptxt>
</entry>
<entry valign="middle">
<ptxt>20 to 30°C (68 to 86°F)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</item>
</list1>
</atten4>
<atten3>
<ptxt>Do not heat the vehicle body or window frame moulding excessively.</ptxt>
</atten3>
</content3>
</subpara>
<subpara id="RM0000038KK027X_01" type-id="01" category="01">
<s-1 id="RM0000038KK027X_01_0034" proc-id="RM23G0E___0000INO00000">
<ptxt>INSTALL FRONT DOOR REAR WINDOW FRAME MOULDING LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Clean the vehicle body surface.</ptxt>
<s3>
<ptxt>Using a heat light, heat the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Remove the double-sided tape from the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Wipe off any tape adhesive residue with cleaner.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install a new front door rear window frame moulding.</ptxt>
<s3>
<ptxt>Using a heat light, heat a new front door rear window frame moulding and the vehicle body surface.</ptxt>
</s3>
<s3>
<ptxt>Remove the peeling paper from the face of the front door rear window frame moulding.</ptxt>
<atten4>
<ptxt>After removing the peeling paper, keep the exposed adhesive free from foreign matter.</ptxt>
</atten4>
</s3>
<s3>
<ptxt>Attach the clip and double-sided tape to install the front door rear window frame moulding.</ptxt>
<atten4>
<ptxt>Press the front door rear window frame moulding firmly to install it.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>Install a new door window frame moulding clip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038KK027X_01_0035" proc-id="RM23G0E___0000JF800000">
<ptxt>INSTALL FRONT DOOR WEATHERSTRIP LH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 clips to install the upper part of the front door weatherstrip.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000038KK027X_01_0036" proc-id="RM23G0E___0000INP00000">
<ptxt>INSTALL FRONT DOOR BELT MOULDING LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 6 claws to install the front door belt moulding.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0025" proc-id="RM23G0E___0000EKF00000">
<ptxt>INSTALL FRONT DOOR GLASS RUN LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door glass run.</ptxt>
<figure>
<graphic graphicname="B238535" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0026" proc-id="RM23G0E___0000EKG00000">
<ptxt>INSTALL FRONT DOOR GLASS SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the cable to the negative (-) battery terminal.</ptxt>
</s2>
<s2>
<ptxt>Connect the power window regulator master switch assembly and move the front door glass sub-assembly so that the door glass bolt installation locations can be seen.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the cable from the negative (-) battery terminal and power window regulator master switch assembly.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten2>
</s2>
<s2>
<ptxt>Insert the front door glass sub-assembly into the front door panel along the front door glass run as indicated by the arrows in the order shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B241243E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Install the front door glass sub-assembly with the 2 bolts.</ptxt>
<figure>
<graphic graphicname="B238531E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>5.5</t-value1>
<t-value2>56</t-value2>
<t-value3>49</t-value3>
</torqueitem>
</torque>
<atten4>
<ptxt>Tighten the bolts in the order shown in the illustration.</ptxt>
</atten4>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0027" proc-id="RM23G0E___0000INK00000">
<ptxt>INSTALL OUTER REAR VIEW MIRROR ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the claw to install the outer rear view mirror, and then install the 3 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0028" proc-id="RM23G0E___0000EKH00000">
<ptxt>INSTALL FRONT DOOR SERVICE HOLE COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply new butyl tape to the front door panel.</ptxt>
</s2>
<s2>
<ptxt>Pass the front door lock remote control cable assembly and front door inside locking cable assembly through a new front door service hole cover.</ptxt>
<figure>
<graphic graphicname="B238607E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reference Point</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Attach the front door service hole cover using the reference points on the front door panel.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>There should be no wrinkles or folds after attaching the service hole cover.</ptxt>
</item>
<item>
<ptxt>After attaching the service hole cover, check the seal quality.</ptxt>
</item>
</list1>
</atten3>
<atten3>
<ptxt>Securely install the front door service hole cover preventing wrinkles and air bubbles.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Attach the 2 clamps.</ptxt>
<figure>
<graphic graphicname="B238530" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Install the bolt to the front door wire.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0029" proc-id="RM23G0E___0000BZ000000">
<ptxt>INSTALL FRONT DOOR INNER GLASS WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the front door inner glass weatherstrip.</ptxt>
<figure>
<graphic graphicname="B238605" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0030" proc-id="RM23G0E___0000BYV00000">
<ptxt>INSTALL FRONT DOOR TRIM BOARD SUB-ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the front door lock remote control cable assembly and front door inside locking cable assembly.</ptxt>
<figure>
<graphic graphicname="B241244" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Connect 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/ Seat Position Memory System:</ptxt>
<s3>
<ptxt>Connect the connectors.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Attach the front door trim board sub-assembly by attaching the 4 claws of the front door inner glass weatherstrip as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="B241251" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Attach the 12 clips and front door trim board retainer to install the front door trim board sub-assembly.</ptxt>
<figure>
<graphic graphicname="B238522" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Install the 3 screws.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0031" proc-id="RM23G0E___0000BYW00000">
<ptxt>INSTALL ASSIST GRIP COVER LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 8 claws to install the assist grip cover.</ptxt>
<figure>
<graphic graphicname="B241252" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0032" proc-id="RM23G0E___0000BYX00000">
<ptxt>INSTALL NO. 2 DOOR INSIDE HANDLE BEZEL LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 3 claws to install the inside handle bezel.</ptxt>
<figure>
<graphic graphicname="B241259" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0033" proc-id="RM23G0E___0000BYY00000">
<ptxt>INSTALL FRONT DOOR LOWER FRAME BRACKET GARNISH LH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Attach the 2 claws to install the front door lower frame bracket garnish.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000038KK027X_01_0013" proc-id="RM23G0E___0000JF700000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000038KK027X_01_0037" proc-id="RM23G0E___0000JF900000">
<ptxt>ADJUST SIDE TELEVISION CAMERA ASSEMBLY (w/ Side Monitor System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/ Rear View Monitor System:</ptxt>
<ptxt>Adjust the side television camera (See page <xref label="Seep01" href="RM000003XM401DX"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ Parking Assist Monitor System:</ptxt>
<ptxt>Adjust the side television camera (See page <xref label="Seep02" href="RM000003XM401CX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>