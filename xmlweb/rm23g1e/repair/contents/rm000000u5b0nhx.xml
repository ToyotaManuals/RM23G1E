<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0008" variety="S0008">
<name>2TR-FE ENGINE CONTROL</name>
<ttl id="12005_S0008_7B8WX_T006L" variety="T006L">
<name>SFI SYSTEM</name>
<para id="RM000000U5B0NHX" category="C" type-id="302AS" name-id="ESVJ6-03" from="201207" to="201210">
<dtccode>P0102</dtccode>
<dtcname>Mass or Volume Air Flow Circuit Low Input</dtcname>
<dtccode>P0103</dtccode>
<dtcname>Mass or Volume Air Flow Circuit High Input</dtcname>
<subpara id="RM000000U5B0NHX_01" type-id="60" category="03" proc-id="RM23G0E___00003A800000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The mass air flow meter is a sensor that measures the amount of air flowing through the throttle valve. The ECM uses this information to determine the fuel injection time and to provide the appropriate air fuel ratio.</ptxt>
<ptxt>Inside the mass air flow meter, there is a heated platinum wire which is exposed to the flow of intake air.</ptxt>
<ptxt>By applying a specific electrical current to the wire, the ECM heats it to a given temperature. The flow of incoming air cools both the wire and an internal thermistor, affecting their resistance. To maintain a constant current value, the ECM varies the voltage applied to the wire and internal thermistor. The voltage level is proportional to the air flow through the sensor and the ECM uses it to calculate the intake air volume.</ptxt>
<ptxt>The circuit is constructed so that the platinum hot wire and temperature sensor create a bridge circuit, and the power transistor is controlled so that the potentials of A and B remain equal to maintain the predetermined temperature.</ptxt>
<atten4>
<ptxt>When any of these DTCs are stored, the ECM enters fail-safe mode. During fail-safe mode, the ignition timing is calculated by the ECM, according to the engine speed and throttle valve position. The ECM continues operating in fail-safe mode until a pass condition is detected.</ptxt>
</atten4>
<figure>
<graphic graphicname="A170707E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0102</ptxt>
</entry>
<entry valign="middle">
<ptxt>The mass air flow meter voltage is below 0.2 V for 3 seconds (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in mass air flow meter circuit</ptxt>
</item>
<item>
<ptxt>Mass air flow meter</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P0103</ptxt>
</entry>
<entry valign="middle">
<ptxt>The mass air flow meter voltage is higher than 4.9 V for 3 seconds (1-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in mass air flow meter circuit</ptxt>
</item>
<item>
<ptxt>Mass air flow meter</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When any of these DTCs are output, check the air flow rate using the intelligent tester. Enter the following menus: Powertrain / Engine and ECT / Data List / MAF.</ptxt>
<table pgwide="1">
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Mass Air Flow Rate (gm/s)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Malfunction</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Approximately 0.0</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open in mass air flow meter power source circuit</ptxt>
</item>
<item>
<ptxt>Open or short in VG circuit</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>271.0 or more</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Open in E2G circuit</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</content5>
</subpara>
<subpara id="RM000000U5B0NHX_07" type-id="32" category="03" proc-id="RM23G0E___00003A900000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A227333E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000U5B0NHX_08" type-id="51" category="05" proc-id="RM23G0E___00003AA00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000U5B0NHX_11" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000U5B0NHX_11_0001" proc-id="RM23G0E___00003AB00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (MASS AIR FLOW RATE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / MAF.</ptxt>
</test1>
<test1>
<ptxt>Read the values displayed on the tester.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Mass air flow rate is approximately 0.0 gm/s</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mass air flow rate is 271.0 gm/s or more</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Mass air flow rate is between 1.0 and 270.0 gm/s*</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*: The value changes when the throttle valve is open or closed with the engine running.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000U5B0NHX_11_0002" fin="false">A</down>
<right ref="RM000000U5B0NHX_11_0012" fin="false">B</right>
<right ref="RM000000U5B0NHX_11_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0002" proc-id="RM23G0E___00003AC00000">
<testtitle>INSPECT MASS AIR FLOW METER (POWER SOURCE VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
<figure>
<graphic graphicname="A127724E55" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C33-1 (+B) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch ON</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Mass Air Flow Meter)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000U5B0NHX_11_0003" fin="false">OK</down>
<right ref="RM000000U5B0NHX_11_0021" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0003" proc-id="RM23G0E___00003AD00000">
<testtitle>CHECK MASS AIR FLOW METER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the mass air flow meter (See page <xref label="Seep01" href="RM0000031C100DX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000U5B0NHX_11_0004" fin="false">OK</down>
<right ref="RM000000U5B0NHX_11_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0004" proc-id="RM23G0E___00003AE00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C33-3 (VG) - C61-13 (VG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C33-2 (E2G) - C61-7 (E2G)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC3" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C33-3 (VG) or C61-13 (VG) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000U5B0NHX_11_0008" fin="true">OK</down>
<right ref="RM000000U5B0NHX_11_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0021" proc-id="RM23G0E___00003AH00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - INTEGRATION RELAY [EFI RELAY])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the integration relay (EFI relay) from the engine room relay block. </ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C33-1 (+B) - 1B-4</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>C33-1 (+B) or 1B-4 - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the integration relay. </ptxt>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000U5B0NHX_11_0024" fin="true">OK</down>
<right ref="RM000000U5B0NHX_11_0023" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0012" proc-id="RM23G0E___00003AF00000">
<testtitle>CHECK HARNESS AND CONNECTOR (SENSOR GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
<figure>
<graphic graphicname="A127724E50" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COLSPEC6" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C33-2 (E2G) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Mass Air Flow Meter)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000U5B0NHX_11_0006" fin="true">OK</down>
<right ref="RM000000U5B0NHX_11_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0013" proc-id="RM23G0E___00003AG00000">
<testtitle>CHECK HARNESS AND CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC7" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C33-2 (E2G) - C61-7 (E2G)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC7" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C33-2 (E2G) or C61-7 (E2G) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the mass air flow meter connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000U5B0NHX_11_0008" fin="true">OK</down>
<right ref="RM000000U5B0NHX_11_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0005">
<testtitle>CHECK FOR INTERMITTENT PROBLEMS<xref label="Seep01" href="RM000000PDQ0VKX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0006">
<testtitle>REPLACE MASS AIR FLOW METER<xref label="Seep01" href="RM000002VE200DX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (MASS AIR FLOW METER - ECM)</testtitle>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0008">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0023">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR (MASS AIR FLOW METER - INTEGRATION RELAY [EFI RELAY])</testtitle>
</testgrp>
<testgrp id="RM000000U5B0NHX_11_0024">
<testtitle>CHECK ECM POWER SOURCE CIRCUIT<xref label="Seep01" href="RM0000027690AYX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>