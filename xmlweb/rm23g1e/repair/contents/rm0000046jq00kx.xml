<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12063_S0028" variety="S0028">
<name>INTERIOR PANELS / TRIM</name>
<ttl id="12063_S0028_7B9FD_T00P1" variety="T00P1">
<name>ROOF HEADLINING (for 3 Door)</name>
<para id="RM0000046JQ00KX" category="A" type-id="80001" name-id="IT4AL-01" from="201210">
<name>REMOVAL</name>
<subpara id="RM0000046JQ00KX_01" type-id="11" category="10" proc-id="RM23G0E___0000I0Z00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM0000046JQ00KX_02" type-id="01" category="01">
<s-1 id="RM0000046JQ00KX_02_0071" proc-id="RM23G0E___0000JR500000">
<ptxt>PRECAUTION</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0065" proc-id="RM23G0E___0000I1U00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0066" proc-id="RM23G0E___0000I1V00001">
<ptxt>REMOVE REAR NO. 1 SEAT ASSEMBLY (w/ Rear No. 1 Seat)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LH Side:</ptxt>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep01" href="RM0000046ZI003X"/>). </ptxt>
</s2>
<s2>
<ptxt>for RH Side:</ptxt>
<ptxt>Remove the rear No. 1 seat assembly (See page <xref label="Seep02" href="RM0000046ZN003X"/>). </ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0001" proc-id="RM23G0E___00008CL00001">
<ptxt>REMOVE DOOR SCUFF PLATE ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238732E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Put protective tape around the door scuff plate.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 4 clips, 10 claws and 2 guides and remove the door scuff plate.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0002" proc-id="RM23G0E___0000F4600001">
<ptxt>REMOVE DOOR SCUFF PLATE ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0003" proc-id="RM23G0E___00008CN00001">
<ptxt>REMOVE COWL SIDE TRIM BOARD LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238734" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Detach the clip and claw and remove the cowl side trim board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0004" proc-id="RM23G0E___0000I1000001">
<ptxt>REMOVE COWL SIDE TRIM BOARD RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0005" proc-id="RM23G0E___0000C5G00001">
<ptxt>REMOVE FRONT DOOR OPENING TRIM WEATHERSTRIP LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238778" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the front door opening trim weatherstrip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0006" proc-id="RM23G0E___0000C5K00001">
<ptxt>REMOVE FRONT DOOR OPENING TRIM WEATHERSTRIP RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0007" proc-id="RM23G0E___0000C0600001">
<ptxt>REMOVE FRONT NO. 1 ASSIST GRIP PLUG LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238741E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other front No. 1 assist grip plug.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the front No. 1 assist grip plug.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0008" proc-id="RM23G0E___0000I1100001">
<ptxt>REMOVE FRONT NO. 1 ASSIST GRIP PLUG RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0056" proc-id="RM23G0E___0000C0300001">
<ptxt>REMOVE NO. 1 ASSIST GRIP
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238742" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other No. 1 assist grip.</ptxt>
</atten4>
<s2>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the No. 1 assist grip.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0010" proc-id="RM23G0E___0000C0400001">
<ptxt>REMOVE FRONT PILLAR GARNISH LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238743" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 3 guides and remove the front pillar garnish.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B238744E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Curtain Shield Airbag:</ptxt>
<ptxt>Protect the curtain shield airbag assembly.</ptxt>
<s3>
<ptxt>Completely cover the airbag with a cloth or nylon sheet and secure the ends of the cover with adhesive tape as shown in the illustration.</ptxt>
<atten3>
<ptxt>Cover the curtain shield airbag with a protective cover as soon as the front pillar garnish is removed.</ptxt>
</atten3>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Curtain Shield Airbag Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Adhesive Tape</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Cover</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0011" proc-id="RM23G0E___0000I1200001">
<ptxt>REMOVE FRONT PILLAR GARNISH RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0064" proc-id="RM23G0E___0000C5700001">
<ptxt>REMOVE TONNEAU COVER ASSEMBLY (w/ Tonneau Cover)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the tonneau cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0012" proc-id="RM23G0E___0000C5800001">
<ptxt>REMOVE MAT SET PLATE COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240525E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for all mat set plate covers.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the mat set plate cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0013" proc-id="RM23G0E___00005NQ00001">
<ptxt>REMOVE REAR FLOOR MAT REAR SUPPORT PLATE</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240526" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the 5 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 6 claws and remove the rear floor mat rear support plate.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0014" proc-id="RM23G0E___0000C5900001">
<ptxt>REMOVE QUARTER TRIM COVER HOLE LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240533" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 2 claws and 2 guides and remove the quarter trim cover hole.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0015" proc-id="RM23G0E___0000C5L00001">
<ptxt>REMOVE QUARTER TRIM COVER HOLE RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0016" proc-id="RM23G0E___0000C5A00001">
<ptxt>REMOVE REAR FLOOR CARPET ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240527" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the rear floor carpet.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0017" proc-id="RM23G0E___0000C5H00001">
<ptxt>REMOVE NO. 1 LUGGAGE COMPARTMENT TRIM HOOK</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181695" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other No. 1 luggage compartment trim hook.</ptxt>
</atten4>
<s2>
<ptxt>Remove the luggage compartment trim hook by turning it clockwise.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0018" proc-id="RM23G0E___0000C5I00001">
<ptxt>REMOVE NO. 1 TONNEAU COVER HOLDER CAP (w/o Tonneau Cover)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240528E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other tonneau cover holder cap.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the tonneau cover holder cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0019" proc-id="RM23G0E___0000C5D00001">
<ptxt>REMOVE FRONT DECK SIDE TRIM COVER (w/ Tonneau Cover)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240051E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other front deck side trim cover.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and remove the front deck side trim cover.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0039" proc-id="RM23G0E___0000C5B00001">
<ptxt>REMOVE QUARTER TRIM POCKET TRAY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240553E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Put protective tape around the quarter trim pocket tray.</ptxt>
</s2>
<s2>
<ptxt>Using a screwdriver, detach the 6 claws and 2 guides, and remove the quarter trim pocket tray.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0057" proc-id="RM23G0E___0000C5C00001">
<ptxt>REMOVE OUTER LAP BELT ANCHOR COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240529" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other outer lap belt anchor cover.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 3 claws and remove the outer lap belt anchor cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0020" proc-id="RM23G0E___0000C5E00001">
<ptxt>REMOVE DECK TRIM SIDE PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240531" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and disconnect the front seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240534" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear No. 1 seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 9 claws and 12 clips and remove the deck trim side panel.</ptxt>
<figure>
<graphic graphicname="B240536" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0021" proc-id="RM23G0E___0000C5F00001">
<ptxt>REMOVE DECK TRIM SIDE PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240532" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the bolt and disconnect the front seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240535" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt and disconnect the rear No. 1 seat outer belt floor anchor.</ptxt>
</s2>
<s2>
<ptxt>Remove the 3 bolts and 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 9 claws and 12 clips.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the deck trim side panel.</ptxt>
<figure>
<graphic graphicname="B240537" width="7.106578999in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0022" proc-id="RM23G0E___0000H8Y00001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240538" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Move the front shoulder belt anchor adjuster to the lowest position.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 clips and 2 guides.</ptxt>
</s2>
<s2>
<ptxt>Pass the front seat outer belt floor anchor through the front quarter trim panel and remove the front quarter trim panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0023" proc-id="RM23G0E___0000I1300001">
<ptxt>REMOVE FRONT QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0024" proc-id="RM23G0E___0000C5J00001">
<ptxt>REMOVE REAR QUARTER TRIM PANEL ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240540" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 7 clips.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240541" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 4 claws and 2 guides, pass the rear No. 1 seat belt floor anchor through the rear quarter trim panel and remove the rear quarter trim panel.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0025" proc-id="RM23G0E___0000I1400001">
<ptxt>REMOVE REAR QUARTER TRIM PANEL ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0058" proc-id="RM23G0E___0000I1O00001">
<ptxt>REMOVE ASSIST GRIP SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184001E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other assist grip.</ptxt>
</atten4>
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the 2 assist grip covers.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 2 clips and remove the assist grip.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 clips from the vehicle body.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0059" proc-id="RM23G0E___0000I1P00001">
<ptxt>REMOVE REAR ASSIST GRIP ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B184002E03" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 4 claws and remove the 2 assist grip covers.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 2 clips and remove the assist grip.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 clips from the vehicle body.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0060" proc-id="RM23G0E___0000I1Q00001">
<ptxt>REMOVE REAR ASSIST GRIP ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0061" proc-id="RM23G0E___0000I1R00001">
<ptxt>REMOVE VISOR BRACKET COVER LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181706" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other visor bracket cover.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 4 claws and remove the visor bracket cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0030" proc-id="RM23G0E___0000I1500001">
<ptxt>REMOVE VISOR BRACKET COVER RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0062" proc-id="RM23G0E___0000I1S00001">
<ptxt>REMOVE VISOR ASSEMBLY LH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B181707" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws and visor.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0032" proc-id="RM23G0E___0000I1600001">
<ptxt>REMOVE VISOR ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0063" proc-id="RM23G0E___0000I1T00001">
<ptxt>REMOVE VISOR HOLDER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B151362E02" width="2.775699831in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other visor holder.</ptxt>
</atten4>
<s2>
<ptxt>Turn the visor holder approximately 45° and pull it out as shown in the illustration.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the visor holder.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0067" proc-id="RM23G0E___0000C2G00000">
<ptxt>REMOVE MAP LIGHT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 4 clips.</ptxt>
<figure>
<graphic graphicname="E198057" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Disconnect the connector and remove the map light.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0068" proc-id="RM23G0E___0000I1W00000">
<ptxt>REMOVE RAIN SENSOR COVER (w/ Rain Sensor)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and remove the rain sensor cover.</ptxt>
<figure>
<graphic graphicname="E197772" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0070" proc-id="RM23G0E___0000FFD00000">
<ptxt>REMOVE INNER REAR VIEW MIRROR STAY HOLDER COVER (w/ EC Mirror)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the 2 claws and slide the inner rear view mirror stay holder cover in the direction indicated by the arrow in the illustration.</ptxt>
<figure>
<graphic graphicname="B175048" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<figure>
<graphic graphicname="B175049" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Detach the 2 claws and remove the inner rear view mirror stay holder cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM0000046JQ00KX_02_0037" proc-id="RM23G0E___0000I1700001">
<ptxt>REMOVE NO. 1 ROOM LIGHT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 1 room light assembly (See page <xref label="Seep01" href="RM000003MUN037X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0038" proc-id="RM23G0E___0000I1800001">
<ptxt>REMOVE NO. 2 ROOM LIGHT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the No. 2 room light assembly (See page <xref label="Seep01" href="RM000003MUN038X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0040" proc-id="RM23G0E___0000I1900001">
<ptxt>REMOVE ROOF HEADLINING ASSEMBLY</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B268455" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>w/ EC Mirror:</ptxt>
<ptxt>Disconnect the inner mirror connector.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="E197773" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Rain Sensor:</ptxt>
<ptxt>Disconnect the rain sensor connector.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B238774" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>w/ Sliding Roof:</ptxt>
<ptxt>Disconnect the drive gear connector.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B238771" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the 2 connectors and detach the 3 clamps from the front pillar LH.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B238770" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the bolt.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the 2 connectors and detach the 3 clamps from the front pillar RH.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240542" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the connector and detach the 3 clamps from the rear pillar LH.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240543" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Disconnect the connector and detach the 4 clamps from the rear pillar RH.</ptxt>
</s2>
<s2>
<figure>
<graphic graphicname="B240544" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>w/o Sliding Roof:</ptxt>
<s3>
<ptxt>Detach the 2 clips.</ptxt>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B240545E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>w/ Sliding Roof:</ptxt>
<s3>
<ptxt>Detach the 2 clips, 2 guides and 8 fasteners.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Guide</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fastener</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
</s2>
<s2>
<figure>
<graphic graphicname="B240566" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Remove the roof headlining from the back door as shown in the illustration.</ptxt>
<atten3>
<ptxt>Be careful not to damage the roof headlining when removing it.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0041" proc-id="RM23G0E___0000I1A00001">
<ptxt>REMOVE FRONT SHOULDER BELT ANCHOR PLATE SUB-ASSEMBLY LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240539" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws of the front shoulder belt anchor plate and slide the front shoulder belt anchor plate in the direction of the arrow to remove it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0042" proc-id="RM23G0E___0000I1B00001">
<ptxt>REMOVE FRONT SHOULDER BELT ANCHOR PLATE SUB-ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0043" proc-id="RM23G0E___0000I1C00001">
<ptxt>REMOVE NO. 1 ROOF SIDE RAIL GARNISH LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240548E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 5 clips.</ptxt>
</s2>
<s2>
<ptxt>Cut off the 5 clips and remove the roof side rail garnish.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cut Off Position</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Remove the 5 clips from the vehicle body.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0044" proc-id="RM23G0E___0000I1D00001">
<ptxt>REMOVE NO. 1 ROOF SIDE RAIL GARNISH RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0045" proc-id="RM23G0E___0000I1E00001">
<ptxt>REMOVE REAR SIDE RAIL SPACER LH (w/o Curtain Shield Airbag)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240547" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the rear side rail spacer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0046" proc-id="RM23G0E___0000I1F00001">
<ptxt>REMOVE REAR SIDE RAIL SPACER RH (w/o Curtain Shield Airbag)</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0047" proc-id="RM23G0E___0000FPW00001">
<ptxt>REMOVE REAR NO. 2 SIDE RAIL SPACER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240546" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the clip.</ptxt>
</s2>
<s2>
<ptxt>Detach the 4 claws and remove the rear side rail spacer.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0048" proc-id="RM23G0E___0000I1G00001">
<ptxt>REMOVE REAR NO. 2 SIDE RAIL SPACER RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0049" proc-id="RM23G0E___0000I1H00001">
<ptxt>REMOVE DECK SIDE TRIM CUP HOLDER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240554" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the deck side trim cup holder.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0050" proc-id="RM23G0E___0000I1I00001">
<ptxt>REMOVE DECK SIDE TRIM CUP HOLDER RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0051" proc-id="RM23G0E___0000I1J00001">
<ptxt>REMOVE REAR SEAT SHOULDER BELT HANGER LH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238782" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Detach the claw and guide and remove the rear seat shoulder belt hanger.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0052" proc-id="RM23G0E___0000I1K00001">
<ptxt>REMOVE REAR SEAT SHOULDER BELT HANGER RH</ptxt>
<content1 releasenbr="1">
<atten4>
<ptxt>Use the same procedure described for the LH side.</ptxt>
</atten4>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0053" proc-id="RM23G0E___0000I1L00001">
<ptxt>REMOVE QUARTER TRIM COVER</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238798" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>Use the same procedure for the other quarter trim cover.</ptxt>
</atten4>
<s2>
<ptxt>Detach the 2 claws and remove the quarter trim cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0054" proc-id="RM23G0E___0000I1M00001">
<ptxt>REMOVE QUARTER TRIM JACK COVER (w/ Spare Tire)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240552" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 7 claws and 3 guides and remove the quarter trim jack cover.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000046JQ00KX_02_0055" proc-id="RM23G0E___0000I1N00001">
<ptxt>REMOVE DECK SIDE TRIM COVER RH (w/o Spare Tire)</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B240550" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Turn the 2 knobs and release the lock.</ptxt>
</s2>
<s2>
<ptxt>Detach the 7 claws and 3 guides and remove the deck side trim cover.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>