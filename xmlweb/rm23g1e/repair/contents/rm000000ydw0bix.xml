<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AL_T00K9" variety="T00K9">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000000YDW0BIX" category="D" type-id="303F2" name-id="CA0004-194" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000000YDW0BIX_z0" proc-id="RM23G0E___0000D9B00000">
<content5 releasenbr="1">
<step1>
<ptxt>LIN COMMUNICATION SYSTEM DESCRIPTION</ptxt>
<ptxt>The LIN communication system is used for communication between the components in the tables below. If communication cannot be performed through LIN communication because of a break in the communication lines or some other reason, the master control ECU of the relevant system stores DTCs. Refer to the table below about the communication bus lines and connected components.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Each component has a fail-safe function which activates to maintain the minimum performance of the system and protect the system.</ptxt>
</item>
<item>
<ptxt>In the following table, "○" indicates that the function applies, and "-" indicates that it does not.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>Door Bus Lines</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="4.25in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Master Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LIN Communication DTC Output Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Main body ECU (multiplex network body ECU)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Multiplex network master switch assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front power window regulator motor assembly LH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front power window regulator motor assembly RH</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear power window regulator motor assembly LH*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear power window regulator motor assembly RH*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Sliding roof drive gear sub-assembly*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Double lock door control relay assembly*3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for 5 Door</ptxt>
</item>
<item>
<ptxt>*2: w/ Sliding Roof System</ptxt>
</item>
<item>
<ptxt>*3: w/ Double Locking System</ptxt>
</item>
</list1>
<table pgwide="1">
<title>Certification Bus Lines (w/ Entry and Start System)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="4.25in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Master Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LIN Communication DTC Output Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Certification ECU</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Power management control ECU</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering lock actuator assembly (steering lock ECU)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ID code box</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Lighting Bus Lines (w/ Adaptive Front-lighting System)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="4.25in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Master Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LIN Communication DTC Output Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Headlight swivel ECU assembly (AFS ECU)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Headlight assembly LH (headlight swivel motor LH, headlight leveling motor LH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Headlight assembly RH (headlight swivel motor RH, headlight leveling motor RH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Air Conditioning Bus Lines</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="4.25in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Master Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LIN Communication DTC Output Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Air conditioning amplifier assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Integration control and panel assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Air conditioning control assembly*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Rear Air Conditioning System</ptxt>
</item>
</list1>
<table pgwide="1">
<title>Windshield Wiper Bus Lines (w/ Rain Sensor)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="4.25in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Master Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LIN Communication DTC Output Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Windshield wiper relay assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rain sensor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Multi-terrain Monitor Bus Lines (w/ Multi-function Switch)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="4.25in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Master Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>LIN Communication DTC Output Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Driving support switch control ECU</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Combination meter assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Accessory meter assembly*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Accessory Meter</ptxt>
</item>
</list1>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>