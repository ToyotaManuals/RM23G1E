<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AM_T00KA" variety="T00KA">
<name>CAN COMMUNICATION SYSTEM (for LHD with Entry and Start System)</name>
<para id="RM000002L8203OX" category="J" type-id="801MT" name-id="NW2JT-02" from="201207" to="201210">
<dtccode/>
<dtcname>Open in CAN Main Wire</dtcname>
<subpara id="RM000002L8203OX_01" type-id="60" category="03" proc-id="RM23G0E___0000DKI00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>There may be an open circuit in the CAN main wire and/or DLC3 branch wire when the resistance between terminals 6 (CANH) and 14 (CANL) of the DLC3 is 69 Ω or higher.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>The resistance between terminals 6 (CANH) and 14 (CANL) of the DLC3 is 69 Ω or higher.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN main wire and connector</ptxt>
</item>
<item>
<ptxt>No. 1 junction connector</ptxt>
</item>
<item>
<ptxt>No. 2 junction connector</ptxt>
</item>
<item>
<ptxt>No. 3 junction connector</ptxt>
</item>
<item>
<ptxt>No. 4 junction connector</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>Combination meter assembly</ptxt>
</item>
<item>
<ptxt>DLC3 branch wire or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002L8203OX_02" type-id="32" category="03" proc-id="RM23G0E___0000DKJ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C217814E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002L8203OX_03" type-id="51" category="05" proc-id="RM23G0E___0000DKK00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Operating the engine switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002L8203OX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002L8203OX_05_0024" proc-id="RM23G0E___0000DKX00000">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the main wire and the branch wire.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the engine switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0001" proc-id="RM23G0E___0000DKL00000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (NO. 3 JUNCTION CONNECTOR - DLC3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G94 No. 3 junction connector connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216818E52" width="2.775699831in" height="2.775699831in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G94-5 (CANH) - G48-6 (CANH)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G94-16 (CANL) - G48-14 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.83in"/>
<colspec colname="COL2" colwidth="3.30in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 3 Junction Connector)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0002" fin="false">OK</down>
<right ref="RM000002L8203OX_05_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0002" proc-id="RM23G0E___0000DKM00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 3 JUNCTION CONNECTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E11" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G94-1 (CANH) - G94-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 3 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0003" fin="false">OK</down>
<right ref="RM000002L8203OX_05_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0003" proc-id="RM23G0E___0000DKN00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 3 JUNCTION CONNECTOR - COMBINATION METER ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E13" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G94-6 (CANH) - G94-17 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 3 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0016" fin="true">OK</down>
<right ref="RM000002L8203OX_05_0027" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0004" proc-id="RM23G0E___0000DKO00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the G94 No. 3 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0005" proc-id="RM23G0E___0000DKP00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 2 JUNCTION CONNECTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G100 No. 2 junction connector connector.</ptxt>
<figure>
<graphic graphicname="C216814E83" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G100-1 (CANH) - G100-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 2 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0006" fin="false">OK</down>
<right ref="RM000002L8203OX_05_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0006" proc-id="RM23G0E___0000DKQ00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 2 JUNCTION CONNECTOR - NO. 3 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E14" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G100-6 (CANH) - G100-17 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 2 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0019" fin="true">OK</down>
<right ref="RM000002L8203OX_05_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0007" proc-id="RM23G0E___0000DKR00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the G100 No. 2 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0031" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0031" proc-id="RM23G0E___0000DKZ00000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (NO. 1 JUNCTION CONNECTOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G97 No. 1 junction connector connector.</ptxt>
<figure>
<graphic graphicname="C216814E15" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G97-6 (CANH) - G97-17 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 1 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0032" fin="false">OK</down>
<right ref="RM000002L8203OX_05_0033" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0032" proc-id="RM23G0E___0000DL000000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (NO. 1 JUNCTION CONNECTOR - NO. 2 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E16" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G97-1 (CANH) - G97-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 1 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0035" fin="true">OK</down>
<right ref="RM000002L8203OX_05_0034" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0033" proc-id="RM23G0E___0000DL100000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the G97 No. 1 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0008" proc-id="RM23G0E___0000DKS00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (ECM - NO. 1 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G56*1 or G58*2 ECM connector.</ptxt>
<figure>
<graphic graphicname="C215892E08" width="2.775699831in" height="2.775699831in"/>
</figure>
<list1 type="nonmark">
<item>
<ptxt>*1: for 1GR-FE</ptxt>
</item>
<item>
<ptxt>*2: for 1KD-FTV</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for 1GR-FE</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G56-32 (CANH) - G56-31 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1KD-FTV</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G58-35 (CANH) - G58-36 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1GR-FE</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 1KD-FTV</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for 1KD-FTV)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0029" fin="true">A</down>
<right ref="RM000002L8203OX_05_0036" fin="true">B</right>
<right ref="RM000002L8203OX_05_0028" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0027" proc-id="RM23G0E___0000DKY00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the G94 No. 3 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0033" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0010" proc-id="RM23G0E___0000DKT00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 4 JUNCTION CONNECTOR - COMBINATION METER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A73 No. 4 junction connector connector.</ptxt>
<figure>
<graphic graphicname="C216814E17" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A73-10 (CANH) - A73-21 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 4 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0011" fin="false">OK</down>
<right ref="RM000002L8203OX_05_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0011" proc-id="RM23G0E___0000DKU00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 4 JUNCTION CONNECTOR - NO. 3 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E18" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A73-9 (CANH) - A73-20 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 4 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0021" fin="true">OK</down>
<right ref="RM000002L8203OX_05_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0012" proc-id="RM23G0E___0000DKV00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the A73 No. 4 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0013" proc-id="RM23G0E___0000DKW00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (COMBINATION METER - NO. 4 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G6 combination meter assembly connector.</ptxt>
<figure>
<graphic graphicname="C198950E11" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G6-40 (CANH) - G6-39 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Combination Meter Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203OX_05_0014" fin="true">OK</down>
<right ref="RM000002L8203OX_05_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203OX_05_0014">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM000003QKY00MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0015">
<testtitle>REPAIR OR REPLACE CAN BRANCH WIRE CONNECTED TO DLC3 (CANH, CANL)</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0016">
<testtitle>REPLACE NO. 3 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0017">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE OR CONNECTOR (NO. 2 JUNCTION CONNECTOR - NO. 3 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0019">
<testtitle>REPLACE NO. 2 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0034">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE OR CONNECTOR (NO. 1 JUNCTION CONNECTOR - NO. 2 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0035">
<testtitle>REPLACE NO. 1 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0028">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE CONNECTED TO ECM (ECM - NO. 1 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0029">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0020">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE OR CONNECTOR (NO. 4 JUNCTION CONNECTOR - NO. 3 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0021">
<testtitle>REPLACE NO. 4 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0022">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE CONNECTED TO COMBINATION METER (COMBINATION METER - NO. 4 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203OX_05_0036">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>