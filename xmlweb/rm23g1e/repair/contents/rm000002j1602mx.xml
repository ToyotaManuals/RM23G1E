<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7B8TY_T003M" variety="T003M">
<name>POWER ASSIST SYSTEMS</name>
<para id="RM000002J1602MX" category="F" type-id="30027" name-id="SS2UY-51" from="201207">
<name>SERVICE DATA</name>
<subpara id="RM000002J1602MX_z0" proc-id="RM23G0E___000009J00000">
<content5 releasenbr="1">
<table pgwide="1">
<title>Power Steering Drive Belt (for 5L-E)</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" align="left" colwidth="3.54in"/>
<colspec colname="COL2" align="left" colwidth="3.54in"/>
<tbody>
<row>
<entry>
<ptxt>Drive belt deflection (New belt)</ptxt>
</entry>
<entry valign="middle">
<ptxt>7.5 to 9.5 mm (0.295 to 0.374 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Drive belt deflection (Used belt)</ptxt>
</entry>
<entry valign="middle">
<ptxt>9.0 to 13 mm (0.354 to 0.512 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Drive belt tension (New belt)</ptxt>
</entry>
<entry valign="middle">
<ptxt>441 to 539 N</ptxt>
<ptxt>(45 to 55 kgf, 99.1 to 121 lbf)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Drive belt tension (Used belt)</ptxt>
</entry>
<entry valign="middle">
<ptxt>245 to 343 N</ptxt>
<ptxt>(25 to 35 kgf, 55.1 to 77.1 lbf)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Power Steering Fluid</title>
<tgroup cols="2" align="left">
<colspec colname="COL1" align="left" colwidth="3.54in"/>
<colspec colname="COL2" align="left" colwidth="3.54in"/>
<tbody>
<row>
<entry>
<ptxt>Fluid level rise (Maximum)</ptxt>
</entry>
<entry valign="middle">
<ptxt>5.0 mm (0.197 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Fluid pressure at idle speed with valve closed</ptxt>
</entry>
<entry valign="middle">
<ptxt>8300 to 8800 kPa (84.7 to 89.7 kgf/cm<sup>2</sup>, 1204 to 1276 psi)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Fluid pressure at idle speed with valve fully open and turn steering wheel to full lock position</ptxt>
</entry>
<entry valign="middle">
<ptxt>8300 to 8800 kPa (84.7 to 89.7 kgf/cm<sup>2</sup>, 1204 to 1276 psi)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Vane Pump (for 1GR-FE, 1KD-FTV and 2TR-FE)</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<tbody>
<row>
<entry>
<ptxt>Vane pump shaft and vane pump housing oil clearance (Maximum)</ptxt>
</entry>
<entry>
<ptxt>0.07 mm (0.00276 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Vane plate thickness (Standard)</ptxt>
</entry>
<entry>
<ptxt>1.405 to 1.411 mm (0.0554 to 0.0555 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Clearance between vane pump rotor groove and vane pump plate (Maximum)</ptxt>
</entry>
<entry>
<ptxt>0.025 mm (0.00098 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Compression spring free length (Minimum)</ptxt>
</entry>
<entry>
<ptxt>29.2 mm (1.15 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Vane pump rotating torque</ptxt>
</entry>
<entry>
<ptxt>0.3 N*m (3 kgf*cm, 2 in.*lbf) or less</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Vane Pump (for 5L-E)</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<tbody>
<row>
<entry>
<ptxt>Vane pump shaft and vane pump housing oil clearance (Maximum)</ptxt>
</entry>
<entry>
<ptxt>0.07 mm (0.00276 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Vane plate thickness (Standard)</ptxt>
</entry>
<entry>
<ptxt>1.397 to 1.403 mm (0.0550 to 0.0552 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Clearance between vane pump rotor groove and vane pump plate (Maximum)</ptxt>
</entry>
<entry>
<ptxt>0.033 mm (0.0013 in.)</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Vane pump rotating torque</ptxt>
</entry>
<entry>
<ptxt>0.3 N*m (3 kgf*cm, 2 in.*lbf) or less</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Flow control valve compression spring length (Minimum)</ptxt>
</entry>
<entry>
<ptxt>32.3 mm (1.28 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>