<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S0023" variety="S0023">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S0023_7B9CL_T00M9" variety="T00M9">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM0000043K3006X" category="D" type-id="303F2" name-id="ED2MK-02" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM0000043K3006X_z0" proc-id="RM23G0E___0000FVA00000">
<content5 releasenbr="1">
<step1>
<ptxt>PRE-CRASH SAFETY SYSTEM DESCRIPTION</ptxt>
<ptxt>The pre-crash safety system detects vehicles and obstacles in front of the vehicle and performs the following: 1) optimally uses system controls to avoid crashes, and 2) uses system controls to reduce impact during a crash.</ptxt>
</step1>
<step1>
<ptxt>PRE-CRASH DETECTION OUTLINE</ptxt>
<step2>
<ptxt>Front crash detection</ptxt>
<ptxt>The driving support ECU receives information about vehicles and obstacles in front of the vehicle from the millimeter wave radar sensor. Then, based on the vehicle speed, road conditions and obstacle location, the driving support ECU determines, in advance, the risk of a crash and if a crash is unavoidable.</ptxt>
</step2>
<step2>
<ptxt>Sudden braking detection</ptxt>
<ptxt>The skid control ECU activates the pre-crash seat belts when it detects sudden braking. Sudden braking is determined based on brake fluid pressure and other information from the master cylinder pressure sensor.</ptxt>
</step2>
<step2>
<ptxt>Vehicle control impossible judgment</ptxt>
<ptxt>The pre-crash seat belts are activated when it is determined that there is a high probability that the vehicle cannot be controlled. The judgment is based on vehicle stability information from the yaw rate sensor and steering angle sensor.</ptxt>
</step2>
</step1>
<step1>
<ptxt>PRE-CRASH SAFETY SYSTEM CONTROL DESCRIPTION</ptxt>
<table pgwide="1">
<tgroup cols="6">
<colspec colname="COL1" colwidth="1.18in"/>
<colspec colname="COL2" colwidth="1.18in"/>
<colspec colname="COL3" colwidth="1.18in"/>
<colspec colname="COL4" colwidth="1.18in"/>
<colspec colname="COL5" colwidth="1.18in"/>
<colspec colname="COL6" colwidth="1.18in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Operation Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sender ECU</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Receiver ECU</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Part Operated</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="5" valign="middle">
<ptxt>Front crash detected</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="ordered">
<item>
<ptxt>Crash is possible</ptxt>
</item>
<item>
<ptxt>Crash is unavoidable</ptxt>
</item>
</list1>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Pre-crash alarm</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Driving support ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Combination meter assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Combination meter assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Skid control ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Skid control buzzer</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Crash is unavoidable</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pre-crash seat belts</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driving support ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Seat belt control ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front seat outer belts</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="ordered">
<item>
<ptxt>Crash is possible</ptxt>
</item>
<item>
<ptxt>Crash is unavoidable</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Pre-crash brake assist</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Driving support ECU</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Skid control ECU</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Brake assist system</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Crash is unavoidable</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pre-crash brake</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<list1 type="ordered">
<item>
<ptxt>Crash is possible</ptxt>
</item>
<item>
<ptxt>Crash is unavoidable</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Air suspension control*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driving support ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Suspension Control ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shock absorber</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Sudden braking detected</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Pre-crash seat belts</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Skid control ECU</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Seat belt control ECU</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Front seat outer belts</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Vehicle control impossible judgment</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: w/ AVS</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>PRE-CRASH ALARM CONTROL</ptxt>
<ptxt>When the system determines that there is a risk of colliding with an obstacle in front of the vehicle, the multi-information display and warning buzzer are used to warn the driver and help with crash avoidance.</ptxt>
</step1>
<step1>
<ptxt>PRE-CRASH SEAT BELT CONTROL</ptxt>
<ptxt>If the pre-crash sensor detects that a collision is unavoidable, the pre-crash safety system retracts the seat belt, thus enhancing the effectiveness of the seat belt pretensioner in a crash. The same happens if the driver makes an emergency braking or loses control of the vehicle.</ptxt>
</step1>
<step1>
<ptxt>PRE-CRASH BRAKE ASSIST CONTROL</ptxt>
<ptxt>When the system determines that there is a risk of colliding with an obstacle in front of the vehicle, brake assist is set to standby mode. Then, when the driver depresses the brake pedal, the hydraulic brake pressure is increased to improve braking effectiveness.</ptxt>
</step1>
<step1>
<ptxt>PRE-CRASH BRAKE CONTROL</ptxt>
<ptxt>When the system determines that a crash with an obstacle is unavoidable, the system performs brake control to reduce the vehicle speed.</ptxt>
</step1>
<step1>
<ptxt>AIR SUSPENSION CONTROL</ptxt>
<ptxt>When it is determined that there is a risk of colliding with an obstacle in front of the vehicle, the AVS is operated to control the damping force, suppress nose diving and rolling, and support collision avoidance through steering.</ptxt>
</step1>
<step1>
<ptxt>PRE-CRASH BRAKE CANCEL SWITCH ASSEMBLY</ptxt>
<ptxt>When the pre-crash brake cancel switch assembly in the No. 1 instrument panel under cover sub-assembly*1 or No. 2 instrument panel under cover sub-assembly*2 is turned on, the pre-crash alarm and pre-crash brake do not operate.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
<atten4>
<ptxt>While the pre-crash brake cancel switch assembly is on, the PCS indicator light in the combination meter assembly illuminates.</ptxt>
</atten4>
</step1>
<step1>
<ptxt>PRE-CRASH SAFETY SYSTEM OPERATING CONDITION </ptxt>
</step1>
<table pgwide="1">
<tgroup cols="7">
<colspec colname="COL1" colwidth="1.01in"/>
<colspec colname="COL2" colwidth="1.01in"/>
<colspec colname="COL3" colwidth="1.01in"/>
<colspec colname="COL4" colwidth="1.01in"/>
<colspec colname="COL5" colwidth="1.01in"/>
<colspec colname="COL6" colwidth="1.01in"/>
<colspec colname="COLSPEC0" colwidth="1.02in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pre-crash Alarm</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pre-crash Seat Belt</ptxt>
<ptxt>(Linked with Understeer or Oversteer Tendency Operation and Sudden Brake Operation)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pre-crash Seat Belt</ptxt>
<ptxt>(Linked with Radar)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pre-crash Brake Assist</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Pre-crash Brake Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Air Suspension Control*3</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Ignition switch is off or ACC</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pre-crash brake cancel switch assembly is on</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Warning message is displayed on multi-information display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Seat belt(s) are fastened</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle speed is 30 km/h (18 mph) or more, regardless of relative speed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle speed is 15 km/h (9 mph) or more and relative speed is 15 km/h (9 mph)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle speed is 5 km/h (3 mph) or more and relative speed is 30 km/h (18 mph)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle speed is 30 km/h (18 mph) or more and relative speed is 30 km/h (18 mph)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle collides with object outside detection range of millimeter wave radar sensor assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Millimeter wave radar sensor assembly cannot detect possible collision object</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>X</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>○: Operation</ptxt>
</item>
<item>
<ptxt>X: No operation</ptxt>
</item>
<item>
<ptxt>*1: Inoperative functions are different depending on malfunction areas</ptxt>
</item>
<item>
<ptxt>*2: Pre-crash seat belts may operate when brakes are suddenly applied</ptxt>
</item>
<item>
<ptxt>*3: w/ AVS</ptxt>
</item>
</list1>
<step1>
<ptxt>PRECAUTION FOR MILLIMETER WAVE RADAR SENSOR ASSEMBLY</ptxt>
<step2>
<ptxt>When the millimeter wave radar sensor assembly detects an object in front of the vehicle under the conditions described below, the driving support ECU may determine that a collision is unavoidable and activate the pre-crash safety system. However, this is not abnormal.</ptxt>
<figure>
<graphic graphicname="E194238E01" width="7.106578999in" height="7.795582503in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>The pre-crash safety system is not activated by safety cones and other plastic objects which cannot be detected by the millimeter wave radar sensor assembly.</ptxt>
</item>
<item>
<ptxt>The pre-crash safety system may or may not be activated by the following objects which cannot always be detected by the millimeter wave radar sensor assembly: people, bicycles, motorcycles, trees, animals, snow fences, etc.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Object Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Example</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Object that cannot be detected</ptxt>
</entry>
<entry valign="middle">
<ptxt>Safety cones, plastic items</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Object that cannot always be detected</ptxt>
</entry>
<entry valign="middle">
<ptxt>People, bicycles, motorcycles, trees, animals, snow fences, etc.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>