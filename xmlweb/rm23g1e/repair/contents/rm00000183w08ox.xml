<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12040_S001O" variety="S001O">
<name>AUDIO / VIDEO</name>
<ttl id="12040_S001O_7B98Z_T00IN" variety="T00IN">
<name>AUDIO AND VISUAL SYSTEM (w/o Navigation System)</name>
<para id="RM00000183W08OX" category="C" type-id="8011N" name-id="AV9OW-03" from="201210">
<dtccode>01-DD</dtccode>
<dtcname>Master Reset</dtcname>
<dtccode>01-E1</dtccode>
<dtcname>Voice Processing Device ON Error</dtcname>
<subpara id="RM00000183W08OX_03" type-id="60" category="03" proc-id="RM23G0E___0000BKV00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.34in"/>
<colspec colname="COL2" colwidth="2.34in"/>
<colspec colname="COL3" colwidth="2.40in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>01-DD*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>The device that should be the master has been disconnected after engine start.</ptxt>
</entry>
<entry morerows="1" valign="middle">
<list1 type="unordered">
<item>
<ptxt>Radio receiver power source circuit</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>AVC-LAN circuit between the radio receiver assembly and the component which has stored this code</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Radio receiver assembly</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Component which has stored this code</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>01-E1*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>The AMP device records that the AMP output does not function even while the source device operates.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>*1: This code may be stored if the engine is started and the ignition switch is turned to the start position again.</ptxt>
</item>
<item>
<ptxt>*2: Even if no fault is present, this trouble code may be stored depending on the battery condition or engine start voltage.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Before starting troubleshooting, be sure to clear DTCs to clear codes stored due to the reasons described in Hint above. Then, check for DTCs and troubleshoot according to the output DTCs.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>The radio receiver assembly is the master unit.</ptxt>
</item>
</list1>
<list1 type="unordered">
<item>
<ptxt>Be sure to clear and recheck DTCs after the inspection is completed to confirm that no DTCs are output.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM00000183W08OX_05" type-id="32" category="03" proc-id="RM23G0E___0000BL200001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E195968E04" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000183W08OX_01" type-id="51" category="05" proc-id="RM23G0E___0000BKU00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Be sure to read Description before performing the following procedures.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM00000183W08OX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000183W08OX_04_0001" proc-id="RM23G0E___0000BKW00001">
<testtitle>CHECK RADIO RECEIVER POWER SOURCE CIRCUIT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Refer to Radio Receiver Power Source Circuit (See page <xref label="Seep01" href="RM0000012CI0IWX"/>).</ptxt>
<atten4>
<ptxt>If the power source circuit is operating normally, proceed to the next step.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000183W08OX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000183W08OX_04_0002" proc-id="RM23G0E___0000BKX00001">
<testtitle>INSPECT RADIO RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the H38* and H36 radio receiver assembly connectors.</ptxt>
<figure>
<graphic graphicname="E145834E32" width="2.775699831in" height="3.779676365in"/>
</figure>
<list1 type="nonmark">
<item>
<ptxt>*: for 9 Speakers</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>H38-5 (ATX+) - H38-15 (ATX-)*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>H36-9 (TXM+) - H36-10 (TXM-)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>60 to 80 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<list1 type="nonmark">
<item>
<ptxt>*: for 9 Speakers</ptxt>
</item>
</list1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>for 9 Speakers</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/o Accessory Meter)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (w/ Accessory Meter)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000183W08OX_04_0009" fin="false">A</down>
<right ref="RM00000183W08OX_04_0004" fin="true">B</right>
<right ref="RM00000183W08OX_04_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000183W08OX_04_0009" proc-id="RM23G0E___0000BKZ00001">
<testtitle>IDENTIFY COMPONENT WHICH HAS STORED THIS CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter the diagnostic mode.</ptxt>
<figure>
<graphic graphicname="E146440E10" width="7.106578999in" height="6.791605969in"/>
</figure>
</test1>
<test1>
<ptxt>Press the preset switch "channel 3" to change to "Detailed Information Mode".</ptxt>
</test1>
<test1>
<ptxt>Identify the component which has stored this code.</ptxt>
<table>
<title>Component Table</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Physical Address</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Radio receiver assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>190</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>"Bluetooth" handsfree module</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>19D</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Stereo component amplifier assembly*</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>440</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Multi-media interface ECU</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>388</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: for 9 Speakers</ptxt>
</item>
</list1>
</test1>
<atten4>
<list1 type="unordered">
<item>
<ptxt>"440 (stereo component amplifier)" is the component which has stored this code in the example shown in the illustration.</ptxt>
</item>
<item>
<ptxt>For details of the DTC display, refer to DTC Check/Clear (See page <xref label="Seep01" href="RM0000014CZ0CBX"/>).</ptxt>
</item>
</list1>
</atten4>
</content6>
<res>
<down ref="RM00000183W08OX_04_0010" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000183W08OX_04_0010" proc-id="RM23G0E___0000BL000001">
<testtitle>CHECK COMPONENT WHICH HAS STORED THIS CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Select the component which has stored this code.</ptxt>
<atten4>
<ptxt>The "Bluetooth" handsfree module is built into the radio receiver assembly. If there is a problem between the "Bluetooth" handsfree module and radio receiver assembly, replace the radio receiver assembly.</ptxt>
</atten4>
<table>
<title>Component Table</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Except "Bluetooth" handsfree module and satellite radio tuner</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Bluetooth" handsfree module (19D) (w/o Accessory Meter)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Bluetooth" handsfree module (19D) (w/ Accessory Meter)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM00000183W08OX_04_0003" fin="false">A</down>
<right ref="RM00000183W08OX_04_0004" fin="true">B</right>
<right ref="RM00000183W08OX_04_0006" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM00000183W08OX_04_0003" proc-id="RM23G0E___0000BKY00001">
<testtitle>CHECK HARNESS AND CONNECTOR (RADIO RECEIVER - COMPONENT WHICH HAS STORED THIS CODE)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>For details of connectors, refer to Terminals of ECU (See page <xref label="Seep01" href="RM0000012A70CHX"/>).</ptxt>
</atten4>
<test1>
<ptxt>Referring to the wiring diagram, check the AVC-LAN circuit between the radio receiver assembly and the component which has stored this code.</ptxt>
<test2>
<ptxt>Disconnect all connectors between the radio receiver assembly and the component which has stored this code.</ptxt>
</test2>
<test2>
<ptxt>Check for an open or short in the AVC-LAN circuit between the radio receiver assembly and the component which has stored this code.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There is no open or short circuit.</ptxt>
</specitem>
</spec>
</test2>
</test1>
</content6>
<res>
<down ref="RM00000183W08OX_04_0013" fin="false">OK</down>
<right ref="RM00000183W08OX_04_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000183W08OX_04_0013" proc-id="RM23G0E___0000BL100001">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the radio receiver assembly.</ptxt>
<list1 type="unordered">
<item>
<ptxt>w/o Accessory Meter: See page <xref label="Seep01" href="RM000003AHY01QX_01_0011"/>
</ptxt>
</item>
<item>
<ptxt>w/ Accessory Meter: See page <xref label="Seep02" href="RM000003AHY01QX_01_0028"/>
</ptxt>
</item>
</list1>
<spec>
<title>OK</title>
<specitem>
<ptxt>The same problem does not occur.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM00000183W08OX_04_0005" fin="true">OK</down>
<right ref="RM00000183W08OX_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM00000183W08OX_04_0004">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01QX_01_0011"/>
</testtitle>
</testgrp>
<testgrp id="RM00000183W08OX_04_0006">
<testtitle>REPLACE RADIO RECEIVER ASSEMBLY<xref label="Seep01" href="RM000003AHY01QX_01_0028"/>
</testtitle>
</testgrp>
<testgrp id="RM00000183W08OX_04_0007">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM00000183W08OX_04_0005">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM00000183W08OX_04_0008">
<testtitle>REPLACE COMPONENT WHICH HAS STORED THIS CODE</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>