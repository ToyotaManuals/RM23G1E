<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S002D" variety="S002D">
<name>WIPER / WASHER</name>
<ttl id="12068_S002D_7B9GP_T00QD" variety="T00QD">
<name>WASHER MOTOR (for Rear Side)</name>
<para id="RM000002XK504UX" category="A" type-id="80001" name-id="WW3YQ-03" from="201210">
<name>REMOVAL</name>
<subpara id="RM000002XK504UX_01" type-id="01" category="01">
<s-1 id="RM000002XK504UX_01_0022" proc-id="RM23G0E___0000IWU00001">
<ptxt>REMOVE RADIATOR GRILLE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the radiator grille (See page <xref label="Seep01" href="RM0000038K700WX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002XK504UX_01_0023" proc-id="RM23G0E___0000J2M00001">
<ptxt>REMOVE FRONT BUMPER COVER
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Put protective tape around the front bumper cover.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts labeled A and 2 bolts labeled B.</ptxt>
</s2>
<s2>
<ptxt>Remove the 6 screws and 6 clips.</ptxt>
<figure>
<graphic graphicname="B236374E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bolt B</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Detach the 12 claws.</ptxt>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Disconnect the 3 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/ TOYOTA Parking Assist-sensor System, w/o Fog Light:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>w/o TOYOTA Parking Assist-sensor System, w/ Fog Light:</ptxt>
<ptxt>Disconnect the 2 connectors.</ptxt>
</s2>
<s2>
<ptxt>w/ Headlight Cleaner System:</ptxt>
<ptxt>Disconnect the headlight cleaner hose.</ptxt>
</s2>
<s2>
<ptxt>Remove the front bumper cover.</ptxt>
<figure>
<graphic graphicname="B236375E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" align="left" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" align="left" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM000002XK504UX_01_0026">
<ptxt>REMOVE FRONT WHEEL</ptxt>
</s-1>
<s-1 id="RM000002XK504UX_01_0027" proc-id="RM23G0E___000075O00000">
<ptxt>REMOVE FRONT FENDER MUDGUARD RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B236298" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 clips and 4 screws.</ptxt>
</s2>
<s2>
<ptxt>Remove the front fender mudguard.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000002XK504UX_01_0028" proc-id="RM23G0E___0000IWK00001">
<ptxt>REMOVE FRONT FENDER SPLASH SHIELD SUB-ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 screws from the front fender splash shield sub-assembly.</ptxt>
<figure>
<graphic graphicname="E197539" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the 13 grommets and remove the front fender splash shield sub-assembly.</ptxt>
<atten4>
<ptxt>The grommets need to be replaced with new ones because they will break when they are removed.</ptxt>
</atten4>
<figure>
<graphic graphicname="E197540" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002XK504UX_01_0029" proc-id="RM23G0E___0000IWI00001">
<ptxt>DRAIN WASHER FLUID
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the washer hose from the hose of the windshield washer motor and pump assembly, and then drain the washer fluid.</ptxt>
<figure>
<graphic graphicname="E197529" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002XK504UX_01_0030" proc-id="RM23G0E___0000IWN00001">
<ptxt>REMOVE WASHER INLET SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Detach the claw and remove the washer inlet sub-assembly.</ptxt>
<figure>
<graphic graphicname="E197530" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002XK504UX_01_0031" proc-id="RM23G0E___0000IWL00001">
<ptxt>REMOVE WINDSHIELD WASHER JAR ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o Headlight Cleaner System:</ptxt>
<ptxt>Disconnect the connector.</ptxt>
<figure>
<graphic graphicname="E198971" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>w/ Headlight Cleaner System:</ptxt>
<ptxt>Disconnect the 3 connectors.</ptxt>
<figure>
<graphic graphicname="E202321" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the 3 screws.</ptxt>
<figure>
<graphic graphicname="E198972" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
<s2>
<ptxt>Detach the guide and remove the washer jar.</ptxt>
<figure>
<graphic graphicname="E198973" width="2.775699831in" height="2.775699831in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM000002XK504UX_01_0004" proc-id="RM23G0E___0000IWT00001">
<ptxt>REMOVE REAR WASHER MOTOR AND PUMP ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the washer motor and pump from the packing of the washer jar.</ptxt>
<figure>
<graphic graphicname="E197546" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>