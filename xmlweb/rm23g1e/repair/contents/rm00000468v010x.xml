<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12060_S0025" variety="S0025">
<name>SEAT BELT</name>
<ttl id="12060_S0025_7B9DS_T00NG" variety="T00NG">
<name>REAR CENTER SEAT OUTER BELT ASSEMBLY (for 60/40 Split Double-folding Seat Type RH Side)</name>
<para id="RM00000468V010X" category="A" type-id="80001" name-id="SB45F-02" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM00000468V010X_02" type-id="11" category="10" proc-id="RM23G0E___0000H6500000">
<content3 releasenbr="1">
<atten2>
<ptxt>Wear protective gloves. Sharp areas on the parts may injure your hands.</ptxt>
</atten2>
</content3>
</subpara>
<subpara id="RM00000468V010X_01" type-id="01" category="01">
<s-1 id="RM00000468V010X_01_0001" proc-id="RM23G0E___0000H6400000">
<ptxt>REMOVE REAR SEAT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the rear No. 1 seat assembly RH (See page <xref label="Seep01" href="RM00000468U00JX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000468V010X_01_0046" proc-id="RM23G0E___0000GHL00000">
<ptxt>REMOVE NO. 1 SEATBACK BOARD SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238055" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a clip remover, detach the 3 clips.</ptxt>
</s2>
<s2>
<ptxt>Detach the guide and remove the board.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0048" proc-id="RM23G0E___0000GHM00000">
<ptxt>REMOVE REAR SEAT INNER RECLINING COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238056" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 guides and remove the cover. </ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0049" proc-id="RM23G0E___0000GII00000">
<ptxt>REMOVE REAR CENTER SEAT INNER BELT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241736E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the fastening tape and remove the band.</ptxt>
</s2>
<s2>
<ptxt>Remove the bolt and inner belt.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Band</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0050" proc-id="RM23G0E___0000GHN00000">
<ptxt>REMOVE CENTER SEATBACK COVER SUB-ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238058" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 guides and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0051" proc-id="RM23G0E___0000GHO00000">
<ptxt>REMOVE CENTER SEATBACK ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238059" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T45 "TORX" socket wrench, remove the "TORX" bolt and bush.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and center seatback.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0053" proc-id="RM23G0E___0000GI200000">
<ptxt>REMOVE CENTER NO. 2 SEATBACK COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238074" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 claws and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0054" proc-id="RM23G0E___0000GI300000">
<ptxt>REMOVE REAR SEATBACK LOCK COVER RH
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238025" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 screws.</ptxt>
</s2>
<s2>
<ptxt>Detach the 2 guides and remove the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0055" proc-id="RM23G0E___0000GI400000">
<ptxt>REMOVE REAR SEATBACK LOCK ASSEMBLY RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 2 hog rings.</ptxt>
<figure>
<graphic graphicname="B238080" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<figure>
<graphic graphicname="B238081" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>Remove the 2 bolts.</ptxt>
</s2>
<s2>
<ptxt>While moving the release button in the direction of the arrow in the illustration, detach it from the grommet and remove the seatback lock.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0056" proc-id="RM23G0E___0000GI600000">
<ptxt>REMOVE REAR SEATBACK STOP BUTTON GROMMET
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238027" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 3 claws and remove the grommet.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0057" proc-id="RM23G0E___0000GI700000">
<ptxt>REMOVE REAR NO. 1 SEAT HEADREST SUPPORT ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238003" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the 4 claws and remove the 2 supports.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0058" proc-id="RM23G0E___0000GI800000">
<ptxt>REMOVE SEAT BELT ANCHOR COVER CAP
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238075E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a screwdriver, detach the 2 claws and 2 guides, and then remove the cap.</ptxt>
<atten4>
<ptxt>Tape the screwdriver tip before use.</ptxt>
</atten4>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Protective Tape</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0059" proc-id="RM23G0E___0000GI900000">
<ptxt>REMOVE REAR SEAT SHOULDER BELT COVER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238077" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the screw.</ptxt>
</s2>
<s2>
<ptxt>Detach the 3 claws and remove the cover.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the outer belt from the cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0060" proc-id="RM23G0E___0000GIA00000">
<ptxt>REMOVE REAR SEATBACK LOCK STRIKER PROTECTOR
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238078" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the protector from the striker.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0061" proc-id="RM23G0E___0000GIB00000">
<ptxt>REMOVE REAR SEAT LOCK STRIKER
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238079" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Using a T45 "TORX" socket wrench, remove the striker.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0062" proc-id="RM23G0E___0000GIC00000">
<ptxt>REMOVE SEATBACK COVER WITH PAD
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238083E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<s2>
<ptxt>Remove the hog rings, detach the hook, and then remove the seatback cover with pad.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hook</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hog Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Disconnect the outer belt from the seatback cover with pad.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0063" proc-id="RM23G0E___0000GIE00000">
<ptxt>REMOVE REAR SEATBACK EDGE PROTECTOR
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the protector from the seatback frame.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0064" proc-id="RM23G0E___0000GIF00000">
<ptxt>REMOVE REAR SEAT SHOULDER BELT GUIDE
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B238085" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Detach the claw and guide, and then remove the belt guide.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM00000468V010X_01_0043" proc-id="RM23G0E___0000GIJ00000">
<ptxt>REMOVE NO. 1 SEAT 3 POINT TYPE BELT ASSEMBLY RH</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="B241741" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the nut and bolt, and then detach the 2 claws and remove the seat belt.</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>