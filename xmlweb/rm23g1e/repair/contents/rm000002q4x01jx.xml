<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S001B" variety="S001B">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S001B_7B96L_T00G9" variety="T00G9">
<name>AIR SUSPENSION SYSTEM</name>
<para id="RM000002Q4X01JX" category="S" type-id="3001E" name-id="SC0EU-36" from="201207" to="201210">
<name>DIAGNOSTIC TROUBLE CODE CHART</name>
<subpara id="RM000002Q4X01JX_z0" proc-id="RM23G0E___00009LG00000">
<content5 releasenbr="1">
<atten3>
<ptxt>Turn the ignition switch off before removing parts.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If no abnormality is found when inspecting parts, inspect the suspension control ECU and ground points for poor contact.</ptxt>
</item>
<item>
<ptxt>If a trouble code is output during the DTC check, check the trouble areas listed for the DTC. For details of each code, refer to the "See page" for the respective "DTC Code" in the DTC chart.</ptxt>
</item>
<item>
<ptxt>When 2 or more DTCs are output, perform circuit inspections one by one until the problem is identified.</ptxt>
</item>
<item>
<ptxt>Inspect the fuse and relay before investigating the trouble areas as shown in the table below.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Air Suspension System</title>
<tgroup cols="4" colsep="1" rowsep="1" align="center">
<colspec colnum="1" colname="1" colwidth="1.42in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="2.83in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.42in" colsep="0"/>
<colspec colnum="4" colname="4" colwidth="1.41in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Detection Item</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Warning Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1713/13</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Rear Height Control Sensor RH Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9400PX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1714/14</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Rear Height Control Sensor LH Circuit Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9400PX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1715/15</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Front Acceleration Sensor RH Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE601VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1716/16</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Front Acceleration Sensor LH Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE601VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1717/17</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Rear Acceleration Sensor Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE601VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1731/31</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>FR Damping Force Control Actuator Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE801GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1732/32</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>FL Damping Force Control Actuator Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE801GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1733/33</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>RR Damping Force Control Actuator Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE801GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1734/34</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>RL Damping Force Control Actuator Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE801GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1745/45</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Exhaust Solenoid</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9500UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1746/46</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Gate Solenoid Valve Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9C012X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1747/47</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Leveling Solenoid Valve Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9D00UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1751/51</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Compressor Relay Coil Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9F012X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1752/52</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Compressor Motor Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N96012X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1761/61</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Continuous Current to Compressor Motor</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PEE01HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1762/62</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Continuous Current to Exhaust Solenoid</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PEF01DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1781/81</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Suspension Control ECU Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9B012X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1782/82</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Low Battery Positive Voltage</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N93012X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1783/83</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Front Wheels Speed Sensor Signal Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PEJ023X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1784/84</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Steering Sensor Signal Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PEK01EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1787/87</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>(Horizontality) G Sensor Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PEM01SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1789/89</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Vehicle Identification Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000003RP201GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1791/91</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Adaptive Variable Suspension (AVS) Switch Circuit (Test Mode DTC)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Test Mode DTC</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000003FV900JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1792/92</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Height Control Switch Circuit (Test Mode DTC)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Test Mode DTC</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9A010X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1794/94</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Height Control OFF Switch Circuit (Test Mode DTC)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Test Mode DTC</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001SGX00VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1796/96</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>(Up &amp; Down) G Sensor FR</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Test Mode DTC</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE601VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1797/97</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>(Up &amp; Down) G Sensor FL</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Test Mode DTC</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE601VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>C1798/98</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>(Up &amp; Down) G Sensor Rear</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Test Mode DTC</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002PE601VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>U0100/71</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with ECM/PCM "A"</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9E00SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>U0114/71</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Four-Wheel Drive Clutch Control Module</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9E00SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>U0122/71</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Vehicle Dynamics Control Module</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9E00SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>U0124/71</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Lateral Acceleration Sensor Module</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9E00SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>U0126/71</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Steering Angle Sensor Module</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9E00SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle">
<ptxt>U0140/71</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Body Control Module</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle">
<ptxt>Not indicated</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle">
<ptxt>
<xref href="RM000001N9E00SX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="unordered">
<item>
<ptxt>*1: The system may store DTC C1761/61 even when the system is normal if one of the following occurs: 1) the vehicle is full of passengers and the luggage compartment is full; 2) the vehicle is being driven or is stopped on an irregular surface; or 3) the air ducts are frozen.</ptxt>
</item>
<item>
<ptxt>*2: The system may store DTC C1762/62 even when the system is normal if one of the following occurs: 1) the vehicle is being driven or is stopped on an irregular surface; or 2) the air tubes are clogged with snow or are frozen. If DTC C1762/62 is stored, vehicle height control stops. However, if the ignition switch is turned off and then to ON, vehicle height control starts.</ptxt>
</item>
</list1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>