<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12069_S002E" variety="S002E">
<name>LIGHTING (EXT)</name>
<ttl id="12069_S002E_7B9GZ_T00QN" variety="T00QN">
<name>LIGHTING SYSTEM</name>
<para id="RM000001A7L07FX" category="J" type-id="303SY" name-id="LE29D-04" from="201207" to="201210">
<dtccode/>
<dtcname>Headlight (HI-BEAM) Circuit</dtcname>
<subpara id="RM000001A7L07FX_01" type-id="60" category="03" proc-id="RM23G0E___0000J1D00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The main body ECU receives headlight dimmer switch information signals and illuminates the high beam headlights.</ptxt>
</content5>
</subpara>
<subpara id="RM000001A7L07FX_02" type-id="32" category="03" proc-id="RM23G0E___0000J1E00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E199761E05" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000001A7L07FX_03" type-id="51" category="05" proc-id="RM23G0E___0000J1F00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Inspect the fuses and bulbs for circuits related to this system before performing the following inspection procedure.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000001A7L07FX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000001A7L07FX_05_0001" proc-id="RM23G0E___0000J1G00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (HEADLIGHT)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Using the intelligent tester, perform the Active Test (See page <xref label="Seep01" href="RM000000PKV0BUX"/>).</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4" align="left">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Head Light Hi</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight dimmer relay</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON/OFF</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Relay operates (high beam headlights illuminate).</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001A7L07FX_05_0020" fin="true">OK</down>
<right ref="RM000001A7L07FX_05_0028" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000001A7L07FX_05_0020">
<testtitle>PROCEED TO NEXT SUSPECTED AREA SHOWN IN PROBLEM SYMPTOMS TABLE<xref label="Seep01" href="RM000002CE20DJX"/>
</testtitle>
</testgrp>
<testgrp id="RM000001A7L07FX_05_0028" proc-id="RM23G0E___0000J1H00000">
<testtitle>INSPECT HEADLIGHT DIMMER RELAY (H-LP HI)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the headlight dimmer relay from the engine room relay block.</ptxt>
<figure>
<graphic graphicname="E108359E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage not applied between terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3 - 5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Battery voltage applied between terminals 1 and 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001A7L07FX_05_0029" fin="false">OK</down>
<right ref="RM000001A7L07FX_05_0034" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001A7L07FX_05_0029" proc-id="RM23G0E___0000J1I00000">
<testtitle>CHECK HARNESS AND CONNECTOR (BATTERY - HEADLIGHT DIMMER RELAY [H-LP HI])</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="E199762E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer relay terminal 1 - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer relay terminal 5 - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Headlight Dimmer Relay)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000001A7L07FX_05_0030" fin="false">OK</down>
<right ref="RM000001A7L07FX_05_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001A7L07FX_05_0030" proc-id="RM23G0E___0000J1J00000">
<testtitle>CHECK HARNESS AND CONNECTOR (HEADLIGHT DIMMER RELAY [H-LP HI] - MAIN BODY ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G64 main body ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Headlight dimmer relay terminal 2 - G64-15 (DIM)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G64-15 (DIM) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000001A7L07FX_05_0020" fin="true">OK</down>
<right ref="RM000001A7L07FX_05_0011" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000001A7L07FX_05_0034">
<testtitle>REPLACE HEADLIGHT DIMMER RELAY</testtitle>
</testgrp>
<testgrp id="RM000001A7L07FX_05_0011">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>