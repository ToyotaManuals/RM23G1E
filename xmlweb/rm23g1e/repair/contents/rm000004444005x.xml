<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="V1">
<name>General</name>
<section id="12003_S0003" variety="S0003">
<name>SPECIFICATIONS</name>
<ttl id="12003_S0003_7B8TH_T0035" variety="T0035">
<name>1KD-FTV EMISSION CONTROL</name>
<para id="RM000004444005X" category="F" type-id="30027" name-id="SS4MG-03" from="201207">
<name>SERVICE DATA</name>
<subpara id="RM000004444005X_z0" proc-id="RM23G0E___000008N00000">
<content5 releasenbr="1">
<table pgwide="1">
<title>EMISSION CONTROL SYSTEM</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vacuum pump assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Standard negative pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle">
<ptxt>Higher than 87 kPa (650 mmHg, 25.6 in.Hg)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>EGR VALVE (w/ DPF)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>5 (M-) - 4 (M+)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>25°C (77°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>0.3 to 100 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>EGR VALVE (w/o DPF)</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 (VC) - 2 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4.0 to 6.0 kΩ</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>3 (EGLS) - 2 (E2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
<ptxt>EGR valve is fully opened</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3.9 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
<ptxt>EGR valve is fully closed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1.0 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>EXHAUST GAS TEMPERATURE SENSOR</title>
<tgroup cols="4">
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Exhaust gas temperature sensor</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>50°C (122°F)</ptxt>
</entry>
<entry morerows="2" valign="middle" align="center">
<ptxt>82 to 137 kΩ</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No. 2 exhaust gas temperature sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No. 3 exhaust gas temperature sensor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>EXHAUST FUEL ADDITION INJECTOR</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>7.1 to 7.9 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>VACUUM REGULATING VALVE</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 13 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>VACUUM SWITCHING VALVE (w/ DPF)</title>
<tgroup cols="4">
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>No. 1 vacuum switching valve (for EGR Bypass Valve)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>33 to 39 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>No. 2 vacuum switching valve (for EGR Bypass Valve)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>VACUUM SWITCHING VALVE (w/o DPF)</title>
<tgroup cols="4">
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Vacuum switching valve (for EGR Cut)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>37 to 44 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vacuum switching valve (for EGR Bypass Valve)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>