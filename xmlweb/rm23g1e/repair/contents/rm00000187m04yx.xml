<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM00000187M04YX" category="C" type-id="302H8" name-id="ESMGN-27" from="201207" to="201210">
<dtccode>P0115</dtccode>
<dtcname>Engine Coolant Temperature Circuit</dtcname>
<dtccode>P0117</dtccode>
<dtcname>Engine Coolant Temperature Circuit Low Input</dtcname>
<dtccode>P0118</dtccode>
<dtcname>Engine Coolant Temperature Circuit High Input</dtcname>
<subpara id="RM00000187M04YX_01" type-id="60" category="03" proc-id="RM23G0E___000028O00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>A thermistor is built into the engine coolant temperature sensor and it changes its resistance value according to the engine coolant temperature.</ptxt>
<ptxt>The structure of the sensor and connection to the ECM is the same as that of the intake air temperature sensor.</ptxt>
<atten4>
<ptxt>If DTC P0115, P0117 or P0118 is stored, the ECM operates the fail-safe function in which the engine coolant temperature is assumed to be 80°C (176°F).</ptxt>
</atten4>
<table pgwide="1">
<title>P0115</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 0.5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open or short in the engine coolant temperature sensor circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open or short in engine coolant temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0117</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 0.5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Short in the engine coolant temperature sensor circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Short in engine coolant temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>P0118</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Detection Drive Pattern</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Engine switch on (IG) for 0.5 seconds</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open in the engine coolant temperature sensor circuit for 0.5 seconds (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Open in engine coolant temperature sensor circuit</ptxt>
</item>
<item>
<ptxt>Engine coolant temperature sensor</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Related Data List</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Data List</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P0115</ptxt>
</entry>
<entry morerows="2" valign="middle">
<ptxt>Coolant Temp</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0117</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0118</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If DTC P0115, P0117 and/or P0118 is stored, the following symptoms may appear:</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Misfire</ptxt>
</item>
<item>
<ptxt>Combustion noise</ptxt>
</item>
<item>
<ptxt>Black smoke</ptxt>
</item>
<item>
<ptxt>White smoke</ptxt>
</item>
<item>
<ptxt>Lack of power</ptxt>
</item>
</list2>
<item>
<ptxt>When DTC P0115, P0117 and/or P0118 is stored, check the engine coolant temperature by entering the following menus: Powertrain / Engine and ECT / Data List / Coolant Temp.</ptxt>
<table pgwide="1">
<title>Reference</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Temperature Displayed</ptxt>
</entry>
<entry valign="middle">
<ptxt>Malfunction</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open circuit</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle">
<ptxt>Short circuit</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187M04YX_02" type-id="32" category="03" proc-id="RM23G0E___000028P00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A210074E14" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM00000187M04YX_03" type-id="51" category="05" proc-id="RM23G0E___000028Q00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK062X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See Page <xref label="Seep04" href="RM0000012XK062X"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM00000187M04YX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM00000187M04YX_04_0001" proc-id="RM23G0E___000028R00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (ENGINE COOLANT TEMPERATURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Coolant Temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>70 to 90°C (158 to 194°F) with warm engine</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>-40°C (-40°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>140°C (284°F) or higher</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>70 to 90°C (158 to 194°F)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If there is an open circuit, the tester indicates -40°C (-40°F).</ptxt>
</item>
<item>
<ptxt>If there is a short circuit, the tester indicates 140°C (284°F) or higher.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM00000187M04YX_04_0002" fin="false">A</down>
<right ref="RM00000187M04YX_04_0004" fin="false">B</right>
<right ref="RM00000187M04YX_04_0012" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0002" proc-id="RM23G0E___000028S00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR OPEN IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the engine coolant temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A213406E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Connect terminals 1 and 2 of the engine coolant temperature sensor wire harness side connector.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Coolant Temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>140°C (284°F) or higher</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Coolant Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Engine Coolant Temperature Sensor)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the engine coolant temperature sensor connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187M04YX_04_0010" fin="false">OK</right>
<right ref="RM00000187M04YX_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0003" proc-id="RM23G0E___000028T00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ENGINE COOLANT TEMPERATURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the engine coolant temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C24-2 - C90-5 (THW)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>C24-1 - C90-11 (ETHW)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the engine coolant temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187M04YX_04_0011" fin="false">OK</right>
<right ref="RM00000187M04YX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0004" proc-id="RM23G0E___000028U00000">
<testtitle>READ VALUE USING INTELLIGENT TESTER (CHECK FOR SHORT IN WIRE HARNESS)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the engine coolant temperature sensor connector.</ptxt>
<figure>
<graphic graphicname="A198733E06" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Coolant Temp.</ptxt>
</test1>
<test1>
<ptxt>Read the value.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>-40°C (-40°F)</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Coolant Temperature Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reconnect the engine coolant temperature sensor connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187M04YX_04_0008" fin="false">OK</right>
<right ref="RM00000187M04YX_04_0005" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0005" proc-id="RM23G0E___000028V00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ENGINE COOLANT TEMPERATURE SENSOR - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the engine coolant temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C24-2 or C90-5 (THW) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the engine coolant temperature sensor connector.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187M04YX_04_0006" fin="false">OK</down>
<right ref="RM00000187M04YX_04_0009" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0006" proc-id="RM23G0E___000028W00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187M04YX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0010" proc-id="RM23G0E___000028Z00000">
<testtitle>CONFIRM GOOD CONNECTION TO SENSOR. IF OK, REPLACE ENGINE COOLANT TEMPERATURE SENSOR.</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the engine coolant temperature sensor (See page <xref label="Seep01" href="RM0000013ZA01FX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187M04YX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0009" proc-id="RM23G0E___000028Y00000">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the harness or connector.</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187M04YX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0011" proc-id="RM23G0E___000029000000">
<testtitle>CONFIRM GOOD CONNECTION TO ECM. IF OK, REPLACE ECM.</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM0000013Z001OX"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM00000187M04YX_04_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0008" proc-id="RM23G0E___000028X00000">
<testtitle>REPLACE ENGINE COOLANT TEMPERATURE SENSOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the engine coolant temperature sensor (See page <xref label="Seep01" href="RM0000013ZA01FX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187M04YX_04_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0012" proc-id="RM23G0E___000029100000">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z7X"/>).</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) for 1 second.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Confirm that the DTC is not output again.</ptxt>
</test1>
</content6>
<res>
<down ref="RM00000187M04YX_04_0013" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM00000187M04YX_04_0013">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>