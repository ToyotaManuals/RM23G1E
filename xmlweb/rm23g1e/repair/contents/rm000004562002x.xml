<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000N" variety="S000N">
<name>5L-E INTAKE / EXHAUST</name>
<ttl id="12010_S000N_7B912_T00AQ" variety="T00AQ">
<name>EXHAUST MANIFOLD</name>
<para id="RM000004562002X" category="A" type-id="30014" name-id="IE0QI-01" from="201207">
<name>INSTALLATION</name>
<subpara id="RM000004562002X_01" type-id="01" category="01">
<s-1 id="RM000004562002X_01_0001" proc-id="RM23G0E___000050V00000">
<ptxt>INSTALL EXHAUST MANIFOLD</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A222731E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install a new gasket to the cylinder head.</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Be sure to install a new gasket in the correct direction as shown in the illustration.</ptxt>
</atten4>
</s2>
<s2>
<figure>
<graphic graphicname="A222729E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the exhaust manifold with the 6 bolts and 2 new nuts. Uniformly tighten the bolts and nuts in several steps.</ptxt>
<torque>
<torqueitem>
<t-value1>52</t-value1>
<t-value2>530</t-value2>
<t-value4>38</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nut</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM000004562002X_01_0002" proc-id="RM23G0E___000050000000">
<ptxt>INSTALL NO. 1 EXHAUST MANIFOLD HEAT INSULATOR</ptxt>
<content1 releasenbr="1">
<s2>
<figure>
<graphic graphicname="A222728E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>Install the insulator with the 3 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>185</t-value2>
<t-value4>13</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>195</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000004562002X_01_0003" proc-id="RM23G0E___00006HA00000">
<ptxt>INSTALL ENGINE OIL LEVEL DIPSTICK GUIDE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Apply clean engine oil to a new O-ring.</ptxt>
</s2>
<s2>
<ptxt>Install the O-ring to the guide.</ptxt>
</s2>
<s2>
<ptxt>Install the guide with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>185</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000004562002X_01_0004" proc-id="RM23G0E___00006HB00000">
<ptxt>INSTALL ENGINE OIL LEVEL DIPSTICK</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the oil level dipstick to the guide.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004562002X_01_0005" proc-id="RM23G0E___00004X800000">
<ptxt>INSTALL FRONT EXHAUST PIPE ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and the front exhaust pipe to the exhaust manifold with 3 new nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>54</t-value1>
<t-value2>554</t-value2>
<t-value4>40</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the No. 1 exhaust pipe support bracket with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>71</t-value1>
<t-value2>724</t-value2>
<t-value4>52</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the clamp with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000004562002X_01_0006" proc-id="RM23G0E___00004US00000">
<ptxt>INSTALL INTAKE PIPE
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the intake pipe with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>184</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the intake pipe clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>6.0</t-value1>
<t-value2>61</t-value2>
<t-value3>53</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000004562002X_01_0007" proc-id="RM23G0E___000013V00000">
<ptxt>INSTALL RESONATOR WITH AIR CLEANER CAP SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Insert the hinge part of the air cleaner cap and hose into the air cleaner case, and then attach the 4 hook clamps.</ptxt>
</s2>
<s2>
<ptxt>Connect the air cleaner cap sub-assembly with the clamp.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 clamps and connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000004562002X_01_0009" proc-id="RM23G0E___00004XV00000">
<ptxt>INSPECT FOR EXHAUST GAS LEAK
</ptxt>
<content1 releasenbr="1">
<list1 type="nonmark">
<item>
<ptxt>If gas is leaking, tighten the areas necessary to stop the leak. Replace damaged parts as necessary.</ptxt>
</item>
</list1>
</content1></s-1>
<s-1 id="RM000004562002X_01_0010" proc-id="RM23G0E___00006HC00000">
<ptxt>INSTALL FRONT NO. 1 FENDER APRON TO FRAME SEAL RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the frame seal with the 5 clips.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000004562002X_01_0011" proc-id="RM23G0E___00006HD00000">
<ptxt>INSTALL FRONT FENDER APRON SEAL RH
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the apron seal with the 5 clips.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>