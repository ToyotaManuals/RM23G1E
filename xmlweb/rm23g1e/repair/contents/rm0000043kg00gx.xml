<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S0023" variety="S0023">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S0023_7B9CL_T00M9" variety="T00M9">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM0000043KG00GX" category="C" type-id="802ZR" name-id="ED2NI-09" from="201207" to="201210">
<dtccode>C1A47</dtccode>
<dtcname>Steering Angle Sensor</dtcname>
<subpara id="RM0000043KG00GX_01" type-id="60" category="03" proc-id="RM23G0E___0000FX200000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The skid control ECU receives steering rotation angle information from the steering angle sensor and sends the information to the driving support ECU through the CAN communication line.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1A47</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>While the vehicle speed is 50 km/h (31 mph) or more, a steering angle sensor malfunction signal is received from the skid control ECU for 1 second.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Vehicle stability control system</ptxt>
</item>
<item>
<ptxt>Driving support ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000043KG00GX_02" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000043KG00GX_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000043KG00GX_03_0001" proc-id="RM23G0E___0000FX300000">
<testtitle>CHECK FOR DTC (VEHICLE STABILITY CONTROL SYSTEM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM0000046KV00IX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Vehicle stability control system DTCs are not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000043KG00GX_03_0002" fin="false">OK</down>
<right ref="RM0000043KG00GX_03_0003" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043KG00GX_03_0002" proc-id="RM23G0E___0000FX400000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000043K500BX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000043K500BX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC C1A47 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000043KG00GX_03_0004" fin="true">OK</down>
<right ref="RM0000043KG00GX_03_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043KG00GX_03_0003">
<testtitle>GO TO VEHICLE STABILITY CONTROL SYSTEM<xref label="Seep01" href="RM0000045Z600OX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043KG00GX_03_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043KG00GX_03_0005">
<testtitle>REPLACE DRIVING SUPPORT ECU<xref label="Seep01" href="RM000002OJO012X"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>