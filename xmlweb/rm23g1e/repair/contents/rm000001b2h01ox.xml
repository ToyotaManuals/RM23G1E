<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97N_T00HB" variety="T00HB">
<name>FRONT SPEED SENSOR</name>
<para id="RM000001B2H01OX" category="A" type-id="30014" name-id="BC9P5-05" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000001B2H01OX_04" type-id="11" category="10" proc-id="RM23G0E___0000AQC00001">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>The procedure listed below is for the LH side.</ptxt>
</item>
<item>
<ptxt>Other than areas where instructions are provided, use the same procedures for the RH and LH sides.</ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000001B2H01OX_03" type-id="01" category="01">
<s-1 id="RM000001B2H01OX_03_0013" proc-id="RM23G0E___00009CJ00001">
<ptxt>INSTALL FRONT SPEED SENSOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the speed sensor with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.5</t-value1>
<t-value2>87</t-value2>
<t-value3>75</t-value3>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure there are no pieces of iron or other foreign matter attached to the sensor tip.</ptxt>
</item>
<item>
<ptxt>While inserting the speed sensor into the knuckle hole, do not strike or damage the sensor tip.</ptxt>
</item>
<item>
<ptxt>After installing the speed sensor, make sure there is no clearance or foreign matter between the sensor stay part and the knuckle.</ptxt>
</item>
<item>
<ptxt>Make sure there is no foreign matter attached to the speed sensor rotor.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001B2H01OX_03_0016" proc-id="RM23G0E___0000AQB00001">
<ptxt>INSTALL SKID CONTROL SENSOR CLAMP</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the skid control sensor clamp with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Install the clamp so that the rotation stopper touches the knuckle.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001B2H01OX_03_0008" proc-id="RM23G0E___00009WN00001">
<ptxt>INSTALL FRONT SKID CONTROL SENSOR WIRE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the connector as follows.</ptxt>
<s3>
<ptxt>for LH:</ptxt>
<ptxt>1. Attach the connector, and then connect the connector.</ptxt>
<atten3>
<ptxt>Securely connect the connector.</ptxt>
</atten3>
</s3>
<s3>
<ptxt>for RH:</ptxt>
<ptxt>1. Install the skid control sensor clamp with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
<atten3>
<ptxt>Make sure the clamp rotation stopper touches the installation position.</ptxt>
</atten3>
<ptxt>2. Attach the connector, and then connect the connector.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Securely connect the connector.</ptxt>
</item>
<item>
<ptxt>When connecting the connector, do not twist the wire harness.</ptxt>
</item>
</list1>
</atten3>
<ptxt>3. Attach the clip.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Install the 2 harness clamps with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When installing the clamps, do not twist the wire harness.</ptxt>
</item>
<item>
<ptxt>Make sure the clamp rotation stopper touches the installation position.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Install the 2 harness clamps with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>127</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When installing the clamps, do not twist the wire harness.</ptxt>
</item>
<item>
<ptxt>Make sure the clamp rotation stopper touches the installation position.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Attach the clip.</ptxt>
</s2>
<s2>
<ptxt>Connect the connector.</ptxt>
<atten3>
<ptxt>Securely connect the connector.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000001B2H01OX_03_0017" proc-id="RM23G0E___0000JQB00001">
<ptxt>INSTALL FRONT WHEEL</ptxt>
<content1 releasenbr="1">
<torque>
<torqueitem>
<t-value1>112</t-value1>
<t-value2>1137</t-value2>
<t-value4>82</t-value4>
</torqueitem>
</torque>
</content1>
</s-1>
<s-1 id="RM000001B2H01OX_03_0011" proc-id="RM23G0E___0000AQA00001">
<ptxt>CHECK SPEED SENSOR SIGNAL</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>w/o VSC:</ptxt>
<ptxt>Check the speed sensor signal (See page <xref label="Seep01" href="RM000000XHT0C0X"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ VSC (for hydraulic brake booster):</ptxt>
<ptxt>Check the speed sensor signal (See page <xref label="Seep02" href="RM00000452K00LX"/>).</ptxt>
</s2>
<s2>
<ptxt>w/ VSC (for vacuum brake booster):</ptxt>
<ptxt>Check the speed sensor signal (See page <xref label="Seep03" href="RM000000XHT0BZX"/>).</ptxt>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>