<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94I_T00E6" variety="T00E6">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1GR-FE)</name>
<para id="RM000000W8D0DRX" category="C" type-id="302GO" name-id="AT7MG-02" from="201207" to="201210">
<dtccode>P2757</dtccode>
<dtcname>Torque Converter Clutch Pressure Control Solenoid Performance (Shift Solenoid Valve SLU)</dtcname>
<subpara id="RM000000W8D0DRX_01" type-id="60" category="03" proc-id="RM23G0E___00008A800000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM uses the signals from the throttle position sensor, air-flow meter, turbine (input) speed sensor, output speed sensor and crankshaft position sensor to monitor the engagement condition of the lock-up clutch.</ptxt>
<figure>
<graphic graphicname="C159072E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<ptxt>Then the ECM compares the engagement condition of the lock-up clutch with the lock-up schedule in the ECM memory to detect mechanical problems of the shift solenoid valve SLU, valve body and torque converter clutch.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2757</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock-up does not occur when driving in the lock-up range (normal driving at 80 km/h (50 mph)), or lock-up remains ON in the lock-up OFF range (2-trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve SLU remains open or closed</ptxt>
</item>
<item>
<ptxt>Valve body is blocked</ptxt>
</item>
<item>
<ptxt>Torque converter clutch assembly</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
<item>
<ptxt>Line pressure is too low</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000W8D0DRX_02" type-id="64" category="03" proc-id="RM23G0E___00008A900000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>Torque converter lock-up is controlled by the ECM based on the turbine (input) speed sensor NT, output speed sensor SP2, engine speed, engine load, engine temperature, vehicle speed, transmission temperature and gear selection. The ECM determines the lock-up status of the torque converter by comparing the engine speed (NE) to the input turbine speed (NT). The ECM calculates the actual transmission gear by comparing input turbine speed (NT) to output shaft speed (SP2). When conditions are appropriate, the ECM requests "lock-up" by applying control voltage to shift solenoid SLU. When SLU is turned on, it applies pressure to the lock-up relay valve and locks the torque converter clutch.</ptxt>
<ptxt>If the ECM detects no lock-up after lock-up has been requested or if it detects lock-up when it is not requested, the ECM interprets this as a fault in the shift solenoid valve SLU or lock-up system performance.</ptxt>
<ptxt>The ECM will turn on the MIL and store the DTC.</ptxt>
<ptxt>Example:</ptxt>
<ptxt>When either of the following is met, the system judges it as a malfunction.</ptxt>
<list1 type="unordered">
<item>
<ptxt>There is a difference in speed between the input side (engine speed) and output side (input turbine speed) of the torque converter when the ECM commands lock-up (Engine speed is at least 70 rpm more than input turbine speed.)</ptxt>
</item>
<item>
<ptxt>There is no difference in speed between the input side (engine speed) and output side (input turbine speed) of the torque converter when the ECM commands lock-up off.</ptxt>
<ptxt>The difference between engine speed and input turbine speed is less than 35 rpm.</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000000W8D0DRX_07" type-id="51" category="05" proc-id="RM23G0E___00008AA00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
<table pgwide="1">
<title>Engine and ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Lock Up</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control shift solenoid SLU to set automatic transmission to lock-up</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON or OFF</ptxt>
</entry>
<entry valign="middle">
<ptxt>Possible to check shift solenoid valve SLU operation.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Throttle valve opening angle: Less than 35%.</ptxt>
</item>
<item>
<ptxt>Vehicle speed: 60 km/h (37 mph) or more.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This test can be conducted when the vehicle speed is 60 km/h (37 mph) or more.</ptxt>
</item>
<item>
<ptxt>This test can be conducted in 5th gear.</ptxt>
</item>
</list1>
</atten4>
</step2>
<step2>
<ptxt>Lightly depress the accelerator pedal and check that the engine speed does not change abruptly.</ptxt>
<figure>
<graphic graphicname="C159073E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When changing the accelerator pedal opening angle while driving, if the engine speed does not change, lock-up is on.</ptxt>
</item>
<item>
<ptxt>Slowly release the accelerator pedal in order to decelerate, but do not fully release the pedal. (Fully releasing the pedal will close the throttle valve and lock-up may be turned off automatically.)</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM000000W8D0DRX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W8D0DRX_08_0001" proc-id="RM23G0E___00008AB00000">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P2757)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Trouble Codes.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only P2757 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P2757 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any other codes besides P2757 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W8D0DRX_08_0002" fin="false">A</down>
<right ref="RM000000W8D0DRX_08_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000W8D0DRX_08_0002" proc-id="RM23G0E___00008A200000">
<testtitle>INSPECT SHIFT SOLENOID VALVE SLU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove shift solenoid valve SLU.</ptxt>
<figure>
<graphic graphicname="C209928E05" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) with a 21 W bulb → Terminal 2</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Terminal 1</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve SLU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000000W8D0DRX_08_0010" fin="false">OK</down>
<right ref="RM000000W8D0DRX_08_0007" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8D0DRX_08_0010" proc-id="RM23G0E___000088G00000">
<testtitle>INSPECT TRANSMISSION VALVE BODY ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the transmission valve body assembly (See page <xref label="Seep01" href="RM0000013FG02LX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There are no foreign objects on any valve.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000W8D0DRX_08_0011" fin="false">OK</down>
<right ref="RM000000W8D0DRX_08_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8D0DRX_08_0011" proc-id="RM23G0E___000088V00000">
<testtitle>INSPECT TORQUE CONVERTER CLUTCH ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the torque converter clutch assembly (See page <xref label="Seep01" href="RM0000013F2042X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The torque converter clutch operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM000000W8D0DRX_08_0005" fin="true">OK</down>
<right ref="RM000000W8D0DRX_08_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W8D0DRX_08_0005">
<testtitle>REPAIR OR REPLACE AUTOMATIC TRANSMISSION ASSEMBLY<xref label="Seep01" href="RM0000018ZD03RX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8D0DRX_08_0006">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G906LX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8D0DRX_08_0007">
<testtitle>REPLACE SHIFT SOLENOID VALVE SLU<xref label="Seep01" href="RM0000013FG02LX_01_0003"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8D0DRX_08_0008">
<testtitle>REPAIR OR REPLACE TRANSMISSION VALVE BODY ASSEMBLY<xref label="Seep01" href="RM0000013CM04BX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W8D0DRX_08_0009">
<testtitle>REPLACE TORQUE CONVERTER CLUTCH ASSEMBLY<xref label="Seep01" href="RM0000018ZD03RX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>