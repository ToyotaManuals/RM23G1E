<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S0023" variety="S0023">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S0023_7B9CL_T00M9" variety="T00M9">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM0000043K900IX" category="C" type-id="802CF" name-id="PC1BS-02" from="201210">
<dtccode>B2075</dtccode>
<dtcname>Passenger Side Seat Belt Buckle Switch Circuit Malfunction</dtcname>
<subpara id="RM0000043K900IX_01" type-id="60" category="03" proc-id="RM23G0E___0000FVT00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when there is a malfunction in the front seat inner belt buckle switch circuit for the passenger side. The seat belt control ECU receives buckle switch condition information from the front seat inner belt and controls the pre-crash safety system.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>B2075</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>A malfunction in the front seat inner belt (buckle switch) circuit.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Front seat inner belt assembly (for passenger side)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>Seat belt control ECU</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000043K900IX_02" type-id="32" category="03" proc-id="RM23G0E___0000FVU00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E200777E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</content5>
</subpara>
<subpara id="RM0000043K900IX_03" type-id="51" category="05">
<name>INSPECTION PROCEDURE</name>
</subpara>
<subpara id="RM0000043K900IX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000043K900IX_05_0010" proc-id="RM23G0E___0000JN400001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000043K500BX"/>).</ptxt>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000043K500BX"/>).</ptxt>
</test1>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2075 is not output.</ptxt>
</specitem>
</spec>
</content6>
<res>
<down ref="RM0000043K900IX_05_0011" fin="true">OK</down>
<right ref="RM0000043K900IX_05_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043K900IX_05_0011">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043K900IX_05_0001" proc-id="RM23G0E___0000FVV00001">
<testtitle>CHECK SEAT BELT CONTROL ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the seat belt control ECU, but do not disconnect the connector (See page <xref label="Seep01" href="RM000003UX700DX"/>).</ptxt>
<figure>
<graphic graphicname="E200101E02" width="7.106578999in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Using an oscilloscope, check the waveform.</ptxt>
<table>
<title>Measurement Condition</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle">
<ptxt>Content</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>G118-5 (PBK+) - G118-6 (PBK-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Tool Setting</ptxt>
</entry>
<entry valign="middle">
<ptxt>2 V/DIV., 20 ms/DIV.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch ON, front seat belt (for passenger side) fastened → unfastened</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<spec>
<title>OK</title>
<specitem>
<ptxt>Waveform is as shown in illustration. </ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000043K900IX_05_0003" fin="true">OK</down>
<right ref="RM0000043K900IX_05_0002" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043K900IX_05_0003">
<testtitle>REPLACE SEAT BELT CONTROL ECU<xref label="Seep01" href="RM000003UX700DX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043K900IX_05_0002" proc-id="RM23G0E___0000FVW00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SEAT BELT CONTROL ECU - FRONT SEAT INNER BELT ASSEMBLY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G118 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the Q21*1 or R34*2 belt connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for LHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G118-5 (PBK+) - Q21-5 (RBE+)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G118-6 (PBK-) - Q21-4 (RBE-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G118-5 (PBK+) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G118-6 (PBK-) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for RHD</title>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G118-5 (PBK+) - R34-5 (LBE+)</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G118-6 (PBK-) - R34-4 (LBE-)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G118-5 (PBK+) - Body ground</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G118-6 (PBK-) - Body ground</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000043K900IX_05_0008" fin="true">OK</down>
<right ref="RM0000043K900IX_05_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000043K900IX_05_0008">
<testtitle>REPLACE FRONT SEAT INNER BELT ASSEMBLY (for Passenger Side)<xref label="Seep01" href="RM000003RQO019X"/>
</testtitle>
</testgrp>
<testgrp id="RM0000043K900IX_05_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>