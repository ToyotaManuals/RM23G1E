<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S0022" variety="S0022">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S0022_7B9C6_T00LU" variety="T00LU">
<name>AIRBAG SYSTEM</name>
<para id="RM000000KT10J3X" category="L" type-id="3001A" name-id="RS92N-07" from="201210">
<name>PRECAUTION</name>
<subpara id="RM000000KT10J3X_z0" proc-id="RM23G0E___0000FFE00002">
<content5 releasenbr="2">
<atten2>
<list1 type="unordered">
<item>
<ptxt>This vehicle is equipped with a Supplemental Restraint System (SRS), which consists of a steering pad, instrument panel passenger airbag, curtain shield airbag, front seat side airbag, lower No. 1 instrument panel airbag, seat belt pretensioner, center airbag sensor, front airbag sensor, side airbag sensor, rear airbag sensor and rear floor side airbag sensor. Failure to carry out service procedures in the correct sequence could cause SRS parts to unexpectedly deploy and possibly lead to serious injuries. Furthermore, if a mistake is made when servicing SRS parts, they may fail to operate when required. Before performing servicing (including installation/removal, inspection and replacement of parts), be sure to read the following precautions.</ptxt>
</item>
<item>
<ptxt>Before starting work, wait at least a minute after the ignition switch is turned off and after the cable is disconnected from the negative (-) battery terminal (SRS parts are equipped with a backup power source. If work is started within a minute of turning the ignition switch off and disconnecting the cable from the negative (-) battery terminal, SRS parts may deploy).</ptxt>
</item>
<item>
<ptxt>Do not expose SRS parts directly to hot air or flames.</ptxt>
</item>
</list1>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Malfunction symptoms of SRS parts are difficult to confirm. DTCs are the most important source of information when troubleshooting. During troubleshooting, always confirm DTCs before disconnecting the cable from the negative (-) battery terminal.</ptxt>
</item>
<item>
<ptxt>For minor collisions where SRS parts do not deploy, always inspect the SRS parts.</ptxt>
</item>
<item>
<ptxt>Before performing repairs, remove the airbag sensors if any kind of impact is likely to occur to an airbag sensor during repairs.</ptxt>
</item>
<item>
<ptxt>Never use SRS parts from another vehicle. When replacing SRS parts, replace them with new ones.</ptxt>
</item>
<item>
<ptxt>Never disassemble or attempt to repair SRS parts.</ptxt>
</item>
<item>
<ptxt>If an SRS part has been dropped, or if there are any cracks, dents or other defects in the case, bracket or connector, replace the SRS part with a new one.</ptxt>
</item>
<item>
<ptxt>Use an ohmmeter/voltmeter with high impedance (10 kΩ/V minimum) for troubleshooting the electrical circuits.</ptxt>
</item>
<item>
<ptxt>Information labels are attached to the periphery of SRS parts. Follow the cautions and instructions on the labels.</ptxt>
</item>
<item>
<ptxt>After work on the SRS is completed, perform the SRS warning light check (See page <xref label="Seep01" href="RM000000XFD0INX"/>).</ptxt>
</item>
<item>
<ptxt>When the cable is disconnected from the negative (-) battery terminal, the memory settings of each system are cleared. Because of this, be sure to write down the settings of each system before starting work. When work is finished, reset the settings of each system as before. Never use a backup power supply from outside the vehicle to avoid clearing the memory in a system.</ptxt>
</item>
<item>
<ptxt>If the vehicle is equipped with a mobile communication system, refer to the Precaution in the Introduction section.</ptxt>
</item>
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable from the negative (-) battery terminal while performing repairs, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep03" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>In the airbag system, the center airbag sensor, front airbag sensor LH and RH, side airbag sensor LH and RH, rear airbag sensor LH and RH, and rear floor side airbag sensor are collectively referred to as the airbag sensors.</ptxt>
</atten4>
<step1>
<ptxt>HANDLING PRECAUTIONS FOR AIRBAG SENSORS</ptxt>
<step2>
<ptxt>Before starting the following operations, wait for at least a minute after disconnecting the cable from the negative (-) battery terminal:</ptxt>
<step3>
<ptxt>Replacement of the airbag sensors.</ptxt>
</step3>
<step3>
<ptxt>Adjustment of the front/rear doors of the vehicle equipped with side airbags and curtain shield airbags (fitting adjustment).</ptxt>
</step3>
</step2>
<step2>
<ptxt>When connecting or disconnecting the airbag sensor connectors, make sure that each sensor is installed in the vehicle.</ptxt>
</step2>
<step2>
<ptxt>Do not use airbag sensors which have been dropped during operation or transportation.</ptxt>
</step2>
<step2>
<ptxt>Do not disassemble the airbag sensors.</ptxt>
</step2>
</step1>
<step1>
<ptxt>INSPECTION PROCEDURE FOR VEHICLE INVOLVED IN ACCIDENT</ptxt>
<step2>
<ptxt>When the airbag has not deployed, confirm the DTCs by checking the SRS warning light. If there is any malfunction in the SRS airbag system, perform troubleshooting.</ptxt>
</step2>
<step2>
<ptxt>When any of the airbags have deployed, replace the airbag sensors and check the installation condition.</ptxt>
</step2>
</step1>
<step1>
<ptxt>SRS CONNECTORS</ptxt>
<step2>
<ptxt>SRS connectors are located as shown in the following illustration.</ptxt>
<figure>
<graphic graphicname="C213200E02" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="C213202E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<figure>
<graphic graphicname="C213201E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<tgroup cols="2" align="left">
<colspec colname="COL2" colwidth="3.50in"/>
<colspec colname="COL3" colwidth="3.58in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Application</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Terminal Twin-lock Mechanism</ptxt>
</entry>
<entry>
<ptxt>Connectors 11, 12, 13, 14, 19, 20</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Activation Prevention Mechanism</ptxt>
</entry>
<entry>
<ptxt>Connectors 2, 4, 9, 13, 17</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Half Connection Prevention Mechanism</ptxt>
</entry>
<entry>
<ptxt>Connectors 10, 12, 13, 14</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Connector Position Assurance Mechanism</ptxt>
</entry>
<entry>
<ptxt>Connectors 8, 16</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Connector Lock Mechanism (1)</ptxt>
</entry>
<entry>
<ptxt>Connectors 6</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Connector Lock Mechanism (2)</ptxt>
</entry>
<entry>
<ptxt>Connectors 2, 4</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Improper Connection Prevention Lock Mechanism</ptxt>
</entry>
<entry>
<ptxt>Connectors 1, 3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>All connectors in the SRS, except the seat position sensor connector, are colored yellow to distinguish them from other connectors. These connectors have special functions and are specially designed for the SRS. All SRS connectors use durable gold-plated terminals which are placed in the locations shown below to ensure high reliability.</ptxt>
<figure>
<graphic graphicname="Z005953E10" width="2.775699831in" height="1.771723296in"/>
</figure>
<step3>
<ptxt>Terminal twin-lock mechanism:</ptxt>
<ptxt>All connectors with a terminal twin-lock mechanism have a two-piece component consisting of a housing and spacer. This design enables the terminal to be locked securely by two locking devices (the retainer and the lance) to prevent terminals from coming out.</ptxt>
</step3>
<step3>
<ptxt>Activation prevention mechanism:</ptxt>
<ptxt>All connectors with an activation prevention mechanism contain a short spring plate. When these connectors are disconnected, the short spring plate creates a short circuit by automatically connecting the positive (+) and negative (-) terminals of the squib.</ptxt>
<figure>
<graphic graphicname="H045309E16" width="7.106578999in" height="4.7836529in"/>
</figure>
</step3>
<step3>
<ptxt>Half connection prevention mechanism:</ptxt>
<ptxt>If the connector is not completely connected, the connector is disconnected by the force of the spring so that no continuity exists.</ptxt>
<figure>
<graphic graphicname="C150151E02" width="7.106578999in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>Connector position assurance mechanism:</ptxt>
<ptxt>The CPA (yellow part) slides only when the housing lock (white part) is completely engaged, which completes the connector engagement.</ptxt>
<figure>
<graphic graphicname="H043306E05" width="7.106578999in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>Connector lock mechanism (1):</ptxt>
<ptxt>Locking the connector lock button connects the connector securely.</ptxt>
<figure>
<graphic graphicname="H101828E04" width="7.106578999in" height="1.771723296in"/>
</figure>
</step3>
<step3>
<ptxt>Connector lock mechanism (2):</ptxt>
<ptxt>Both the primary lock with holder lances and the secondary lock with retainer prevent the connectors from becoming disconnected.</ptxt>
<figure>
<graphic graphicname="H043918E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
<step3>
<ptxt>Improper connection prevention lock mechanism:</ptxt>
<ptxt>When connecting the holder, the lever is pushed into the lock position by rotating around the A axis to lock the holder securely.</ptxt>
<figure>
<graphic graphicname="H043245E03" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>