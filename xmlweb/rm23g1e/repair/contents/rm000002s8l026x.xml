<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AL_T00K9" variety="T00K9">
<name>LIN COMMUNICATION SYSTEM</name>
<para id="RM000002S8L026X" category="C" type-id="801Y5" name-id="NW1MR-03" from="201207" to="201210">
<dtccode>B2323</dtccode>
<dtcname>Rear Door RH ECU Communication Stop</dtcname>
<subpara id="RM000002S8L026X_01" type-id="60" category="03" proc-id="RM23G0E___0000DBY00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when LIN communication between the rear power window regulator motor assembly RH and main body ECU (multiplex network body ECU) stops for 10 seconds or more.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.56in"/>
<colspec colname="COL2" colwidth="3.16in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2323</ptxt>
</entry>
<entry valign="middle">
<ptxt>No communication between the rear power window regulator motor assembly RH and main body ECU (multiplex network body ECU) for 10 seconds or more.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Rear power window regulator motor assembly RH</ptxt>
</item>
<item>
<ptxt>Main body ECU (multiplex network body ECU)</ptxt>
</item>
<item>
<ptxt>Harness or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002S8L026X_02" type-id="32" category="03" proc-id="RM23G0E___0000DBZ00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C211493E38" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002S8L026X_03" type-id="51" category="05" proc-id="RM23G0E___0000DC000000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When using the intelligent tester with the ignition switch off to troubleshoot:</ptxt>
<ptxt>Connect the intelligent tester to the vehicle and turn a courtesy light switch on and off at 1.5 second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and bulbs for circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>When communication between the rear power window regulator motor assembly RH and main body ECU (multiplex network body ECU) stops, DTC B2325 is also stored.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002S8L026X_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002S8L026X_04_0001" proc-id="RM23G0E___0000DC100000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8L026X_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8L026X_04_0002" proc-id="RM23G0E___0000DC200000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2323 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8L026X_04_0007" fin="true">OK</down>
<right ref="RM000002S8L026X_04_0003" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8L026X_04_0003" proc-id="RM23G0E___0000DC300000">
<testtitle>CHECK HARNESS AND CONNECTOR (MAIN BODY ECU - REAR POWER WINDOW REGULATOR MOTOR RH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the main body ECU (multiplex network body ECU) from the driver side junction block assembly (See page <xref label="Seep01" href="RM000003WBO02VX"/>).</ptxt>
</test1>
<test1>
<ptxt>Disconnect the L4 rear power window regulator motor assembly RH connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A-16 (LIN2) - L4-9 (LIN)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>A-16 (LIN2) or L4-9 (LIN) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8L026X_04_0004" fin="false">OK</down>
<right ref="RM000002S8L026X_04_0008" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8L026X_04_0004" proc-id="RM23G0E___0000DC400000">
<testtitle>CHECK HARNESS AND CONNECTOR (REAR POWER WINDOW REGULATOR MOTOR RH - BATTERY AND BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the L4 rear power window regulator motor assembly RH connector.</ptxt>
<figure>
<graphic graphicname="C216965E08" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L4-1 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>L4-2 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>

<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Rear Power Window Regulator Motor Assembly RH)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM000002S8L026X_04_0005" fin="false">OK</down>
<right ref="RM000002S8L026X_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8L026X_04_0005" proc-id="RM23G0E___0000DC500000">
<testtitle>REPLACE REAR POWER WINDOW REGULATOR MOTOR ASSEMBLY RH</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the rear power window regulator motor assembly RH with a new or normally functioning one (See page <xref label="Seep01" href="RM0000011RX077X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8L026X_04_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8L026X_04_0012" proc-id="RM23G0E___0000DC700000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002S8L026X_04_0006" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002S8L026X_04_0006" proc-id="RM23G0E___0000DC600000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000002S8D02RX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2323 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002S8L026X_04_0011" fin="true">OK</down>
<right ref="RM000002S8L026X_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002S8L026X_04_0011">
<testtitle>END (REAR POWER WINDOW REGULATOR MOTOR RH IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002S8L026X_04_0007">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002S8L026X_04_0008">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8L026X_04_0009">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002S8L026X_04_0010">
<testtitle>REPLACE MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)<xref label="Seep01" href="RM000003WBO02VX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>