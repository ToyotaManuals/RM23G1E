<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0011" variety="S0011">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0011_7B93I_T00D6" variety="T00D6">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM0000044GW00AX" category="D" type-id="303F2" name-id="CC2IB-01" from="201207">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM0000044GW00AX_z0" proc-id="RM23G0E___000078H00000">
<content5 releasenbr="1">
<step1>
<ptxt>GENERAL</ptxt>
<step2>
<ptxt>The dynamic radar cruise control system has 2 cruise control modes: constant speed control mode and vehicle-to-vehicle distance control mode.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The vehicle-to-vehicle distance control mode is always selected when starting the dynamic radar cruise control system.</ptxt>
</item>
<item>
<ptxt>Operation of the constant speed control mode is the same as the conventional cruise control system.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>If there is a vehicle ahead in the same lane, the system maintains the vehicle distance that has been set by the driver. If the system detects a vehicle driving at a slower speed ahead while the driver is driving at a constant speed, it closes the throttle valve to decelerate. If further deceleration is required, the system controls the brake actuator in order to apply the brakes. Thereafter, if there are no vehicles ahead within the set vehicle-to-vehicle distance because either the vehicle ahead or the driver has changed lanes, the system accelerates slowly to reach the set vehicle speed and resumes driving at the constant speed.</ptxt>
</step2>
<step2>
<ptxt>for 1KD-FTV:</ptxt>
<ptxt>If there is a vehicle ahead in the same lane, the system maintains the vehicle distance that has been set by the driver. If the system detects a vehicle driving at a slower speed ahead while the driver is driving at a constant speed, it decreases the fuel injection quantity to decelerate. If further deceleration is required, the system controls the brake actuator in order to apply the brakes. Thereafter, if there are no vehicles ahead within the set vehicle-to-vehicle distance because either the vehicle ahead or the driver has changed lanes, the system accelerates slowly to reach the set vehicle speed and resumes driving at the constant speed.</ptxt>
</step2>
<step2>
<ptxt>The constant speed control mode is designed to maintain a constant cruising speed. The vehicle-to-vehicle distance control mode is designed to control cruising using a constant speed function, deceleration cruising function, follow-up cruising function and acceleration cruising function.</ptxt>
</step2>
<step2>
<ptxt>The millimeter wave radar sensor assembly and driving support ECU control the system while the vehicle-to-vehicle distance control mode is in operation and send signals to each actuator and ECU.</ptxt>
</step2>
<step2>
<ptxt>In vehicle-to-vehicle distance control mode, the dynamic radar cruise control system receives signals from the yaw rate sensor and steering angle sensor. Based on these signals, it then estimates curve radius and corrects the information on the preceding vehicle while turning. It can also adjust the brake control when approaching another vehicle.</ptxt>
</step2>
<step2>
<ptxt>This system judges the presence of a vehicle in front and the distance to it based on the signals from the millimeter wave radar sensor while the vehicle-to-vehicle distance control mode is in operation. Using this information, the system informs the driver of any danger with the warning buzzer, performs brake control and turns on the stop light when approaching the vehicle in front.</ptxt>
</step2>
<step2>
<ptxt>The following illustration shows an example under the following conditions: the vehicle speed is 100 km/h (62 mph) and the speed of the vehicle in front is 80 km/h (50 mph).  The vehicle distance setting can be changed to 3 distances by operating the steering pad switch (distance control switch): long (approximately 50 m [160 ft.]), middle (approximately 40 m [130 ft.]) and short (approximately 30 m [100 ft.]) (when the vehicle speed is approximately 80 km/h [50 mph]).</ptxt>
<figure>
<graphic graphicname="E200743E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Vehicle distance increases and decreases in accordance with vehicle speed.</ptxt>
</item>
<item>
<ptxt>The controlling condition is indicated on the multi-information display in the combination meter assembly.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
<step1>
<ptxt>FUNCTION OF MAIN COMPONENTS</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Combination meter assembly (Cruise control indicator light)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Turns on when the cruise control switch is on.</ptxt>
</item>
<item>
<ptxt>If the ECM or driving support ECU detects a malfunction, this light flashes to warn the driver.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Combination meter assembly (Buzzer)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If the ECM detects an automatic cancel signal while the vehicle is operating under cruise control, this buzzer sounds once to inform the driver.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Combination meter assembly (Master warning light)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If the ECM or driving support ECU detects malfunction, this light comes on to inform the driver</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Combination meter assembly (Multi-information display)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>While the system is in vehicle-to-vehicle distance control mode, the multi-information display receives signals from the ECM in order to display system conditions in the graphic area.</ptxt>
</item>
<item>
<ptxt>If the ECM or driving support ECU detects a malfunction signal while the vehicle is operating under cruise control, a warning message is displayed in the warning area to inform the driver.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Cruise control switch (ON-OFF switch)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turns the cruise control system on/off.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Cruise control switch (Control switch)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>The vehicle speed setting, deceleration setting preset speed resumption, acceleration setting and canceling signals are output to the ECM through operation of this switch.</ptxt>
</item>
<item>
<ptxt>Switches control modes between constant speed control mode and   vehicle-to-vehicle distance control mode.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Steering pad switch assembly (Distance control switch)</ptxt>
</entry>
<entry valign="middle">
<ptxt>While the system is in vehicle-to-vehicle distance control mode, the driver can operate the steering pad switch (distance control switch) to select the vehicle-to-vehicle distance from 3 possible settings: long, middle and short.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Stop light switch assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Outputs the brake pedal status signal to the ECM which is then sent to the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Windshield wiper switch assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transmits windshield wiper switch information to the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Millimeter wave radar sensor assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Emits radar waves forward, uses reflected waves for detecting the presence of a vehicle in front, vehicle-to-vehicle distance and relative speed and transmits this information to the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Steering angle sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the angle and direction of steering and transmits its signal to the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Yaw rate sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the yaw rate of the vehicle and transmits its signal to the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Master cylinder solenoid (Skid control ECU)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Actuates the brakes.</ptxt>
</item>
<item>
<ptxt>While the system is in vehicle-to-vehicle distance control mode, the skid control ECU actuates the brake actuator in accordance with the brake request signal received from the ECM.</ptxt>
</item>
<item>
<ptxt>Upon receiving the signal from the ECM, the skid control ECU sounds the skid control buzzer.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Skid control buzzer</ptxt>
</entry>
<entry valign="middle">
<ptxt>This buzzer sounds upon receiving the signal from the skid control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>ECM</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>If the ECM detects a malfunction in the dynamic radar cruise control system, it stores DTCs.</ptxt>
</item>
<item>
<ptxt>Controls the dynamic radar cruise control system in accordance with signals from switches, sensors and the driving support ECU.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Park/neutral position switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Outputs a shift position signal to the ECM which is then sent to the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Throttle with motor body assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upon receiving a signal from the ECM, the throttle motor actuates the throttle valve.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Driving support ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>While the system is in vehicle-to-vehicle distance control mode, the driving support ECU detects the vehicle in front based on the signal from the millimeter wave radar sensor. Then, the driving support ECU calculates the acceleration or deceleration rate in order to attain the target vehicle-to-vehicle distance and outputs request signals to the ECM and skid control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle speed sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>A vehicle speed signal which is output from the skid control ECU is sent to the ECM via the combination meter assembly</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step1>
<step1>
<ptxt>LIMIT CONTROL</ptxt>
<step2>
<ptxt>Low speed limit</ptxt>
<ptxt>The lower limit of the speed setting range is set at approximately 50 km/h (30 mph). The cruise control system cannot be set when the driving vehicle speed is below the low speed limit. Cruise control operation is automatically canceled when the vehicle speed decreases below the low speed limit (40 km/h [25 mph]) while the cruise control system is in operation. When the vehicle speed is increased above the low speed limit after the cruise control operation is canceled, pressing the +RES switch increases the vehicle speed to the stored speed.</ptxt>
</step2>
<step2>
<ptxt>High speed limit (Constant speed control mode)</ptxt>
<ptxt>The upper limit of the speed setting range is approximately 200 km/h (125 mph). The cruise control system cannot be set when the driving vehicle speed is over the high speed limit. Also, RESUME/ACCEL cannot be used to increase speed beyond the high speed limit.</ptxt>
</step2>
</step1>
<step1>
<ptxt>CRUISE CONTROL OPERATION</ptxt>
<ptxt>The cruise control switch operates 8 functions: SET, COAST, TAP-DOWN, RESUME, ACCEL, TAP-UP, CANCEL and MODE. The SET, TAP-DOWN and COAST functions and the RESUME, TAP-UP and ACCEL functions are operated with the same switch. The cruise control switch is an automatic return type switch which turns on only while pushing it in each arrow direction and turns off after releasing it. The dynamic radar cruise control system has two cruise control modes: constant speed control mode and vehicle-to-vehicle distance control mode.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The vehicle-to-vehicle distance control mode is always selected when starting up the dynamic radar cruise control system (the Cruise control indicator light and RADAR READY turn on).</ptxt>
<figure>
<graphic graphicname="E196311E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="left" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control indicator light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
<item>
<ptxt>Operation of the constant speed control mode is the same as the conventional cruise control system.</ptxt>
</item>
</list1>
<step2>
<ptxt>MODE CONTROL</ptxt>
<ptxt>Pushing the cruise control switch to MODE for more than 1 second while driving in vehicle-to-vehicle distance control mode (RADAR READY is on) switches the dynamic radar cruise control system to the constant speed control mode.</ptxt>
</step2>
<step2>
<ptxt>SET CONTROL (Constant speed control mode)</ptxt>
<figure>
<graphic graphicname="E196312E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>The vehicle speed is stored and constant speed control is maintained when pushing the cruise control switch to -SET while driving with the vehicle speed within the set speed range (between the low and high speed limits) after pushing the cruise control switch on and entering the constant speed control mode (the Cruise control indicator light and SET indicator light turn on).</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="left" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control indicator light (Constant Speed Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>SET Indicator Light</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>SET CONTROL (Vehicle-to-vehicle distance control mode)</ptxt>
<figure>
<graphic graphicname="E196313E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<ptxt>The vehicle speed is stored and vehicle-to-vehicle control is maintained when pushing the cruise control switch to -SET while driving with the cruise control switch on (RADAR READY is on) and with the vehicle speed within the set speed range (between the low and high speed limits) (the Cruise control indicator light and SET indicator light turn on).</ptxt>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="left" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cruise control indicator light (Vehicle-to-vehicle Distance Control Mode)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>SET Indicator Light</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>COAST CONTROL (Constant speed control mode)</ptxt>
<step3>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>When the cruise control switch is pushed to -SET and held in that position while the cruise control system is operating, the ECM sends a throttle valve opening angle 0° demand signal to the throttle with motor body. Then, the vehicle speed is stored and maintained when the cruise control switch is released.</ptxt>
</step3>
<step3>
<ptxt>for 1KD-FTV:</ptxt>
<ptxt>When the cruise control switch is pushed to -SET and held in that position while the cruise control system is operating, the ECM sends a fuel injection quantity signal to the injector driver. Then, the vehicle speed is stored and maintained when the cruise control switch is released.</ptxt>
</step3>
</step2>
<step2>
<ptxt>COAST CONTROL (Vehicle-to-vehicle distance control mode)</ptxt>
<ptxt>When the cruise control switch is pushed to -SET and held while the vehicle-to-vehicle distance control mode is in operation, the stored vehicle speed decreases by approximately 5 km/h or 5 mph.</ptxt>
</step2>
<step2>
<ptxt>TAP-DOWN CONTROL (Constant speed control mode)</ptxt>
<ptxt>When tapping down the cruise control switch to -SET (for approximately 0.6 seconds) while the constant speed control mode is in operation, the stored vehicle speed decreases by approximately 1.6 km/h (1 mph). When the cruise control switch is pushed to -SET and the difference between the driving and stored vehicle speed is more than 5 km/h (3 mph), the vehicle speed is stored and constant speed control is maintained.</ptxt>
</step2>
<step2>
<ptxt>TAP-DOWN CONTROL (Vehicle-to-vehicle distance control mode) (for Europe)</ptxt>
<ptxt>When tapping down the cruise control switch to -SET (for approximately 0.6 seconds) while the vehicle-to-vehicle distance control mode is in operation, the stored vehicle speed decreases by approximately 5 km/h or 5 mph.</ptxt>
</step2>
<step2>
<ptxt>TAP-DOWN CONTROL (Vehicle-to-vehicle distance control mode) (except Europe)</ptxt>
<ptxt>When tapping down the cruise control switch to -SET (for approximately 0.6 seconds) while the vehicle-to-vehicle distance control mode is in operation, the stored vehicle speed decreases by approximately 1 km/h or 1 mph.</ptxt>
</step2>
<step2>
<ptxt>ACCELERATION CONTROL (Constant speed control mode)</ptxt>
<step3>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>When the cruise control switch is pushed to -SET and held in that position while the cruise control system is operating, the ECM sends a throttle valve opening angle 0° demand signal to the throttle with motor body. Then, the vehicle speed is stored and maintained when the cruise control switch is released.</ptxt>
</step3>
<step3>
<ptxt>1KD-FTV:</ptxt>
<ptxt>While the cruise control system is in operation, if the cruise control switch is pressed to +RES, the injector driver increases the fuel injection quantity. When the cruise control switch is released from +RES, the vehicle speed is stored and the vehicle is controlled at a constant speed.</ptxt>
</step3>
</step2>
<step2>
<ptxt>ACCELERATION CONTROL (Vehicle-to-vehicle distance control mode)</ptxt>
<ptxt>When the cruise control switch is pushed to +RES and held while the vehicle-to-vehicle distance control mode is in operation, the stored vehicle speed increases by approximately 5 km/h or 5 mph. Pushing the cruise control switch to +RES while following the vehicle in front with the vehicle-to-vehicle distance control mode on does not increase the actual vehicle speed, but changes only the set vehicle speed.</ptxt>
</step2>
<step2>
<ptxt>TAP-UP CONTROL (Constant speed control mode)</ptxt>
<ptxt>When tapping up the cruise control switch to +RES (for approximately 0.6 seconds) while the constant speed control mode is in operation, the stored vehicle speed increases by approximately 1.6 km/h (1 mph). However, when the difference between the driving and the stored vehicle speeds is more than 5 km/h (3 mph), the stored vehicle speed is not changed.</ptxt>
</step2>
<step2>
<ptxt>TAP-UP CONTROL (Vehicle-to-vehicle distance control mode) (for Europe)</ptxt>
<ptxt>When tapping up the cruise control switch to +RES (for approximately 0.6 seconds) while the vehicle-to-vehicle distance control mode is in operation, the stored vehicle speed increases by approximately 5 km/h or 5 mph.</ptxt>
</step2>
<step2>
<ptxt>TAP-UP CONTROL (Vehicle-to-vehicle distance control mode) (except Europe)</ptxt>
<ptxt>When tapping up the cruise control switch to +RES (for approximately 0.6 seconds) while the vehicle-to-vehicle distance control mode is in operation, the stored vehicle speed increases by approximately 1 km/h or 1 mph.</ptxt>
</step2>
<step2>
<ptxt>SHIFT DOWN CONTROL</ptxt>
<list1 type="unordered">
<item>
<ptxt>Overdrive may be canceled automatically when using cruise control while driving on inclined roads, etc.</ptxt>
</item>
<item>
<ptxt>for 1GR-FE:</ptxt>
<ptxt>After overdrive is canceled, if the system determines that the road is no longer inclined based on the throttle value opening amount overdrive will be restored.</ptxt>
</item>
<item>
<ptxt>for 1KD-FTV:</ptxt>
<ptxt>After overdrive is canceled, if the system determines that the road is no longer inclined based on the fuel injection volume, overdrive is restored.</ptxt>
</item>
<item>
<ptxt>If overdrive is canceled during an ACCEL or RESUME operation, overdrive is restored after the ACCEL or RESUME operation is completed.</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>MANUAL CANCEL CONTROL</ptxt>
<ptxt>Performing any of the following cancels cruise control while it is in operation (the stored vehicle speed in the ECM is maintained).</ptxt>
<list1 type="unordered">
<item>
<ptxt>Depressing the brake pedal</ptxt>
</item>
<item>
<ptxt>Moving the shift lever from D to N, or selecting 3rd, 2nd or 1st range with the shift lever in S</ptxt>
</item>
<item>
<ptxt>Driving the vehicle while the vehicle stability control system is operating</ptxt>
</item>
<item>
<ptxt>When the ECM detects a cruise control cancel signal from the four wheel drive control ECU</ptxt>
</item>
<item>
<ptxt>Pulling the cruise control switch to CANCEL</ptxt>
</item>
<item>
<ptxt>Pushing the cruise control switch off (the stored vehicle speed in the ECM is not maintained)</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>RES (RESUME) CONTROL</ptxt>
<list1 type="unordered">
<item>
<ptxt>If cruise control operation is canceled with the stop light switch, CANCEL switch, etc., and if the driving speed is within the set speed range, pushing the cruise control switch to +RES restores the vehicle speed memorized at the time of cancellation, and initiates constant speed control.</ptxt>
</item>
<item>
<ptxt>Vehicle-to-vehicle distance control mode</ptxt>
<ptxt>When the vehicle-to-vehicle distance control mode is operating, if the vehicle in front changes lanes, your vehicle changes lanes, or another situation occurs so that there is no vehicle in front, the vehicle speed automatically increases slowly to the stored vehicle speed. At this time, if the +RES switch is pressed, the vehicle speed increases quickly to the stored vehicle speed.</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>BRAKE CONTROL</ptxt>
<ptxt>The driving support ECU determines the distance to the vehicle in front, relative speed, target decreasing speed and deceleration rate. Based on this data, the ECU may transmit a brake demand signal to the skid control ECU.</ptxt>
</step1>
<step1>
<ptxt>AUTO CANCEL (FAIL-SAFE)</ptxt>
<ptxt>This system has an automatic cancellation function (fail-safe) (See page <xref label="Seep01" href="RM0000044H300AX"/>).</ptxt>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>