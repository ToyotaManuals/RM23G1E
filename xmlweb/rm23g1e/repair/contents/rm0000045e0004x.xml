<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12008_S000G" variety="S000G">
<name>5L-E FUEL</name>
<ttl id="12008_S000G_7B8ZI_T0096" variety="T0096">
<name>FUEL SUB TANK</name>
<para id="RM0000045E0004X" category="A" type-id="30014" name-id="FU540-03" from="201207" to="201210">
<name>INSTALLATION</name>
<subpara id="RM0000045E0004X_01" type-id="01" category="01">
<s-1 id="RM0000045E0004X_01_0001" proc-id="RM23G0E___00005XV00000">
<ptxt>INSTALL FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel tank breather hose to the fuel sub tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0002" proc-id="RM23G0E___00005XW00000">
<ptxt>INSTALL FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fuel tank to filler pipe hose to the fuel sub tank.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0003" proc-id="RM23G0E___00005XX00000">
<ptxt>INSTALL FUEL TANK VENT TUBE ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install a new gasket and fuel tank vent tube with the 8 screws.</ptxt>
<torque>
<torqueitem>
<t-value1>4.0</t-value1>
<t-value2>41</t-value2>
<t-value3>35</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Attach the wire harness clamp.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0004" proc-id="RM23G0E___00005XY00000">
<ptxt>INSTALL FUEL SUB TANK SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set the fuel sub tank on a transmission jack and lift up the transmission jack.</ptxt>
</s2>
<s2>
<ptxt>Connect the 2 fuel tank bands with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>40</t-value1>
<t-value2>408</t-value2>
<t-value4>30</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the fuel sender gauge connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0005" proc-id="RM23G0E___00005XZ00000">
<ptxt>CONNECT FUEL TANK BREATHER HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank breather hose to the filler pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0006" proc-id="RM23G0E___00005Y000000">
<ptxt>CONNECT FUEL TANK TO FILLER PIPE HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel tank to filler pipe hose to the filler pipe.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0007" proc-id="RM23G0E___00005Y100000">
<ptxt>CONNECT FUEL HOSE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the fuel evaporation hose, fuel breather hose and 2 fuel hoses.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0008" proc-id="RM23G0E___00005Y200000">
<ptxt>INSTALL NO. 2 FUEL TANK PROTECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 fuel tank protector with the 5 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0009" proc-id="RM23G0E___00005Y300000">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM0000045E0004X_01_0012" proc-id="RM23G0E___00004X500000">
<ptxt>BLEED AIR FROM FUEL SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using the hand pump mounted on the fuel filter cap, bleed the air from the fuel system. Continue pumping until the pump resistance increases.</ptxt>
<figure>
<graphic graphicname="A225897" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1></s-1>
<s-1 id="RM0000045E0004X_01_0013" proc-id="RM23G0E___00004XB00000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Check that there are no fuel leaks anywhere in the fuel system after performing maintenance.</ptxt>
<atten4>
<ptxt>When checking for fuel leaks, make sure that there is pressure in the fuel line.</ptxt>
</atten4>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>