<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B7_T00KV" variety="T00KV">
<name>ENTRY AND START SYSTEM (for Entry Function)</name>
<para id="RM000000XUI0ESX" category="J" type-id="800Q2" name-id="TD4YH-03" from="201210">
<dtccode/>
<dtcname>Driver Side Door Entry Lock and Unlock Functions do not Operate</dtcname>
<subpara id="RM000000XUI0ESX_01" type-id="60" category="03" proc-id="RM23G0E___0000ES300001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the driver side door entry lock and unlock functions do not operate, one of the following may be malfunctioning: 1) the power door lock control system, 2) the front door outside handle assembly (for driver side), or 3) the certification ECU.</ptxt>
</content5>
</subpara>
<subpara id="RM000000XUI0ESX_02" type-id="32" category="03" proc-id="RM23G0E___0000ES400001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B243941E01" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000XUI0ESX_03" type-id="51" category="05" proc-id="RM23G0E___0000ES500001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>The entry and start system (for Entry Function) uses a multiplex communication system (LIN communication system) and the CAN communication system. Inspect the communication function by following How to Proceed with Troubleshooting (See page <xref label="Seep01" href="RM000000XU70CAX"/>). Troubleshoot the entry and start system (for Entry Function) after confirming that the communication systems are functioning properly.</ptxt>
</item>
<item>
<ptxt>When using the intelligent tester with the engine switch off to troubleshoot: Connect the intelligent tester to the DLC3 and turn a courtesy light switch on and off at 1.5-second intervals until communication between the intelligent tester and vehicle begins.</ptxt>
</item>
<item>
<ptxt>Check that there are no electrical key transmitters in the vehicle.</ptxt>
</item>
<item>
<ptxt>Before performing the inspection, check that DTC B1242 (wireless door lock control) is not output (See page <xref label="Seep02" href="RM000002BZF045X"/>).</ptxt>
</item>
<item>
<ptxt>When checking the entry lock operation multiple times, the lock operation may be limited to 2 consecutive operations depending on the settings. In order to perform the entry lock operation 3 or more times, an unlock operation must be performed once (any type of unlock operation is sufficient). However, only consecutive entry lock operations are limited. Using the wireless lock or other types of lock operations, it is possible to perform consecutive lock operations without this limitation.</ptxt>
</item>
<item>
<ptxt>Before replacing the certification ECU, refer to the Service Bulletin.</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XUI0ESX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XUI0ESX_05_0020" proc-id="RM23G0E___0000ES600001">
<testtitle>CHECK POWER DOOR LOCK OPERATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>When the door control switch on the multiplex network master switch assembly is operated, check that the doors unlock and lock according to the switch operation (See page <xref label="Seep01" href="RM000002T6K05GX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Door locks operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ESX_05_0026" fin="false">OK</down>
<right ref="RM000000XUI0ESX_05_0034" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0026" proc-id="RM23G0E___0000ES700001">
<testtitle>CHECK WAVE ENVIRONMENT</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bring the electrical key transmitter approximately 0.3 m (0.984 ft.) from the door outside handle (for driver side) and perform a driver side door entry lock and unlock operation check (See page <xref label="Seep01" href="RM000002U9W05DX"/>).</ptxt>
<atten3>
<ptxt>If the key is brought within 0.2 m (0.656 ft.) of the door handle, communication is not possible.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When the electrical key transmitter is brought near the driver side door outside handle, the possibility of wave interference decreases, and it can be determined if wave interference is causing the problem symptom.</ptxt>
</item>
<item>
<ptxt>If the operation check is normal, the possibility of wave interference is high. Also, added vehicle components may cause wave interference. If installed, remove them and perform the operation check.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>Entry function operates normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ESX_05_0039" fin="true">OK</down>
<right ref="RM000000XUI0ESX_05_0050" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0050" proc-id="RM23G0E___0000ESC00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (DOOR LOCK POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Body / Main Body / Data List.</ptxt>
</test1>
<test1>
<ptxt>Read the Data List according to the display on the intelligent tester.</ptxt>
<table pgwide="1">
<title>Main Body</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Measurement Item/Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Normal Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>FR Door Lock Pos*1</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front RH side door lock position switch signal / UNLOCK or LOCK</ptxt>
</entry>
<entry>
<ptxt>UNLOCK: Front RH side door unlocked</ptxt>
<ptxt>LOCK: Front RH side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>FL Door Lock Pos*2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Front LH side door lock position switch signal / UNLOCK or LOCK</ptxt>
</entry>
<entry>
<ptxt>UNLOCK: Front LH side door unlocked</ptxt>
<ptxt>LOCK: Front LH side door locked</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for RHD</ptxt>
</item>
<item>
<ptxt>*2: for LHD</ptxt>
</item>
</list1>
<spec>
<title>OK</title>
<specitem>
<ptxt>On the intelligent tester screen, the display changes between LOCK and UNLOCK as shown in the chart above.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ESX_05_0029" fin="false">OK</down>
<right ref="RM000000XUI0ESX_05_0051" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0029" proc-id="RM23G0E___0000ES900001">
<testtitle>CHECK HARNESS AND CONNECTOR (FRONT DOOR OUTSIDE HANDLE - CERTIFICATION ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the O1*1 or N1*2 handle connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Disconnect the G38 ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>O1-3 (ANT2) - G38-4 (CG1B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O1-6 (ANT1) - G38-3 (CLG1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O1-5 (B) - G38-32 (POS1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O1-2 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-4 (CG1B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-3 (CLG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-32 (POS1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.36in"/>
<colspec colname="COL2" colwidth="1.36in"/>
<colspec colname="COL3" colwidth="1.41in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>N1-3 (ANT2) - G38-4 (CG1B)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N1-6 (ANT1) - G38-3 (CLG1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N1-5 (B) - G38-32 (POS1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N1-2 (GND) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-4 (CG1B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-3 (CLG1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G38-32 (POS1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ESX_05_0030" fin="false">OK</down>
<right ref="RM000000XUI0ESX_05_0036" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0030" proc-id="RM23G0E___0000ESA00001">
<testtitle>CHECK CERTIFICATION ECU (OUTPUT SIGNAL)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the O1*1 or N1*2 handle connector.</ptxt>
<figure>
<graphic graphicname="B199753E38" width="2.775699831in" height="1.771723296in"/>
</figure>
<list1 type="nonmark">
<item>
<ptxt>*1: for LHD</ptxt>
</item>
<item>
<ptxt>*2: for RHD</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<subtitle>for LHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>O1-3 (ANT2) - O1-6 (ANT1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, all doors closed, key not in cabin and transmitter lock switch not pressed → pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No pulse generation → Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>O1-5 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off → on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9 to 14 V → Below 2 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<subtitle>for RHD</subtitle>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>N1-3 (ANT2) - N1-6 (ANT1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch off, all doors closed, key not in cabin and transmitter lock switch not pressed → pressed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No pulse generation → Pulse generation</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>N1-5 (B) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Engine switch off → on (IG)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>9 to 14 V → Below 2 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry>
<ptxt>for LHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry>
<ptxt>for RHD</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Front Door Outside Handle Assembly [for Driver Side])</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ESX_05_0027" fin="false">OK</down>
<right ref="RM000000XUI0ESX_05_0040" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0027" proc-id="RM23G0E___0000ES800001">
<testtitle>REPLACE FRONT DOOR OUTSIDE HANDLE ASSEMBLY (FOR DRIVER SIDE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the front door outside handle assembly (for driver side) with a new or normally functioning one (See page <xref label="Seep01" href="RM0000011QF0CPX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ESX_05_0049" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0049" proc-id="RM23G0E___0000ESB00001">
<testtitle>CHECK FRONT DOOR OUTSIDE HANDLE ASSEMBLY (OPERATION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check that the entry functions operate normally (See page <xref label="Seep01" href="RM000002U9W05DX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Entry functions operate normally.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000XUI0ESX_05_0044" fin="true">OK</down>
<right ref="RM000000XUI0ESX_05_0040" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0034">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM<xref label="Seep01" href="RM0000011G908TX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0039">
<testtitle>AFFECTED BY WAVE INTERFERENCE</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0036">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0040">
<testtitle>REPLACE CERTIFICATION ECU</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0044">
<testtitle>END (FRONT DOOR OUTSIDE HANDLE ASSEMBLY IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000000XUI0ESX_05_0051">
<testtitle>GO TO POWER DOOR LOCK CONTROL SYSTEM (Proceed to Only Driver Door Lock/Unlock Functions do not Operate)<xref label="Seep01" href="RM0000011GC05OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>