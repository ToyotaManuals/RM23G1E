<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000000W0D0B6X" category="J" type-id="305JX" name-id="ES0PZ0-118" from="201210">
<dtccode/>
<dtcname>Engine Knocking or Rattling</dtcname>
<subpara id="RM000000W0D0B6X_01" type-id="60" category="03" proc-id="RM23G0E___00002MG00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Malfunction Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Main Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Related Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Knocking and abnormal sound due to extremely rich combustion</ptxt>
</item>
<item>
<ptxt>Abnormal sound due to friction between parts</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="nonmark">
<item>
<ptxt>(a) Injector assembly malfunctions</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Injector sliding malfunction</ptxt>
</item>
<item>
<ptxt>Injector stuck closed</ptxt>
</item>
<item>
<ptxt>Injector stuck open</ptxt>
</item>
<item>
<ptxt>Deposits in injector</ptxt>
</item>
</list2>
<list2 type="unordered">
<item>
<ptxt>Injector circuit malfunction</ptxt>
</item>
</list2>
</list1>
<list1 type="nonmark">
<item>
<ptxt>(b) Abnormal common rail pressure</ptxt>
</item>
<list2 type="unordered">
<item>
<ptxt>Suction control valve</ptxt>
</item>
<item>
<ptxt>Fuel pulsation sound</ptxt>
</item>
<item>
<ptxt>Air in fuel</ptxt>
</item>
</list2>
</list1>
<list1 type="nonmark">
<item>
<ptxt>(c) Friction between parts</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>(d) Compression pressure</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Injector compensation codes</ptxt>
</item>
<item>
<ptxt>Fuel leakage</ptxt>
</item>
<item>
<ptxt>Intake air system leakage</ptxt>
</item>
<item>
<ptxt>Intake air system blockage</ptxt>
</item>
<item>
<ptxt>EGR system</ptxt>
</item>
<item>
<ptxt>Throttle valve system</ptxt>
</item>
<item>
<ptxt>Fuel pressure sensor</ptxt>
</item>
<item>
<ptxt>Manifold absolute pressure sensor</ptxt>
</item>
<item>
<ptxt>Mass air flow meter</ptxt>
</item>
<item>
<ptxt>Atmospheric pressure sensor (built into ECM)</ptxt>
</item>
<item>
<ptxt>Vehicle modifications</ptxt>
</item>
<item>
<ptxt>Low quality fuel</ptxt>
</item>
<item>
<ptxt>Lack of fuel</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>Specified values in the following troubleshooting flowchart are for reference only. Variations in the Data List values may occur depending on the measuring conditions or the vehicle's age. Do not judge the vehicle to be normal even when the Data List values indicate a standard level. There are possibly  some concealed factors of the malfunction.</ptxt>
</item>
<item>
<ptxt>Check that the vehicle has not been modified in any way prior to the vehicle inspection.</ptxt>
</item>
</list1>
</atten4>
</content5>
</subpara>
<subpara id="RM000000W0D0B6X_02" type-id="51" category="05" proc-id="RM23G0E___00002MH00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>This troubleshooting procedure checks for knocking and rattling.</ptxt>
</item>
<item>
<ptxt>Knocking is most likely to occur while the engine is idling.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07MX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000W0D0B6X_03" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000W0D0B6X_03_0001" proc-id="RM23G0E___00002MI00001">
<testtitle>CHECK SOUND AREA</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Find the source of the abnormal sound using a mechanic's stethoscope.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Combustion noise from engine</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Noise from specific part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0065" fin="false">A</down>
<right ref="RM000000W0D0B6X_03_0046" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0065" proc-id="RM23G0E___00002ML00001">
<testtitle>READ OUTPUT DTCS (RELATING TO ENGINE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>No DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Engine related DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0072" fin="false">A</down>
<right ref="RM000000W0D0B6X_03_0107" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0072" proc-id="RM23G0E___00002MN00001">
<testtitle>CHECK INJECTOR COMPENSATION CODE</testtitle>
<content6 releasenbr="1">
<atten3>
<ptxt>Injector compensation codes are unique, 30-digit, alphanumeric values printed on the head portion of each injector assembly. If an incorrect injector compensation code is input into the ECM, the engine may rattle or engine idling may become rough. In addition, engine failure may occur and the life of the engine may be shortened.</ptxt>
</atten3>
<test1>
<ptxt>Check the injector compensation code (See page <xref label="Seep01" href="RM0000012XK07MX_02_0006"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Compensation codes of the installed injector assemblies are the same as the compensation codes registered in the ECM.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0124" fin="false">OK</down>
<right ref="RM000000W0D0B6X_03_0110" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0124" proc-id="RM23G0E___00002MY00001">
<testtitle>TAKE SNAPSHOT DURING IDLING AND 4000 RPM (AFTER ENGINE WARMED UP)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data.</ptxt>
</test1>
<test1>
<ptxt>Take a snapshot of the following Data List items.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A snapshot can be used to compare vehicle data from the time of the malfunction to normal data and is very useful for troubleshooting. The data in the list below is that of a normal vehicle, but as the data varies between individual vehicles, this data should only be used for reference.</ptxt>
</item>
<item>
<ptxt>Graphs like the ones shown below can be displayed by transferring the stored snapshot data from the tester to a PC. Intelligent Viewer must be installed on the PC.</ptxt>
</item>
<item>
<ptxt>Check the Data List at idling and at 4000 rpm with no load after the engine is warmed up.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0125" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0125" proc-id="RM23G0E___00002MZ00001">
<testtitle>CHECK SNAPSHOT (INJECTION FEEDBACK VAL #1 TO #4 AND INJECTION VOLUME)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check Injection Feedback Val #1 to #4 and Injection Volume in the snapshot taken when the engine was idling and at 4000 rpm with no load.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Injection Feedback Val for at least one cylinder is more than +3 mm<sup>3</sup>/st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Injection Volume is 10 mm<sup>3</sup>/st or less at 4000 rpm</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When there is a problem with the operation of an injector assembly due to foreign matter in the inside of the injector assembly, etc., the fuel injection volume decreases. As a result, the ECM gives instructions to increase the fuel injection volume, which causes Injection Feedback Val to increase.</ptxt>
</item>
<item>
<ptxt>The ECM controls the system so that the sum of Injection Feedback Val for all of the cylinders is approximately 0 mm<sup>3</sup>/st. Even if the value of Injection Feedback Val for a cylinder is less than -3.0 mm<sup>3</sup>/st (-3.0 mm<sup>3</sup>/st is the lowest normal value), as long as the value of Injection Feedback Val for each of the other cylinders is 3.0 mm<sup>3</sup>/st or less, the injector assemblies are not malfunctioning.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0126" fin="false">C</down>
<right ref="RM000000W0D0B6X_03_0121" fin="false">A</right>
<right ref="RM000000W0D0B6X_03_0129" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0126" proc-id="RM23G0E___00002N000001">
<testtitle>CHECK SNAPSHOT (FUEL PRESS AND TARGET COMMON RAIL PRESSURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check Fuel Press and Target Common Rail Pressure in the snapshot taken when the engine was warmed up and idling.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Fuel Press is within +/-5000 kPa of Target Common Rail Pressure.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0091" fin="false">OK</down>
<right ref="RM000000W0D0B6X_03_0054" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0091" proc-id="RM23G0E___00002MO00001">
<testtitle>CHECK THE TEMPERATURE WHEN KNOCKING OCCURS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the temperature when knocking trouble occurs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Knocking occurs only in cold engine</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Knocking occurs both in cold and warmed up engine</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0093" fin="false">A</down>
<right ref="RM000000W0D0B6X_03_0070" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0093" proc-id="RM23G0E___00002MP00001">
<testtitle>INSPECT GLOW PLUG (RESISTANCE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the glow plug (See page <xref label="Seep01" href="RM0000044ZK00EX_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0127" fin="false">OK</down>
<right ref="RM000000W0D0B6X_03_0113" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0127" proc-id="RM23G0E___00002N100001">
<testtitle>TAKE SNAPSHOT DURING IDLING (WHEN ENGINE COLD)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Make sure that the engine is cold so that the snapshot can be taken under the correct conditions.</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data.</ptxt>
</test1>
<test1>
<ptxt>Take a snapshot of the following Data List items.</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A snapshot can be used to compare vehicle data from the time of the malfunction to normal data and is very useful for troubleshooting. The data in the list below is that of a normal vehicle, but as the data varies between individual vehicles, this data should only be used for reference.</ptxt>
</item>
<item>
<ptxt>Graphs like the ones shown below can be displayed by transferring the stored snapshot data from the tester to a PC. Intelligent Viewer must be installed on the PC.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0128" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0128" proc-id="RM23G0E___00002N200001">
<testtitle>CHECK SNAPSHOT (INJECTION FEEDBACK VAL #1 TO #4)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check Injection Feedback Val #1 to #4 in the snapshot taken when the engine was idling.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>Injection Feedback Val for at least one cylinder is more than +3 mm<sup>3</sup>/st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When there is a problem with the operation of an injector assembly due to foreign matter in the inside of the injector assembly, etc., the fuel injection volume decreases. As a result, the ECM gives instructions to increase the fuel injection volume, which causes Injection Feedback Val to increase.</ptxt>
</item>
<item>
<ptxt>The ECM controls the system so that the sum of Injection Feedback Val for all of the cylinders is approximately 0 mm<sup>3</sup>/st. Even if the value of Injection Feedback Val for a cylinder is less than -3.0 mm<sup>3</sup>/st (-3.0 mm<sup>3</sup>/st is the lowest normal value), as long as the value of Injection Feedback Val for each of the other cylinders is 3.0 mm<sup>3</sup>/st or less, the injector assemblies are not malfunctioning.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<right ref="RM000000W0D0B6X_03_0041" fin="false">A</right>
<right ref="RM000000W0D0B6X_03_0070" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0129" proc-id="RM23G0E___00002N300001">
<testtitle>REPLACE INJECTOR ASSEMBLIES OF ALL CYLINDERS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the injector assemblies (See page <xref label="Seep01" href="RM0000044TN00SX"/>).</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injector assembly for a cylinder, always be sure to use a new injection pipe.</ptxt>
</item>
<item>
<ptxt>Follow the procedure in the repair manual and temporarily install the injection pipes and nozzle leakage pipe, and then correctly position the injector assemblies. After that, tighten parts according to the torque specifications.</ptxt>
</item>
<item>
<ptxt>If the installation procedure is not performed correctly, injector assemblies may become out of position, which may cause the injector assemblies to deteriorate, resulting in malfunctions.</ptxt>
</item>
<item>
<ptxt>If an injector assembly deteriorates and malfunctions, other problems such as knocking, rough idle, etc. may occur.</ptxt>
</item>
<item>
<ptxt>If an injector assembly becomes out of position, it is possible that the seal between the injector assembly and injection pipe may become incomplete, resulting in a fuel leak.</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<right ref="RM000000W0D0B6X_03_0130" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0121" proc-id="RM23G0E___00002MW00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (CHECK THE CYLINDER COMPRESSION)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>Use this Active Test to help determine whether a cylinder has compression loss or not.</ptxt>
</atten4>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Check the Cylinder Compression / Data List / Compression / Engine Speed of Cyl #1 to #4.</ptxt>
</test1>
<test1>
<ptxt>Check the engine speed during the Active Test.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Except below</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>The values of Engine Speed Cyl #1 to #4 are within +/-10 rpm of each other.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When cranking, if the speed of a cylinder is approximately 100 rpm more than the other cylinders, there is probably a complete loss of compression in that cylinder.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0122" fin="false">A</down>
<right ref="RM000000W0D0B6X_03_0041" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0122" proc-id="RM23G0E___00002MX00001">
<testtitle>CHECK CYLINDER COMPRESSION PRESSURE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the cylinder compression pressure (See page <xref label="Seep01" href="RM000001473025X_01_0002"/>).</ptxt>
<atten4>
<ptxt>When compression is low, there may be cracks in the piston or the injector may be installed improperly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0041" fin="false">OK</down>
<right ref="RM000000W0D0B6X_03_0123" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0041" proc-id="RM23G0E___00002MJ00001">
<testtitle>REPLACE INJECTOR ASSEMBLY OF MALFUNCTIONING CYLINDER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the injector assembly of the malfunctioning cylinder (See page <xref label="Seep01" href="RM0000044TN00SX"/>).</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injector assembly for a cylinder, always be sure to use a new injection pipe.</ptxt>
</item>
<item>
<ptxt>Follow the procedure in the repair manual and temporarily install the injection pipes and nozzle leakage pipe, and then correctly position the injector assemblies. After that, tighten parts according to the torque specifications.</ptxt>
</item>
<item>
<ptxt>If the installation procedure is not performed correctly, injector assemblies may become out of position, which may cause the injector assemblies to deteriorate, resulting in malfunctions.</ptxt>
</item>
<item>
<ptxt>If an injector assembly deteriorates and malfunctions, other problems such as knocking, rough idle, etc. may occur.</ptxt>
</item>
<item>
<ptxt>If an injector assembly becomes out of position, it is possible that the seal between the injector assembly and injection pipe may become incomplete, resulting in a fuel leak.</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0130" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0130" proc-id="RM23G0E___00002N400001">
<testtitle>REPLACE FUEL FILTER ELEMENT SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the fuel filter element sub-assembly (See page <xref label="Seep01" href="RM0000045HW004X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0110" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0110" proc-id="RM23G0E___00002MU00001">
<testtitle>REGISTER INJECTOR COMPENSATION CODE AND PERFORM PILOT QUANTITY LEARNING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the injector compensation codes (See page <xref label="Seep01" href="RM0000012XK07MX_02_0003"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the fuel injector pilot quantity learning (See page <xref label="Seep02" href="RM0000012XK07MX_02_0009"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0117" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0117" proc-id="RM23G0E___00002MV00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0097" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0097" proc-id="RM23G0E___00002MQ00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the knocking has been successfully repaired.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0045" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0054" proc-id="RM23G0E___00002MK00001">
<testtitle>REPLACE SUCTION CONTROL VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the suction control valve (See page <xref label="Seep01" href="RM000004MF600DX"/>).</ptxt>
<atten4>
<ptxt>If the common rail pressure becomes too high when transitioning due to problems with SCV operation, knocking noise worsens.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0099" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0099" proc-id="RM23G0E___00002MR00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0100" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0100" proc-id="RM23G0E___00002MS00001">
<testtitle>PERFORM FUEL SUPPLY PUMP INITIALIZATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform fuel supply pump initialization (See page <xref label="Seep01" href="RM000000TIN05LX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0103" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0103" proc-id="RM23G0E___00002MT00001">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check whether the knocking has been successfully repaired by starting the engine.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0045" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0070" proc-id="RM23G0E___00002MM00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (MAP AND ATMOSPHERE PRESSURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / MAP and Atmosphere Pressure.</ptxt>
</test1>
<test1>
<ptxt>Compare MAP to Atmosphere Pressure when the engine switch is on (IG) (do not start the engine).</ptxt>
<spec>
<title>Standard</title>
<specitem>
<ptxt>Difference between MAP and Atmosphere Pressure is less than 8 kPa.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If MAP and Atmosphere Pressure have the same value, both are normal. If there is a difference of 8 kPa or more, compare the values to the atmospheric pressure for that day. The sensor whose deviation is the greatest is malfunctioning.</ptxt>
</item>
<item>
<ptxt>Standard atmospheric pressure is 101 kPa. For every 100 m increase in elevation, pressure drops by 1 kPa. This varies by weather.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="5.31in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>MAP and Atmosphere Pressure have same value</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>MAP is different from actual atmospheric pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>Atmosphere Pressure is different from actual atmospheric pressure</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0131" fin="false">A</down>
<right ref="RM000000W0D0B6X_03_0108" fin="true">B</right>
<right ref="RM000000W0D0B6X_03_0109" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0131" proc-id="RM23G0E___00002N500001">
<testtitle>BASIC INSPECTION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the fuel quality.</ptxt>
</test1>
<test1>
<ptxt>Check the fuel for air.</ptxt>
</test1>
<test1>
<ptxt>Check the fuel system for blockages.</ptxt>
</test1>
<test1>
<ptxt>Check the air filter.</ptxt>
</test1>
<test1>
<ptxt>Check the engine oil.</ptxt>
</test1>
<test1>
<ptxt>Check the engine coolant.</ptxt>
</test1>
<test1>
<ptxt>Check the engine idling speed and maximum engine speed.</ptxt>
</test1>
<test1>
<ptxt>Check the vacuum pump.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000W0D0B6X_03_0045" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0045">
<testtitle>END</testtitle>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0046">
<testtitle>REPAIR OR REPLACE MALFUNCTIONING PARTS</testtitle>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0107">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000031HW050X"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0113">
<testtitle>REPLACE GLOW PLUG<xref label="Seep01" href="RM000000JIT00WX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0123">
<testtitle>CHECK ENGINE TO DETERMINE CAUSE OF LOW COMPRESSION</testtitle>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0109">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001YX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000W0D0B6X_03_0108">
<testtitle>REPLACE MANIFOLD ABSOLUTE PRESSURE SENSOR<xref label="Seep01" href="RM0000045B300AX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>