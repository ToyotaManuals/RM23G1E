<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12051_S001X" variety="S001X">
<name>DOOR LOCK</name>
<ttl id="12051_S001X_7B9AS_T00KG" variety="T00KG">
<name>WIRELESS DOOR LOCK CONTROL SYSTEM (w/ Entry and Start System)</name>
<para id="RM000000XX804KX" category="D" type-id="303F2" name-id="DL024-94" from="201210">
<name>SYSTEM DESCRIPTION</name>
<subpara id="RM000000XX804KX_z0" proc-id="RM23G0E___0000EIL00001">
<content5 releasenbr="1">
<step1>
<ptxt>WIRELESS DOOR LOCK CONTROL SYSTEM DESCRIPTION</ptxt>
<step2>
<ptxt>This system locks/unlocks the doors remotely. The wireless door lock control system has the following features:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The certification ECU performs the code identification process and the main body ECU controls the door locks. A serial data link is provided for communication between the certification ECU and main body ECU.</ptxt>
</item>
<item>
<ptxt>A key-integrated type transmitter is used and it contains the following 3 switches: the lock switch, unlock switch and glass hatch open switch*.</ptxt>
</item>
<item>
<ptxt>An LED is mounted on the transmitter to show if the battery is depleted.</ptxt>
</item>
</list1>
<list1 type="nonmark">
<item>
<ptxt>*: w/ Glass Hatch Opener System</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>FUNCTION OF MAIN COMPONENTS</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Door control transmitter</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Contains the lock, unlock and glass hatch open*1 switches.</ptxt>
</item>
<item>
<ptxt>Transmits weak electric waves (recognition codes and function codes) to the door control receiver.</ptxt>
</item>
<item>
<ptxt>Illuminates the indicator light (LED) during transmission.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Door control receiver</ptxt>
</entry>
<entry valign="middle">
<ptxt>Receives weak electric waves (recognition codes and function codes) from the door control transmitter via the electrical key antenna and changes the waves to code data.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front door courtesy light switch</ptxt>
<ptxt>Rear door courtesy light switch*2</ptxt>
<ptxt>Back door courtesy light switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turns on when the door is opened and off when the door is closed. Outputs the door status (open or closed) to the main body ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Door lock position switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transmits the door lock positions of each door to the main body ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Wireless door lock buzzer</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sounds when the door ajar warning function is operating.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Certification ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Matches the code data with the registered code and outputs the code data to main body ECU.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Glass Hatch Opener System</ptxt>
</item>
<item>
<ptxt>*2: for 5 Door</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>SYSTEM FUNCTION</ptxt>
<step2>
<ptxt>The wireless door lock control system has the following functions:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Function</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>All door lock function</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressing the lock switch locks all the doors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>All door unlock function</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressing the unlock switch unlocks all the doors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Glass hatch open function*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Pressing the glass hatch open switch for approximately 1 second or more opens the glass hatch.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Chattering prevention function</ptxt>
</entry>
<entry valign="middle">
<ptxt>If a switch is pressed, the corresponding switch operation is performed once. If a switch is held down, the corresponding switch operation occurs only once and does not repeatedly activate. When pressing a switch at 1 second intervals, the corresponding operation activates once for every press of the switch.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Answer-back function</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>When the doors are locked by wireless operation, the hazard warning lights flash once and the wireless door lock buzzer*2 sounds once.</ptxt>
</item>
<item>
<ptxt>When the doors are unlocked by wireless operation, the hazard warning lights flash twice and the wireless door lock buzzer*2 sounds twice.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Automatic lock function</ptxt>
</entry>
<entry valign="middle">
<ptxt>If no doors are opened within 30 seconds after they are unlocked by the door control transmitter, all the doors lock again automatically.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Illuminated entry function</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>When the map light door switch is on and all the doors are locked, pressing the unlock switch causes the interior light illumination to turn on and all the doors to unlock simultaneously.</ptxt>
</item>
<item>
<ptxt>If the doors have not been opened, the interior light turns off after approximately 15 seconds.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Door ajar warning function</ptxt>
</entry>
<entry valign="middle">
<ptxt>If any door is open or ajar, pressing the lock switch causes the wireless door lock buzzer to sound for approximately 5 seconds.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Memory call function*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>If the unlock switch of a memory call registered transmitter is pressed and the driver door is opened, the following parts move to the memory call position: the front seat, steering wheel and outer mirrors.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: w/ Glass Hatch Opener System</ptxt>
</item>
<item>
<ptxt>*2: w/ Wireless Buzzer Answer-back</ptxt>
</item>
<item>
<ptxt>*3: w/ Memory</ptxt>
</item>
</list1>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>