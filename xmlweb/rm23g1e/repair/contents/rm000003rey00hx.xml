<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12013_S000Y" variety="S000Y">
<name>2TR-FE STARTING</name>
<ttl id="12013_S000Y_7B92T_T00CH" variety="T00CH">
<name>STARTER (for 1.4 kW Type)</name>
<para id="RM000003REY00HX" category="A" type-id="80001" name-id="ST5T6-02" from="201210">
<name>REMOVAL</name>
<subpara id="RM000003REY00HX_01" type-id="01" category="01">
<s-1 id="RM000003REY00HX_01_0001" proc-id="RM23G0E___00006ZA00001">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000003REY00HX_01_0006" proc-id="RM23G0E___00005BE00001">
<ptxt>REMOVE REAR ENGINE UNDER COVER ASSEMBLY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the 4 bolts and rear engine under cover.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003REY00HX_01_0003" proc-id="RM23G0E___00006GO00000">
<ptxt>REMOVE TRANSMISSION OIL FILLER TUBE SUB-ASSEMBLY (for Automatic Transmission)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C213708" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the transmission oil dipstick.</ptxt>
</s2>
<s2>
<ptxt>Remove the 2 bolts and oil filler tube.</ptxt>
</s2>
<s2>
<ptxt>Remove the O-ring from the oil filler tube.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003REY00HX_01_0004" proc-id="RM23G0E___00006ZB00001">
<ptxt>DISCONNECT CLUTCH RELEASE CYLINDER ASSEMBLY (for Manual Transmission)
</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="C215699" width="2.775699831in" height="1.771723296in"/>
</figure>
<s2>
<ptxt>Remove the 2 bolts and disconnect the release cylinder assembly.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000003REY00HX_01_0005" proc-id="RM23G0E___00006ZC00001">
<ptxt>REMOVE STARTER ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the starter connector.</ptxt>
<figure>
<graphic graphicname="A224311" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Remove the terminal cap.</ptxt>
</s2>
<s2>
<ptxt>Remove the nut and disconnect the starter wire.</ptxt>
</s2>
<s2>
<ptxt>for Automatic Transmission:</ptxt>
<ptxt>Remove the 2 bolts and starter assembly.</ptxt>
<figure>
<graphic graphicname="A224312" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for Manual Transmission:</ptxt>
<s3>
<ptxt>Remove the bolt and disconnect the clutch flexible hose bracket.</ptxt>
<figure>
<graphic graphicname="A226197E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry>
<ptxt>Clutch Flexible Hose Bracket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s3>
<s3>
<ptxt>Remove the bolt and starter assembly.</ptxt>
</s3>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>