<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0005" variety="S0005">
<name>1GR-FE ENGINE CONTROL</name>
<ttl id="12005_S0005_7B8VO_T005C" variety="T005C">
<name>SFI SYSTEM</name>
<para id="RM000000PFX0TZX" category="C" type-id="302IJ" name-id="ESMC0-07" from="201207" to="201210">
<dtccode>P2119</dtccode>
<dtcname>Throttle Actuator Control Throttle Body Range / Performance</dtcname>
<subpara id="RM000000PFX0TZX_01" type-id="61" category="03" proc-id="RM23G0E___00000R400000">
<name>SYSTEM DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The Electronic Throttle Control System (ETCS) is composed of the throttle actuator, throttle position sensor, accelerator pedal position sensor and ECM. The ECM operates the throttle actuator to regulate the throttle valve in response to driver inputs. The throttle position sensor detects the opening angle of the throttle valve, and provides the ECM with feedback so that the throttle valve can be appropriately controlled by the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="0.85in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="3.11in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2119</ptxt>
</entry>
<entry valign="middle">
<ptxt>The throttle valve opening angle continues to vary greatly from the target opening angle (1 trip detection logic).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Electronic Throttle Control System (ETCS)</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000PFX0TZX_02" type-id="64" category="03" proc-id="RM23G0E___00000R500000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The ECM determines the actual opening angle of the throttle valve from the throttle position sensor signal. The actual opening angle is compared to the target opening angle calculated by the ECM. If the difference between these two values is outside the standard range, the ECM interprets this as a malfunction in the ETCS. The ECM then illuminates the MIL and stores the DTC.</ptxt>
<ptxt>If the malfunction is not repaired successfully, the DTC is stored when the accelerator pedal is quickly released (to close the throttle valve) after the engine speed reaches 5000 rpm by fully depressing the accelerator pedal (to fully open the throttle valve).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFX0TZX_06" type-id="62" category="03" proc-id="RM23G0E___00000R600000">
<name>FAIL-SAFE</name>
<content5 releasenbr="1">
<ptxt>When this DTC as well as other DTCs relating to ETCS (Electronic Throttle Control System) malfunctions are stored, the ECM enters fail-safe mode. During fail-safe mode, the ECM cuts the current to the throttle actuator, and the throttle valve is returned to a 7° throttle angle by the return spring. The ECM then adjusts the engine output by controlling the fuel injection (intermittent fuel-cut) and ignition timing in accordance with the accelerator pedal opening angle to allow the vehicle to continue running at a minimal speed. If the accelerator pedal is depressed firmly and gently, the vehicle can be driven slowly.</ptxt>
<ptxt>The ECM continues operating in fail-safe mode until a pass condition is detected, and the ignition switch is then turned off.</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFX0TZX_07" type-id="32" category="03" proc-id="RM23G0E___00000R700000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTC P2102 (See page <xref label="Seep01" href="RM000000PFU0Q4X_07"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000PFX0TZX_08" type-id="51" category="05" proc-id="RM23G0E___00000R800000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Read freeze frame data using the intelligent tester. Freeze frame data records the engine condition when malfunctions are detected. When troubleshooting, freeze frame data can help determine if the vehicle was moving or stationary, if the engine was warmed up or not, if the air fuel ratio was lean or rich, and other data from the time the malfunction occurred.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000000PFX0TZX_09" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000PFX0TZX_09_0001" proc-id="RM23G0E___00000R900000">
<testtitle>CHECK FOR ANY OTHER DTCS OUTPUT (IN ADDITION TO DTC P2119)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P2119</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>P2119 and other DTCs</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any DTCs other than P2119 are output, troubleshoot those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PFX0TZX_09_0006" fin="false">A</down>
<right ref="RM000000PFX0TZX_09_0003" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000PFX0TZX_09_0006" proc-id="RM23G0E___00000RB00000">
<testtitle>INSPECT THROTTLE BODY WITH MOTOR ASSEMBLY (RESISTANCE OF THROTTLE ACTUATOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the throttle body with motor assembly (See page <xref label="Seep01" href="RM000000Q0N039X_01_0001"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFX0TZX_09_0007" fin="false">OK</down>
<right ref="RM000000PFX0TZX_09_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFX0TZX_09_0007" proc-id="RM23G0E___00000RC00000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the ECM (See page <xref label="Seep01" href="RM00000329202EX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000PFX0TZX_09_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000000PFX0TZX_09_0002" proc-id="RM23G0E___00000RA00000">
<testtitle>CHECK WHETHER DTC OUTPUT RECURS (DTC P2119)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z5X"/>).</ptxt>
</test1>
<test1>
<ptxt>Allow the engine to idle for 15 seconds.</ptxt>
<atten2>
<ptxt>Exercise extreme care and take precautions in procedure "A" and "B" below. Failure to do so may result in the vehicle unexpectedly rolling away.</ptxt>
</atten2>
</test1>
<test1>
<ptxt>Securely apply the parking brake and move the shift lever to D (procedure "A").</ptxt>
</test1>
<test1>
<ptxt>While depressing the brake pedal securely, fully depress the accelerator pedal for 5 seconds (procedure "B").</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTC output.</ptxt>
</specitem>
</spec>
<atten4>
<ptxt>The output voltage of the throttle position sensor can be checked during procedure "B" using the intelligent tester. Variations in the output voltage indicate that the throttle actuator is in operation. To check the output voltage using the intelligent tester, enter the following menus: Powertrain / Engine and ECT /Data List / ETCS / Throttle Position No.1.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000PFX0TZX_09_0008" fin="true">OK</down>
<right ref="RM000000PFX0TZX_09_0004" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000PFX0TZX_09_0003">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM000002ZSO00PX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFX0TZX_09_0004">
<testtitle>REPLACE THROTTLE BODY WITH MOTOR ASSEMBLY<xref label="Seep01" href="RM000000Q0P03UX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000PFX0TZX_09_0008">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>