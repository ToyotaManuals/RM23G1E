<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12052_S001Y" variety="S001Y">
<name>THEFT DETERRENT / KEYLESS ENTRY</name>
<ttl id="12052_S001Y_7B9B9_T00KX" variety="T00KX">
<name>ENGINE IMMOBILISER SYSTEM (w/ Entry and Start System)</name>
<para id="RM000002M6X06NX" category="C" type-id="3018W" name-id="TD5J0-02" from="201207" to="201210">
<dtccode>B2799</dtccode>
<dtcname>Engine Immobiliser System Malfunction</dtcname>
<subpara id="RM000002M6X06NX_01" type-id="60" category="03" proc-id="RM23G0E___0000EYT00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC is stored when one of the following occurs: 1) the ECM detects errors in its own communications with the ID code box (immobiliser code ECU); 2) the ECM detects errors in the communication lines; or 3) the ECU communication ID between the ID code box (immobiliser code ECU) and ECM is different and an engine start is attempted.</ptxt>
<ptxt>Before troubleshooting for this DTC, make sure no certification ECU DTCs are output. If output, troubleshoot the certification ECU DTCs first.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>B2799</ptxt>
</entry>
<entry valign="middle">
<ptxt>One of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>An error in the communication between the ECM and ID code box (immobiliser code ECU).</ptxt>
</item>
<item>
<ptxt>An error in the communication lines.</ptxt>
</item>
<item>
<ptxt>The communication ID is different during communication with the ID code box (immobiliser code ECU).</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Harness or connector</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
<item>
<ptxt>ID code box (immobiliser code ECU)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000002M6X06NX_02" type-id="32" category="03" proc-id="RM23G0E___0000EYU00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="B243072E05" width="7.106578999in" height="2.775699831in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002M6X06NX_03" type-id="51" category="05" proc-id="RM23G0E___0000EYV00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the ID code box (immobiliser code ECU) or ECM, refer to the Service Bulletin.</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000002M6X06NX_08" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002M6X06NX_08_0028" proc-id="RM23G0E___0000EZ300000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000QYC058X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0029" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0029" proc-id="RM23G0E___0000EZ400000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000QYC058X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2799 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0038" fin="true">OK</down>
<right ref="RM000002M6X06NX_08_0024" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0024" proc-id="RM23G0E___0000EZ000000">
<testtitle>REGISTER ECU COMMUNICATION ID</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the ECU communication ID (Refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0025" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0025" proc-id="RM23G0E___0000EZ100000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000QYC058X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0026" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0026" proc-id="RM23G0E___0000EZ200000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000QYC058X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2799 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0039" fin="true">OK</down>
<right ref="RM000002M6X06NX_08_0014" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0014" proc-id="RM23G0E___0000EYY00000">
<testtitle>CHECK CONNECTION OF CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch off.</ptxt>
</test1>
<test1>
<ptxt>Check that the connectors are properly connected to the ECM and ID code box (immobiliser code ECU).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Connectors are properly connected.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0002" fin="false">OK</down>
<right ref="RM000002M6X06NX_08_0040" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0002" proc-id="RM23G0E___0000EYW00000">
<testtitle>CHECK HARNESS AND CONNECTOR (ID CODE BOX - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G37 ID code box (immobiliser code ECU) connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G56*1 or G58*2 ECM connector.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: for 1GR-FE</ptxt>
</item>
<item>
<ptxt>*2: for 1KD-FTV</ptxt>
</item>
</list1>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<title>for 1GR-FE</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G37-5 (EFII) - G56-20 (IMO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G37-6 (EFIO) - G56-14 (IMI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G37-5 (EFII) or G56-20 (IMO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G37-6 (EFIO) or G56-14 (IMI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>for 1KD-FTV</title>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G37-5 (EFII) - G58-29 (IMO)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G37-6 (EFIO) - G58-28 (IMI)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G37-5 (EFII) or G58-29 (IMO) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G37-6 (EFIO) or G58-28 (IMI) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0031" fin="false">OK</down>
<right ref="RM000002M6X06NX_08_0016" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0031" proc-id="RM23G0E___0000EZ500000">
<testtitle>REPLACE ECM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Temporarily replace the ECM with a new one.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>for 1GR-FE (See page <xref label="Seep01" href="RM00000329202EX"/>)</ptxt>
</item>
<item>
<ptxt>for 1KD-FTV (See page <xref label="Seep02" href="RM0000013Z001OX"/>)</ptxt>
</item>
</list1>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0032" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0032" proc-id="RM23G0E___0000EZ600000">
<testtitle>REGISTER ECU COMMUNICATION ID</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the ECU communication ID (Refer to the Service Bulletin).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0023" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0023" proc-id="RM23G0E___0000EYZ00000">
<testtitle>CLEAR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000QYC058X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0012" proc-id="RM23G0E___0000EYX00000">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep01" href="RM000000QYC058X"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC B2799 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM000002M6X06NX_08_0009" fin="true">OK</down>
<right ref="RM000002M6X06NX_08_0018" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0009">
<testtitle>END (ECM IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0038">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0039">
<testtitle>END (ECU COMMUNICATION ID IS NOT REGISTERED CORRECTLY)</testtitle>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0040">
<testtitle>CONNECT CONNECTOR CORRECTLY</testtitle>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0016">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002M6X06NX_08_0018">
<testtitle>REPLACE ID CODE BOX (IMMOBILISER CODE ECU)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>