<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12007_S000B" variety="S000B">
<name>5L-E ENGINE MECHANICAL</name>
<ttl id="12007_S000B_7B8XT_T007H" variety="T007H">
<name>TIMING BELT</name>
<para id="RM00000126H00FX" category="A" type-id="30014" name-id="EMAT3-02" from="201210">
<name>INSTALLATION</name>
<subpara id="RM00000126H00FX_01" type-id="01" category="01">
<s-1 id="RM00000126H00FX_01_0049" proc-id="RM23G0E___00004V500001">
<ptxt>INSTALL NO. 1 TIMING BELT IDLER SUB-ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 1 belt idler with the 3 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>44</t-value1>
<t-value2>449</t-value2>
<t-value4>32</t-value4>
</torqueitem>
<subtitle>for bolt B, C</subtitle>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>195</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
<atten4>
<figure>
<graphic graphicname="A057641E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<list1 type="unordered">
<item>
<ptxt>The bolt lengths for bolt A, B and C as follows.</ptxt>
</item>
<item>
<ptxt>Bolt C is combined with the No. 1 timing belt idler.</ptxt>
</item>
</list1>
</atten4>
<spec>
<title>Standard Bolt</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry align="center">
<ptxt>Item</ptxt>
</entry>
<entry align="center">
<ptxt>Length</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>A</ptxt>
</entry>
<entry align="center">
<ptxt>76.5 mm (3.01 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>B</ptxt>
</entry>
<entry align="center">
<ptxt>42.9 mm (1.69 in.)</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>C</ptxt>
</entry>
<entry align="center">
<ptxt>41.3 mm (1.63 in.)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<figure>
<graphic graphicname="A056838E05" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0005" proc-id="RM23G0E___00004UV00001">
<ptxt>SET NO. 1 CYLINDER TO TDC/COMPRESSION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using the crankshaft pulley bolt, align the groove of the crankshaft pulley with the timing pointer by turning the crankshaft clockwise.</ptxt>
<figure>
<graphic graphicname="A058728E03" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Timing Mark</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Do not turn the crankshaft pulley counterclockwise.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Set the timing and drive pulleys at each position.</ptxt>
<figure>
<graphic graphicname="A071317E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Make sure the engine is cold.</ptxt>
</item>
<item>
<ptxt>When turning the crankshaft or camshaft, the valve heads will hit against the piston top. Do not turn them more than necessary.</ptxt>
</item>
</list1>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0006" proc-id="RM23G0E___00004UW00001">
<ptxt>INSTALL TIMING BELT</ptxt>
<content1 releasenbr="1">
<figure>
<graphic graphicname="A058721E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten4>
<ptxt>If reusing the timing belt, align the points marked during removal, and install the timing belt with the arrow pointing in the direction the belt moves when the engine is running.</ptxt>
</atten4>
<s2>
<ptxt>Remove any oil or water on each pulley, and keep them clean.</ptxt>
</s2>
<s2>
<ptxt>Install the timing belt to the crankshaft timing and timing belt idlers.</ptxt>
</s2>
<s2>
<ptxt>Using SST, slightly turn the injection pump drive pulley clockwise. Install the timing belt to the pulley, and align the timing marks of the drive pulley and timing belt case.</ptxt>
<figure>
<graphic graphicname="A224020E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09960-10010</s-number>
<s-subnumber>09962-01000</s-subnumber>
<s-subnumber>09963-01000</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Timing Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Using SST, slightly turn the camshaft timing pulley clockwise. Install the timing belt to the timing pulley, and align the timing marks of the timing pulley and timing belt case.</ptxt>
<figure>
<graphic graphicname="A224021E01" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09960-10010</s-number>
<s-subnumber>09962-01000</s-subnumber>
<s-subnumber>09963-01000</s-subnumber>
</sstitem>
</sst>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Timing Mark</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Check that the timing belt has tension between the injection pump drive and camshaft timing pulleys.</ptxt>
</s2>
<s2>
<ptxt>Install the timing belt to the No. 1 timing belt idler.</ptxt>
</s2>
<s2>
<ptxt>Loosen the No. 1 timing belt idler bolt (A), and stretch the timing belt.</ptxt>
<figure>
<graphic graphicname="A058734E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Slowly turn the crankshaft pulley.</ptxt>
<atten3>
<ptxt>Always turn the crankshaft clockwise.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Tighten the No. 1 timing belt idler bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>44</t-value1>
<t-value2>449</t-value2>
<t-value4>33</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0007" proc-id="RM23G0E___00004UX00001">
<ptxt>CHECK NO. 1 CYLINDER TO TDC/COMPRESSION</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Slowly turn the crankshaft pulley 2 revolutions from TDC to TDC.</ptxt>
<atten3>
<ptxt>Always turn the crankshaft clockwise.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Check that the timing marks for each pulley align as shown in the illustration.</ptxt>
<figure>
<graphic graphicname="A228092" width="2.775699831in" height="1.771723296in"/>
</figure>
<ptxt>If the timing marks do not align, remove the timing belt and reinstall it.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0008" proc-id="RM23G0E___00004UY00001">
<ptxt>INSTALL TIMING BELT GUIDE</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the timing belt guide with the cup side facing outward.</ptxt>
<figure>
<graphic graphicname="A058736E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0009" proc-id="RM23G0E___00004UZ00001">
<ptxt>INSTALL TIMING BELT COVER</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install 2 new gaskets to the timing belt cover.</ptxt>
<figure>
<graphic graphicname="A112018E02" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gasket</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Install the timing belt cover with the 11 bolts and washers.</ptxt>
<figure>
<graphic graphicname="A112017E01" width="2.775699831in" height="2.775699831in"/>
</figure>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>8.5</t-value1>
<t-value2>85</t-value2>
<t-value4>75</t-value4>
</torqueitem>
<subtitle>except bolt A</subtitle>
<torqueitem>
<t-value1>11</t-value1>
<t-value2>107</t-value2>
<t-value4>8</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0048" proc-id="RM23G0E___00004V400001">
<ptxt>INSTALL IDLE PULLEY ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the idle pulley bracket with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>29</t-value1>
<t-value2>300</t-value2>
<t-value4>22</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0010" proc-id="RM23G0E___00004V000001">
<ptxt>INSTALL CRANKSHAFT PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Align the key groove of the pulley with the pulley set key, and slide the pulley onto the crankshaft to install it.</ptxt>
</s2>
<s2>
<ptxt>Using SST, install the pulley bolt.</ptxt>
<figure>
<graphic graphicname="A058727E02" width="2.775699831in" height="1.771723296in"/>
</figure>
<sst>
<sstitem>
<s-number>09213-54015</s-number>
<s-subnumber>91651-60855</s-subnumber>
</sstitem>
<sstitem>
<s-number>09330-00021</s-number>
</sstitem>
</sst>
<torque>
<torqueitem>
<t-value1>235</t-value1>
<t-value2>2396</t-value2>
<t-value4>173</t-value4>
</torqueitem>
</torque>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turn</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hold</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0011" proc-id="RM23G0E___00004V100001">
<ptxt>INSTALL VANE PUMP DRIVE PULLEY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the vane pump drive pulley and cooler compressor drive pulley with the 4 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>19</t-value1>
<t-value2>194</t-value2>
<t-value4>14</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0044" proc-id="RM23G0E___00004V300001">
<ptxt>INSTALL FAN SHROUD</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the fan shroud (See page <xref label="Seep01" href="RM00000144D01VX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0031" proc-id="RM23G0E___00004V200001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM00000126H00FX_01_0038" proc-id="RM23G0E___00004UL00000">
<ptxt>INSPECT ENGINE IDLE SPEED
</ptxt>
<content1 releasenbr="2">
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>When using the intelligent tester:</ptxt>
<s3>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
<spec>
<title>Idle speed</title>
<specitem>
<ptxt>720 to 820 rpm</ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Turn all the electrical systems and the A/C off.</ptxt>
</item>
<item>
<ptxt>When checking the idling speed, the shift lever should be in neutral.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Refer to the intelligent tester operator's manual for further details.</ptxt>
</atten4>
</s3>
</s2>
<s2>
<ptxt>When not using an intelligent tester:</ptxt>
<figure>
<graphic graphicname="A206183E08" width="2.775699831in" height="1.771723296in"/>
</figure>
<s3>
<ptxt>Using SST, connect the tachometer test probe to terminal 9 (TAC) of the DLC3.</ptxt>
<sst>
<sstitem>
<s-number>09843-18040</s-number>
</sstitem>
</sst>
</s3>
<s3>
<ptxt>Check the idle speed.</ptxt>
<spec>
<title>Standard idle speed</title>
<specitem>
<ptxt>720 to 820 rpm</ptxt>
</specitem>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front View of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Turn all the electrical systems and the A/C off.</ptxt>
</item>
<item>
<ptxt>When checking the idling speed, the shift lever should be in neutral.</ptxt>
</item>
<item>
<ptxt>Confirm the terminal number before connecting them. Connecting the wrong terminal can be damage the engine.</ptxt>
</item>
</list1>
</atten3>
</s3>
</s2>
</content1></s-1>
<s-1 id="RM00000126H00FX_01_0039" proc-id="RM23G0E___00004UM00000">
<ptxt>INSPECT MAXIMUM ENGINE SPEED
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Start the engine.</ptxt>
</s2>
<s2>
<ptxt>Fully depress the accelerator pedal.</ptxt>
</s2>
<s2>
<ptxt>Check the maximum speed.</ptxt>
<spec>
<title>Maximum engine speed</title>
<specitem>
<ptxt>4850 to 4950 rpm</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>