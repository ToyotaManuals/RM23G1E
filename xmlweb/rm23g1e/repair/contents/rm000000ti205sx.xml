<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000000TI205SX" category="J" type-id="30076" name-id="ESTFF-03" from="201207" to="201210">
<dtccode/>
<dtcname>ECM Power Source Circuit</dtcname>
<subpara id="RM000000TI205SX_01" type-id="60" category="03" proc-id="RM23G0E___000023700000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>When the engine switch is turned on (IG), the battery voltage is applied to terminal IGSW of the ECM. The ECM "MREL" output signal causes a current to flow to the coil, closing the contacts of the EFI relay and supplying power to terminal +B of the ECM.</ptxt>
</content5>
</subpara>
<subpara id="RM000000TI205SX_02" type-id="32" category="03" proc-id="RM23G0E___000023800000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="A249250E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000000TI205SX_03" type-id="51" category="05" proc-id="RM23G0E___000023900000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Inspect the fuses of circuits related to this system before performing the following inspection procedure.</ptxt>
</item>
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK062X"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See Page <xref label="Seep04" href="RM0000012XK062X"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000000TI205SX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000TI205SX_05_0018" proc-id="RM23G0E___000023A00000">
<testtitle>INSPECT NO. 1 INTEGRATION RELAY (NO. 1 INTEGRATION RELAY - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
<figure>
<graphic graphicname="A223349E02" width="2.775699831in" height="3.779676365in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>1C-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Room Relay Block</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to No. 1 Integration Relay)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Reinstall the integration relay.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI205SX_05_0019" fin="false">OK</down>
<right ref="RM000000TI205SX_05_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI205SX_05_0019" proc-id="RM23G0E___000023B00000">
<testtitle>INSPECT NO. 1 INTEGRATION RELAY (EFI, EFI MAIN, IG2)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Inspect the No. 1 integration relay (EFI) (See page <xref label="Seep01" href="RM00000469100VX_01_0004"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the No. 1 integration relay (EFI MAIN) (See page <xref label="Seep03" href="RM00000469100VX_01_0008"/>).</ptxt>
</test1>
<test1>
<ptxt>Inspect the No. 1 integration relay (IG2) (See page <xref label="Seep02" href="RM00000469100VX_01_0006"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI205SX_05_0022" fin="false">OK</down>
<right ref="RM000000TI205SX_05_0030" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI205SX_05_0022" proc-id="RM23G0E___000023D00000">
<testtitle>INSPECT ECM (IGSW VOLTAGE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the engine switch on (IG).</ptxt>
<figure>
<graphic graphicname="A177861E24" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below. </ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC2" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G58-24 (IGSW) - C93-1 (E1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine switch on (IG)</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component with harness connected</ptxt>
<ptxt>(ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000TI205SX_05_0020" fin="false">OK</down>
<right ref="RM000000TI205SX_05_0026" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI205SX_05_0020" proc-id="RM23G0E___000023C00000">
<testtitle>CHECK WIRE HARNESS (NO. 1 INTEGRATION RELAY - ECM)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the wire harness between the ECM and No. 1 integration relay.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G58-11 (MREL) - 1B-2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G57-24 (+B) - 1B-8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G57-17 (+B2) - 1B-8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G58-24 (IGSW) - 1A-5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC3" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G58-11 (MREL) or 1B-2 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G57-24 (+B) or 1B-8 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G57-17 (+B2) or 1B-8 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>G58-24 (IGSW) or 1A-5 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reconnect the ECM connector.</ptxt>
</test1>
<test1>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI205SX_05_0033" fin="true">OK</down>
<right ref="RM000000TI205SX_05_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI205SX_05_0026" proc-id="RM23G0E___000023E00000">
<testtitle>CHECK WIRE HARNESS (POWER MANAGEMENT CONTROL ECU - NO. 1 INTEGRATION RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the power management control ECU.</ptxt>
</test1>
<test1>
<ptxt>Remove the No. 1 integration relay from the engine room relay block.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance (Check for Open)</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.96in"/>
<colspec colname="COLSPEC4" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.35in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G50-8 (IG2D) - 1A-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<spec>
<title>Standard Resistance (Check for Short)</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COLSPEC4" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G50-8 (IG2D) or 1A-1 - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Reinstall the power management control ECU.</ptxt>
</test1>
<test1>
<ptxt>Reinstall the No. 1 integration relay.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000000TI205SX_05_0036" fin="true">OK</down>
<right ref="RM000000TI205SX_05_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000TI205SX_05_0036">
<testtitle>CHECK ENTRY AND START SYSTEM<xref label="Seep01" href="RM000000YEF0FUX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000TI205SX_05_0030">
<testtitle>REPLACE NO. 1 INTEGRATION RELAY</testtitle>
</testgrp>
<testgrp id="RM000000TI205SX_05_0031">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000TI205SX_05_0033">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001OX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>