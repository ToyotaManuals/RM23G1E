<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AP_T00KD" variety="T00KD">
<name>CAN COMMUNICATION SYSTEM (for RHD without Entry and Start System)</name>
<para id="RM00000482H006X" category="T" type-id="3001H" name-id="NW1N2-04" from="201210">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM00000482H006X_z0" proc-id="RM23G0E___0000E9G00001">
<content5 releasenbr="17">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and relays related to this system before inspecting the suspected areas below.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Result of CAN V1 Bus Check</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Open in CAN Main Bus Line.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Main Bus Line</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002L82049X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Short in CAN Bus Lines.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Bus Lines</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002L7Z09YX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Short to +B in CAN Bus Line.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Bus Line</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002L7Z09ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Short to GND in CAN Bus Line.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Bus Line</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002L7Z0A0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>Open in One Side of CAN Branch Line.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>CAN Bus Branch Line</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001YZM05HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Refer to How to Proceed with Troubleshooting for the CAN V1 Bus Check procedure.</ptxt>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Communication Stop Mode Table </title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Skid Control (ABS/VSC/TRAC)" is not displayed on the intelligent tester.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Brake Actuator (Skid Control ECU) Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000039LU039X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Air Conditioning Amplifier" is not displayed on the intelligent tester.</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Conditioning Amplifier Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RTD03DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"PPS" is not displayed on the intelligent tester.*1</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Power Steering ECU Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003F7B02EX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Spiral cable (Steering Angle Sensor)" is not displayed on the intelligent tester.*2</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Steering Angle Sensor Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002TGF04AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"Yaw Rate Sensor" is not displayed on the intelligent tester.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Yaw Rate Sensor Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003TN301ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"ECM (Engine)" is not displayed on the intelligent tester.*1</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>ECM Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RSZ04HX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"Main Body" is not displayed on the intelligent tester.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Main Body ECU Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RTG04JX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"Combination Meter" is not displayed on the intelligent tester.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Combination Meter ECU Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001TNA04AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"Airbag" is not displayed on the intelligent tester.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Center Airbag Sensor Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001TN904CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="0" colsep="1" valign="middle" align="left">
<ptxt>"Display and Navigation (AVN)" is not displayed on the intelligent tester.*3</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Display and Navigation Assembly Communication Stop Mode</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000109K08UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>"Four Wheel Drive Control" is not displayed on the intelligent tester.</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>4WD Control ECU Communication Stop Mode</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000109K08VX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for 2TR-FE</ptxt>
</item>
<item>
<ptxt>*2: w/ Vehicle Stability Control System (for Vacuum Brake Booster)</ptxt>
</item>
<item>
<ptxt>*3: w/ Navigation System</ptxt>
</item>
</list1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>