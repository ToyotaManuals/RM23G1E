<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12061_S0026" variety="S0026">
<name>HEATING / AIR CONDITIONING</name>
<ttl id="12061_S0026_7B9E8_T00NW" variety="T00NW">
<name>AIR CONDITIONING SYSTEM (for Automatic Air Conditioning System)</name>
<para id="RM000002LIN043X" category="S" type-id="3001E" name-id="AC0339-124" from="201210">
<name>DIAGNOSTIC TROUBLE CODE CHART</name>
<subpara id="RM000002LIN043X_z0" proc-id="RM23G0E___0000H9K00001">
<content5 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a trouble code is output during the DTC check, inspect the trouble areas listed for that code. For details of the code, refer to the "See page" below.</ptxt>
</item>
<item>
<ptxt>*: The air conditioning amplifier stores the DTC of the respective malfunction that has been detected for the period of time indicated in parentheses.</ptxt>
</item>
</list1>
</atten4>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Air Conditioning System (for Automatic Air Conditioning System)</title>
<tgroup cols="5" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="1.42in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="1.42in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.42in" colsep="0"/>
<colspec colnum="4" colname="4" colwidth="1.42in" colsep="0"/>
<colspec colnum="5" colname="5" align="center" colwidth="1.4in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Detection Item</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>Memory*</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1411/11</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Room Temperature Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(4 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>If the front room temperature is approx. -18.6°C (- 1.48°F) or less, DTC B1411/11 may be stored even though the system is normal.</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002LJ302XX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1412/12</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Ambient Temperature Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(4 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>If the ambient temperature is approx. -52.9°C (- 63.22°F) or less, DTC B1412/12 may be stored even though the system is normal.</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002LJ404IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1413/13</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Evaporator Temperature Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(4 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002LJ5044X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1417/17</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Rear Evaporator Temperature Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(4 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002DJ1022X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1419/19</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Rear Room Temperature Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(4 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002DJ201DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1422/22</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Compressor Lock Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM0000026BV07BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1423/23</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Pressure Sensor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(4 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000003E0P03KX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1441/41</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Mix Damper Control Servo Motor Circuit (Passenger Side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002LLI03TX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1442/42</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Inlet Damper Control Servo Motor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002LLJ03UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1443/43</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Outlet Damper Control Servo Motor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002ROM042X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1446/46</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Air Mix Damper Control Servo Motor Circuit (Driver Side)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002RON02XX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1447/47</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Rear Air Mix Damper Control Servo Motor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002DO304CX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1449/49</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Rear Air Outlet Damper Control Servo Motor Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(30 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002DO304DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B1497/97</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>BUS IC Communication Malfunction</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(10 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000002LLQ03MX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B14A2</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Driver Side Solar Sensor Short Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(4 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>If the check is performed in a dark place, DTC B14A3 (front passenger side solar sensor short circuit) may also be stored.</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000003N3S054X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>B14A3</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Front Passenger Side Solar Sensor Short Circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>Stored</ptxt>
<ptxt>(4 seconds or more)</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>If the check is performed in a dark place, DTC B14A2 (driver side solar sensor short circuit) may also be stored.</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000003N3S055X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>U0100</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with ECM</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000003UXN04DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>U0142</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Main Body ECU</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000003UXN04DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>U0155</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="left">
<ptxt>Lost Communication with Combination Meter</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="4" colsep="1" valign="middle" align="left">
<ptxt>-</ptxt>
</entry>
<entry colname="5" colsep="1" valign="middle">
<ptxt>
<xref href="RM000003UXN04DX" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>