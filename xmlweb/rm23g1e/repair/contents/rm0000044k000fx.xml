<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12014_S0011" variety="S0011">
<name>CRUISE CONTROL</name>
<ttl id="12014_S0011_7B93I_T00D6" variety="T00D6">
<name>DYNAMIC RADAR CRUISE CONTROL SYSTEM</name>
<para id="RM0000044K000FX" category="C" type-id="305G7" name-id="CC2IS-04" from="201210">
<dtccode>P1615</dtccode>
<dtcname>Communication Error from Distance Control ECU to ECM</dtcname>
<subpara id="RM0000044K000FX_01" type-id="60" category="03" proc-id="RM23G0E___000079K00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The driving support ECU receives information about the area in front of the vehicle from the millimeter wave radar sensor and then sends brake control demand signals (deceleration demand) and diagnosis signals, such as signal indicating the millimeter wave radar sensor assembly is dirty or malfunctioning, to the ECM.</ptxt>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>P1615</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>While the vehicle speed is 50 km/h (31 mph) or more with the cruise control switch on, communication data from the driving support ECU is logically inconsistent for 1 second.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Driving support ECU</ptxt>
</item>
<item>
<ptxt>ECM</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM0000044K000FX_02" type-id="51" category="05" proc-id="RM23G0E___000079L00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When the driving support ECU or ECM is replaced with a new one, initialization must be performed (See page <xref label="Seep01" href="RM0000044H500AX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM0000044K000FX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000044K000FX_04_0006" proc-id="RM23G0E___000079O00001">
<testtitle>CONFIRM DESIGNATION INFORMATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Confirm the designation information.</ptxt>
<test2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test2>
<test2>
<ptxt>Turn the cruise control switch on.</ptxt>
</test2>
<test2>
<ptxt>With the brake pedal depressed, push the cruise control switch to +RES 3 times within 3 seconds. Check that the skid control buzzer sounds at this time.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>The skid control buzzer sounds (initialization is performed).</ptxt>
</specitem>
</spec>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Do not turn the light control switch on at this time as doing so causes the beam axis to deviate. If the light control switch is turned on by mistake, readjust the beam axis (See page <xref label="Seep01" href="RM000000WQG01RX"/>).</ptxt>
</item>
<item>
<ptxt>If initialization cannot be performed, first inspect the stop light switch circuit, cruise control switch circuit and skid control buzzer circuit.</ptxt>
</item>
</list1>
</atten3>
</test2>
<test2>
<ptxt>Turn the ignition switch off.</ptxt>
</test2>
</test1>
</content6>
<res>
<down ref="RM0000044K000FX_04_0007" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000044K000FX_04_0007" proc-id="RM23G0E___000079P00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000044GY00AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the following to make sure the DTC detection conditions are met.</ptxt>
<atten4>
<ptxt>If the detection conditions are not met, the system cannot detect the malfunction. </ptxt>
</atten4>
<test2>
<ptxt>Drive the vehicle at a speed of 50 km/h (31 mph) or more.</ptxt>
</test2>
<test2>
<ptxt>Turn the cruise control switch on. </ptxt>
</test2>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000044GY00AX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC P1615 is not output.</ptxt>
</specitem>
</spec>
</test1>
</content6>
<res>
<down ref="RM0000044K000FX_04_0009" fin="true">OK</down>
<right ref="RM0000044K000FX_04_0001" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM0000044K000FX_04_0001" proc-id="RM23G0E___000079M00001">
<testtitle>REPLACE DRIVING SUPPORT ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the driving support ECU (See page <xref label="Seep01" href="RM000002OJO018X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM0000044K000FX_04_0002" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM0000044K000FX_04_0002" proc-id="RM23G0E___000079N00001">
<testtitle>CHECK FOR DTC</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM0000044GY00AX"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the following to make sure the DTC detection conditions are met.</ptxt>
<atten4>
<ptxt>If the detection conditions are not met, the system cannot detect the malfunction. </ptxt>
</atten4>
<test2>
<ptxt>Drive the vehicle at a speed of 50 km/h (31 mph) or more.</ptxt>
</test2>
<test2>
<ptxt>Turn the cruise control switch on. </ptxt>
</test2>
</test1>
<test1>
<ptxt>Check for DTCs (See page <xref label="Seep02" href="RM0000044GY00AX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>DTC P1615 is not output.</ptxt>
</specitem>
</spec>
<table>
<title>Result</title>
<tgroup cols="2" align="center">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1GR-FE)</ptxt>
</entry>
<entry valign="middle">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 1KD-FTV)</ptxt>
</entry>
<entry valign="middle">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM0000044K000FX_04_0003" fin="true">A</down>
<right ref="RM0000044K000FX_04_0004" fin="true">B</right>
<right ref="RM0000044K000FX_04_0005" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM0000044K000FX_04_0003">
<testtitle>END (DRIVING SUPPORT ECU IS DEFECTIVE)</testtitle>
</testgrp>
<testgrp id="RM0000044K000FX_04_0004">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM00000329202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044K000FX_04_0005">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM0000013Z001YX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000044K000FX_04_0009">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>