<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12005_S0007" variety="S0007">
<name>1KD-FTV ENGINE CONTROL</name>
<ttl id="12005_S0007_7B8WB_T005Z" variety="T005Z">
<name>ECD SYSTEM (w/ DPF)</name>
<para id="RM000004KNK010X" category="J" type-id="805CS" name-id="ESZKF-02" from="201210">
<dtccode/>
<dtcname>Fuel Injection System</dtcname>
<subpara id="RM000004KNK010X_01" type-id="51" category="05" proc-id="RM23G0E___000030A00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>After replacing the ECM, the new ECM needs registration (See page <xref label="Seep01" href="RM0000012XK07MX"/>) and initialization (See page <xref label="Seep02" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing the fuel supply pump assembly, the ECM needs initialization (See page <xref label="Seep03" href="RM000000TIN05LX"/>).</ptxt>
</item>
<item>
<ptxt>After replacing an injector assembly, the ECM needs registration (See page <xref label="Seep04" href="RM0000012XK07MX"/>).</ptxt>
</item>
</list1>
</atten3>
</content5>
</subpara>
<subpara id="RM000004KNK010X_02" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000004KNK010X_02_0001" proc-id="RM23G0E___000030B00001">
<testtitle>CHECK INJECTOR COMPENSATION CODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the injector compensation code (See page <xref label="Seep01" href="RM0000012XK07MX_02_0006"/>).</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Except below</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Compensation codes of the installed injector assemblies are the same as the compensation codes registered in the ECM</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0002" fin="false">A</down>
<right ref="RM000004KNK010X_02_0003" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0002" proc-id="RM23G0E___000030C00001">
<testtitle>REGISTER INJECTOR COMPENSATION CODE AND PERFORM PILOT QUANTITY LEARNING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the injector compensation codes (See page <xref label="Seep01" href="RM0000012XK07MX_02_0003"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the injector pilot quantity learning (See page <xref label="Seep02" href="RM0000012XK07MX_02_0009"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0003" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0003" proc-id="RM23G0E___000030D00001">
<testtitle>TAKE SNAPSHOT DURING IDLING AND 4000 RPM (PROCEDURE 3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / All Data.</ptxt>
</test1>
<test1>
<ptxt>Take a snapshot of the following Data List items.</ptxt>
<figure>
<graphic graphicname="A176220E13" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Snapshot Button</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reference</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>A snapshot can be used to compare vehicle data from the time of the malfunction to normal data and is very useful for troubleshooting. The data in the list below is that of a normal vehicle, but as the data varies between individual vehicles, this data should only be used for reference.</ptxt>
</item>
<item>
<ptxt>Graphs like the ones shown below can be displayed by transferring the stored snapshot data from the tester to a PC. Intelligent Viewer must be installed on the PC.</ptxt>
</item>
<item>
<ptxt>Check the Data List at idling and at 4000 rpm with no load after the engine is warmed up.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0004" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0004" proc-id="RM23G0E___000030E00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INJECTION FEEDBACK VAL #1 TO #4 AND INJECTION VOLUME)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check Injection Feedback Val #1 to #4 and Injection Volume in the snapshot taken in procedure 3 when the engine was idling and at 4000 rpm with no load.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Injection Feedback Val for at least one cylinder is more than 3.0 mm<sup>3</sup>/st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Injection Volume is 10 mm<sup>3</sup>/st or less at 4000 rpm</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When there is a problem with the operation of an injector assembly due to foreign matter in the inside of the injector assembly, etc., the fuel injection volume decreases. As a result, the ECM gives instructions to increase the fuel injection volume, which causes Injection Feedback Val to increase.</ptxt>
</item>
<item>
<ptxt>The ECM controls the system so that the sum of Injection Feedback Val for all of the cylinders is approximately 0 mm<sup>3</sup>/st. Even if the value of Injection Feedback Val for a cylinder is less than -3.0 mm<sup>3</sup>/st (-3.0 mm<sup>3</sup>/st is the lowest normal value), as long as the value of Injection Feedback Val for each of the other cylinders is 3.0 mm<sup>3</sup>/st or less, the injector assemblies are not malfunctioning.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0005" fin="false">A</down>
<right ref="RM000004KNK010X_02_0011" fin="false">B</right>
<right ref="RM000004KNK010X_02_0010" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0005" proc-id="RM23G0E___000030F00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (CHECK THE CYLINDER COMPRESSION)</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>Use this Active Test to help determine whether a cylinder has compression loss or not.</ptxt>
</atten4>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>

<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Check the Cylinder Compression / Data List / Compression / Engine Speed of Cyl #1 to #4.</ptxt>
</test1>
<test1>
<ptxt>Check the engine speed during the Active Test.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Except below</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>The values of Engine Speed Cyl #1 to #4 are within +/-10 rpm of each other.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When cranking, if the speed of a cylinder is approximately 100 rpm more than the other cylinders, there is probably a complete loss of compression in that cylinder.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0006" fin="false">A</down>
<right ref="RM000004KNK010X_02_0007" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0006" proc-id="RM23G0E___000030G00001">
<testtitle>CHECK CYLINDER COMPRESSION PRESSURE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the cylinder compression pressure (See page <xref label="Seep01" href="RM000001473025X_01_0002"/>).</ptxt>
<atten4>
<ptxt>When compression is low, there may be cracks in the piston or the injector assembly may be installed improperly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0007" fin="false">OK</down>
<right ref="RM000004KNK010X_02_0015" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0007" proc-id="RM23G0E___000030H00001">
<testtitle>PERFORM PILOT QUANTITY LEARNING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform the injector pilot quantity learning (See page <xref label="Seep01" href="RM0000012XK07MX_02_0009"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0008" proc-id="RM23G0E___000030I00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INJECTION FEEDBACK VAL #1 TO #4)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Injection Feedback Val #1 to #4.</ptxt>
</test1>
<test1>
<ptxt>Read the values.</ptxt>
<spec>
<title>Standard</title>
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="2.36in"/>
<colspec colname="COL2" colwidth="2.36in"/>
<colspec colname="COL3" colwidth="2.36in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Engine Condition*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Normal Value</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Injection Feedback Val #1</ptxt>
<ptxt>Injection Feedback Val #2</ptxt>
<ptxt>Injection Feedback Val #3</ptxt>
<ptxt>Injection Feedback Val #4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Idling</ptxt>
</entry>
<entry valign="middle">
<ptxt> -3.0 mm<sup>3</sup>/st to 3.0 mm<sup>3</sup>/st</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten4>
<ptxt>*: The A/C switch and all accessory switches should be off, the engine coolant temperature should be 75°C (167°F) or more and the engine should be idled for 1 minute or more.</ptxt>
</atten4>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Injection Feedback Val for at least one cylinder is more than 3.0 mm<sup>3</sup>/st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>When there is a problem with the operation of an injector assembly due to foreign matter in the inside of the injector assembly, etc., the fuel injection volume decreases. As a result, the ECM gives instructions to increase the fuel injection volume, which causes Injection Feedback Val to increase.</ptxt>
</item>
<item>
<ptxt>The ECM controls the system so that the sum of Injection Feedback Val for all of the cylinders is approximately 0 mm<sup>3</sup>/st. Even if the value of Injection Feedback Val for a cylinder is less than -3.0 mm<sup>3</sup>/st (-3.0 mm<sup>3</sup>/st is the lowest normal value), as long as the value of Injection Feedback Val for each of the other cylinders is 3.0 mm<sup>3</sup>/st or less, the injector assemblies are not malfunctioning.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0009" fin="false">A</down>
<right ref="RM000004KNK010X_02_0016" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0009" proc-id="RM23G0E___000030J00001">
<testtitle>REPLACE INJECTOR ASSEMBLY OF MALFUNCTIONING CYLINDER</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the injector assembly of the malfunctioning cylinder (See page <xref label="Seep01" href="RM0000044TN00SX"/>).</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injector assembly for a cylinder, always be sure to use a new injection pipe.</ptxt>
</item>
<item>
<ptxt>Follow the procedure in the repair manual and temporarily install the injection pipes and nozzle leakage pipe, and then correctly position the injector assemblies. After that, tighten parts according to the torque specifications.</ptxt>
</item>
<item>
<ptxt>If the installation procedure is not performed correctly, injector assemblies may become out of position, which may cause the injector assemblies to deteriorate, resulting in malfunctions.</ptxt>
</item>
<item>
<ptxt>If an injector assembly deteriorates and malfunctions, other problems such as knocking, rough idle, etc. may occur.</ptxt>
</item>
<item>
<ptxt>If an injector assembly becomes out of position, it is possible that the seal between the injector assembly and injection pipe may become incomplete, resulting in a fuel leak.</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<right ref="RM000004KNK010X_02_0012" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0010" proc-id="RM23G0E___000030K00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (INJECTION VOLUME)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check Injection Volume in the snapshot taken in procedure 3 when the engine was idling.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Injection Volume while idling after engine warm up is 15 mm<sup>3</sup>/st or more</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Check that PM regeneration is not being performed.</ptxt>
<ptxt>The idling speed during PM regeneration is 800 to 900 rpm (with the shift lever in N) for an automatic transmission and 900 to 1000 rpm for a manual transmission.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0011" fin="false">A</down>
<right ref="RM000004KNK010X_02_0016" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0011" proc-id="RM23G0E___000030L00001">
<testtitle>REPLACE INJECTOR ASSEMBLIES OF ALL CYLINDERS</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the injector assemblies of all cylinders (See page <xref label="Seep01" href="RM0000044TN00SX"/>).</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing the injector assembly for a cylinder, always be sure to use a new injection pipe.</ptxt>
</item>
<item>
<ptxt>Follow the procedure in the repair manual and temporarily install the injection pipes and nozzle leakage pipe, and then correctly position the injector assemblies. After that, tighten parts according to the torque specifications.</ptxt>
</item>
<item>
<ptxt>If the installation procedure is not performed correctly, injector assemblies may become out of position, which may cause the injector assemblies to deteriorate, resulting in malfunctions.</ptxt>
</item>
<item>
<ptxt>If an injector assembly deteriorates and malfunctions, other problems such as knocking, rough idle, etc. may occur.</ptxt>
</item>
<item>
<ptxt>If an injector assembly becomes out of position, it is possible that the seal between the injector assembly and injection pipe may become incomplete, resulting in a fuel leak.</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0012" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0012" proc-id="RM23G0E___000030M00001">
<testtitle>REPLACE FUEL FILTER ELEMENT SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the fuel filter element sub-assembly (See page <xref label="Seep01" href="RM0000045HW004X"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0013" proc-id="RM23G0E___000030N00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0014" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0014" proc-id="RM23G0E___000030O00001">
<testtitle>REGISTER INJECTOR COMPENSATION CODE AND PERFORM PILOT QUANTITY LEARNING</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Register the injector compensation codes (See page <xref label="Seep01" href="RM0000012XK07MX_02_0003"/>).</ptxt>
</test1>
<test1>
<ptxt>Perform the injector pilot quantity learning (See page <xref label="Seep02" href="RM0000012XK07MX_02_0009"/>).</ptxt>
</test1>
</content6>
<res>
<right ref="RM000004KNK010X_02_0016" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0015" proc-id="RM23G0E___000030P00001">
<testtitle>CHECK AND REPLACE ENGINE ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check and replace the engine assembly.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0016" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0016" proc-id="RM23G0E___000030Q00001">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (TEST THE FUEL LEAK)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the engine switch on (IG) and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Test the Fuel Leak / Data List / Fuel Press, Target Common Rail Pressure, and Target Pump SCV Current.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Take a snapshot with the intelligent tester during the Active Test.</ptxt>
<figure>
<graphic graphicname="A176220E13" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Snapshot Button</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reference</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>Detailed graphs can be displayed by transferring the stored snapshot from the tester to a PC (Personal Computer) with Intelligent Viewer installed.</ptxt>
</atten4>
</test1>
<test1>
<ptxt>Measure the difference between the target fuel pressure (Target Common Rail Pressure) and the actual fuel pressure (Fuel Press) when the "Test the Fuel Leak" Active Test is performed.</ptxt>
<atten4>
<ptxt>In order to obtain an exact measurement, perform the Active Test 5 times and measure the difference once each time the Active Test is performed.</ptxt>
</atten4>
<figure>
<graphic graphicname="A227749E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<spec>
<title>OK</title>
<specitem>
<ptxt>The difference between the target fuel pressure and the actual fuel pressure 2 seconds after the Active Test starts is less than 10000 kPa (102.0 kgf/cm<sup>2</sup>, 1451 psi).</ptxt>
</specitem>
</spec>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry align="center">
<ptxt>Result</ptxt>
</entry>
<entry align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry align="center">
<ptxt>NG</ptxt>
</entry>
<entry align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>OK*</ptxt>
</entry>
<entry align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>*: Even when the results of the Active Test are normal, take a snapshot of vehicle data when the vehicle is accelerating. If "Fuel Press" does not follow "Target Common Rail Pressure", proceed to step 17.</ptxt>
<ptxt>If "Fuel Press" does not follow "Target Common Rail Pressure", the fuel filter may be clogged.</ptxt>
</atten3>
<atten4>
<list1 type="unordered">
<item>
<ptxt>"Target Common Rail Pressure" means target fuel pressure controlled by the ECM.</ptxt>
</item>
<item>
<ptxt>"Fuel Press" means actual fuel pressure in common rail assembly.</ptxt>
</item>
<item>
<ptxt>If the pressure discharge valve mounted on the common rail assembly is malfunctioning, the actual fuel pressure may change as indicated by "Pressure Discharge Valve Malfunctioning" in the illustration.</ptxt>
</item>
<item>
<ptxt>The pressure discharge valve operates to discharge fuel pressure when the internal pressure of the common rail exceeds the target fuel pressure.</ptxt>
</item>
<item>
<ptxt>There are 2 fuel pressure sensor circuits and both of them display the same value. If the difference between Fuel Press and Common Rail Press Sens 2 exceeds 10000 kPa (102 kgf/cm<sup>2</sup>, 1451 psi) for 2 seconds or more, it is necessary to inspect or repair the wiring for the fuel pressure sensor.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0018" fin="false">A</down>
<right ref="RM000004KNK010X_02_0025" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0018" proc-id="RM23G0E___000030S00001">
<testtitle>INSPECT COMMON RAIL ASSEMBLY (PRESSURE DISCHARGE VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Utility / Pressure Discharge Valve Check (See page <xref label="Seep01" href="RM000002SY802OX_01_0005"/>).</ptxt>
</test1>
<test1>
<ptxt>Select "Always Closed Check".</ptxt>
</test1>
<test1>
<ptxt>Press "Next".</ptxt>
</test1>
<test1>
<ptxt>Read the Value of fuel pressure values.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>Fuel pressure is properly maintained.</ptxt>
</specitem>
</spec>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If the fuel pressure cannot be maintained, the pressure discharge valve may not have a tight seal.</ptxt>
</item>
<item>
<ptxt>If the fuel pressure is being properly maintained, there are no problems with the sealing properties of the high-pressure fuel system. Therefore, an insufficient supply of fuel due to the fuel filter being clogged, or squashed or blocked low-pressure fuel pipes or hoses may be causing the reduction in common rail pressure.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0017" fin="false">OK</down>
<right ref="RM000004KNK010X_02_0022" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0017" proc-id="RM23G0E___000030R00001">
<testtitle>INSPECT CLOGGED FUEL PIPE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and turn the tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Data List / Diesel Injection.</ptxt>
</test1>
<test1>
<ptxt>With no load after the engine is warmed up, take a snapshot when idling and vehicle was accelerating with the accelerator pedal fully depressed in 2nd gear.</ptxt>
</test1>
<test1>
<ptxt>Check the Target Common Rail Pressure and Fuel Press value.</ptxt>
<figure>
<graphic graphicname="A261221E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>While idling the vehicle, "Fuel Press" deviates from the normal range and varies periodically with a period of approximately 2 seconds or more*1</ptxt>
</entry>
<entry morerows="1" valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"Fuel Press" is normal while idling the vehicle, but "Fuel Press" does not follow "Target Common Rail Pressure" when the accelerator pedal is fully depressed in 2nd gear*2</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<list1 type="unordered">
<item>
<ptxt>*1: The cause may be a squashed or blocked fuel pipe or hose.</ptxt>
</item>
<item>
<ptxt>*2: The cause may be a clogged fuel filter.</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0023" fin="false">A</down>
<right ref="RM000004KNK010X_02_0026" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0023" proc-id="RM23G0E___000030U00001">
<testtitle>REPAIR OR REPLACE CLOGGED FUEL LINE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Repair or replace the clogged (including frozen fuel) fuel pipe.</ptxt>
</test1>
<test1>
<ptxt>Replace the fuel filter element sub-assembly.</ptxt>
<atten4>
<ptxt>If "Fuel Press" does not follow "Target Common Rail Pressure" when the accelerator pedal is fully depressed in 2nd gear, replace the fuel filter element sub-assembly.</ptxt>
</atten4>
</test1>
</content6>
<res>
<right ref="RM000004KNK010X_02_0024" fin="false">NEXT</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0022" proc-id="RM23G0E___000030T00001">
<testtitle>REPLACE COMMON RAIL ASSEMBLY (PRESSURE DISCHARGE VALVE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the common rail assembly (See page <xref label="Seep01" href="RM0000044W300SX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0024" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0024" proc-id="RM23G0E___000030V00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0025" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0025" proc-id="RM23G0E___000030W00001">
<testtitle>READ VALUE USING INTELLIGENT TESTER (FUEL PRESS AND TARGET COMMON RAIL PRESSURE)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check Fuel Press and Target Common Rail Pressure in the snapshot taken in procedure 3 when the engine was idling.</ptxt>
<table pgwide="1">
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="4.96in"/>
<colspec colname="COL2" colwidth="2.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Difference between Fuel Press and Target Common Rail Pressure is 5000 kPa (51.0 kgf/cm<sup>2</sup>, 725 psi) or more</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Except above</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0029" fin="true">A</down>
<right ref="RM000004KNK010X_02_0026" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0026" proc-id="RM23G0E___000030X00001">
<testtitle>REPLACE SUCTION CONTROL VALVE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Replace the suction control valve (See page <xref label="Seep01" href="RM000004MF600DX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0027" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0027" proc-id="RM23G0E___000030Y00001">
<testtitle>BLEED AIR FROM FUEL SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Bleed the air from the fuel system (See page <xref label="Seep01" href="RM000002SY802OX_01_0002"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0028" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0028" proc-id="RM23G0E___000030Z00001">
<testtitle>PERFORM SUPPLY PUMP INITIALIZATION</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Perform supply pump initialization (See page <xref label="Seep01" href="RM000000TIN05LX"/>).</ptxt>
</test1>
</content6>
<res>
<down ref="RM000004KNK010X_02_0030" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0030">
<testtitle>CONFIRM WHETHER MALFUNCTION HAS BEEN SUCCESSFULLY REPAIRED</testtitle>
<res>
<down ref="RM000004KNK010X_02_0029" fin="true">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000004KNK010X_02_0029">
<testtitle>END</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>