<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97K_T00H8" variety="T00H8">
<name>ANTI-LOCK BRAKE SYSTEM</name>
<para id="RM000000XIF0BDX" category="C" type-id="803LE" name-id="BC67E-11" from="201207" to="201210">
<dtccode>C1427</dtccode>
<dtcname>Motor Malfunction</dtcname>
<subpara id="RM000000XIF0BDX_01" type-id="60" category="03" proc-id="RM23G0E___0000A4M00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<table pgwide="1">
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="3.12in"/>
<colspec colname="COL3" colwidth="2.90in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>C1427</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>The actuator pump motor does not operate properly.</ptxt>
</entry>
<entry valign="middle" align="left">
<list1 type="unordered">
<item>
<ptxt>Brake actuator assembly (Ground circuit)</ptxt>
</item>
<item>
<ptxt>Brake actuator assembly (Motor circuit)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
<subpara id="RM000000XIF0BDX_02" type-id="32" category="03" proc-id="RM23G0E___0000A4N00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<ptxt>Refer to DTCs C0273, C0274 and C1361 (See page <xref label="Seep01" href="RM000001JD60C6X_02"/>).</ptxt>
</content5>
</subpara>
<subpara id="RM000000XIF0BDX_03" type-id="51" category="05" proc-id="RM23G0E___0000A4O00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>When replacing the brake actuator assembly, perform calibration (See page <xref label="Seep01" href="RM000000XHR08IX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000000XIF0BDX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000000XIF0BDX_04_0002" proc-id="RM23G0E___0000A4P00000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (MOTOR RELAY)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Turn the ignition switch off</ptxt>
</test1>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Chassis / ABS/VSC/TRC / Active Test.</ptxt>
<table pgwide="1">
<title>ABS/VSC/TRC</title>
<tgroup cols="4" align="center">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COLSPEC0" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Motor Relay</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Motor relay</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Relay ON / OFF</ptxt>
</entry>
<entry align="left">
<ptxt>The operating sound of the motor can be heard.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Check for the operating sound of the motor relay when operating it with the intelligent tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.60in"/>
<colspec colname="COL2" colwidth="1.53in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Operating sound is not heard</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Operating sound is heard</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000000XIF0BDX_04_0007" fin="false">A</down>
<right ref="RM000000XIF0BDX_04_0003" fin="false">B</right>
</res>
</testgrp>
<testgrp id="RM000000XIF0BDX_04_0007" proc-id="RM23G0E___0000A2L00000">
<testtitle>CHECK HARNESS AND CONNECTOR (GND2 TERMINAL)
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A6 skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="left">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>A6-13 (GND2) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6><res>
<down ref="RM000000XIF0BDX_04_0003" fin="false">OK</down>
<right ref="RM000000XIF0BDX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000000XIF0BDX_04_0003" proc-id="RM23G0E___0000A4Q00000">
<testtitle>RECONFIRM DTC</testtitle>
<content6 releasenbr="1">
<atten4>
<ptxt>This code is stored when a problem is identified in the brake actuator assembly.</ptxt>
<ptxt>The motor relay is in the brake actuator assembly.</ptxt>
<ptxt>Therefore, motor relay unit inspections cannot be performed. Be sure to check if the DTC is output before replacing the brake actuator assembly.</ptxt>
</atten4>
<test1>
<ptxt>Turn the ignition switch off.</ptxt>
</test1>
<test1>
<ptxt>Reconnect the skid control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000XHV0EZX"/>).</ptxt>
</test1>
<test1>
<ptxt>Start the engine.</ptxt>
</test1>
<test1>
<ptxt>Drive the vehicle at a speed of 20 km/h (12 mph) or more for 30 seconds or more.</ptxt>
</test1>
<test1>
<ptxt>Check if the same DTC is output (See page <xref label="Seep02" href="RM000000XHV0EZX"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.69in"/>
<colspec colname="COL2" colwidth="1.44in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>If a speed signal of 6 km/h (4 mph) or more is input to the skid control ECU with the ignition switch ON and the stop light switch off, the ECU performs self-diagnosis of the motor and solenoid circuits.</ptxt>
</item>
<item>
<ptxt>If the normal system code is output (no trouble codes are output), slightly jiggle the connectors, wire harnesses, and fuses of the brake actuator assembly. Make sure that no DTCs are output.</ptxt>
</item>
<item>
<ptxt>If any DTCs are output while jiggling a connector or wire harness of the brake actuator assembly (skid control ECU), inspect and repair the connector or wire harness. The DTCs were probably output due to a bad connection of the connector terminal.</ptxt>
</item>
</list1>
</atten4>
</test1>
</content6>
<res>
<down ref="RM000000XIF0BDX_04_0004" fin="true">A</down>
<right ref="RM000000XIF0BDX_04_0006" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000000XIF0BDX_04_0004">
<testtitle>USE SIMULATION METHOD TO CHECK<xref label="Seep01" href="RM000003YMJ00HX"/>
</testtitle>
</testgrp>
<testgrp id="RM000000XIF0BDX_04_0005">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000000XIF0BDX_04_0006">
<testtitle>REPLACE BRAKE ACTUATOR ASSEMBLY<xref label="Seep01" href="RM0000034UX01JX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>