<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94H_T00E5" variety="T00E5">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1KD-FTV)</name>
<para id="RM0000012UR03RX" category="C" type-id="302FT" name-id="AT4JK-05" from="201207" to="201210">
<dtccode>P0776</dtccode>
<dtcname>Pressure Control Solenoid "B" Performance (Shift Solenoid Valve SL2)</dtcname>
<subpara id="RM0000012UR03RX_01" type-id="60" category="03" proc-id="RM23G0E___000083H00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>The TCM uses signals from the output shaft speed sensor and input speed sensor to detect the actual gear position (1st, 2nd, 3rd, 4th or 5th gear).</ptxt>
<ptxt>Then the TCM compares the actual gear with the shift schedule in the TCM memory to detect mechanical problems of the shift solenoid valves, valve body or automatic transmission (clutch, brake, gear, etc.).</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="2.83in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>DTC Code</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Detection Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>P0776</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gear required by the TCM does not match the actual gear when driving (2-trip detection logic*1, 1-trip detection logic*2).</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Shift solenoid valve SL2 remains open</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SLT remains open or closed</ptxt>
</item>
<item>
<ptxt>Valve body is blocked</ptxt>
</item>
<item>
<ptxt>Automatic transmission (clutch, brake, gear, etc.)</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<ptxt>*1: w/ OBD</ptxt>
<ptxt>*2: w/o OBD</ptxt>
</content5>
</subpara>
<subpara id="RM0000012UR03RX_02" type-id="64" category="03" proc-id="RM23G0E___000083I00000">
<name>MONITOR DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>This DTC indicates a stuck ON malfunction of shift solenoid valve SL2.</ptxt>
<ptxt>The TCM commands gear shifts by turning the shift solenoid valves ON/OFF. When the gear position commanded by the TCM and the actual gear position are not the same, the TCM illuminates the MIL and stores the DTC.</ptxt>
</content5>
</subpara>
<subpara id="RM0000012UR03RX_03" type-id="51" category="05" proc-id="RM23G0E___000083J00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<step1>
<ptxt>ACTIVE TEST</ptxt>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<step2>
<ptxt>Warm up the engine.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch off.</ptxt>
</step2>
<step2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</step2>
<step2>
<ptxt>Turn the ignition switch to ON.</ptxt>
</step2>
<step2>
<ptxt>Turn the intelligent tester on.</ptxt>
</step2>
<step2>
<ptxt>Enter the following menus: Powertrain / ECT / Active Test.</ptxt>
</step2>
<step2>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
<atten4>
<ptxt>While driving, the shift position can be forcibly changed with the intelligent tester.</ptxt>
<ptxt>Comparing the shift position commanded by the Active Test with the actual shift position enables you to confirm the problem (See page <xref label="Seep01" href="RM000000O8L0MZX"/>).</ptxt>
</atten4>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Control the Shift Position</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valves and set each shift position</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Press "→" button: Shift up</ptxt>
</item>
<item>
<ptxt>Press "←" button: Shift down</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Possible to check operation of the shift solenoid valves.</ptxt>
<ptxt>[Vehicle Condition]</ptxt>
<ptxt>50 km/h (31 mph) or less</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>This test can be conducted when the vehicle speed is 50 km/h (31 mph) or less.</ptxt>
</item>
<item>
<ptxt>The 4th to 5th up-shift must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>The 5th to 4th down-shift must be performed with the accelerator pedal released.</ptxt>
</item>
<item>
<ptxt>Do not operate the accelerator pedal for at least 2 seconds after shifting and do not shift successively.</ptxt>
</item>
<item>
<ptxt>The shift position commanded by the TCM is shown in the Data List display on the tester.</ptxt>
</item>
<item>
<ptxt>Shift solenoid valve SL2 is turned ON/OFF normally when the shift lever is in D.</ptxt>
<table pgwide="1">
<tgroup cols="6">
<colspec colname="COL1" align="left" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.13in"/>
<colspec colname="COL3" colwidth="1.13in"/>
<colspec colname="COL4" colwidth="1.13in"/>
<colspec colname="COL5" colwidth="1.13in"/>
<colspec colname="COL6" colwidth="1.14in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>TCM gear shift command</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>4th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5th</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shift solenoid valve SL2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>ON</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>OFF</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</step2>
</step1>
</content5>
</subpara>
<subpara id="RM0000012UR03RX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM0000012UR03RX_04_0001" proc-id="RM23G0E___000083K00000">
<testtitle>CHECK DTC OUTPUT (IN ADDITION TO DTC P0776)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Turn the ignition switch to ON.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / ECT / DTC.</ptxt>
</test1>
<test1>
<ptxt>Read the DTCs using the tester.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Only P0776 is output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>P0776 and other DTCs are output</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If any other codes besides P0776 are output, perform troubleshooting for those DTCs first.</ptxt>
</atten4>
</test1>
</content6>
<res>
<down ref="RM0000012UR03RX_04_0010" fin="false">A</down>
<right ref="RM0000012UR03RX_04_0004" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM0000012UR03RX_04_0010" proc-id="RM23G0E___000083900000">
<testtitle>PERFORM ACTIVE TEST USING INTELLIGENT TESTER (SHIFT SOLENOID VALVE SLT)
</testtitle>
<content6 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Perform the test while the ATF temperature is between 50 and 80°C (122 and 176°F).</ptxt>
</item>
<item>
<ptxt>Be careful to prevent the hose of SST from interfering with the exhaust pipe.</ptxt>
</item>
<item>
<ptxt>Perform the test with the A/C off.</ptxt>
</item>
</list1>
</atten3>
<atten4>
<ptxt>Using the intelligent tester to perform Active Tests allows relays, VSVs, actuators and other items to be operated without removing any parts. This non-intrusive functional inspection can be very useful because intermittent operation may be discovered before parts or wiring is disturbed. Performing Active Tests early in troubleshooting is one way to save diagnostic time. Data List information can be displayed while performing Active Tests.</ptxt>
</atten4>
<test1>
<ptxt>Remove the test plug from the transmission case and connect SST.</ptxt>
<figure>
<graphic graphicname="C214323E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<sst>
<sstitem>
<s-number>09992-00095</s-number>
<s-subnumber>09992-00231</s-subnumber>
<s-subnumber>09992-00271</s-subnumber>
</sstitem>
</sst>
<test1>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</test1>
<test1>
<ptxt>Start the engine and warm it up.</ptxt>
</test1>
<test1>
<ptxt>Measure the line pressure with SST.</ptxt>
</test1>
<test1>
<ptxt>Turn the intelligent tester on.</ptxt>
</test1>
<test1>
<ptxt>Enter the following menus: Powertrain / ECT / Active Test.</ptxt>
</test1>
<test1>
<ptxt>According to the display on the tester, perform the Active Test.</ptxt>
</test1>
<test1>
<ptxt>Measure the line pressure.</ptxt>
<table pgwide="1">
<title>ECT</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Test Part</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Control Range</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Note</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Activate the Solenoid (SLT)*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Operate shift solenoid valve SLT and raise line pressure</ptxt>
</entry>
<entry valign="middle">
<ptxt>ON or OFF</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>OFF: Line pressure up (when Active Test "Activate the Solenoid (SLT)" is performed, TCM commands shift solenoid valve SLT to turn OFF)</ptxt>
</item>
<item>
<ptxt>ON: No action (normal operation)</ptxt>
</item>
</list1>
</atten4>
</entry>
<entry valign="middle">
<ptxt>[Vehicle Condition]</ptxt>
<list1 type="unordered">
<item>
<ptxt>Vehicle stopped</ptxt>
</item>
<item>
<ptxt>Engine idling</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>*: Activate the Solenoid (SLT) in the Active Test is performed to check the line pressure changes by connecting SST to the automatic transmission, which is used in the Hydraulic Test (See page <xref label="Seep01" href="RM000000W7B0NEX"/>) as well. Note that the pressure values in the Active Test and hydraulic test are different.</ptxt>
</atten4>
<spec>
<title>OK</title>
<specitem>
<ptxt>The line pressure changes as specified when performing the Active Test.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM0000012UR03RX_04_0002" fin="false">OK</down>
<right ref="RM0000012UR03RX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012UR03RX_04_0002" proc-id="RM23G0E___000083L00000">
<testtitle>INSPECT SHIFT SOLENOID VALVE SL2</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove shift solenoid valve SL2.</ptxt>
</test1>
<figure>
<graphic graphicname="C197715E09" width="2.775699831in" height="3.779676365in"/>
</figure>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1 - 2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>20°C (68°F)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>5.0 to 5.6 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<test1>
<ptxt>Apply 12 V battery voltage to the shift solenoid valve and check that the valve moves and makes an operating noise.</ptxt>
<spec>
<title>OK</title>
<table>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Measurement Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Battery positive (+) with a 21 W bulb → Terminal 1</ptxt>
</item>
<item>
<ptxt>Battery negative (-) → Terminal 2</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Valve moves and makes an operating noise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" align="left" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Component without harness connected</ptxt>
<ptxt>(Shift Solenoid Valve SL2)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content6>
<res>
<down ref="RM0000012UR03RX_04_0011" fin="false">OK</down>
<right ref="RM0000012UR03RX_04_0005" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012UR03RX_04_0011" proc-id="RM23G0E___000083A00000">
<testtitle>INSPECT TRANSMISSION VALVE BODY ASSEMBLY
</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check the transmission valve body assembly (See page <xref label="Seep01" href="RM0000013FG02LX"/>).</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>There are no foreign objects on any valve.</ptxt>
</specitem>
</spec>
</test1>
</content6><res>
<down ref="RM0000012UR03RX_04_0007" fin="true">OK</down>
<right ref="RM0000012UR03RX_04_0006" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM0000012UR03RX_04_0007">
<testtitle>REPAIR OR REPLACE AUTOMATIC TRANSMISSION ASSEMBLY<xref label="Seep01" href="RM0000013F600XX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012UR03RX_04_0004">
<testtitle>GO TO DTC CHART<xref label="Seep01" href="RM0000030G906KX"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012UR03RX_04_0009">
<testtitle>REPLACE SHIFT SOLENOID VALVE SLT<xref label="Seep01" href="RM0000013FG02LX_01_0005"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012UR03RX_04_0005">
<testtitle>REPLACE SHIFT SOLENOID VALVE SL2<xref label="Seep01" href="RM0000013FG02LX_01_0004"/>
</testtitle>
</testgrp>
<testgrp id="RM0000012UR03RX_04_0006">
<testtitle>REPAIR OR REPLACE TRANSMISSION VALVE BODY ASSEMBLY<xref label="Seep01" href="RM0000013CM04BX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>