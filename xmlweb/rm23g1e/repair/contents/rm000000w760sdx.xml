<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12016_S0013" variety="S0013">
<name>A750F AUTOMATIC TRANSMISSION / TRANSAXLE</name>
<ttl id="12016_S0013_7B94H_T00E5" variety="T00E5">
<name>AUTOMATIC TRANSMISSION SYSTEM (for 1KD-FTV)</name>
<para id="RM000000W760SDX" category="D" type-id="303F3" name-id="AT61J-03" from="201207" to="201210">
<name>DIAGNOSIS SYSTEM</name>
<subpara id="RM000000W760SDX_z0" proc-id="RM23G0E___000086E00000">
<content5 releasenbr="1">
<step1>
<ptxt>EURO-OBD</ptxt>
<step2>
<ptxt>When troubleshooting Europe On-Board Diagnostic (Euro-OBD) vehicles, the vehicle must be connected to an OBD scan tool (complying with ISO 15765-4). Various data output from the vehicle TCM can then be read.</ptxt>
</step2>
<step2>
<ptxt>Euro-OBD regulations require that the vehicle on-board computer illuminate the Malfunction Indicator Lamp (MIL) on the instrument panel when the computer detects a malfunction in any of the following:</ptxt>
<figure>
<graphic graphicname="C201372" width="2.775699831in" height="1.771723296in"/>
</figure>
<list1 type="unordered">
<item>
<ptxt>The emission control system/components</ptxt>
</item>
<item>
<ptxt>The powertrain control components which affect vehicle emissions</ptxt>
</item>
<item>
<ptxt>The computer</ptxt>
</item>
</list1>
<ptxt>In addition, the applicable Diagnostic Trouble Codes (DTCs) prescribed by ISO 15765-4 are stored in the TCM memory.</ptxt>
<ptxt>If the malfunction does not recur in 3 consecutive trips, the MIL goes off automatically but the DTCs remain stored in the TCM memory.</ptxt>
</step2>
<step2>
<ptxt>To check DTCs, connect the intelligent tester or OBD scan tool to the Data Link Connector 3 (DLC3) of the vehicle. The scan tool displays DTCs, freeze frame data and a variety of engine data.</ptxt>
<ptxt>The DTCs and freeze frame data can be cleared with the scan tool (See page <xref label="Seep01" href="RM000000W770S3X"/>).</ptxt>
</step2>
</step1>
<step1>
<ptxt>M-OBD (EXCEPT EUROPEAN SPECIFICATION)</ptxt>
<step2>
<ptxt>When troubleshooting Multiplex On-Board Diagnostic (M-OBD) vehicles, the vehicle must be connected to the intelligent tester. Various data output from the TCM can then be read.</ptxt>
</step2>
<step2>
<ptxt>OBD regulations require that the vehicle on-board computer illuminate the MIL on the instrument panel when the computer detects a malfunction in any of the following:</ptxt>
<figure>
<graphic graphicname="C201372" width="2.775699831in" height="1.771723296in"/>
</figure>
<list1 type="unordered">
<item>
<ptxt>The emission control system/components</ptxt>
</item>
<item>
<ptxt>The powertrain control components which affect vehicle emissions</ptxt>
</item>
<item>
<ptxt>The computer</ptxt>
</item>
</list1>
<ptxt>In addition, the applicable Diagnostic Trouble Codes (DTCs) prescribed by ISO 15765-4 are stored in the TCM memory.</ptxt>
<ptxt>If the malfunction does not recur in 3 consecutive trips, the MIL goes off automatically but the DTCs remain stored in the TCM memory.</ptxt>
</step2>
</step1>
<step1>
<ptxt>NORMAL MODE AND CHECK MODE</ptxt>
<ptxt>The diagnosis system operates in normal mode during normal vehicle use. In normal mode, 2-trip detection logic is used to ensure accurate detection of malfunctions. Check mode is also available to technicians as an option. In check mode, 1-trip detection logic is used to increase the ability of the system to detect malfunctions, including intermittent malfunctions, when reproducing malfunction symptoms (intelligent tester only).</ptxt>
</step1>
<step1>
<ptxt>2-TRIP DETECTION LOGIC</ptxt>
<step2>
<ptxt>When a malfunction is first detected, the malfunction is temporarily stored in the TCM memory (1st trip). If the ignition switch is turned off and then turned ON again, and the same malfunction is detected again, the MIL illuminates.</ptxt>
</step2>
</step1>
<step1>
<ptxt>FREEZE FRAME DATA</ptxt>
<step2>
<ptxt>The TCM records vehicle and driving condition information as freeze frame data the moment a DTC is stored. When troubleshooting, freeze frame data can be helpful in determining whether the vehicle was moving or stationary, whether the engine was warmed up or not, whether the air-fuel ratio was lean or rich, as well as other data recorded at the time of a malfunction.</ptxt>
</step2>
<step2>
<ptxt>The intelligent tester displays freeze frame data recorded at five different points: 1) 3 times before the DTC is stored, 2) once when the DTC is stored, and 3) once after the DTC is stored. The data can be used to reproduce the vehicle condition from around the time of the malfunction. The data may be helpful in determining the cause of a malfunction. It may also be helpful in determining whether a DTC is being caused by a temporary malfunction.</ptxt>
<figure>
<graphic graphicname="C140837E08" width="2.775699831in" height="1.771723296in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>CHECK DATA LINK CONNECTOR 3 (DLC3) (See page <xref label="Seep02" href="RM000003YMH00EX"/>)</ptxt>
</step1>
<step1>
<ptxt>CHECK BATTERY VOLTAGE</ptxt>
<spec>
<title>Standard voltage</title>
<specitem>
<ptxt>11 to 14 V</ptxt>
</specitem>
</spec>
<ptxt>If the voltage is below 11 V, replace the battery before proceeding.</ptxt>
</step1>
<step1>
<ptxt>CHECK MIL</ptxt>
<step2>
<ptxt>Check that the MIL illuminates when the ignition switch is turned to ON.</ptxt>
<ptxt>If the MIL does not illuminate, there is a problem in the MIL circuit (for w/ DPF: See page <xref label="Seep03" href="RM000000WZ10W2X"/>, for w/o DPF <xref label="Seep04" href="RM000000WZ10W1X"/>).</ptxt>
</step2>
<step2>
<ptxt>Check that when the engine is started, the MIL goes off.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>