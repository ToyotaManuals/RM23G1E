<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="57">
<name>Power Source / Network</name>
<section id="12050_S001W" variety="S001W">
<name>NETWORKING</name>
<ttl id="12050_S001W_7B9AN_T00KB" variety="T00KB">
<name>CAN COMMUNICATION SYSTEM (for LHD without Entry and Start System)</name>
<para id="RM000002L8203PX" category="J" type-id="801MT" name-id="NW2GN-02" from="201207" to="201210">
<dtccode/>
<dtcname>Open in CAN Main Wire</dtcname>
<subpara id="RM000002L8203PX_01" type-id="60" category="03" proc-id="RM23G0E___0000DWC00000">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>There may be an open circuit in the CAN main wire and/or DLC3 branch wire when the resistance between terminals 6 (CANH) and 14 (CANL) of the DLC3 is 69 Ω or higher.</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>The resistance between terminals 6 (CANH) and 14 (CANL) of the DLC3 is 69 Ω or higher.</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>CAN main wire and connector</ptxt>
</item>
<item>
<ptxt>ECM*1</ptxt>
</item>
<item>
<ptxt>No. 1 junction connector</ptxt>
</item>
<item>
<ptxt>No. 2 junction connector</ptxt>
</item>
<item>
<ptxt>No. 3 junction connector</ptxt>
</item>
<item>
<ptxt>No. 4 junction connector</ptxt>
</item>
<item>
<ptxt>No. 5 junction connector*2</ptxt>
</item>
<item>
<ptxt>Combination meter assembly</ptxt>
</item>
<item>
<ptxt>DLC3 branch wire or connector</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for 2TR-FE</ptxt>
</item>
<item>
<ptxt>*2: for 5L-E</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000002L8203PX_02" type-id="32" category="03" proc-id="RM23G0E___0000DWD00000">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="C217560E01" width="7.106578999in" height="6.791605969in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000002L8203PX_03" type-id="51" category="05" proc-id="RM23G0E___0000DWE00000">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten4>
<ptxt>Operating the ignition switch, any switches or any doors triggers related ECU and sensor communication with the CAN, which causes resistance variation.</ptxt>
</atten4>
</content5>
</subpara>
<subpara id="RM000002L8203PX_05" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000002L8203PX_05_0024" proc-id="RM23G0E___0000DWR00000">
<testtitle>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</testtitle>
<content6 releasenbr="2">
<test1>
<ptxt>Disconnect the cable from the negative (-) battery terminal before measuring the resistances of the main wire and the branch wire.</ptxt>
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0001" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0001" proc-id="RM23G0E___0000DWF00000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (NO. 4 JUNCTION CONNECTOR - DLC3)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G94 No. 4 junction connector connector.</ptxt>
<figure>
<graphic graphicname="C216818E53" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry>
<ptxt>Tester Connection</ptxt>
</entry>
<entry>
<ptxt>Switch Condition</ptxt>
</entry>
<entry>
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G94-5 (CANH) - G48-6 (CANH)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>G94-16 (CANL) - G48-14 (CANL)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 4 Junction Connector)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of DLC3</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0003" fin="false">OK</down>
<right ref="RM000002L8203PX_05_0015" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0003" proc-id="RM23G0E___0000DWH00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 4 JUNCTION CONNECTOR - COMBINATION METER)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E35" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G94-2 (CANH) - G94-13 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" align="center" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 4 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0002" fin="false">OK</down>
<right ref="RM000002L8203PX_05_0012" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0002" proc-id="RM23G0E___0000DWG00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 4 JUNCTION CONNECTOR - ECM OR NO. 5 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E36" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G94-1 (CANH) - G94-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 4 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0021" fin="true">OK</down>
<right ref="RM000002L8203PX_05_0004" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0012" proc-id="RM23G0E___0000DWP00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the G94 No. 4 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0013" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0013" proc-id="RM23G0E___0000DWQ00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (COMBINATION METER - NO. 4 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G6 combination meter assembly connector.</ptxt>
<figure>
<graphic graphicname="C198950E11" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G6-40 (CANH) - G6-39 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Combination Meter Assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0014" fin="true">OK</down>
<right ref="RM000002L8203PX_05_0022" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0004" proc-id="RM23G0E___0000DWI00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the G94 No. 4 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0011" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0011" proc-id="RM23G0E___0000DWO00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 3 JUNCTION CONNECTOR - ECM OR NO. 5 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G100 No. 3 junction connector connector.</ptxt>
<figure>
<graphic graphicname="C216814E37" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G100-1 (CANH) - G100-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 3 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0010" fin="false">OK</down>
<right ref="RM000002L8203PX_05_0027" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0010" proc-id="RM23G0E___0000DWN00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 3 JUNCTION CONNECTOR - NO. 4 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E38" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G100-6 (CANH) - G100-17 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 3 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0016" fin="true">OK</down>
<right ref="RM000002L8203PX_05_0020" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0027" proc-id="RM23G0E___0000DWS00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the G100 No. 3 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0005" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0005" proc-id="RM23G0E___0000DWJ00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 2 JUNCTION CONNECTOR - ECM OR NO. 5 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G97 No. 2 junction connector connector.</ptxt>
<figure>
<graphic graphicname="C216814E39" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G97-6 (CANH) - G97-17 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 2 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0006" fin="false">OK</down>
<right ref="RM000002L8203PX_05_0007" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0006" proc-id="RM23G0E___0000DWK00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 2 JUNCTION CONNECTOR - NO. 3 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E40" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G97-1 (CANH) - G97-12 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 2 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0019" fin="true">OK</down>
<right ref="RM000002L8203PX_05_0017" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0007" proc-id="RM23G0E___0000DWL00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the G97 No. 2 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0031" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0031" proc-id="RM23G0E___0000DWT00000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (NO. 1 JUNCTION CONNECTOR - ECM OR NO. 5 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the A72 No. 1 junction connector connector.</ptxt>
<figure>
<graphic graphicname="C216814E42" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A72-10 (CANH) - A72-21 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 1 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 2TR-FE)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for 5L-E)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0032" fin="false">A</down>
<right ref="RM000002L8203PX_05_0033" fin="false">B</right>
<right ref="RM000002L8203PX_05_0038" fin="false">C</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0032" proc-id="RM23G0E___0000DWU00000">
<testtitle>CHECK FOR OPEN IN CAN BUS WIRE (NO. 1 JUNCTION CONNECTOR - NO. 2 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<figure>
<graphic graphicname="C216814E41" width="2.775699831in" height="1.771723296in"/>
</figure>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>A72-9 (CANH) - A72-20 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 1 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0035" fin="true">OK</down>
<right ref="RM000002L8203PX_05_0034" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0033" proc-id="RM23G0E___0000DWV00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the A72 No. 1 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0008" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0008" proc-id="RM23G0E___0000DWM00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (ECM - NO. 1 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G60 ECM connector.</ptxt>
<figure>
<graphic graphicname="C215892E12" width="2.775699831in" height="2.775699831in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G60-34 (CANH) - G60-33 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to ECM)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0029" fin="true">OK</down>
<right ref="RM000002L8203PX_05_0028" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0038" proc-id="RM23G0E___0000DWW00000">
<testtitle>CONNECT CONNECTOR</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Reconnect the A72 No. 1 junction connector connector.</ptxt>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0039" fin="false">NEXT</down>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0039" proc-id="RM23G0E___0000DWX00000">
<testtitle>CHECK FOR OPEN IN CAN BUS MAIN WIRE (NO. 5 JUNCTION CONNECTOR - NO. 1 JUNCTION CONNECTOR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G101 No. 5 junction connector connector.</ptxt>
<figure>
<graphic graphicname="C210060E04" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>G101-3 (CANH) - G101-2 (CANL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Ignition switch off</ptxt>
</entry>
<entry valign="middle">
<ptxt>108 to 132 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Rear view of wire harness connector</ptxt>
<ptxt>(to No. 5 Junction Connector)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000002L8203PX_05_0036" fin="true">OK</down>
<right ref="RM000002L8203PX_05_0040" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000002L8203PX_05_0015">
<testtitle>REPAIR OR REPLACE CAN BRANCH WIRE CONNECTED TO DLC3 (CANH, CANL)</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0021">
<testtitle>REPLACE NO. 4 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0014">
<testtitle>REPLACE COMBINATION METER ASSEMBLY<xref label="Seep01" href="RM000003QKY00MX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0022">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE CONNECTED TO COMBINATION METER (COMBINATION METER - NO. 4 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0016">
<testtitle>REPLACE NO. 3 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0020">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE OR CONNECTOR (NO. 3 JUNCTION CONNECTOR - NO. 4 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0019">
<testtitle>REPLACE NO. 2 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0017">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE OR CONNECTOR (NO. 2 JUNCTION CONNECTOR - NO. 3 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0035">
<testtitle>REPLACE NO. 1 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0034">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE OR CONNECTOR (NO. 1 JUNCTION CONNECTOR - NO. 2 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0029">
<testtitle>REPLACE ECM<xref label="Seep01" href="RM000000VW202NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0028">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE CONNECTED TO ECM (ECM - NO. 1 JUNCTION CONNECTOR)</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0036">
<testtitle>REPLACE NO. 5 JUNCTION CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000002L8203PX_05_0040">
<testtitle>REPAIR OR REPLACE CAN MAIN WIRE CONNECTED TO NO. 5 JUNCTION CONNECTOR (NO. 5 JUNCTION CONNECTOR - NO. 1 JUNCTION CONNECTOR)</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>