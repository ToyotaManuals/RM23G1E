<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12030_S001G" variety="S001G">
<name>BRAKE CONTROL / DYNAMIC CONTROL SYSTEMS</name>
<ttl id="12030_S001G_7B97M_T00HA" variety="T00HA">
<name>VEHICLE STABILITY CONTROL SYSTEM (for Vacuum Brake Booster)</name>
<para id="RM000000XHN0FDX" category="T" type-id="3001H" name-id="BC002K-434" from="201210">
<name>PROBLEM SYMPTOMS TABLE</name>
<subpara id="RM000000XHN0FDX_z0" proc-id="RM23G0E___0000AI000001">
<content5 releasenbr="3">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the table below to help determine the cause of problem symptoms. If multiple suspected areas are listed, the potential causes of the symptoms are listed in order of probability in the "Suspected Area" column of the table. Check each symptom by checking the suspected areas in the order they are listed. Replace parts as necessary.</ptxt>
</item>
<item>
<ptxt>Inspect the fuses and relays related to this system before inspecting the suspected areas below.</ptxt>
</item>
</list1>
</atten4>
<atten3>
<ptxt>When replacing the brake actuator assembly, sensor, etc., turn the ignition switch off.</ptxt>
</atten3>
<table frame="all" colsep="1" rowsep="1" pgwide="1">
<title>Vehicle Stability Control System</title>
<tgroup cols="3" colsep="1" rowsep="1">
<colspec colnum="1" colname="1" colwidth="2.79in" colsep="0"/>
<colspec colnum="2" colname="2" colwidth="3.17in" colsep="0"/>
<colspec colnum="3" colname="3" colwidth="1.12in" colsep="0"/>
<thead valign="top">
<row rowsep="1">
<entry colname="1" colsep="1" valign="middle" align="center">
<ptxt>Symptom</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle" align="center">
<ptxt>Suspected Area</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>See page</ptxt>
</entry>
</row>
</thead>
<tbody valign="top">
<row rowsep="1">
<entry morerows="10" colsep="1" valign="middle">
<ptxt>ABS, BA and/or EBD does not operate</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check for DTCs again and make sure that normal system code is output</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XHV0G5X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>IG power source circuit (low power supply voltage malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XW60FYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>IG power source circuit (high power supply voltage malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (comparison malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XI90SPX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (open or short malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XI90SQX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (output malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIA0K2X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (comparison malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369R06AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (open or short malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369306BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (output malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369S06FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check brake actuator assembly with intelligent tester (check brake actuator assembly operation using Active Test function). If abnormal, check hydraulic circuit for leaks.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002VQ702IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>If symptoms still occur even after above circuits in suspected area column have been inspected and proved to be normal, replace brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="9" colsep="1" valign="middle">
<ptxt>ABS, BA and/or EBD does not operate efficiently</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check for DTCs again and make sure that normal system code is output</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XHV0G5X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (comparison malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XI90SPX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (open or short malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XI90SQX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (output malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIA0K2X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (comparison malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369R06AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (open or short malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369306BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (output malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369S06FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Stop light switch circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIE0NXX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check brake actuator assembly with the intelligent tester (check brake actuator assembly operation using Active Test function). If abnormal, check hydraulic circuit for leaks.</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000002VQ702IX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>If symptoms still occur even after above circuits in suspected area column have been inspected and proved to be normal, replace brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle">
<ptxt>ABS warning light abnormal (Remains on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>ABS warning light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIH0G8X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>IG power source circuit (High power supply voltage malfunction)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle">
<ptxt>ABS warning light abnormal (Does not come on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>ABS warning light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIQ0G4X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle">
<ptxt>Brake warning light abnormal (Remains on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake warning light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000YHG0G5X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>IG power source circuit (High power supply voltage malfunction)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle">
<ptxt>Brake warning light abnormal (Does not come on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake warning light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000YHH0G5X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="20" colsep="1" valign="middle">
<ptxt>VSC and/or TRC does not operate</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check for DTCs again and make sure that normal system code is output</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XHV0G5X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>IG power source circuit (low power supply voltage malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XW60FYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>IG power source circuit (high power supply voltage malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Check hydraulic circuit for leaks</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (comparison malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XI90SPX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (open or short malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XI90SQX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Front speed sensor circuit (output malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIA0K2X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (comparison malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369R06AX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (open or short malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369306BX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Rear speed sensor circuit (output malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000369S06FX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Yaw rate and acceleration sensor circuit (zero point malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIK0BUX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Yaw rate and acceleration sensor circuit (power supply malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RFV0H2X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Acceleration sensor circuit (comparison malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000035P605QX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Yaw rate sensor circuit (comparison malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000035P505ZX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Yaw rate and acceleration sensor circuit (stuck or output malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001RFV0H1X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Yaw rate and acceleration sensor circuit (internal malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000035P4066X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Steering angle sensor circuit (zero point malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000ZRR0CYX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Steering angle sensor circuit (power supply malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIM0Q0X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Steering angle sensor circuit (output malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIM0Q2X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Steering angle sensor circuit (internal malfunction)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIM0Q1X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>If symptoms still occur even after above circuits in suspected area column have been inspected and proved to be normal, replace brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle">
<ptxt>VSC OFF indicator abnormal (Remains on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>VSC OFF indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001FQO0FVX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle">
<ptxt>VSC OFF indicator abnormal (Does not come on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>VSC OFF indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001FQP0FTX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="2" colsep="1" valign="middle">
<ptxt>Slip indicator light abnormal (Remains on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Slip indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000YHI0C8X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>IG power source circuit (High power supply voltage malfunction)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry morerows="1" colsep="1" valign="middle">
<ptxt>Slip indicator light abnormal (Does not come on)</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>Slip indicator light circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000YHJ0C8X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="1" colsep="1" valign="middle">
<ptxt>TRC OFF indicator light abnormal (Remains on)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>TRC OFF indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003QXV01GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="1" colsep="1" valign="middle">
<ptxt>TRC OFF indicator light abnormal (Does not come on)</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>TRC OFF indicator light circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000003QXW01GX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry morerows="3" colsep="1" valign="middle">
<ptxt>DTC check cannot be performed</ptxt>
</entry>
<entry colsep="1" valign="middle">
<ptxt>Check for DTCs again and make sure that normal system code is output</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XHV0G5X" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>TC and CG terminal circuit</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIU0KVX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>IG power source circuit (high power supply voltage malfunction)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM00000368W06RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row>
<entry colsep="1" valign="middle">
<ptxt>If symptoms still occur even after above circuits in suspected area column have been inspected and proved to be normal, replace brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="1" morerows="1" colsep="1" valign="middle">
<ptxt>Sensor check cannot be performed</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>TS and CG terminal circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000001DXV02RX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colname="2" colsep="1" valign="middle">
<ptxt>Brake actuator assembly (skid control ECU)</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM0000034UX01UX" label="XX-XX"/>
</ptxt>
</entry>
</row>
<row rowsep="1">
<entry colsep="1" valign="middle">
<ptxt>Buzzer abnormal</ptxt>
</entry>
<entry colname="2" colsep="1" valign="middle">
<ptxt>VSC buzzer circuit</ptxt>
</entry>
<entry colname="3" colsep="1" valign="middle" align="center">
<ptxt>
<xref href="RM000000XIS0B2X" label="XX-XX"/>
</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>