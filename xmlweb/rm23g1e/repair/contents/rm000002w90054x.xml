<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="54">
<name>Brake</name>
<section id="12031_S001H" variety="S001H">
<name>BRAKE SYSTEM (OTHER)</name>
<ttl id="12031_S001H_7B97Y_T00HM" variety="T00HM">
<name>BRAKE PEDAL (for Vacuum Brake Booster)</name>
<para id="RM000002W90054X" category="A" type-id="80001" name-id="BR504-02" from="201207" to="201210">
<name>REMOVAL</name>
<subpara id="RM000002W90054X_02" type-id="11" category="10" proc-id="RM23G0E___0000ATG00000">
<content3 releasenbr="1">
<atten4>
<list1 type="unordered">
<item>
<ptxt>Use the same procedure for RHD and LHD vehicles.</ptxt>
</item>
<item>
<ptxt>The procedure listed below is for LHD vehicles. </ptxt>
</item>
</list1>
</atten4>
</content3>
</subpara>
<subpara id="RM000002W90054X_01" type-id="01" category="01">
<s-1 id="RM000002W90054X_01_0039" proc-id="RM23G0E___0000ATD00000">
<ptxt>DISCONNECT CABLE FROM NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten2>
<ptxt>Wait at least 90 seconds after disconnecting the cable from the negative (-) battery terminal to disable the SRS system.</ptxt>
</atten2>
<atten3>
<list1 type="unordered">
<item>
<ptxt>After turning the ignition switch off, waiting time may be required before disconnecting the cable from the battery terminal. Therefore, make sure to read the disconnecting the cable from the battery terminal notice before proceeding with work (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</item>
<item>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep02" href="RM000003YMF00IX"/>).</ptxt>
</item>
</list1>
</atten3>
</content1>
</s-1>
<s-1 id="RM000002W90054X_01_0047" proc-id="RM23G0E___0000ATE00000">
<ptxt>REMOVE LOWER NO. 1 INSTRUMENT PANEL AIRBAG ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the lower No. 1 instrument panel airbag assembly (See page <xref label="Seep01" href="RM000003YPV01WX"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002W90054X_01_0050">
<ptxt>REMOVE BRAKE PEDAL RETURN SPRING</ptxt>
</s-1>
<s-1 id="RM000002W90054X_01_0002" proc-id="RM23G0E___0000ATA00000">
<ptxt>REMOVE PUSH ROD PIN</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the clip and push rod pin from the brake pedal.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002W90054X_01_0038" proc-id="RM23G0E___0000ATC00000">
<ptxt>REMOVE STOP LIGHT SWITCH ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the stop light switch connector.</ptxt>
</s2>
<s2>
<ptxt>Remove the stop light switch assembly (See page <xref label="Seep01" href="RM000003QJT00TX_01_0003"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000002W90054X_01_0049" proc-id="RM23G0E___0000ATF00000">
<ptxt>DISCONNECT BRAKE PEDAL LOAD SENSING SWITCH</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Disconnect the brake pedal load sensing switch connector.</ptxt>
<figure>
<graphic graphicname="C215262E01" width="2.775699831in" height="3.779676365in"/>
</figure>
</s2>
</content1>
</s-1>
<s-1 id="RM000002W90054X_01_0003" proc-id="RM23G0E___0000ATB00000">
<ptxt>REMOVE BRAKE PEDAL SUPPORT ASSEMBLY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Remove the brake pedal support reinforcement set bolt.</ptxt>
<figure>
<graphic graphicname="C215922" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for LHD:</ptxt>
<s3>
<ptxt>Remove the brake master cylinder sub-assembly (See page <xref label="Seep01" href="RM000002WF104UX"/>).</ptxt>
</s3>
<s3>
<ptxt>Remove the brake booster assembly (See page <xref label="Seep02" href="RM000002WLS03RX"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<s3>
<ptxt>Remove the brake actuator assembly (See page <xref label="Seep03" href="RM0000034UX01JX"/>).</ptxt>
</s3>
<s3>
<ptxt>Remove the brake master cylinder sub-assembly (See page <xref label="Seep04" href="RM000002WF104UX"/>).</ptxt>
</s3>
<s3>
<ptxt>Remove the brake booster assembly (See page <xref label="Seep05" href="RM000002WLS03RX"/>).</ptxt>
</s3>
</s2>
<s2>
<ptxt>Remove the 4 nuts and brake pedal support assembly.</ptxt>
<figure>
<graphic graphicname="C214646" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
</content1>
</s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>