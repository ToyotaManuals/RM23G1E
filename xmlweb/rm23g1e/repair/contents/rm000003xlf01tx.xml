<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001Q" variety="S001Q">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001Q_7B9A5_T00JT" variety="T00JT">
<name>REAR VIEW MONITOR SYSTEM (w/ Side Monitor System)</name>
<para id="RM000003XLF01TX" category="J" type-id="803ZW" name-id="PM583-02" from="201210">
<dtccode/>
<dtcname>"CHK" message(s) are displayed on the SIGNAL CHECK screen.</dtcname>
<subpara id="RM000003XLF01TX_01" type-id="60" category="03" proc-id="RM23G0E___0000CZM00001">
<name>DESCRIPTION</name>
<content5 releasenbr="1">
<ptxt>On the SIGNAL CHECK screen, it is possible to check if the signals sent to the parking assist ECU are normal (See page <xref label="Seep01" href="RM00000472I00AX"/>).</ptxt>
<atten4>
<list1 type="unordered">
<item>
<ptxt>On the SIGNAL CHECK screen, "OK" (blue) is displayed for items with a normal inspection result or input state.</ptxt>
</item>
<item>
<ptxt>On the SIGNAL CHECK screen, "CHK" (red) is displayed for items with an abnormal inspection result or input state.</ptxt>
</item>
<item>
<ptxt>Displayed items may differ depending on vehicle specifications.</ptxt>
</item>
</list1>
</atten4>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="0.76in"/>
<colspec colname="COL2" colwidth="1.50in"/>
<colspec colname="COLSPEC1" colwidth="1.50in"/>
<colspec colname="COLSPEC0" colwidth="0.99in"/>
<colspec colname="COL5" colwidth="2.33in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Item</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Signal Input Method</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detail</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC Output when Abnormal Result is Displayed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Signal Receiver</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>SPEED</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Speed signal input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Skid control ECU</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>CAMERA SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle wire harness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering pad switch assembly (wide view front and side monitor switch) signal input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is not output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering pad switch assembly (wide view front and side monitor switch)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>BCTY</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Status of CAN communication with main body ECU</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main body ECU (Multiplex network body ECU)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MIRROR SW</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Outer rear view mirror retract signal input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Main body ECU (Multiplex network body ECU)</ptxt>
</item>
<item>
<ptxt>Outer rear view mirror assembly</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>SHIFT</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle wire harness</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shift signal input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Park/neutral position switch assembly*1</ptxt>
</item>
<item>
<ptxt>Back-up light switch assembly*2</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>STR SIG</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering angle sensor signal input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Spiral cable sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>HANDLE</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering position signal input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main body ECU (Multiplex network body ECU)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>BODY</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Body size signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main body ECU (Multiplex network body ECU)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>DEST</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Destination information signal input</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Main body ECU (Multiplex network body ECU)</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>MTM</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>Multi-terrain monitor signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>Individual setting</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>T/M</ptxt>
</entry>
<entry valign="middle">
<ptxt>CAN communication</ptxt>
</entry>
<entry valign="middle">
<ptxt>Transmission type signal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>DTC is output</ptxt>
</entry>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: for Automatic Transmission</ptxt>
</item>
<item>
<ptxt>*2: for Manual Transmission</ptxt>
</item>
</list1>
</content5>
</subpara>
<subpara id="RM000003XLF01TX_02" type-id="32" category="03" proc-id="RM23G0E___0000CZN00001">
<name>WIRING DIAGRAM</name>
<content5 releasenbr="1">
<figure>
<graphic graphicname="E199588E01" width="7.106578999in" height="8.799559038in"/>
</figure>
</content5>
</subpara>
<subpara id="RM000003XLF01TX_03" type-id="51" category="05" proc-id="RM23G0E___0000CZO00001">
<name>INSPECTION PROCEDURE</name>
<content5 releasenbr="1">
<atten3>
<ptxt>Depending on the parts that are replaced or operations that are performed during vehicle inspection or maintenance, calibration of other systems as well as the parking assist monitor system may be needed (See page <xref label="Seep02" href="RM00000472200BX"/>).</ptxt>
</atten3>
</content5>
</subpara>
<subpara id="RM000003XLF01TX_04" type-id="01" category="01">
<name>PROCEDURE</name>
<testgrp id="RM000003XLF01TX_04_0001" proc-id="RM23G0E___0000CZP00001">
<testtitle>CHECK DISPLAY CHECK MODE</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check which items display "CHK" (red) on the SIGNAL CHECK screen.</ptxt>
<figure>
<graphic graphicname="E199589" width="2.775699831in" height="1.771723296in"/>
</figure>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.01in"/>
<colspec colname="COL2" colwidth="1.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>"CAMERA SW" displays "CHK" (red)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"MIRROR SW" displays "CHK" (red)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"SHIFT" displays "CHK" (red)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"MTM" displays "CHK" (red)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>"SPEED", "BCTY", "STR SIG", "HANDLE", "BODY", "DEST" or "T/M" displays "CHK" (red)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>E</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0003" fin="false">A</down>
<right ref="RM000003XLF01TX_04_0002" fin="false">B</right>
<right ref="RM000003XLF01TX_04_0018" fin="false">C</right>
<right ref="RM000003XLF01TX_04_0025" fin="false">D</right>
<right ref="RM000003XLF01TX_04_0007" fin="true">E</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0003" proc-id="RM23G0E___0000CZR00001">
<testtitle>INSPECT STEERING PAD SWITCH ASSEMBLY (WIDE VIEW FRONT AND SIDE MONITOR SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the steering pad switch assembly (See page <xref label="Seep01" href="RM0000039SA01CX"/>).</ptxt>
<figure>
<graphic graphicname="E192966E11" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COLSPEC1" colwidth="2.83in"/>
<colspec colname="COLSPEC0" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Wide View Front and Side Monitor Switch</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Switch Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>7 (+DP) - 3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wide view front and side monitor switch pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 2.5 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7 (+DP) - 3 (GND1)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Wide view front and side monitor switch not pushed</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>95 to 105 kΩ</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0035" fin="false">OK</down>
<right ref="RM000003XLF01TX_04_0009" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0035" proc-id="RM23G0E___0000D0000001">
<testtitle>INSPECT SPIRAL CABLE SUB-ASSEMBLY</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Remove the spiral cable sub-assembly (See page <xref label="Seep02" href="RM000002O8X01NX"/>).</ptxt>
<figure>
<graphic graphicname="E168547E51" width="2.775699831in" height="4.7836529in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COLSPEC0" colwidth="1.37in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="2" valign="middle">
<ptxt>7 (+DP) - 3 (+DP)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Spiral cable is turned 2.5 rotations counterclockwise</ptxt>
</entry>
<entry morerows="5" valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Spiral cable is centered</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Spiral cable is turned 2.5 rotations clockwise</ptxt>
</entry>
</row>
<row>
<entry morerows="2" valign="middle">
<ptxt>3 (GND1) - 10 (GND1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Spiral cable is turned 2.5 rotations counterclockwise</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Spiral cable is centered</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Spiral cable is turned 2.5 rotations clockwise</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<atten2>
<ptxt>The spiral cable is an important part of the SRS airbag system. Incorrect removal or installation of the spiral cable may prevent the airbag from deploying. Be sure to read the Precaution in the SRS section (See page <xref label="Seep01" href="RM000000KT10J3X"/>).</ptxt>
</atten2>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Steering Pad Switch Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0012" fin="false">OK</down>
<right ref="RM000003XLF01TX_04_0010" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0012" proc-id="RM23G0E___0000CZT00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SPIRAL CABLE - BODY GROUND)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G40 spiral cable sub-assembly connector.</ptxt>
<figure>
<graphic graphicname="E100093E03" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G40-10 (GND1) - Body ground</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Spiral Cable Sub-assembly)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0005" fin="false">OK</down>
<right ref="RM000003XLF01TX_04_0013" fin="false">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0005" proc-id="RM23G0E___0000CZS00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SPIRAL CABLE - PARKING ASSIST ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G40 spiral cable sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G40-3 (+DP) - I3-23 (BLSW)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0006" fin="true">A</down>
<right ref="RM000003XLF01TX_04_0016" fin="true">B</right>
<right ref="RM000003XLF01TX_04_0026" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0013" proc-id="RM23G0E___0000CZU00001">
<testtitle>CHECK HARNESS AND CONNECTOR (SPIRAL CABLE - DRIVING SUPPORT SWITCH CONTROL ECU)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the G40 spiral cable sub-assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G76 driving support switch control ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>G40-10 (GND1) - G76-4 (SWI2)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0014" fin="true">OK</down>
<right ref="RM000003XLF01TX_04_0031" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0002" proc-id="RM23G0E___0000CZQ00001">
<testtitle>CHECK POWER MIRROR CONTROL SYSTEM (POWER MIRROR RETRACT FUNCTION)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Operate the mirror retract switch to check if the power mirror retract function of the outer rear view mirror assembly (passenger side) operates normally (See page <xref label="Seep01" href="RM0000030W9022X"/>).</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.01in"/>
<colspec colname="COL2" colwidth="1.12in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Power mirror retract function is normal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Power mirror retract function is abnormal</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0017" fin="false">A</down>
<right ref="RM000003XLF01TX_04_0008" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0017" proc-id="RM23G0E___0000CZV00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARKING ASSIST ECU - OUTER REAR VIEW MIRROR)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the G11 outer mirror switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>I3-10 (MPOS) - G11-14 (MF)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry>
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry>
<ptxt>NG</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0006" fin="true">A</down>
<right ref="RM000003XLF01TX_04_0016" fin="true">B</right>
<right ref="RM000003XLF01TX_04_0026" fin="true">C</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0018" proc-id="RM23G0E___0000CZW00001">
<testtitle>CHECK PARKING ASSIST ECU</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
<figure>
<graphic graphicname="E185668E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</test1>
<test1>
<ptxt>Measure the voltage according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Voltage</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch ON, shift lever in R</ptxt>
</entry>
<entry valign="middle">
<ptxt>11 to 14 V</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle" align="left">
<ptxt>Ignition switch ON, shift lever in any position except R</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 V</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>OK (for LHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>OK (for RHD)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Automatic Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>C</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>NG (for Manual Transmission)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>D</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front view of wire harness connector</ptxt>
<ptxt>(to Parking Assist ECU)</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0006" fin="true">A</down>
<right ref="RM000003XLF01TX_04_0016" fin="true">B</right>
<right ref="RM000003XLF01TX_04_0021" fin="false">C</right>
<right ref="RM000003XLF01TX_04_0024" fin="false">D</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0021" proc-id="RM23G0E___0000CZX00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARKING ASSIST ECU - PARK/NEUTRAL POSITION SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C40 park/neutral position switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - C40-1 (RL)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0011" fin="true">OK</down>
<right ref="RM000003XLF01TX_04_0032" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0024" proc-id="RM23G0E___0000CZY00001">
<testtitle>CHECK HARNESS AND CONNECTOR (PARKING ASSIST ECU - BACK-UP LIGHT SWITCH)</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Disconnect the I3 parking assist ECU connector.</ptxt>
</test1>
<test1>
<ptxt>Disconnect the C41 back-up light switch assembly connector.</ptxt>
</test1>
<test1>
<ptxt>Measure the resistance according to the value(s) in the table below.</ptxt>
<spec>
<title>Standard Resistance</title>
<table>
<tgroup cols="3" align="center">
<colspec colname="COL1" colwidth="1.38in"/>
<colspec colname="COL2" colwidth="1.38in"/>
<colspec colname="COL3" colwidth="1.37in"/>
<thead>
<row>
<entry valign="middle">
<ptxt>Tester Connection</ptxt>
</entry>
<entry valign="middle">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle">
<ptxt>Specified Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - C41-1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>Below 1 Ω</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>I3-15 (REV) - Body ground</ptxt>
</entry>
<entry valign="middle">
<ptxt>Always</ptxt>
</entry>
<entry valign="middle">
<ptxt>10 kΩ or higher</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</spec>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0027" fin="true">OK</down>
<right ref="RM000003XLF01TX_04_0033" fin="true">NG</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0025" proc-id="RM23G0E___0000CZZ00001">
<testtitle>CHECK REAR VIEW MONITOR SYSTEM</testtitle>
<content6 releasenbr="1">
<test1>
<ptxt>Check if the side monitor system or wide view front monitor system has the problem.</ptxt>
<table>
<title>Result</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.07in"/>
<colspec colname="COL2" colwidth="2.06in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Result</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Proceed to</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Side monitor system has problem</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>A</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Wide view front monitor system has problem</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>B</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</test1>
</content6>
<res>
<down ref="RM000003XLF01TX_04_0028" fin="true">A</down>
<right ref="RM000003XLF01TX_04_0029" fin="true">B</right>
</res>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0007">
<testtitle>CHECK FOR DTC<xref label="Seep01" href="RM000003Y8H00OX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0009">
<testtitle>REPLACE STEERING PAD SWITCH ASSEMBLY<xref label="Seep01" href="RM0000039SA01CX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0010">
<testtitle>REPLACE SPIRAL CABLE SUB-ASSEMBLY<xref label="Seep01" href="RM000002O8X01NX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0006">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM00000466X01EX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0016">
<testtitle>REPLACE PARKING ASSIST ECU<xref label="Seep01" href="RM00000470500GX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0026">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0014">
<testtitle>REPLACE DRIVING SUPPORT SWITCH CONTROL ECU</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0031">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0008">
<testtitle>GO TO POWER MIRROR CONTROL SYSTEM<xref label="Seep01" href="RM0000030W9022X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0011">
<testtitle>REPLACE PARK/NEUTRAL POSITION SWITCH ASSEMBLY<xref label="Seep01" href="RM0000010NC06FX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0032">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0027">
<testtitle>REPLACE BACK-UP LIGHT SWITCH ASSEMBLY<xref label="Seep01" href="RM0000047AR002X"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0033">
<testtitle>REPAIR OR REPLACE HARNESS OR CONNECTOR</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0028">
<testtitle>GO TO SIDE MONITOR SYSTEM<xref label="Seep01" href="RM000003XLW01SX"/>
</testtitle>
</testgrp>
<testgrp id="RM000003XLF01TX_04_0029">
<testtitle>GO TO WIDE VIEW FRONT MONITOR SYSTEM<xref label="Seep01" href="RM000003EAY00JX"/>
</testtitle>
</testgrp>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>