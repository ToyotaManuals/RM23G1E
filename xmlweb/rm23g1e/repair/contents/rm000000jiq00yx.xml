<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="RM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12013_S0010" variety="S0010">
<name>1KD-FTV STARTING</name>
<ttl id="12013_S0010_7B936_T00CU" variety="T00CU">
<name>GLOW PLUG (w/o DPF)</name>
<para id="RM000000JIQ00YX" category="A" type-id="30014" name-id="ST6MS-01" from="201210">
<name>INSTALLATION</name>
<subpara id="RM000000JIQ00YX_01" type-id="01" category="01">
<s-1 id="RM000000JIQ00YX_01_0079" proc-id="RM23G0E___000072B00001">
<ptxt>INSTALL GLOW PLUG</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>Measure the resistance of the glow plug when reinstalling it. If the result is not as specified, replace the glow plug with a new one.</ptxt>
</item>
<item>
<ptxt>Replace the glow plug with a new one when it has been dropped or subjected to a physical impact.</ptxt>
</item>
<item>
<ptxt>Remove any carbon deposits from the glow plug hole when reinstalling the glow plug.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Clean the glow plug hole.</ptxt>
<s3>
<ptxt>Wrap tape around a drill bit with a diameter of 4.4 mm (0.173 in.), 118.5 mm (4.665 in.) from its tip.</ptxt>
</s3>
<s3>
<ptxt>Insert the drill bit 118.5 mm (4.665 in.) into the glow plug hole (up to the tape) and remove any carbon deposits by turning the drill bit by hand.</ptxt>
</s3>
<s3>
<ptxt>Insert a drill bit with a diameter of 4 mm (0.157 in.) into the glow plug hole and remove any carbon deposits from the end of the glow plug hole by turning the drill bit by hand.</ptxt>
</s3>
<s3>
<ptxt>Using a 12 mm deep socket wrench, install the glow plug assembly.</ptxt>
<torque>
<torqueitem>
<t-value1>18</t-value1>
<t-value2>178</t-value2>
<t-value4>13</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not use any tools, such as air tools, which are liable to cause an impact to the glow plugs when installing them.</ptxt>
</atten3>
</s3>
</s2>
<s2>
<ptxt>Install the 4 glow plugs.</ptxt>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>133</t-value2>
<t-value4>10</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Do not use any tools, such as air tools, which are liable to cause an impact to the glow plugs when installing them.</ptxt>
</atten3>
</s2>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0113">
<ptxt>INSTALL NO. 1 INTAKE MANIFOLD INSULATOR</ptxt>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0070" proc-id="RM23G0E___000072A00001">
<ptxt>INSTALL NO. 1 GLOW PLUG CONNECTOR</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the No. 1 glow plug connector with the 4 nuts.</ptxt>
</s2>
<s2>
<ptxt>Tighten the 4 nuts.</ptxt>
<torque>
<torqueitem>
<t-value1>2.2</t-value1>
<t-value2>22</t-value2>
<t-value3>19</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the 4 screw grommets.</ptxt>
</s2>
<s2>
<ptxt>Connect the wire harness to the No. 1 glow plug connector with the nut.</ptxt>
<torque>
<torqueitem>
<t-value1>2.6</t-value1>
<t-value2>27</t-value2>
<t-value3>23</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the screw grommet.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0090" proc-id="RM23G0E___000072C00001">
<ptxt>INSTALL NO. 2 MANIFOLD STAY</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the No. 2 manifold stay with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0098" proc-id="RM23G0E___00006B200001">
<ptxt>INSTALL INTAKE PIPE STAY
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the intake pipe stay with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>23</t-value1>
<t-value2>235</t-value2>
<t-value4>17</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000JIQ00YX_01_0092" proc-id="RM23G0E___00004BN00001">
<ptxt>INSTALL INTAKE AIR CONNECTOR WITH DIESEL THROTTLE BODY ASSEMBLY (w/o EGR System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Set a new gasket on the intake manifold.</ptxt>
<figure>
<graphic graphicname="A222611" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<ptxt>Make sure the claw of the gasket face the intake manifold as shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Install the intake air connector with diesel throttle body with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A230168E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Tighten the bolts in the order shown in the illustration.</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Connect the throttle position sensor connector.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0093" proc-id="RM23G0E___00004BH00001">
<ptxt>INSTALL AIR CONNECTOR STAY (w/o EGR System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Temporarily install the air connector stay with the 3 bolts.</ptxt>
<figure>
<graphic graphicname="A222614E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>Tighten the bolt labeled A.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 2 bolts labeled B.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0104" proc-id="RM23G0E___00004BO00001">
<ptxt>CONNECT ENGINE WIRE (w/o EGR System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>for LHD:</ptxt>
<ptxt>Connect the engine wire with the clamp and install the 2 bolts.</ptxt>
<torque>
<subtitle>for bolt A</subtitle>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>131</t-value2>
<t-value4>9</t-value4>
</torqueitem>
<subtitle>for bolt B</subtitle>
<torqueitem>
<t-value1>22</t-value1>
<t-value2>220</t-value2>
<t-value4>16</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A225769E01" width="2.775699831in" height="1.771723296in"/>
</figure>
</s2>
<s2>
<ptxt>for RHD:</ptxt>
<ptxt>Connect the engine wire with the bolt.</ptxt>
<figure>
<graphic graphicname="A225770" width="2.775699831in" height="1.771723296in"/>
</figure>
<torque>
<torqueitem>
<t-value1>13</t-value1>
<t-value2>131</t-value2>
<t-value4>9</t-value4>
</torqueitem>
</torque>
</s2>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0099" proc-id="RM23G0E___00005JN00001">
<ptxt>INSTALL NO. 1 NO. 2 AND NO. 3 INJECTION PIPE SUB-ASSEMBLY (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<atten3>
<list1 type="unordered">
<item>
<ptxt>When replacing an injector, it is necessary to replace the 4 injection pipes with new ones.</ptxt>
</item>
<item>
<ptxt>Keep the joints of the injection pipe clean.</ptxt>
</item>
</list1>
</atten3>
<s2>
<ptxt>Temporarily install the No. 1, No. 2 and No. 3 injection pipes with the union nuts.</ptxt>
</s2>
<s2>
<ptxt>Install the No. 2 and No. 3 injection pipe clamps with the 2 bolts and 2 nuts as shown in the illustration.</ptxt>
<torque>
<torqueitem>
<t-value1>5.0</t-value1>
<t-value2>51</t-value2>
<t-value3>44</t-value3>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A223288E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>No. 2 Injection Pipe</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>If the painted mark on the No. 2 injection pipe has disappeared, use the illustration as a reference to install the clamps.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, tighten the injection pipe union nuts on the common rail side.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
<figure>
<graphic graphicname="A224501E01" width="2.775699831in" height="3.779676365in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Common Rail Side</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Injector Side</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep01" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
<s2>
<ptxt>Using a 17 mm union nut wrench, tighten the injection pipe union nuts on the injector side.</ptxt>
<torque>
<torqueitem>
<t-value1>35</t-value1>
<t-value2>357</t-value2>
<t-value4>26</t-value4>
</torqueitem>
</torque>
<atten3>
<ptxt>Use the formula to calculate special torque values for situations where a union nut wrench is combined with a torque wrench (See page <xref label="Seep02" href="RM000003YMD00GX"/>).</ptxt>
</atten3>
</s2>
</content1></s-1>
<s-1 id="RM000000JIQ00YX_01_0096" proc-id="RM23G0E___00004BI00001">
<ptxt>INSTALL THROTTLE BODY BRACKET (w/o EGR System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the throttle body bracket with the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Install the gas filter with gas filter bracket with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the vacuum hose.</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0109" proc-id="RM23G0E___00001B100000">
<ptxt>INSTALL NO. 1 INTAKE PIPE (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 1 intake pipe with the air hose and install the 2 bolts.</ptxt>
<torque>
<torqueitem>
<t-value1>20</t-value1>
<t-value2>204</t-value2>
<t-value4>15</t-value4>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the 2 clamps of the No. 1 air hose.</ptxt>
<torque>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Tighten the clamp of the intercooler air hose.</ptxt>
<torque>
<torqueitem>
<t-value1>6.5</t-value1>
<t-value2>66</t-value2>
<t-value3>58</t-value3>
</torqueitem>
</torque>
</s2>
<s2>
<ptxt>Connect the vacuum hose to the manifold absolute pressure sensor connector.</ptxt>
</s2>
<s2>
<ptxt>Connect the 3 connectors to the intake air temperature sensor, throttle control motor and manifold absolute pressure sensor.</ptxt>
</s2>
<s2>
<ptxt>Attach the 2 clamps.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000JIQ00YX_01_0110" proc-id="RM23G0E___00001B200000">
<ptxt>INSTALL NO. 4 VACUUM TRANSMITTING PIPE SUB-ASSEMBLY (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the No. 4 vacuum transmitting pipe with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>8.0</t-value1>
<t-value2>82</t-value2>
<t-value3>71</t-value3>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000JIQ00YX_01_0111" proc-id="RM23G0E___00001B300000">
<ptxt>INSTALL INLET HEATER WATER HOSE (w/o EGR System)
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Connect the inlet heater water hose with the bolt.</ptxt>
<torque>
<torqueitem>
<t-value1>26</t-value1>
<t-value2>260</t-value2>
<t-value4>19</t-value4>
</torqueitem>
</torque>
</s2>
</content1></s-1>
<s-1 id="RM000000JIQ00YX_01_0101" proc-id="RM23G0E___000072D00001">
<ptxt>INSTALL ELECTRIC EGR CONTROL VALVE ASSEMBLY (w/ EGR System)</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Install the electric EGR control valve (See page <xref label="Seep01" href="RM000002RK6033X"/>).</ptxt>
</s2>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0112" proc-id="RM23G0E___000072E00001">
<ptxt>CONNECT CABLE TO NEGATIVE BATTERY TERMINAL</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>When disconnecting the cable, some systems need to be initialized after the cable is reconnected (See page <xref label="Seep01" href="RM000003YMF00MX"/>).</ptxt>
</atten3>
</content1>
</s-1>
<s-1 id="RM000000JIQ00YX_01_0106" proc-id="RM23G0E___00005JG00000">
<ptxt>BLEED AIR FROM FUEL SYSTEM
</ptxt>
<content1 releasenbr="1">
<s2>
<ptxt>Using the hand pump mounted on the fuel filter cap, bleed the air from the fuel system. Continue pumping until the pump resistance increases.</ptxt>
<figure>
<graphic graphicname="A223284" width="2.775699831in" height="1.771723296in"/>
</figure>
<atten3>
<list1 type="unordered">
<item>
<ptxt>The maximum hand pump pumping speed is 2 strokes per second.</ptxt>
</item>
<item>
<ptxt>The hand pump must be pushed with a full stroke during pumping.</ptxt>
</item>
<item>
<ptxt>When the fuel pressure at the supply pump inlet port reaches a saturated pressure, the hand pump resistance increases.</ptxt>
</item>
<item>
<ptxt>If pumping is interrupted during the air bleeding process, fuel in the fuel line may return to the fuel tank. Continue pumping until the hand pump resistance increases.</ptxt>
</item>
<item>
<ptxt>If the hand pump resistance does not increase despite consecutively pumping 200 times or more, there may be a fuel leak between the fuel tank and fuel filter, the hand pump may be malfunctioning, or the vehicle may have run out of fuel.</ptxt>
</item>
<item>
<ptxt>If air bleeding using the hand pump is incomplete, the common rail pressure does not rise to the pressure range necessary for normal use and the engine cannot be started.</ptxt>
</item>
</list1>
</atten3>
</s2>
<s2>
<ptxt>Start the engine.</ptxt>
<atten3>
<list1 type="unordered">
<item>
<ptxt>Even if air bleeding using the hand pump has been completed, the starter may need to be cranked for 10 seconds or more to start the engine.</ptxt>
</item>
<item>
<ptxt>Do not crank the engine continuously for more than 20 seconds. The battery may be discharged.</ptxt>
</item>
<item>
<ptxt>Use a fully-charged battery.</ptxt>
</item>
</list1>
</atten3>
<s3>
<ptxt>When the engine can be started, proceed to the next step.</ptxt>
</s3>
<s3>
<ptxt>If the engine cannot be started, bleed the air again using the hand pump until the hand pump resistance increases (refer to the procedures above). Then start the engine.</ptxt>
</s3>
</s2>
<s2>
<ptxt>Turn the ignition switch off.</ptxt>
</s2>
<s2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Turn the ignition switch on (IG) and turn the intelligent tester on.</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep01" href="RM000000PDK0Z6X"/>).</ptxt>
</s2>
<s2>
<ptxt>Start the engine.*1</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / Active Test / Test the Fuel Leak.*2</ptxt>
<figure>
<graphic graphicname="A194065E05" width="2.775699831in" height="2.775699831in"/>
</figure>
<table>
<title>Text in Illustration</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="0.41in"/>
<colspec colname="COL2" colwidth="3.72in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reference</ptxt>
<ptxt>Active Test Operation</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</s2>
<s2>
<ptxt>Perform the following test 5 times with on/off intervals of 10 seconds: Active Test / Test the Fuel Leak.*3</ptxt>
</s2>
<s2>
<ptxt>Allow the engine to idle for 3 minutes or more after performing the Active Test for the 5th time.</ptxt>
<figure>
<graphic graphicname="A192848E04" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>When the Active Test "Test the Fuel Leak" is used to change the pump control mode, the actual fuel pressure inside the common rail drops below the target fuel pressure when the Active Test is off, but this is normal and does not indicate a pump malfunction.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</s2>
<s2>
<ptxt>Read Current DTCs.</ptxt>
</s2>
<s2>
<ptxt>Clear the DTCs (See page <xref label="Seep02" href="RM000000PDK0Z6X"/>).</ptxt>
<atten4>
<ptxt>It is necessary to clear the DTCs as DTC P1604 or P1605 may be stored when air is bled from the fuel system after replacing or repairing fuel system parts.</ptxt>
</atten4>
</s2>
<s2>
<ptxt>Repeat steps *1 to *3.</ptxt>
</s2>
<s2>
<ptxt>Enter the following menus: Powertrain / Engine and ECT / DTC.</ptxt>
</s2>
<s2>
<ptxt>Read Current DTCs.</ptxt>
<spec>
<title>OK</title>
<specitem>
<ptxt>No DTCs are output.</ptxt>
</specitem>
</spec>
</s2>
</content1></s-1>
<s-1 id="RM000000JIQ00YX_01_0107" proc-id="RM23G0E___00005JF00000">
<ptxt>INSPECT FOR FUEL LEAK
</ptxt>
<content1 releasenbr="1">
<atten2>
<list1 type="unordered">
<item>
<ptxt>During Active Test mode, engine speed becomes high and combustion noise becomes loud, so pay attention.</ptxt>
</item>
<item>
<ptxt>During Active Test mode, fuel becomes highly pressurized. Be extremely careful not to expose your eyes, hands, or body to escaped fuel.</ptxt>
</item>
</list1>
</atten2>
<s2>
<ptxt>Check that there are no leaks from any part of the fuel system when the engine is stopped. If there is fuel leakage, repair or replace parts as necessary.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and check that there are no leaks from any part of the fuel system. If there is fuel leakage, repair or replace parts as necessary.</ptxt>
</s2>
<s2>
<ptxt>Disconnect the return hose from the common rail.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and check for fuel leaks from the return pipe.</ptxt>
<ptxt>If there is fuel leakage, replace the common rail.</ptxt>
</s2>
<s2>
<ptxt>Connect the intelligent tester to the DLC3.</ptxt>
</s2>
<s2>
<ptxt>Start the engine and turn the intelligent tester on.</ptxt>
</s2>
<s2>
<ptxt>Select the Fuel Leak test from the Active Test mode on the intelligent tester.</ptxt>
</s2>
<s2>
<ptxt>If the intelligent tester is not available, fully depress the accelerator pedal quickly. Increase the engine speed to the maximum and maintain that speed for 2 seconds. Repeat this operation several times.</ptxt>
</s2>
<s2>
<ptxt>Check that there are no leaks from any part of the fuel system.</ptxt>
<atten3>
<ptxt>A return pipe leakage of less than 10 cc (0.6 cu in.) per minute is acceptable.</ptxt>
</atten3>
<ptxt>If there is fuel leakage, repair or replace parts as necessary.</ptxt>
</s2>
<s2>
<ptxt>Reconnect the return hose to the common rail.</ptxt>
</s2>
</content1></s-1>
<s-1 id="RM000000JIQ00YX_01_0108" proc-id="RM23G0E___00001CD00000">
<ptxt>INSPECT FOR COOLANT LEAK
</ptxt>
<content1 releasenbr="1">
<atten3>
<ptxt>Before each inspection, turn the A/C switch off.</ptxt>
</atten3>
<atten2>
<ptxt>Do not remove the radiator reservoir cap while the engine and radiator are still hot. Pressurized, hot engine coolant and steam may be released and cause serious burns.</ptxt>
</atten2>
<s2>
<ptxt>Fill the radiator with coolant and attach a radiator cap tester.</ptxt>
</s2>
<s2>
<ptxt>Warm up the engine.</ptxt>
</s2>
<s2>
<ptxt>Using the radiator cap tester, increase the pressure inside the radiator to 123 kPa (1.3 kgf/cm<sup>2</sup>, 18 psi), and check that the pressure does not drop.</ptxt>
<ptxt>If the pressure drops, check the hoses, radiator and water pump for leaks. If no external leaks are found, check the heater core, cylinder block and head.</ptxt>
</s2>
</content1></s-1>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>