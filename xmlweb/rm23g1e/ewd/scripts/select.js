/*
   Copyright (c) 2002 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/


var g_objPopup = null;
var g_objSystemList = null;


function call_runOnLoadProc()
{
	try{

		showPrintButton( (getURLParam("print") == "yes") );


		var subPath = chkGPTitleExist("./system/title-gp.xml");
		var pathPS = chkPSTitleExist("./system/title-ps.xml");
		g_objSystemList = new TitleList("./system/title.xml"
										, TITLE_TYPE_SYSTEM
										, subPath
										, pathPS);

		setGPTitleExist();
		setPSTitleExist();
	}
	catch( e ){
		window.alert(e.stack);
	}
}



function setGPTitleExist()
{
	var fncSetGpFlg = parent.setLoadedGpListFlag;
	if(typeof fncSetGpFlg == "function"){
		fncSetGpFlg(g_objSystemList.objGpList.loaded);
	}
}



function setPSTitleExist()
{
	var fncSetPsFlg = parent.setLoadedPsListFlag;
	if(typeof fncSetPsFlg == "function"){
		if(g_objSystemList.objPSList != null){
			fncSetPsFlg(g_objSystemList.objPSList.loaded);
		}
		else{
			fncSetPsFlg(false);
		}
	}
}



function chkGPTitleExist(path)
{
	var fncGetFlg = parent.getLoadedGpListFlag;
	if(parent.getURLParam("globalnavi") == "no" || (typeof fncGetFlg == "function" && fncGetFlg() == false)){
		return "";	
	}
	return path;
}



function chkPSTitleExist(path)
{
	var fncGetFlg = parent.getLoadedPsListFlag;
	if(parent.getURLParam("globalnavi") == "no" || (typeof fncGetFlg == "function" && fncGetFlg() == false)){
		return "";	
	}
	return path;
}



function call_runOnClickLocation()
{
	try{
		showMenu("location");
	}
	catch( e ){
		window.alert(e.stack);
	}
}

function call_runOnClickDiagram()
{
	try{
		showPopUpObject(window.event.srcElement, document.all("Diagram"));
	}
	catch( e ) {
		window.alert(e.stack);
	}
}

function call_runOnClickList()
{
	try{
		showPopUpObject(window.event.srcElement, document.all("List"));
	}
	catch( e ) {
		window.alert(e.stack);
	}
}

function call_runOnClickIntro()
{
	try{
		showIntro();
	}
	catch( e ){
		window.alert(e.stack);
	}
}


function call_runOnClickPopUpItem(srcObj)
{
	try{
		switch( srcObj.id ){
			case "diag_overall":
				showMenu("overall");
				break;
			
			case "diag_system":
				showMenu("system");
				break;
			
			case "diag_ps":
				showDiagram4PS();
				break;
			
			case "diag_gp":
				showDiagram4GP();
				break;
			
			case "list_ps":
				showPsMatrix();
				break;
			
			case "list_conn":
				showConnList();
				break;
			
			default:
				confirm(srcObj.id);
				break;
		}
		
		hidePopUpObject();
	}
	catch( e ) {
		window.alert(e.stack);
	}
}

function call_runOnMouseOver(obj)
{

	obj.className = "popup-item-hover";
}

function call_runOnMouseOut(obj)
{

	obj.className = "popup-item";
}








function call_runOnClickTop()
{
	try{
		var paramTable = makeParamTable();
		appendRequiredParam(paramTable, top);
		

		top.window.location.href = "../../index.html" + makeParamString(paramTable);
	}
	catch( e ){
		window.alert(e.stack);
	}
}



function call_runOnClickBack()
{
	try{
		var winMain = parent.d_mainframe;
		if(new URL(winMain.src).pathname.search(/intro.index.html$/i) != -1){
			if(new URL(winMain.src).pathname.search(/intro.top.xml$/i) != -1){
				showMenu("system");
			} else {
				showIntro();
			}
		}
		else if(new URL(winMain.src).pathname.search(/howto.index.html$/i) != -1){
			showIntro();
		}

		else if(new URL(winMain.src).pathname.search(/overall.pdf..+.pdf$/i) != -1){
			showMenu("overall");
		}

		else if(winMain.contentWindow.getURLParam("ewd_type") == "system"){
			showMenu("system");
		}

		else if(winMain.contentWindow.getURLParam("ewd_type") == "routing" ||
				winMain.contentWindow.getURLParam("ewd_type") == "relay"){
			showMenu("location");
		}

		else if(winMain.contentWindow.getURLParam("category") == "location" ||
				winMain.contentWindow.getURLParam("category") == "overall"  ||
				winMain.contentWindow.getURLParam("category") == "ps"  ||
				winMain.contentWindow.getURLParam("category") == "gp"){
			showMenu("system");
		}

		else if(winMain.contentWindow.getURLParam("category") == "system"){
			call_runOnClickTop();
		}


		else{
			var paramTable = makeParamTable();
			appendRequiredParam(paramTable, top);
			top.window.location.href = "../index.html" + makeParamString(paramTable);
		}
	}
	catch( e ){
		window.alert(e.stack);
	}
}


function call_runOnClickNew()
{
	try{


		window.open("../../index.html" , "_blank", "width=950, height=620, top=16, left=16, resizable=yes");
	}
	catch( e ){
		window.alert(e.stack);
	}
}


function call_runOnClickPrint()
{
	try{
		showPrintWindow();
	}
	catch( e ){
		window.alert(e.stack);
	}
}






function findPos(obj, foundScrollLeft, foundScrollTop) {
    var curleft = 0;
    var curtop = 0;
    if(obj.offsetLeft) curleft += parseInt(obj.offsetLeft);
    if(obj.offsetTop) curtop += parseInt(obj.offsetTop);
    if(obj.scrollTop && obj.scrollTop > 0) {
        curtop -= parseInt(obj.scrollTop);
        foundScrollTop = true;
    }
    if(obj.scrollLeft && obj.scrollLeft > 0) {
        curleft -= parseInt(obj.scrollLeft);
        foundScrollLeft = true;
    }
    if(obj.offsetParent) {
        var pos = findPos(obj.offsetParent, foundScrollLeft, foundScrollTop);
        curleft += pos[0];
        curtop += pos[1];
    } else if(obj.ownerDocument) {
        var thewindow = obj.ownerDocument.defaultView;
        if(!thewindow && obj.ownerDocument.parentWindow)
            thewindow = obj.ownerDocument.parentWindow;
        if(thewindow) {
            if (!foundScrollTop && thewindow.scrollY && thewindow.scrollY > 0) curtop -= parseInt(thewindow.scrollY);
            if (!foundScrollLeft && thewindow.scrollX && thewindow.scrollX > 0) curleft -= parseInt(thewindow.scrollX);
            if(thewindow.frameElement) {
                var pos = findPos(thewindow.frameElement);
                curleft += pos[0];
                curtop += pos[1];
            }
        }
    }
    return [curleft,curtop];
}


function createPopUpObject()
{

if(!window.createPopup){
    window.createPopup = function (){
		var popup = window.top.document.createElement('dialog');
		window.top.document.body.appendChild(popup);
		popup.Id = "popupDialog";
		popup.innerHTML = '<a href="#" id="popup-close" style="position: absolute; top: 0; right: 5sp; font-size: 20sp; color: #000; text-decoration: none; outline: none;">&times;</a><div id="popup-body"></div>';
		window.top.document.getElementById('popup-close').addEventListener('click', function(e) {
			e.preventDefault();
			popup.hide();
		});
		popup.document = window.top.document.getElementById('popup-body');
		popup.document.contentWindow = window;

		var isShown = false;

        popup.hide = function (){
            isShown = false;
			popup.close();
			window.top.document.body.removeChild(popup);
        }

        popup.showCustom = function (x, y, w, h, pElement){
            if(typeof(x) !== 'undefined'){
                var elPos = [0, 0];
                if(pElement) elPos = findPos(pElement);//maybe validate that this is a DOM node instead of just falsy
                elPos[0] += y, elPos[1] += x;

                if(isNaN(w)) w = popup.document.scrollWidth;
                if(isNaN(h)) h = popup.document.scrollHeight;
                if(elPos[0] + parseInt(w) > window.top.document.body.clientWidth) elPos[0] = window.top.document.body.clientWidth - parseInt(w) - 5;
                if(elPos[1] + parseInt(h) > window.top.document.body.clientHeight) elPos[1] = window.top.document.body.clientHeight - parseInt(h) - 5;

				popup.style.position = "absolute";
                popup.style.left = elPos[0] + "px";
                popup.style.top = elPos[1] + "px";
                popup.style.width = w + "px";
                popup.style.height = h + "px";
            }
			popup.style.right = "auto";
			popup.style.bottom = "auto";
            popup.style.display = "block";
			popup.showModal();
            isShown = true;
        }

		var popupClicked = false;
        var hidepopup = function (event){
            if(isShown)
                setTimeout(function (){
                    if(!popupClicked){
                        popup.hide();
                    }
                    popupClicked = false;
                }, 150);//timeout will allow the click event to trigger inside the frame before closing.
        }
        window.addEventListener('click', hidepopup, true);
        window.addEventListener('blur', hidepopup, true);
        return popup;
    }
}


	g_objPopup = window.createPopup();

if(typeof g_objPopup.document.createStyleSheet === 'undefined') {
    g_objPopup.document.createStyleSheet = (function() {
        function createStyleSheet(href) {
            if(typeof href !== 'undefined') {
                var element = g_objPopup.ownerDocument.createElement('link');
                element.type = 'text/css';
                element.rel = 'stylesheet';
                element.href = href;
            }
            else {
                var element = g_objPopup.ownerDocument.createElement('style');
                element.type = 'text/css';
            }

            g_objPopup.ownerDocument.getElementsByTagName('head')[0].appendChild(element);

			var style = g_objPopup.ownerDocument.createElement("style");
			g_objPopup.ownerDocument.head.appendChild(style); // must append before you can access sheet property
			var sheet = style.sheet;

            if(typeof sheet.addRule === 'undefined')
                sheet.addRule = addRule;

            if(typeof sheet.removeRule === 'undefined')
                sheet.removeRule = sheet.deleteRule;

            return sheet;
        }

        function addRule(selectorText, cssText, index) {
            if(typeof index === 'undefined')
                index = this.cssRules.length;

            this.insertRule(selectorText + ' {' + cssText + '}', index);
        }

        return createStyleSheet;
    })();
}

	g_objPopup.document.createStyleSheet("styles/popup.css");
}

function showPopUpObject(objSrc, objDisp)
{
	createPopUpObject();

	g_objPopup.document.innerHTML = objDisp.innerHTML;
	
	var objTags = g_objPopup.document.getElementsByTagName("a");


	var width  = document.all("val-popup-width").value;
	var height = document.all("val-popup-height").value * objTags.length;
	
	g_objPopup.showCustom(5, 15, width, height, objSrc);
}

function hidePopUpObject()
{
	g_objPopup.hide();
}

function showIntro()
{
	parent.d_mainframe.src = window.top.location.pathname.replace(/[^/]*$/, '') + "intro/index.html";
}


function showMenu( category )
{
	var paramTable = makeParamTable();
	appendRequiredParam(paramTable, top);
	paramTable.put("category", category);
	if( parent.d_mainframe == null ) return;
	parent.d_mainframe.src = window.top.location.pathname.replace(/[^/]*$/, '') + "contents/menu/category_frame.html" + makeParamString(paramTable);
}

function showDiagram( titleType, titleCode )
{
	var paramTable = makeParamTable();
	appendRequiredParam(paramTable, top);
	paramTable.put("ewd", titleCode);
	paramTable.put("ewd_type", titleType);
	parent.d_mainframe.src = window.top.location.pathname.replace(/[^/]*$/, '') + "contents/common/fig_frame.html" + makeParamString(paramTable);
}

function showPsMatrix()
{
	var paramTable = makeParamTable();
	appendRequiredParam(paramTable, top);
	paramTable.put("loads","ps");
	
	parent.d_mainframe.src = window.top.location.pathname.replace(/[^/]*$/, '') + "contents/loads/matrix.html" + makeParamString(paramTable);
}


function showConnList()
{
	var strWait = "Please Wait...";
	
	switch(g_objSystemList.getLang()){
		case "de":	strWait = "Bitte warten.";	break;
		case "es":	strWait = "Espere, por favor."; break;
		case "fr":	strWait = "Prière d'attendre."; break;
	}
	
	var strHTML = "<html><head><link rel='stylesheet' type='text/css' href='styles/contents.css'/></head>";
	strHTML += "<body>";
	strHTML += "<div id='d_msg' name='d_msgWait'>\n<h5 style='margin: 50px 30px;'>\n"+ strWait + "\n</h5>\n</div>\n";
	strHTML += "</body></html>";

	parent.d_mainframe.contentWindow.document.write(strHTML);
	window.setTimeout("showConnListMain()", 1);

}

function showConnListMain()
{
	var paramTable = makeParamTable();
	appendRequiredParam(paramTable, top);
	
	parent.d_mainframe.src = window.top.location.pathname.replace(/[^/]*$/, '') + "contents/connector/parts.html" + makeParamString(paramTable);
}




function showDiagram4PS()
{

	if( g_objSystemList.objPSList.loaded ){
		showMenu("ps");
	}
	else{
		var PS_FIG = "PS";
		var titleObj = g_objSystemList.getTitleObjByBaseFig(PS_FIG);
		
		showDiagram("system", titleObj.getCode());
	}
}


function showDiagram4GP()
{
	if( g_objSystemList.objGpList.loaded ){
		showMenu("gp");
	}
	else{
		showDiagram("system", "GP");
	}
}





function showPrintButton( flgShow )
{
	if( flgShow ){
		document.getElementById("printBtn").style.visibility = "visible";
	} else {
		document.getElementById("printBtn").style.visibility = "hidden";
	}
}
