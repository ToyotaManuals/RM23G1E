/*
   Copyright (c) 2002 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/

async function openDialog(targetWindow, url, arg, opt) {
    window.showModalDialog = window.showModalDialog || function(url, arg, opt) {
        url = url || ''; //URL of a dialog
        arg = arg || null; //arguments to a dialog
        opt = opt || 'dialogWidth:300px;dialogHeight:200px'; //options: dialogTop;dialogLeft;dialogWidth;dialogHeight or CSS styles
		
		var styleConverted = opt.replace(/dialog/gi, '');

		var existingDialog = window.top.document.getElementsByTagName('dialog')[0];
		if (existingDialog != null)
		{
			var dialog = existingDialog;
			dialog.style.cssText = styleConverted;
			window.top.document.getElementById('dialog-body').dialogArguments = arg;
			window.top.document.getElementById('dialog-body').src = url;
			return new Promise(function(resolve, reject) {
				dialog.addEventListener('close', function() {
					var returnValue = window.top.document.getElementById('dialog-body').returnValue;
					resolve(returnValue);
				});
			});
		}
		else
		{
			var dialog = window.top.document.body.appendChild(window.top.document.createElement('dialog'));
			var styleConverted = opt.replace(/dialog/gi, '');
			dialog.innerHTML = '<a href="#" id="dialog-close" style="position: absolute; top: 0; right: 5sp; font-size: 20sp; color: #000; text-decoration: none; outline: none;">&times;</a><iframe id="dialog-body" src="' + url + '" style="border: 0; width: 100%; height: 100%;"></iframe>';
			dialog.style.cssText = styleConverted;
			window.top.document.getElementById('dialog-body').dialogArguments = arg;
			window.top.document.getElementById('dialog-close').addEventListener('click', function(e) {
				e.preventDefault();
				dialog.close();
			});
			dialog.showModal();
			return new Promise(function(resolve, reject) {
				dialog.addEventListener('close', function() {
					var returnValue = window.top.document.getElementById('dialog-body').returnValue;
					window.top.document.body.removeChild(dialog);
					resolve(returnValue);
				});
			});
		}
    };

	var ret = await window.showModalDialog(url, arg, opt);
	return ret;
}


function openNewEWD( destURL, paramTable )
{
	paramTable.put("globalnavi","no");
	paramTable.put("localnavi","no");
	paramTable.put("imode","ewd");
	
	destURL += makeParamString(paramTable);
	
	var winStyle = makeWinStyleString();
	
	window.open( destURL , "newEwd" , winStyle);
}



function makeWinStyleString()
{

	var height = parent.d_map.contentWindow.document.body.clientHeight;
	var width = parent.d_map.contentWindow.document.body.clientWidth;
	var winY = window.screen.availHeight - height - 50;
	var winX = window.screen.availWidth - width - 20;
	
	var winStyle = "";
	winStyle += "height:" + height + ",width:" + width;
	winStyle += ",top=" + winY + ",left=" + winX;
	winStyle += ",status:no,resizable:yes,scrollbars:yes";
	
	return winStyle;
}


function openCompCheck(strParam)
{

	openWindowToCenter("../../../repair/html/isp_toc/ewd_frame.html" + strParam, true, 700, 600 );
}


async function openRepairInfo(strParam, lang)
{
	var width = 910;

	if( lang == "en" )	width = 800;
	else if( lang == "de" ) width = 1080;

	var args = new Array();
	args[0] = "print";
	args[1] = "repair_wire";
	args[2] = strParam;

	var path = "contents/common/dialog.html"
	var style = "dialogWidth:" + width + "px; dialogHeight:320px; dialogTop:50px; resizable=yes; help=no;";
	await openDialog( window.top, path, args, style );


}



async function openTermInfo(strId, connNo)
{
	var args = new Array();
	args[0] = "print";
	args[1] = "terminal_info";
	args[2] = "?terminalId=" + strId + "&connNo=" + connNo;

	var path = "contents/common/dialog.html"
	var style = "dialogWidth:900px; dialogHeight:480px; dialogTop:250px; resizable=yes; help=no;";
	await openDialog( window.top, path, args, style );


}


function openWindowToCenter(url, blnResize, nWidth, nHeight )
{
	var nTop  = (window.screen.availHeight - nHeight) / 2;
	var nLeft = (window.screen.availWidth  - nWidth)  / 2;
	
	var strWinStyle = "width:" + nWidth + ",height:" + nHeight + ",top:" + nTop + ",left:" + nLeft
	if( blnResize ){
		strWinStyle = strWinStyle + ",resizable=yes";
	}
	
	window.open( url, "_blank", strWinStyle );
}


async function openNewDialog( targetWindow, destURL,paramTable,width,height)
{
	var sFeatures = "dialogWidth:" + width +"px; dialogHeight:" + height + "px; status:yes; resizable:yes; help:no;"

	paramTable.put("globalnavi","no");
	paramTable.put("localnavi","no");
	paramTable.put("imode","ewd");

	window.open(destURL + makeParamString(paramTable));
}



async function openNewPrintDialog( name, bodyHTML )
{
	var args = new Array();
	args[0] = "print";
	args[1] = name;
	args[2] = bodyHTML;
	
	var path = "contents/common/print.html"
	var style = "dialogWidth:700px; dialogHeight:620px; resizable=yes; help=no;";
	await openDialog( window.top, path, args, style );
}


async function openNewPrintDialog2(name, arg)
{
	var args = new Array();
	args[0] = "print";
	args[1] = name;
	args[2] = arg;
	
	var path = "contents/common/print.html"
	var style = "dialogWidth:700px; dialogHeight:620px; resizable=yes; help=no;";
	await openDialog( window.top, path, args, style );
}



async function openOutlineDialog( systemCode )
{
	var args = new Array();
	args[0] = "print";
	args[1] = "outline";
	args[2] = systemCode;
	
	var path = "contents/common/print.html"
	var style = "dialogWidth:700px; dialogHeight:620px; resizable=yes; help=no;";
	await openDialog( window.top, path, args, style );
}



async function openReadMeDialog()
{
	var args = new Array();
	args[0] = "print";
	args[1] = "readme";
	
	var path = "contents/common/print.html"
	var style = "dialogWidth:800px; dialogHeight:620px; resizable=yes; help=no;";
	await openDialog( window.top, path, args, style );
}
