/*
   Copyright (c) 2002-2007 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/

function call_runOnLoadProc()
{
	try{
		switch( window.top.document.getElementById('dialog-body').dialogArguments[1] ){

			case "repair_wire":
				self.d_preview.src = self.d_preview.src.replace(/[^/]*$/, '') + "../connector/repair.html" + window.top.document.getElementById('dialog-body').dialogArguments[2];
				break;


			case "terminal_info":
				self.d_preview.src = self.d_preview.src.replace(/[^/]*$/, '') + "../connector/terminfo.html" + window.top.document.getElementById('dialog-body').dialogArguments[2];
				break;
		}
	}
	catch(e){
		window.alert(e.stack);
	}

}

function getType()
{
	return window.top.document.getElementById('dialog-body').dialogArguments[1]
}

function getArgs()
{
	return window.top.document.getElementById('dialog-body').dialogArguments[2];
}


