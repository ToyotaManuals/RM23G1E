/*
   Copyright (c) 2002 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/


function call_runOnClickClose()
{
	try{
		if (window.top.document.getElementsByTagName('dialog').length == 0){
      parent.window.close();
    } else {
      window.top.document.getElementsByTagName('dialog')[0].close();
    }

	}
	catch( e ){
		window.alert(e.stack);
	}
}
