


function call_runOnLoadProc()
{
	try{


		if( isPrintDialog() ) {
			switch( window.top.document.getElementById('dialog-body').dialogArguments[1] ){
				case "parts":
					self.d_preview.src = window.top.location.pathname.replace(/[^/]*$/, '') + "contents/connector/partsref_print.html";
					break;
					
				case "loads_ps":
					self.d_preview.src = window.top.location.pathname.replace(/[^/]*$/, '') + "contents/loads/matrix_print.html";
					break;
					
				case "intro":
					self.d_preview.src = window.top.location.pathname.replace(/[^/]*$/, '') + "intro/print.html";
					break;
				
				case "outline":
					var path = "contents/system/outline/" + window.top.document.getElementById('dialog-body').dialogArguments[2] + ".xml";
					self.d_preview.src = window.top.location.pathname.replace(/[^/]*$/, '') + path;
					break;
				
				case "readme":
					self.d_preview.src = window.top.location.pathname.replace(/[^/]*$/, '') + "intro/readme.html";
					break;

			}
		}
	}
	catch(e){
		window.alert(e.stack);
	}
}



function isPrintDialog()
{
	if( window.top.document.getElementById('dialog-body') != null ) {
		if( window.top.document.getElementById('dialog-body').dialogArguments[0] == "print" )
			return true;
		else
			return false;
	}
}
