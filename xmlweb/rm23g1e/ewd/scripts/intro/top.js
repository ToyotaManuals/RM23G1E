


function call_runOnMouseOver()
{
	try{
		window.event.srcElement.className = "titlelist_hover";
	}
	catch(e){
		window.alert(e.stack);
	}
}

function call_runOnMouseOut()
{
	try{
		window.event.srcElement.className = "titlelist";
	}
	catch(e){
		window.alert(e.stack);
	}
}

function call_runOnClickContents(code)
{
	var name = window.event.srcElement.innerHTML;

	
	if( code == "help" ){

		parent.window.location.href = "../howto/index.html";
	}
	else if( code == "wh-help" ){
		parent.window.location.href = "../intro-wh/howto/index.html";
	}
	else{
		parent.showLeftFrame(true);
		
		parent.d_header.contentWindow.updateHTML(name);
		parent.d_menu.contentWindow.updateHTML(name);
		parent.d_submenu.contentWindow.updateHTML(code);
		
		self.window.location.href = code + ".xml";
	}
}
