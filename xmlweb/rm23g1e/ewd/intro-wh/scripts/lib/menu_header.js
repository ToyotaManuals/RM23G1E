/*
   Copyright (c) 2002-2007 SHINTEC HOZUMI Co.,LTD.
   All Rights Reserved. 
*/


function call_runOnClickClose()
{
	try{
		window.top.document.getElementsByTagName('dialog')[0].close();
	}
	catch( e ){
		window.alert(e.stack);
	}
}



function call_runOnClickTop()
{
	try{
		parent.d_preview.src = parent.d_preview.src.replace(/[^/]*$/, '') + "top.html";
	}
	catch( e ){
		window.alert(e.stack);
	}
}

