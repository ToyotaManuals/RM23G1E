<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="NM23G1E">
<servcat id="56">
<name>Audio / Visual / Telematics</name>
<section id="12044_S001L" variety="S001L">
<name>PARK ASSIST / MONITORING</name>
<ttl id="12044_S001L_7BDCJ_T0021" variety="T0021">
<name>MULTI-TERRAIN MONITOR SYSTEM</name>
<para id="RM0000047W4008X" category="F" type-id="8048W" name-id="PM37P-01" from="201207">
<name>GENERAL</name>
<subpara id="RM0000047W4008X_z0" proc-id="NM23G0E___000005M00000">
<content5 releasenbr="1">
<step1>
<ptxt>OUTLINE</ptxt>
<step2>
<ptxt>In the multi-terrain select, the multi-terrain monitor system uses television camera assemblies located on the front grille, outer rear view mirror assembly and back door*1 or spare tire back door carrier*2 and displays images on the multi display*3 or accessory meter assembly*4 to allow the driver to see blind spots around the vehicle, thus assisting the driver's vision.</ptxt>
</step2>
<step2>
<ptxt>A switch to turn the multi-terrain monitor system on and off and to switch the screen display is located on the steering wheel.</ptxt>
</step2>
<step2>
<ptxt>The system consists of the television camera assemblies, multi display assembly*3, accessory meter assembly*4, steering pad switch and parking assist ECU.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: Models without back door spare tire carrier</ptxt>
</item>
<item>
<ptxt>*2: Models with back door spare tire carrier</ptxt>
</item>
<item>
<ptxt>*3: Models with navigation system</ptxt>
</item>
<item>
<ptxt>*4: Models without navigation system</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>PRECAUTION</ptxt>
<step2>
<ptxt>In the following cases, it may become difficult to see items in the image on the screen, but this does not indicate a malfunction.</ptxt>
<step3>
<ptxt>In the dark (for example, at night)</ptxt>
</step3>
<step3>
<ptxt>When the temperature near the lens is very high or low</ptxt>
</step3>
<step3>
<ptxt>When water droplets are present on the television camera, or when humidity is high (for example, when it rains)</ptxt>
</step3>
<step3>
<ptxt>When foreign matter (for example, mud or salt) is present on the television camera lens</ptxt>
</step3>
<step3>
<ptxt>When sunlight or headlights are shining directly into the television camera lens</ptxt>
</step3>
</step2>
<step2>
<ptxt>If a bright light (for example, sunlight reflected off the vehicle body) is picked up by the television camera, the smear effect*, a phenomenon experienced by a CCD camera, may occur.</ptxt>
<atten4>
<ptxt>*: A phenomenon that occurs when a bright light is received by the television camera and transmitted to the screen. The light source will appear to have a vertical streak above and below it.</ptxt>
<figure>
<graphic graphicname="Y104525E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*a</ptxt>
</entry>
<entry>
<ptxt>Vertical Streak</ptxt>
</entry>
<entry align="center">
<ptxt>*b</ptxt>
</entry>
<entry>
<ptxt>Bright Light</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*c</ptxt>
</entry>
<entry>
<ptxt>Front View Mode</ptxt>
</entry>
<entry align="center">
<ptxt>*d</ptxt>
</entry>
<entry>
<ptxt>Rear View Mode</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</atten4>
</step2>
<step2>
<ptxt>Initialization Function During Part Replacement</ptxt>
<step3>
<ptxt>The initialization function must always be performed whenever one of the conditions indicated in the table below occurs.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<colspec colname="COL3" colwidth="3.18in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Parts Name</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Initialization Item</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Television Camera Assemblies</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Camera position shifting</ptxt>
</item>
<item>
<ptxt>Removal and reassembly</ptxt>
</item>
<item>
<ptxt>Replacement</ptxt>
</item>
<item>
<ptxt>Deformation due to the mount area having been struck</ptxt>
</item>
</list1>
</entry>
<entry valign="middle">
<ptxt>Camera position adjustment*</ptxt>
<list1 type="unordered">
<item>
<ptxt>Roll angle, pan angle, tilt angle</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Parking Assist ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Replacement</ptxt>
</entry>
<entry valign="middle">
<ptxt>Camera position adjustment*</ptxt>
<list1 type="unordered">
<item>
<ptxt>Roll angle, pan angle, tilt angle</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: Models with multi display assembly</ptxt>
</item>
</list1>
</step3>
<step3>
<ptxt>The initialization function described above is performed on the service menu screen.</ptxt>
</step3>
<step3>
<ptxt>The method for displaying the service menu screen is the same as that for the multi display. For details, refer to the corresponding Repair Manual for this model.</ptxt>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>