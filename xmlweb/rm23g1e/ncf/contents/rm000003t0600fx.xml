<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="NM23G1E">
<servcat id="52">
<name>Drivetrain</name>
<section id="12019_S0012" variety="S0012">
<name>R150F MANUAL TRANSMISSION / TRANSAXLE</name>
<ttl id="12019_S0012_7BDBP_T0017" variety="T0017">
<name>MANUAL TRANSMISSION SYSTEM</name>
<para id="RM000003T0600FX" category="F" type-id="8048Z" name-id="MT0OT-05" from="201207">
<name>DETAILS</name>
<subpara id="RM000003T0600FX_z0" proc-id="NM23G0E___000003B00000">
<content5 releasenbr="1">
<step1>
<ptxt>CONSTRUCTION</ptxt>
<step2>
<ptxt>Gear Train</ptxt>
<step3>
<ptxt>The transmission synchromesh mechanism is an all-speed constant mesh type.</ptxt>
<figure>
<graphic graphicname="X102221E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.57in"/>
<colspec colname="COL2" colwidth="2.97in"/>
<colspec colname="COL3" colwidth="0.57in"/>
<colspec colname="COL4" colwidth="2.97in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Input Shaft</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>4th Gear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>3rd Gear</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>2nd Gear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reverse Gear</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>1st Gear</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>5th Gear</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Output Shaft</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>Counter Shaft</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Single-cone Type Synchromesh Mechanism</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*11</ptxt>
</entry>
<entry valign="middle">
<ptxt>Triple-cone Type Synchromesh Mechanism</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*12</ptxt>
</entry>
<entry valign="middle">
<ptxt>Reverse Synchromesh Mechanism</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<table pgwide="1">
<title>Synchromesh Mechanism</title>
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="1.42in"/>
<colspec colname="COL4" colwidth="1.42in"/>
<colspec colname="COL5" colwidth="1.4in"/>
<tbody>
<row>
<entry valign="middle">
<ptxt>Gear</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>1st</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>2nd</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>3rd, 4th and 5th</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Reverse</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Synchromesh Mechanism Type</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Single-cone</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Triple-cone</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Single-cone</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Reverse</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Reverse Synchromesh Mechanism</ptxt>
<step3>
<ptxt>The reverse synchromesh mechanism consists of 2 shifting keys, a shifting key spring, and a shifting key return spring.</ptxt>
<figure>
<graphic graphicname="X102222E01" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Counter 5th Gear</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hub Sleeve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shifting Key</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shifting Key Spring</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Shifting Key Return Spring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Snap Ring</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>OPERATION</ptxt>
<step2>
<ptxt>Reverse Synchromesh Mechanism</ptxt>
<step3>
<ptxt>While shifting into 5th, the 2 shifting keys move in unison with the hub sleeve (F1). With "A" acting as the fulcrum, the 2 shifting keys move to push the synchronizer ring only for the amount of the lever ratio (F2). The shift effort can be reduced only by this amount.</ptxt>
<figure>
<graphic graphicname="X102223E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
<step3>
<ptxt>While shifting into reverse, with "B" acting as the fulcrum, the 2 shifting keys move to push the synchronizer ring (F2). As a result, a brake is applied to the rotation of the counter shaft and gear noise is prevented.</ptxt>
<figure>
<graphic graphicname="X102224E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>