<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="NM23G1E">
<servcat id="V1">
<name>General</name>
<section id="12001_S0001" variety="S0001">
<name>INTRODUCTION</name>
<ttl id="12001_S0001_7BDAJ_T0001" variety="T0001">
<name>FEATURES</name>
<para id="RM00000405700HX" category="F" type-id="80493" name-id="IN0IP-12" from="201207">
<name>EQUIPMENT</name>
<subpara id="RM00000405700HX_z0" proc-id="NM23G0E___000000400000">
<content5 releasenbr="1">
<step1>
<ptxt>OUTLINE</ptxt>
<step2>
<ptxt>Entry and Start System</ptxt>
<step3>
<ptxt>The entry and start system mainly consists of the wireless door lock control function, engine immobiliser function, start function and entry function.</ptxt>
</step3>
<step3>
<ptxt>The start function allows the power source mode to be switched between off, on (ACC), and on (IG). The power source can be switched by pushing the engine switch while the key is being carried by the driver. In addition, when the key is carried by the driver, the driver can start the engine by pushing the engine switch with the brake pedal*1 or clutch pedal*2 depressed.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: Models with automatic transmission</ptxt>
</item>
<item>
<ptxt>*2: Models with manual transmission</ptxt>
</item>
</list1>
</step3>
<step3>
<ptxt>For the entry function, the following functions can be operated when the driver is carrying the key:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The doors unlock if the outside door handle is gripped or by pressing the back door lock switch (Entry Unlock Function).</ptxt>
</item>
<item>
<ptxt>All doors are locked by touching the lock sensor of the front outside door handle or by pressing the back door lock switch (Entry Lock Function).</ptxt>
</item>
</list1>
</step3>
</step2>
<step2>
<ptxt>Combination Meter Assembly</ptxt>
<step3>
<ptxt>An Optitron display type combination meter assembly or an analog display type combination meter assembly is used.</ptxt>
</step3>
<step3>
<ptxt>The Optitron display type combination meter assembly achieves excellent visibility through the use of smoke acrylic in the protective panel, and bright Light Emitting Diodes (LEDs) that have high contrast to illuminate the indicator and the dial.</ptxt>
<figure>
<graphic graphicname="Y104284" width="7.106578999in" height="2.775699831in"/>
</figure>
<figure>
<graphic graphicname="Y104285" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>TOYOTA Parking Assist-sensor System</ptxt>
<step3>
<ptxt>The TOYOTA parking assist-sensor system uses ultrasonic sensors and is equipped with a beeping alarm. It can detect obstacles at the rear of the vehicle, and provide information to the driver regarding the distance.</ptxt>
</step3>
<step3>
<ptxt>The TOYOTA parking assist-sensor system informs the driver of the approximate distance between the sensors and the obstacles, as well as their positions, by displaying them on the multi-information display in the combination meter assembly, the multi display assembly*1 or the accessory meter assembly*2, and by sounding a buzzer.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: Models with navigation system</ptxt>
</item>
<item>
<ptxt>*2: Models without navigation system</ptxt>
</item>
</list1>
</step3>
</step2>
<step2>
<ptxt>Parking Assist Monitor System</ptxt>
<step3>
<ptxt>The rear parking assist monitor system consists of the rear monitor and parking guide lines to assist the driver in parking the vehicle.</ptxt>
</step3>
<step3>
<ptxt>The rear parking assist monitor system has a rear television camera assembly mounted on the back door*1 or the back door spare tire carrier*2 to display a view of the area behind the vehicle on the multi display assembly.</ptxt>
</step3>
<step3>
<ptxt>This system consists of the rear television camera assembly, parking assist ECU, and multi display.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: Models without back door spare tire carrier</ptxt>
</item>
<item>
<ptxt>*2: Models with back door spare tire carrier</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="Y104534E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Manual Assist</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Parallel Parking Assist</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Monitor System</ptxt>
<step3>
<ptxt>To assist the driver in parking the vehicle by monitoring the area behind it, the rear monitor system has a rear television camera assembly mounted on the back door*1 or back door spare tire carrier*2.</ptxt>
</step3>
<step3>
<ptxt>This system consists of the rear television camera assembly, multi display assembly*3, and accessory meter assembly*4.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: Models without back door spare tire carrier</ptxt>
</item>
<item>
<ptxt>*2: Models with back door spare tire carrier</ptxt>
</item>
<item>
<ptxt>*3: Models with navigation system</ptxt>
</item>
<item>
<ptxt>*4: Models without navigation system</ptxt>
</item>
</list1>
</step3>
</step2>
<step2>
<ptxt>Wide View Front and Side Monitor System</ptxt>
<step3>
<ptxt>The wide view front and side monitor system uses television camera assemblies located on the front grille and passenger-side outer rear view mirror and displays images on the multi display assembly*1 or accessory meter assembly*2 to allow the driver to see blind spots in the front and passenger side areas, thus assisting the driver's vision.</ptxt>
</step3>
<step3>
<ptxt>The screen display mode can be switched between manual display mode and automatic display mode, in consideration of usability.</ptxt>
</step3>
<step3>
<ptxt>A round monitor main switch has been placed on the steering wheel for switching the screen display and switching the wide view front and side monitor system on and off.</ptxt>
</step3>
<step3>
<ptxt>The system consists of the front television camera assembly, side television camera assembly, multi display assembly*1, accessory meter assembly*2 and parking assist ECU.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: Models with navigation system</ptxt>
</item>
<item>
<ptxt>*2: Models without navigation system</ptxt>
</item>
</list1>
</step3>
</step2>
<step2>
<ptxt>Multi-terrain Monitor System</ptxt>
<step3>
<ptxt>In the multi-terrain select, the multi-terrain monitor system uses television camera assemblies located on the front grille, outer rear view mirror assembly and back door*1 or back door spare tire carrier*2 and displays images on the multi display assembly*3 or accessory meter assembly*4 to allow the driver to see blind spots around the vehicle, thus assisting the driver's vision.</ptxt>
</step3>
<step3>
<ptxt>A switch to turn the multi-terrain monitor system on and off and switch the screen display is located on the steering wheel.</ptxt>
</step3>
<step3>
<ptxt>The system consists of the television camera assemblies, multi display assembly*3, accessory meter assembly*4, steering pad switch and parking assist ECU.</ptxt>
<list1 type="nonmark">
<item>
<ptxt>*1: Models without back door spare tire carrier</ptxt>
</item>
<item>
<ptxt>*2: Models with back door spare tire carrier</ptxt>
</item>
<item>
<ptxt>*3: Models with navigation system</ptxt>
</item>
<item>
<ptxt>*4: Models without navigation system</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="Y104822E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>Models without Back Door Spare Tire Carrier</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>Models with Back Door Spare Tire Carrier</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Television Camera Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side Television Camera Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Television Camera Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front View Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear View Mode</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side Front Simultaneous View Mode</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*d</ptxt>
</entry>
<entry valign="middle">
<ptxt>Side Rear Simultaneous View Mode</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Rear Seat Entertainment System (RSES)</ptxt>
<step3>
<ptxt>The RSES displays images from the DVD player or an auxiliary player on the television display assembly. The RSES also outputs sound via the speakers or headphones.</ptxt>
</step3>
<step3>
<ptxt>The RSES television display assembly is provided on roof headlining, thus enabling each rear seat occupant to view the television image while looking straight forward.</ptxt>
</step3>
<step3>
<ptxt>A pair of 9.0-inch Video Graphics Array (VGA) monitor is used for the television system. In addition, Light Emitting Diodes (LEDs) are used to provide mercury-free backlighting. This reduces the burden on the environment and ensures brightness.</ptxt>
</step3>
<step3>
<ptxt>DVD and VIDEO buttons are provided separately on the remote controller, thus enhancing usability.</ptxt>
</step3>
<step3>
<ptxt>A menu screen exclusive to the RSES is provided, thus enhancing operability of each content selection.</ptxt>
<figure>
<graphic graphicname="Y104506E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Television Display Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Remote Controller</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Seat</ptxt>
<step3>
<ptxt>Seat Variation</ptxt>
<list1 type="unordered">
<item>
<ptxt>An 8-way power seat, a 6-way manual seat, a 4-way power seat and a 4-way manual seat are provided for the front seat.</ptxt>
</item>
<item>
<ptxt>A bench type front passenger seat is used on RHD 10-passenger models for Africa and Papua New Guinea as an option.</ptxt>
</item>
<item>
<ptxt>A bench type front passenger seat is used on LHD 10-passenger models for Africa as an option.</ptxt>
</item>
<item>
<ptxt>A 60/40 split tumble seat is provided for the rear No. 1 seat on 3-door models.</ptxt>
</item>
<item>
<ptxt>A 60/40 split double-folding seat is provided for the rear No. 1 seat on 5-passenger models, 5-door models and 10-passenger models.</ptxt>
</item>
<item>
<ptxt>A 60/40 split slide walk-in seat is provided for the rear No. 1 seat on 7-passenger models.</ptxt>
</item>
<item>
<ptxt>A double-flat seat (manual/power) is provided for the rear No. 2 seat on 7-passenger models.</ptxt>
</item>
<item>
<ptxt>Face to face seat is provided for the rear No. 2 seat on 9-passenger models and 10-passenger models.</ptxt>
</item>
</list1>
</step3>
<step3>
<ptxt>Front Power Seat</ptxt>
<list1 type="unordered">
<item>
<ptxt>A power seat control system is used for the driver seat and front passenger seat.</ptxt>
</item>
<item>
<ptxt>A memory function is used in the power seat control system for the driver seat.</ptxt>
</item>
</list1>
</step3>
<step3>
<ptxt>Rear No. 2 Power Seat with Folding Function</ptxt>
<list1 type="unordered">
<item>
<ptxt>A power seat control system is used for rear No. 2 seat.</ptxt>
</item>
<item>
<ptxt>The rear No. 2 seat is stowed automatically by the folding function.</ptxt>
</item>
</list1>
</step3>
<step3>
<ptxt>Seat Cover</ptxt>
<ptxt>Genuine leather and 2 types of fabric are provided for the seat cover.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Glass Hatch Opener System</ptxt>
<list1 type="unordered">
<item>
<ptxt>The glass hatch is opened by pressing a button on the models without back door spare tire carrier.</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="Y104785" width="7.106578999in" height="3.779676365in"/>
</figure>
</step2>
<step2>
<ptxt>Exterior Color and Interior Color</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COLSPEC2" colwidth="1.77in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Exterior</ptxt>
</entry>
<entry namest="COL3" nameend="COLSPEC2" valign="middle" align="center">
<ptxt>Interior</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Color No.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Color Name</ptxt>
</entry>
<entry namest="COL3" nameend="COLSPEC2" valign="middle" align="center">
<ptxt>Color Name</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>040</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Super White II</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>070</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>White Pearl Crystal Shine</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1H2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Dark Steel Mica</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1F7</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Silver Metallic</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>1G3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Gray Metallic</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>202</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Black</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3R0</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Blackish Red Mica</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4T8</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Beige Metallic</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6V2</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Green Mica Metallic</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>8R3</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Grayish Blue Metallic</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Ivory</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Medium Gray</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>