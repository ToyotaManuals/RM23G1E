<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="NM23G1E">
<servcat id="53">
<name>Suspension</name>
<section id="12024_S0018" variety="S0018">
<name>SUSPENSION CONTROL</name>
<ttl id="12024_S0018_7BDBY_T001G" variety="T001G">
<name>ADAPTIVE VARIABLE SUSPENSION SYSTEM</name>
<para id="RM0000045OP007X" category="F" type-id="8048Z" name-id="SC1H8-01" from="201207">
<name>DETAILS</name>
<subpara id="RM0000045OP007X_z0" proc-id="NM23G0E___000003Y00000">
<content5 releasenbr="1">
<step1>
<ptxt>FUNCTION OF MAIN COMPONENTS</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.48in"/>
<colspec colname="COL2" colwidth="4.60in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Absorber Control Actuator</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes the damping force of the shock absorbers.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Shock Absorber Assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>Changes the oil flow passage by rotating the rotary valve, and switches the damping force.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front and Rear Acceleration Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the vertical acceleration rate of the body.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering Angle Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the steering direction and the angle of the steering wheel.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Yaw Rate Sensor Assembly</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Detects the vehicle's yaw rate.</ptxt>
</item>
<item>
<ptxt>Detects the vehicle's longitudinal and lateral acceleration and deceleration.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Absorber Control Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switches the damping force control mode.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Suspension Control ECU</ptxt>
<ptxt>(with Built-in Front Acceleration Sensor LH)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Estimates the condition of the vehicle in accordance with the signals provided by the sensors and switches, and outputs control signals to the absorber control actuators.</ptxt>
</item>
<item>
<ptxt>Detects the vertical acceleration rate of the body.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Skid Control ECU</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Sends the vehicle speed signal to the suspension control ECU.</ptxt>
</item>
<item>
<ptxt>Sends the brake pedal depressing signal to the suspension control ECU.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>4WD Control ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sends the transfer drive mode to the suspension control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Driving Support ECU Assembly*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Makes a request for damping force control to the suspension control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Power Management Control ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sends the engine switch condition to the suspension control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>ECM</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sends the drive torque signal to the suspension control ECU.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: Models with dynamic radar cruise control system</ptxt>
</item>
</list1>
</step1>
<step1>
<ptxt>SYSTEM CONTROL</ptxt>
<step2>
<ptxt>The AVS effects the following controls:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.48in"/>
<colspec colname="COL2" colwidth="4.60in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Repercussion Control (H∞ Control)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Smoothly changes the damping force to a target value in accordance with the changes in the road surface or driving conditions. In this way, excellent ride comfort is achieved while ensuring a high level of vibration damping performance.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Roughness Sensing Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>When the road surface condition does not require a damping force, this function controls the shock absorbers so that their damping force will not increase.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Unsprung Damping Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>If unsprung resonance is detected, this function controls so that the damping force will not decrease below a certain level, in order to reduce the unsprung resonance.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Roll Posture Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>Regulates the damping force to reduce the phase difference between the vehicle roll and pitch angles during steering, thus providing smooth superior maneuverability.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Anti-dive Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>During braking, this function makes the damping force firmer to restrain the body dive, thus ensuring excellent stability and controllability.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Anti-squat Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>During acceleration, this function makes the damping force firmer to minimize the changes in the vehicle body posture.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle Speed Sensing Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>This function varies the variable range of the damping force in accordance with vehicle speed in order to achieve a soft and comfortable ride and a stable driving condition. The damping force is controlled at a softer variable range at low speeds, and at a firmer variable range at high speeds.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Damping Mode Selection</ptxt>
</entry>
<entry valign="middle">
<ptxt>The absorber control switch enables the driver to select a desired damping force from the 3 modes.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pre-crash Safety System Operation Control*</ptxt>
</entry>
<entry valign="middle">
<ptxt>Switches damping force to the hard side in accordance with damping force control request signals from the driving support ECU assembly, and suppresses dive during braking.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>L4 Range Control</ptxt>
</entry>
<entry valign="middle">
<ptxt>The suspension control ECU controls damping force to a level that is optimal for driving on rough roads in the low speed range and for normal driving in the intermediate speed range, when the transfer drive mode is in L4F or L4L. This ensures ride comfort and driveability in off-road driving.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: Models with dynamic radar cruise control system</ptxt>
</item>
</list1>
</step2>
<step2>
<ptxt>The suspension control ECU receives signals from the sensors and switches to control the absorber control actuators. It uses these signals to optimally control the damping force in accordance with the driving conditions and road conditions.</ptxt>
<figure>
<graphic graphicname="X102050E01" width="7.106578999in" height="7.795582503in"/>
</figure>
</step2>
</step1>
<step1>
<ptxt>CONSTRUCTION</ptxt>
<step2>
<ptxt>Shock Absorber Assembly and Absorber Control Actuator</ptxt>
<step3>
<ptxt>The front shock absorber assembly is equipped with an external actuator.</ptxt>
</step3>
<step3>
<ptxt>The rear shock absorber assembly is equipped with an internal actuator.</ptxt>
</step3>
<step3>
<ptxt>The piston rod is equipped with a rotary valve, a soft damping force valve and a hard damping force valve.</ptxt>
<figure>
<graphic graphicname="X102051E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration (Front Shock Absorber)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Absorber Control Actuator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actuator Bracket</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Front Shock Absorber Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Soft Damping Force Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hard Damping Force Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rotary Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<figure>
<graphic graphicname="X102052E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration (Rear Shock Absorber)</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rear Shock Absorber Assembly </ptxt>
<ptxt>- Absorber Control Actuator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Soft Damping Force Valve</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Hard Damping Force Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rotary Valve</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>An absorber control actuator changes the damping force in accordance with the signals received from the suspension control ECU.</ptxt>
</step3>
<step3>
<ptxt>A 16-step step motor turns the rotary valve in the shock absorber assembly to vary the opening of the oil passage. This enables the damping force to be changed smoothly.</ptxt>
<figure>
<graphic graphicname="X102053E01" width="7.106578999in" height="7.795582503in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Front Acceleration Sensor</ptxt>
<step3>
<ptxt>The front acceleration sensors detect the vertical movement of the body.</ptxt>
</step3>
<step3>
<ptxt>The front acceleration sensors are placed on the right and left caul sides. The acceleration sensors independently detect the vertical acceleration rate.</ptxt>
<figure>
<graphic graphicname="X102054E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
</step2>
</step1>
<step1>
<ptxt>FAIL-SAFE</ptxt>
<step2>
<ptxt>If a malfunction occurs in the AVS, the suspension control ECU prohibits the damping force control.</ptxt>
</step2>
</step1>
<step1>
<ptxt>DIAGNOSIS</ptxt>
<step2>
<ptxt>The suspension control ECU will also store a Diagnostic Trouble Code (DTC). The DTC can be accessed through the use of an intelligent tester II. For details, refer to the corresponding Repair Manual for this model.</ptxt>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>