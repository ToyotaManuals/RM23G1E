<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="NM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12056_S001R" variety="S001R">
<name>SUPPLEMENTAL RESTRAINT SYSTEMS</name>
<ttl id="12056_S001R_7BDCU_T002C" variety="T002C">
<name>AIRBAG SYSTEM</name>
<para id="RM0000045MN008X" category="F" type-id="8048Z" name-id="RS92R-01" from="201207">
<name>DETAILS</name>
<subpara id="RM0000045MN008X_z0" proc-id="NM23G0E___000006Q00000">
<content5 releasenbr="1">
<step1>
<ptxt>FUNCTION OF MAIN COMPONENTS</ptxt>
<step2>
<ptxt>The main components in the airbag system have the following functions:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="2.83in"/>
<colspec colname="COL2" colwidth="4.25in"/>
<thead>
<row>
<entry align="center">
<ptxt>Component</ptxt>
</entry>
<entry align="center">
<ptxt>Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Center Airbag Sensor Assembly</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor assembly receives signals from the deceleration sensor and safing sensor built into the center airbag sensor assembly and one of the front airbag sensors, determines whether the steering pad (driver side squib), front passenger airbag, driver knee airbag and front seat outer belt assembly (pretensioner squib) should be activated, and diagnoses system malfunctions.</ptxt>
</item>
<item>
<ptxt>The center airbag sensor assembly receives signals from the deceleration sensor and the safing sensor built into the center airbag sensor assembly, the side airbag sensor assembly, rear airbag sensor, rear floor side airbag sensor, determines whether the front seat side airbag, the curtain shield airbag and front seat outer belt assembly (pretensioner squib) should be activated, and diagnoses system malfunctions.*1</ptxt>
</item>
<item>
<ptxt>The center airbag sensor assembly sends the airbag deployment signal to the ECM through the Controller Area Network (CAN) to operate the fuel pump control.*2</ptxt>
</item>
<item>
<ptxt>The center airbag sensor assembly sends the airbag deployment signal to the main body ECU to operate the collision door lock release function.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Steering Pad (Driver Side Squib)</ptxt>
</entry>
<entry morerows="4" valign="middle">
<ptxt>These consist of an inflator, a bag, and so on. The inflator deploys gas in accordance with an expansion signal from the center airbag sensor assembly and deploys the bag.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front Passenger Airbag Assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Driver Knee Airbag*3</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front Seat Side Airbag Assembly*1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Curtain Shield Airbag Assembly*1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Front Airbag Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>The front airbag sensor uses an electrical type deceleration sensor. Based on the deceleration of the vehicle during a frontal collision, a distortion is created in the sensor and converted into an electrical signal. Accordingly, the extent of the initial collision can be detected in detail.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Side Airbag Sensor Assembly*1</ptxt>
</entry>
<entry morerows="1" valign="middle">
<ptxt>A deceleration sensor is enclosed in the side airbag sensor and rear airbag sensor. Based on the deceleration of the vehicle during a side or rear side collision, a distortion is created in the sensor and converted into an electrical signal.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear Airbag Sensor*1</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Rear Floor Side Airbag Sensor *1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Based on the deceleration of the vehicle during a rear side collision, a distortion is created in the sensor and converted into an electrical signal. This sensor functions as a safing sensor to detect the deployment of the SRS curtain shield airbag.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Airbag Cut Off Switch Cylinder*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>The deployment of the front passenger airbag assembly the time of collisions can be turned on and off as required.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Passenger Airbag Off Indicator Light*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates when the airbag cut off switch cylinder is off.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>SRS Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>The SRS warning light turns on to alert the driver when the center airbag sensor assembly detects a malfunction in the airbag system.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*1: Models with SRS side and curtain shield airbag</ptxt>
</item>
<item>
<ptxt>*2: Models with gasoline engine</ptxt>
</item>
<item>
<ptxt>*3: Models with SRS knee airbag</ptxt>
</item>
<item>
<ptxt>*4: Models with airbag cut off switch cylinder</ptxt>
</item>
</list1>
</step2>
</step1>
<step1>
<ptxt>OPERATING CONDITION</ptxt>
<step2>
<ptxt>If the impact of a collision is greater than the specified value, the SRS is activated automatically. The center airbag sensor assembly includes the safing sensor and deceleration sensor. The safing sensor is designed to turn on at a lower deceleration rate than the deceleration sensor.</ptxt>
<step3>
<ptxt>In case of a frontal collision, the center airbag sensor assembly determines whether airbag deployment or pretensioner activation is necessary based on signals from the deceleration sensor and the front airbag sensor. If the safing sensor turns on simultaneously, current flows to the squibs to deploy the SRS components as shown in the illustration below. However, a deployment ignition signal may be output with the deceleration sensor on signal even without a signal from the front airbag sensor.</ptxt>
<figure>
<graphic graphicname="Y104191E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</step3>
<step3>
<ptxt>In case of a side collision, the center airbag sensor assembly determines whether airbag deployment is necessary based on signals from the side airbag sensor assembly. If the safing sensor turn on simultaneously with the side airbag sensor assembly, current flows to the squibs to deploy the SRS as shown in the illustration below:</ptxt>
<figure>
<graphic graphicname="Y104192E01" width="7.106578999in" height="3.779676365in"/>
</figure>
</step3>
<step3>
<ptxt>In case of a rear side collision, the center airbag sensor assembly determines whether airbag deployment is necessary based on signals from the rear airbag sensor. If the safing sensor and rear floor side airbag sensor turn on simultaneously with the rear airbag sensor, current flows to the squib to deploy the SRS as shown in the illustration below.</ptxt>
</step3>
<step3>
<ptxt>If the front seat side airbag assembly deploys, the curtain shield airbag assembly will also deploy, regardless of whether the signal is output from the rear airbag sensor.</ptxt>
<figure>
<graphic graphicname="Y104193E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</step3>
</step2>
</step1>
<step1>
<ptxt>FUNCTION</ptxt>
<step2>
<ptxt>Airbag Cut Off Switch Cylinder</ptxt>
<step3>
<ptxt>A cylinder to disable the deployment of the front passenger airbag assembly has been provided. This cylinder can be used if the deployment of those airbags is dangerous, such as when the passenger is wearing a Child Restraint System (CRS) seat belt.</ptxt>
</step3>
<step3>
<ptxt>This cylinder is located on the side surface of the front passenger side of the instrument panel. It is switched on or off using a mechanical key. When the airbag cut off switch cylinder is off, the passenger airbag off indicator light will illuminate. At this time, the front passenger airbag assembly will be disabled regardless of the collision conditions.</ptxt>
<figure>
<graphic graphicname="Y104199E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Passenger Airbag Off Indicator Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Airbag Cut Off Switch Cylinder</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Mechanical Key</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>CONSTRUCTION</ptxt>
<step2>
<ptxt>Driver Airbag Assembly</ptxt>
<step3>
<ptxt>The inflator and bag are built into the steering pad.</ptxt>
<figure>
<graphic graphicname="Y104197E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Inflator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bag</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>A - A Cross Section</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Front Passenger Airbag Assembly</ptxt>
<step3>
<ptxt>The front passenger airbag assembly is located in the upper section of the instrument panel of the front passenger seat.</ptxt>
</step3>
<step3>
<ptxt>The front passenger airbag assembly consists of an inflator and a bag.</ptxt>
<figure>
<graphic graphicname="Y104712E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Inflator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Driver Knee Airbag Assembly</ptxt>
<step3>
<ptxt>The driver knee airbag assembly consists of an airbag door, an inflator, and a bag.</ptxt>
<figure>
<graphic graphicname="Y104198E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Inflator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bag</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Airbag Door</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>A - A Cross Section</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Front Seat Side Airbag Assembly</ptxt>
<step3>
<ptxt>Front seat side airbag assemblies are installed in the seat backs of the driver seat and the front passenger seat. Each front seat side airbag assembly is a one-piece design, consisting of an inflator and a bag.</ptxt>
<figure>
<graphic graphicname="Y104715E01" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bag</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Inflator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>A - A Cross Section</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Curtain Shield Airbag Assembly</ptxt>
<step3>
<ptxt>Curtain shield airbag assemblies are installed in the area close to the sides of the headliner. Each SRS curtain shield airbag assembly is a one-piece design, consisting of an inflator and a bag.</ptxt>
<figure>
<graphic graphicname="Y104711E01" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*A</ptxt>
</entry>
<entry valign="middle">
<ptxt>3-door Models</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*B</ptxt>
</entry>
<entry valign="middle">
<ptxt>5-door, 5-passenger Seat Models</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*C</ptxt>
</entry>
<entry valign="middle">
<ptxt>5-door, 7-passenger Seat Models</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Inflator</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Bag</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>D - D Cross Section</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>E - E Cross Section</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>F - F Cross Section</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>SYSTEM CONTROL</ptxt>
<step2>
<ptxt>Airbag for Frontal Collision</ptxt>
<step3>
<ptxt>There are 3 types of airbags for frontal collisions: steering pad, front passenger, and knee airbags. These airbags deploy simultaneously.</ptxt>
<figure>
<graphic graphicname="Y103483E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Airbag for Side/Rear Side Collision</ptxt>
<step3>
<ptxt>The airbag for side collisions contains 2 types of airbags: front seat side and curtain shield airbags. These airbags deploy simultaneously. The airbag for rear side collisions contains only the curtain shield airbags.</ptxt>
<list1 type="unordered">
<item>
<ptxt>With the airbag for side collisions, if the side airbag sensor assembly detects an impact, it informs the center airbag sensor assembly, which causes the front seat side, curtain shield airbag and front seat outer belt assembly (pretensioner squib) to be deployed simultaneously.</ptxt>
</item>
<item>
<ptxt>With the rear airbag for side collisions, if the rear airbag sensor and rear floor side airbag sensor detect an impact, they inform the center airbag sensor assembly, which causes the curtain shield airbag to be deployed.</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="Y105247E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<figure>
<graphic graphicname="Y104194E01" width="7.106578999in" height="5.787629434in"/>
</figure>
</step3>
</step2>
</step1>
<step1>
<ptxt>DIAGNOSIS</ptxt>
<step2>
<ptxt>Summary</ptxt>
<step3>
<ptxt>If the center airbag sensor assembly detects a malfunction in the SRS airbag system, the center airbag sensor assembly stores the malfunction data in memory, in addition to illuminating the SRS warning light.</ptxt>
<list1 type="unordered">
<item>
<ptxt>The center airbag sensor assembly outputs the malfunction data as 5-digit Diagnostic Trouble Codes (DTCs) to an intelligent tester II or the SRS warning light.</ptxt>
</item>
</list1>
<figure>
<graphic graphicname="Y101206E01" width="7.106578999in" height="1.771723296in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>SRS Airbag System DTCs</ptxt>
<step3>
<ptxt>The 5-digit DTCs can be read after connecting an intelligent tester II to the DLC3.</ptxt>
</step3>
<step3>
<ptxt>If the SRS airbag deploys, the center airbag sensor assembly will turn on the SRS warning light. However, in contrast to the ordinary diagnosis function, a DTC will not be stored. The SRS warning light cannot be turned off. It is necessary to replace the center airbag sensor assembly with a new one.</ptxt>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>