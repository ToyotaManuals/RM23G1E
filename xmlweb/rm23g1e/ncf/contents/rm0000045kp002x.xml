<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="NM23G1E">
<servcat id="51">
<name>Engine / Hybrid System</name>
<section id="12010_S000I" variety="S000I">
<name>1KD-FTV INTAKE / EXHAUST</name>
<ttl id="12010_S000I_7BDB0_T000I" variety="T000I">
<name>INTAKE SYSTEM</name>
<para id="RM0000045KP002X" category="F" type-id="8048Z" name-id="IE0PM-01" from="201207">
<name>DETAILS</name>
<subpara id="RM0000045KP002X_z0" proc-id="NM23G0E___000001K00000">
<content5 releasenbr="1">
<step1>
<ptxt>CONSTRUCTION</ptxt>
<step2>
<ptxt>Air Cleaner Assembly</ptxt>
<step3>
<ptxt>A nonwoven, full-fabric type air cleaner filter element is used.</ptxt>
</step3>
<step3>
<ptxt>A cyclone pre-cleaner has been provided in the air cleaner inlet. This cyclone pre-cleaner has a screw shape and gathers large-sized dust particles in a dust cup by swirling the inlet air.</ptxt>
<figure>
<graphic graphicname="W102337E02" width="7.106578999in" height="3.779676365in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Cleaner Cap Sub-assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Cleaner Filter Element Sub-assembly</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Air Cleaner Case Sub-assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cyclone Pre-cleaner</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbinated Blade</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Dust Cup</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Swirl Air Flow</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Diesel Throttle Body Assembly</ptxt>
<step3>
<ptxt>A diesel throttle valve equipped with a rotary solenoid type throttle control motor is used to improve EGR performance and to reduce the vibration and noise when stopping the engine. The rotary solenoid type throttle control motor makes the diesel throttle valve respond quickly.</ptxt>
<figure>
<graphic graphicname="W102358E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diesel Throttle Body Assembly</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle Position Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Diesel Throttle Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Throttle Control Motor</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Intake Manifold</ptxt>
<step3>
<ptxt>An intake manifold provided with an air intake chamber is used in order to reduce the swirl variances between the cylinders. </ptxt>
</step3>
<step3>
<ptxt>A vacuum-actuated swirl control valve is provided in one of the two intake ports provided for each cylinder. The swirl control valve consists of a stainless steel shaft and an actuator, which are integrated in the valve. The swirl control valve with 3-stage control is used.</ptxt>
<figure>
<graphic graphicname="W102359E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Intake Manifold</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Actuator</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Swirl Control Valve</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Turbocharger Sub-assembly</ptxt>
<step3>
<ptxt>A variable nozzle vane type turbocharger is used. A water jacket is provided in the bearing housing to improve the cooling performance of the turbocharger.</ptxt>
</step3>
<step3>
<ptxt>This turbocharger has achieved great improvements in low-speed torque, maximum output, fuel consumption, and emission reduction. These improvements have been accomplished through variable control of the nozzle vane position, and an optimal velocity of the exhaust gas inflow to the turbine at all times in response to the engine condition.</ptxt>
</step3>
<step3>
<ptxt>The ECM outputs a signal to the turbo motor driver, which actuates the DC motor, to control the nozzle vane position.</ptxt>
</step3>
<step3>
<ptxt>The variable nozzle vane type turbocharger consists primarily of a compressor wheel, turbine wheel, nozzle vane, unison ring, drive arm, driven arm, DC motor, linkage and nozzle vane position sensor.</ptxt>
<figure>
<graphic graphicname="W102360E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>DC Motor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nozzle Vane Position Sensor</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Water Jacket</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Compressor Wheel</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Turbine Wheel</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nozzle Vane</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unison Ring </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Full-close Stopper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>Linkage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*10</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driven Arm</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*11</ptxt>
</entry>
<entry valign="middle">
<ptxt>Drive Arm</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<list1 type="unordered">
<item>
<ptxt>To control the nozzle vane position, the turbo motor driver renders the contact position of the linkage with the full-close stopper (thus fully closing the nozzle vane) as the zero point for the nozzle vane position sensor.</ptxt>
</item>
<item>
<ptxt>If the turbocharger has been reinstalled or replaced, turn the engine switch from on (IG) to off once, and make sure that the linkage comes in contact with the full-close stopper.</ptxt>
</item>
<item>
<ptxt>The full-close stopper position, which is adjusted at the factory at the time of shipment, is not serviceable in the field. For this reason, if the linkage does not come in contact with the full-close stopper during an inspection, the turbocharger assembly must be replaced. Never attempt to loosen or tighten the locknut of the full-close stopper because it will adversely affect the performance of the engine.</ptxt>
</item>
<item>
<ptxt>For details, refer to the corresponding Repair Manual for this model.</ptxt>
<figure>
<graphic graphicname="W102361E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Linkage</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Full-close Stopper</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>Lock Nut</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Open</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Closed</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</item>
</list1>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>OPERATION</ptxt>
<step2>
<ptxt>Turbocharger</ptxt>
<step3>
<ptxt>The exhaust gas from the exhaust manifold goes through the nozzle vane inside the turbo charger housing, and flows to the exhaust pipe through the turbine. The speed of the turbine (supercharging pressure) differs depending on the flow velocity of the exhaust gas going through the turbine and the flow velocity of the exhaust gas is controlled by the opening. In such a time of idling, when the exhaust gas is less, the nozzle vane is almost fully closed, but as there is a slight clearance between the vanes, the exhaust gas flows through this clearance to the exhaust pipe. Therefore, there is no bypass.</ptxt>
<figure>
<graphic graphicname="W102362E01" width="7.106578999in" height="1.771723296in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nozzle Vane</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<graphic graphicname="V100136" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Intake Air</ptxt>
</entry>
<entry valign="middle" align="center">
<graphic graphicname="V100135" width="0.3937007874015748in" height="0.3937007874015748in"/>
</entry>
<entry valign="middle">
<ptxt>Exhaust Gas</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>When the engine is running in a low speed range, the DC motor presses down the linkage by a signal from the turbo motor driver. The tip of the linkage rotates the unison ring counterclockwise through a drive arm. The unison ring contains a driven arm, which is placed through the cutout portion of the unison ring. This driven arm also moves in the direction of the rotation of the unison ring. The fulcrum of the driven arm is an axis that is integrated with the nozzle vane behind the plate. When the driven arm moves counterclockwise, the nozzle vane moves toward the closing direction. This results in increasing the velocity of the exhaust gas flowing to the turbine, as well as the speed of the turbine. As a result, torque is improved when the engine is running at low speeds. </ptxt>
<figure>
<graphic graphicname="W102363E01" width="7.106578999in" height="2.775699831in"/>
</figure>
<table pgwide="1">
<title>Text in Illustration</title>
<tgroup cols="4">
<colspec colname="COL1" colwidth="0.71in"/>
<colspec colname="COL2" colwidth="2.83in"/>
<colspec colname="COL3" colwidth="0.71in"/>
<colspec colname="COL4" colwidth="2.83in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>*1</ptxt>
</entry>
<entry valign="middle">
<ptxt>Nozzle Vane</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*2</ptxt>
</entry>
<entry valign="middle">
<ptxt>Plate</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*3</ptxt>
</entry>
<entry valign="middle">
<ptxt>DC Motor</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*4</ptxt>
</entry>
<entry valign="middle">
<ptxt>Linkage</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*5</ptxt>
</entry>
<entry valign="middle">
<ptxt>Unison Ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*6</ptxt>
</entry>
<entry valign="middle">
<ptxt>Driven Arm</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*7</ptxt>
</entry>
<entry valign="middle">
<ptxt>Drive Arm</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*8</ptxt>
</entry>
<entry valign="middle">
<ptxt>Fulcrum</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*9</ptxt>
</entry>
<entry valign="middle">
<ptxt>Cutout Portion of Unison Ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*a</ptxt>
</entry>
<entry valign="middle">
<ptxt>Gas Flow</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>*b</ptxt>
</entry>
<entry valign="middle">
<ptxt>Movement of Linkage</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>*c</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rotation Direction of Unison Ring</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>When the engine is running in a medium-to-high speed range, the DC motor pulls up the linkage by a signal from the turbo motor driver. With this, the driven arm moves clockwise and this opens the nozzle vane and holds the specified supercharging pressure, thus lowering the back pressure and improving the output and fuel consumption.</ptxt>
<figure>
<graphic graphicname="W102364" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>