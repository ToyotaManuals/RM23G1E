<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="NM23G1E">
<servcat id="59">
<name>Vehicle Exterior</name>
<section id="12068_S001Z" variety="S001Z">
<name>WIPER / WASHER</name>
<ttl id="12068_S001Z_7BDD6_T002O" variety="T002O">
<name>WIPER AND WASHER SYSTEM</name>
<para id="RM0000045NW007X" category="F" type-id="8048Z" name-id="WW31Z-01" from="201207">
<name>DETAILS</name>
<subpara id="RM0000045NW007X_z0" proc-id="NM23G0E___000007Z00000">
<content5 releasenbr="1">
<step1>
<ptxt>OPERATING CONDITION</ptxt>
<step2>
<ptxt>Headlight Cleaner System</ptxt>
<step3>
<ptxt>The headlight cleaner system operates as follows:</ptxt>
<table pgwide="1">
<tgroup cols="4">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="1.77in"/>
<colspec colname="COL4" colwidth="1.77in"/>
<thead>
<row>
<entry morerows="1" valign="middle" align="center">
<ptxt>Headlight Low Beam</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Headlight Cleaner Switch</ptxt>
</entry>
<entry namest="COL3" nameend="COL4" valign="middle" align="center">
<ptxt>Washer Switch (Windshield Wiper Switch Assembly)</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>On</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On</ptxt>
<ptxt>(First Time)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>On</ptxt>
<ptxt>(Second Time or later)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Turn On</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Activated</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Activated</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not Activated</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Turn Off</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Activated</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not Activated</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Not Activated</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>SYSTEM CONTROL</ptxt>
<step2>
<ptxt>The wiper and washer system has the following functions:</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="2.48in"/>
<colspec colname="COL3" colwidth="3.54in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Function </ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Outline</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry morerows="4" valign="middle">
<ptxt>Wiper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Vehicle Speed-sensing, Adjustable Interval</ptxt>
</entry>
<entry valign="middle">
<ptxt>This function controls the wiper interval time in accordance with the vehicle speed when the windshield wiper switch assembly is in the AUTO or INT position.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Vehicle Speed Switching</ptxt>
</entry>
<entry valign="middle">
<ptxt>Automatically selects an intermittent operation when the vehicle is stopped with the windshield wiper switch assembly in the LO position.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Stopped Vehicle Intermittent Wiper</ptxt>
</entry>
<entry valign="middle">
<ptxt>Automatically selects an intermittent operation when the windshield wiper switch assembly is switched to the LO position from any positions other than LO with the vehicle stopped.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Raindrop Sensing</ptxt>
</entry>
<entry valign="middle">
<ptxt>Controls the wiping interval and speed in accordance with the amount of rain, the vehicle speed and the taillight signals when the windshield wiper switch assembly is in the AUTO position.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Washer-linked Wiper with Drip-prevention</ptxt>
</entry>
<entry valign="middle">
<ptxt>To prevent the fluid from dripping after the washer has been operated, this function operates the wipers once after they have operated in unison with the washer.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Washer</ptxt>
</entry>
<entry valign="middle">
<ptxt>Headlight Cleaner</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sprays washer fluid on the headlight lenses to clean them.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
<step2>
<ptxt>Vehicle Speed-sensing, Adjustable Interval Function</ptxt>
<step3>
<ptxt>When the windshield wiper switch assembly is in the INT position (on models without rain drop sensing function), or AUTO position (on models with rain drop sensing function), the interval time of the wiper is controlled in 3 stages in accordance with the automatic control adjuster position and the vehicle speed.</ptxt>
</step3>
<step3>
<ptxt>The interval time can be controlled steplessly within each range.</ptxt>
<figure>
<graphic graphicname="Y101071E08" width="7.106578999in" height="4.7836529in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Vehicle Speed Switching Function</ptxt>
<step3>
<ptxt>When the windshield wiper switch assembly is in the LO position and the vehicle has stopped running, wiper operation is automatically switched to intermittent operation. </ptxt>
</step3>
<step3>
<ptxt>The front wiper relay circuit judges that the vehicle is being driven if one of the following conditions is met:</ptxt>
<list1 type="unordered">
<item>
<ptxt>The vehicle speed is anything other than 0 km/h (0mph).</ptxt>
</item>
<item>
<ptxt>The stop light switch and the parking brake switch are off and the shift lever is in a position other than P or N.</ptxt>
</item>
</list1>
</step3>
<step3>
<ptxt>If other conditions exist, the front wiper relay circuit judges that the vehicle is stopped. It controls the wipers as shown in the timing chart below.</ptxt>
</step3>
<step3>
<ptxt>When the vehicle stops running and the windshield wiper switch assembly is in the LO position, the wipers operate twice in LO. It then switches to intermittent operation with an interval time of approximately 2.5 seconds. If the vehicle starts driving during this intermittent wiper operation, the wiper returns automatically to LO operation.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Stopped Vehicle Intermittent Wiper Function</ptxt>
<step3>
<ptxt>When the windshield wiper switch assembly is changed to the LO position while the vehicle is stopped, the wipers operate 3 times in LO. It then switches automatically to an intermittent operation with an interval time of approximately 2.5 seconds.</ptxt>
</step3>
<step3>
<ptxt>When the automatic control adjuster is in the FAST position, the wipers operate in LO when the vehicle is stopped. </ptxt>
<figure>
<graphic graphicname="Y104794E01" width="7.106578999in" height="4.7836529in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Raindrop Sensing Function</ptxt>
<step3>
<ptxt>The raindrop sensing function controls the wiper timing in accordance with the amount of raindrops that strikes the windshield when the windshield wiper switch assembly is in the AUTO position.</ptxt>
</step3>
<step3>
<ptxt>The front wiper relay circuit controls the timing based on the signal from the rain sensor.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Washer-linked Wiper with Drip-prevention Function</ptxt>
<step3>
<ptxt>When the windshield wiper switch assembly is set to OFF or AUTO and the washer switch is turned on for approximately 0.2 seconds or more, the wipers start operating in LO at the same time as the washer fluid is sprayed.</ptxt>
</step3>
<step3>
<ptxt>The wipers operate 3 times in LO after the washer switch is turned off.</ptxt>
</step3>
<step3>
<ptxt>As shown in the following diagram, after the end of this operation, the wipers operate once more. They do so after an interval determined by vehicle speed, so that any washer fluid drips are wiped away.</ptxt>
<figure>
<graphic graphicname="Y101074E08" width="7.106578999in" height="4.7836529in"/>
</figure>
<table pgwide="1">
<title>Interval Time Table</title>
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.54in"/>
<colspec colname="COL2" colwidth="3.54in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Vehicle Speed (km/h [mph])</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Interval Time</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>Approx. 0 to 59 (0 to 37)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 3 seconds</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Approx. 60 to 79 (37 to 49)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 5 seconds</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Approx. 80 to 119 (50 to 74)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 7 seconds</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Approx. 120 to 159 (75 to 99)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 5 seconds</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Approx. 160 to 169 (99 to 105)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Approx. 3 seconds</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>Approx. 170 or more (106 or more)</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>No operation</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>CONSTRUCTION</ptxt>
<step2>
<ptxt>Rain Sensor</ptxt>
<step3>
<ptxt>The rain sensor consists of a Light Emitting Diode (LED) that emits infrared rays, a photo diode that can receive those rays, a lens and rain sensor tape.</ptxt>
<figure>
<graphic graphicname="Y101102E05" width="7.106578999in" height="2.775699831in"/>
</figure>
<atten4>
<ptxt>If the rain sensor tape has been peeled off during a windshield glass replacement, make sure to affix new rain sensor tape. Failure to do so will lead to a system malfunction.</ptxt>
</atten4>
</step3>
<step3>
<ptxt>If no raindrops are present in the detection area, the infrared rays emitted by the LED are all reflected from the outer surface of the windshield glass and are received by the photo diode.</ptxt>
</step3>
<step3>
<ptxt>If raindrops are present in the detection area, a portion of the emitted infrared rays penetrates the windshield glass due to the change in the difference of the index of refraction between the glass and the area outside the windshield (difference between air and water). The ability of the windshield glass to reflect all light back inside changes due to the presence of raindrops. This change of internal reflection due to the presence of water reduces the amount of infrared rays that are received by the photo diode. The amount of this reduction is then used to determine the amount of raindrops. Thus, this function selects the INT, LO, and HI wiper operations in order to operate the wipers at an optimal speed and timing.</ptxt>
<figure>
<graphic graphicname="Y101103E06" width="7.106578999in" height="2.775699831in"/>
</figure>
</step3>
</step2>
<step2>
<ptxt>Washer Nozzle</ptxt>
<step3>
<ptxt>Spray type washer nozzles are located under the engine hood to ensure good appearance. These nozzles can spray windshield washer fluid over a wide area by spraying it in a fan shape. The washer fluid volume has been reduced so as not to hinder the driver's view when the washer system is operated.</ptxt>
<figure>
<graphic graphicname="Y101075E06" width="7.106578999in" height="3.779676365in"/>
</figure>
<atten4>
<ptxt>Spray type washer nozzles cannot be adjusted because of their structure. Do not attempt to adjust the nozzles as it could damage them. If adjustment is necessary, adjust the nozzles by replacing them with nozzles that have different spray angles. For details, refer to the corresponding Repair Manual for this model.</ptxt>
</atten4>
</step3>
</step2>
</step1>
<step1>
<ptxt>FAIL-SAFE</ptxt>
<step2>
<ptxt>The following fail-safe measures are taken if a malfunction is detected by the windshield wiper switch assembly.</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.77in"/>
<colspec colname="COL3" colwidth="3.54in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Trouble Area</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Wiper Motor Malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stops the operation of the raindrop sensing function.</ptxt>
</entry>
</row>
<row>
<entry morerows="1" valign="middle">
<ptxt>Communication Signal Malfunction</ptxt>
</entry>
<entry valign="middle">
<ptxt>Rain Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stops the operation of the raindrop sensing function.</ptxt>
<ptxt>When a signal cannot be received from the rain sensor, the raindrop sensing function is not performed and the status that existed prior to the occurrence of abnormality is maintained.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Combination Meter Assembly</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Stops the operation of the vehicle speed-sensing, adjustable interval vehicle function.</ptxt>
</item>
<item>
<ptxt>Stops the operation of the speed switching function.</ptxt>
</item>
<item>
<ptxt>Stops the operation of the stopped vehicle intermittent wiper function.</ptxt>
</item>
<item>
<ptxt>The wiper interval can be adjusted.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>