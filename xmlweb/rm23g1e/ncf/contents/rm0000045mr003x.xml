<?xml version="1.0" encoding="UTF-8"?>
<tmc-service-inc>
<pub id="NM23G1E">
<servcat id="58">
<name>Vehicle Interior</name>
<section id="12057_S001S" variety="S001S">
<name>PRE-CRASH SAFETY</name>
<ttl id="12057_S001S_7BDCV_T002D" variety="T002D">
<name>PRE-CRASH SAFETY SYSTEM</name>
<para id="RM0000045MR003X" category="F" type-id="8048Z" name-id="PC0VB-01" from="201207">
<name>DETAILS</name>
<subpara id="RM0000045MR003X_z0" proc-id="NM23G0E___000006U00000">
<content5 releasenbr="1">
<step1>
<ptxt>FUNCTION OF MAIN COMPONENTS</ptxt>
<step2>
<ptxt>The pre-crash safety system consists of the following parts:</ptxt>
<table pgwide="1">
<tgroup cols="3">
<colspec colname="COL1" colwidth="1.42in"/>
<colspec colname="COL2" colwidth="1.42in"/>
<colspec colname="COL3" colwidth="4.24in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Component</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Function</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Millimeter Wave Radar Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Radiates millimeter radio wave radar forward, uses the reflected millimeter radio wave for detecting the presence of a vehicle being driven ahead, the vehicle-to-vehicle distance, and the relative speed, and transmits these pieces of information to the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Speed Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the wheel speed signal and transmits the signal to the skid control ECU.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Brake Actuator Assembly</ptxt>
<ptxt>- Skid Control ECU</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Receives a brake assist standby request signal from the driving support ECU and switches the brake assist to the standby mode. When the driver depresses the brake pedal, it operates the brake assist.</ptxt>
</item>
<item>
<ptxt>Even if the driver does not apply the brakes, if the skid control ECU receives a pre-crash brake request signal from the driving support ECU, it applies the brakes.</ptxt>
</item>
<item>
<ptxt>Determines that the brakes have been applied suddenly through a signal received from the master cylinder pressure sensor, and outputs a seat belt operation signal to the seat belt control ECU.</ptxt>
</item>
<item>
<ptxt>Determines the presence of a front wheel skid tendency or rear wheel skid tendency, and outputs a seat belt operation signal to the seat belt control ECU.</ptxt>
</item>
<item>
<ptxt>Transmits a vehicle speed signal to the driving support ECU.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Front Seat Outer Belt Assembly</ptxt>
<ptxt>- Seat Belt Motor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Retracts the seat belt in accordance with signals received from the seat belt control ECU.</ptxt>
</entry>
</row>
<row>
<entry morerows="3" valign="middle">
<ptxt>Combination Meter Assembly</ptxt>
</entry>
<entry valign="middle">
<ptxt>PCS Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates or blinks to warn the driver in accordance with signals from the seat belt control ECU and the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Multi-information Display</ptxt>
</entry>
<entry valign="middle">
<ptxt>Displays a warning message to inform or warn the driver of the system condition in accordance with signals from the seat belt control ECU and the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Master Warning Light</ptxt>
</entry>
<entry valign="middle">
<ptxt>Illuminates to warn the driver in accordance with signals from the seat belt control ECU and the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Multi Buzzer</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sounds to warn the driver in accordance with signals from the seat belt control ECU and the driving support ECU when the system is malfunctioning.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Steering Angle Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the angle and direction of steering and transmits a signal to the skid control ECU and the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Yaw Rate Sensor</ptxt>
</entry>
<entry valign="middle">
<ptxt>Detects the yaw rate and lateral / longitudinal deceleration of the vehicle and transmits the signal to the skid control ECU and the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Seat Belt Control ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Receives a seat belt operation request signal from the driving support ECU or skid control ECU and operates the seat belts.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Driving Support ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Makes judgments on whether a collision is unavoidable based on the information received from the millimeter wave radar sensor. It then outputs a seat belt operation signal and brake assist standby request signal if required.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Suspension Control ECU</ptxt>
</entry>
<entry valign="middle">
<ptxt>Sends control signals to the absorber control actuator based on the signals of the driving support ECU.</ptxt>
</entry>
</row>
<row>
<entry namest="COL1" nameend="COL2" valign="middle">
<ptxt>Pre-crash Brake Cancel Switch</ptxt>
</entry>
<entry valign="middle">
<ptxt>Stops the pre-crash brake control when the switch is turned on. The PCS warning light illuminates to notify that the pre-crash brake control is off.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<atten4>
<ptxt>When the seat belt control ECU is replaced, active test of the seat belt motor must be performed in accordance with the following procedure to check that the motor operates properly:</ptxt>
<list1 type="ordered">
<item>
<ptxt>Engine switch is on.</ptxt>
</item>
<item>
<ptxt>Monitor the PCS warning light for at least 3 seconds after turning the engine switch on (IG). Make sure that the warning light does not remain on.</ptxt>
</item>
<item>
<ptxt>Connect the intelligent tester II to the DLC3 and enter the select active test mode.</ptxt>
</item>
<item>
<ptxt>Perform active test of the front seat belt motor and check that the motor operates properly. For details, refer to the corresponding Repair Manual for this model.</ptxt>
</item>
</list1>
</atten4>
</step2>
</step1>
<step1>
<ptxt>OPERATING CONDITION</ptxt>
<step2>
<ptxt>Controls on the pre-crash safety system are performed under the following conditions:</ptxt>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="3.19in"/>
<colspec colname="COL2" colwidth="3.89in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Control</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Condition</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<ptxt>Pre-crash Seat Belt</ptxt>
<ptxt>(Sudden Braking Operation/Front or Rear Wheel Skid Tendency Operation)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front seat belt is buckled.</ptxt>
</item>
<item>
<ptxt>Vehicle speed is approx. 30 km/h (20 mph) or above.</ptxt>
</item>
<item>
<ptxt>Brakes are suddenly applied, or front or rear wheel skid tendency is detected.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pre-crash Seat Belt</ptxt>
<ptxt>(Pre-crash Safety Operation)</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Front seat belt is buckled.</ptxt>
</item>
<item>
<ptxt>Vehicle speed is approx. 5 km/h (3 mph) or above.</ptxt>
</item>
<item>
<ptxt>Oncoming vehicle or an obstacle relative speed is approx. 30 km/h (20 mph) or above.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pre-crash Brake Assist</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Vehicle speed is approx. 30 km/h (20 mph) or above.</ptxt>
</item>
<item>
<ptxt>Oncoming vehicle or an obstacle relative speed is approx. 30 km/h (20 mph) or above.</ptxt>
</item>
<item>
<ptxt>Brake pedal is depressed.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>Pre-crash Brake</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>Pre-crash brake cancel switch is not pressed.</ptxt>
</item>
<item>
<ptxt>Vehicle speed is approx. 15 km/h (10 mph) or above.</ptxt>
</item>
<item>
<ptxt>Oncoming vehicle or an obstacle relative speed is approx. 15 km/h (10 mph) or above.</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle">
<ptxt>AVS</ptxt>
</entry>
<entry valign="middle">
<list1 type="nonmark">
<item>
<ptxt>Vehicle speed is approx. 5 km/h (3 mph) or above.</ptxt>
</item>
<item>
<ptxt>Oncoming vehicle or an obstacle relative speed is approx. 30 km/h (20 mph) or above.</ptxt>
</item>
</list1>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step2>
</step1>
<step1>
<ptxt>SYSTEM CONTROL</ptxt>
<step2>
<ptxt>Front or Rear Wheel Skid Tendency Operation</ptxt>
<step3>
<ptxt>The front or rear wheel skid tendency operation is performed as follows:</ptxt>
<figure>
<graphic graphicname="Y104236E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="6.02in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>While the vehicle is traveling at approx. 30 km/h (20 mph) or above, the skid control ECU determines the front or rear wheel skid tendency condition based on the signals from the steering angle sensor, yaw rate sensor and speed sensors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The skid control ECU outputs a seat belt operation request signal to the seat belt control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The seat belt control ECU determines the front seat belt motor operation conditions based on this signal and the seat belt buckle switch signal. Then, it retracts the seat belts by operating the seat belt motors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The seat belts return to a normal state when the relevant conditions of the vehicle have stabilized.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Sudden Braking Operation</ptxt>
<step3>
<ptxt>The sudden braking operation is performed as follows:</ptxt>
<figure>
<graphic graphicname="Y104237E01" width="7.106578999in" height="5.787629434in"/>
</figure>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="6.02in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>While the vehicle is traveling at approx. 30 km/h (20 mph) or above, the skid control ECU can determine a sudden braking condition based on the signals from the master cylinder pressure sensor, wheel cylinder pressure sensors, stop light switch and speed sensors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>At this time, the skid control ECU outputs a seat belt operation request signal to the seat belt control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The seat belt control ECU determines the front seat belt motor operation conditions based on this signal and the seat belt buckle switch signal. Then, it retracts the seat belts by operating the seat belt motors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The seat belts return to a normal state when the brake pedal is released.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
<step2>
<ptxt>Pre-crash Safety Operation (Models with Pre-crash Brake Control)</ptxt>
<step3>
<ptxt>The pre-crash safety operation is performed as follows:</ptxt>
<figure>
<graphic graphicname="Y102344E01" width="7.106578999in" height="8.799559038in"/>
</figure>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="6.02in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The driving support ECU determines that the possibility of a collision is high based on the signals received from the millimeter wave radar sensor, steering angle sensor, speed sensors, and yaw rate sensor.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>At this time, the driving support ECU sends the following signals:</ptxt>
<list1 type="unordered">
<item>
<ptxt>A caution display request signal is sent to the multi-information display.</ptxt>
</item>
<item>
<ptxt>A caution buzzer request signal is sent to the skid control ECU.</ptxt>
</item>
<item>
<ptxt>An AVS control operation request signal is sent to the suspension control ECU.*</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upon receiving this signal, the skid control ECU sets the brake assist to the standby mode, and the absorber control ECU optimizes the amount of damping of the absorber control actuators*.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The driving support ECU determines that an unavoidable collision condition exists based on the signals received from the millimeter wave radar sensor, speed sensors, steering angle sensor, and yaw rate sensor.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>At this time, the driving support ECU outputs a seat belt operation request signal to the seat belt control ECU, a brake request signal to the skid control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The seat belt control ECU determines the front seat belt motor operation condition based on this signal and the seat belt buckle switch signal, and retracts the seat belts by operating the front seat belt motors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If no collision occurs, the seat belts and the brake assist will return to their normal states.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*:	Models with AVS</ptxt>
</item>
</list1>
</step3>
</step2>
<step2>
<ptxt>Pre-crash Safety Operation (Models without Pre-crash Brake Control)</ptxt>
<step3>
<ptxt>The pre-crash safety operation is performed as follows:</ptxt>
<figure>
<graphic graphicname="Y102345E01" width="7.106578999in" height="7.795582503in"/>
</figure>
<table pgwide="1">
<tgroup cols="2">
<colspec colname="COL1" colwidth="1.06in"/>
<colspec colname="COL2" colwidth="6.02in"/>
<tbody>
<row>
<entry valign="middle" align="center">
<ptxt>1)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The driving support ECU determines that the possibility of a collision is high based on the signals received from the millimeter wave radar sensor, steering angle sensor, speed sensors, and yaw rate sensor.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>2)</ptxt>
</entry>
<entry valign="middle">
<ptxt>At this time, the driving support ECU sends the following signals:</ptxt>
<list1 type="unordered">
<item>
<ptxt>A caution display request signal is sent to the multi-information display.</ptxt>
</item>
<item>
<ptxt>An AVS control operation request signal is sent to the suspension control ECU.*</ptxt>
</item>
</list1>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>3)</ptxt>
</entry>
<entry valign="middle">
<ptxt>Upon receiving this signal, the skid control ECU sets the brake assist to the standby mode, and the absorber control ECU optimizes the amount of damping of the absorber control actuators*.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>4)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The driving support ECU determines that an unavoidable collision condition exists based on the signals received from the millimeter wave radar sensor, speed sensors, steering angle sensor, and yaw rate sensor.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>5)</ptxt>
</entry>
<entry valign="middle">
<ptxt>At this time, the driving support ECU outputs a seat belt operation request signal to the seat belt control ECU.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>6)</ptxt>
</entry>
<entry valign="middle">
<ptxt>The seat belt control ECU determines the front seat belt motor operation condition based on this signal and the seat belt buckle switch signal, and retracts the seat belts by operating the front seat belt motors.</ptxt>
</entry>
</row>
<row>
<entry valign="middle" align="center">
<ptxt>7)</ptxt>
</entry>
<entry valign="middle">
<ptxt>If no collision occurs, the seat belts and the brake assist will return to their normal states.</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
<list1 type="nonmark">
<item>
<ptxt>*: Models with AVS</ptxt>
</item>
</list1>
</step3>
</step2>
</step1>
<step1>
<ptxt>CONSTRUCTION</ptxt>
<step2>
<ptxt>Millimeter Wave Radar Sensor</ptxt>
<step3>
<ptxt>The millimeter wave radar sensor consists of a millimeter wave radar circuit, signal processing circuit, and CPU.</ptxt>
</step3>
<step3>
<ptxt>For details on the millimeter wave radar sensor, see the Dynamic Radar Cruise Control System.</ptxt>
</step3>
</step2>
<step2>
<ptxt>Combination Meter Assembly</ptxt>
<step3>
<ptxt>The combination meter assembly provides a master warning light, PCS warning light, buzzer, and multi-information display to warn and indicate in the pre-crash safety system.</ptxt>
</step3>
<step3>
<ptxt>If the driving support ECU determines that the possibility of a collision is high, it sends a signal to the combination meter assembly. Upon receiving this signal, the combination meter assembly indicates a warning on the multi-information display. This warning is only displayed on models with pre-crash brake control. Details are indicated below:</ptxt>
<table pgwide="1">
<tgroup cols="5">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.06in"/>
<colspec colname="COL3" colwidth="2.12in"/>
<colspec colname="COL4" colwidth="1.06in"/>
<colspec colname="COL5" colwidth="1.07in"/>
<thead>
<row>
<entry namest="COL1" nameend="COL2" valign="middle" align="center">
<ptxt>Multi-information Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Details</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>PCS Warning Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Skid Control Buzzer Assembly</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<graphic graphicname="Y104240" width="1.3385826771653544in" height="0.9448818897637796in"/>
</entry>
<entry valign="middle">
<ptxt>Displays</ptxt>
</entry>
<entry valign="middle">
<list1 type="unordered">
<item>
<ptxt>The driving support ECU has determined that the possibility of a collision is high.</ptxt>
</item>
<item>
<ptxt>The driving support ECU has determined that the possibility of a collision is even higher than the above.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>Blinks</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Intermittently Sounds</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
<step3>
<ptxt>2 types of warning messages are used for the pre-crash safety system, as described below. The pre-crash safety system will not operate when these messages appear in the combination meter assembly:</ptxt>
<table pgwide="1">
<tgroup cols="6">
<colspec colname="COL1" colwidth="1.77in"/>
<colspec colname="COL2" colwidth="1.91in"/>
<colspec colname="COL3" colwidth="0.85in"/>
<colspec colname="COL4" colwidth="0.85in"/>
<colspec colname="COL5" colwidth="0.85in"/>
<colspec colname="COL6" colwidth="0.85in"/>
<thead>
<row>
<entry valign="middle" align="center">
<ptxt>Multi-information Display</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Detail</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Master Warning Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>PCS Warning Light</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Multi Buzzer</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Diagnostic Trouble Code (DTC)</ptxt>
</entry>
</row>
</thead>
<tbody>
<row>
<entry valign="middle">
<graphic graphicname="Y104241" width="1.3385826771653544in" height="0.9448818897637796in"/>
</entry>
<entry valign="middle">
<ptxt>This message appears when the seat belt control ECU and the driving support ECU detects a system malfunction.</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Blinks</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Sounds Once</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>○</ptxt>
</entry>
</row>
<row>
<entry valign="middle">
<graphic graphicname="Y104242" width="1.3385826771653544in" height="0.9448818897637796in"/>
</entry>
<entry valign="middle">
<ptxt>This message appears when the seat belt control ECU determines the following conditions:</ptxt>
<list1 type="unordered">
<item>
<ptxt>Dirty millimeter wave radar sensor</ptxt>
</item>
<item>
<ptxt>Poor weather condition</ptxt>
</item>
<item>
<ptxt>Overheated seat belt control ECU </ptxt>
</item>
<item>
<ptxt>After these conditions have been resolved, the system will operate normally.</ptxt>
</item>
</list1>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>Illuminates</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
<entry valign="middle" align="center">
<ptxt>-</ptxt>
</entry>
</row>
</tbody>
</tgroup>
</table>
</step3>
</step2>
</step1>
<step1>
<ptxt>DIAGNOSIS</ptxt>
<step2>
<ptxt>Initial Check</ptxt>
<step3>
<ptxt>The seat belt control ECU performs an initial check on the system for approximately 3 seconds after the engine switch has been turned on.</ptxt>
</step3>
</step2>
<step2>
<ptxt>DTC Output</ptxt>
<step3>
<ptxt>If the seat belt control ECU detects a malfunction in the pre-crash safety system, it stores a 5- digit DTC in memory.</ptxt>
</step3>
<step3>
<ptxt>The 5-digit DTC can be read by connecting an intelligent tester II to the DLC3.</ptxt>
</step3>
</step2>
</step1>
</content5>
</subpara>
</para>
</ttl>
</section>
</servcat>
</pub>
</tmc-service-inc>