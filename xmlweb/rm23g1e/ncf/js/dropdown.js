/* $Id: dropdown.js,v 1.13 2004/09/24 10:55:31 k-narumi Exp $ */
//===========================================================================
// ドロップダウンリスト
//
// for Translation
//   000  ITEM_NOT_FOUND
//===========================================================================

var DROPDOWN_WIDTH    = 450;   // ドロップダウンの表示幅
var DROPDOWN_OFFSET_X = 3;     // ドロップダウンの表示位置X方向オフセット
var DROPDOWN_OFFSET_Y = 15;    // ドロップダウンの表示位置Y方向オフセット
var DROPDOWN_DELAY    = 2000;  // ドロップダウンを自動的消去までの時間
var SCROLL_THRESHOLD  = 5;     // スクロールバーを出現個数
var SCROLLBAR_MARGIN  = 16;    // スクロールバーのマージン
var LINE_HEIGHT       = 20;    // 1行の高さ

var g_dropdown        = null;  // ドロップダウンオブジェクト
var g_dtid            = null;  // ドロップダウン消去処理命令ID
var g_dclear          = false; // ドロップダウン消去フラグ
// 適用時期XPath
var g_queryFromTo     = "";
var g_queryFromOnly   = "";
var g_queryToOnly     = "";
var g_queryNone       = "((@from='' or not(@from)) and (@to='' or not(@to)))";

//===========================================================================
// ドロップダウンリストを表示
//
// pos         表示位置
// obj         表示内容
// windowTitle ウィンドウタイトル
//===========================================================================
function findPos(obj, foundScrollLeft, foundScrollTop) {
    var curleft = 0;
    var curtop = 0;
    if(obj.offsetLeft) curleft += parseInt(obj.offsetLeft);
    if(obj.offsetTop) curtop += parseInt(obj.offsetTop);
    if(obj.scrollTop && obj.scrollTop > 0) {
        curtop -= parseInt(obj.scrollTop);
        foundScrollTop = true;
    }
    if(obj.scrollLeft && obj.scrollLeft > 0) {
        curleft -= parseInt(obj.scrollLeft);
        foundScrollLeft = true;
    }
    if(obj.offsetParent) {
        var pos = findPos(obj.offsetParent, foundScrollLeft, foundScrollTop);
        curleft += pos[0];
        curtop += pos[1];
    } else if(obj.ownerDocument) {
        var thewindow = obj.ownerDocument.defaultView;
        if(!thewindow && obj.ownerDocument.parentWindow)
            thewindow = obj.ownerDocument.parentWindow;
        if(thewindow) {
            if (!foundScrollTop && thewindow.scrollY && thewindow.scrollY > 0) curtop -= parseInt(thewindow.scrollY);
            if (!foundScrollLeft && thewindow.scrollX && thewindow.scrollX > 0) curleft -= parseInt(thewindow.scrollX);
            if(thewindow.frameElement) {
                var pos = findPos(thewindow.frameElement);
                curleft += pos[0];
                curtop += pos[1];
            }
        }
    }
    return [curleft,curtop];
}

function createDropDown(pos, obj, windowTitle) {
if(!window.createPopup){
    window.createPopup = function (){
		var popup = window.top.document.getElementsByTagName('dialog')[0];
		if (popup == null)
		{
			popup = window.top.document.body.appendChild(window.top.document.createElement('dialog'));
		}
		popup.Id = "popupDialog";
		popup.innerHTML = '<div id="popup-body"></div>';
		popup.document = window.top.document.getElementById('popup-body');
		popup.document.contentWindow = window;

		var isShown = false;

        popup.hide = function (){
            isShown = false;
			if (window.top.document.getElementById('popup-body') != null)
			{
				popup.close();
				window.top.document.body.removeChild(popup);
			}
        }

        popup.showCustom = function (x, y, w, h, pElement){
            if(typeof(x) !== 'undefined'){
                var elPos = [0, 0];
                if(pElement) elPos = findPos(pElement);//maybe validate that this is a DOM node instead of just falsy
                elPos[0] += y, elPos[1] += x;

                if(isNaN(w)) w = popup.document.scrollWidth;
                if(isNaN(h)) h = popup.document.scrollHeight;
                if(elPos[0] + parseInt(w) > window.top.document.body.clientWidth) elPos[0] = window.top.document.body.clientWidth - parseInt(w) - 5;
                if(elPos[1] + parseInt(h) > window.top.document.body.clientHeight) elPos[1] = window.top.document.body.clientHeight - parseInt(h) - 5;

				popup.style.position = "absolute";
                popup.style.left = elPos[0] + "px";
                popup.style.top = elPos[1] + "px";
                popup.style.width = w + "px";
                //popup.style.height = h + "px";
				popup.style.height = "auto";
            }
			popup.style.right = "auto";
			popup.style.bottom = "auto";
            popup.style.display = "block";
			popup.style.padding = "0px 0px 0px 0px";
			popup.showModal();
            isShown = true;
        }

		var popupClicked = false;
        var hidepopup = function (event){
            if(isShown)
                setTimeout(function (){
                    if(!popupClicked){
                        popup.hide();
                    }
                    popupClicked = false;
                }, 150);//timeout will allow the click event to trigger inside the frame before closing.*/
        }
//        window.addEventListener('click', hidepopup, true);
//        window.addEventListener('blur', hidepopup, true);
        return popup;
    }
}

	innerURLs = obj.getElementsByTagName("a")
	if (innerURLs.length == 0) return;
	if (innerURLs.length < 2) {
	  var url = innerURLs[0];
		selectDropDown(url, windowTitle);
		return;
	}
	var height = LINE_HEIGHT * innerURLs.length + SCROLLBAR_MARGIN;
	if (height > LINE_HEIGHT * SCROLL_THRESHOLD + SCROLLBAR_MARGIN)
		height = LINE_HEIGHT * SCROLL_THRESHOLD + SCROLLBAR_MARGIN;
	g_dropdown = window.createPopup();

if(typeof g_dropdown.document.createStyleSheet === 'undefined') {
    g_dropdown.document.createStyleSheet = (function() {
        function createStyleSheet(href) {
            if(typeof href !== 'undefined') {
                var element = g_dropdown.ownerDocument.createElement('link');
                element.type = 'text/css';
                element.rel = 'stylesheet';
                element.href = href;
            }
            else {
                var element = g_dropdown.ownerDocument.createElement('style');
                element.type = 'text/css';
            }

            g_dropdown.ownerDocument.getElementsByTagName('head')[0].appendChild(element);

			var style = g_dropdown.ownerDocument.createElement("style");
			g_dropdown.ownerDocument.head.appendChild(style); // must append before you can access sheet property
			var sheet = style.sheet;

            if(typeof sheet.addRule === 'undefined')
                sheet.addRule = addRule;

            if(typeof sheet.removeRule === 'undefined')
                sheet.removeRule = sheet.deleteRule;

            return sheet;
        }

        function addRule(selectorText, cssText, index) {
            if(typeof index === 'undefined')
                index = this.cssRules.length;

            this.insertRule(selectorText + ' {' + cssText + '}', index);
        }

        return createStyleSheet;
    })();
}

	g_dropdown.showCustom(DROPDOWN_OFFSET_X, DROPDOWN_OFFSET_Y, DROPDOWN_WIDTH, height, pos);
	g_dropdown.document.onmouseover = clearDropDownDelay;
	g_dropdown.document.onmouseout = setDropDownDelay;
	g_dropdown.document.createStyleSheet("../../css/dropdown.css");
	g_dropdown.document.style.overflow = "auto";
	g_dropdown.document.style.border = "solid 1px #999999";
	g_dropdown.document.innerHTML = obj.innerHTML;
	g_dclear = false;
}

//===========================================================================
// ドロップダウンリストを消去する
//===========================================================================
function undispDropDown() {
	if (g_dropdown!=null) g_dropdown.hide();
}

//===========================================================================
// 指定時間後ドロップダウン消去設定を行なう
//===========================================================================
function setDropDownDelay() {
	g_dtid = setTimeout("undispDropDown()", DROPDOWN_DELAY);
	g_dclear = false;
}

//===========================================================================
// ドロップダウン消去設定を解除する
//===========================================================================
function clearDropDownDelay() {
	clearTimeout(g_dtid);
	g_dclear = true;
}

//===========================================================================
// 消去設定
//===========================================================================
function dropDownDelayUndisp() {
	if (!g_dclear) setDropDownDelay();
}

//===========================================================================
// ドロップダウン選択でPOPUP表示
//
// obj          HTMLアンカーオブジェクト(IDは'paraId_subparaId'形式)
// windowTitle  クリックした時に表示されるウィンドウのタイトル
//===========================================================================
var ITEM_NOT_FOUND = "There are no information available." // 000
function selectDropDown(obj, windowTitle) {
	var objId = obj.id;
	var paraId = objId;
	var regOther = new RegExp("ncf|ewd", "g");
	if (paraId.search(regOther)>0) {
		openNewWindow(paraId, "", g_term, g_vin, g_filter);
		return null;
	}
	var ids = objId.split("_");
	paraId = ids[0];
	var subparaId = (ids.length>1) ? ids[0]+"_"+ids[1] : null;

	var objXml = initXml(paraId.toLowerCase()+".xml", false, false);
	if (objXml==null) {
		alert(ITEM_NOT_FOUND);
		return null;
	}
	var objCat = getFirstElementByXPath(objXml, "//para/@category").textContent;
	var objXsl = (objCat=="C" || objCat=="J") 
						 ? initXsl("../../xsl/trb/help.xsl")
						 : initXsl("../../xsl/toc/contents.xsl")
	var args = new Hashtable();
	args.put("xml", objXml);
	args.put("xsl", objXsl);
	args.put("title", windowTitle);
	setSeePageMargin((screen.width - SEEPAGE_WIDTH) / 2);
	args.put("margin", getSeePageMargin());
	if (subparaId!=null) args.put("subparaId", subparaId);
	(async(args) => {
	setDialogArgs(args);
	await openDialog("../etc/notes_frame.html", "nts");
	clearDialogArgs();
	})(args);
}

//===========================================================================
// ドロップダウン表示内容を作成する
//
// itemNodeList   表示対象のノードリスト
// obj            表示場所となるオブジェクト
// windowTitle    ウィンドウタイトル
// htmlFile       呼び出し元のHTMLファイル
//===========================================================================
function createDropDownItem(itemNodeList, obj, windowTitle, htmlFile) {
	var ret = "";
	for (var i=0; i<itemNodeList.length; i++) {
		var itemNode = itemNodeList[i];
		var itemId = itemNode.getAttribute("id");
		var itemName = getFirstElementByXPath(itemNode.ownerDocument, "name", itemNode).textContent;
		ret += "<div>";
		ret += "<a id='"+itemId+"' style='cursor: pointer;' class='inside' onClick='top."+htmlFile+".selectDropDown(this, \""+windowTitle +"\")'>";
		ret += itemName;
		ret += "</a>";
		ret += "</div>";
	}
	obj.innerHTML=ret;
}

//===========================================================================
// EWD, NCFのドロップダウンリストを作成する
//
// objXml     配線図、解説書XML
// obj        ドロップダウンリストの内容を作成するHTMLオブジェクト
// titleName  システム名(修理書: タイトル名)
// type       コンテンツのタイプ文字列("ewd" or "ncf")
// term       適用時期
//===========================================================================
function createDropDownOther(objXml, obj, titleName, type, term) {
	if (objXml==null || titleName==null || titleName.length==0) return;
	createTermXPath(term);
	if (g_queryFromTo.length==0) return;
	var urlNodes = new Array();
	urlNodes_tmp = getElementsByXPath(objXml, "//para["+g_queryFromTo+" or "+g_queryFromOnly+" or "+g_queryToOnly+" or "+g_queryNone+"]");
	for (var i=urlNodes_tmp.length-1; i>=0; i--) {
		var nodePara = urlNodes_tmp[i];
		listSysName = nodePara.getElementsByTagName("system-name");
		for (var j=0; j<listSysName.length; j++) {
			var sysName = listSysName[j].textContent;
			if (titleName==sysName) {
				urlNodes.push(nodePara);
			}
		}
	}
	var ret = "";
	var length= urlNodes.length;
	for (var i=0; i<length; i++) {
		var itemNode = urlNodes.pop();
		var itemName = "";
		var itemUrl  = "../../../"+type+"/";
		if (getFirstElementByXPath(objXml, "name", itemNode)!=null) {
			itemName = getFirstElementByXPath(objXml, "name", itemNode).textContent;
			itemUrl += itemNode.getAttribute("url");
		} else {
			itemName = getFirstElementByXPath(objXml, "ancestor::section/name", itemNode).textContent;
			itemUrl += getFirstElementByXPath(objXml, "ancestor::section", itemNode).getAttribute("url");
		}
		ret += "<div>";
		ret += "<a id='"+itemUrl+"' style='cursor: pointer;' class='inside' onClick='parent.selectDropDown(this, \"\")'>";
		ret += itemName;
		ret += "</a>";
		ret += "</div>";
	}
	obj.innerHTML=ret;
}

//===========================================================================
// 適用時期用のXPath文字列を作成する(private member)
// 
// term 適用時期
//===========================================================================
function createTermXPath(term) {
	if (term==null) return;
	g_queryFromTo   = "(@from <= "+term+" and "+term+" < @to)";
	g_queryFromOnly = "(@from <= "+term+" and (@to='' or not(@to)))";
	g_queryToOnly   = "((@from='' or not(@from)) and "+term+" < @to)";
}